# @(#) envx.sh 
# @(#) Mynor Ramirez
# @(#) Mayo 2010            
# @(#) Shell utilizado para cargar el ambiente de desarrollo
# @(#) Se carga lo siguiente:
# @(#) 1. Variables del Desarollador (Genero)
# @(#) 2. Variables de Base de datos (Informix)
# @(#) 3. Variables de aplicacion   

# @(#) Variables del Desarollador (Genero)
. /opt/4js/gst310/dev/envgenero

# @(#) 2. Variables de Base de datos (Informix)
. /opt/informix/ifmx.sh

# @(#) 3. Variables de aplicacion   
. /app/cmd/enva.sh 
