# @(#) enva.sh 
# @(#) Mynor Ramirez
# @(#) Mayo 2010            
# @(#) Shell utilizado para cargar las variables de aplicacion 

# Asignando valores
export USERID=`logname`
export TERM=vt100
export DIRAPL=/app
export DIRCMD=$DIRAPL/cmd
export TERMCAP=$DIRCMD/termcap
export SPOOLDIR=$HOME/spl
export PATH=$DIRCMD:$PATH
