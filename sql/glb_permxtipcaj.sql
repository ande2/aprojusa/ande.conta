






{ TABLE "sistemas".fac_permxcct row size = 40 number of columns = 5 index size = 22 }

create table "sistemas".fac_permxcct 
  (
    cencos smallint not null ,
    userid char(15) not null ,
    usuaid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (cencos,userid)  constraint "sistemas".pkfacpermxcct
  );

revoke all on "sistemas".fac_permxcct from "public" as "sistemas";




