-- Actualizaciones
-- Enero 19 2011

drop table inv_provedrs ;
create table "sistemas".inv_provedrs
  (
    codprv integer not null ,
    nomprv varchar(100,1) not null ,
    codabr char(2) not null ,
    numnit varchar(30,1) not null ,
    numtel varchar(30,1),
    numfax varchar(30,1),
    dirprv varchar(100,1) not null ,
    nomcon varchar(50,1),
    bemail varchar(100,1),
    codpai smallint not null ,
    observ varchar(255,1),
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (codabr)  constraint "sistemas".uqinvprovedrs,
    primary key (codprv) constraint "sistemas".pkinvprovedrs,
    foreign key (codpai) references glb_mtpaises constraint "sistemas".fkglbmtpaises1
  );

alter table inv_subcateg
modify (subcat char(4) not null);

--alter table his_preciosm
--drop constraint fkinvproducts2;

alter table his_preciosm
add constraint
(
 foreign key (cditem) references inv_products on delete cascade constraint fkinvproducts2
);

load from parametos.unl insert into glb_paramtrs;
