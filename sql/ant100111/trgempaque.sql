create trigger trginsertarempaque insert on inv_products referencing new as pos
    for each row
        (
        execute procedure sptempaquedefault(pos.cditem)
       );
