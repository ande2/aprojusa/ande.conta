
DROP PROCEDURE sptsaldosxproducto;
CREATE PROCEDURE sptsaldosxproducto(wcodemp SMALLINT,wcodsuc SMALLINT,wcodbod SMALLINT,
                                    wtipmov SMALLINT,waniotr SMALLINT,wmestra SMALLINT,
                                    wcditem INT, wtipope SMALLINT,wcodabr VARCHAR(20))

 -- Procedimiento para calcular el saldo de un producto    
 -- Los saldos son por empresa, sucursal, bodega, tipo de saldo, anio, mes y producto
 -- Mynor Ramirez
 -- Diciembre 2010 

 DEFINE wtipsld  LIKE inv_saldopro.tipsld;
 DEFINE wsalteo  LIKE inv_saldopro.tipsld;
 DEFINE xcanuni  LIKE inv_saldopro.canuni;
 DEFINE xprepro  LIKE inv_saldopro.prepro;
 DEFINE xtotpro  LIKE inv_saldopro.totpro;
 DEFINE xcodemp  LIKE inv_saldopro.codemp;

-- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Obteniendo tipo de saldo para SALDO TEORICO 
 LET wsalteo = NULL;
 SELECT NVL(a.valchr,"0")
  INTO  wsalteo
  FROM  glb_paramtrs a
  WHERE a.numpar = 3
    AND a.tippar = 1;

 -- Obtienendo tipo de saldo del tipo de movimiento por el tipo de operacion 
 LET wtipsld = NULL;
 SELECT NVL(a.valchr,"0")
  INTO  wtipsld 
  FROM  glb_paramtrs a
  WHERE a.numpar = 4
    AND a.tippar = wtipope;

 -- Verificando si tipo de saldo ya existe
 LET xcodemp = NULL;
 SELECT UNIQUE a.codemp
  INTO  xcodemp
  FROM  inv_saldopro a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.tipsld = wtipsld
    AND a.aniotr = waniotr
    AND a.mestra = wmestra
    AND a.cditem = wcditem;

 -- Si ya existe el tipo de saldo lo borra
 IF xcodemp IS NOT NULL THEN
    -- Eliminado tipo de saldo
    DELETE FROM inv_saldopro
    WHERE inv_saldopro.codemp = wcodemp
      AND inv_saldopro.codsuc = wcodsuc
      AND inv_saldopro.codbod = wcodbod
      AND inv_saldopro.tipsld = wtipsld
      AND inv_saldopro.aniotr = waniotr
      AND inv_saldopro.mestra = wmestra
      AND inv_saldopro.cditem = wcditem;
 END IF

 -- Creando saldo por tipo de saldo
 SET LOCK MODE TO WAIT;
 INSERT INTO inv_saldopro
 SELECT 0,
        a.codemp,
        a.codsuc,
        a.codbod,
        b.valchr,
        a.aniotr,
        a.mestra,
        a.cditem,
        a.codabr,
        NVL(SUM(a.canuni),0),
        (NVL(SUM(a.totpro),0)/NVL(SUM(a.canuni),0)),
        NVL(SUM(a.totpro),0),
        USER,
        TODAY,
        CURRENT
  FROM  inv_dtransac a,glb_paramtrs b
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.aniotr = waniotr
    AND a.mestra = wmestra
    AND a.cditem = wcditem
    AND a.estado = "V"
    AND a.tipope = wtipope
    AND b.numpar = 4
    AND b.tippar = wtipope 
 GROUP BY 1,2,3,4,5,6,7,8,9; 

 -- Verificando si el tipo de saldo teorico existe
 LET xcodemp = NULL;
 SELECT UNIQUE a.codemp
  INTO  xcodemp
  FROM  inv_saldopro a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.tipsld = wsalteo
    AND a.aniotr = waniotr
    AND a.mestra = wmestra
    AND a.cditem = wcditem;

 -- Si ya existe el saldo teorico por producto lo borra
 IF xcodemp IS NOT NULL THEN
    -- Eliminado tipo de saldo 
    DELETE FROM inv_saldopro
    WHERE inv_saldopro.codemp = wcodemp
      AND inv_saldopro.codsuc = wcodsuc
      AND inv_saldopro.codbod = wcodbod
      AND inv_saldopro.tipsld = wsalteo
      AND inv_saldopro.aniotr = waniotr
      AND inv_saldopro.mestra = wmestra
      AND inv_saldopro.cditem = wcditem;
 END IF

 -- Creando saldo teorico por producto
 CALL fxsaldomovimientos(wcodemp,wcodsuc,wcodbod,waniotr,wmestra,wcditem)
 RETURNING xcanuni,xprepro,xtotpro;

 -- Creando saldo teorico del producto 
 SET LOCK MODE TO WAIT;
 INSERT INTO inv_saldopro
 VALUES (0,
         wcodemp,
         wcodsuc,
         wcodbod,
         wsalteo,
         waniotr,
         wmestra,
         wcditem,
         wcodabr,
         xcanuni,
         xprepro,
         xtotpro,
         USER,
         CURRENT,
         CURRENT HOUR TO SECOND);
END PROCEDURE;
