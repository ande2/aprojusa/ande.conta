
  DROP FUNCTION  fxsaldomovimientos;

CREATE FUNCTION  fxsaldomovimientos(wcodemp SMALLINT,wcodsuc SMALLINT,wcodbod SMALLINT,
                                    waniotr SMALLINT,wmestra SMALLINT,wcditem VARCHAR(20))
 RETURNING DEC(16,2),DEC(16,6),DEC(16,2)

 DEFINE wtipsld  LIKE inv_saldopro.tipsld;
 DEFINE spcanuni LIKE inv_saldopro.canuni;
 DEFINE spprepro LIKE inv_saldopro.prepro;
 DEFINE sptotpro LIKE inv_saldopro.totpro;
 DEFINE spopeuni LIKE inv_saldopro.canuni;
 DEFINE spopeval LIKE inv_saldopro.totpro;
 DEFINE xcanuni  LIKE inv_saldopro.canuni;
 DEFINE xprepro  LIKE inv_saldopro.prepro;
 DEFINE xtotpro  LIKE inv_saldopro.totpro;
 DEFINE aniant   SMALLINT;
 DEFINE mesant   SMALLINT;
 DEFINE conteo   INTEGER;

 -- Funcion para calcular el saldo de un producto por sus movimientos
 -- Los saldos son por empresa, sucursal, bodega, tipo de saldo, anio, mes y producto
 -- Mynor Ramirez
 -- Diciembre 2010

 -- Definiendo nivel de aislamiento
 SET ISOLATION COMMITTED READ;

 -- Obteniendo tipo de saldo SALDO INICIAL
 SELECT NVL(a.valchr,"0")
  INTO  wtipsld
  FROM  glb_paramtrs a
  WHERE a.tippar = 2;

 -- Verificando si ya existe cargando el saldo inicial
 LET spcanuni = 0;
 LET sptotpro = 0;
 LET conteo   = 0;

 SELECT NVL(SUM(a.canuni),0),
        NVL(SUM(a.totpro),0)
  INTO  spcanuni,
        sptotpro
  FROM  inv_saldopro a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.tipsld = wtipsld
    AND a.aniotr = waniotr
    AND a.mestra = wmestra
    AND a.cditem = wcditem;

 -- Verificando si existe saldo inicial, de lo contrario obtiene fisico del mes pasado
 IF (spcanuni+sptotpro=0) THEN
    -- Si no existe obtiene fisico del mes pasado
    IF (wmestra=1) THEN
       LET aniant = (waniotr-1);
       LET mesant = 12;
    ELSE
       LET aniant = waniotr;
       LET mesant = (wmestra-1);
    END IF;

    -- Obteniendo fisico del mes pasado
    CALL fxsaldofisico(wcodemp,wcodsuc,wcodbod,aniant,mesant,wcditem)
    RETURNING spcanuni;

    LET sptotpro = 0;
 END IF

 -- Calculando saldo de movimientos
 SELECT NVL(SUM(a.opeuni),0),
        NVL(SUM(a.opeval),0)
  INTO  spopeuni,
        spopeval
  FROM  inv_dtransac a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.aniotr = waniotr
    AND a.mestra = wmestra
    AND a.cditem = wcditem
    AND a.estado = "V";

 -- Calculando saldo
 LET xcanuni = (spcanuni+spopeuni);
 LET xtotpro = (sptotpro+spopeval);

 -- Calculando precio promedio
 LET xprepro = 0;
 IF (xcanuni>0) THEN
    LET xprepro = (xtotpro/xcanuni);
 END IF

 -- Verificando nulos
 IF xcanuni IS NULL THEN LET xcanuni = 0; END IF;
 IF xprepro IS NULL THEN LET xprepro = 0; END IF;
 IF xtotpro IS NULL THEN LET xtotpro = 0; END IF;

 RETURN xcanuni,xprepro,xtotpro;
END FUNCTION;
