DROP FUNCTION fxsaldofisico;
CREATE FUNCTION  fxsaldofisico(wcodemp SMALLINT,wcodsuc SMALLINT,wcodbod SMALLINT,waniotr SMALLINT,
                               wmestra SMALLINT,wcditem VARCHAR(20))
 RETURNING DEC(12,2)

 DEFINE spcanuni LIKE inv_saldopro.canuni;

 -- Funcion para obtener el saldo de inventario fisico de un producto
 -- Mynor Ramirez
 -- Diciembre 2010

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;
 LET  spcanuni = 0; 

 -- Obteniendo inventario fisico del producto
 IF spcanuni IS NULL THEN LET spcanuni = 0; END IF;

 RETURN spcanuni;
END FUNCTION;
