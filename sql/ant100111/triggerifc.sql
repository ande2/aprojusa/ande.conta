







drop trigger trgupdestadomovto;
create trigger trgupdestadomovto update of estado
    on inv_mtransac referencing new as pos
    for each row
        (
        update inv_dtransac set inv_dtransac.estado = pos.estado  where (inv_dtransac.lnktra = pos.lnktra )
         );

drop trigger trginsertarproducto;
create trigger trginsertarproducto insert on inv_dtransac referencing new as pos
    for each row
        (
        execute procedure sptsaldosxproducto(pos.codemp,pos.codsuc,pos.codbod,pos.tipmov,pos.aniotr,pos.mestra,
                                             pos.cditem,pos.tipope,pos.codabr)
        );

drop trigger trgupdestadoproducto;
create trigger trgupdestadoproducto update of estado on inv_dtransac referencing new as pos
    for each row
        (
        execute procedure sptsaldosxproducto(pos.codemp,pos.codsuc,pos.codbod,pos.tipmov,pos.aniotr,pos.mestra,
                                             pos.cditem,pos.tipope,pos.codabr)
        );

drop trigger trgupdcanuniproducto;
create trigger trgupdcanuniproducto update of canuni on inv_dtransac referencing new as pos
    for each row
        (
        execute procedure sptsaldosxproducto(pos.codemp,pos.codsuc,pos.codbod,pos.tipmov,pos.aniotr,pos.mestra,
                                             pos.cditem,pos.tipope,pos.codabr)
        );

drop trigger trgcupdpreuniproducto;
create trigger trgcupdpreuniproducto update of preuni on inv_dtransac referencing new as pos
    for each row
        (
        execute procedure sptsaldosxproducto(pos.codemp,pos.codsuc,pos.codbod,pos.tipmov,pos.aniotr,pos.mestra,
                                             pos.cditem,pos.tipope,pos.codabr)
        );
