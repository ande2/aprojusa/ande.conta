create trigger trgaanularmovoto update of estado 
    on inv_mtransac referencing new as pos
    for each row
        (
        update inv_dtransac set inc_drtansac.estado = pos.estado  where (inv_dtransac.lnktra = pos.lnktra ) );

create trigger trginsertarproducto insert on inv_dtransac referencing new as pos
    for each row
        (
        execute procedure sptsaldosxproducto(pos.codemp 
    ,pos.unineg ,pos.tipmov ,pos.cditem ,pos.aniotr ,pos.mestra ,pos.tipope 
    ));

create trigger "sistemas".tgactestadoanulado update of estado 
    on "sistemas".inc_dtransac referencing new as pos
    for each row
        (
        execute procedure "sistemas".spsaldosxproducto(pos.codemp 
    ,pos.unineg ,pos.tipmov ,pos.cditem ,pos.aniotr ,pos.mestra ,pos.tipope 
    ));

create trigger "sistemas".tgctualizacanlbs update of canlbs on 
    "sistemas".inc_dtransac referencing new as pos
    for each row
        (
        execute procedure "sistemas".spsaldosxproducto(pos.codemp 
    ,pos.unineg ,pos.tipmov ,pos.cditem ,pos.aniotr ,pos.mestra ,pos.tipope 
    ));

create trigger "sistemas".tgctualizacanuni update of canuni on 
    "sistemas".inc_dtransac referencing new as pos
    for each row
        (
        execute procedure "sistemas".spsaldosxproducto(pos.codemp 
    ,pos.unineg ,pos.tipmov ,pos.cditem ,pos.aniotr ,pos.mestra ,pos.tipope 
    ));

create trigger "sistemas".tgctualizapreciouni update of preuni 
    on "sistemas".inc_dtransac referencing new as pos
    for each row
        (
        execute procedure "sistemas".spsaldosxproducto(pos.codemp 
    ,pos.unineg ,pos.tipmov ,pos.cditem ,pos.aniotr ,pos.mestra ,pos.tipope 
    ));

create trigger "sistemas".tgcosteoproductos insert on "sistemas"
    .inc_bitprocs referencing new as pos
    for each row
        when ((pos.tippro = 1 ) )
            (
            execute procedure "sistemas".spcosteoproductos(pos.codemp 
    ,pos.unineg ,pos.aniotr ,pos.mestra )),
        when ((pos.tippro = 2 ) )
            (
            execute procedure "sistemas".spabsorciones(pos.codemp 
    ,pos.unineg ,pos.aniotr ,pos.mestra )),
        when ((pos.tippro = 3 ) )
            (
            execute procedure "sistemas".spamanecerxregion(pos.aniotr 
    ,pos.mestra ));

create trigger "sistemas".trgtrasladostranx insert on "sistemas"
    .inc_trasuneg referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptrasladosalmacen(pos.lnkori 
    ));

create trigger "sistemas".trganulatraslados update of estado 
    on "sistemas".inc_trasuneg referencing new as pos
    for each row
        when ((pos.estado = 'A' ) )
            (
            execute procedure "sistemas".spanlautoalmacen(pos.lnkori 
    ));

create trigger "sistemas".tgrregistraprocesos insert on "sistemas"
    .inp_bitprocs referencing new as a
    for each row
        when ((a.tippro = 1 ) )
            (
            execute procedure "sistemas".spamanecerxregion(a.anosem 
    ,a.numsem ));



update statistics high for table inc_dtransac (
     codemp, codpro, estado, tipope, unineg) 
     resolution   0.50000 ;
update statistics high for table inc_maesaldo (
     aniotr, cditem, codemp, codval, mestra, 
     unineg) 
     resolution   0.50000 ;
update statistics high for table inc_mtransac (
     fecemi, lnktra) 
     resolution   0.50000 ;
update statistics high for table inc_prodcats (
     cditem, codcat, subcat) 
     resolution   0.50000 ;

 


