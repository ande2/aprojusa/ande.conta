{ DATABASE invpdi  delimiter | }

grant dba to "sistemas";
grant dba to "root";
grant dba to "public";
grant connect to "rtabora";









create function "informix".checksum(p1 lvarchar, p2 integer)
	RETURNS integer
	with (NOT VARIANT, HANDLESNULLS, PARALLELIZABLE)
	EXTERNAL NAME "$INFORMIXDIR/extend/checksum/checksum.so(lvarchar_checksum)"
	LANGUAGE C;

create function "informix".checksum(p1 integer, p2 integer)
    RETURNS integer
    with (NOT VARIANT, HANDLESNULLS, PARALLELIZABLE)
    EXTERNAL NAME "$INFORMIXDIR/extend/checksum/checksum.so(integer_checksum)"
    LANGUAGE C;

create function "informix".checksum(p1 smallint, p2 integer)
    RETURNS integer
    with (NOT VARIANT, HANDLESNULLS, PARALLELIZABLE)
    EXTERNAL NAME "$INFORMIXDIR/extend/checksum/checksum.so(short_checksum)"
    LANGUAGE C;

create function "informix".checksum(p1 references byte, p2 integer)
    RETURNS integer
    with (NOT VARIANT, HANDLESNULLS, PARALLELIZABLE)
    EXTERNAL NAME "$INFORMIXDIR/extend/checksum/checksum.so(classicBlob_checksum)"
    LANGUAGE C;

create function "informix".checksum(p1 references text, p2 integer)
    RETURNS integer
    with (NOT VARIANT, HANDLESNULLS, PARALLELIZABLE)
    EXTERNAL NAME "$INFORMIXDIR/extend/checksum/checksum.so(classicBlob_checksum)"
    LANGUAGE C;

create function "informix".checksum(p1 int8, p2 integer)
    RETURNS integer
    with (NOT VARIANT, HANDLESNULLS, PARALLELIZABLE)
    EXTERNAL NAME "$INFORMIXDIR/extend/checksum/checksum.so(int8_checksum)"
    LANGUAGE C;

create function "informix".checksum(p1 decimal, p2 integer)
    RETURNS integer
    with (NOT VARIANT, HANDLESNULLS, PARALLELIZABLE)
    EXTERNAL NAME "$INFORMIXDIR/extend/checksum/checksum.so(decimal_checksum)"
    LANGUAGE C;

create function "informix".checksum(p1 money, p2 integer)
    RETURNS integer
    with (NOT VARIANT, HANDLESNULLS, PARALLELIZABLE)
    EXTERNAL NAME "$INFORMIXDIR/extend/checksum/checksum.so(money_checksum)"
    LANGUAGE C;

create function "informix".checksum(p1 float, p2 integer)
    RETURNS integer
    with (NOT VARIANT, HANDLESNULLS, PARALLELIZABLE)
    EXTERNAL NAME "$INFORMIXDIR/extend/checksum/checksum.so(double_checksum)"
    LANGUAGE C;

create function "informix".checksum(p1 real, p2 integer)
    RETURNS integer
    with (NOT VARIANT, HANDLESNULLS, PARALLELIZABLE)
    EXTERNAL NAME "$INFORMIXDIR/extend/checksum/checksum.so(real_checksum)"
    LANGUAGE C;

create function "informix".checksum(p1 date, p2 integer)
    RETURNS integer
    with (NOT VARIANT, HANDLESNULLS, PARALLELIZABLE)
    EXTERNAL NAME "$INFORMIXDIR/extend/checksum/checksum.so(date_checksum)"
    LANGUAGE C;

create function "informix".checksum(p1 datetime year to fraction, p2 integer)
    RETURNS integer
    with (NOT VARIANT, HANDLESNULLS, PARALLELIZABLE)
    EXTERNAL NAME "$INFORMIXDIR/extend/checksum/checksum.so(datetime_checksum)"
    LANGUAGE C;

create function "informix".checksum(p1 interval year to month, p2 integer)
    RETURNS integer
    with (NOT VARIANT, HANDLESNULLS, PARALLELIZABLE)
    EXTERNAL NAME "$INFORMIXDIR/extend/checksum/checksum.so(interval_checksum)"
    LANGUAGE C;

create function "informix".checksum(p1 set, p2 integer)
    RETURNS integer
    with (NOT VARIANT, HANDLESNULLS, PARALLELIZABLE)
    EXTERNAL NAME "$INFORMIXDIR/extend/checksum/checksum.so(collection_checksum)"
    LANGUAGE C;

create function "informix".checksum(p1 multiset, p2 integer)
    RETURNS integer
    with (NOT VARIANT, HANDLESNULLS, PARALLELIZABLE)
    EXTERNAL NAME "$INFORMIXDIR/extend/checksum/checksum.so(collection_checksum)"
    LANGUAGE C;

create function "informix".checksum(p1 list, p2 integer)
    RETURNS integer
    with (NOT VARIANT, HANDLESNULLS, PARALLELIZABLE)
    EXTERNAL NAME "$INFORMIXDIR/extend/checksum/checksum.so(collection_checksum)"
    LANGUAGE C;

create function "informix".checksum(p1 row, p2 integer)
    RETURNS integer
    with (NOT VARIANT, HANDLESNULLS, PARALLELIZABLE)
    EXTERNAL NAME "$INFORMIXDIR/extend/checksum/checksum.so(row_checksum)"
    LANGUAGE C;


 


 

{ TABLE "sistemas".ind_tipomovs row size = 92 number of columns = 11 index size = 
              9 }
{ unload file name = ind_t00105.unl number of rows = 26 }

create table "sistemas".ind_tipomovs 
  (
    codemp smallint not null ,
    tipmov smallint not null ,
    nommov char(30) not null ,
    abrnom char(5) not null ,
    nomdet char(30),
    tipope smallint not null ,
    movcls char(1),
    subcls char(2),
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (tipope IN (1 ,0 )) constraint "sistemas".ckindtipomovs1,
    primary key (codemp,tipmov)  constraint "sistemas".pkindtipomovs
  )  extent size 16 next size 16 lock mode row;
revoke all on "sistemas".ind_tipomovs from "public";

{ TABLE "sistemas".ind_parmxemp row size = 46 number of columns = 15 index size = 
              25 }
{ unload file name = ind_p00106.unl number of rows = 3 }

create table "sistemas".ind_parmxemp 
  (
    unineg smallint not null ,
    codemp smallint not null ,
    codbod smallint not null ,
    tipmov smallint not null ,
    coddes smallint,
    maxpes decimal(8,2) not null ,
    maxdvc smallint not null ,
    maxdrp smallint,
    numsec smallint not null ,
    tipdoc char(1) not null ,
    nserie smallint not null ,
    porsob decimal(5,2) not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (maxdvc >= 0 ) constraint "sistemas".ckindparmxemp2,
    primary key (unineg,codemp)  constraint "sistemas".pkindparmxemp
  )  extent size 16 next size 16 lock mode row;
revoke all on "sistemas".ind_parmxemp from "public";

{ TABLE "sistemas".ind_unegocio row size = 90 number of columns = 9 index size = 
              7 }
{ unload file name = ind_u00108.unl number of rows = 54 }

create table "sistemas".ind_unegocio 
  (
    unineg smallint not null ,
    nomneg char(50) not null ,
    abrneg char(15) not null ,
    tipneg smallint not null ,
    region smallint not null ,
    trasld char(1) not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (unineg)  constraint "sistemas".pkindunegocio
  )  extent size 16 next size 16 lock mode row;
revoke all on "sistemas".ind_unegocio from "public";

{ TABLE "sistemas".ind_ncorstra row size = 4 number of columns = 1 index size = 9 
              }
{ unload file name = ind_n00110.unl number of rows = 0 }

create table "sistemas".ind_ncorstra 
  (
    numcor serial not null ,
    primary key (numcor)  constraint "sistemas".pkindncorstra
  )  extent size 16 next size 16 lock mode row;
revoke all on "sistemas".ind_ncorstra from "public";

{ TABLE "sistemas".ind_cntratis row size = 72 number of columns = 6 index size = 
              9 }
{ unload file name = ind_c00112.unl number of rows = 19 }

create table "sistemas".ind_cntratis 
  (
    unineg smallint not null ,
    numcon smallint not null ,
    nomcon char(50) not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (unineg,numcon)  constraint "sistemas".pkindcntratis
  )  extent size 16 next size 16 lock mode row;
revoke all on "sistemas".ind_cntratis from "public";

{ TABLE "sistemas".inc_pxunirem row size = 82 number of columns = 11 index size = 
              11 }
{ unload file name = inc_p00113.unl number of rows = 22 }

create table "sistemas".inc_pxunirem 
  (
    unineg smallint not null ,
    tipsrv smallint not null ,
    codori smallint not null ,
    unirem smallint not null ,
    codemp smallint not null ,
    tipmov smallint not null ,
    coddes smallint not null ,
    basrem char(50) not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (unineg,tipsrv,codori)  constraint "sistemas".pkincpxunirem
  )  extent size 16 next size 16 lock mode row;
revoke all on "sistemas".inc_pxunirem from "public";

{ TABLE "sistemas".inc_mtransac row size = 313 number of columns = 37 index size 
              = 78 }
{ unload file name = inc_m00116.unl number of rows = 2246 }

create table "sistemas".inc_mtransac 
  (
    lnktra serial not null ,
    unineg smallint not null ,
    codemp smallint not null ,
    tipmov smallint not null ,
    codori smallint not null ,
    coddes smallint,
    numdoc integer not null ,
    aniotr smallint not null ,
    mestra smallint not null ,
    fecemi date not null ,
    numcon smallint,
    nplaca char(12),
    tipope smallint not null ,
    march1 integer,
    trasld integer,
    lnktrl integer,
    horsal datetime hour to second,
    hentmk datetime hour to second,
    haptmk datetime hour to second,
    feclle date,
    horlle datetime hour to second,
    tmpsal decimal(5,2),
    tmplle decimal(5,2),
    estado char(1) not null ,
    motanl char(80),
    fecanl date,
    horanl datetime hour to second,
    usranl char(10),
    impres char(1) not null ,
    numrem integer,
    fecdsc date,
    hordsc datetime hour to second,
    observ char(100),
    mermas char(1),
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (unineg,codemp,tipmov,coddes,numdoc)  constraint "sistemas".ukincmtransac,
    
    check (numdoc > 0 ) constraint "sistemas".ckincmtransac2,
    
    check (tipope IN (1 ,0 )) constraint "sistemas".ckincmtransac3,
    
    check (fecemi > DATE ('01/01/2003' ) ) constraint "sistemas".ckincmtransac1,
    primary key (lnktra)  constraint "sistemas".pkincmtransac
  )  extent size 16 next size 16 lock mode row;
revoke all on "sistemas".inc_mtransac from "public";

{ TABLE "sistemas".inc_usuaxmov row size = 32 number of columns = 6 index size = 
              0 }
{ unload file name = inc_u00117.unl number of rows = 547 }

create table "sistemas".inc_usuaxmov 
  (
    unineg smallint not null ,
    userid char(10) not null ,
    tipmov smallint not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".inc_usuaxmov from "public";

{ TABLE "sistemas".inc_invcierr row size = 38 number of columns = 10 index size = 
              9 }
{ unload file name = inc_i00118.unl number of rows = 16 }

create table "sistemas".inc_invcierr 
  (
    lnkcrr serial not null ,
    codemp smallint not null ,
    unineg smallint not null ,
    fecini date not null ,
    fecfin date not null ,
    annioc smallint not null ,
    mescie smallint not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".inc_invcierr from "public";

{ TABLE "sistemas".inc_prodcats row size = 43 number of columns = 7 index size = 
              54 }
{ unload file name = inc_p00119.unl number of rows = 1224 }

create table "sistemas".inc_prodcats 
  (
    codcat smallint not null ,
    subcat smallint,
    nacimp char(1),
    cditem char(20) not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (cditem)  constraint "sistemas".uqincprodcats
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".inc_prodcats from "public";

{ TABLE "sistemas".inc_subcateg row size = 72 number of columns = 6 index size = 
              9 }
{ unload file name = inc_s00122.unl number of rows = 62 }

create table "sistemas".inc_subcateg 
  (
    codcat smallint not null ,
    subcat smallint not null ,
    subdes char(50) not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codcat,subcat)  constraint "sistemas".pkincsubcateg
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".inc_subcateg from "public";

{ TABLE "sistemas".inc_tipcateg row size = 70 number of columns = 5 index size = 
              7 }
{ unload file name = inc_t00123.unl number of rows = 13 }

create table "sistemas".inc_tipcateg 
  (
    codcat smallint not null ,
    nomcat char(50) not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codcat)  constraint "sistemas".pkinctipcateg
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".inc_tipcateg from "public";

{ TABLE "sistemas".inc_invcierd row size = 173 number of columns = 26 index size 
              = 38 }
{ unload file name = inc_i00125.unl number of rows = 1090 }

create table "sistemas".inc_invcierd 
  (
    lnkcrr integer not null ,
    codemp smallint not null ,
    unineg smallint not null ,
    annioc smallint not null ,
    mescie smallint not null ,
    cditem char(20) not null ,
    iniuni integer not null ,
    inilbs decimal(16,2) not null ,
    entuni integer not null ,
    entlbs decimal(16,2) not null ,
    saluni integer not null ,
    sallbs decimal(16,2) not null ,
    venuni integer not null ,
    venlbs decimal(16,2) not null ,
    mersag decimal(16,2) not null ,
    mernor decimal(16,2) not null ,
    merven decimal(16,2) not null ,
    mercam decimal(16,2) not null ,
    merdis decimal(16,2) not null ,
    teouni integer not null ,
    teolbs decimal(16,2) not null ,
    fisuni integer not null ,
    fislbs decimal(16,2) not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (lnkcrr,cditem)  constraint "sistemas".pkincinvcierd
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".inc_invcierd from "public";

{ TABLE "sistemas".inc_setprods row size = 47 number of columns = 7 index size = 
              27 }
{ unload file name = inc_s00126.unl number of rows = 2 }

create table "sistemas".inc_setprods 
  (
    codemp smallint not null ,
    cditem char(20) not null ,
    pesuni decimal(10,4) not null ,
    meruni char(1) not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codemp,cditem)  constraint "sistemas".pkincsetprods
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".inc_setprods from "public";

{ TABLE "sistemas".inc_permxung row size = 32 number of columns = 6 index size = 
              0 }
{ unload file name = inc_p00127.unl number of rows = 58 }

create table "sistemas".inc_permxung 
  (
    userid char(10) not null ,
    unineg smallint not null ,
    codemp smallint not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".inc_permxung from "public";

{ TABLE "sistemas".ind_factpeso row size = 6 number of columns = 2 index size = 0 
              }
{ unload file name = ind_f00128.unl number of rows = 1 }

create table "sistemas".ind_factpeso 
  (
    pescnl decimal(4,2) not null ,
    pesfac decimal(4,2) not null 
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".ind_factpeso from "public";

{ TABLE "sistemas".ind_fproduni row size = 33 number of columns = 6 index size = 
              0 }
{ unload file name = ind_f00129.unl number of rows = 350 }

create table "sistemas".ind_fproduni 
  (
    cditem char(20) not null ,
    factor decimal(8,4) not null ,
    unilbs char(1) not null ,
    prcent decimal(8,4) not null ,
    mprcen char(1),
    mrsuma char(1)
  )  extent size 16 next size 16 lock mode row;
revoke all on "sistemas".ind_fproduni from "public";

{ TABLE "sistemas".inc_histcrrs row size = 36 number of columns = 9 index size = 
              9 }
{ unload file name = inc_h00130.unl number of rows = 9 }

create table "sistemas".inc_histcrrs 
  (
    lnkhst serial not null ,
    codemp smallint not null ,
    unineg smallint not null ,
    annioc integer not null ,
    mescie smallint not null ,
    lnkcrr integer not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  )  extent size 16 next size 16 lock mode row;
revoke all on "sistemas".inc_histcrrs from "public";

{ TABLE "sistemas".inc_dtransac row size = 177 number of columns = 36 index size 
              = 156 }
{ unload file name = inc_d00131.unl number of rows = 31042 }

create table "sistemas".inc_dtransac 
  (
    lnktra integer not null ,
    unineg smallint not null ,
    codemp smallint not null ,
    tipmov smallint,
    codori smallint not null ,
    coddes smallint,
    codbod smallint not null ,
    cditem char(20) not null ,
    numdoc integer not null ,
    aniotr smallint,
    mestra smallint,
    fecpro date,
    bodpro smallint not null ,
    bodori char(2) not null ,
    codpro smallint not null ,
    codest char(2) not null ,
    codesp char(3) not null ,
    tippes char(1) not null ,
    pstara decimal(10,2) not null ,
    pbruto decimal(10,2) not null ,
    canuni integer not null ,
    canlbs decimal(10,2) not null ,
    preuni decimal(12,6),
    totpro decimal(12,2),
    pespro decimal(7,2) not null ,
    fecven date,
    numcor integer not null ,
    barcod char(30) not null ,
    tipope smallint not null ,
    estado char(1),
    opeuni integer,
    opelbs decimal(10,2),
    opeval decimal(12,2),
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (pespro >= 0. ) constraint "sistemas".ckincdtransac4,
    
    check (numcor > 0 ) constraint "sistemas".ckincdtransac5,
    
    check (canlbs >= 0. ) constraint "sistemas".ckincdtransac3,
    
    check (canuni >= 0 ) constraint "sistemas".ckincdtransac2,
    
    check (tipope IN (1 ,0 )) constraint "sistemas".ckincdtransac6,
    primary key (lnktra,numcor)  constraint "sistemas".pkincdtransac
  )  extent size 16 next size 16 lock mode row;
revoke all on "sistemas".inc_dtransac from "public";

{ TABLE "sistemas".inp_products row size = 214 number of columns = 31 index size 
              = 27 }
{ unload file name = inp_p00132.unl number of rows = 509 }

create table "sistemas".inp_products 
  (
    codemp smallint not null ,
    cditem char(20) not null ,
    codant char(20),
    codcat smallint not null ,
    codfam smallint,
    dsitem char(50) not null ,
    desant char(20),
    codcar smallint,
    tenpro char(1),
    codcor smallint,
    codmar smallint,
    codcli smallint,
    numcli char(4),
    rangos char(20),
    codpaq smallint,
    codpie smallint,
    diaven smallint not null ,
    uniemp smallint not null ,
    codori char(2) not null ,
    codpro char(10),
    codest char(2) not null ,
    codesp char(3) not null ,
    numpro smallint not null ,
    subpro smallint,
    uniepq char(1),
    codepq char(15),
    tippro char(1),
    estado char(1) not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codemp,cditem)  constraint "sistemas".pkinpproducts
  )  extent size 16 next size 16 lock mode row;
revoke all on "sistemas".inp_products from "public";

{ TABLE "sistemas".inc_maesaldo row size = 84 number of columns = 13 index size = 
              42 }
{ unload file name = inc_m00134.unl number of rows = 11099 }

create table "sistemas".inc_maesaldo 
  (
    codemp smallint not null ,
    unineg smallint not null ,
    cditem char(20) not null ,
    codval smallint not null ,
    canuni decimal(16,2) not null ,
    canlbs decimal(16,2) not null ,
    preuni decimal(16,6),
    totpro decimal(16,2),
    aniotr smallint,
    mestra smallint,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  )  extent size 16 next size 16 lock mode row;
revoke all on "sistemas".inc_maesaldo from "public";

{ TABLE "sistemas".inc_mvalores row size = 48 number of columns = 7 index size = 
              9 }
{ unload file name = inc_m00135.unl number of rows = 14 }

create table "sistemas".inc_mvalores 
  (
    codemp smallint not null ,
    codval smallint not null ,
    desval char(25) not null ,
    movims char(1) not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (codval > 0 ) constraint "sistemas".ckincmvalores1,
    primary key (codemp,codval)  constraint "sistemas".pkincmvalores
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".inc_mvalores from "public";

{ TABLE "sistemas".inc_pesoxuni row size = 48 number of columns = 7 index size = 
              27 }
{ unload file name = inc_p00136.unl number of rows = 327 }

create table "sistemas".inc_pesoxuni 
  (
    codemp smallint not null ,
    cditem char(20) not null ,
    unimed smallint not null ,
    factor decimal(9,4) not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codemp,cditem)  constraint "sistemas".pkincpesoxuni
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".inc_pesoxuni from "public";

{ TABLE "sistemas".inc_unegmovs row size = 24 number of columns = 6 index size = 
              18 }
{ unload file name = inc_u00137.unl number of rows = 2 }

create table "sistemas".inc_unegmovs 
  (
    codemp smallint not null ,
    unineg smallint not null ,
    tipmov smallint not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (tipmov)  constraint "sistemas".uqincunegmovs,
    primary key (codemp,unineg,tipmov)  constraint "sistemas".pkincunegmovs
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".inc_unegmovs from "public";

{ TABLE "sistemas".inc_dvalores row size = 24 number of columns = 6 index size = 
              11 }
{ unload file name = inc_d00138.unl number of rows = 26 }

create table "sistemas".inc_dvalores 
  (
    codemp smallint not null ,
    codval smallint not null ,
    tipmov smallint not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (tipmov > 0 ) constraint "sistemas".ckincdvalores2,
    
    check (codval > 0 ) constraint "sistemas".ckincdvalores1,
    primary key (codemp,codval,tipmov)  constraint "sistemas".pkindmvalores
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".inc_dvalores from "public";

{ TABLE "sistemas".inc_numtrans row size = 4 number of columns = 1 index size = 9 
              }
{ unload file name = inc_n00157.unl number of rows = 1973 }

create table "sistemas".inc_numtrans 
  (
    numdoc serial not null 
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".inc_numtrans from "public";

{ TABLE "sistemas".rpt_invcompollo row size = 381 number of columns = 33 index size 
              = 0 }
{ unload file name = rpt_i00160.unl number of rows = 8481 }

create table "sistemas".rpt_invcompollo 
  (
    lnkrep integer,
    lnkbod integer,
    nombod char(30),
    codemp smallint,
    nomemp char(50),
    fecini date,
    fecfin date,
    codcat integer,
    nomcat char(30),
    subcat integer,
    subdes char(30),
    coitem char(20),
    dsitem char(50),
    nommed char(30),
    sinuni integer,
    entuni integer,
    saluni integer,
    safuni integer,
    sinlbs decimal(12,2),
    entlbs decimal(12,2),
    sallbs decimal(12,2),
    saflbs decimal(12,2),
    sinval decimal(12,2),
    entval decimal(12,2),
    salval decimal(12,2),
    safval decimal(12,2),
    cospro decimal(12,6),
    fisuni integer,
    fislbs decimal(12,2),
    fisval decimal(12,2),
    difuni integer,
    diflbs decimal(12,2),
    difval decimal(12,2)
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".rpt_invcompollo from "public";

{ TABLE "sistemas".inc_corelrpt row size = 4 number of columns = 1 index size = 9 
              }
{ unload file name = inc_c00161.unl number of rows = 756 }

create table "sistemas".inc_corelrpt 
  (
    numcor serial not null 
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".inc_corelrpt from "public";

{ TABLE "sistemas".inc_pagranel row size = 30 number of columns = 1 index size = 
              0 }
{ unload file name = inc_p00173.unl number of rows = 104 }

create table "sistemas".inc_pagranel 
  (
    cditem char(30) not null 
  )  extent size 16 next size 16 lock mode row;
revoke all on "sistemas".inc_pagranel from "public";

{ TABLE "sistemas".inc_paramtrs row size = 16 number of columns = 8 index size = 
              0 }
{ unload file name = inc_p00178.unl number of rows = 1 }

create table "sistemas".inc_paramtrs 
  (
    codcos smallint not null ,
    codteo smallint not null ,
    codini smallint,
    tipope smallint,
    tipmov smallint,
    absmas smallint,
    absmen smallint,
    codfis smallint
  )  extent size 16 next size 16 lock mode row;
revoke all on "sistemas".inc_paramtrs from "public";

{ TABLE "sistemas".rpt_movimxprodu row size = 505 number of columns = 26 index size 
              = 0 }
{ unload file name = rpt_m00179.unl number of rows = 32880 }

create table "sistemas".rpt_movimxprodu 
  (
    lnkrep integer not null ,
    lnkbod integer not null ,
    nombod char(50),
    codemp smallint,
    nomemp char(50),
    tipope char(50),
    fecini date,
    fecfin date,
    tipmov integer,
    nommov char(50),
    orides smallint,
    noride char(50),
    codcat integer,
    nomcat char(50),
    subcat smallint,
    subdes char(50),
    coitem integer,
    dsitem char(50),
    nommed char(30),
    numrem integer,
    numdoc integer,
    fecemi date,
    canuni integer,
    canlbs decimal(12,2),
    preuni decimal(16,2) not null ,
    totpro decimal(16,2) not null 
  )  extent size 16 next size 16 lock mode row;
revoke all on "sistemas".rpt_movimxprodu from "public";

{ TABLE "sistemas".inc_bitprocs row size = 34 number of columns = 9 index size = 
              9 }
{ unload file name = inc_b00204.unl number of rows = 99 }

create table "sistemas".inc_bitprocs 
  (
    lnkbit serial not null ,
    codemp smallint not null ,
    unineg smallint not null ,
    aniotr integer not null ,
    mestra smallint not null ,
    tippro smallint not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (lnkbit)  constraint "sistemas".pkincbitprocs
  )  extent size 16 next size 16 lock mode row;
revoke all on "sistemas".inc_bitprocs from "public";

{ TABLE "sistemas".inc_trasuneg row size = 31 number of columns = 7 index size = 
              9 }
{ unload file name = inc_t00205.unl number of rows = 385 }

create table "sistemas".inc_trasuneg 
  (
    lnktra serial not null ,
    lnkori integer not null ,
    lnkdes integer not null ,
    estado char(1) not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (lnktra)  constraint "sistemas".pkinctrasuneg
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".inc_trasuneg from "public";

{ TABLE "sistemas".tmp_mtransac row size = 313 number of columns = 37 index size 
              = 26 }
{ unload file name = tmp_m00218.unl number of rows = 934 }

create table "sistemas".tmp_mtransac 
  (
    lnktra serial not null ,
    unineg smallint not null ,
    codemp smallint not null ,
    tipmov smallint not null ,
    codori smallint not null ,
    coddes smallint,
    numdoc integer not null ,
    aniotr smallint not null ,
    mestra smallint not null ,
    fecemi date not null ,
    numcon smallint,
    nplaca char(12),
    tipope smallint not null ,
    march1 integer,
    trasld integer,
    lnktrl integer,
    horsal datetime hour to second,
    hentmk datetime hour to second,
    haptmk datetime hour to second,
    feclle date,
    horlle datetime hour to second,
    tmpsal decimal(5,2),
    tmplle decimal(5,2),
    estado char(1) not null ,
    motanl char(80),
    fecanl date,
    horanl datetime hour to second,
    usranl char(10),
    impres char(1) not null ,
    numrem integer,
    fecdsc date,
    hordsc datetime hour to second,
    observ char(100),
    mermas char(1),
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (unineg,codemp,tipmov,coddes,numdoc) ,
    primary key (lnktra) 
  )  extent size 16 next size 16 lock mode row;
revoke all on "sistemas".tmp_mtransac from "public";

{ TABLE "sistemas".tmp_dtransac row size = 177 number of columns = 36 index size 
              = 13 }
{ unload file name = tmp_d00219.unl number of rows = 12815 }

create table "sistemas".tmp_dtransac 
  (
    lnktra integer not null ,
    unineg smallint not null ,
    codemp smallint not null ,
    tipmov smallint,
    codori smallint not null ,
    coddes smallint,
    codbod smallint not null ,
    cditem char(20) not null ,
    numdoc integer not null ,
    aniotr smallint,
    mestra smallint,
    fecpro date,
    bodpro smallint not null ,
    bodori char(2) not null ,
    codpro smallint not null ,
    codest char(2) not null ,
    codesp char(3) not null ,
    tippes char(1) not null ,
    pstara decimal(10,2) not null ,
    pbruto decimal(10,2) not null ,
    canuni integer not null ,
    canlbs decimal(10,2) not null ,
    preuni decimal(12,6),
    totpro decimal(12,2),
    pespro decimal(7,2) not null ,
    fecven date,
    numcor integer not null ,
    barcod char(30) not null ,
    tipope smallint not null ,
    estado char(1),
    opeuni integer,
    opelbs decimal(10,2),
    opeval decimal(12,2),
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (lnktra,numcor) 
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".tmp_dtransac from "public";

{ TABLE "sistemas".rpt_invamanecer row size = 438 number of columns = 36 index size 
              = 0 }
{ unload file name = rpt_i00220.unl number of rows = 6429 }

create table "sistemas".rpt_invamanecer 
  (
    lnkrep integer not null ,
    lnkbod integer not null ,
    nombod char(30),
    codemp smallint,
    nomemp char(50),
    fecini date,
    fecfin date,
    codcat integer,
    nomcat char(50),
    subcat smallint,
    subdes char(50),
    coitem char(20),
    dsitem char(50),
    nommed char(30),
    sinuni integer,
    sinlbs decimal(12,2),
    entuni integer,
    entlbs decimal(12,2),
    disuni integer,
    dislbs decimal(12,2),
    saluni integer,
    sallbs decimal(12,2),
    venuni integer,
    venlbs decimal(12,2),
    mersgt decimal(12,2),
    mernor decimal(12,2),
    merven decimal(12,2),
    mercam decimal(12,2),
    merdis decimal(12,2),
    totmer decimal(12,2),
    fisuni integer,
    teouni integer,
    difuni integer,
    fislbs decimal(16,2),
    teolbs decimal(12,2),
    diflbs decimal(16,2)
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".rpt_invamanecer from "public";

{ TABLE "sistemas".inc_unegocio row size = 92 number of columns = 9 index size = 
              9 }
{ unload file name = inc_u00228.unl number of rows = 50 }

create table "sistemas".inc_unegocio 
  (
    unineg serial not null ,
    nomneg char(50) not null ,
    abrneg char(15) not null ,
    tipneg smallint not null ,
    region smallint not null ,
    trasld char(1) not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (unineg)  constraint "sistemas".pkincunegocio
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".inc_unegocio from "public";

{ TABLE "sistemas".iniciales row size = 14 number of columns = 3 index size = 0 }
{ unload file name = inici00229.unl number of rows = 99 }

create table "sistemas".iniciales 
  (
    unineg smallint,
    cditem integer,
    totpro decimal(14,2)
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".iniciales from "public";

{ TABLE "sistemas".inc_saldxsem row size = 41 number of columns = 10 index size = 
              11 }
{ unload file name = inc_s00242.unl number of rows = 28944 }

create table "sistemas".inc_saldxsem 
  (
    unineg smallint not null ,
    anosem integer,
    numsem smallint not null ,
    region smallint,
    fecini date not null ,
    fecfin date not null ,
    codpro integer,
    canuni integer,
    canlbs decimal(14,2),
    canupl decimal(11,2)
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".inc_saldxsem from "public";

{ TABLE "sistemas".inc_coluparm row size = 56 number of columns = 7 index size = 
              0 }
{ unload file name = inc_c00244.unl number of rows = 8 }

create table "sistemas".inc_coluparm 
  (
    codreg serial not null ,
    nomcol char(30) not null ,
    colnum smallint,
    tipneg smallint,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".inc_coluparm from "public";

{ TABLE "sistemas".inc_regiones row size = 52 number of columns = 5 index size = 
              0 }
{ unload file name = inc_r00247.unl number of rows = 3 }

create table "sistemas".inc_regiones 
  (
    codreg serial not null ,
    nomreg char(30) not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".inc_regiones from "public";

{ TABLE "sistemas".inp_bitprocs row size = 28 number of columns = 7 index size = 
              0 }
{ unload file name = inp_b00250.unl number of rows = 4 }

create table "sistemas".inp_bitprocs 
  (
    lnkbit serial not null ,
    anosem smallint not null ,
    numsem smallint not null ,
    tippro smallint not null ,
    nlogin char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".inp_bitprocs from "public";

{ TABLE "sistemas".inc_proinvam row size = 43 number of columns = 11 index size = 
              0 }
{ unload file name = inc_p00251.unl number of rows = 200 }

create table "sistemas".inc_proinvam 
  (
    cditem integer,
    sumpro char(1),
    respro char(1),
    polcla decimal(5,2),
    polclb decimal(5,2),
    porcen decimal(5,2),
    aparec char(1),
    partes char(1),
    nlogin char(15),
    fecsis date,
    horsis datetime hour to second
  )  extent size 16 next size 16 lock mode row;
revoke all on "sistemas".inc_proinvam from "public";


grant select on "sistemas".ind_tipomovs to "public" as "sistemas";
grant update on "sistemas".ind_tipomovs to "public" as "sistemas";
grant insert on "sistemas".ind_tipomovs to "public" as "sistemas";
grant delete on "sistemas".ind_tipomovs to "public" as "sistemas";
grant index on "sistemas".ind_tipomovs to "public" as "sistemas";
grant select on "sistemas".ind_parmxemp to "public" as "sistemas";
grant update on "sistemas".ind_parmxemp to "public" as "sistemas";
grant insert on "sistemas".ind_parmxemp to "public" as "sistemas";
grant delete on "sistemas".ind_parmxemp to "public" as "sistemas";
grant index on "sistemas".ind_parmxemp to "public" as "sistemas";
grant select on "sistemas".ind_unegocio to "public" as "sistemas";
grant update on "sistemas".ind_unegocio to "public" as "sistemas";
grant insert on "sistemas".ind_unegocio to "public" as "sistemas";
grant delete on "sistemas".ind_unegocio to "public" as "sistemas";
grant index on "sistemas".ind_unegocio to "public" as "sistemas";
grant select on "sistemas".ind_ncorstra to "public" as "sistemas";
grant update on "sistemas".ind_ncorstra to "public" as "sistemas";
grant insert on "sistemas".ind_ncorstra to "public" as "sistemas";
grant delete on "sistemas".ind_ncorstra to "public" as "sistemas";
grant index on "sistemas".ind_ncorstra to "public" as "sistemas";
grant select on "sistemas".ind_cntratis to "public" as "sistemas";
grant update on "sistemas".ind_cntratis to "public" as "sistemas";
grant insert on "sistemas".ind_cntratis to "public" as "sistemas";
grant delete on "sistemas".ind_cntratis to "public" as "sistemas";
grant index on "sistemas".ind_cntratis to "public" as "sistemas";
grant select on "sistemas".inc_pxunirem to "public" as "sistemas";
grant update on "sistemas".inc_pxunirem to "public" as "sistemas";
grant insert on "sistemas".inc_pxunirem to "public" as "sistemas";
grant delete on "sistemas".inc_pxunirem to "public" as "sistemas";
grant index on "sistemas".inc_pxunirem to "public" as "sistemas";
grant select on "sistemas".inc_mtransac to "public" as "sistemas";
grant update on "sistemas".inc_mtransac to "public" as "sistemas";
grant insert on "sistemas".inc_mtransac to "public" as "sistemas";
grant delete on "sistemas".inc_mtransac to "public" as "sistemas";
grant index on "sistemas".inc_mtransac to "public" as "sistemas";
grant alter on "sistemas".inc_mtransac to "public" as "sistemas";
grant references on "sistemas".inc_mtransac to "public" as "sistemas";
grant select on "sistemas".inc_usuaxmov to "public" as "sistemas";
grant update on "sistemas".inc_usuaxmov to "public" as "sistemas";
grant insert on "sistemas".inc_usuaxmov to "public" as "sistemas";
grant delete on "sistemas".inc_usuaxmov to "public" as "sistemas";
grant index on "sistemas".inc_usuaxmov to "public" as "sistemas";
grant alter on "sistemas".inc_usuaxmov to "public" as "sistemas";
grant references on "sistemas".inc_usuaxmov to "public" as "sistemas";
grant select on "sistemas".inc_invcierr to "public" as "sistemas";
grant update on "sistemas".inc_invcierr to "public" as "sistemas";
grant insert on "sistemas".inc_invcierr to "public" as "sistemas";
grant delete on "sistemas".inc_invcierr to "public" as "sistemas";
grant index on "sistemas".inc_invcierr to "public" as "sistemas";
grant alter on "sistemas".inc_invcierr to "public" as "sistemas";
grant references on "sistemas".inc_invcierr to "public" as "sistemas";
grant select on "sistemas".inc_prodcats to "public" as "sistemas";
grant update on "sistemas".inc_prodcats to "public" as "sistemas";
grant insert on "sistemas".inc_prodcats to "public" as "sistemas";
grant delete on "sistemas".inc_prodcats to "public" as "sistemas";
grant index on "sistemas".inc_prodcats to "public" as "sistemas";
grant alter on "sistemas".inc_prodcats to "public" as "sistemas";
grant references on "sistemas".inc_prodcats to "public" as "sistemas";
grant select on "sistemas".inc_subcateg to "public" as "sistemas";
grant update on "sistemas".inc_subcateg to "public" as "sistemas";
grant insert on "sistemas".inc_subcateg to "public" as "sistemas";
grant delete on "sistemas".inc_subcateg to "public" as "sistemas";
grant index on "sistemas".inc_subcateg to "public" as "sistemas";
grant alter on "sistemas".inc_subcateg to "public" as "sistemas";
grant references on "sistemas".inc_subcateg to "public" as "sistemas";
grant select on "sistemas".inc_tipcateg to "public" as "sistemas";
grant update on "sistemas".inc_tipcateg to "public" as "sistemas";
grant insert on "sistemas".inc_tipcateg to "public" as "sistemas";
grant delete on "sistemas".inc_tipcateg to "public" as "sistemas";
grant index on "sistemas".inc_tipcateg to "public" as "sistemas";
grant alter on "sistemas".inc_tipcateg to "public" as "sistemas";
grant references on "sistemas".inc_tipcateg to "public" as "sistemas";
grant select on "sistemas".inc_invcierd to "public" as "sistemas";
grant update on "sistemas".inc_invcierd to "public" as "sistemas";
grant insert on "sistemas".inc_invcierd to "public" as "sistemas";
grant delete on "sistemas".inc_invcierd to "public" as "sistemas";
grant index on "sistemas".inc_invcierd to "public" as "sistemas";
grant alter on "sistemas".inc_invcierd to "public" as "sistemas";
grant references on "sistemas".inc_invcierd to "public" as "sistemas";
grant select on "sistemas".inc_setprods to "public" as "sistemas";
grant update on "sistemas".inc_setprods to "public" as "sistemas";
grant insert on "sistemas".inc_setprods to "public" as "sistemas";
grant delete on "sistemas".inc_setprods to "public" as "sistemas";
grant index on "sistemas".inc_setprods to "public" as "sistemas";
grant alter on "sistemas".inc_setprods to "public" as "sistemas";
grant references on "sistemas".inc_setprods to "public" as "sistemas";
grant select on "sistemas".inc_permxung to "public" as "sistemas";
grant update on "sistemas".inc_permxung to "public" as "sistemas";
grant insert on "sistemas".inc_permxung to "public" as "sistemas";
grant delete on "sistemas".inc_permxung to "public" as "sistemas";
grant index on "sistemas".inc_permxung to "public" as "sistemas";
grant select on "sistemas".ind_factpeso to "public" as "sistemas";
grant update on "sistemas".ind_factpeso to "public" as "sistemas";
grant insert on "sistemas".ind_factpeso to "public" as "sistemas";
grant delete on "sistemas".ind_factpeso to "public" as "sistemas";
grant index on "sistemas".ind_factpeso to "public" as "sistemas";
grant alter on "sistemas".ind_factpeso to "public" as "sistemas";
grant references on "sistemas".ind_factpeso to "public" as "sistemas";
grant select on "sistemas".ind_fproduni to "public" as "sistemas";
grant update on "sistemas".ind_fproduni to "public" as "sistemas";
grant insert on "sistemas".ind_fproduni to "public" as "sistemas";
grant delete on "sistemas".ind_fproduni to "public" as "sistemas";
grant index on "sistemas".ind_fproduni to "public" as "sistemas";
grant alter on "sistemas".ind_fproduni to "public" as "sistemas";
grant references on "sistemas".ind_fproduni to "public" as "sistemas";
grant select on "sistemas".inc_histcrrs to "public" as "sistemas";
grant update on "sistemas".inc_histcrrs to "public" as "sistemas";
grant insert on "sistemas".inc_histcrrs to "public" as "sistemas";
grant delete on "sistemas".inc_histcrrs to "public" as "sistemas";
grant index on "sistemas".inc_histcrrs to "public" as "sistemas";
grant alter on "sistemas".inc_histcrrs to "public" as "sistemas";
grant references on "sistemas".inc_histcrrs to "public" as "sistemas";
grant select on "sistemas".inc_dtransac to "public" as "sistemas";
grant update on "sistemas".inc_dtransac to "public" as "sistemas";
grant insert on "sistemas".inc_dtransac to "public" as "sistemas";
grant delete on "sistemas".inc_dtransac to "public" as "sistemas";
grant index on "sistemas".inc_dtransac to "public" as "sistemas";
grant alter on "sistemas".inc_dtransac to "public" as "sistemas";
grant references on "sistemas".inc_dtransac to "public" as "sistemas";
grant select on "sistemas".inp_products to "public" as "sistemas";
grant update on "sistemas".inp_products to "public" as "sistemas";
grant insert on "sistemas".inp_products to "public" as "sistemas";
grant delete on "sistemas".inp_products to "public" as "sistemas";
grant index on "sistemas".inp_products to "public" as "sistemas";
grant alter on "sistemas".inp_products to "public" as "sistemas";
grant references on "sistemas".inp_products to "public" as "sistemas";
grant select on "sistemas".inc_maesaldo to "public" as "sistemas";
grant update on "sistemas".inc_maesaldo to "public" as "sistemas";
grant insert on "sistemas".inc_maesaldo to "public" as "sistemas";
grant delete on "sistemas".inc_maesaldo to "public" as "sistemas";
grant index on "sistemas".inc_maesaldo to "public" as "sistemas";
grant select on "sistemas".inc_mvalores to "public" as "sistemas";
grant update on "sistemas".inc_mvalores to "public" as "sistemas";
grant insert on "sistemas".inc_mvalores to "public" as "sistemas";
grant delete on "sistemas".inc_mvalores to "public" as "sistemas";
grant index on "sistemas".inc_mvalores to "public" as "sistemas";
grant select on "sistemas".inc_pesoxuni to "public" as "sistemas";
grant update on "sistemas".inc_pesoxuni to "public" as "sistemas";
grant insert on "sistemas".inc_pesoxuni to "public" as "sistemas";
grant delete on "sistemas".inc_pesoxuni to "public" as "sistemas";
grant index on "sistemas".inc_pesoxuni to "public" as "sistemas";
grant select on "sistemas".inc_unegmovs to "public" as "sistemas";
grant update on "sistemas".inc_unegmovs to "public" as "sistemas";
grant insert on "sistemas".inc_unegmovs to "public" as "sistemas";
grant delete on "sistemas".inc_unegmovs to "public" as "sistemas";
grant index on "sistemas".inc_unegmovs to "public" as "sistemas";
grant select on "sistemas".inc_dvalores to "public" as "sistemas";
grant update on "sistemas".inc_dvalores to "public" as "sistemas";
grant insert on "sistemas".inc_dvalores to "public" as "sistemas";
grant delete on "sistemas".inc_dvalores to "public" as "sistemas";
grant index on "sistemas".inc_dvalores to "public" as "sistemas";
grant select on "sistemas".inc_numtrans to "public" as "sistemas";
grant update on "sistemas".inc_numtrans to "public" as "sistemas";
grant insert on "sistemas".inc_numtrans to "public" as "sistemas";
grant delete on "sistemas".inc_numtrans to "public" as "sistemas";
grant index on "sistemas".inc_numtrans to "public" as "sistemas";
grant select on "sistemas".rpt_invcompollo to "public" as "sistemas";
grant update on "sistemas".rpt_invcompollo to "public" as "sistemas";
grant insert on "sistemas".rpt_invcompollo to "public" as "sistemas";
grant delete on "sistemas".rpt_invcompollo to "public" as "sistemas";
grant index on "sistemas".rpt_invcompollo to "public" as "sistemas";
grant alter on "sistemas".rpt_invcompollo to "public" as "sistemas";
grant references on "sistemas".rpt_invcompollo to "public" as "sistemas";
grant select on "sistemas".inc_corelrpt to "public" as "sistemas";
grant update on "sistemas".inc_corelrpt to "public" as "sistemas";
grant insert on "sistemas".inc_corelrpt to "public" as "sistemas";
grant delete on "sistemas".inc_corelrpt to "public" as "sistemas";
grant index on "sistemas".inc_corelrpt to "public" as "sistemas";
grant alter on "sistemas".inc_corelrpt to "public" as "sistemas";
grant references on "sistemas".inc_corelrpt to "public" as "sistemas";
grant select on "sistemas".inc_pagranel to "public" as "sistemas";
grant update on "sistemas".inc_pagranel to "public" as "sistemas";
grant insert on "sistemas".inc_pagranel to "public" as "sistemas";
grant delete on "sistemas".inc_pagranel to "public" as "sistemas";
grant index on "sistemas".inc_pagranel to "public" as "sistemas";
grant select on "sistemas".inc_paramtrs to "public" as "sistemas";
grant update on "sistemas".inc_paramtrs to "public" as "sistemas";
grant insert on "sistemas".inc_paramtrs to "public" as "sistemas";
grant delete on "sistemas".inc_paramtrs to "public" as "sistemas";
grant index on "sistemas".inc_paramtrs to "public" as "sistemas";
grant select on "sistemas".inc_bitprocs to "public" as "sistemas";
grant update on "sistemas".inc_bitprocs to "public" as "sistemas";
grant insert on "sistemas".inc_bitprocs to "public" as "sistemas";
grant delete on "sistemas".inc_bitprocs to "public" as "sistemas";
grant index on "sistemas".inc_bitprocs to "public" as "sistemas";
grant select on "sistemas".inc_trasuneg to "public" as "sistemas";
grant update on "sistemas".inc_trasuneg to "public" as "sistemas";
grant insert on "sistemas".inc_trasuneg to "public" as "sistemas";
grant delete on "sistemas".inc_trasuneg to "public" as "sistemas";
grant index on "sistemas".inc_trasuneg to "public" as "sistemas";
grant select on "sistemas".tmp_mtransac to "public" as "sistemas";
grant update on "sistemas".tmp_mtransac to "public" as "sistemas";
grant insert on "sistemas".tmp_mtransac to "public" as "sistemas";
grant delete on "sistemas".tmp_mtransac to "public" as "sistemas";
grant index on "sistemas".tmp_mtransac to "public" as "sistemas";
grant select on "sistemas".tmp_dtransac to "public" as "sistemas";
grant update on "sistemas".tmp_dtransac to "public" as "sistemas";
grant insert on "sistemas".tmp_dtransac to "public" as "sistemas";
grant delete on "sistemas".tmp_dtransac to "public" as "sistemas";
grant index on "sistemas".tmp_dtransac to "public" as "sistemas";
grant select on "sistemas".inc_unegocio to "public" as "sistemas";
grant update on "sistemas".inc_unegocio to "public" as "sistemas";
grant insert on "sistemas".inc_unegocio to "public" as "sistemas";
grant delete on "sistemas".inc_unegocio to "public" as "sistemas";
grant index on "sistemas".inc_unegocio to "public" as "sistemas";
grant select on "sistemas".inc_saldxsem to "public" as "sistemas";
grant update on "sistemas".inc_saldxsem to "public" as "sistemas";
grant insert on "sistemas".inc_saldxsem to "public" as "sistemas";
grant delete on "sistemas".inc_saldxsem to "public" as "sistemas";
grant index on "sistemas".inc_saldxsem to "public" as "sistemas";
grant alter on "sistemas".inc_saldxsem to "public" as "sistemas";
grant references on "sistemas".inc_saldxsem to "public" as "sistemas";
grant select on "sistemas".inc_regiones to "public" as "sistemas";
grant update on "sistemas".inc_regiones to "public" as "sistemas";
grant insert on "sistemas".inc_regiones to "public" as "sistemas";
grant delete on "sistemas".inc_regiones to "public" as "sistemas";
grant index on "sistemas".inc_regiones to "public" as "sistemas";


create synonym "sistemas".inc_mtransac_lce for "sistemas".inc_mtransac;
create synonym "sistemas".inc_dtransac_lce for "sistemas".inc_dtransac;
create synonym "sistemas".ind_tipomovs_lce for "sistemas".ind_tipomovs;
create synonym "sistemas".inc_paramtrs_lce for "sistemas".inc_paramtrs;
create synonym "sistemas".inc_maesaldo_lce for "sistemas".inc_maesaldo;

create synonym "sistemas".inc_dtransac_tgu for invpdi@srvbkhntg01_tcp:"sistemas".inc_dtransac;
create synonym "sistemas".ind_tipomovs_tgu for invpdi@srvbkhntg01_tcp:"sistemas".ind_tipomovs;
create synonym "sistemas".inc_maesaldo_sps for invpdi@sps_tcp:"sistemas".inc_maesaldo;
create synonym "sistemas".inc_maesaldo_tgu for invpdi@srvbkhntg01_tcp:"sistemas".inc_maesaldo;
create synonym "sistemas".glb_permisos for globales:"sistemas".glb_permisos;
create synonym "sistemas".glb_usuarios for globales:"sistemas".glb_usuarios;
create synonym "sistemas".glb_empresas for globales:"sistemas".glb_empresas;
create synonym "sistemas".ind_products for distribu:"sistemas".producto;
create synonym "sistemas".producto for distribu:"sistemas".producto;
create synonym "sistemas".glb_descrips for globales:"sistemas".glb_descrips;
create synonym "sistemas".ind_rapespro for invendis:"sistemas".ind_rapespro;
create synonym "sistemas".factur_h for distribu@srvrbhntg01_tcp:"sistemas".factur_h;
create synonym "sistemas".factur_d for distribu@srvrbhntg01_tcp:"sistemas".factur_d;
create synonym "sistemas".des106 for distribu@srvrbhntg01_tcp:"sistemas".des106;
create synonym "sistemas".glb_programs for globales:"sistemas".glb_programs;
create synonym "sistemas".glb_cmbtodos for globales:"sistemas".glb_cmbtodos;
create synonym "sistemas".inc_mtransac_tgu for invpdi@srvbkhntg01_tcp:"sistemas".inc_mtransac;
create synonym "sistemas".glb_semanacl for globales:"sistemas".glb_semanacl;
create synonym "sistemas".vwinvamanecernorte for invpdi@sps_tcp:"sistemas".vwinvamanecernorte;
create synonym "sistemas".vwinvamanecercentro for invpdi@srvbkhntg01_tcp:"sistemas".vwinvamanecercentro;
create synonym "sistemas".inc_paramtrs_tgu for invpdi@srvbkhntg01_tcp:"sistemas".inc_paramtrs;
create synonym "sistemas".inc_dtransac_sps for invpdi@sps_tcp:"sistemas".inc_dtransac;
create synonym "sistemas".inc_mtransac_sps for invpdi@sps_tcp:"sistemas".inc_mtransac;
create synonym "sistemas".ind_tipomovs_sps for invpdi@sps_tcp:"sistemas".ind_tipomovs;
create synonym "sistemas".inc_paramtrs_sps for invpdi@sps_tcp:"sistemas".inc_paramtrs;
create synonym "sistemas".inp_regiones for invppp@noraves_tcp:"sistemas".inp_regiones;




CREATE PROCEDURE "sistemas".spindpedidos(punineg SMALLINT, pnoruta SMALLINT,
                              pfecdes DATE    , ptipcar CHAR(1))
RETURNING SMALLINT,CHAR(35),SMALLINT,DATE,SMALLINT,CHAR(35),SMALLINT,
          CHAR(35),INT,INT,CHAR(35),SMALLINT,CHAR(35),INT,DEC(16,2);

DEFINE wfecdes  DATE;
DEFINE wnumdoc,
       wcodcli,
       wcanuni  INT;
DEFINE wunineg,
       wcodven,
       wnoruta,
       wcditem,
       wcodsup  SMALLINT;
DEFINE wnomcli,
       wnomven,
       wnomneg,
       wnomsup,
       wdsitem  CHAR(35);
DEFINE wcanlbs  DEC(16,2);

 FOREACH
 SELECT a.unineg unineg,e.nomneg nomneg,a.noruta noruta,a.fecdes fecdes,
        a.codven codven,d.nomven nomven,a.codsup codsup,
        a.observ observ,a.numdoc numdoc,a.codcli codcli,
        a.nomcli nomcli,b.cditem cditem,c.dsitem dsitem,
        sum(b.canuni) canuni,sum(b.canlbs) canlbs
 INTO   wunineg,wnomneg,wnoruta,wfecdes,wcodven,wnomven,wcodsup,wnomsup,
        wnumdoc,wcodcli,wnomcli,wcditem,wdsitem,wcanuni,wcanlbs
 FROM  ind_mpedidos a,ind_dpedidos b,
       ind_products c,vta_vendedrs d,
       ind_unegocio e
 WHERE a.lnkped = b.lnkped
 AND   e.unineg = a.unineg
 AND   c.codemp = a.codemp
 AND   c.cditem = b.cditem
 AND   a.codven = d.codven
 AND   a.codsup = d.codsup
 AND   a.noruta = d.noruta
 AND   a.unineg = punineg
 AND   a.fecdes = pfecdes
 AND   a.noruta = pnoruta
 AND   a.tipcar = ptipcar
 AND   a.tipped = 2
 AND   a.estado = "V"
 AND   a.nofase in (1,2)
 GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13


  RETURN wunineg,wnomneg,wnoruta,wfecdes,wcodven,wnomven,wcodsup,wnomsup,
         wnumdoc,wcodcli,wnomcli,wcditem,wdsitem,wcanuni,wcanlbs WITH RESUME;

  END FOREACH;

END PROCEDURE;

CREATE PROCEDURE "sistemas".spincremisiones(pfecini DATE,     pfecfin DATE,
                                 pcodori SMALLINT, pcoddes SMALLINT)
RETURNING SMALLINT,CHAR(35),SMALLINT,CHAR(35),SMALLINT,CHAR(35),
          INT,DEC(16,2);

DEFINE wcanuni  INT;
DEFINE wcditem,
       wcodori,
       wcoddes  SMALLINT;
DEFINE wdsitem,
       wnomori,
       wnomdes  CHAR(35);
DEFINE wcanlbs  DEC(16,2);

  FOREACH SELECT a.codori codori,a.coddes coddes,b.codpro codpro,
                 d.dsitem dsitem,sum(b.canuni) canuni,sum(b.canlbs) canlbs
  INTO  wcodori,wcoddes,wcditem,wdsitem,wcanuni,wcanlbs
  FROM  inc_mtransac a,inc_dtransac b,ind_products d
  WHERE a.lnktra = b.lnktra
  AND   a.codemp = b.codemp
  AND   a.unineg = b.unineg
  AND   a.numdoc = b.numdoc
  AND   a.codori = b.codori
  AND   a.coddes = b.coddes
  AND   a.tipope = b.tipope
  AND   d.codemp = b.codemp
  AND   d.cditem = b.codpro
  AND   a.codori = pcodori
  AND   a.unineg = pcoddes
  AND   a.coddes = pcoddes
  AND   a.tipmov = 1
  AND   a.fecemi >= pfecini
  AND   a.fecemi <= pfecfin
  AND   a.estado = "V"
  GROUP BY 1,2,3,4
  ORDER BY 1,2,3,4

  --#nombre del origen
  SELECT u.nomneg INTO wnomori
  FROM ind_unegocio u
  WHERE u.unineg = wcodori;


  --#nombre del destino
  SELECT v.nomneg INTO wnomdes
  FROM ind_unegocio v
  WHERE v.unineg = wcoddes;

  RETURN wcodori,wnomori,wcoddes,wnomdes,
         wcditem,wdsitem,wcanuni,wcanlbs WITH RESUME;

  END FOREACH;

END PROCEDURE;

CREATE PROCEDURE "sistemas".diariodecamaras22(sp_unineg SMALLINT,
                                 sp_cditem CHAR(20),
                                 sp_fecemi DATE,
                                 sp_canlbs DEC(10,2))

RETURNING CHAR(20),CHAR(50),INTEGER,DEC(12,2),DEC(12,2),DEC(12,2),DEC(12,2);

DEFINE rs_cditem CHAR(20);
DEFINE rs_dsitem CHAR(50);
DEFINE rs_canuni DEC(12,2);
DEFINE rs_psorig DEC(12,2);
DEFINE rs_canlbs DEC(12,2);
DEFINE rs_mermas DEC(12,2);
DEFINE rs_psprom DEC(12,2);

  FOREACH SELECT c.cditem,
                     p.dsitem,
                     c.canuni,
                     c.psorig,
                     c.canlbs,
                     c.mermas,
                     (c.canlbs/c.canuni)
                INTO rs_cditem,
                     rs_dsitem,
                     rs_canuni,
                     rs_psorig,
                     rs_canlbs,
                     rs_mermas,
                     rs_psprom
                FROM ind_psajepro c,ind_products p
                WHERE c.unineg = sp_unineg
                  AND c.fecemi = sp_fecemi
                  AND p.codemp = c.codemp
                  AND p.cditem = c.cditem
                  ORDER BY c.cditem

 RETURN rs_cditem,
                      rs_dsitem,
                      rs_canuni,
                      rs_psorig,
                      rs_canlbs,
                      rs_mermas,
                      rs_psprom WITH RESUME;
      END FOREACH;
END PROCEDURE;

CREATE PROCEDURE "sistemas".sptrasladosxalmacen(wunineg INT,wcodemp INT,wfecini DATE,wfecfin DATE)

 RETURNING INTEGER,CHAR(80),SMALLINT,CHAR(80),INTEGER,
           INTEGER,INTEGER,CHAR(30),CHAR(80),INTEGER,
           INTEGER,DECIMAL(18,2),DECIMAL(18,2),DATE,
           DATE,DATE,INTEGER,INTEGER,CHAR(1),INTEGER,
           DECIMAL(18,2),DECIMAL(18,2),INTEGER,INTEGER,
           CHAR(80)

 --Definiendo Variables
 DEFINE splnktra INTEGER;
 DEFINE spunineg INTEGER;
 DEFINE spnomneg CHAR(80);
 DEFINE vunineg  INTEGER;
 DEFINE vnomneg  CHAR(80);
 DEFINE spcodemp SMALLINT;
 DEFINE spnomemp CHAR(80);
 DEFINE sptipmov INTEGER;
 DEFINE spcodori INTEGER;
 DEFINE spcoddes INTEGER;
 DEFINE spcditem CHAR(30);
 DEFINE spnompro CHAR(80);
 DEFINE spnumrem INTEGER;
 DEFINE spnumcor INTEGER;
 DEFINE spuniori INTEGER;
 DEFINE splbsori DECIMAL(18,2);
 DEFINE sptotori DECIMAL(18,2);
 DEFINE spfecemi DATE;
 DEFINE spmestra INTEGER;
 DEFINE spaniotr INTEGER;
 DEFINE spestado CHAR(1);
 DEFINE spunides INTEGER;
 DEFINE splbsdes DECIMAL(18,2);
 DEFINE sptotdes DECIMAL(18,2);
 DEFINE sptipsrv INTEGER;

 -- Asignando nivel de aislamiento
 SET ISOLATION TO DIRTY READ;

 --Inicializando Variables
 LET splnktra = 0;
 LET spunineg = 0;
 LET spnomneg = NULL;
 LET vunineg  = 0;
 LET vnomneg  = NULL;
 LET spcodemp = 0;
 LET spnomemp = NULL;
 LET sptipmov = 0;
 LET spcodori = 0;
 LET spcoddes = 0;
 LET spcditem = NULL;
 LET spnompro = NULL;
 LET spnumrem = 0;
 LET spnumcor = 0;
 LET spuniori = 0;
 LET splbsori = 0;
 LET sptotori = 0;
 LET spfecemi = NULL;
 LET spmestra = 0;
 LET spaniotr = 0;
 LET spestado = NULL;
 LET spunides = 0;
 LET splbsdes = 0;
 LET sptotdes = 0;
 LET sptipsrv = 0;

IF (wunineg = 0) THEN
  LET vunineg = 0;
  LET vnomneg = "TODOS";
  FOREACH SELECT a.lnktra,a.unineg,e.nomneg,a.codemp,d.abrnom,
                 a.tipmov,a.codori,a.coddes,b.cditem,f.nombre,
                 a.numrem,b.numcor,b.canuni,b.canlbs,b.totpro,
                 a.fecemi,a.mestra,a.aniotr,a.estado,g.tipsrv
            INTO splnktra,spunineg,spnomneg,spcodemp,spnomemp,sptipmov,
                 spcodori,spcoddes,spcditem,spnompro,spnumrem,
                 spnumcor,spuniori,splbsori,sptotori,spfecemi,
                 spmestra,spaniotr,spestado,sptipsrv
            FROM inc_mtransac a, inc_dtransac b,ind_tipomovs c,
                 glb_empresas d, ind_unegocio e,producto f,
                 inc_pxunirem g
           WHERE a.lnktra   = b.lnktra
             AND a.fecemi  >= wfecini
             AND a.fecemi  <= wfecfin
             AND a.estado   = "V"
             AND e.unineg   = a.unineg
             AND d.codemp   = a.codemp
             AND f.producto = b.cditem
             AND c.tipmov   = a.tipmov
             AND g.unineg   = a.coddes
             AND g.tipsrv   >0
             AND c.subcls   = "T"

             IF (sptipsrv = 1) THEN
                 SELECT NVL(y.canuni,0),
                        NVL(y.canlbs,0),
                        NVL(y.totpro,0)
                  INTO spunides,splbsdes,sptotdes
                  FROM invpdi@srvbkhntg01_tcp:inc_mtransac x,
                       invpdi@srvbkhntg01_tcp:inc_dtransac y
                  WHERE x.lnktra = y.lnktra
                    AND x.unineg = spcoddes
                    AND x.codemp = spcodemp
                    AND x.coddes = spcoddes
                    AND x.numrem = spnumrem
                    AND x.lnktrl = splnktra
                    AND y.cditem = spcditem
                    AND y.numcor = spnumcor;
              END IF;

             IF (sptipsrv = 2) THEN
                SELECT NVL(y.canuni,0),
                       NVL(y.canlbs,0),
                       NVL(y.totpro,0)
                  INTO spunides,splbsdes,sptotdes
                  FROM  invpdi@sps_tcp:inc_mtransac x,
                        invpdi@sps_tcp:inc_dtransac y
                  WHERE x.lnktra = y.lnktra
                    AND x.unineg = spcoddes
                    AND x.codemp = spcodemp
                    AND x.coddes = spcoddes
                    AND x.numrem = spnumrem
                    AND x.lnktrl = splnktra
                    AND y.cditem = spcditem
                    AND y.numcor = spnumcor;
             END IF;

             IF (sptipsrv = 3) THEN
                SELECT NVL(y.canuni,0),
                       NVL(y.canlbs,0),
                       NVL(y.totpro,0)
                  INTO spunides,splbsdes,sptotdes
                  FROM invpdi@srvaphnlc01_tcp:inc_mtransac x,
                       invpdi@srvaphnlc01_tcp:inc_dtransac y
                  WHERE x.lnktra = y.lnktra
                    AND x.unineg = spcoddes
                    AND x.codemp = spcodemp
                    AND x.coddes = spcoddes
                    AND x.numrem = spnumrem
                    AND x.lnktrl = splnktra
                    AND y.cditem = spcditem
                    AND y.numcor = spnumcor;
             END IF;

             RETURN spunineg,spnomneg,spcodemp,spnomemp,
                    sptipmov,spcodori,spcoddes,spcditem,
                    spnompro,spnumrem,spuniori,splbsori,
                    sptotori,wfecini,wfecfin,spfecemi,
                    spmestra,spaniotr,spestado,spunides,
                    splbsdes,sptotdes,sptipsrv,vunineg,
                    vnomneg WITH RESUME;
  END FOREACH;

 ELSE

  FOREACH SELECT a.lnktra,a.unineg,e.nomneg,a.codemp,d.abrnom,a.tipmov,
                 a.codori,a.coddes,b.cditem,f.nombre,a.numrem,
                 b.numcor,b.canuni,b.canlbs,b.totpro,a.fecemi,
                 a.mestra,a.aniotr,a.estado,g.tipsrv
            INTO splnktra,spunineg,spnomneg,spcodemp,spnomemp,sptipmov,
                 spcodori,spcoddes,spcditem,spnompro,spnumrem,
                 spnumcor,spuniori,splbsori,sptotori,spfecemi,
                 spmestra,spaniotr,spestado,sptipsrv
            FROM inc_mtransac a, inc_dtransac b,ind_tipomovs c,
                 glb_empresas d, ind_unegocio e,producto f,
                 inc_pxunirem g
           WHERE a.lnktra   = b.lnktra
             AND a.unineg   = wunineg
             AND a.codemp   = wcodemp
             AND a.fecemi  >= wfecini
             AND a.fecemi  <= wfecfin
             AND a.estado   = "V"
             AND e.unineg   = a.unineg
             AND d.codemp   = a.codemp
             AND f.producto = b.cditem
             AND c.tipmov   = a.tipmov
             AND g.unineg   = a.coddes
             AND g.tipsrv   >0
             AND c.subcls   = "T"

             IF (sptipsrv = 1) THEN
                 SELECT NVL(y.canuni,0),
                        NVL(y.canlbs,0),
                        NVL(y.totpro,0)
                  INTO spunides,splbsdes,sptotdes
                  FROM invpdi@srvbkhntg01_tcp:inc_mtransac x,
                       invpdi@srvbkhntg01_tcp:inc_dtransac y
                  WHERE x.lnktra = y.lnktra
                    AND x.unineg = spcoddes
                    AND x.codemp = spcodemp
                    AND x.coddes = spcoddes
                    AND x.numrem = spnumrem
                    AND x.lnktrl = splnktra
                    AND y.cditem = spcditem
                    AND y.numcor = spnumcor;
              END IF;

             IF (sptipsrv = 2) THEN
                SELECT NVL(y.canuni,0),
                       NVL(y.canlbs,0),
                       NVL(y.totpro,0)
                  INTO spunides,splbsdes,sptotdes
                  FROM  invpdi@sps_tcp:inc_mtransac x,
                        invpdi@sps_tcp:inc_dtransac y
                  WHERE x.lnktra = y.lnktra
                    AND x.unineg = spcoddes
                    AND x.codemp = spcodemp
                    AND x.coddes = spcoddes
                    AND x.numrem = spnumrem
                    AND x.lnktrl = splnktra
                    AND y.cditem = spcditem
                    AND y.numcor = spnumcor;
             END IF;

             IF (sptipsrv = 3) THEN
                SELECT NVL(y.canuni,0),
                       NVL(y.canlbs,0),
                       NVL(y.totpro,0)
                  INTO spunides,splbsdes,sptotdes
                  FROM invpdi@srvaphnlc01_tcp:inc_mtransac x,
                       invpdi@srvaphnlc01_tcp:inc_dtransac y
                  WHERE x.lnktra = y.lnktra
                    AND x.unineg = spcoddes
                    AND x.codemp = spcodemp
                    AND x.coddes = spcoddes
                    AND x.numrem = spnumrem
                    AND x.lnktrl = splnktra
                    AND y.cditem = spcditem
                    AND y.numcor = spnumcor;
             END IF;

             LET vunineg = spunineg;
             LET vnomneg = spnomneg;

             RETURN spunineg,spnomneg,spcodemp,spnomemp,
                    sptipmov,spcodori,spcoddes,spcditem,
                    spnompro,spnumrem,spuniori,splbsori,
                    sptotori,wfecini,wfecfin,spfecemi,
                    spmestra,spaniotr,spestado,spunides,
                    splbsdes,sptotdes,sptipsrv ,vunineg,
                    vnomneg WITH RESUME;
  END FOREACH;
 END IF;
END PROCEDURE;

CREATE FUNCTION  "sistemas".fxinventariofisico(wcodemp SMALLINT,wunineg SMALLINT,wcditem CHAR(20),waniotr SMALLINT,wmestra SMALLINT)
 RETURNING INT,DEC(12,2)

 DEFINE spcanuni LIKE inc_maesaldo.canuni;
 DEFINE spcanlbs LIKE inc_maesaldo.canlbs;

 -- Mynor Ramirez
 -- Enero 2009
 -- Calculo de inventario fisico por producto

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ;

 -- Obteniendo inventario fisico del producto
 SELECT  NVL(SUM(a.fisuni),0),
         NVL(SUM(a.fislbs),0)
  INTO   spcanuni,
         spcanlbs
   FROM  inc_invcierd a
   WHERE a.codemp = wcodemp
     AND a.unineg = wunineg
     AND a.annioc = waniotr
     AND a.mescie = wmestra
     AND a.cditem = wcditem;

 IF spcanuni IS NULL THEN LET spcanuni = 0; END IF;
 IF spcanlbs IS NULL THEN LET spcanlbs = 0; END IF;

 RETURN spcanuni,spcanlbs;
END FUNCTION;

CREATE FUNCTION  "sistemas".fxsaldoteorico(wcodemp SMALLINT,wunineg SMALLINT,wcditem CHAR(20),waniotr SMALLINT,wmestra SMALLINT)
 RETURNING INT,DEC(12,2),DEC(12,2)

 DEFINE wcodini  LIKE inc_maesaldo.codval;
 DEFINE spcanuni LIKE inc_maesaldo.canuni;
 DEFINE spcanlbs LIKE inc_maesaldo.canlbs;
 DEFINE sptotpro LIKE inc_maesaldo.totpro;
 DEFINE spopeuni LIKE inc_dtransac.opeuni;
 DEFINE spopelbs LIKE inc_dtransac.opelbs;
 DEFINE spopeval LIKE inc_dtransac.opeval;
 DEFINE xcanuni  LIKE inc_maesaldo.canuni;
 DEFINE xcanlbs  LIKE inc_maesaldo.canlbs;
 DEFINE xtotpro  LIKE inc_maesaldo.totpro;
 DEFINE aniant   SMALLINT;
 DEFINE mesant   SMALLINT;

 -- Mynor Ramirez
 -- Enero 2009
 -- Calculo de saldo teorico por producto

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ;

 -- Clasificacion del saldo inicial
 LET wcodini = 1;

 -- Obteniendo saldo teorico del producto
 -- Obteniendo saldo inicial
 SELECT NVL(SUM(a.canuni),0),
        NVL(SUM(a.canlbs),0),
        NVL(SUM(a.totpro),0)
  INTO  spcanuni,
        spcanlbs,
        sptotpro
  FROM  inc_maesaldo a
  WHERE a.codemp = wcodemp
    AND a.unineg = wunineg
    AND a.cditem = wcditem
    AND a.codval = wcodini
    AND a.aniotr = waniotr
    AND a.mestra = wmestra;

 -- Verificando si existe saldo inicial, de lo contrario obtiene fisico del mes pasado
 IF ((spcanuni+spcanlbs+sptotpro)=0) THEN
    -- Si no existe obtiene fisico del mes pasado
    IF (wmestra=1) THEN
       LET aniant = (waniotr-1);
       LET mesant = 12;
    ELSE
       LET aniant = waniotr;
       LET mesant = (wmestra-1);
    END IF;

    -- Obteniendo fisico del mes pasado
    CALL fxinventariofisico(wcodemp,wunineg,wcditem,aniant,mesant)
    RETURNING spcanuni,spcanlbs;

    LET sptotpro = 0;
 END IF

 -- Obteniendo saldos
 SELECT NVL(SUM(a.opeuni),0),
        NVL(SUM(a.opelbs),0),
        NVL(SUM(a.opeval),0)
  INTO  spopeuni,
        spopelbs,
        spopeval
  FROM  inc_dtransac a
  WHERE a.codemp = wcodemp
    AND a.unineg = wunineg
    AND a.codpro = wcditem
    AND a.aniotr = waniotr
    AND a.mestra = wmestra
    AND a.estado = "V";

 -- Calculando saldo
 LET xcanuni = (spcanuni+spopeuni);
 LET xcanlbs = (spcanlbs+spopelbs);
 LET xtotpro = (sptotpro+spopeval);

 IF xcanuni IS NULL THEN LET xcanuni = 0; END IF;
 IF xcanlbs IS NULL THEN LET xcanlbs = 0; END IF;
 IF xtotpro IS NULL THEN LET xtotpro = 0; END IF;

 RETURN xcanuni,xcanlbs,xtotpro;
END FUNCTION;

CREATE FUNCTION "sistemas".fxcosteoexterno(wcodemp SMALLINT,wunineg SMALLINT,waniotr SMALLINT,wmestra SMALLINT,wcditem CHAR(20))
 RETURNING INT,DEC(12,2),DEC(14,2)

 DEFINE wcodcos  LIKE inc_paramtrs.codcos;
 DEFINE spcditem LIKE inc_maesaldo.cditem;
 DEFINE spcospro LIke inc_dtransac.preuni;
 DEFINE spcanuni LIke inc_maesaldo.canuni;
 DEFINE spcanlbs LIKE inc_maesaldo.canlbs;
 DEFINE sptotpro LIke inc_maesaldo.totpro;
 DEFINE splnktra LIKE inc_dtransac.lnktra;
 DEFINE spnumcor LIKE inc_dtransac.numcor;
 DEFINE sppreuni LIKE inc_maesaldo.preuni;
 DEFINE sptipope LIKE inc_dtransac.tipope;
 DEFINE spcodori LIKE inc_dtransac.codori;
 DEFINE spuniext LIke inc_maesaldo.canuni;
 DEFINE splbsext LIKE inc_maesaldo.canlbs;
 DEFINE sptotext LIke inc_maesaldo.totpro;
 DEFINE spunimed SMALLINT;
 DEFINE wtipsrv  SMALLINT;
 DEFINE wcosext  DEC(14,2);

 -- Mynor Ramirez
 -- Marzo 2009
 -- Costeo de productos de almacenes externos

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ;

 -- Clasificacion del costeo
 SELECT a.codcos
  INTO  wcodcos
  FROM  inc_paramtrs a;

 -- Inicializando datos
 LET spcodori = 0;
 LET spcospro = 0;
 LET spcanuni = 0;
 LET spcanlbs = 0;
 LET sptotpro = 0;
 LET spuniext = 0;
 LET splbsext = 0;
 LET sptotext = 0;

 -- Costeando ingresos de almacenes externos
 -- Tipos de movimiento con subcls = X
 FOREACH SELECT a.lnktra,
                a.codori,
                a.cditem,
                a.numcor,
                a.canuni,
                a.canlbs,
                a.preuni,
                a.totpro,
                a.tipope,
                c.mca_unid_libr
           INTO splnktra,
                spcodori,
                spcditem,
                spnumcor,
                spcanuni,
                spcanlbs,
                sppreuni,
                sptotpro,
                sptipope,
                spunimed
          FROM  inc_dtransac a,producto c,ind_tipomovs b
          WHERE a.codemp   = wcodemp
            AND a.unineg   = wunineg
            AND a.aniotr   = waniotr
            AND a.mestra   = wmestra
            AND a.codpro   = wcditem
            AND a.estado   = "V"
            AND a.tipope   IN (1)
            AND a.codori  != a.unineg
            AND b.tipmov   = a.tipmov
            AND b.subcls   IN ("X")
            AND c.producto = a.cditem

  -- Inicializando precio de costo
  LET sppreuni = 0;

  -- Buscar la unidad de negocio
  LET wtipsrv = (SELECT a.tipsrv
                  FROM  inc_pxunirem a
                  WHERE a.unineg = spcodori
                    AND a.tipsrv >0);

  -- Servidor TGU
  IF (wtipsrv=1) THEN
     -- Obtiene costo del origen
     LET sppreuni = (SELECT NVL(a.preuni,0)
                      FROM  inc_maesaldo_tgu a
                      WHERE a.codemp = wcodemp
                        AND a.unineg = spcodori
                        AND a.cditem = spcditem
                        AND a.codval = wcodcos
                        AND a.aniotr = waniotr
                        AND a.mestra = wmestra);
  END IF;

  -- Servidor SPS
  IF (wtipsrv=2) THEN
     -- Obtiene costo del origen
     LET sppreuni = (SELECT NVL(a.preuni,0)
                      FROM  inc_maesaldo_sps a
                      WHERE a.codemp = wcodemp
                        AND a.unineg = spcodori
                        AND a.cditem = spcditem
                        AND a.codval = wcodcos
                        AND a.aniotr = waniotr
                        AND a.mestra = wmestra);
  END IF;

  -- Servidor SPS
  IF (wtipsrv=3) THEN
      -- Obtiene costo del origen
      LET sppreuni = (SELECT NVL(a.preuni,0)
                       FROM  inc_maesaldo_lce a
                       WHERE a.codemp = wcodemp
                         AND a.unineg = spcodori
                         AND a.cditem = spcditem
                         AND a.codval = wcodcos
                         AND a.aniotr = waniotr
                         AND a.mestra = wmestra);
  END IF;

  -- Verificando costo
  IF sppreuni IS NULL THEN
     LET sppreuni = 0;
  END IF;

  -- Costeando
  -- Verificando unidad de medida
  IF (spunimed=1) THEN -- Unidades
     LET sptotpro = (sppreuni*spcanuni);
  END IF;
  IF (spunimed=2) THEN -- Libras
     LET sptotpro = (sppreuni*spcanlbs);
  END IF;
  IF (spunimed=3) THEN -- Libras y Unidades
     LET sptotpro = (sppreuni*spcanlbs);
  END IF;

  -- Actualizando precio y valor total de las transacciones
  UPDATE inc_dtransac
  SET    inc_dtransac.totpro = sptotpro,
         inc_dtransac.opeval = sptotpro,
         inc_dtransac.preuni = sppreuni
  WHERE  inc_dtransac.lnktra = splnktra
    AND  inc_dtransac.cditem = spcditem
    AND  inc_dtransac.numcor = spnumcor;

  -- Acumulando datos
  LET spuniext = (spuniext+spcanuni);
  LET splbsext = (splbsext+spcanlbs);
  LET sptotext = (sptotext+sptotpro);
 END FOREACH;

 RETURN spuniext,splbsext,sptotext;
END FUNCTION;

CREATE PROCEDURE "sistemas".spamanecernacional(wanio INT,wnumsem INT)

 RETURNING SMALLINT,SMALLINT,SMALLINT,SMALLINT,CHAR(50),
           INTEGER,CHAR(50),INTEGER,DECIMAL(11,2),DECIMAL(16,2)

  --Definiendo variables.
  DEFINE xanosem SMALLINT;
  DEFINE xnumsem SMALLINT;
  DEFINE xcolnum SMALLINT;
  DEFINE xregion SMALLINT;
  DEFINE xnomcol CHAR(50);
  DEFINE xcodpro INTEGER;
  DEFINE xnombre CHAR(50);
  DEFINE xcanuni INTEGER;
  DEFINE xcanupl DECIMAL(11,2);
  DEFINE xcanlbs DECIMAL(16,2);

  -- Atrapando excepciones
  ON EXCEPTION IN (-958)
   DROP TABLE datos1;
   DROP TABLE datos2;
   DROP TABLE datos3;
  END EXCEPTION WITH RESUME;
  ON EXCEPTION IN (-206)
  END EXCEPTION WITH RESUME;

  --Inicializando variables.
  LET xanosem = 0;
  LET xnumsem = 0;
  LET xcolnum = 0;
  LET xregion = 0;
  LET xnomcol = "";
  LET xcodpro = 0;
  LET xnombre = "";
  LET xcanuni = 0;
  LET xcanupl = 0;
  LET xcanlbs = 0;

  --Definiendo nivel de aislamiento.
  SET ISOLATION TO DIRTY READ;

  --Seleccionando datos a nivel nacional.
  SELECT a.anosem,a.numsem,b.colnum,a.region,b.nomcol,
         a.codpro,d.nombre,a.canuni,a.canupl,a.canlbs
   FROM  vwinvamanecercentro a, inc_coluparm b,producto d
   WHERE a.anosem = wanio
     AND a.numsem = wnumsem
     AND b.codreg = a.region
     AND d.producto = a.codpro
     AND d.acumulado = 1 INTO TEMP datos1;

   SELECT a.anosem,a.numsem,b.colnum,a.region,b.nomcol,
         a.codpro,d.nombre,a.canuni,a.canupl,a.canlbs
   FROM  vwinvamanecernorte a, inc_coluparm b,producto d
   WHERE a.anosem = wanio
     AND a.numsem = wnumsem
     AND b.codreg = a.region
     AND d.producto = a.codpro
     AND d.acumulado = 1 INTO TEMP datos2;

  SELECT a.anosem,a.numsem,b.colnum,a.region,b.nomcol,
         a.codpro,d.nombre,a.canuni,a.canupl,a.canlbs
   FROM  vwinvamanecerlitoral a, inc_coluparm b,producto d
   WHERE a.anosem = wanio
     AND a.numsem = wnumsem
     AND b.codreg = a.region
     AND d.producto = a.codpro
     AND d.acumulado = 1 INTO TEMP datos3;

  SELECT a.anosem,
         a.numsem,
         4 as colnum,
         6 as region,
         "TOTAL DISTRIBUIDORAS" AS nomcol,
         a.codpro,
         a.nombre,
         SUM(a.canuni) as canuni,
         SUM(a.canupl) as canupl,
         SUM(a.canlbs) as canlbs
   FROM  datos1 a GROUP BY 1,2,3,4,5,6,7 UNION
  SELECT a.anosem,
         a.numsem,
         4 as colnum,
         6 as region,
         "TOTAL DISTRIBUIDORAS" AS nomcol,
         a.codpro,
         a.nombre,
         SUM(a.canuni) as canuni,
         SUM(a.canupl) as canupl,
         SUM(a.canlbs) as canlbs
   FROM  datos2 a GROUP BY 1,2,3,4,5,6,7 UNION
  SELECT a.anosem,
         a.numsem,
         4 as colnum,
         6 as region,
         "TOTAL DISTRIBUIDORAS" AS nomcol,
         a.codpro,
         a.nombre,
         SUM(a.canuni) as canuni,
         SUM(a.canupl) as canupl,
         SUM(a.canlbs) as canlbs
   FROM  datos3 a GROUP BY 1,2,3,4,5,6,7
   INTO TEMP datos4;

  SELECT a.anosem,
         a.numsem,
         7 as colnum,
         7 as region,
         "TOTAL NACIONAL" AS nomcol,
         a.codpro,
         a.nombre,
         SUM(a.canuni) as canuni,
         SUM(a.canupl) as canupl,
         SUM(a.canlbs) as canlbs
   FROM  datos4 a GROUP BY 1,2,3,4,5,6,7
   INTO TEMP datos5;

  FOREACH SELECT a.*
           INTO  xanosem,xnumsem,xcolnum,xregion,xnomcol,
                 xcodpro,xnombre,xcanuni,xcanupl,xcanlbs
           FROM  datos1 a
           ORDER BY 1,2,3,4,5,6,7
   RETURN xanosem,xnumsem,xcolnum,xregion,xnomcol,xcodpro,
          xnombre,xcanuni,xcanupl,xcanlbs WITH RESUME;
  END FOREACH;

  FOREACH SELECT a.*
           INTO  xanosem,xnumsem,xcolnum,xregion,xnomcol,
                 xcodpro,xnombre,xcanuni,xcanupl,xcanlbs
           FROM  datos2 a
           ORDER BY 1,2,3,4,5,6,7

   RETURN xanosem,xnumsem,xcolnum,xregion,xnomcol,xcodpro,
          xnombre,xcanuni,xcanupl,xcanlbs WITH RESUME;
  END FOREACH;

  FOREACH SELECT a.*
           INTO  xanosem,xnumsem,xcolnum,xregion,xnomcol,
                 xcodpro,xnombre,xcanuni,xcanupl,xcanlbs
           FROM  datos3 a
           ORDER BY 1,2,3,4,5,6,7
   RETURN xanosem,xnumsem,xcolnum,xregion,xnomcol,xcodpro,
          xnombre,xcanuni,xcanupl,xcanlbs WITH RESUME;
  END FOREACH;

  FOREACH SELECT a.*
           INTO  xanosem,xnumsem,xcolnum,xregion,xnomcol,
                 xcodpro,xnombre,xcanuni,xcanupl,xcanlbs
           FROM  datos4 a
           ORDER BY 1,2,3,4,5,6,7
   RETURN xanosem,xnumsem,xcolnum,xregion,xnomcol,xcodpro,
          xnombre,xcanuni,xcanupl,xcanlbs WITH RESUME;
  END FOREACH;

  FOREACH SELECT a.*
           INTO  xanosem,xnumsem,xcolnum,xregion,xnomcol,
                 xcodpro,xnombre,xcanuni,xcanupl,xcanlbs
           FROM  datos5 a
           ORDER BY 1,2,3,4,5,6,7
   RETURN xanosem,xnumsem,xcolnum,xregion,xnomcol,xcodpro,
          xnombre,xcanuni,xcanupl,xcanlbs WITH RESUME;
  END FOREACH;

  DROP TABLE datos4;
  DROP TABLE datos5;
END PROCEDURE;

CREATE FUNCTION "sistemas".fxvalorcosteo(wcodemp SMALLINT,wunineg SMALLINT,waniotr SMALLINT,wmestra SMALLINT,
                              wcodval SMALLINT,wcditem CHAR(20),wunimed SMALLINT,spcanuni DEC(16,2),spcanlbs DEC(16,2))

 RETURNING DECIMAL(16,6),DECIMAL(16,2)

 DEFINE spcospro LIke inc_dtransac.preuni;
 DEFINE sptotpro LIke inc_maesaldo.totpro;

 -- Mynor Ramirez
 -- Marzo 2009
 -- Valorizacion de unidades y libras en base a costo promedio

 -- Obteniendo costo promedio
 SELECT NVL(a.preuni,0)
  INTO  spcospro
  FROM  inc_maesaldo a
  WHERE a.codemp = wcodemp
    AND a.unineg = wunineg
    AND a.cditem = wcditem
    AND a.codval = wcodval
    AND a.aniotr = waniotr
    AND a.mestra = wmestra;

 -- Costeando
 LET sptotpro = 0;

 -- Verificando unidad de medida
 IF (wunimed=1) THEN -- Unidades
    LET sptotpro = (spcospro*spcanuni);
 END IF;
 IF (wunimed=2) THEN -- Libras
    LET sptotpro = (spcospro*spcanlbs);
 END IF;
 IF (wunimed=3) THEN -- Libras y Unidades
    LET sptotpro = (spcospro*spcanlbs);
 END IF;

 -- Verificando productos sin costeo
 IF spcospro IS NULL THEN LET spcospro = 0; END IF;
 IF sptotpro IS NULL THEN LET sptotpro = 0; END IF;

 RETURN spcospro,sptotpro;
END FUNCTION;


 


 


 

CREATE PROCEDURE "sistemas".spinddtransac1(wunineg SMALLINT,
                                wcodemp SMALLINT,wcodbod SMALLINT,
                                wcditem CHAR(20),wcodori CHAR(2),
                                wcodpro CHAR(4) ,wcodest CHAR(2),
                                wcodesp CHAR(3) ,wfecemi DATE,
                                wtipope SMALLINT)
 DEFINE totuni,totlbs,pespro DEC(10,2);
 DEFINE conteo               SMALLINT;

 -- Actualizando existencia por empresa,bodega y producto

 -- Definiendo el nivel de aislamiento
 SET ISOLATION TO DIRTY READ;

 -- Totalizando cantidad de unidades
 LET totuni = (SELECT SUM(a.saluni) FROM ind_dtransac a
               WHERE a.unineg = wunineg
                 AND a.codemp = wcodemp
                 AND a.codbod = wcodbod
                 AND a.cditem = wcditem);

 IF totuni IS NULL THEN
    LET totuni = 0;
 END IF;

 -- Totalizando cantidad de libras del producto
 LET totlbs = (SELECT SUM(a.sallbs) FROM ind_dtransac a
               WHERE a.unineg = wunineg
                 AND a.codemp = wcodemp
                 AND a.codbod = wcodbod
                 AND a.cditem = wcditem);

 IF totlbs IS NULL THEN
    LET totlbs = 0;
 END IF;

 -- Calculando peso promedio
 LET pespro = 0;
 IF (totuni>0) THEN
    LET pespro = (totlbs/totuni);
 END IF

 -- Verificando producto existe, si no existe lo crea
 SELECT COUNT(*)
  INTO  conteo
  FROM  ind_proenbod a
  WHERE (a.unineg = wunineg)
    AND (a.codemp = wcodemp)
    AND (a.codbod = wcodbod)
    AND (a.cditem = wcditem);

  IF (conteo=0) THEN
     -- Creando producto en bodega
     SET LOCK MODE TO WAIT;

     INSERT INTO ind_proenbod
     VALUES (wunineg,wcodemp,wcodbod,wcditem,wcodori,wcodpro,wcodest,
             wcodesp,0,0,0,"","",user,today,current hour to second);
  END IF;

 -- Verificando tipo de operacion
 IF (wtipope=0) THEN -- Egresos
    -- Actualizando producto en bodega
    SET LOCK MODE TO WAIT;
    UPDATE ind_proenbod
    SET    ind_proenbod.saluni = totuni,
           ind_proenbod.sallbs = totlbs,
           ind_proenbod.pespro = pespro,
           ind_proenbod.fulsal = wfecemi
    WHERE  ind_proenbod.unineg = wunineg
      AND  ind_proenbod.codemp = wcodemp
      AND  ind_proenbod.codbod = wcodbod
      AND  ind_proenbod.cditem = wcditem;
 ELSE                -- Ingresos
    -- Actualizando producto en bodega
    SET LOCK MODE TO WAIT;
    UPDATE ind_proenbod
    SET    ind_proenbod.saluni = totuni,
           ind_proenbod.sallbs = totlbs,
           ind_proenbod.pespro = pespro,
           ind_proenbod.fulent = wfecemi
    WHERE  ind_proenbod.unineg = wunineg
      AND  ind_proenbod.codemp = wcodemp
      AND  ind_proenbod.codbod = wcodbod
      AND  ind_proenbod.cditem = wcditem;
  END IF
END PROCEDURE;

CREATE PROCEDURE "sistemas".spindpdpedido1(wlnkped INT)
 DEFINE conteo  SMALLINT;

 -- Definiendo el nivel de aislamiento
 SET ISOLATION TO DIRTY READ;

 -- Verificando si pedido existe, si no existe lo actualiza en ind_mpedidos
 SELECT COUNT(*)
  INTO  conteo
  FROM  ind_pdpedidos a
  WHERE (a.lnkped = wlnkped);

  IF (conteo=1) THEN
    -- Actializando la fase del pedido
    SET LOCK MODE TO WAIT;

    UPDATE ind_mpedidos
    SET    ind_mpedidos.nofase = 3
    WHERE  ind_mpedidos.lnkped = wlnkped;
  END IF;
END PROCEDURE;

CREATE PROCEDURE "sistemas".spindusuaxest1(wlnkped INT,wnofase INT)

 DEFINE conteo  SMALLINT;

 -- Definiendo el nivel de aislamiento
 SET ISOLATION TO DIRTY READ;

 -- Verificando si pedido existe
 SELECT COUNT(*)
  INTO  conteo
  FROM  ind_usuaxest a
  WHERE (a.lnkped = wlnkped);

  IF (conteo=1) THEN
    -- Actualizando usuario y fecha/hora de la nueva fase del pedido
    SET LOCK MODE TO WAIT;

    IF (wnofase=2) THEN
      UPDATE ind_usuaxest
      SET    ind_usuaxest.usraut = USER,
             ind_usuaxest.fecaut = TODAY,
             ind_usuaxest.horaut = CURRENT
      WHERE  ind_usuaxest.lnkped = wlnkped;
    END IF;
    IF (wnofase=3) THEN
      UPDATE ind_usuaxest
      SET    ind_usuaxest.usrpro = USER,
             ind_usuaxest.fecpro = TODAY,
             ind_usuaxest.horpro = CURRENT
      WHERE  ind_usuaxest.lnkped = wlnkped;
    END IF;
    IF (wnofase=4) THEN
      UPDATE ind_usuaxest
      SET    ind_usuaxest.usrcie = USER,
             ind_usuaxest.feccie = TODAY,
             ind_usuaxest.horcie = CURRENT
      WHERE  ind_usuaxest.lnkped = wlnkped;
    END IF;

  END IF;
END PROCEDURE;

CREATE PROCEDURE "sistemas".spsaldosinvpollo(wcodemp SMALLINT,wunineg SMALLINT,wtipmov SMALLINT,
                                             wcditem CHAR(20),waniotr SMALLINT,wmestra SMALLINT,
                                             wtipope SMALLINT)

 DEFINE wcodval LIKE inc_mvalores.codval;
 DEFINE xcodemp LIKE inc_mvalores.codemp;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ;

 -- Seleccionado clasificacion
 LET wcodval = NULL;
 SELECT a.codval
  INTO  wcodval
  FROM  inc_mvalores a,inc_dvalores b
  WHERE a.codval = b.codval
    AND a.movims = "S"
    AND b.tipmov = wtipmov;

  -- Verificando si saldo x clasificacion ya existe
  LET xcodemp = NULL;
  SELECT UNIQUE a.codemp
   INTO  xcodemp
   FROM inc_maesaldo a
   WHERE a.codemp = wcodemp
     AND a.unineg = wunineg
     AND a.cditem = wcditem
     AND a.codval = wcodval
     AND a.aniotr = waniotr
     AND a.mestra = wmestra;

  -- Si ya existe el saldo por clasificacion lo borra
  IF xcodemp IS NOT NULL THEN
     -- Eliminado saldo x clasificacion
     DELETE FROM inc_maesaldo
     WHERE inc_maesaldo.codemp = wcodemp
       AND inc_maesaldo.unineg = wunineg
       AND inc_maesaldo.cditem = wcditem
       AND inc_maesaldo.codval = wcodval
       AND inc_maesaldo.aniotr = waniotr
       AND inc_maesaldo.mestra = wmestra;
  END IF

  -- Creando saldo por clasificacion
  SET LOCK MODE TO WAIT;
  INSERT INTO inc_maesaldo
  SELECT a.codemp     , a.unineg     , a.codpro, b.codval,
         SUM(a.canuni), SUM(a.canlbs), a.aniotr, a.mestra,
         USER         , TODAY        , CURRENT
   FROM  inc_dtransac a,inc_mvalores b,inc_dvalores c
   WHERE a.codemp = wcodemp
     AND a.unineg = wunineg
     AND a.codpro = wcditem
     AND a.aniotr = waniotr
     AND a.mestra = wmestra
     AND a.estado = "V"
     AND a.tipope = wtipope
     AND c.codval = wcodval 
     AND c.tipmov = a.tipmov
     AND b.codval = c.codval
     AND b.movims = "S"
  GROUP BY 1,2,3,4,7,8,9,10,11;
END PROCEDURE;

CREATE PROCEDURE "sistemas".sptrasladosalmacen(wlnktra INTEGER)
 DEFINE detlnktra integer;
 DEFINE detunineg smallint;
 DEFINE detcodemp smallint;
 DEFINE dettipmov smallint;
 DEFINE detcodori smallint;
 DEFINE detcoddes smallint;
 DEFINE detcodbod smallint;
 DEFINE detcditem char(20);
 DEFINE detnumdoc integer;
 DEFINE detaniotr smallint;
 DEFINE detmestra smallint;
 DEFINE detfecpro date;
 DEFINE detbodpro smallint;
 DEFINE detbodori char(2);
 DEFINE detcodpro smallint;
 DEFINE detcodest char(2);
 DEFINE detcodesp char(3);
 DEFINE dettippes char(1);
 DEFINE detpstara decimal(10,2);
 DEFINE detpbruto decimal(10,2);
 DEFINE detcanuni integer;
 DEFINE detcanlbs decimal(10,2);
 DEFINE detpreuni decimal(12,6);
 DEFINE dettotpro decimal(12,2);
 DEFINE detpespro decimal(7,2);
 DEFINE detfecven date;
 DEFINE detnumcor integer;
 DEFINE detbarcod char(30);
 DEFINE dettipope smallint;
 DEFINE detestado char(1);
 DEFINE detopeuni integer;
 DEFINE detopelbs decimal(10,2);
 DEFINE detopeval decimal(12,2);
 DEFINE detnlogin char(10);
 DEFINE detfecsis date;
 DEFINE dethorsis datetime hour to second;
 DEFINE wtipsrv smallint;

 -- Mynor Ramirez / Manuel Ortega
 -- Febrero 2009
 -- Traslado de transacciones entre almacenes

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ;

 -- Buscar la unidad de negocio
 LET wtipsrv = (SELECT a.tipsrv
                FROM inc_mtransac b ,inc_pxunirem a
                WHERE b.lnktra = wlnktra
                  AND a.unineg = b.coddes 
                  AND a.tipsrv >0);

 -- Verificando servidor
 IF (wtipsrv=1) THEN  -- Tegus
   -- Creando encabezado de la transaccion
   INSERT INTO inc_mtransac_tgu
   SELECT 0,
          a.coddes,
          a.codemp,
          b.tipmov,
          a.codori,
          a.coddes,
          a.numdoc,
          a.aniotr,
          a.mestra,
          a.fecemi,
          a.numcon,
          a.nplaca,
          c.tipope,
          a.march1,
          a.trasld,
          a.lnktra,
          a.horsal,
          a.hentmk,
          a.haptmk,
          a.feclle,
          a.horlle,
          a.tmpsal,
          a.tmplle,
          a.estado,
          a.motanl,
          a.fecanl,
          a.horanl,
          a.usranl,
          a.impres,
          a.numrem,
          a.fecdsc,
          a.hordsc,
          a.observ,
          a.mermas,
          user,
          current,
          current hour to second
    FROM  inc_mtransac a,inc_paramtrs b,ind_tipomovs c
    WHERE a.lnktra = wlnktra
      AND b.tipmov = c.tipmov;

   -- Obteniendo serial de la tabla encabezado
   SELECT UNIQUE DBINFO("SQLCA.SQLERRD1")
    INTO  detlnktra
     FROM inc_mtransac_tgu;

   -- Creando detalle de la transaccion
   FOREACH SELECT a.coddes,
                  a.codemp,
                  b.tipmov,
                  a.codori,
                  a.coddes,
                  a.codbod,
                  a.cditem,
                  a.numdoc,
                  a.aniotr,
                  a.mestra,
                  a.fecpro,
                  a.bodpro,
                  a.bodori,
                  a.codpro,
                  a.codest,
                  a.codesp,
                  a.tippes,
                  a.pstara,
                  a.pbruto,
                  a.canuni,
                  a.canlbs,
                  a.preuni,
                  a.totpro,
                  a.pespro,
                  a.fecven,
                  a.numcor,
                  a.barcod,
                  c.tipope,
                  a.estado,
                  (a.opeuni*(-1)),
                  (a.opelbs*(-1)),
                  (a.opeval*(-1)),
                  user,
                  current,
                  current hour to second
            INTO  detunineg,
                  detcodemp,
                  dettipmov,
                  detcodori,
                  detcoddes,
                  detcodbod,
                  detcditem,
                  detnumdoc,
                  detaniotr,
                  detmestra,
                  detfecpro,
                  detbodpro,
                  detbodori,
                  detcodpro,
                  detcodest,
                  detcodesp,
                  dettippes,
                  detpstara,
                  detpbruto,
                  detcanuni,
                  detcanlbs,
                  detpreuni,
                  dettotpro,
                  detpespro,
                  detfecven,
                  detnumcor,
                  detbarcod,
                  dettipope,
                  detestado,
                  detopeuni,
                  detopelbs,
                  detopeval,
                  detnlogin,
                  detfecsis,
                  dethorsis
             FROM  inc_dtransac a,inc_paramtrs b,ind_tipomovs c
             WHERE a.lnktra = wlnktra
               AND b.tipmov = c.tipmov

    -- Insertando filas del detalle
    INSERT INTO inc_dtransac_tgu
    VALUES (detlnktra, detunineg, detcodemp, dettipmov,
            detcodori, detcoddes, detcodbod, detcditem,
            detnumdoc, detaniotr, detmestra, detfecpro,
            detbodpro, detbodori, detcodpro, detcodest,
            detcodesp, dettippes, detpstara, detpbruto,
            detcanuni, detcanlbs, detpreuni, dettotpro,
            detpespro, detfecven, detnumcor, detbarcod,
            dettipope, detestado, detopeuni, detopelbs,
           detopeval, detnlogin, detfecsis, dethorsis);
  END FOREACH;
 END IF;

 IF (wtipsrv=2) THEN  -- San Pedro
   -- Creando encabezado de la transaccion
   INSERT INTO inc_mtransac_sps
   SELECT 0,
          a.coddes,
          a.codemp,
          b.tipmov,
          a.codori,
          a.coddes,
          a.numdoc,
          a.aniotr,
          a.mestra,
          a.fecemi,
          a.numcon,
          a.nplaca,
          c.tipope,
          a.march1,
          a.trasld,
          a.lnktra,
          a.horsal,
          a.hentmk,
          a.haptmk,
          a.feclle,
          a.horlle,
          a.tmpsal,
          a.tmplle,
          a.estado,
          a.motanl,
          a.fecanl,
          a.horanl,
          a.usranl,
          a.impres,
          a.numrem,
          a.fecdsc,
          a.hordsc,
          a.observ,
          a.mermas,
          user,
          current,
          current hour to second
    FROM  inc_mtransac a,inc_paramtrs b,ind_tipomovs c
    WHERE a.lnktra = wlnktra
      AND b.tipmov = c.tipmov;

   -- Obteniendo serial de la tabla encabezado
   SELECT UNIQUE DBINFO("SQLCA.SQLERRD1")
    INTO  detlnktra
     FROM inc_mtransac_sps;

   -- Creando detalle de la transaccion
   FOREACH SELECT a.coddes,
                  a.codemp,
                  b.tipmov,
                  a.codori,
                  a.coddes,
                  a.codbod,
                  a.cditem,
                  a.numdoc,
                  a.aniotr,
                  a.mestra,
                  a.fecpro,
                  a.bodpro,
                  a.bodori,
                  a.codpro,
                  a.codest,
                  a.codesp,
                  a.tippes,
                  a.pstara,
                  a.pbruto,
                  a.canuni,
                  a.canlbs,
                  a.preuni,
                  a.totpro,
                  a.pespro,
                  a.fecven,
                  a.numcor,
                  a.barcod,
                  c.tipope,
                  a.estado,
                  (a.opeuni*(-1)),
                  (a.opelbs*(-1)),
                  (a.opeval*(-1)),
                  user,
                  current,
                  current hour to second
            INTO  detunineg,
                  detcodemp,
                  dettipmov,
                  detcodori,
                  detcoddes,
                  detcodbod,
                  detcditem,
                  detnumdoc,
                  detaniotr,
                  detmestra,
                  detfecpro,
                  detbodpro,
                  detbodori,
                  detcodpro,
                  detcodest,
                  detcodesp,
                  dettippes,
                  detpstara,
                  detpbruto,
                  detcanuni,
                  detcanlbs,
                  detpreuni,
                  dettotpro,
                  detpespro,
                  detfecven,
                  detnumcor,
                  detbarcod,
                  dettipope,
                  detestado,
                  detopeuni,
                  detopelbs,
                  detopeval,
                  detnlogin,
                  detfecsis,
                  dethorsis
             FROM  inc_dtransac a,inc_paramtrs b,ind_tipomovs c
             WHERE a.lnktra = wlnktra
               AND b.tipmov = c.tipmov

    -- Insertando filas del detalle
    INSERT INTO inc_dtransac_sps
    VALUES (detlnktra, detunineg, detcodemp, dettipmov,
            detcodori, detcoddes, detcodbod, detcditem,
            detnumdoc, detaniotr, detmestra, detfecpro,
            detbodpro, detbodori, detcodpro, detcodest,
            detcodesp, dettippes, detpstara, detpbruto,
            detcanuni, detcanlbs, detpreuni, dettotpro,
            detpespro, detfecven, detnumcor, detbarcod,
            dettipope, detestado, detopeuni, detopelbs,
            detopeval, detnlogin, detfecsis, dethorsis);
  END FOREACH;
 END IF;

 IF (wtipsrv=3) THEN  -- La Ceiba
   -- Creando encabezado de la transaccion
   INSERT INTO inc_mtransac_lce
   SELECT 0,
          a.coddes,
          a.codemp,
          b.tipmov,
          a.codori,
          a.coddes,
          a.numdoc,
          a.aniotr,
          a.mestra,
          a.fecemi,
          a.numcon,
          a.nplaca,
          c.tipope,
          a.march1,
          a.trasld,
          a.lnktra,
          a.horsal,
          a.hentmk,
          a.haptmk,
          a.feclle,
          a.horlle,
          a.tmpsal,
          a.tmplle,
          a.estado,
          a.motanl,
          a.fecanl,
          a.horanl,
          a.usranl,
          a.impres,
          a.numrem,
          a.fecdsc,
          a.hordsc,
          a.observ,
          a.mermas,
          user,
          current,
          current hour to second
    FROM  inc_mtransac a,inc_paramtrs b,ind_tipomovs c
    WHERE a.lnktra = wlnktra
      AND b.tipmov = c.tipmov;

   -- Obteniendo serial de la tabla encabezado
   SELECT UNIQUE DBINFO("SQLCA.SQLERRD1")
    INTO  detlnktra
     FROM inc_mtransac_lce;

   -- Creando detalle de la transaccion
   FOREACH SELECT a.coddes,
                  a.codemp,
                  b.tipmov,
                  a.codori,
                  a.coddes,
                  a.codbod,
                  a.cditem,
                  a.numdoc,
                  a.aniotr,
                  a.mestra,
                  a.fecpro,
                  a.bodpro,
                  a.bodori,
                  a.codpro,
                  a.codest,
                  a.codesp,
                  a.tippes,
                  a.pstara,
                  a.pbruto,
                  a.canuni,
                  a.canlbs,
                  a.preuni,
                  a.totpro,
                  a.pespro,
                  a.fecven,
                  a.numcor,
                  a.barcod,
                  c.tipope,
                  a.estado,
                  (a.opeuni*(-1)),
                  (a.opelbs*(-1)),
                  (a.opeval*(-1)),
                  user,
                  current,
                  current hour to second
            INTO  detunineg,
                  detcodemp,
                  dettipmov,
                  detcodori,
                  detcoddes,
                  detcodbod,
                  detcditem,
                  detnumdoc,
                  detaniotr,
                  detmestra,
                  detfecpro,
                  detbodpro,
                  detbodori,
                  detcodpro,
                  detcodest,
                  detcodesp,
                  dettippes,
                  detpstara,
                  detpbruto,
                  detcanuni,
                  detcanlbs,
                  detpreuni,
                  dettotpro,
                  detpespro,
                  detfecven,
                  detnumcor,
                  detbarcod,
                  dettipope,
                  detestado,
                  detopeuni,
                  detopelbs,
                  detopeval,
                  detnlogin,
                  detfecsis,
                  dethorsis
             FROM  inc_dtransac a,inc_paramtrs b,ind_tipomovs c
             WHERE a.lnktra = wlnktra
               AND b.tipmov = c.tipmov

    -- Insertando filas del detalle
    INSERT INTO inc_dtransac_lce
    VALUES (detlnktra, detunineg, detcodemp, dettipmov,
            detcodori, detcoddes, detcodbod, detcditem,
            detnumdoc, detaniotr, detmestra, detfecpro,
            detbodpro, detbodori, detcodpro, detcodest,
            detcodesp, dettippes, detpstara, detpbruto,
            detcanuni, detcanlbs, detpreuni, dettotpro,
            detpespro, detfecven, detnumcor, detbarcod,
            dettipope, detestado, detopeuni, detopelbs,
            detopeval, detnlogin, detfecsis, dethorsis);
   END FOREACH;
 END IF;
END PROCEDURE;

CREATE PROCEDURE "sistemas".spsaldosxproducto(wcodemp SMALLINT,wunineg SMALLINT,wtipmov SMALLINT,
                                              wcditem CHAR(20),waniotr SMALLINT,wmestra SMALLINT,
                                              wtipope SMALLINT)

 DEFINE wcodval  LIKE inc_mvalores.codval;
 DEFINE xcodemp  LIKE inc_mvalores.codemp;
 DEFINE wcodteo  LIKE inc_mvalores.codval;
 DEFINE wcodini  LIKE inc_mvalores.codval;
 DEFINE spcanuni LIKE inc_maesaldo.canuni;
 DEFINE spcanlbs LIKE inc_maesaldo.canlbs;
 DEFINE sptotpro LIKE inc_maesaldo.totpro;
 DEFINE spopeuni LIKE inc_dtransac.opeuni;
 DEFINE spopelbs LIKE inc_dtransac.opelbs;
 DEFINE spopeval LIKE inc_dtransac.opeval;
 DEFINE xcanuni  LIKE inc_maesaldo.canuni;
 DEFINE xcanlbs  LIKE inc_maesaldo.canlbs;
 DEFINE xtotpro  LIKE inc_maesaldo.totpro;
 DEFINE aniant   SMALLINT;
 DEFINE mesant   SMALLINT;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ;

 -- Clasificacion del saldo teorico x producto
 LET wcodteo = 10;
 LET wcodini = 1;

 -- Seleccionado clasificacion
 LET wcodval = NULL;
 SELECT a.codval
  INTO  wcodval
  FROM  inc_mvalores a,inc_dvalores b
  WHERE a.codval = b.codval
    AND a.movims = "S"
    AND b.tipmov = wtipmov;

 -- Verificando si saldo x clasificacion ya existe
 LET xcodemp = NULL;
 SELECT UNIQUE a.codemp
  INTO  xcodemp
  FROM inc_maesaldo a
  WHERE a.codemp = wcodemp
    AND a.unineg = wunineg
    AND a.cditem = wcditem
    AND a.codval = wcodval
    AND a.aniotr = waniotr
    AND a.mestra = wmestra;

 -- Si ya existe el saldo por clasificacion lo borra
 IF xcodemp IS NOT NULL THEN
    -- Eliminado saldo x clasificacion
    DELETE FROM inc_maesaldo
    WHERE inc_maesaldo.codemp = wcodemp
      AND inc_maesaldo.unineg = wunineg
      AND inc_maesaldo.cditem = wcditem
      AND inc_maesaldo.codval = wcodval
      AND inc_maesaldo.aniotr = waniotr
      AND inc_maesaldo.mestra = wmestra;
 END IF

 -- Creando saldo por clasificacion
 SET LOCK MODE TO WAIT;

 INSERT INTO inc_maesaldo
 SELECT a.codemp,
        a.unineg,
        a.codpro,
        b.codval,
        NVL(SUM(a.canuni),0),
        NVL(SUM(a.canlbs),0),
        0,
        NVL(SUM(a.totpro),0),
        a.aniotr,
        a.mestra,
        USER,
        TODAY,
        CURRENT
  FROM  inc_dtransac a,inc_mvalores b,inc_dvalores c
  WHERE a.codemp = wcodemp
    AND a.unineg = wunineg
    AND a.codpro = wcditem
    AND a.aniotr = waniotr
    AND a.mestra = wmestra
    AND a.estado = "V"
    AND a.tipope = wtipope
    AND c.codval = wcodval
    AND c.tipmov = a.tipmov
    AND b.codval = c.codval
    AND b.movims = "S"
 GROUP BY 1,2,3,4,9,10,11,12,13;

 -- Verificando si saldo teorico por producto existe
 LET xcodemp = NULL;
 SELECT UNIQUE a.codemp
  INTO  xcodemp
  FROM  inc_maesaldo a
  WHERE a.codemp = wcodemp
    AND a.unineg = wunineg
    AND a.cditem = wcditem
    AND a.codval = wcodteo
    AND a.aniotr = waniotr
    AND a.mestra = wmestra;

 -- Si ya existe el saldo teorico por producto lo borra
 IF xcodemp IS NOT NULL THEN
    -- Eliminado saldo x clasificacion
    DELETE FROM inc_maesaldo
    WHERE inc_maesaldo.codemp = wcodemp
      AND inc_maesaldo.unineg = wunineg
      AND inc_maesaldo.cditem = wcditem
      AND inc_maesaldo.codval = wcodteo
      AND inc_maesaldo.aniotr = waniotr
      AND inc_maesaldo.mestra = wmestra;
 END IF

 -- Creando saldo teorico por producto
 CALL fxsaldoteorico(wcodemp,wunineg,wcditem,waniotr,wmestra)
 RETURNING xcanuni,xcanlbs,xtotpro;

 -- Creando saldo teorico
 SET LOCK MODE TO WAIT;

 INSERT INTO inc_maesaldo
 VALUES (wcodemp,
         wunineg,
         wcditem,
         wcodteo,
         xcanuni,
         xcanlbs,
         0,
         xtotpro,
         waniotr,
         wmestra,
         USER,
         CURRENT,
         CURRENT HOUR TO SECOND);
END PROCEDURE;

CREATE PROCEDURE "sistemas".spanlautoalmacen(wlnktra INTEGER)
 DEFINE westado LIKE inc_mtransac.estado;
 DEFINE wmotanl LIKE inc_mtransac.motanl;
 DEFINE wfecanl LIKE inc_mtransac.fecanl;
 DEFINE whoranl LIKE inc_mtransac.horanl;
 DEFINE wusranl LIKE inc_mtransac.usranl;
 DEFINE wtipsrv INTEGER;
 DEFINE wsubcls CHAR;

 -- Manuel Ortega
 -- Miercoles, 11 de Marzo del 2009
 -- Jueves,    12 de Marzo del 2009
 -- Proceso de anulacion de remisiones de entrada en la unidad destino

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ;

 -- Obteniendo datos de la transaccion
 LET  westado = NULL;
 LET  wmotanl = NULL;
 LET  wfecanl = NULL;
 LET  whoranl = NULL;
 LET  wusranl = NULL;

 SELECT a.estado,a.motanl,a.fecanl,a.horanl,a.usranl
  INTO  westado,wmotanl,wfecanl,whoranl,wusranl
  FROM  inc_mtransac a
  WHERE (a.lnktra = wlnktra);

 -- Obteniendo si transaccion es de traslado
 LET wsubcls = (SELECT c.subcls
                FROM inc_mtransac z,ind_tipomovs c
                WHERE z.lnktra = wlnktra
                AND c.tipmov = z.tipmov);

 IF (wsubcls = 'T') THEN
    -- Buscar el tipo de servidor de la unidad destino
    LET wtipsrv = (SELECT a.tipsrv
                  FROM inc_mtransac b ,inc_pxunirem a
                  WHERE b.lnktra = wlnktra
                  AND a.unineg = b.coddes
                  AND a.tipsrv >0);

   -- Verificando servidor
   IF (wtipsrv=1) THEN  -- Tegucigalpa
     --Actualizando estado = A en tabla inc_mtransac de unidad destino
     UPDATE inc_mtransac_tgu
     SET estado = westado,
         motanl = wmotanl,
         fecanl = wfecanl,
         horanl = whoranl,
         usranl = wusranl
     WHERE lnktrl = wlnktra;

   END IF;

   IF (wtipsrv=2) THEN  -- San Pedro Sula
     --Actualizando estado = A, en tabla inc_mtransac de unidad destino
     UPDATE inc_mtransac_sps
     SET estado = westado,
         motanl = wmotanl,
         fecanl = wfecanl,
         horanl = whoranl,
         usranl = wusranl
     WHERE lnktrl = wlnktra;
   END IF;

   IF (wtipsrv=3) THEN  -- La Ceiba
     --Actualizando estado = A, en tabla inc_mtransac de unidad destino
     UPDATE inc_mtransac_lce
     SET estado = westado,
         motanl = wmotanl,
         fecanl = wfecanl,
         horanl = whoranl,
         usranl = wusranl
     WHERE lnktrl = wlnktra;
   END IF;
 END IF;
END PROCEDURE;

CREATE PROCEDURE "sistemas".spabsorciones(wcodemp SMALLINT,wunineg SMALLINT,
waniotr SMALLINT,wmestra SMALLINT)

 DEFINE xcodemp  LIKE inc_mvalores.codemp;
 DEFINE wcodcos  LIKE inc_paramtrs.codcos;
 DEFINE wcodini  LIKE inc_paramtrs.codini;
 DEFINE spcditem LIKE inc_maesaldo.cditem;
 DEFINE spcospro LIKE inc_dtransac.preuni;
 DEFINE spcanuni LIKE inc_maesaldo.canuni;
 DEFINE spcanlbs LIKE inc_maesaldo.canlbs;
 DEFINE sptotpro LIke inc_maesaldo.totpro;
 DEFINE scanuni  LIke inc_maesaldo.canuni;
 DEFINE scanlbs  LIke inc_maesaldo.canlbs;
 DEFINE stotpro  LIke inc_maesaldo.totpro;
 DEFINE etotpro  LIke inc_maesaldo.totpro;
 DEFINE splnktra LIKE inc_dtransac.lnktra;
 DEFINE spnumcor LIKE inc_dtransac.numcor;
 DEFINE sppreuni LIKE inc_maesaldo.preuni;
 DEFINE spopeval LIKE inc_dtransac.opeval;
 DEFINE sptipope LIKE inc_dtransac.tipope;
 DEFINE spunimed SMALLINT;
 DEFINE wcodteo  LIKE inc_paramtrs.codteo; --Saldo Teorico
 DEFINE wabsmas  LIKE inc_paramtrs.absmas; --Absorcion (+)
 DEFINE wabsmen  LIKE inc_paramtrs.absmen; --Absorcion (-)
 DEFINE wfisuni  LIKE inc_invcierd.fisuni; --Fisico Unidades
 DEFINE wfislbs  LIKE inc_invcierd.fislbs; --Fisico Libras
 DEFINE widabsmas LIKE inc_paramtrs.absmas;
 DEFINE widabsmen LIKE inc_paramtrs.absmen;
 DEFINE wunipos SMALLINT;
 DEFINE wlbspos SMALLINT;

 -- Mynor Ramirez/Manuel Ortega
 -- Viernes 20 de Febrero del 2009
 -- Proceso de Absorcion para el inventario comparativo (Fisico - Teorico)

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ;

 -- Clasificacion de absorcion
 SELECT a.codteo,a.absmas,a.absmen,a.codcos
  INTO  wcodteo,wabsmas,wabsmen,wcodcos 
  FROM  inc_paramtrs a;

 -- Eliminando absorcion existente
 DELETE FROM inc_maesaldo
 WHERE inc_maesaldo.codemp  = wcodemp
   AND inc_maesaldo.unineg  = wunineg
   AND inc_maesaldo.cditem IS NOT NULL
   AND (inc_maesaldo.codval = wabsmas OR inc_maesaldo.codval = wabsmen)
   AND inc_maesaldo.aniotr  = waniotr
   AND inc_maesaldo.mestra  = wmestra;

 -- Inicializando datos
 LET spcospro = 0;
 LET spcanuni = 0;
 LET spcanlbs = 0;
 LET sptotpro = 0;
 LET scanuni  = 0;
 LET scanlbs  = 0;
 LET stotpro  = 0;
 LET wfisuni  = 0;
 LET wfislbs  = 0;
 LET spcditem = 0;
 LET spunimed = 0;
 LET widabsmas = 0;
 LET widabsmen = 0;
 LET wunipos = 0;
 LET wlbspos = 0;

 -- Generando proceso de absorcion
 FOREACH SELECT a.producto,
                a.mca_unid_libr
          INTO  spcditem,
                spunimed
          FROM  producto a
          ORDER BY a.producto

  -- Seleccionando saldo teorico
  CALL fxsaldoteorico(wcodemp,wunineg,spcditem,waniotr,wmestra)
  RETURNING scanuni,scanlbs,stotpro;

  -- Seleccionando inventario fisico
  CALL fxinventariofisico(wcodemp,wunineg,spcditem,waniotr,wmestra)
  RETURNING wfisuni,wfislbs;

  -- Excluyendo productos sin fisico y teorico
  IF ((scanuni+scanlbs+wfisuni+wfislbs)=0) THEN
     CONTINUE FOREACH;
  END IF

  -- Inicializando datos
  LET spcanuni = 0;
  LET spcanlbs = 0;
  LET sptotpro = 0;

  -- Verificando libras
  IF (wfisuni>scanuni) THEN
     LET spcanuni = (wfisuni-scanuni);
     LET wunipos = 1;
  ELSE
     LET spcanuni = (scanuni-wfisuni);
     LET wunipos = 0;
  END IF;

  -- Verificando unidades
  IF (wfislbs>scanlbs) THEN
     LET spcanlbs = (wfislbs-scanlbs);
     LET wlbspos = 1;
  ELSE
     LET spcanlbs = (scanlbs-wfislbs);
     LET wlbspos = 0;
  END IF;

  -- Excluyendo productos sin diferencias
  IF ((spcanuni+spcanlbs)=0) THEN
     CONTINUE FOREACH;
  END IF

  -- Verificando tipo de transaccion
  IF (wunipos=1) AND (wlbspos=1) THEN

     -- Costeando total de absorcion
     LET spcospro,sptotpro = fxvalorcosteo(wcodemp,wunineg,waniotr,wmestra,wcodcos,spcditem,spunimed,spcanuni,spcanlbs);

     INSERT INTO inc_maesaldo
     VALUES (wcodemp,wunineg,spcditem,wabsmas,spcanuni,spcanlbs,
             spcospro,sptotpro,waniotr,wmestra,USER,TODAY,CURRENT);
  ELSE
     IF (wunipos=1) THEN

        -- Costeando total de absorcion
        LET spcospro,sptotpro = fxvalorcosteo(wcodemp,wunineg,waniotr,wmestra,wcodcos,spcditem,spunimed,spcanuni,0);

        INSERT INTO inc_maesaldo
        VALUES (wcodemp,wunineg,spcditem,wabsmas,spcanuni,0,
                spcospro,sptotpro,waniotr,wmestra,USER,TODAY,CURRENT);
     ELSE
        IF (spcanuni>0) THEN

         -- Costeando total de absorcion
         LET spcospro,sptotpro = fxvalorcosteo(wcodemp,wunineg,waniotr,wmestra,wcodcos,spcditem,spunimed,spcanuni,0);

         INSERT INTO inc_maesaldo
         VALUES (wcodemp,wunineg,spcditem,wabsmen,spcanuni,0,
                 spcospro,sptotpro,waniotr,wmestra,USER,TODAY,CURRENT);
        END IF;
     END IF;

     IF (wlbspos=1) THEN

        -- Costeando total de absorcion
        LET spcospro,sptotpro = fxvalorcosteo(wcodemp,wunineg,waniotr,wmestra,wcodcos,spcditem,spunimed,0,spcanlbs);

        INSERT INTO inc_maesaldo
        VALUES (wcodemp,wunineg,spcditem,wabsmas,0,spcanlbs,
                spcospro,sptotpro,waniotr,wmestra,USER,TODAY,CURRENT);
     ELSE
        IF (spcanlbs>0) THEN
 
         -- Costeando total de absorcion
         LET spcospro,sptotpro = fxvalorcosteo(wcodemp,wunineg,waniotr,wmestra,wcodcos,spcditem,spunimed,0,spcanlbs); 

         INSERT INTO inc_maesaldo
         VALUES (wcodemp,wunineg,spcditem,wabsmen,0,spcanlbs,
                 spcospro,sptotpro,waniotr,wmestra,USER,TODAY,CURRENT);
       END IF;
     END IF;
  END IF;
 END FOREACH;
END PROCEDURE;

CREATE PROCEDURE "sistemas".spamanecerxregion(wanio INT,wnumsem INT)

 --Definiendo Variables
 DEFINE xunineg SMALLINT;
 DEFINE xcodemp SMALLINT;
 DEFINE xregion SMALLINT;
 DEFINE xcodpro INTEGER;
 DEFINE xfechai CHAR(15);
 DEFINE xfecini DATE;
 DEFINE xfecfin DATE;
 DEFINE xiniuni INTEGER;
 DEFINE xinilbs DECIMAL(16,2);
 DEFINE xentuni INTEGER;
 DEFINE xentlbs DECIMAL(16,2);
 DEFINE xsaluni INTEGER;
 DEFINE xsallbs DECIMAL (16,2);
 DEFINE xsaldun INTEGER;
 DEFINE xsaldlb DECIMAL(16,2);
 DEFINE cmes    INTEGER;
 DEFINE canio   INTEGER;

 --Consolidacion de inventario amanecer x region
 --Louis Farfan
 --Marzo 2009

 --Inicializando variables
 LET xunineg = 0;
 LET xcodemp = 1;
 LET xregion = 0;
 LET xcodpro = 0;
 LET xfechai = NULL;
 LET xfecini = NULL;
 LET xfecfin = NULL;
 LET xiniuni = 0;
 LET xinilbs = 0;
 LET xentuni = 0;
 LET xentlbs = 0;
 LET xsaluni = 0;
 LET xsallbs = 0;
 LET cmes    = 0;
 LET canio   = 0;

 --Definiendo nivel de aislamiento.
 SET ISOLATION TO DIRTY READ;

 --Borrando datos segun anio y semana
 DELETE FROM inc_saldxsem
 WHERE inc_saldxsem.anosem = wanio
   AND inc_saldxsem.numsem = wnumsem;

 --Selecionando fecha inicial y final segun semana
 SELECT a.fcisem,a.fcfsem
   INTO xfecini,xfecfin
   FROM glb_semanacl a
  WHERE a.anosem = wanio
    AND a.numsem = wnumsem;

 --Creando fecha inicial seleccion
 LET xfechai = MDY(MONTH(xfecini),01,YEAR(xfecini));

 -- Seleciconando productos
 FOREACH SELECT a.cditem
          INTO  xcodpro
          FROM  inc_proinvam a
          WHERE (a.aparec = "S")

  LET xsaldun = 0;
  LET xsaldlb = 0;

  -- Selecionando unidades de negocio
  FOREACH SELECT a.unineg,a.region
           INTO  xunineg,xregion
           FROM  inc_unegocio a
           WHERE a.tipneg IN (2,3,4,6)

   -- Seleccionando saldo inicial
   SELECT NVL(SUM(c.canuni),0),NVL(SUM(c.canlbs),0)
    INTO  xiniuni,xinilbs
    FROM  inc_maesaldo c
    WHERE c.codemp IS NOT NULL
      AND c.unineg = xunineg
      AND c.cditem = xcodpro
      AND c.codval = 1
      AND c.aniotr = YEAR(xfecini)
      AND c.mestra = MONTH(xfecini);

   -- Si no hay inicial busca fisico del mes pasado
   IF ((xiniuni+xinilbs)=0) THEN
      LET cmes  = (MONTH(xfecini)-1);
      LET canio = YEAR(xfecini);
      IF cmes = 0 THEN
        LET cmes  = 12;
        LET canio = (canio-1);
      END IF;

      -- Seleccionando fisico del mes pasado
      SELECT  NVL(SUM(a.fisuni),0),
              NVL(SUM(a.fislbs),0)
        INTO  xiniuni,xinilbs
        FROM  inc_invcierd a
        WHERE a.codemp IS NOT NULL
          AND a.unineg = xunineg
          AND a.annioc = canio
          AND a.mescie = cmes
          AND a.cditem = xcodpro;
   END IF;

   -- Seleccionando entradas
   SELECT NVL(SUM(b.canuni),0),NVL(SUM(b.canlbs),0)
    INTO  xentuni,xentlbs
    FROM  inc_mtransac a,inc_dtransac b
    WHERE a.lnktra = b.lnktra
      AND a.fecemi >= TO_DATE(xfechai,'%d/%m/%Y')
      AND a.fecemi <= xfecfin
      AND b.codemp = a.codemp
      AND b.unineg = xunineg
      AND b.codpro = xcodpro
      AND b.estado = "V"
      AND b.tipope = 1;

   -- Seleccionando salidas
   SELECT NVL(SUM(b.canuni),0),NVL(SUM(b.canlbs),0)
    INTO  xsaluni,xsallbs
    FROM  inc_mtransac a,inc_dtransac b
    WHERE a.lnktra = b.lnktra
      AND a.fecemi >= TO_DATE(xfechai,'%d/%m/%Y')
      AND a.fecemi <= xfecfin
      AND b.codemp = a.codemp
      AND b.unineg = xunineg
      AND b.codpro = xcodpro
      AND b.estado = "V"
      AND b.tipope = 0;

   --Calculando saldos
   LET xsaldun = (xiniuni + xentuni - xsaluni);
   LET xsaldlb = (xinilbs + xentlbs - xsallbs);

   -- Insertando datos del amanecer x region
   INSERT INTO inc_saldxsem
   VALUES (xunineg,wanio,wnumsem,
           xregion,xfecini,xfecfin,
           xcodpro,xsaldun,xsaldlb,
           0);
  END FOREACH;
 END FOREACH;
END PROCEDURE;

CREATE PROCEDURE "sistemas".spcosteoproductos(wcodemp SMALLINT,wunineg SMALLINT,
 waniotr SMALLINT,wmestra SMALLINT)

 DEFINE xcodemp  LIKE inc_maesaldo.codemp;
 DEFINE wcodcos  LIKE inc_paramtrs.codcos;
 DEFINE wcodini  LIKE inc_paramtrs.codini;
 DEFINE wcodfis  LIKE inc_paramtrs.codini;
 DEFINE spcditem LIKE inc_maesaldo.cditem;
 DEFINE spcospro LIke inc_dtransac.preuni;
 DEFINE spcanuni LIke inc_maesaldo.canuni;
 DEFINE spcanlbs LIKE inc_maesaldo.canlbs;
 DEFINE sptotpro LIke inc_maesaldo.totpro;
 DEFINE scanuni  LIke inc_maesaldo.canuni;
 DEFINE scanlbs  LIke inc_maesaldo.canlbs;
 DEFINE stotpro  LIke inc_maesaldo.totpro;
 DEFINE ecanuni  LIke inc_maesaldo.canuni;
 DEFINE ecanlbs  LIke inc_maesaldo.canlbs;
 DEFINE etotpro  LIke inc_maesaldo.totpro;
 DEFINE splnktra LIKE inc_dtransac.lnktra;
 DEFINE spnumcor LIKE inc_dtransac.numcor;
 DEFINE sppreuni LIKE inc_maesaldo.preuni;
 DEFINE spopeval LIKE inc_dtransac.opeval;
 DEFINE sptipope LIKE inc_dtransac.tipope;
 DEFINE spcodori LIKE inc_dtransac.codori;
 DEFINE xcanuni  LIke inc_maesaldo.canuni;
 DEFINE xcanlbs  LIke inc_maesaldo.canlbs;
 DEFINE xtotpro  LIke inc_maesaldo.totpro;
 DEFINE sfisuni  LIke inc_maesaldo.canlbs;
 DEFINE sfislbs  LIke inc_maesaldo.canlbs;
 DEFINE stotfis  LIke inc_maesaldo.totpro;
 DEFINE spunimed SMALLINT;
 DEFINE wtipsrv  SMALLINT;

 -- Mynor Ramirez
 -- Enero 2009
 -- Costeo de productos
 -- SET DEBUG FILE TO "trace.out" ;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ;

 -- Clasificacion del costeo
 SELECT a.codcos,a.codini,a.codfis
  INTO  wcodcos,wcodini,wcodfis
  FROM  inc_paramtrs a;

 -- Verificando si costeo ya existe
 LET xcodemp = NULL;
 SELECT UNIQUE a.codemp
  INTO  xcodemp
  FROM inc_maesaldo a
  WHERE a.codemp = wcodemp
    AND a.unineg = wunineg
    AND a.cditem IS NOT NULL
    AND a.codval = wcodcos
    AND a.aniotr = waniotr
    AND a.mestra = wmestra;

 -- Si ya existe el costeo lo elimina
 IF xcodemp IS NOT NULL THEN
    -- Eliminado costeo
    DELETE FROM inc_maesaldo
    WHERE inc_maesaldo.codemp = wcodemp
      AND inc_maesaldo.unineg = wunineg
      AND inc_maesaldo.cditem IS NOT NULL
      AND inc_maesaldo.codval = wcodcos
      AND inc_maesaldo.aniotr = waniotr
      AND inc_maesaldo.mestra = wmestra;
 END IF;

 -- Inicializando datos
 LET spcodori = 0;
 LET spcospro = 0;
 LET spcanuni = 0;
 LET spcanlbs = 0;
 LET sptotpro = 0;
 LET scanuni  = 0;
 LET scanlbs  = 0;
 LET stotpro  = 0;
 LET ecanuni  = 0;
 LET ecanlbs  = 0;
 LET etotpro  = 0;
 LET xcanuni  = 0;
 LET xcanlbs  = 0;
 LET xtotpro  = 0;

 -- Costeando productos
 FOREACH SELECT a.producto,
                a.mca_unid_libr
          INTO  spcditem,
                spunimed
          FROM  producto a
          WHERE a.acumulado IN (1,2)
          ORDER BY a.producto

  -- Totalizando saldo inicial
  SELECT NVL(SUM(a.canuni),0),
         NVL(SUM(a.canlbs),0),
         NVL(SUM(a.totpro),0)
   INTO  scanuni,
         scanlbs,
         stotpro
   FROM  inc_maesaldo a
   WHERE a.codemp = wcodemp
     AND a.unineg = wunineg
     AND a.cditem = spcditem
     AND a.codval = wcodini
     AND a.aniotr = waniotr
     AND a.mestra = wmestra;

  -- Totalizando entradas
  SELECT NVL(SUM(a.canuni),0),
         NVL(SUM(a.canlbs),0),
         NVL(SUM(a.totpro),0)
   INTO  ecanuni,
         ecanlbs,
         etotpro
   FROM  inc_dtransac a,ind_tipomovs b
   WHERE a.codemp = wcodemp
     AND a.unineg = wunineg
     AND a.aniotr = waniotr
     AND a.mestra = wmestra
     AND a.codpro = spcditem
     AND a.estado = "V"
     AND a.tipope = 1
     AND b.tipmov = a.tipmov
     AND b.subcls IN ("P");

  -- Obteniendo costo de almacenes externos
  CALL fxcosteoexterno(wcodemp,wunineg,waniotr,wmestra,spcditem)
  RETURNING xcanuni,xcanlbs,xtotpro;

  -- Totalizando entradas + saldo inicial
  LET spcanuni = (scanuni+ecanuni+xcanuni);
  LET spcanlbs = (scanlbs+ecanlbs+xcanlbs);
  LET sptotpro = (stotpro+etotpro+xtotpro);

  -- Verificando unidad de medida
  LET spcospro = 0;
  IF (spunimed=1) THEN -- Unidades
     IF (spcanuni>0) THEN
        LET spcospro = (sptotpro/spcanuni);
     END IF;
  END IF;
  IF (spunimed=2) THEN -- Libras
     IF (spcanlbs>0) THEN
        LET spcospro = (sptotpro/spcanlbs);
     END IF;
  END IF;
  IF (spunimed=3) THEN -- Libras y Unidades
     IF (spcanlbs>0) THEN
        LET spcospro = (sptotpro/spcanlbs);
     END IF;
  END IF;

  -- Verificando si producto tiene costo
  IF (spcospro=0) THEN
     CONTINUE FOREACH;
  END IF;

  -- Insertando costeo x producto
  INSERT INTO inc_maesaldo
  VALUES (wcodemp,
          wunineg,
          spcditem,
          wcodcos,
          spcanuni,
          spcanlbs,
          spcospro,
          sptotpro,
          waniotr,
          wmestra,
          USER,
          TODAY,
          CURRENT);

  -- Costeando inventario fisico
  -- Buscando inventario fisico
  SELECT NVL(a.canuni,0),NVL(a.canlbs,0)
   INTO  sfisuni,sfislbs
   FROM  inc_maesaldo a
   WHERE a.codemp = wcodemp
     AND a.unineg = wunineg
     AND a.cditem = spcditem
     AND a.codval = wcodfis
     AND a.aniotr = waniotr
     AND a.mestra = wmestra;

  -- Costeando inventario fisico
  -- Verificando unidad de medida
  IF (spunimed=1) THEN -- Unidades
     LET stotfis = (spcospro*sfisuni);
  END IF;
  IF (spunimed=2) THEN -- Libras
     LET stotfis = (spcospro*sfislbs);
  END IF;
  IF (spunimed=3) THEN -- Libras y Unidades
     LET stotfis = (spcospro*sfislbs);
  END IF;

  -- Costeando inventario fisico del mes
  UPDATE inc_maesaldo
  SET    inc_maesaldo.preuni = spcospro,
         inc_maesaldo.totpro = stotfis
  WHERE  inc_maesaldo.codemp = wcodemp
    AND  inc_maesaldo.unineg = wunineg
    AND  inc_maesaldo.cditem = spcditem
    AND  inc_maesaldo.codval = wcodfis
    AND  inc_maesaldo.aniotr = waniotr
    AND  inc_maesaldo.mestra = wmestra;  
 END FOREACH;

 -- Costeando salidas de producto e ingresos de productos
 -- que no sean tipos de movimiento de costeo
 FOREACH SELECT a.lnktra,
                a.codori,
                a.cditem,
                a.numcor,
                a.canuni,
                a.canlbs,
                a.preuni,
                a.totpro,
                a.opeval,
                a.tipope,
                c.mca_unid_libr
           INTO splnktra,
                spcodori,
                spcditem,
                spnumcor,
                spcanuni,
                spcanlbs,
                sppreuni,
                sptotpro,
                spopeval,
                sptipope,
                spunimed
          FROM  inc_dtransac a,producto c,ind_tipomovs b
          WHERE a.codemp   = wcodemp
            AND a.unineg   = wunineg
            AND a.aniotr   = waniotr
            AND a.mestra   = wmestra
            AND a.codpro   IS NOT NULL
            AND a.estado   = "V"
            AND a.tipope   IN (1,0)
            AND b.tipmov   = a.tipmov
            AND b.subcls   NOT IN ("P","X")
            AND c.producto = a.cditem

  -- Inicializando precio de costo
  LET sppreuni = 0;

  -- Obtiene costo de la unidad de negocio
  LET sppreuni = (SELECT NVL(a.preuni,0)
                   FROM  inc_maesaldo a
                   WHERE a.codemp = wcodemp
                     AND a.unineg = wunineg
                     AND a.cditem = spcditem
                     AND a.codval = wcodcos
                     AND a.aniotr = waniotr
                     AND a.mestra = wmestra);

  -- Verificando costo
  IF sppreuni IS NULL THEN
     LET sppreuni = 0;
  END IF;

  -- Costeando
  -- Verificando unidad de medida
  IF (spunimed=1) THEN -- Unidades
     LET sptotpro = (sppreuni*spcanuni);
  END IF;
  IF (spunimed=2) THEN -- Libras
     LET sptotpro = (sppreuni*spcanlbs);
  END IF;
  IF (spunimed=3) THEN -- Libras y Unidades
     LET sptotpro = (sppreuni*spcanlbs);
  END IF;

  -- Verificando tipo de movimiento
  IF (sptipope=1) THEN                    -- Ingresos
     LET spopeval = sptotpro;
  ELSE
     LET spopeval = (sptotpro*(-1));      -- Salidas
  END IF;

  -- Actualizando precio y valor total de las transacciones
  UPDATE inc_dtransac
  SET    inc_dtransac.totpro = sptotpro,
         inc_dtransac.opeval = spopeval,
         inc_dtransac.preuni = sppreuni
  WHERE  inc_dtransac.lnktra = splnktra
    AND  inc_dtransac.cditem = spcditem
    AND  inc_dtransac.numcor = spnumcor;
 END FOREACH;
END PROCEDURE;


 


 


 

grant  execute on function "informix".checksum (lvarchar,integer) to "public" as "informix";
grant  execute on function "informix".checksum (integer,integer) to "public" as "informix";
grant  execute on function "informix".checksum (smallint,integer) to "public" as "informix";
grant  execute on function "informix".checksum (references byte,integer) to "public" as "informix";
grant  execute on function "informix".checksum (references text,integer) to "public" as "informix";
grant  execute on function "informix".checksum (int8,integer) to "public" as "informix";
grant  execute on function "informix".checksum (decimal,integer) to "public" as "informix";
grant  execute on function "informix".checksum (money,integer) to "public" as "informix";
grant  execute on function "informix".checksum (float,integer) to "public" as "informix";
grant  execute on function "informix".checksum (smallfloat,integer) to "public" as "informix";
grant  execute on function "informix".checksum (date,integer) to "public" as "informix";
grant  execute on function "informix".checksum (datetime,integer) to "public" as "informix";
grant  execute on function "informix".checksum (interval,integer) to "public" as "informix";
grant  execute on function "informix".checksum (set,integer) to "public" as "informix";
grant  execute on function "informix".checksum (multiset,integer) to "public" as "informix";
grant  execute on function "informix".checksum (list,integer) to "public" as "informix";
grant  execute on function "informix".checksum (row,integer) to "public" as "informix";
grant  execute on function "sistemas".spindpedidos (smallint,smallint,date,char) to "public" as "sistemas";
grant  execute on function "sistemas".spincremisiones (date,date,smallint,smallint) to "public" as "sistemas";
grant  execute on function "sistemas".diariodecamaras22 (smallint,char,date,decimal) to "public" as "sistemas";
grant  execute on procedure "sistemas".spinddtransac1 (smallint,smallint,smallint,char,char,char,char,char,date,smallint) to "public" as "sistemas";
grant  execute on procedure "sistemas".spindpdpedido1 (integer) to "public" as "sistemas";
grant  execute on procedure "sistemas".spindusuaxest1 (integer,integer) to "public" as "sistemas";
grant  execute on procedure "sistemas".spsaldosinvpollo (smallint,smallint,smallint,char,smallint,smallint,smallint) to "public" as "sistemas";
grant  execute on procedure "sistemas".sptrasladosalmacen (integer) to "public" as "sistemas";
grant  execute on function "sistemas".sptrasladosxalmacen (integer,integer,date,date) to "public" as "sistemas";
grant  execute on function "sistemas".fxinventariofisico (smallint,smallint,char,smallint,smallint) to "public" as "sistemas";
grant  execute on function "sistemas".fxsaldoteorico (smallint,smallint,char,smallint,smallint) to "public" as "sistemas";
grant  execute on procedure "sistemas".spsaldosxproducto (smallint,smallint,smallint,char,smallint,smallint,smallint) to "public" as "sistemas";
grant  execute on function "sistemas".fxcosteoexterno (smallint,smallint,smallint,smallint,char) to "public" as "sistemas";
grant  execute on procedure "sistemas".spanlautoalmacen (integer) to "public" as "sistemas";
grant  execute on function "sistemas".spamanecernacional (integer,integer) to "public" as "sistemas";
grant  execute on function "sistemas".fxvalorcosteo (smallint,smallint,smallint,smallint,smallint,char,smallint,decimal,decimal) to "public" as "sistemas";
grant  execute on procedure "sistemas".spabsorciones (smallint,smallint,smallint,smallint) to "public" as "sistemas";
grant  execute on procedure "sistemas".spamanecerxregion (integer,integer) to "public" as "sistemas";
grant  execute on procedure "sistemas".spcosteoproductos (smallint,smallint,smallint,smallint) to "public" as "sistemas";

revoke usage on language SPL from public ;

grant usage on language SPL to public ;


create view "sistemas".vta_creditos (codemp,codcli) as 
  select x0.empresa_d ,x0.cliente from distribu:"sistemas".creditos 
    x0 ;    
create view "sistemas".vta_rutasvta (codemp,noruta,codsec,codven) as 
  select x0.empresa ,x0.ruta ,x0.seccion ,x0.agente from distribu:
    "sistemas".rutas x0 ;                                     
      
create view "sistemas".ind_mespecipre (codcli,cditem,tippre,fecini,fecfin) as 
  select x0.cliente ,x0.producto ,x0.tipo_precio ,x0.fecha_ini 
    ,x0.fecha_fin from distribu:"sistemas".especipre x0 ;
create view "sistemas".ind_mprecios (cditem,tippre,precio,fecsis,nlogin) as 
  select x0.producto ,x0.tipo ,x0.precio ,x0.fecha ,x0.usuario 
    from distribu:"sistemas".precios x0 ;                   
create view "sistemas".vta_vendedrs (codemp,noruta,codven,codsup,venepl,nomven,supepl,nomsup) as 
  select x0.empresa ,x0.ruta ,x0.agente ,x0.supventas ,x0.empleado 
    ,x1.nombre ,x3.empleado ,x3.nombre from distribu:"sistemas"
    .agentes x0 ,distribu:"sistemas".empleados x1 ,distribu:"sistemas"
    .superven x2 ,distribu:"sistemas".empleados x3 where ((((((x2.empresa 
    = x0.empresa ) AND (x2.supventas = x0.supventas ) ) AND (x1.empresa 
    = x0.empresa ) ) AND (x1.empleado = x0.empleado ) ) AND (x3.empresa 
    = x2.empresa ) ) AND (x3.empleado = x2.empleado ) ) ;    
                               
create view "sistemas".vta_clientes (codcli,nomcli,dircli,noruta,tippre,estado) as 
  select distinct x0.codcli ,x0.nomcli ,x0.dircli ,x0.noruta ,
    x0.tippre ,x0.estado from distribu:"sistemas".fac_clientes 
    x0 ;                                                   
create view "sistemas".vwreportemermas (unineg,nomneg,codemp,nomemp,tipmov,nommov,codpro,dsitem,codcat,nomcat,subcat,subdes,fecemi,canlbs) as 
  select distinct x2.unineg ,x7.nomneg ,x2.codemp ,x8.abrnom ,
    x0.tipmov ,x6.nommov ,x0.codpro ,x1.nombre ,x5.codcat  ,x3.nomcat 
    ,x5.subcat ,x4.subdes ,x2.fecemi ,sum(x0.canlbs ) from "sistemas"
    .inc_dtransac x0 ,distribu@srvrbhntg01_tcp:"sistemas".producto 
    x1 ,"sistemas".inc_mtransac x2 ,"sistemas".inc_tipcateg x3 
    ,"sistemas".inc_subcateg x4 ,"sistemas".inc_prodcats x5 ,"sistemas"
    .ind_tipomovs x6 ,"sistemas".ind_unegocio x7 ,globales:"sistemas"
    .glb_empresas x8 where (((((((((((x2.lnktra = x0.lnktra ) 
    AND (x8.codemp = x2.codemp ) ) AND (x7.unineg = x2.unineg 
    )  ) AND (x2.estado = 'V' ) ) AND (x1.producto = x0.codpro 
    ) ) AND  (x0.cditem = x5.cditem ) ) AND (x3.codcat = x5.codcat 
    ) ) AND (x4.codcat = x5.codcat ) ) AND (x4.subcat = x5.subcat 
    ) ) AND (x6.tipmov = x2.tipmov ) ) AND (x6.movcls = 'M' ) 
    ) group by x2.unineg ,x7.nomneg ,x2.codemp ,x8.abrnom ,x0.tipmov 
    ,x6.nommov ,x0.codpro ,x1.nombre ,x5.codcat ,x3.nomcat ,x5.subcat 
    ,x4.subdes ,x2.fecemi ;                                  
                           
create view "sistemas".vwproductossinmovimiento (unineg,nomneg,codemp,nomemp,tipneg,region,aniotr,mestra,codpro,dsitem,codcat,nomcat,subcat,subdes,unimed,codval,canuni,canlbs,totpro,cospro,unipol) as 
  select x5.unineg ,x5.nomneg ,x6.codemp ,x6.abrnom ,x5.tipneg 
    ,x5.region ,x0.aniotr ,x0.mestra ,x1.producto ,x1.nombre 
    ,x4.codcat ,x3.nomcat ,x4.subcat ,x4.subdes ,CASE WHEN (x1.mca_unid_libr 
    = 1 )  THEN 'UNI'  WHEN (x1.mca_unid_libr = 2 )  THEN 'LBS'
      WHEN (x1.mca_unid_libr = 3 )  THEN 'U&L'  END ,x0.codval 
    ,x0.canuni ,x0.canlbs ,x0.totpro ,CASE WHEN ((x1.mca_unid_libr 
    = 1  ) AND (x0.canuni > 0.00 ) )  THEN (x0.totpro / x0.canuni 
    )  WHEN ((x1.mca_unid_libr = 2 ) AND (x0.canlbs > 0.00 ) 
    )  THEN (x0.totpro / x0.canlbs )  WHEN ((x1.mca_unid_libr 
    = 3 ) AND (x0.canlbs > 0.00 ) )  THEN (x0.totpro / x0.canlbs 
    )  END ,0 from "sistemas".inc_maesaldo x0 ,distribu:"sistemas"
    .producto x1 ,"sistemas".inc_prodcats x2 ,"sistemas".inc_tipcateg 
    x3 ,"sistemas".inc_subcateg x4 ,"sistemas".ind_unegocio x5 
    ,globales:"sistemas".glb_empresas x6 where (((((((((x0.codval 
    = 1 ) AND (x1.producto = x0.cditem ) ) AND (x5.unineg = x0.unineg 
    ) ) AND (x6.codemp = x0.codemp ) ) AND (x2.cditem = x0.cditem 
    ) ) AND (x3.codcat = x2.codcat ) ) AND (x4.codcat = x2.codcat 
    ) ) AND (x4.subcat = x2.subcat ) ) AND NOT EXISTS (select 
    x7.lnktra ,x7.unineg ,x7.codemp ,x7.tipmov ,x7.codori ,x7.coddes 
    ,x7.codbod ,x7.cditem ,x7.numdoc ,x7.aniotr ,x7.mestra ,x7.fecpro 
    ,x7.bodpro ,x7.bodori ,x7.codpro ,x7.codest ,x7.codesp ,x7.tippes 
    ,x7.pstara ,x7.pbruto ,x7.canuni  ,x7.canlbs ,x7.preuni ,
    x7.totpro ,x7.pespro ,x7.fecven ,x7.numcor ,x7.barcod ,x7.tipope 
    ,x7.estado ,x7.opeuni ,x7.opelbs ,x7.opeval ,x7.nlogin ,x7.fecsis 
    ,x7.horsis from "sistemas".inc_dtransac x7 where ((((((x7.codemp 
    = x0.codemp ) AND (x7.unineg = x0.unineg ) ) AND (x7.aniotr 
    = x0.aniotr ) ) AND (x7.mestra = x0.mestra ) ) AND (x7.codpro 
    = x0.cditem ) ) AND (x7.estado = 'V' ) ) )  ) ;           
                                                       
create view "sistemas".vwreportesaldosxalmacen (unineg,nomneg,codemp,nomemp,tipneg,region,aniotr,mestra,codpro,dsitem,codcat,nomcat,subcat,subdes,unimed,codval,canuni,canlbs,totpro,cospro,unipol) as 
  select x5.unineg ,x5.nomneg ,x6.codemp ,x6.abrnom ,x5.tipneg 
    ,x5.region ,x0.aniotr ,x0.mestra ,x1.producto ,x1.nombre 
    ,x4.codcat ,x3.nomcat ,x4.subcat ,x4.subdes ,CASE WHEN (x1.mca_unid_libr 
    = 1 )  THEN 'UNI'  WHEN (x1.mca_unid_libr = 2 )  THEN 'LBS'
      WHEN (x1.mca_unid_libr = 3 )  THEN 'U&L'  END ,x0.codval 
    ,x0.canuni ,x0.canlbs ,x0.totpro ,CASE WHEN ((x1.mca_unid_libr 
    = 1 ) AND (x0.canuni > 0.00 ) )  THEN (x0.totpro / x0.canuni 
    )  WHEN  ((x1.mca_unid_libr = 2 ) AND (x0.canlbs > 0.00 ) 
    )  THEN (x0.totpro / x0.canlbs )  WHEN ((x1.mca_unid_libr 
    = 3 ) AND (x0.canlbs  > 0.00 ) )  THEN (x0.totpro / x0.canlbs 
    )  END ,0 from "sistemas".inc_maesaldo x0 ,distribu:"sistemas"
    .producto x1 ,"sistemas".inc_prodcats x2 ,"sistemas".inc_tipcateg 
    x3 ,"sistemas".inc_subcateg x4 ,"sistemas".ind_unegocio x5 
    ,globales:"sistemas".glb_empresas x6 where (((((((x1.producto 
    = x0.cditem ) AND (x5.unineg =  x0.unineg ) ) AND (x6.codemp 
    = x0.codemp ) ) AND (x0.cditem = x2.cditem ) ) AND (x3.codcat 
    = x2.codcat ) ) AND (x4.codcat = x2.codcat ) ) AND (x4.subcat 
    = x2.subcat ) ) ;                       
create view "sistemas".vwsaldosxalmacennacional (unineg,nomneg,codemp,nomemp,tipneg,region,aniotr,mestra,codpro,dsitem,codcat,nomcat,subcat,subdes,unimed,codval,canuni,canlbs,totpro,cospro,unipol) as 
  select x0.unineg ,x0.nomneg ,x0.codemp ,x0.nomemp ,x0.tipneg 
    ,x0.region ,x0.aniotr ,x0.mestra ,x0.codpro ,x0.dsitem ,x0.codcat 
    ,x0.nomcat ,x0.subcat ,x0.subdes ,x0.unimed ,x0.codval ,x0.canuni 
    ,x0.canlbs ,x0.totpro ,x0.cospro ,x0.unipol from invpdi@srvbkhntg01_tcp:
    "sistemas".vwreportesaldosxalmacen x0  union select x1.unineg 
    ,x1.nomneg ,x1.codemp ,x1.nomemp ,x1.tipneg ,x1.region ,x1.aniotr 
    ,x1.mestra ,x1.codpro ,x1.dsitem ,x1.codcat ,x1.nomcat ,x1.subcat 
    ,x1.subdes ,x1.unimed ,x1.codval ,x1.canuni ,x1.canlbs ,x1.totpro 
    ,x1.cospro ,x1.unipol from invpdi@sps_tcp:"sistemas".vwreportesaldosxalmacen 
    x1  union select x2.unineg ,x2.nomneg ,x2.codemp ,x2.nomemp 
    ,x2.tipneg ,x2.region ,x2.aniotr ,x2.mestra ,x2.codpro ,x2.dsitem 
    ,x2.codcat ,x2.nomcat ,x2.subcat ,x2.subdes ,x2.unimed ,x2.codval 
    ,x2.canuni ,x2.canlbs ,x2.totpro ,x2.cospro ,x2.unipol from 
    "sistemas".vwreportesaldosxalmacen x2 ;                   
                                             
create view "sistemas".vwproductossinmovimientonacional (unineg,nomneg,codemp,nomemp,tipneg,region,aniotr,mestra,codpro,dsitem,codcat,nomcat,subcat,subdes,unimed,codval,canuni,canlbs,totpro,cospro,unipol) as 
  select x0.unineg ,x0.nomneg ,x0.codemp ,x0.nomemp ,x0.tipneg 
    ,x0.region ,x0.aniotr ,x0.mestra ,x0.codpro ,x0.dsitem ,x0.codcat 
    ,x0.nomcat ,x0.subcat ,x0.subdes ,x0.unimed ,x0.codval ,x0.canuni 
    ,x0.canlbs ,x0.totpro ,x0.cospro ,x0.unipol from invpdi@srvbkhntg01_tcp:
    "sistemas".vwproductossinmovimiento x0   union select x1.unineg 
    ,x1.nomneg ,x1.codemp ,x1.nomemp ,x1.tipneg ,x1.region ,x1.aniotr 
    ,x1.mestra ,x1.codpro ,x1.dsitem ,x1.codcat ,x1.nomcat ,x1.subcat 
    ,x1.subdes ,x1.unimed ,x1.codval ,x1.canuni ,x1.canlbs ,x1.totpro 
    ,x1.cospro ,x1.unipol from invpdi@sps_tcp:"sistemas".vwproductossinmovimiento 
    x1  union select x2.unineg ,x2.nomneg ,x2.codemp ,x2.nomemp 
    ,x2.tipneg ,x2.region ,x2.aniotr ,x2.mestra ,x2.codpro ,x2.dsitem 
    ,x2.codcat ,x2.nomcat  ,x2.subcat ,x2.subdes ,x2.unimed ,
    x2.codval ,x2.canuni ,x2.canlbs ,x2.totpro ,x2.cospro ,x2.unipol 
    from "sistemas".vwproductossinmovimiento x2 ;             
                                        
create view "sistemas".vwglbsemanas (numper,anosem,numsem,fcisem,fcfsem) as 
  select ((x0.anosem || ' - Sem ' ) || x0.numsem ) ,x0.anosem ,
    x0.numsem ,x0.fcisem ,x0.fcfsem from globales:"sistemas".glb_semanacl 
    x0 ;                                              
create view "sistemas".vwinvamanecerlitoral (unineg,anosem,numsem,region,codpro,canuni,canupl,canlbs) as 
  select x1.unineg ,x1.anosem ,x1.numsem ,x1.region ,x0.cditem 
    ,x1.canuni ,x1.canupl ,x1.canlbs from "sistemas".inc_proinvam 
    x0 ,"sistemas".inc_saldxsem  x1 ,globales:"sistemas".glb_semanacl 
    x2 where (((x1.codpro = x0.cditem ) AND (x2.numsem = x1.numsem 
    ) ) AND (x2.anosem = x1.anosem ) ) ;                     
                                        

grant select on "sistemas".vta_creditos to "public" as "sistemas";
grant select on "sistemas".vta_rutasvta to "public" as "sistemas";
grant select on "sistemas".ind_mespecipre to "puiblic" as "sistemas";
grant select on "sistemas".ind_mespecipre to "public" as "sistemas";
grant select on "sistemas".ind_mprecios to "public" as "sistemas";
grant select on "sistemas".vta_vendedrs to "public" as "sistemas";
grant select on "sistemas".vta_clientes to "public" as "sistemas";
grant select on "sistemas".vwreportemermas to "public" as "sistemas";
grant select on "sistemas".vwproductossinmovimiento to "public" as "sistemas";
grant select on "sistemas".vwreportesaldosxalmacen to "public" as "sistemas";
grant select on "sistemas".vwsaldosxalmacennacional to "public" as "sistemas";
grant select on "sistemas".vwproductossinmovimientonacional to "public" as "sistemas";
grant select on "sistemas".vwinvamanecerlitoral to "public" as "sistemas";


create index "sistemas".idxincmtransac_1 on "sistemas".inc_mtransac 
    (lnktra,unineg,fecemi,estado) using btree  in invcmdbs ;
create index "sistemas".idxincmtransac_2 on "sistemas".inc_mtransac 
    (lnktra,unineg,codemp,coddes,numrem,lnktrl) using btree  
    in invcmdbs ;
create index "sistemas".idxincmtransac_3 on "sistemas".inc_mtransac 
    (lnktra,fecemi) using btree  in invcmdbs ;
create unique index "sistemas".ix194_1 on "sistemas".inc_invcierr 
    (lnkcrr) using btree  in idxsdbs ;
alter table "sistemas".inc_invcierr add constraint primary key 
    (lnkcrr) constraint "sistemas".pkincinvcierr  ;
create index "sistemas".idxincprodcats1 on "sistemas".inc_prodcats 
    (codcat,subcat,cditem) using btree  in idxsdbs ;
create unique index "sistemas".ix199_1 on "sistemas".inc_histcrrs 
    (lnkhst) using btree  in idxsdbs ;
alter table "sistemas".inc_histcrrs add constraint primary key 
    (lnkhst) constraint "sistemas".pkinchistcrrs  ;
create index "sistemas".idxincdtransac3 on "sistemas".inc_dtransac 
    (codemp,unineg,aniotr,mestra,codpro,estado) using btree  
    in idxsdbs ;
create index "sistemas".idxincdtransac_1 on "sistemas".inc_dtransac 
    (lnktra,unineg,codemp,codori,coddes,numdoc,codpro) using 
    btree  in idxsdbs ;
create index "sistemas".idxincdtransac_2 on "sistemas".inc_dtransac 
    (canuni,canlbs) using btree  in idxsdbs ;
create index "sistemas".idxincdtransac_3 on "sistemas".inc_dtransac 
    (opeuni,opelbs) using btree  in idxsdbs ;
create index "sistemas".idxincdtransac_6 on "sistemas".inc_dtransac 
    (codemp,unineg,codpro,estado,tipope) using btree  in invcmdbs 
    ;
create index "sistemas".idxincdtranssac_4 on "sistemas".inc_dtransac 
    (codemp,unineg,aniotr,mestra,codpro,estado,tipope) using 
    btree  in idxsdbs ;
create index "sistemas".idxincdtranssac_5 on "sistemas".inc_dtransac 
    (lnktra,cditem,numcor) using btree  in idxsdbs ;
create index "sistemas".idxincmaesaldo_1 on "sistemas".inc_maesaldo 
    (codemp,unineg,cditem,codval,aniotr,mestra) using btree  
    in invcmdbs ;
create unique index "sistemas".ix228_1 on "sistemas".inc_numtrans 
    (numdoc) using btree  in idxsdbs ;
create unique index "sistemas".ix248_1 on "sistemas".inc_corelrpt 
    (numcor) using btree  in invcmdbs ;
create index "sistemas".idxincsaldxsem_1 on "sistemas".inc_saldxsem 
    (anosem,numsem) using btree  in invcmdbs ;

alter table "sistemas".ind_parmxemp add constraint (foreign key 
    (codemp,tipmov) references "sistemas".ind_tipomovs  constraint 
    "sistemas".fkindtipomovs2);

alter table "sistemas".ind_parmxemp add constraint (foreign key 
    (unineg) references "sistemas".ind_unegocio  constraint "sistemas"
    .fkindunegocio2);

alter table "sistemas".inc_invcierd add constraint (foreign key 
    (lnkcrr) references "sistemas".inc_invcierr  on delete cascade 
    constraint "sistemas".fkincinvcierd1);

alter table "sistemas".inc_maesaldo add constraint (foreign key 
    (unineg) references "sistemas".ind_unegocio  constraint "sistemas"
    .fkincmaesaldo2);

alter table "sistemas".inc_dtransac add constraint (foreign key 
    (lnktra) references "sistemas".inc_mtransac  constraint "sistemas"
    .fkincmtransac1);


create trigger "sistemas".tgelidetalleproductos delete on "sistemas"
    .inc_mtransac referencing old as pre
    for each row
        (
        delete from "sistemas".inc_dtransac  where (lnktra = 
    pre.lnktra ) );

create trigger "sistemas".trganlautoalmacen update of estado 
    on "sistemas".inc_mtransac referencing new as pos
    for each row
        (
        update "sistemas".inc_dtransac set "sistemas".inc_dtransac.estado 
    = pos.estado  where (lnktra = pos.lnktra ) );

create trigger "sistemas".tginsertaproducto insert on "sistemas"
    .inc_dtransac referencing new as pos
    for each row
        (
        execute procedure "sistemas".spsaldosxproducto(pos.codemp 
    ,pos.unineg ,pos.tipmov ,pos.cditem ,pos.aniotr ,pos.mestra ,pos.tipope 
    ));

create trigger "sistemas".tgactestadoanulado update of estado 
    on "sistemas".inc_dtransac referencing new as pos
    for each row
        (
        execute procedure "sistemas".spsaldosxproducto(pos.codemp 
    ,pos.unineg ,pos.tipmov ,pos.cditem ,pos.aniotr ,pos.mestra ,pos.tipope 
    ));

create trigger "sistemas".tgctualizacanlbs update of canlbs on 
    "sistemas".inc_dtransac referencing new as pos
    for each row
        (
        execute procedure "sistemas".spsaldosxproducto(pos.codemp 
    ,pos.unineg ,pos.tipmov ,pos.cditem ,pos.aniotr ,pos.mestra ,pos.tipope 
    ));

create trigger "sistemas".tgctualizacanuni update of canuni on 
    "sistemas".inc_dtransac referencing new as pos
    for each row
        (
        execute procedure "sistemas".spsaldosxproducto(pos.codemp 
    ,pos.unineg ,pos.tipmov ,pos.cditem ,pos.aniotr ,pos.mestra ,pos.tipope 
    ));

create trigger "sistemas".tgctualizapreciouni update of preuni 
    on "sistemas".inc_dtransac referencing new as pos
    for each row
        (
        execute procedure "sistemas".spsaldosxproducto(pos.codemp 
    ,pos.unineg ,pos.tipmov ,pos.cditem ,pos.aniotr ,pos.mestra ,pos.tipope 
    ));

create trigger "sistemas".tgcosteoproductos insert on "sistemas"
    .inc_bitprocs referencing new as pos
    for each row
        when ((pos.tippro = 1 ) )
            (
            execute procedure "sistemas".spcosteoproductos(pos.codemp 
    ,pos.unineg ,pos.aniotr ,pos.mestra )),
        when ((pos.tippro = 2 ) )
            (
            execute procedure "sistemas".spabsorciones(pos.codemp 
    ,pos.unineg ,pos.aniotr ,pos.mestra )),
        when ((pos.tippro = 3 ) )
            (
            execute procedure "sistemas".spamanecerxregion(pos.aniotr 
    ,pos.mestra ));

create trigger "sistemas".trgtrasladostranx insert on "sistemas"
    .inc_trasuneg referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptrasladosalmacen(pos.lnkori 
    ));

create trigger "sistemas".trganulatraslados update of estado 
    on "sistemas".inc_trasuneg referencing new as pos
    for each row
        when ((pos.estado = 'A' ) )
            (
            execute procedure "sistemas".spanlautoalmacen(pos.lnkori 
    ));

create trigger "sistemas".tgrregistraprocesos insert on "sistemas"
    .inp_bitprocs referencing new as a
    for each row
        when ((a.tippro = 1 ) )
            (
            execute procedure "sistemas".spamanecerxregion(a.anosem 
    ,a.numsem ));



update statistics high for table inc_dtransac (
     codemp, codpro, estado, tipope, unineg) 
     resolution   0.50000 ;
update statistics high for table inc_maesaldo (
     aniotr, cditem, codemp, codval, mestra, 
     unineg) 
     resolution   0.50000 ;
update statistics high for table inc_mtransac (
     fecemi, lnktra) 
     resolution   0.50000 ;
update statistics high for table inc_prodcats (
     cditem, codcat, subcat) 
     resolution   0.50000 ;

 


