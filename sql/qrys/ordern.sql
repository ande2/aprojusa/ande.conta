
DBSCHEMA Schema Utility       INFORMIX-SQL Version 10.00.UC1    
Copyright IBM Corporation 1996, 2004 All rights reserved
Software Serial Number AAA#B000000
{ TABLE "sistemas".inv_ordentra row size = 401 number of columns = 25 index size 
              = 9 }
create table "sistemas".inv_ordentra 
  (
    lnkord serial not null ,
    lnktra integer not null ,
    codcli smallint not null ,
    fecord date not null ,
    subcat smallint not null ,
    codcol smallint not null ,
    cantid decimal(12,2) not null ,
    xlargo decimal(5,2) not null ,
    yancho decimal(5,2) not null ,
    observ char(100),
    premed decimal(9,6) not null ,
    totord decimal(14,2) not null ,
    totabo decimal(14,2) not null ,
    salord decimal(14,2) not null ,
    fecofe date not null ,
    fecent date not null ,
    doctos char(100) not null ,
    obspre char(100),
    precio smallint,
    tipord smallint not null ,
    estado char(1) not null ,
    nofase smallint not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  );
revoke all on "sistemas".inv_ordentra from "public";



create unique index "sistemas".ix189_1 on "sistemas".inv_ordentra 
    (lnkord) using btree ;



