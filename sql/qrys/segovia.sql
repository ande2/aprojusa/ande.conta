
DBSCHEMA Schema Utility       INFORMIX-SQL Version 10.00.UC1    
Copyright IBM Corporation 1996, 2004 All rights reserved
Software Serial Number AAA#B000000
{ TABLE "sistemas".glb_categors row size = 78 number of columns = 6 index size = 
              14 }
create table "sistemas".glb_categors 
  (
    codcat smallint not null ,
    nomcat varchar(50,1) not null ,
    codabr char(2) not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (codabr)  constraint "sistemas".uqinvcategpro,
    primary key (codcat)  constraint "sistemas".pkinvcategpro
  );
revoke all on "sistemas".glb_categors from "public";

{ TABLE "sistemas".inv_colorpro row size = 78 number of columns = 6 index size = 
              14 }
create table "sistemas".inv_colorpro 
  (
    codcol smallint not null ,
    nomcol varchar(50,1) not null ,
    codabr char(2) not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (codabr)  constraint "sistemas".uqinvcolorpro,
    primary key (codcol)  constraint "sistemas".pkinvcolorpro
  );
revoke all on "sistemas".inv_colorpro from "public";

{ TABLE "sistemas".inv_tipomovs row size = 86 number of columns = 8 index size = 
              0 }
create table "sistemas".inv_tipomovs 
  (
    tipmov smallint not null ,
    nommov varchar(50,1) not null ,
    tipope smallint not null ,
    nomabr char(6) not null ,
    hayval smallint not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  );
revoke all on "sistemas".inv_tipomovs from "public";

{ TABLE "sistemas".glb_programs row size = 240 number of columns = 6 index size = 
              20 }
create table "sistemas".glb_programs 
  (
    codpro char(15) not null ,
    nompro varchar(100,1) not null ,
    dcrpro varchar(100,1),
    userid char(15) not null ,
    fecis date not null ,
    horsis datetime hour to second not null ,
    primary key (codpro)  constraint "sistemas".pkglbprograms
  );
revoke all on "sistemas".glb_programs from "public";

{ TABLE "sistemas".inv_products row size = 194 number of columns = 18 index size 
              = 39 }
create table "sistemas".inv_products 
  (
    cditem serial not null ,
    codcat smallint not null ,
    subcat smallint not null ,
    codcol smallint not null ,
    codprv integer not null ,
    codmed integer not null ,
    unimed smallint not null ,
    dsitem varchar(100,1) not null ,
    codabr varchar(20,1) not null ,
    premin decimal(12,2) not null ,
    presug decimal(12,2) not null ,
    pulcom decimal(12,2) not null ,
    estado smallint not null ,
    status smallint not null ,
    fulcam date,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (premin >= 0. ) constraint "sistemas".ckinvproducts1,
    
    check (estado IN (1 ,0 )) constraint "sistemas".ckinvproducts2,
    
    check (status IN (1 ,0 )) constraint "sistemas".ckinvproducts3,
    primary key (cditem)  constraint "sistemas".pkinvproducts
  );
revoke all on "sistemas".inv_products from "public";

{ TABLE "sistemas".inv_unimedid row size = 86 number of columns = 7 index size = 
              7 }
create table "sistemas".inv_unimedid 
  (
    unimed smallint not null ,
    nommed varchar(50,1) not null ,
    nomabr char(4) not null ,
    factor decimal(9,6) not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (unimed)  constraint "sistemas".pkinvunimedid
  );
revoke all on "sistemas".inv_unimedid from "public";

{ TABLE "sistemas".glb_paramtrs row size = 288 number of columns = 6 index size = 
              0 }
create table "sistemas".glb_paramtrs 
  (
    numpar integer not null ,
    tippar integer not null ,
    valchr varchar(255,1) not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  );
revoke all on "sistemas".glb_paramtrs from "public";

{ TABLE "sistemas".inv_permxbod row size = 40 number of columns = 5 index size = 
              0 }
create table "sistemas".inv_permxbod 
  (
    codbod smallint not null ,
    userid char(15) not null ,
    usuaid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  );
revoke all on "sistemas".inv_permxbod from "public";

{ TABLE "sistemas".inv_permxtmv row size = 40 number of columns = 5 index size = 
              0 }
create table "sistemas".inv_permxtmv 
  (
    tipmov smallint not null ,
    userid char(15) not null ,
    usuaid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  );
revoke all on "sistemas".inv_permxtmv from "public";

{ TABLE "sistemas".inv_mtransac row size = 467 number of columns = 23 index size 
              = 9 }
create table "sistemas".inv_mtransac 
  (
    lnktra serial not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    tipmov smallint not null ,
    codori integer not null ,
    coddes integer not null ,
    numrf1 varchar(20),
    numrf2 varchar(20),
    aniotr smallint not null ,
    mestra smallint not null ,
    fecemi date not null ,
    tipope smallint not null ,
    observ varchar(255),
    estado char(1) not null ,
    motanl varchar(100),
    fecanl date,
    horanl datetime hour to second,
    usranl char(10),
    impres char(1) not null ,
    userid char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (lnktra)  constraint "sistemas".pkinvmtransac
  );
revoke all on "sistemas".inv_mtransac from "public";

{ TABLE "sistemas".inv_dtransac row size = 161 number of columns = 27 index size 
              = 9 }
create table "sistemas".inv_dtransac 
  (
    lnktra integer not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    tipmov smallint not null ,
    codori integer not null ,
    coddes integer not null ,
    fecemi date,
    aniotr smallint,
    mestra smallint,
    cditem integer not null ,
    codabr varchar(20,1) not null ,
    codepq smallint not null ,
    canepq decimal(14,2) not null ,
    canuni decimal(14,2) not null ,
    preuni decimal(14,6) not null ,
    totpro decimal(14,2) not null ,
    prepro decimal(14,2) not null ,
    correl smallint not null ,
    barcod varchar(20),
    tipope smallint not null ,
    estado char(1) not null ,
    opeval decimal(14,2) not null ,
    opeuni decimal(14,2) not null ,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  );
revoke all on "sistemas".inv_dtransac from "public";

{ TABLE "sistemas".glb_usuarios row size = 142 number of columns = 6 index size = 
              21 }
create table "sistemas".glb_usuarios 
  (
    userid varchar(15,1) not null ,
    nomusr varchar(50,1) not null ,
    email1 varchar(50,1),
    usuaid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (userid)  constraint "sistemas".pkglbusuarios
  );
revoke all on "sistemas".glb_usuarios from "public";

{ TABLE "sistemas".inv_mtiposal row size = 59 number of columns = 6 index size = 
              7 }
create table "sistemas".inv_mtiposal 
  (
    tipsld smallint not null ,
    destip varchar(30) not null ,
    haymov smallint,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (tipsld)  constraint "sistemas".pkinvmtiposal
  );
revoke all on "sistemas".inv_mtiposal from "public";

{ TABLE "sistemas".inv_saldopro row size = 86 number of columns = 15 index size = 
              30 }
create table "sistemas".inv_saldopro 
  (
    lnksal serial not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    tipsld smallint not null ,
    aniotr smallint not null ,
    mestra smallint not null ,
    cditem integer not null ,
    codabr varchar(20,1) not null ,
    canuni decimal(16,2) not null ,
    prepro decimal(16,6) not null ,
    totpro decimal(16,2) not null ,
    userid char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (codemp,codsuc,codbod,tipsld,aniotr,mestra,cditem)  constraint "sistemas".uqinvsaldopro,
    primary key (lnksal)  constraint "sistemas".pkinvsaldopro
  );
revoke all on "sistemas".inv_saldopro from "public";

{ TABLE "sistemas".inv_empaques row size = 66 number of columns = 7 index size = 
              7 }
create table "sistemas".inv_empaques 
  (
    codepq smallint not null ,
    nomepq varchar(30) not null ,
    cantid decimal(12,6) not null ,
    unimed smallint not null ,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (cantid > 0. ) constraint "sistemas".ckinvempaques1,
    primary key (codepq)  constraint "sistemas".pkiviempaques
  );
revoke all on "sistemas".inv_empaques from "public";

{ TABLE "sistemas".inv_epqsxpro row size = 82 number of columns = 8 index size = 
              29 }
create table "sistemas".inv_epqsxpro 
  (
    lnkepq serial not null ,
    cditem integer not null ,
    codepq smallint not null ,
    nomepq varchar(40,1) not null ,
    cantid decimal(12,6) not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (cditem,codepq)  constraint "sistemas".uqinvepqsxpro,
    primary key (lnkepq)  constraint "sistemas".pkinvepqsxpro
  );
revoke all on "sistemas".inv_epqsxpro from "public";

{ TABLE "sistemas".glb_rolesusr row size = 79 number of columns = 6 index size = 
              7 }
create table "sistemas".glb_rolesusr 
  (
    idrole smallint not null ,
    nmrole varchar(50,1) not null ,
    tprole smallint not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (tprole IN (1 ,2 ,3 )) constraint "sistemas".ckglbrolesusr1,
    primary key (idrole)  constraint "sistemas".pkglbrolesusr
  );
revoke all on "sistemas".glb_rolesusr from "public";

{ TABLE "sistemas".glb_empresas row size = 238 number of columns = 10 index size 
              = 7 }
create table "sistemas".glb_empresas 
  (
    codemp smallint not null ,
    nomemp varchar(50,1) not null ,
    nomabr varchar(12,1) not null ,
    numnit varchar(15,1) not null ,
    numtel varchar(15,1),
    numfax varchar(15,1),
    diremp varchar(100,1) not null ,
    userid char(15) not null ,
    fecsis date,
    horsis datetime hour to second,
    primary key (codemp)  constraint "sistemas".pkglbemrpesas
  );
revoke all on "sistemas".glb_empresas from "public";

{ TABLE "sistemas".glb_sucsxemp row size = 244 number of columns = 11 index size 
              = 14 }
create table "sistemas".glb_sucsxemp 
  (
    codsuc smallint not null ,
    codemp smallint not null ,
    nomsuc varchar(50,1) not null ,
    nomabr varchar(16,1) not null ,
    numnit varchar(15,1) not null ,
    numtel varchar(15,1),
    numfax varchar(15,1),
    dirsuc varchar(100,1) not null ,
    userid char(15) not null ,
    fecsis date,
    horsis datetime hour to second,
    primary key (codsuc)  constraint "sistemas".pkglbsucsxemp
  );
revoke all on "sistemas".glb_sucsxemp from "public";

{ TABLE "sistemas".inv_mbodegas row size = 97 number of columns = 10 index size = 
              21 }
create table "sistemas".inv_mbodegas 
  (
    codbod smallint not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    nombod varchar(50,1) not null ,
    nomabr varchar(12,1),
    chkexi smallint not null ,
    hayfac smallint not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (chkexi IN (1 ,0 )) constraint "sistemas".ckinvmbodegas1,
    primary key (codbod)  constraint "sistemas".pkinvmbodegas
  );
revoke all on "sistemas".inv_mbodegas from "public";

{ TABLE "sistemas".inv_proenbod row size = 103 number of columns = 15 index size 
              = 15 }
create table "sistemas".inv_proenbod 
  (
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    cditem integer not null ,
    codabr varchar(20) not null ,
    eximin decimal(14,2) not null ,
    eximax decimal(14,2) not null ,
    fulent date,
    fulsal date,
    exican decimal(14,2) not null ,
    exival decimal(14,2) not null ,
    cospro decimal(14,6) not null ,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (eximin >= 0. ) constraint "sistemas".ckinvproenbod1,
    
    check (eximax >= 0. ) constraint "sistemas".ckinvproenbod2,
    primary key (codemp,codsuc,codbod,cditem)  constraint "sistemas".pkinvproenbod
  );
revoke all on "sistemas".inv_proenbod from "public";

{ TABLE "sistemas".inv_tofisico row size = 97 number of columns = 17 index size = 
              23 }
create table "sistemas".inv_tofisico 
  (
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    aniotr smallint not null ,
    mestra smallint not null ,
    fecinv date not null ,
    cditem integer not null ,
    codabr varchar(20,1) not null ,
    codepq smallint not null ,
    canepq decimal(16,2) not null ,
    canuni decimal(16,2) not null ,
    totpro decimal(16,2) not null ,
    canexi decimal(16,2) not null ,
    finmes smallint not null ,
    userid char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (codepq >= 0 ) constraint "sistemas".ckinvtofisico1,
    
    check (canepq > 0. ) constraint "sistemas".ckinvtofisico2,
    
    check (canuni > 0. ) constraint "sistemas".ckinvtofisico3,
    
    check (totpro >= 0. ) constraint "sistemas".ckinvtofisico4,
    
    check (canexi >= 0. ) constraint "sistemas".ckinvtofisico5,
    
    check (finmes IN (1 ,0 )) constraint "sistemas".ckinvtofisico6,
    
    check (fecinv >= DATE ('01/01/2010' ) ) constraint "sistemas".ckinvtofisico7,
    primary key (codemp,codsuc,codbod,aniotr,mestra,fecinv,cditem)  constraint "sistemas".pkinvtofisico
  );
revoke all on "sistemas".inv_tofisico from "public";

{ TABLE "sistemas".inv_subcateg row size = 98 number of columns = 11 index size = 
              9 }
create table "sistemas".inv_subcateg 
  (
    codcat smallint not null ,
    subcat smallint not null ,
    nomsub varchar(50,1) not null ,
    codabr char(4) not null ,
    tipsub smallint not null ,
    lonmed smallint,
    premin decimal(9,6) not null ,
    presug decimal(9,6) not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codcat,subcat)  constraint "sistemas".pkinvsubcateg
  );
revoke all on "sistemas".inv_subcateg from "public";

{ TABLE "sistemas".inv_provedrs row size = 736 number of columns = 15 index size 
              = 23 }
create table "sistemas".inv_provedrs 
  (
    codprv integer not null ,
    nomprv varchar(100,1) not null ,
    codabr char(2) not null ,
    numnit varchar(30,1) not null ,
    numtel varchar(30,1),
    numfax varchar(30,1),
    dirprv varchar(100,1) not null ,
    nomcon varchar(50,1),
    bemail varchar(100,1),
    tipprv smallint not null ,
    codpai smallint not null ,
    observ varchar(255,1),
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (codabr)  constraint "sistemas".uqinvprovedrs,
    primary key (codprv)  constraint "sistemas".pkinvprovedrs
  );
revoke all on "sistemas".inv_provedrs from "public";

{ TABLE "sistemas".glb_mtpaises row size = 67 number of columns = 5 index size = 
              7 }
create table "sistemas".glb_mtpaises 
  (
    codpai smallint not null ,
    nompai varchar(40,1) not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codpai)  constraint "sistemas".pkglbmtpaises
  );
revoke all on "sistemas".glb_mtpaises from "public";

{ TABLE "sistemas".his_preciosm row size = 46 number of columns = 7 index size = 
              18 }
create table "sistemas".his_preciosm 
  (
    lnkpre serial not null ,
    cditem integer not null ,
    preant decimal(12,2) not null ,
    premin decimal(12,2) not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  );
revoke all on "sistemas".his_preciosm from "public";

{ TABLE "sistemas".fac_emitacre row size = 78 number of columns = 6 index size = 
              7 }
create table "sistemas".fac_emitacre 
  (
    codemi smallint not null ,
    nomemi varchar(40) not null ,
    nomabr char(12) not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codemi)  constraint "sistemas".pkfacemitacre
  );
revoke all on "sistemas".fac_emitacre from "public";

{ TABLE "sistemas".his_mtransac row size = 507 number of columns = 27 index size 
              = 9 }
create table "sistemas".his_mtransac 
  (
    lnkhis serial not null ,
    lnktra integer not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    tipmov smallint not null ,
    codori integer not null ,
    coddes integer not null ,
    numrf1 varchar(20),
    numrf2 varchar(20),
    aniotr smallint not null ,
    mestra smallint not null ,
    fecemi date not null ,
    tipope smallint not null ,
    observ varchar(255),
    estado char(1) not null ,
    motanl varchar(100),
    fecanl date,
    horanl datetime hour to second,
    usranl varchar(15),
    impres char(1) not null ,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    usract varchar(15) not null ,
    fecact date not null ,
    horact datetime hour to second not null ,
    primary key (lnkhis)  constraint "sistemas".pkhismtransac
  );
revoke all on "sistemas".his_mtransac from "public";

{ TABLE "sistemas".his_dtransac row size = 165 number of columns = 28 index size 
              = 9 }
create table "sistemas".his_dtransac 
  (
    lnkhis integer not null ,
    lnktra integer not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    tipmov smallint not null ,
    codori integer not null ,
    coddes integer not null ,
    fecemi date,
    aniotr smallint,
    mestra smallint,
    cditem integer not null ,
    codabr varchar(20,1) not null ,
    codepq smallint not null ,
    canepq decimal(14,2) not null ,
    canuni decimal(14,2) not null ,
    preuni decimal(14,6) not null ,
    totpro decimal(14,2) not null ,
    prepro decimal(14,2) not null ,
    correl smallint not null ,
    barcod varchar(20),
    tipope smallint not null ,
    estado char(1) not null ,
    opeval decimal(14,2) not null ,
    opeuni decimal(14,2) not null ,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  );
revoke all on "sistemas".his_dtransac from "public";

{ TABLE "sistemas".fac_tipodocs row size = 73 number of columns = 9 index size = 
              7 }
create table "sistemas".fac_tipodocs 
  (
    tipdoc smallint not null ,
    nomdoc varchar(40) not null ,
    nomabr char(6) not null ,
    exeimp smallint not null ,
    hayimp smallint not null ,
    numfor smallint not null ,
    userid char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (exeimp IN (0 ,1 )) constraint "sistemas".ckfactipodocs1,
    
    check (hayimp IN (0 ,1 )) constraint "sistemas".ckfactipodocs2,
    primary key (tipdoc)  constraint "sistemas".pkfactipodocs
  );
revoke all on "sistemas".fac_tipodocs from "public";

{ TABLE "sistemas".fac_puntovta row size = 77 number of columns = 10 index size = 
              7 }
create table "sistemas".fac_puntovta 
  (
    numpos smallint not null ,
    nompos varchar(40) not null ,
    chkinv smallint not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    chkexi smallint not null ,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (chkexi IN (1 ,0 )) constraint "sistemas".ckfacpuntovta1,
    
    check (chkinv IN (1 ,0 )) constraint "sistemas".ckfacpuntovta2,
    primary key (numpos)  constraint "sistemas".pkfacpuntovta
  );
revoke all on "sistemas".fac_puntovta from "public";

{ TABLE "sistemas".fac_tdocxpos row size = 48 number of columns = 10 index size = 
              30 }
create table "sistemas".fac_tdocxpos 
  (
    lnktdc serial not null ,
    numpos smallint not null ,
    codemp smallint not null ,
    tipdoc smallint not null ,
    nserie char(10) not null ,
    numcor integer,
    estado char(1) not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (numpos,codemp,tipdoc,nserie)  constraint "sistemas".uqfactdocxpos,
    
    check (estado IN ('A' ,'B' )) constraint "sistemas".ckfactdocxpos1,
    primary key (lnktdc)  constraint "sistemas".pkfactdocxpos
  );
revoke all on "sistemas".fac_tdocxpos from "public";

{ TABLE "sistemas".fac_impresol row size = 75 number of columns = 12 index size = 
              48 }
create table "sistemas".fac_impresol 
  (
    lnkres serial not null ,
    codemp smallint not null ,
    tipdoc smallint not null ,
    nserie char(10) not null ,
    nresol char(20) not null ,
    fecres date not null ,
    numini integer not null ,
    numfin integer not null ,
    estado smallint not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (codemp,tipdoc,nserie,nresol)  constraint "sistemas".uqfacivaresol,
    
    check (estado IN (1 ,0 )) constraint "sistemas".ckfacivaresol1,
    
    check (fecres >= DATE ('01/01/2009' ) ) constraint "sistemas".ckfacivaresol2,
    
    check (numini > 0 ) constraint "sistemas".ckfacivaresol3,
    
    check (numfin > 0 ) constraint "sistemas".ckfacivaresol4,
    primary key (lnkres)  constraint "sistemas".pkfacivaresol
  );
revoke all on "sistemas".fac_impresol from "public";

{ TABLE "sistemas".his_clientes row size = 473 number of columns = 21 index size 
              = 9 }
create table "sistemas".his_clientes 
  (
    lnkcli serial not null ,
    codcli integer not null ,
    nomcli char(60) not null ,
    numnit char(30) not null ,
    numtel char(30),
    numfax char(15),
    dircli char(100) not null ,
    nomcon char(60),
    bemail char(100),
    hayfac smallint not null ,
    moncre decimal(14,2) not null ,
    diacre smallint not null ,
    salcre decimal(14,2) not null ,
    status smallint not null ,
    estado smallint not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    usract char(15) not null ,
    fecact date not null ,
    horact datetime hour to second not null ,
    primary key (lnkcli)  constraint "sistemas".pkhisclientes
  );
revoke all on "sistemas".his_clientes from "public";

{ TABLE "sistemas".fac_clientes row size = 448 number of columns = 18 index size 
              = 9 }
create table "sistemas".fac_clientes 
  (
    codcli serial not null ,
    nomcli char(60) not null ,
    numnit char(30) not null ,
    numtel char(30),
    numfax char(15),
    dircli char(100) not null ,
    nomcon char(60),
    bemail char(100),
    hayfac smallint not null ,
    haydat smallint not null ,
    moncre decimal(14,2) not null ,
    diacre smallint not null ,
    salcre decimal(14,2) not null ,
    status smallint not null ,
    estado smallint not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (estado IN (1 ,0 )) constraint "sistemas".ckfacclientes1,
    
    check (status IN (1 ,0 )) constraint "sistemas".ckfacclientes2,
    
    check (hayfac IN (1 ,0 )) constraint "sistemas".ckfacclientes3,
    
    check (moncre >= 0. ) constraint "sistemas".ckfacclientes4,
    
    check (diacre >= 0 ) constraint "sistemas".ckfacclientes5,
    primary key (codcli)  constraint "sistemas".pkfacclientes
  );
revoke all on "sistemas".fac_clientes from "public";

{ TABLE "sistemas".his_products row size = 200 number of columns = 19 index size 
              = 9 }
create table "sistemas".his_products 
  (
    lnkpro serial not null ,
    cditem integer not null ,
    codcat smallint not null ,
    subcat smallint not null ,
    codcol smallint not null ,
    codprv integer not null ,
    codmed integer not null ,
    unimed smallint not null ,
    dsitem char(100) not null ,
    codabr char(20) not null ,
    premin decimal(12,2) not null ,
    estado char(1) not null ,
    status smallint,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    usract char(15) not null ,
    fecact date not null ,
    horact datetime hour to second not null ,
    primary key (lnkpro)  constraint "sistemas".pkhisproducts
  );
revoke all on "sistemas".his_products from "public";

{ TABLE "sistemas".med row size = 22 number of columns = 2 index size = 0 }
create table "sistemas".med 
  (
    codigo smallint,
    codabr char(20)
  );
revoke all on "sistemas".med from "public";

{ TABLE "sistemas".fac_vicortes row size = 70 number of columns = 13 index size = 
              0 }
create table "sistemas".fac_vicortes 
  (
    lnkcor serial not null ,
    numpos smallint not null ,
    codemp smallint not null ,
    feccor date not null ,
    tipcor smallint not null ,
    cajero varchar(15) not null ,
    ndpque integer,
    ndpdol integer,
    ndpsof integer,
    numlot integer,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (feccor >= DATE ('01/02/2011' ) ) constraint "sistemas".ckfacvicortes1
  );
revoke all on "sistemas".fac_vicortes from "public";

{ TABLE "sistemas".his_precioss row size = 46 number of columns = 7 index size = 
              18 }
create table "sistemas".his_precioss 
  (
    lnkpre serial not null ,
    cditem integer not null ,
    preant decimal(12,2) not null ,
    presug decimal(12,2) not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (lnkpre)  constraint "sistemas".pkhisprecioss1
  );
revoke all on "sistemas".his_precioss from "public";

{ TABLE "sistemas".inv_medidpro row size = 116 number of columns = 12 index size 
              = 7 }
create table "sistemas".inv_medidpro 
  (
    codmed smallint not null ,
    codabr char(12) not null ,
    desmed varchar(60,1) not null ,
    unimed smallint not null ,
    epqfra smallint not null ,
    xlargo decimal(5,2) not null ,
    yancho decimal(5,2) not null ,
    medtot decimal(5,2) not null ,
    unimto smallint,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codmed)  constraint "sistemas".pkinvmedidpro
  );
revoke all on "sistemas".inv_medidpro from "public";

{ TABLE "sistemas".pro row size = 115 number of columns = 7 index size = 0 }
create table "sistemas".pro 
  (
    codbod smallint,
    nombod char(30),
    lnktra integer,
    correl smallint,
    codabr char(20),
    dsitem char(50),
    cantid decimal(12,2)
  );
revoke all on "sistemas".pro from "public";

{ TABLE "sistemas".fac_mtransac row size = 489 number of columns = 39 index size 
              = 9 }
create table "sistemas".fac_mtransac 
  (
    lnktra serial not null ,
    numpos smallint not null ,
    codemp smallint not null ,
    lnktdc integer not null ,
    tipdoc smallint not null ,
    nserie varchar(5) not null ,
    numdoc integer not null ,
    fecemi date not null ,
    codcli integer not null ,
    numnit varchar(20) not null ,
    nomcli varchar(60) not null ,
    dircli varchar(100) not null ,
    credit smallint not null ,
    hayord smallint not null ,
    exenta smallint not null ,
    moneda smallint not null ,
    tascam decimal(14,6) not null ,
    efecti decimal(14,2) not null ,
    cheque decimal(14,2) not null ,
    tarcre decimal(14,2) not null ,
    ordcom decimal(14,2) not null ,
    depmon decimal(14,2) not null ,
    subtot decimal(14,2) not null ,
    totdoc decimal(14,2) not null ,
    totiva decimal(14,2) not null ,
    poriva decimal(5,2) not null ,
    totpag decimal(14,2) not null ,
    saldis decimal(14,2) not null ,
    estado char(1) not null ,
    feccor date,
    usrope varchar(15),
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    motanl varchar(100,1),
    usranl varchar(15),
    fecanl date,
    horanl datetime hour to second,
    lnkinv integer not null ,
    
    check (estado IN ('V' ,'A' )) constraint "sistemas".ckfacmtransac1,
    
    check (numdoc > 0 ) constraint "sistemas".ckfacmtransac2,
    
    check (fecemi > DATE ('01/01/2011' ) ) constraint "sistemas".ckfacmtransac3,
    
    check (efecti >= 0. ) constraint "sistemas".ckfacmtransac4,
    
    check (cheque >= 0. ) constraint "sistemas".ckfacmtransac5,
    
    check (tarcre >= 0. ) constraint "sistemas".ckfacmtransac6,
    
    check (subtot >= 0. ) constraint "sistemas".ckfacmtransac7,
    
    check (totdoc >= 0. ) constraint "sistemas".ckfacmtransac8,
    
    check (poriva >= 0. ) constraint "sistemas".ckfacmtransac10,
    
    check (totiva >= 0. ) constraint "sistemas".ckfacmtransac11,
    primary key (lnktra)  constraint "sistemas".pkfacmtransac
  );
revoke all on "sistemas".fac_mtransac from "public";

{ TABLE "sistemas".fac_dtransac row size = 79 number of columns = 14 index size = 
              9 }
create table "sistemas".fac_dtransac 
  (
    lnktra integer not null ,
    correl smallint not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    cditem integer not null ,
    codabr char(20) not null ,
    unimed smallint not null ,
    canori decimal(14,2) not null ,
    unimto smallint not null ,
    cantid decimal(14,2) not null ,
    preuni decimal(12,2) not null ,
    totpro decimal(14,2) not null ,
    factor decimal(14,6) not null ,
    
    check (correl > 0 ) constraint "sistemas".ckfacdtransac1,
    
    check (totpro >= 0. ) constraint "sistemas".ckfacdtransac4
  );
revoke all on "sistemas".fac_dtransac from "public";

{ TABLE "sistemas".fac_tarjetas row size = 37 number of columns = 6 index size = 
              9 }
create table "sistemas".fac_tarjetas 
  (
    lnktra integer not null ,
    correl smallint not null ,
    codemi smallint not null ,
    ntacre char(20) not null ,
    numvou integer not null ,
    totval decimal(8,2) not null ,
    
    check (correl > 0 ) constraint "sistemas".ckfactarjetas1,
    
    check (numvou > 0 ) constraint "sistemas".ckfactarjetas2,
    
    check (totval > 0. ) constraint "sistemas".ckfactarjetas3
  );
revoke all on "sistemas".fac_tarjetas from "public";

{ TABLE "sistemas".inv_ordentra row size = 588 number of columns = 30 index size 
              = 18 }
create table "sistemas".inv_ordentra 
  (
    lnkord serial not null ,
    lnktra integer not null ,
    numord char(15) not null ,
    ordcmp char(20),
    codcli smallint not null ,
    fecord date not null ,
    subcat smallint not null ,
    codcol smallint not null ,
    cantid decimal(12,2) not null ,
    xlargo decimal(5,2) not null ,
    yancho decimal(5,2) not null ,
    descrp char(100),
    observ char(100),
    premed decimal(9,6) not null ,
    totord decimal(14,2) not null ,
    totabo decimal(14,2) not null ,
    salord decimal(14,2) not null ,
    fecofe date not null ,
    fecent date not null ,
    doctos char(100) not null ,
    obspre char(100),
    precio smallint not null ,
    compre char(50),
    tipord smallint not null ,
    medida smallint not null ,
    estado char(1) not null ,
    nofase smallint not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (lnkord)  constraint "sistemas".pkinvordentra
  );
revoke all on "sistemas".inv_ordentra from "public";

{ TABLE "sistemas".glb_opcsrole row size = 56 number of columns = 9 index size = 
              9 }
create table "sistemas".glb_opcsrole 
  (
    roleid smallint not null ,
    opcrid smallint not null ,
    fecini date not null ,
    fecfin date not null ,
    activo char(1) not null ,
    passwd char(20),
    usuaid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (roleid,opcrid) 
  );
revoke all on "sistemas".glb_opcsrole from "public";

{ TABLE "sistemas".glb_userrole row size = 49 number of columns = 8 index size = 
              22 }
create table "sistemas".glb_userrole 
  (
    userid char(15) not null ,
    roleid smallint not null ,
    fecini date not null ,
    fecfin date not null ,
    activo char(1) not null ,
    usuaid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (userid,roleid) 
  );
revoke all on "sistemas".glb_userrole from "public";

{ TABLE "sistemas".glb_permxusr row size = 85 number of columns = 10 index size = 
              37 }
create table "sistemas".glb_permxusr 
  (
    codpro char(15) not null ,
    progid smallint not null ,
    userid char(15) not null ,
    activo smallint not null ,
    passwd char(20),
    fecini date,
    fecfin date,
    usuaid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codpro,progid,userid)  constraint "sistemas".pkglbpermxusr
  );
revoke all on "sistemas".glb_permxusr from "public";

{ TABLE "sistemas".fac_usuaxpos row size = 57 number of columns = 6 index size = 
              7 }
create table "sistemas".fac_usuaxpos 
  (
    numpos smallint not null ,
    userid varchar(15) not null ,
    passwd varchar(15),
    usuaid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null 
  );
revoke all on "sistemas".fac_usuaxpos from "public";


create view "sistemas".vis_saldosxmovxmes (codemp,codsuc,codbod,aniotr,mestra,cditem,tipope,saluni,salval) as 
  select x0.codemp ,x0.codsuc ,x0.codbod ,x0.aniotr ,x0.mestra 
    ,x0.cditem ,x0.tipope ,NVL (sum(x0.canuni ) ,0 ),NVL (sum(x0.totpro 
    ) ,0 )from "sistemas".inv_dtransac x0 where (x0.estado = 'V'
     ) group by x0.codemp ,x0.codsuc ,x0.codbod ,x0.aniotr ,x0.mestra 
    ,x0.cditem ,x0.tipope ;                                  
                               
create view "sistemas".vis_tipdocxpos (lnktdc,numpos,nomdoc) as  
  select x0.lnktdc ,x0.numpos ,((((('EMPRESA ' || x2.nomabr ) || 
    ' ' ) || TRIM ( BOTH  ' ' FROM x1.nomdoc ) ) || ' SERIE ' ) 
    || TRIM ( BOTH  ' ' FROM x0.nserie ) ) from "sistemas".fac_tdocxpos 
    x0 ,"sistemas".fac_tipodocs x1 ,"sistemas".glb_empresas x2 
    where  (((x0.tipdoc = x1.tipdoc ) AND (x0.estado = 'A' ) ) 
    AND (x2.codemp = x0.codemp ) ) ;                         
                        

create unique index "sistemas".ix130_1 on "sistemas".his_preciosm 
    (lnkpre) using btree ;
alter table "sistemas".glb_sucsxemp add constraint (foreign key 
    (codemp) references "sistemas".glb_empresas  constraint "sistemas"
    .fkglbempresas);

alter table "sistemas".inv_mbodegas add constraint (foreign key 
    (codemp) references "sistemas".glb_empresas  constraint "sistemas"
    .fkglbempresas2);

alter table "sistemas".inv_products add constraint (foreign key 
    (codcat) references "sistemas".glb_categors  constraint "sistemas"
    .fkinvcategpro1);

alter table "sistemas".inv_products add constraint (foreign key 
    (codcol) references "sistemas".inv_colorpro  constraint "sistemas"
    .fkinvcolorpro1);

alter table "sistemas".inv_products add constraint (foreign key 
    (codprv) references "sistemas".inv_provedrs  constraint "sistemas"
    .fkinvprovedrs1);

alter table "sistemas".inv_products add constraint (foreign key 
    (unimed) references "sistemas".inv_unimedid  constraint "sistemas"
    .fkinvunimedid1);

alter table "sistemas".inv_mbodegas add constraint (foreign key 
    (codsuc) references "sistemas".glb_sucsxemp  constraint "sistemas"
    .fkglbsucsxemp1);

alter table "sistemas".inv_epqsxpro add constraint (foreign key 
    (cditem) references "sistemas".inv_products  on delete cascade 
    constraint "sistemas".fkinvproducts3);

alter table "sistemas".his_preciosm add constraint (foreign key 
    (cditem) references "sistemas".inv_products  on delete cascade 
    constraint "sistemas".fkinvproducts2);

alter table "sistemas".inv_provedrs add constraint (foreign key 
    (codpai) references "sistemas".glb_mtpaises  constraint "sistemas"
    .fkglbmtpaises1);

alter table "sistemas".inv_dtransac add constraint (foreign key 
    (lnktra) references "sistemas".inv_mtransac  constraint "sistemas"
    .fkinvmtransac1);

alter table "sistemas".his_dtransac add constraint (foreign key 
    (lnkhis) references "sistemas".his_mtransac  on delete cascade 
    constraint "sistemas".fkhismtransac1);

alter table "sistemas".fac_usuaxpos add constraint (foreign key 
    (numpos) references "sistemas".fac_puntovta  on delete cascade 
    constraint "sistemas".fkfacpuntovta1);

alter table "sistemas".his_precioss add constraint (foreign key 
    (cditem) references "sistemas".inv_products  on delete cascade 
    constraint "sistemas".fkinvproducts4);

alter table "sistemas".fac_dtransac add constraint (foreign key 
    (lnktra) references "sistemas".fac_mtransac  on delete cascade 
    constraint "sistemas".fkfacmtransac1);

alter table "sistemas".fac_tarjetas add constraint (foreign key 
    (lnktra) references "sistemas".fac_mtransac  on delete cascade 
    constraint "sistemas".fkfacmtransac2);

alter table "sistemas".inv_ordentra add constraint (foreign key 
    (lnktra) references "sistemas".fac_mtransac  on delete cascade 
    constraint "sistemas".fkfacmtransac3);


create trigger "sistemas".trginsertarempaque insert on "sistemas"
    .inv_products referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptempaquedefault(pos.cditem 
    ));

create trigger "sistemas".trgupdatepreciominimo update of premin 
    on "sistemas".inv_products referencing old as pre new as pos
    
    for each row
        (
        insert into "sistemas".his_preciosm (lnkpre,cditem,preant,
    premin,userid,fecsis,horsis)  values (0 ,pre.cditem ,pre.premin ,
    pos.premin ,USER ,CURRENT year to fraction(3) ,CURRENT hour to second 
    ));

create trigger "sistemas".trgupdateunimed update of unimed on 
    "sistemas".inv_products referencing old as pre new as pos
    for each row
        (
        execute procedure "sistemas".sptempaquedefault(pos.cditem 
    ));

create trigger "sistemas".trgupdateproductos update of fulcam 
    on "sistemas".inv_products referencing old as pre new as pos
    
    for each row
        (
        insert into "sistemas".his_products (lnkpro,cditem,codcat,
    subcat,codcol,codprv,codmed,unimed,dsitem,codabr,premin,estado,status,
    userid,fecsis,horsis,usract,fecact,horact)  values (0 ,pre.cditem 
    ,pre.codcat ,pre.subcat ,pre.codcol ,pre.codprv ,pre.codmed ,pre.unimed 
    ,pre.dsitem ,pre.codabr ,pre.premin ,pre.estado ,pre.status ,pre.userid 
    ,pre.fecsis ,pre.horsis ,USER ,CURRENT year to fraction(3) ,CURRENT 
    hour to second ));

create trigger "sistemas".trgupdatepreciosugerido update of presug 
    on "sistemas".inv_products referencing old as pre new as pos
    
    for each row
        (
        insert into "sistemas".his_precioss (lnkpre,cditem,preant,
    presug,userid,fecsis,horsis)  values (0 ,pre.cditem ,pre.presug ,
    pos.presug ,USER ,CURRENT year to fraction(3) ,CURRENT hour to second 
    ));

create trigger "sistemas".trgupdestadomovto update of estado 
    on "sistemas".inv_mtransac referencing new as pos
    for each row
        (
        update "sistemas".inv_dtransac set "sistemas".inv_dtransac.estado 
    = pos.estado  where (lnktra = pos.lnktra ) );

create trigger "sistemas".trgeliminarmovto delete on "sistemas"
    .inv_mtransac referencing old as pre
    for each row
        (
        delete from "sistemas".inv_dtransac  where (lnktra = 
    pre.lnktra ) );

create trigger "sistemas".trginsertarproducto insert on "sistemas"
    .inv_dtransac referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptsaldosxproducto(pos.codemp 
    ,pos.codsuc ,pos.codbod ,pos.tipmov ,pos.aniotr ,pos.mestra ,pos.cditem 
    ,pos.tipope ,pos.codabr ,pos.preuni ));

create trigger "sistemas".trgupdestadoproducto update of estado 
    on "sistemas".inv_dtransac referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptsaldosxproducto(pos.codemp 
    ,pos.codsuc ,pos.codbod ,pos.tipmov ,pos.aniotr ,pos.mestra ,pos.cditem 
    ,pos.tipope ,pos.codabr ,pos.preuni ));

create trigger "sistemas".trgupdcanuniproducto update of canuni 
    on "sistemas".inv_dtransac referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptsaldosxproducto(pos.codemp 
    ,pos.codsuc ,pos.codbod ,pos.tipmov ,pos.aniotr ,pos.mestra ,pos.cditem 
    ,pos.tipope ,pos.codabr ,pos.preuni ));

create trigger "sistemas".trgcupdpreuniproducto update of preuni 
    on "sistemas".inv_dtransac referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptsaldosxproducto(pos.codemp 
    ,pos.codsuc ,pos.codbod ,pos.tipmov ,pos.aniotr ,pos.mestra ,pos.cditem 
    ,pos.tipope ,pos.codabr ,pos.preuni ));

create trigger "sistemas".trgeliminarproducto delete on "sistemas"
    .inv_dtransac referencing old as pre
    for each row
        (
        execute procedure "sistemas".sptsaldosxproducto(pre.codemp 
    ,pre.codsuc ,pre.codbod ,pre.tipmov ,pre.aniotr ,pre.mestra ,pre.cditem 
    ,pre.tipope ,pre.codabr ,pre.preuni ));

create trigger "sistemas".trgupdateclientes update on "sistemas"
    .fac_clientes referencing old as pre new as pos
    for each row
        (
        insert into "sistemas".his_clientes (lnkcli,codcli,nomcli,
    numnit,numtel,numfax,dircli,nomcon,bemail,hayfac,moncre,diacre,salcre,
    status,estado,userid,fecsis,horsis,usract,fecact,horact)  values 
    (0 ,pre.codcli ,pre.nomcli ,pre.numnit ,pre.numtel ,pre.numfax ,pre.dircli 
    ,pre.nomcon ,pre.bemail ,pre.hayfac ,pre.moncre ,pre.diacre ,pre.salcre 
    ,pre.status ,pre.estado ,pre.userid ,pre.fecsis ,pre.horsis ,USER 
    ,CURRENT year to fraction(3) ,CURRENT hour to second ));

create trigger "sistemas".trgcortecaja insert on "sistemas".fac_vicortes 
    referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptcortecaja(pos.numpos 
    ,pos.codemp ,pos.feccor ,pos.cajero ));

create trigger "sistemas".trganulcortecaja delete on "sistemas"
    .fac_vicortes referencing old as pre
    for each row
        (
        execute procedure "sistemas".sptanulcortecaja(pre.numpos 
    ,pre.codemp ,pre.feccor ,pre.cajero ));

create trigger "sistemas".trganulaordent update of estado on 
    "sistemas".fac_mtransac referencing new as pos
    for each row
        (
        update "sistemas".inv_ordentra set "sistemas".inv_ordentra.estado 
    = pos.estado  where (lnktra = pos.lnktra ) );



