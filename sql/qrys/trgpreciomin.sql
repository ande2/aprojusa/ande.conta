
drop trigger "sistemas".trgupdateprecio; 
create trigger "sistemas".trgupdatepreciominimo update of premin on 
    "sistemas".inv_products referencing old as pre new as pos
    for each row
        (
        insert into "sistemas".his_preciosm (lnkpre,cditem,preant,
    premin,userid,fecsis,horsis)  values (0 ,pre.cditem ,pre.premin ,
    pos.premin ,USER ,CURRENT year to fraction(3) ,CURRENT hour to second 
    ));



