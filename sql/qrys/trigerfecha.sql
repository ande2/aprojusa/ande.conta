
create trigger "sistemas".trgupdfechaemision update of fecemi
    on "sistemas".inv_mtransac referencing new as pos
    for each row
        (
         update inv_dtransac
         set    inv_dtransac.fecemi = pos.fecemi
         where  inv_dtransac.lnktra = pos.lnktra
        )
