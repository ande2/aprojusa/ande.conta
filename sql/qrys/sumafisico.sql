 
CREATE PROCEDURE "sistemas".sptsumafisico(wcodemp SMALLINT,wcodsuc SMALLINT,wcodbod SMALLINT,
                                          wfecinv DATE,wcditem INT,wcodabr CHAR(20))

 -- Procedimiento para totalizar lo registrado en el inventario fisico
 -- Mynor Ramirez
 -- Mayo 2010

 DEFINE xtotuni DEC(12,2);
 DEFINE wmestra LIKE inv_tofisico.mestra;
 DEFINE waniotr LIKE inv_tofisico.aniotr;
 DEFINE wlnktra LIKE inv_mtransac.lnktra;
 DEFINE wtipmov LIKE inv_mtransac.tipmov;
 DEFINE conteo  INT;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;
 LET waniotr = YEAR(wfecinv);
 LET wmestra = MONTH(wfecinv);

 -- Totalizando conteo fisico
 SELECT NVL(SUM(a.canuni),0)
  INTO  xtotuni
  FROM  inv_scfisico a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.fecinv = wfecinv
    AND a.cditem = wcditem;

 -- Verificando si producto ya existe regstrado en inventario fisico
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_tofisico a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.fecinv = wfecinv
    AND a.aniotr = waniotr
    AND a.mestra = wmestra
    AND a.cditem = wcditem;
  IF (conteo>0) THEN
     -- Si ya existe borra el inventario
     SET LOCK MODE TO WAIT;
     DELETE FROM inv_tofisico
     WHERE inv_tofisico.codemp = wcodemp
       AND inv_tofisico.codsuc = wcodsuc
       AND inv_tofisico.codbod = wcodbod
       AND inv_tofisico.fecinv = wfecinv
       AND inv_tofisico.aniotr = waniotr
       AND inv_tofisico.mestra = wmestra
       AND inv_tofisico.cditem = wcditem;
  END IF

  -- Insertando ivnentario
  SET LOCK MODE TO WAIT;
  INSERT INTO inv_tofisico
  VALUES (wcodemp,
          wcodsuc,
          wcodbod,
          waniotr,
          wmestra,
          wfecinv,
          wcditem,
          wcodabr,
          1,
          xtotuni,
          xtotuni,
          0,
          0,
          0,
          USER,
          CURRENT,
          CURRENT HOUR TO SECOND);

  -- Creando movimiento del producto por saldo inicial por conteo fisico
  --LET wlnktra = 2367;
  --LET wtipmov = 107;

  LET wlnktra = 2271;
  LET wtipmov = 1; 

  -- Borrando movimiento
  DELETE FROM inv_dtransac
  WHERE inv_dtransac.lnktra = wlnktra
    AND inv_dtransac.codemp = wcodemp
    AND inv_dtransac.codsuc = wcodsuc
    AND inv_dtransac.codbod = wcodbod
    AND inv_dtransac.tipmov = wtipmov
    AND inv_dtransac.fecemi = wfecinv
    AND inv_dtransac.aniotr = waniotr
    AND inv_dtransac.mestra = wmestra
    AND inv_dtransac.cditem = wcditem;

  -- Insertando movimiento
  SET LOCK MODE TO WAIT;
  INSERT INTO inv_dtransac
  VALUES (wlnktra,
          wcodemp,
          wcodsuc,
          wcodbod,
          wtipmov,
          1,
          0,
          wfecinv,
          waniotr,
          wmestra,
          wcditem,
          wcodabr,
          0,
          xtotuni,
          xtotuni,
          0,
          0,
          0,
          1,
          wcodabr,
          1,
          "V",
          0,
          xtotuni,
          USER,
          CURRENT,
          CURRENT HOUR TO SECOND);
END PROCEDURE;


 

 

 

