
  drop trigger "sistemas".trganulafac;

create trigger "sistemas".trganulafac update of estado on
    "sistemas".fac_mtransac referencing new as pos
    for each row
        (

         update fac_apartads
         set    fac_apartads.estado = 0,
                fac_apartads.observ = pos.motanl
         where  fac_apartads.lnktra = pos.lnktra,

         update inv_mtransac
         set    inv_mtransac.estado = pos.estado,
                inv_mtransac.motanl = pos.motanl,
                inv_mtransac.usranl = pos.usranl,
                inv_mtransac.fecanl = pos.fecanl,
                inv_mtransac.horanl = pos.horanl
         where  inv_mtransac.numrf1 = pos.lnktra
           and  inv_mtransac.tipmov = 1,

         execute procedure "sistemas".stpsaldoapartados(pos.lnkapa)

        );
