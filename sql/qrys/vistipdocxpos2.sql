DROP VIEW vis_tipdocxpos;
CREATE VIEW vis_tipdocxpos (lnktdc,numpos,nomdoc)
AS
SELECT a.lnktdc,a.numpos,TRIM(b.nomdoc)||' SERIE '||TRIM(a.nserie)||' '||TRIM(a.nomdoc)
FROM  fac_tdocxpos a,fac_tipodocs b,glb_empresas c
WHERE a.tipdoc = b.tipdoc
  AND a.estado = 'A'
  AND c.codemp = a.codemp;
GRANT SELECT ON vis_tipdocxpos TO PUBLIC;
