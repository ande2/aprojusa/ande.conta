
drop trigger "sistemas".trgupdestadoproducto;
create trigger "sistemas".trgupdestadoproducto update of estado
    on "sistemas".inv_dtransac referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptsaldosxproducto(pos.codemp
    ,pos.codsuc ,pos.codbod ,pos.tipmov ,pos.aniotr ,pos.mestra ,pos.cditem
    ,pos.tipope ,pos.codabr, pos.preuni ));

drop trigger "sistemas".trgupdcanuniproducto;
create trigger "sistemas".trgupdcanuniproducto update of canuni
    on "sistemas".inv_dtransac referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptsaldosxproducto(pos.codemp
    ,pos.codsuc ,pos.codbod ,pos.tipmov ,pos.aniotr ,pos.mestra ,pos.cditem
    ,pos.tipope ,pos.codabr, pos.preuni ));


drop trigger "sistemas".trgcupdpreuniproducto;
create trigger "sistemas".trgcupdpreuniproducto update of preuni
    on "sistemas".inv_dtransac referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptsaldosxproducto(pos.codemp
    ,pos.codsuc ,pos.codbod ,pos.tipmov ,pos.aniotr ,pos.mestra ,pos.cditem
    ,pos.tipope ,pos.codabr, pos.preuni ));


drop trigger "sistemas".trgeliminarproducto;
create trigger "sistemas".trgeliminarproducto delete on "sistemas"
    .inv_dtransac referencing old as pre
    for each row
        (
        execute procedure "sistemas".sptsaldosxproducto(pre.codemp
    ,pre.codsuc ,pre.codbod ,pre.tipmov ,pre.aniotr ,pre.mestra ,pre.cditem
    ,pre.tipope ,pre.codabr, pre.preuni ));
