
{ TABLE "sistemas".fac_tarjetas row size = 37 number of columns = 6 index size = 
              0 }
create table "sistemas".fac_tarjetas 
  (
    lnktra integer not null ,
    correl smallint not null ,
    codemi smallint not null ,
    ntacre char(20) not null ,
    numvou integer not null ,
    totval decimal(8,2) not null ,
    
    check (correl > 0 ) constraint "sistemas".ckfactarjetas1,
    
    check (numvou > 0 ) constraint "sistemas".ckfactarjetas2,
    
    check (totval > 0. ) constraint "sistemas".ckfactarjetas3
  );
revoke all on "sistemas".fac_tarjetas from "public" as "sistemas";




