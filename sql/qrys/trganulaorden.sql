


  drop trigger "sistemas".trganulaordent;

create trigger "sistemas".trganulaordent update of estado on
    "sistemas".fac_mtransac referencing new as pos
    for each row
        (

         update inv_ordentra
         set    inv_ordentra.estado = pos.estado
         where  inv_ordentra.lnktra = pos.lnktra,

         update inv_mtransac
         set    inv_mtransac.estado = pos.estado,
                inv_mtransac.motanl = pos.motanl,
                inv_mtransac.usranl = pos.usranl,
                inv_mtransac.fecanl = pos.fecanl,
                inv_mtransac.horanl = pos.horanl
         where  inv_mtransac.numrf1 = pos.lnktra
           and  inv_mtransac.tipmov = 1
        );
