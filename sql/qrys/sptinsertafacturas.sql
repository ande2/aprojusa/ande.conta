


DROP PROCEDURE "sistemas".sptinsertafacturas;
CREATE PROCEDURE "sistemas".sptinsertafacturas(wlnktra INTEGER,
                                               wnumpos SMALLINT,
                                               wcodemp SMALLINT,
                                               wnumdoc INTEGER,
                                               wfecemi DATE,
                                               wcodcli INTEGER)

 -- Procedimiento para insertar los documentos de facturacion en el inventario
 -- Mynor Ramirez
 -- Abril 2011

 DEFINE wtipmov  LIKE inv_mtransac.tipmov;
 DEFINE wcodsuc  LIKE inv_mtransac.codsuc;
 DEFINE wcodbod  LIKE inv_mtransac.codbod;
 DEFINE waniotr  LIKE inv_mtransac.aniotr;
 DEFINE wmestra  LIKE inv_mtransac.mestra;
 DEFINE wobserv  LIKE inv_mtransac.observ;
 DEFINE westado  LIKE inv_mtransac.estado;
 DEFINE wtipope  LIKE inv_mtransac.tipope;
 DEFINE wchkinv  LIKE fac_puntovta.chkinv;
 DEFINE xlnktra  LIKE inv_dtransac.lnktra;
 DEFINE xcditem  LIKE inv_dtransac.cditem;
 DEFINE xcodabr  LIKE inv_dtransac.codabr;
 DEFINE xnserie  LIKE inv_dtransac.nserie;
 DEFINE xcodepq  LIKE inv_dtransac.codepq;
 DEFINE xcanepq  LIKE inv_dtransac.canepq;
 DEFINE xcanuni  LIKE inv_dtransac.canuni;
 DEFINE xpreuni  LIKE inv_dtransac.preuni;
 DEFINE xtotpro  LIKE inv_dtransac.totpro;
 DEFINE xcorrel  LIKE inv_dtransac.correl;
 DEFINE xbarcod  LIKE inv_dtransac.barcod;
 DEFINE xopeval  LIKE inv_dtransac.opeval;
 DEFINE xopeuni  LIKE inv_dtransac.opeuni;
 DEFINE xnomdoc  LIKE fac_tipodocs.nomdoc;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Seleccionando datos del punto de venta
 SELECT NVL(a.codsuc,0),NVL(a.codbod,0),NVL(a.chkinv,0)
  INTO  wcodsuc,wcodbod,wchkinv
  FROM  fac_puntovta a
  WHERE a.numpos = wnumpos;

 -- Verificando si existen datos
 IF (wcodsuc>0) THEN

  -- Verificando si punto de venta afecta inventario
  IF (wchkinv>0) THEN

   -- Obteniendo tipo de movimiento default de para facturacion
   SELECT NVL(a.valchr,0)
    INTO  wtipmov
    FROM  glb_paramtrs a
    WHERE a.numpar = 13
      AND a.tippar = 1;

   -- Seleccionando tipo de movimiento
   SELECT NVL(a.tipmov,0),NVL(a.tipope,0)
    INTO  wtipmov,wtipope
    FROM  inv_tipomovs a
    WHERE a.tipmov = wtipmov;

   -- Verificando si existen datos
   IF (wtipmov>0) THEN
    -- Obteniendo nombre del tipo de documento
    SELECT NVL(a.nomdoc,0),NVL(a.tipope,0)
     INTO  wtipmov,wtipope
     FROM  inv_tipomovs a
     WHERE a.tipmov = wtipmov;

    -- Asignando datos
    LET xnomdoc = 
    LET wmestra = MONTH(wfecemi);
    LET waniotr = YEAR(wfecemi);
    LET wobserv = "FACTURACION DOCUMENTO # "||wnumdoc;
    LET westado = "V";

    -- Seleccionando datos
    INSERT INTO inv_mtransac
    VALUES (0                     ,  -- Id unico
            wcodemp               ,  -- Empresa
            wcodsuc               ,  -- Sucursal
            wcodbod               ,  -- Bodega
            wtipmov               ,  -- Tipo de movimiento
            0                     ,  -- Origen
            ""                    ,  -- Nombre origen
            wcodcli               ,  -- Destino
            ""                    ,  -- Nombre destino
            wlnktra               ,  -- ID factura
            wnumdoc               ,  -- Numero de documento de facturacion
            0                     ,  -- Movimiento es externo Movimiento es externo
            waniotr               ,  -- Anio
            wmestra               ,  -- Mes
            wfecemi               ,  -- Fecha de emision
            wtipope               ,  -- Tipo de operacion del movimiento
            wobserv               ,  -- Observaciones
            westado               ,  -- Estado V=igente
            0                     ,  -- ID numero de consumo
            0                     ,  -- 1=Consumo automatico 0=No consumo automatico
            NULL                  ,  -- Motivo de la anulacion
            NULL                  ,  -- Fecha de anulacion
            NULL                  ,  -- Hora de la anulacion
            NULL                  ,  -- Usuario anulo
            "S"                   ,  -- Documento impreso
            USER                  ,  -- Usuario registro
            CURRENT               ,  -- Fecha de registro
            CURRENT HOUR TO SECOND); -- Hora de registro

    -- Obteniendo serial de la tabla encabezado
    SELECT UNIQUE DBINFO("SQLCA.SQLERRD1")
     INTO  xlnktra
     FROM  inv_mtransac;

    -- Creando detalle de la transaccion
    LET xnserie = "";
    FOREACH SELECT a.cditem,
                   a.codabr,
                   a.codepq,
                   a.canepq,
                   a.cantid,
                   a.preuni,
                   a.totpro,
                   "0",
                   a.correl
             INTO  xcditem,
                   xcodabr,
                   xcodepq,
                   xcanepq,
                   xcanuni,
                   xpreuni,
                   xtotpro,
                   xbarcod,
                   xcorrel
             FROM  fac_dtransac a
             WHERE a.lnktra = wlnktra

     -- Calculando cantdad y valor para calculo de saldo
     IF (wtipope=0) THEN
        LET xopeuni = (xcanuni*(-1));
        LET xopeval = (xtotpro*(-1));
     ELSE
        LET xopeuni = xcanuni;
        LET xopeval = xtotpro;
     END IF;

     -- Insertando detalle
     INSERT INTO inv_dtransac
     VALUES (xlnktra               ,  -- id transaccion
             wcodemp               ,  -- empresa
             wcodsuc               ,  -- sucursal
             wcodbod               ,  -- bodega
             wtipmov               ,  -- tipo de movimiento
             0                     ,  -- origen
             wcodcli               ,  -- destino
             wfecemi               ,  -- fecha de emision
             waniotr               ,  -- anio
             wmestra               ,  -- mes
             xcditem               ,  -- codigo de producto
             xcodabr               ,  -- codigo abreviado
             xnserie               ,  -- numero de serie
             0                     ,  -- codigo de empaque
             xcanepq               ,  -- cantidad de empaque
             xcanuni               ,  -- cantidad
             xpreuni               ,  -- precio unitario
             xtotpro               ,  -- total del producto
             0                     ,  -- precio promedio
             xcorrel               ,  -- correlativo
             xbarcod               ,  -- codigo de barra
             wtipope               ,  -- tipo de operacion del movimniento
             westado               ,  -- estado
             xopeval               ,  -- total
             xopeuni               ,  -- cantidad
             1                     ,  -- Actualiza existencia
             USER                  ,  -- Usuario registro
             CURRENT               ,  -- Fecha de registro
             CURRENT HOUR TO SECOND); -- Hora de registro

    END FOREACH;

   END IF;

  END IF;

 END IF;
END PROCEDURE;
