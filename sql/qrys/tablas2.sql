create table "sistemas".fac_clientes 
  (
    codcli serial not null ,
    nomcli char(60) not null ,
    numnit char(30) not null ,
    numtel char(30),
    numfax char(15),
    dircli char(100) not null ,
    nomcon char(60),
    bemail char(100),
    hayfac smallint not null ,
    moncre decimal(14,2) not null ,
    diacre smallint not null ,
    salcre decimal(14,2) not null ,
    status smallint not null ,
    estado smallint not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codcli)  constraint "sistemas".pkfacclientes
  );
revoke all on "sistemas".fac_clientes from "public";

create trigger "sistemas".trgupdateclientes update on "sistemas"
    .fac_clientes referencing old as pre new as pos
    for each row
        (
        insert into "sistemas".his_clientes (lnkcli,codcli,nomcli,
    numnit,numtel,numfax,dircli,nomcon,bemail,hayfac,moncre,diacre,salcre,
    status,estado,userid,fecsis,horsis,usract,fecact,horact)  values 
    (0 ,pre.codcli ,pre.nomcli ,pre.numnit ,pre.numtel ,pre.numfax ,pre.dircli 
    ,pre.nomcon ,pre.bemail ,pre.hayfac ,pre.moncre ,pre.diacre ,pre.salcre 
    ,pre.status ,pre.estado ,pre.userid ,pre.fecsis ,pre.horsis ,USER 
    ,CURRENT year to fraction(3) ,CURRENT hour to second ));



