

--DROP   PROCEDURE "sistemas".sptcortecaja;

CREATE PROCEDURE "sistemas".sptcortecaja(wnumpos INT,wcodemp INT,wfeccor DATE,wcajero VARCHAR(15))
 -- Procedimiento para actualizar los documentos de venta con corte de caja
 -- Mynor Ramirez
 -- Enero 2011

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Actualizando fecha de corte por cada documento de venta
 SET LOCK MODE TO WAIT;

 -- Actualizando
 UPDATE fac_mtransac
 SET    fac_mtransac.feccor = wfeccor
 WHERE (fac_mtransac.numpos = wnumpos)
   AND (fac_mtransac.codemp = wcodemp)
   AND (fac_mtransac.feccor IS NULL)
   AND (fac_mtransac.userid = wcajero);
END PROCEDURE;
