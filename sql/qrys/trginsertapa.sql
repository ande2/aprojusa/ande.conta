


  drop trigger "sistemas".trgupdateapa;

create trigger "sistemas".trgupdateapa update of lnkapa on "sistemas".fac_mtransac
    referencing new as pos
    for each row (
            execute procedure "sistemas".stpsaldoapartados(pos.lnkapa
    ));
