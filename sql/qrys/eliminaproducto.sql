create trigger "sistemas".trgeliminarproducto delete on "sistemas"
    .inv_dtransac referencing old as pre
    for each row
        (
        execute procedure "sistemas".sptsaldosxproducto(pre.codemp
    ,pre.codsuc ,pre.codbod ,pre.tipmov ,pre.aniotr ,pre.mestra ,pre.cditem
    ,pre.tipope ,pre.codabr ));
