create trigger "sistemas".trganulcortecaja delete on                
    "sistemas".fac_vicortes referencing old as pre 
    for each row
        (
       execute procedure "sistemas".sptanulcortecaja(pre.numpos,pre.codemp,pre.feccor,pre.cajero) 
        );
