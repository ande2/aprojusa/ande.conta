update inv_products
set codabr[9,20] = (select b.codabr from inv_medidpro b
                    where b.codmed = inv_products.codmed)
where exists (select b.codmed from inv_medidpro	b
               where b.codmed = inv_products.codmed
                 and b.codabr != inv_products.codabr[9,20])
