drop trigger trgcancelapa;

create trigger trgcancelapa update of cancel on "sistemas"
    .fac_apartads referencing new as pos
    for each row
        when ((pos.cancel = 1 ) )
        (
          execute procedure sptcancelapa(pos.lnkapa,pos.lnktra,pos.numpos,pos.codcli)
        );
