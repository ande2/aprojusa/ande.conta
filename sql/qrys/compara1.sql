select cditem,codabr[1,8],codabr[9,20]
from inv_products
where exists (select b.codmed from inv_medidpro	b
               where b.codmed = inv_products.codmed
                 and b.codabr != inv_products.codabr[9,20])
