
drop trigger "sistemas".trginsertafisico;

create trigger trginsertafisico insert on
    inv_scfisico referencing new as pos
    for each row
        (
         execute procedure sptsumafisico(pos.codemp,pos.codsuc,pos.codbod,pos.fecinv,pos.cditem,pos.codabr)
        );
