create trigger "sistemas".trgcortecaja insert on                
    "sistemas".fac_vicortes referencing new as pos
    for each row
        (
       execute procedure "sistemas".sptcortecaja(pos.numpos,pos.codemp,pos.feccor,pos.cajero) 
        );

