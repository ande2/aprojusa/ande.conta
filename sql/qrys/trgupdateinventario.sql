
drop trigger "sistemas".trgupdateinventario;
create trigger trgupdateinventario update of lnkinv on
    fac_mtransac referencing old as pre new as pos
    for each row
     when (pos.lnkapa=0) 
        (
         execute procedure sptinsertafacturas(pos.lnktra,
         pos.numpos,pos.codemp,pos.numdoc,pos.fecemi,pos.codcli)
        );
