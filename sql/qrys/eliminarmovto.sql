create trigger "sistemas".trgeliminarmovto delete on "sistemas"
    .inv_mtransac referencing old as pre
    for each row
        (
        delete from inv_dtransac
        where lnktra = pre.lnktra
        );
