--drop trigger trginsertpartados;
create trigger trginsertpartados insert on
    fac_apartads referencing new as pos
    for each row
        (
         execute function fxsaldoapartados(pos.lnkapa)
        );
