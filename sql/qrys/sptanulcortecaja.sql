DROP PROCEDURE "sistemas".sptanulcortecaja;

CREATE PROCEDURE "sistemas".sptanulcortecaja(wnumpos INT,wcodemp INT,wfeccor DATE,wcajero VARCHAR(15))
 -- Procedimiento para anular un corte de caja 
 -- Mynor Ramirez
 -- Enero 2011

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Actualizando fecha de corte por cada documento de venta
 SET LOCK MODE TO WAIT;

 -- Actualizando
 UPDATE fac_mtransac
 SET    fac_mtransac.feccor = NULL
 WHERE (fac_mtransac.numpos = wnumpos)
   AND (fac_mtransac.codemp = wcodemp)
   AND (fac_mtransac.feccor = wfeccor)
   AND (fac_mtransac.userid = wcajero);
END PROCEDURE;
