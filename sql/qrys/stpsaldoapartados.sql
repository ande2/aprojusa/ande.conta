DROP PROCEDURE stpsaldoapartados;
CREATE PROCEDURE  "sistemas".stpsaldoapartados(wlnkapa INTEGER)

 DEFINE wtotpro LIKE fac_apartads.totpro;
 DEFINE wabonos LIKE fac_apartads.totabo;
 DEFINE wsaldos LIKE fac_apartads.totsal;

 -- Procedure para calcular el saldo de un apartado en bases a sus abonos
 -- Mynor Ramirez
 -- Mayo 2011

 -- Definiendo nivel de aislamiento
 SET ISOLATION COMMITTED READ;

 -- Verificando que sean abonos de apartados
 IF wlnkapa>0 THEN 
  -- Seleccionando valor original
  LET wtotpro = 0;
  SELECT SUM(a.totsal)
   INTO  wtotpro
   FROM  fac_apartads a
   WHERE a.lnkapa = wlnkapa
     AND a.lnknid = 0
     AND a.estado = 1;

  -- Sumando abonos
  LET wabonos = 0;
  SELECT NVL(SUM(a.totabo),0)
   INTO  wabonos
   FROM  fac_apartads a
   WHERE a.lnkapa > 0
     AND a.lnknid = wlnkapa
     AND a.estado = 1;

  -- Calculando saldo
  LET wsaldos = (wtotpro-wabonos);

  -- ACtualizando saldo
  SET LOCK MODE TO WAIT;
  UPDATE fac_apartads
  SET    fac_apartads.salact = wsaldos
  WHERE  fac_apartads.lnkapa = wlnkapa;
 END IF; 
END PROCEDURE
