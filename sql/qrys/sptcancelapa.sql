
DROP PROCEDURE sptcancelapa;

CREATE PROCEDURE "sistemas".sptcancelapa(wlnkapa INTEGER,
                                         wlnktra INTEGER,
                                         wnumpos SMALLINT,
                                         wcodcli INTEGER)

 -- Procedimiento para insertar el movimiento de inventario por cancelacion
 -- de un apartado
 -- Mynor Ramirez
 -- Octubre 2011

 DEFINE wcodemp  LIKE fac_mtransac.codemp;
 DEFINE wnumdoc  LIKE fac_mtransac.numdoc;
 DEFINE wfecemi  LIKE inv_mtransac.fecemi;
 DEFINE wtipmov  LIKE inv_mtransac.tipmov;
 DEFINE wcodsuc  LIKE inv_mtransac.codsuc;
 DEFINE wcodbod  LIKE inv_mtransac.codbod;
 DEFINE waniotr  LIKE inv_mtransac.aniotr;
 DEFINE wmestra  LIKE inv_mtransac.mestra;
 DEFINE wobserv  LIKE inv_mtransac.observ;
 DEFINE westado  LIKE inv_mtransac.estado;
 DEFINE wtipope  LIKE inv_mtransac.tipope;
 DEFINE wchkinv  LIKE fac_puntovta.chkinv;
 DEFINE xlnktra  LIKE inv_dtransac.lnktra;
 DEFINE xcditem  LIKE inv_dtransac.cditem;
 DEFINE xcodabr  LIKE inv_dtransac.codabr;
 DEFINE xcodepq  LIKE inv_dtransac.codepq;
 DEFINE xcanepq  LIKE inv_dtransac.canepq;
 DEFINE xcanuni  LIKE inv_dtransac.canuni;
 DEFINE xpreuni  LIKE inv_dtransac.preuni;
 DEFINE xtotpro  LIKE inv_dtransac.totpro;
 DEFINE xcorrel  LIKE inv_dtransac.correl;
 DEFINE xbarcod  LIKE inv_dtransac.barcod;
 DEFINE xopeval  LIKE inv_dtransac.opeval;
 DEFINE xopeuni  LIKE inv_dtransac.opeuni;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Seleccionando datos del documento de facturaciom
 LET wcodemp = NULL;
 LET wnumdoc = NULL;

 SELECT a.codemp,a.numdoc
  INTO  wcodemp,wnumdoc
  FROM  fac_mtransac a
  WHERE a.lnktra = wlnktra;

 -- Seleccionando datos del punto de venta
 SELECT NVL(a.codsuc,0),NVL(a.codbod,0),NVL(a.chkinv,0)
  INTO  wcodsuc,wcodbod,wchkinv
  FROM  fac_puntovta a
  WHERE a.numpos = wnumpos;

 -- Verificando si existen datos
 IF (wcodsuc>0) THEN

  -- Verificando si punto de venta afecta inventario
  IF (wchkinv>0) THEN

   -- Obteniendo tipo de movimiento default para la devolucion por cancelacion del
   -- apartado
   SELECT NVL(a.valchr,0)
    INTO  wtipmov
    FROM  glb_paramtrs a
    WHERE a.numpar = 14
      AND a.tippar = 1;

   -- Seleccionando tipo de movimiento
   SELECT NVL(a.tipmov,0),NVL(a.tipope,0)
    INTO  wtipmov,wtipope
    FROM  inv_tipomovs a
    WHERE a.tipmov = wtipmov;

   -- Verificando si existen datos
   IF (wtipmov>0) THEN

    -- Asignando datos
    LET wfecemi = TODAY;
    LET wmestra = MONTH(wfecemi);
    LET waniotr = YEAR(wfecemi);
    LET wobserv = "DEVOLUCION APARTADO # "||wlnkapa||" DOCUMENTO # "||wnumdoc;
    LET westado = "V";

    -- Seleccionando datos
    INSERT INTO inv_mtransac
    VALUES (0                     ,  -- Id unico
            wcodemp               ,  -- Empresa
            wcodsuc               ,  -- Sucursal
            wcodbod               ,  -- Bodega
            wtipmov               ,  -- Tipo de movimiento
            0                     ,  -- Origen
            wcodcli               ,  -- Destino
            wlnktra               ,  -- ID factura
            wnumdoc               ,  -- Numero de documento de facturacion
            0                     ,  -- Movimiento es externo
            waniotr               ,  -- Anio
            wmestra               ,  -- Mes
            wfecemi               ,  -- Fecha de emision
            wtipope               ,  -- Tipo de operacion del movimiento
            wobserv               ,  -- Observaciones
            westado               ,  -- Estado V=igente
            NULL                  ,  -- Motivo de la anulacion
            NULL                  ,  -- Fecha de anulacion
            NULL                  ,  -- Hora de la anulacion
            NULL                  ,  -- Usuario anulo
            "S"                   ,  -- Documento impreso
            USER                  ,  -- Usuario registro
            CURRENT               ,  -- Fecha de registro
            CURRENT HOUR TO SECOND); -- Hora de registro

    -- Obteniendo serial de la tabla encabezado
    SELECT UNIQUE DBINFO("SQLCA.SQLERRD1")
     INTO  xlnktra
     FROM  inv_mtransac;

    -- Creando detalle de la transaccion
    FOREACH SELECT a.cditem,
                   a.codabr,
                   a.unimed,
                   a.cantid,
                   a.cantid,
                   a.preuni,
                   a.totpro,
                   "0",
                   a.correl
             INTO  xcditem,
                   xcodabr,
                   xcodepq,
                   xcanepq,
                   xcanuni,
                   xpreuni,
                   xtotpro,
                   xbarcod,
                   xcorrel
             FROM  fac_dtransac a
             WHERE a.lnktra = wlnktra

     -- Calculando cantdad y valor para calculo de saldo
     IF (wtipope=0) THEN
        LET xopeuni = (xcanuni*(-1));
        LET xopeval = (xtotpro*(-1));
     ELSE
        LET xopeuni = xcanuni;
        LET xopeval = xtotpro;
     END IF;

     -- Insertando detalle
     INSERT INTO inv_dtransac
     VALUES (xlnktra               ,  -- id transaccion
             wcodemp               ,  -- empresa
             wcodsuc               ,  -- sucursal
             wcodbod               ,  -- bodega
             wtipmov               ,  -- tipo de movimiento
             0                     ,  -- origen
             wcodcli               ,  -- destino
             wfecemi               ,  -- fecha de emision
             waniotr               ,  -- anio
             wmestra               ,  -- mes
             xcditem               ,  -- codigo de producto
             xcodabr               ,  -- codigo abreviado
             0                     ,  -- codigo de empaque
             xcanepq               ,  -- cantidad de empaque
             xcanuni               ,  -- cantidad
             xpreuni               ,  -- precio unitario
             xtotpro               ,  -- total del producto
             0                     ,  -- precio promedio
             xcorrel               ,  -- correlativo
             xbarcod               ,  -- codigo de barra
             wtipope               ,  -- tipo de operacion del movimniento
             westado               ,  -- estado
             xopeval               ,  -- total
             xopeuni               ,  -- cantidad
             USER                  ,  -- Usuario registro
             CURRENT               ,  -- Fecha de registro
             CURRENT HOUR TO SECOND); -- Hora de registro

    END FOREACH;

   END IF;

  END IF;

 END IF;
END PROCEDURE;
