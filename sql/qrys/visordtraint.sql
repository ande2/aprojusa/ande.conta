

drop view vis_ordentraint;

create view vis_ordentraint
(numpos,nombrepos,ordentrabajo,fechaorden,cliente,cantidad,clase,color,medidas,nombremedida,usuario,
   serie,documento,ordencompra,fechaofrecida,descripcion)

as

select a.numpos,f.nompos,a.lnkord,a.fecord,b.nomcli,a.cantid,c.nomsub,
       d.nomcol,a.xlargo||" X "||a.yancho,
       case (a.medida) when 1 then "PIES" when 0 then "METROS" end,
       e.usrope,e.nserie,e.numdoc,e.ordcmp,a.fecofe,a.descrp
from inv_ordentra a,fac_clientes b,inv_subcateg c,inv_colorpro d,
     fac_mtransac e,fac_puntovta f
where b.codcli = a.codcli
  and c.subcat = a.subcat
  and d.codcol = a.codcol
  and e.lnktra = a.lnktra
  and f.numpos = a.numpos; 

grant select on vis_ordentraint to public;
