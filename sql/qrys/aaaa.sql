create table "sistemas".fac_tipodocs 
  (
    tipdoc char(1) not null ,
    nomdoc varchar(40) not null ,
    impdoc char(1) not null ,
    blqueo char(1) not null ,
    passwd char(1) not null ,
    arqueo char(1) not null ,
    datfis char(1) not null ,
    coraut char(1) not null ,
    susrid char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (tipdoc)  constraint "sistemas".pkfactipodocs
  );

