
drop view vis_balanceapa;

create view vis_balanceapa
(numpos,nomcli,numtel,lnkapa,fecemi,totpro,salact,fecven,fulpag,observ,lnktra,cancel)
as
select a.numpos,
       b.nomcli,
       b.numtel,
       a.lnkapa,
       a.fecemi,
       a.totpro,
       a.salact,
       a.fecven,
       a.fulpag,
       a.observ,
       a.lnktra,
       a.cancel
from fac_apartads a,fac_clientes b
where a.codcli = b.codcli
and a.lnknid =0	and a.estado = 1;

grant select on vis_balanceapa to public;
