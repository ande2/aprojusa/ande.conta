DROP PROCEDURE "sistemas".sptrebajaconsumos;

CREATE PROCEDURE "sistemas".sptrebajaconsumos(wlnktra INTEGER,
                                              wcodemp SMALLINT,
                                              wcodsuc SMALLINT,
                                              wcodbod SMALLINT,
                                              wfecemi DATE,
                                              wtipope SMALLINT)

 -- Procedimiento para insertar los consumos de producto automaticos en el inventario
 -- Mynor Ramirez
 -- Juio 2012

 DEFINE wtipmov  LIKE inv_mtransac.tipmov;
 DEFINE waniotr  LIKE inv_mtransac.aniotr;
 DEFINE wmestra  LIKE inv_mtransac.mestra;
 DEFINE wobserv  LIKE inv_mtransac.observ;
 DEFINE westado  LIKE inv_mtransac.estado;
 DEFINE wchkinv  LIKE fac_puntovta.chkinv;
 DEFINE xlnktra  LIKE inv_dtransac.lnktra;
 DEFINE xcodabr  LIKE inv_dtransac.codabr;
 DEFINE xnserie  LIKE inv_dtransac.nserie;
 DEFINE xcodepq  LIKE inv_dtransac.codepq;
 DEFINE xcanepq  LIKE inv_dtransac.canepq;
 DEFINE xpreuni  LIKE inv_dtransac.preuni;
 DEFINE xtotpro  LIKE inv_dtransac.totpro;
 DEFINE xcorrel  LIKE inv_dtransac.correl;
 DEFINE xbarcod  LIKE inv_dtransac.barcod;
 DEFINE xopeval  LIKE inv_dtransac.opeval;
 DEFINE xopeuni  LIKE inv_dtransac.opeuni;
 DEFINE xtipmov  LIKE inv_dtransac.tipmov;
 DEFINE xnumdoc  LIKE inv_mtransac.numrf1;
 DEFINE xtipope  LIKE inv_dtransac.tipope;
 DEFINE xcditem  LIKE inv_dtransac.cditem;
 DEFINE xcoddes  LIKE inv_mtransac.coddes;
 DEFINE xnomdes  LIKE inv_mtransac.nomdes;
 DEFINE wcditem  LIKE inv_dtransac.cditem;
 DEFINE wcanuni  LIKE inv_dtransac.canuni;
 DEFINE xcanuni  DEC(14,6);
 DEFINE weximov  SMALLINT;
 DEFINE conteo   INT;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Verificando si tipo de movimiento es cargo 
 IF (wtipope=1) THEN

   -- Eliminando movimiento de consumo
   SET LOCK MODE TO WAIT;
   DELETE FROM inv_mtransac
   WHERE inv_mtransac.lnkcon = wlnktra;

   -- Obteniendo tipo de movimiento default de consumos automaticos
   SELECT NVL(a.valchr,0)
    INTO  wtipmov
    FROM  glb_paramtrs a
    WHERE a.numpar = 13
      AND a.tippar = 2;

   -- Seleccionando tipo de movimiento
   SELECT NVL(a.tipmov,0),NVL(a.tipope,0)
    INTO  xtipmov,xtipope
    FROM  inv_tipomovs a
    WHERE a.tipmov = wtipmov;

   -- Verificando si existen datos
   IF (xtipmov>0) THEN

    -- Verificando si movimiento tiene productos que contengan subproductos
    SELECT COUNT(*)
     INTO  conteo
     FROM  inv_dtransac d ,inv_dproduct s
     WHERE d.lnktra = wlnktra
       AND s.cditem = d.cditem
       AND s.cantid >0;

    IF (conteo>0) THEN
     -- Creando encabezado
     -- Seleccionando datos
     SELECT NVL(a.numrf1,"0"),NVL(a.coddes,0),a.nomdes
      INTO  xnumdoc,xcoddes,xnomdes
      FROM  inv_mtransac a
      WHERE a.lnktra = wlnktra;

     -- Asignando datos
     LET wmestra = MONTH(wfecemi);
     LET waniotr = YEAR(wfecemi);
     LET wobserv = "CONSUMO AUTOMATICO";
     LET xbarcod = "";
     LET westado = "V";
     LET xnserie = "";
     LET xpreuni = 0;

     -- Seleccionando datos
     SET LOCK MODE TO WAIT;
     INSERT INTO inv_mtransac
     VALUES (0                     ,  -- Id unico
             wcodemp               ,  -- Empresa
             wcodsuc               ,  -- Sucursal
             wcodbod               ,  -- Bodega
             xtipmov               ,  -- Tipo de movimiento
             0                     ,  -- Origen
             ""                    ,  -- Nombre del origen
             xcoddes               ,  -- Destino
             xnomdes               ,  -- Nombre del Destino
             wlnktra               ,  -- ID Movimiento
             xnumdoc               ,  -- Numero de documento
             0                     ,  -- Movimiento es externo
             waniotr               ,  -- Anio
             wmestra               ,  -- Mes
             wfecemi               ,  -- Fecha de emision
             xtipope               ,  -- Tipo de operacion del movimiento
             wobserv               ,  -- Observaciones
             westado               ,  -- Estado V=igente
             wlnktra               ,  -- Numero id de transaccion que origino el consumo
             0                     ,  -- Consumo automatico
             NULL                  ,  -- Motivo de la anulacion
             NULL                  ,  -- Fecha de anulacion
             NULL                  ,  -- Hora de la anulacion
             NULL                  ,  -- Usuario anulo
             "S"                   ,  -- Documento impreso
             USER                  ,  -- Usuario registro
             CURRENT               ,  -- Fecha de registro
             CURRENT HOUR TO SECOND); -- Hora de registro

     -- Obteniendo serial de la tabla encabezado
     SELECT UNIQUE DBINFO("SQLCA.SQLERRD1")
      INTO  xlnktra
      FROM  inv_mtransac;

    -- Seleccionando productos que tiene subproductos
    -- Verificando si movimiento tiene productos que contengan subproductos
    FOREACH SELECT a.cditem,
                   a.canuni,
                   b.citems,
                   b.codabr,
                   b.cantid
             INTO  wcditem,
                   wcanuni,
                   xcditem,
                   xcodabr,
                   xcanuni
             FROM  inv_dtransac a,inv_dproduct b
             WHERE a.lnktra = wlnktra
               AND b.cditem = a.cditem
               AND b.cantid >0

      -- Obteniendo correlativo de producto de la transaccion
      SELECT (NVL(MAX(a.correl),0)+1)
       INTO  xcorrel
       FROM  inv_dtransac a
       WHERE a.lnktra = xlnktra;

      -- Calculando cantidad y valor para calculo de saldo
      LET xcanuni = (xcanuni*wcanuni);
      LET xcanepq = xcanuni;
      LET xtotpro = 0;

      IF (xtipope=0) THEN
         LET xopeuni = (xcanuni*(-1));
         LET xopeval = (xtotpro*(-1));
      ELSE
         LET xopeuni = xcanuni;
         LET xopeval = xtotpro;
      END IF;

      -- Insertando detalle
      SET LOCK MODE TO WAIT;
      INSERT INTO inv_dtransac
      VALUES (xlnktra               ,  -- id transaccion
              wcodemp               ,  -- empresa
              wcodsuc               ,  -- sucursal
              wcodbod               ,  -- bodega
              xtipmov               ,  -- tipo de movimiento
              0                     ,  -- origen
              xcoddes               ,  -- destino
              wfecemi               ,  -- fecha de emision
              waniotr               ,  -- anio
              wmestra               ,  -- mes
              xcditem               ,  -- codigo de producto
              xcodabr               ,  -- codigo abreviado
              xnserie               ,  -- numero de serie
              0                     ,  -- codigo de empaque
              xcanepq               ,  -- cantidad de empaque
              xcanuni               ,  -- cantidad
              xpreuni               ,  -- precio unitario
              xtotpro               ,  -- total del producto
              0                     ,  -- precio promedio
              xcorrel               ,  -- correlativo
              xbarcod               ,  -- codigo de barra
              xtipope               ,  -- tipo de operacion del movimniento
              westado               ,  -- estado
              xopeval               ,  -- total
              xopeuni               ,  -- cantidad
              1                     ,  -- actualiza existencia automaticamente 
              USER                  ,  -- Usuario registro
              CURRENT               ,  -- Fecha de registro
              CURRENT HOUR TO SECOND); -- Hora de registro

    END FOREACH;

   END IF;

  END IF;

 END IF;
END PROCEDURE;
