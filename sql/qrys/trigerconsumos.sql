
drop trigger "sistemas".trginsertarproducto;
create trigger "sistemas".trginsertarproducto insert on "sistemas"
    .inv_dtransac referencing new as pos
    for each row
       (
        execute procedure sptrebajaconsumos(pos.lnktra,pos.codemp,pos.codsuc,pos.codemp,
          pos.fecemi,pos.cditem,pos.canuni,pos.tipope)
        );
