drop trigger trgingresosxpeso;
create trigger trgingresosxpeso insert on inv_pesajpro referencing new as pos
    for each row
     when (pos.ixpeso=1)
       (
         execute procedure sptingresosxpeso(1,1,1,
         pos.fecpro,1,1,"COSTAS BURGER CENTRAL",pos.cditem,
         pos.codabr,pos.numetq,pos.canpes,pos.cxpeso),

         execute procedure sptsalidasxpeso(1,1,1,
         pos.fecpro,0,11,"COSTAS BURGER CENTRAL",pos.cditma,
         pos.codaba,pos.numetq,pos.canpes)

       );
