DROP PROCEDURE "sistemas".sptsaldosxproducto;
CREATE PROCEDURE "sistemas".sptsaldosxproducto(wcodemp SMALLINT,wcodsuc SMALLINT,
                               wcodbod SMALLINT,wtipmov SMALLINT,waniotr SMALLINT,
                               wmestra SMALLINT,wcditem INT,wtipope SMALLINT,
                               wcodabr VARCHAR(20),wpreuni DEC(14,6))

 -- Procedimiento para calcular y registrar el saldo x producto
 -- Los saldos son por empresa, sucursal, bodega, y producto
 -- Mynor Ramirez
 -- Junio 2012    

 DEFINE xcanuni  LIKE inv_proenbod.exican;
 DEFINE xcospro  LIKE inv_proenbod.cospro;
 DEFINE xtotpro  LIKE inv_proenbod.exival;
 DEFINE xcodemp  LIKE inv_proenbod.codemp;
 DEFINE xfulent  LIKE inv_proenbod.fulent;
 DEFINE xfulsal  LIKE inv_proenbod.fulsal;
 DEFINE whayval  LIKE inv_tipomovs.hayval;

-- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Calculando saldo en base a movimientos
 CALL fxsaldomovimientos(wcodemp,wcodsuc,wcodbod,waniotr,wmestra,wcditem)
 RETURNING xcanuni,xcospro,xtotpro,xfulent,xfulsal;

 -- Verificando si producto ya existe en saldos x bodega
 LET xcodemp = NULL;
 SELECT UNIQUE a.codemp
  INTO  xcodemp
  FROM  inv_proenbod a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.cditem = wcditem;

 -- Si ya existe el saldo del producto lo actualiza, sino lo inserta
 IF xcodemp IS NOT NULL THEN
    -- Actualizando
    SET LOCK MODE TO WAIT;
    UPDATE inv_proenbod
    SET    inv_proenbod.fulent = xfulent,
           inv_proenbod.fulsal = xfulsal,
           inv_proenbod.exican = xcanuni,
           inv_proenbod.exival = xtotpro,
           inv_proenbod.cospro = xcospro
    WHERE  inv_proenbod.codemp = wcodemp
      AND  inv_proenbod.codsuc = wcodsuc
      AND  inv_proenbod.codbod = wcodbod
      AND  inv_proenbod.cditem = wcditem;
 ELSE
    -- Insertando
    SET LOCK MODE TO WAIT;
    INSERT INTO inv_proenbod
    VALUES (wcodemp,
            wcodsuc,
            wcodbod,
            wcditem,
            wcodabr,
            0,
            0,
            xfulent,
            xfulsal,
            xcanuni,
            xtotpro,
            xcospro,
            USER,
            CURRENT,
            CURRENT HOUR TO SECOND);
 END IF

 -- Actualizando existencia total del producto tomando en cuenta todas las
 -- bodegas
 -- Totalizando existencia
 SELECT NVL(SUM(a.exican),0)
  INTO  xcanuni
  FROM  inv_proenbod a
  WHERE (a.cditem = wcditem);

 -- Actualizando ultimo precio de compra si el tipo de movimiento esta parametrizado
 LET whayval = 0;
 SELECT NVL(a.hayval,0)
  INTO  whayval
  FROM  inv_tipomovs a
  WHERE a.tipmov = wtipmov;

 -- Verificando si tipo de movimiento actualiza valores
 IF (whayval=1) THEN
    -- Actualizando producto
    SET LOCK MODE TO WAIT;
    UPDATE inv_products
    SET    inv_products.pulcom = wpreuni,
           inv_products.exiact = xcanuni
    WHERE  inv_products.cditem = wcditem;
 ELSE
    -- Actualizando existencia
    SET LOCK MODE TO WAIT;
    UPDATE inv_products
    SET    inv_products.exiact = xcanuni
    WHERE  inv_products.cditem = wcditem;
 END IF
END PROCEDURE;
