{create table "sistemas".his_products
  (
    lnkpro serial not null ,
    cditem integer not null ,
    codcat smallint not null ,
    subcat smallint not null ,
    codcol smallint not null ,
    codprv integer not null ,
    codmed integer not null ,
    unimed smallint not null ,
    dsitem char(100) not null ,
    codabr char(20) not null ,
    premin decimal(12,2) not null ,
    estado char(1) not null ,
    status smallint,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    usract char(15) not null ,
    fecact date not null ,
    horact datetime hour to second not null ,
    primary key (lnkpro)  constraint "sistemas".pkhisproducts
  );}

create trigger "sistemas".trgupdateproductos update of fulcam 
    on "sistemas".inv_products referencing old as pre new as pos
    
    for each row
        (
        insert into "sistemas".his_products (lnkpro,cditem,codcat,
    subcat,codcol,codprv,codmed,unimed,dsitem,codabr,premin,estado,status,
    userid,fecsis,horsis,usract,fecact,horact)  values (0 ,pre.cditem 
    ,pre.codcat ,pre.subcat ,pre.codcol ,pre.codprv ,pre.codmed ,pre.unimed 
    ,pre.dsitem ,pre.codabr ,pre.premin ,pre.estado ,pre.status ,pre.userid 
    ,pre.fecsis ,pre.horsis ,USER ,CURRENT year to fraction(3) ,CURRENT 
    hour to second ));



