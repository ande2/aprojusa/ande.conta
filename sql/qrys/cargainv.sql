
{
create table "sistemas".inv_dtransac
  (
    lnktra integer not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    tipmov smallint not null ,
    codori integer not null ,
    coddes integer not null ,
    fecemi date,
    aniotr smallint,
    mestra smallint,
    cditem integer not null ,
    codabr varchar(20,1) not null ,
    codepq smallint not null ,
    canepq decimal(14,2) not null ,
    canuni decimal(14,2) not null ,
    preuni decimal(14,6) not null ,
    totpro decimal(14,2) not null ,
    prepro decimal(14,2) not null ,
    correl smallint not null ,
    barcod varchar(20),
    tipope smallint not null ,
    estado char(1) not null ,
    opeval decimal(14,2) not null ,
    opeuni decimal(14,2) not null ,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null
}

insert into inv_dtransac 
SELECT 1,codemp,codsuc,codbod,1,1,0,"07052011",2011,5,cditem,codabr,0,canuni,canuni,0,0,0,1,codabr,1,"V",0,canuni,user,current,
       current 
from inv_tofisico
