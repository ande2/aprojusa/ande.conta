
DROP FUNCTION  "sistemas".fxsaldomovimientos; 
 
CREATE FUNCTION  "sistemas".fxsaldomovimientos(wcodemp SMALLINT,wcodsuc SMALLINT,wcodbod SMALLINT,
                                    waniotr SMALLINT,wmestra SMALLINT,wcditem VARCHAR(20))
 RETURNING DEC(16,2),DEC(14,6),DEC(16,2),DATE,DATE

 DEFINE spcanuni LIKE inv_proenbod.exican;
 DEFINE spcospro LIKE inv_proenbod.cospro;
 DEFINE sptotpro LIKE inv_proenbod.exival;
 DEFINE spopeuni LIKE inv_proenbod.exican;
 DEFINE spopeval LIKE inv_proenbod.exival;
 DEFINE xcanuni  LIKE inv_proenbod.exican;
 DEFINE xcospro  LIKE inv_proenbod.cospro;
 DEFINE xtotpro  LIKE inv_proenbod.exival;
 DEFINE xfulent  LIKE inv_proenbod.fulent;
 DEFINE xfulsal  LIKE inv_proenbod.fulsal;
 DEFINE aniant   SMALLINT;
 DEFINE mesant   SMALLINT;
 DEFINE conteo   INTEGER;

 -- Funcion para calcular el saldo de un producto por sus movimientos
 -- Los saldos son por empresa, sucursal, bodega y producto
 -- Mynor Ramirez
 -- Diciembre 2010

 -- Definiendo nivel de aislamiento
 SET ISOLATION COMMITTED READ;

 -- Calculando saldo de movimientos
 SELECT NVL(SUM(a.opeuni),0),
        NVL(SUM(a.opeval),0)
  INTO  spopeuni,
        spopeval
  FROM  inv_dtransac a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.cditem = wcditem
    AND a.tipope >= 0 
    AND a.estado = "V" 
    AND a.actexi = 1;

 -- Calculando fecha de ultima entrada
 SELECT MAX(a.fecemi)
  INTO  xfulent
  FROM  inv_dtransac a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.cditem = wcditem
    AND a.tipope = 1
    AND a.estado = "V"
    AND a.actexi = 1;

 -- Calculando fecha de ultima salida
 SELECT MAX(a.fecemi)
  INTO  xfulsal
  FROM  inv_dtransac a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.cditem = wcditem
    AND a.tipope = 0
    AND a.estado = "V"
    AND a.actexi = 1;

 -- Asignando saldos
 LET xcanuni = (spopeuni);
 LET xtotpro = (spopeval);

 -- Calculando costo promedio
 LET xcospro = 0;
 IF (xcanuni>0) THEN
    LET xcospro = (xtotpro/xcanuni);
 END IF

 -- Verificando nulos
 IF xcanuni IS NULL THEN LET xcanuni = 0; END IF;
 IF xcospro IS NULL THEN LET xcospro = 0; END IF;
 IF xtotpro IS NULL THEN LET xtotpro = 0; END IF;

 RETURN xcanuni,xcospro,xtotpro,xfulent,xfulsal;
END FUNCTION;
