


drop view vis_balanceapa;

create view vis_balanceapa
(nomcli,numtel,lnkapa,fecemi,totpro,salact,fecven,fulpag,observ)
as
select b.nomcli,
       b.numtel,
       a.lnkapa,
       a.fecemi,
       a.totpro,
       a.salact,
       a.fecemi+60,
       (select max(x.fecemi) from fac_apartads x
       where x.lnknid = a.lnkapa),
       a.observ
from fac_apartads a,fac_clientes b
where a.codcli = b.codcli
and a.lnknid =0	and a.estado = 1;

grant select on vis_balanceapa to public;
