

drop trigger "sistemas".trgdeleteapa; 

create trigger "sistemas".trgdeleteapa delete on "sistemas".fac_mtransac 
    referencing old as pre
    for each row
            (
            delete from fac_apartads 
            where fac_apartads.lnktra = pre.lnktra,

            execute procedure "sistemas".stpsaldoapartados(pre.lnkapa 
    ));
