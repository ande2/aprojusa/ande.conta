
DBSCHEMA Schema Utility       INFORMIX-SQL Version 10.00.UC1    
Copyright IBM Corporation 1996, 2004 All rights reserved
Software Serial Number AAA#B000000
{ TABLE "sistemas".fac_puntovta row size = 77 number of columns = 10 index size = 
              7 }
create table "sistemas".fac_puntovta 
  (
    numpos smallint not null ,
    nompos varchar(40) not null ,
    chkinv smallint not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    chkexi smallint not null ,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (chkexi IN (1 ,0 )) constraint "sistemas".ckfacpuntovta1,
    
    check (chkinv IN (1 ,0 )) constraint "sistemas".ckfacpuntovta2,
    primary key (numpos)  constraint "sistemas".pkfacpuntovta
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".fac_puntovta from "public";







DBSCHEMA Schema Utility       INFORMIX-SQL Version 10.00.UC1    
Copyright IBM Corporation 1996, 2004 All rights reserved
Software Serial Number AAA#B000000
{ TABLE "sistemas".fac_mtransac row size = 445 number of columns = 33 index size 
              = 9 }
create table "sistemas".fac_mtransac 
  (
    lnktra serial not null ,
    numpos smallint not null ,
    codemp smallint not null ,
    lnktdc integer not null ,
    tipdoc smallint not null ,
    nserie varchar(5) not null ,
    numdoc integer not null ,
    fecemi date not null ,
    codcli integer not null ,
    numnit varchar(20) not null ,
    nomcli varchar(60) not null ,
    dircli varchar(100) not null ,
    exenta smallint not null ,
    moneda smallint not null ,
    tascam decimal(14,6) not null ,
    efecti decimal(14,2) not null ,
    cheque decimal(14,2) not null ,
    tarcre decimal(14,2) not null ,
    exciva decimal(14,2) not null ,
    subtot decimal(14,2) not null ,
    totdoc decimal(14,2) not null ,
    totiva decimal(14,2) not null ,
    poriva decimal(5,2) not null ,
    estado char(1) not null ,
    feccor date,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    motanl varchar(100,1),
    usranl varchar(15),
    fecanl date,
    horanl datetime hour to second,
    lnkinv integer not null ,
    
    check (estado IN ('V' ,'A' )) constraint "sistemas".ckfacmtransac1,
    
    check (numdoc > 0 ) constraint "sistemas".ckfacmtransac2,
    
    check (fecemi > DATE ('01/01/2011' ) ) constraint "sistemas".ckfacmtransac3,
    
    check (efecti >= 0. ) constraint "sistemas".ckfacmtransac4,
    
    check (cheque >= 0. ) constraint "sistemas".ckfacmtransac5,
    
    check (tarcre >= 0. ) constraint "sistemas".ckfacmtransac6,
    
    check (subtot >= 0. ) constraint "sistemas".ckfacmtransac7,
    
    check (totdoc >= 0. ) constraint "sistemas".ckfacmtransac8,
    
    check (poriva >= 0. ) constraint "sistemas".ckfacmtransac10,
    
    check (totiva >= 0. ) constraint "sistemas".ckfacmtransac11,
    primary key (lnktra)  constraint "sistemas".pkfacmtransac
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".fac_mtransac from "public";







DBSCHEMA Schema Utility       INFORMIX-SQL Version 10.00.UC1    
Copyright IBM Corporation 1996, 2004 All rights reserved
Software Serial Number AAA#B000000
{ TABLE "sistemas".fac_dtransac row size = 69 number of columns = 12 index size = 
              9 }
create table "sistemas".fac_dtransac 
  (
    lnktra integer not null ,
    correl smallint not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    cditem integer not null ,
    codabr char(20) not null ,
    codepq smallint not null ,
    canepq decimal(14,6) not null ,
    cantid decimal(14,6) not null ,
    preuni decimal(12,2) not null ,
    totpro decimal(14,2) not null ,
    
    check (correl > 0 ) constraint "sistemas".ckfacdtransac1,
    
    check (canepq > 0.000000 ) constraint "sistemas".ckfacdtransac2,
    
    check (cantid > 0.000000 ) constraint "sistemas".ckfacdtransac3,
    
    check (totpro >= 0.00 ) constraint "sistemas".ckfacdtransac4
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".fac_dtransac from "public";



alter table "sistemas".fac_dtransac add constraint (foreign key 
    (lnktra) references "sistemas".fac_mtransac  on delete cascade 
    constraint "sistemas".fkfacmtransac1);





DBSCHEMA Schema Utility       INFORMIX-SQL Version 10.00.UC1    
Copyright IBM Corporation 1996, 2004 All rights reserved
Software Serial Number AAA#B000000
{ TABLE "sistemas".fac_tipodocs row size = 71 number of columns = 8 index size = 
              7 }
create table "sistemas".fac_tipodocs 
  (
    tipdoc smallint not null ,
    nomdoc varchar(40) not null ,
    nomabr char(6) not null ,
    exeimp smallint not null ,
    hayimp smallint not null ,
    userid char(10) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    
    check (exeimp IN (0 ,1 )) constraint "sistemas".ckfactipodocs1,
    
    check (hayimp IN (0 ,1 )) constraint "sistemas".ckfactipodocs2,
    primary key (tipdoc)  constraint "sistemas".pkfactipodocs
  )  extent size 16 next size 16 lock mode row;
revoke all on "sistemas".fac_tipodocs from "public";







DBSCHEMA Schema Utility       INFORMIX-SQL Version 10.00.UC1    
Copyright IBM Corporation 1996, 2004 All rights reserved
Software Serial Number AAA#B000000
{ TABLE "sistemas".fac_tdocxpos row size = 48 number of columns = 10 index size = 
              30 }
create table "sistemas".fac_tdocxpos 
  (
    lnktdc serial not null ,
    numpos smallint not null ,
    codemp smallint not null ,
    tipdoc smallint not null ,
    nserie char(10) not null ,
    numcor integer not null ,
    estado char(1) not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (numpos,codemp,tipdoc,nserie)  constraint "sistemas".uqfactdocxpos,
    
    check (estado IN ('A' ,'B' )) constraint "sistemas".ckfactdocxpos1,
    primary key (lnktdc)  constraint "sistemas".pkfactdocxpos
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".fac_tdocxpos from "public";







DBSCHEMA Schema Utility       INFORMIX-SQL Version 10.00.UC1    
Copyright IBM Corporation 1996, 2004 All rights reserved
Software Serial Number AAA#B000000
{ TABLE "sistemas".fac_tarjetas row size = 37 number of columns = 6 index size = 
              0 }
create table "sistemas".fac_tarjetas 
  (
    lnktra integer not null ,
    correl smallint not null ,
    codemi smallint not null ,
    ntacre char(20) not null ,
    numvou integer not null ,
    totval decimal(8,2) not null ,
    
    check (correl > 0 ) constraint "sistemas".ckfactarjetas1,
    
    check (numvou > 0 ) constraint "sistemas".ckfactarjetas2,
    
    check (totval > 0.00 ) constraint "sistemas".ckfactarjetas3
  )  extent size 16 next size 16 lock mode page;
revoke all on "sistemas".fac_tarjetas from "public";






