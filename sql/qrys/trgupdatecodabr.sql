create trigger "sistemas".trgupdatecodabr update of codabr
    on "sistemas".inv_products referencing old as pre new as pos

    for each row
        (
         update inv_proenbod
         set    inv_proenbod.codabr = pos.codabr
         where  inv_proenbod.cditem = pre.cditem,

         update inv_dtransac
         set    inv_dtransac.codabr = pos.codabr
         where  inv_dtransac.cditem = pre.cditem,

         update fac_dtransac
         set    fac_dtransac.codabr = pos.codabr
         where  fac_dtransac.cditem = pre.cditem,

         update inv_tofisico
         set    inv_tofisico.codabr = pos.codabr
         where  inv_tofisico.cditem = pre.cditem,

         update inv_scfisico
         set    inv_scfisico.codabr = pos.codabr
         where  inv_scfisico.cditem = pre.cditem
        );
