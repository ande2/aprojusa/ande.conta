
create trigger "sistemas".trgupdatepreciosugerido update of presug on 
    "sistemas".inv_products referencing old as pre new as pos
    for each row
        (
        insert into "sistemas".his_precioss (lnkpre,cditem,preant,
    presug,userid,fecsis,horsis)  values (0 ,pre.cditem ,pre.presug ,
    pos.presug ,USER ,CURRENT year to fraction(3) ,CURRENT hour to second 
    ));
