

DROP   PROCEDURE "sistemas".sptempaquedefault;

CREATE PROCEDURE "sistemas".sptempaquedefault(wcditem INT)
 -- Procedimiento para crear el empaque default de un producto despues de grabarlo en la tabla de empaques por producto
 -- Mynor Ramirez
 -- Diciembre 2010

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Borrando empaque antes de grabar
 SET LOCK MODE TO WAIT;
 DELETE FROM inv_epqsxpro
 WHERE inv_epqsxpro.cditem = wcditem;

 -- Creando empaque default
 INSERT INTO inv_epqsxpro
 SELECT 0,a.cditem,0,b.nommed,1,USER,CURRENT,CURRENT HOUR TO SECOND
  FROM  inv_products a,inv_unimedid b
  WHERE b.unimed = a.unimed
    AND a.cditem = wcditem;

END PROCEDURE;
