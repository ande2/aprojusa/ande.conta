
drop table fac_mtransac;
create table "sistemas".fac_mtransac 
  (
    lnktra serial not null ,
    numpos smallint not null ,
    codemp smallint not null ,
    lnktdc integer not null ,
    tipdoc smallint not null ,
    nserie varchar(5) not null ,
    numdoc integer not null ,
    fecemi date not null ,
    codcli integer not null ,
    numnit varchar(20) not null ,
    nomcli varchar(60) not null ,
    dircli varchar(100) not null ,
    credit smallint not null ,
    hayord smallint not null ,
    exenta smallint not null ,
    moneda smallint not null ,
    tascam decimal(14,6) not null ,
    efecti decimal(14,2) not null ,
    cheque decimal(14,2) not null ,
    tarcre decimal(14,2) not null ,
    ordcom decimal(14,2) not null ,
    depmon decimal(14,2) not null ,
    subtot decimal(14,2) not null ,
    totdoc decimal(14,2) not null ,
    totiva decimal(14,2) not null ,
    poriva decimal(5,2) not null ,
    totpag decimal(14,2) not null ,
    saldis decimal(14,2) not null ,
    estado char(1) not null ,
    feccor date,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    motanl varchar(100,1),
    usranl varchar(15),
    fecanl date,
    horanl datetime hour to second,
    lnkinv integer not null ,
    
    check (estado IN ('V' ,'A' )) constraint "sistemas".ckfacmtransac1,
    
    check (numdoc > 0 ) constraint "sistemas".ckfacmtransac2,
    
    check (fecemi > DATE ('01/01/2011' ) ) constraint "sistemas".ckfacmtransac3,
    
    check (efecti >= 0. ) constraint "sistemas".ckfacmtransac4,
    
    check (cheque >= 0. ) constraint "sistemas".ckfacmtransac5,
    
    check (tarcre >= 0. ) constraint "sistemas".ckfacmtransac6,
    
    check (subtot >= 0. ) constraint "sistemas".ckfacmtransac7,
    
    check (totdoc >= 0. ) constraint "sistemas".ckfacmtransac8,
    
    check (poriva >= 0. ) constraint "sistemas".ckfacmtransac10,
    
    check (totiva >= 0. ) constraint "sistemas".ckfacmtransac11,
    primary key (lnktra)  constraint "sistemas".pkfacmtransac
  );


create trigger "sistemas".trganulaordent update of estado on 
    "sistemas".fac_mtransac referencing new as pos
    for each row
        (
        update "sistemas".inv_ordentra set "sistemas".inv_ordentra.estado 
    = pos.estado  where (lnktra = pos.lnktra ) );

drop table "sistemas".fac_tarjetas;
create table "sistemas".fac_tarjetas 
  (
    lnktra integer not null ,
    correl smallint not null ,
    codemi smallint not null ,
    ntacre char(20) not null ,
    numvou integer not null ,
    totval decimal(8,2) not null ,
    
    check (correl > 0 ) constraint "sistemas".ckfactarjetas1,
    
    check (numvou > 0 ) constraint "sistemas".ckfactarjetas2,
    
    check (totval > 0. ) constraint "sistemas".ckfactarjetas3
  );

alter table "sistemas".fac_tarjetas add constraint (foreign key 
    (lnktra) references "sistemas".fac_mtransac  on delete cascade 
    constraint "sistemas".fkfacmtransac2);

drop table "sistemas".fac_dtransac;
create table "sistemas".fac_dtransac 
  (
    lnktra integer not null ,
    correl smallint not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    cditem integer not null ,
    codabr char(20) not null ,
    unimed smallint not null ,
    canori decimal(14,2) not null ,
    unimto smallint not null ,
    cantid decimal(14,2) not null ,
    preuni decimal(12,2) not null ,
    totpro decimal(14,2) not null ,
    factor decimal(14,6) not null ,
    
    check (correl > 0 ) constraint "sistemas".ckfacdtransac1,
    
    check (totpro >= 0. ) constraint "sistemas".ckfacdtransac4
  );

alter table "sistemas".fac_dtransac add constraint (foreign key 
    (lnktra) references "sistemas".fac_mtransac  on delete cascade 
    constraint "sistemas".fkfacmtransac1);

drop table "sistemas".inv_ordentra;
create table "sistemas".inv_ordentra 
  (
    lnkord serial not null ,
    lnktra integer not null ,
    numord char(15) not null ,
    codcli smallint not null ,
    fecord date not null ,
    subcat smallint not null ,
    codcol smallint not null ,
    cantid decimal(12,2) not null ,
    xlargo decimal(5,2) not null ,
    yancho decimal(5,2) not null ,
    descrp char(100) not null,
    observ char(100),
    premed decimal(9,6) not null ,
    totord decimal(14,2) not null ,
    totabo decimal(14,2) not null ,
    salord decimal(14,2) not null ,
    fecofe date not null ,
    fecent date not null ,
    doctos char(100) not null ,
    obspre char(100),
    precio smallint not null ,
    compre char(50),
    tipord smallint not null ,
    estado char(1) not null ,
    nofase smallint not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (lnkord)  constraint "sistemas".pkinvordentra
  );

alter table "sistemas".inv_ordentra add constraint (foreign key 
    (lnktra) references "sistemas".fac_mtransac  on delete cascade 
    constraint "sistemas".fkfacmtransac3);

