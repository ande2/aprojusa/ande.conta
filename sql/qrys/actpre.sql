update inv_products
set presug = (select a.precio from precios a
               where a.item   = inv_products.codabr)
where exists (select b.item   from precios b
               where b.item   = inv_products.codabr)
