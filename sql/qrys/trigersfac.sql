
create trigger "sistemas".trganulaordent update of estado on 
    "sistemas".fac_mtransac referencing new as pos
    for each row
        (
        update "sistemas".inv_ordentra set "sistemas".inv_ordentra.estado 
    = pos.estado  where (lnktra = pos.lnktra ) ,

        update "sistemas".inv_mtransac set "sistemas".inv_mtransac.estado 
    = pos.estado ,"sistemas".inv_mtransac.motanl = pos.motanl ,"sistemas"
    .inv_mtransac.usranl = pos.usranl ,"sistemas".inv_mtransac.fecanl 
    = pos.fecanl ,"sistemas".inv_mtransac.horanl = pos.horanl  where ((numrf1 
    = pos.lnktra ) AND (tipmov = 1 ) ) );

create trigger "sistemas".trgupdateinventario update of lnkinv 
    on "sistemas".fac_mtransac referencing old as pre new as pos
    
    for each row
        when ((pos.hayord = 0 ) )
            (
            execute procedure "sistemas".sptinsertafacturas(pos.lnktra 
    ,pos.numpos ,pos.codemp ,pos.numdoc ,pos.fecemi ,pos.codcli ));

create trigger "sistemas".trgdeleteapa delete on "sistemas".fac_mtransac 
    referencing old as pre
    for each row
        when ((pre.lnkapa > 0 ) )
            (
            execute procedure "sistemas".stpsaldoapartados(pre.lnkapa 
    ));

create trigger "sistemas".trginsertapa insert on "sistemas".fac_mtransac 
    referencing new as pos
    for each row
        when ((pos.lnkapa > 0 ) )
            (
            execute procedure "sistemas".stpsaldoapartados(pos.lnkapa 
    ));

create trigger "sistemas".trgupdateapa update of lnkapa on "sistemas"
    .fac_mtransac referencing new as pos
    for each row
        when ((pos.lnkapa > 0 ) )
            (
            execute procedure "sistemas".stpsaldoapartados(pos.lnkapa 
    ));



