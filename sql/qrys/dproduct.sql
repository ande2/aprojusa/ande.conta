

drop table "sistemas".inv_dproduct;
create table "sistemas".inv_dproduct
  (
    cditem integer not null ,
    citems integer,
    codabr varchar(20,1) not null ,
    cantid decimal(14,6) not null ,
    correl smallint not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null,
    unique (cditem,citems) constraint uqinvdproduct
  )  extent size 16 next size 16 lock mode row;
