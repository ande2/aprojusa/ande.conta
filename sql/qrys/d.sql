droip proceduRE "sistemas".sptingresosxpeso;

CREATE PROCEDURE "sistemas".sptingresosxpeso(wcodemp SMALLINT,
                                             wcodsuc SMALLINT,
                                             wcodbod SMALLINT,
                                             wfecemi DATE,
                                             wtipope SMALLINT,
                                             wcodori SMALLINT,
                                             wnomori VARCHAR(100),
                                             wcditem INT,
                                             wcodabr VARCHAR(20),
                                             wnumetq VARCHAR(20),
                                             wcanuni DEC(12,2))

 -- Procedimiento para insertar un movimiento de ingreso x peso automaticamente
 -- Mynor Ramirez
 -- Agosto 2012

 DEFINE wtipmov  LIKE inv_mtransac.tipmov;
 DEFINE waniotr  LIKE inv_mtransac.aniotr;
 DEFINE wmestra  LIKE inv_mtransac.mestra;
 DEFINE wobserv  LIKE inv_mtransac.observ;
 DEFINE westado  LIKE inv_mtransac.estado;
 DEFINE wchkinv  LIKE fac_puntovta.chkinv;
 DEFINE wlnktra  LIKE inv_dtransac.lnktra;
 DEFINE xlnktra  LIKE inv_dtransac.lnktra;
 DEFINE xcodabr  LIKE inv_dtransac.codabr;
 DEFINE xnserie  LIKE inv_dtransac.nserie;
 DEFINE xcodepq  LIKE inv_dtransac.codepq;
 DEFINE xcanepq  LIKE inv_dtransac.canepq;
 DEFINE xpreuni  LIKE inv_dtransac.preuni;
 DEFINE xtotpro  LIKE inv_dtransac.totpro;
 DEFINE xcorrel  LIKE inv_dtransac.correl;
 DEFINE xbarcod  LIKE inv_dtransac.barcod;
 DEFINE xopeval  LIKE inv_dtransac.opeval;
 DEFINE xopeuni  LIKE inv_dtransac.opeuni;
 DEFINE xtipmov  LIKE inv_dtransac.tipmov;
 DEFINE xnumdoc  LIKE inv_mtransac.numrf1;
 DEFINE xtipope  LIKE inv_dtransac.tipope;
 DEFINE xcanuni  DEC(14,6);
 DEFINE weximov  SMALLINT;
 DEFINE conteo   INT;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Verificando si tipo de movimiento es cargo
 IF (wtipope=1) THEN

   -- Obteniendo tipo de movimiento default de ingresos por peso
   SELECT NVL(a.valchr,0)
    INTO  wtipmov
    FROM  glb_paramtrs a
    WHERE a.numpar = 13
      AND a.tippar = 10;

   -- Seleccionando tipo de movimiento
   SELECT NVL(a.tipmov,0),NVL(a.tipope,0)
    INTO  xtipmov,xtipope
    FROM  inv_tipomovs a
    WHERE a.tipmov = wtipmov;

   -- Verificando si existen datos
   IF (xtipmov>0) THEN

    -- Verificando si existe un movimiento de ingreso por peso de la misma fecha
    SELECT UNIQUE a.lnktra
     INTO  xlnktra
     FROM  inv_mtransac a
     WHERE a.codemp = wcodemp
       AND a.codsuc = wcodsuc
       AND a.codbod = wcodbod
       AND a.tipmov = xtipmov
       AND a.fecemi = wfecemi;

    IF (xlnktra=0) OR xlnktra IS NULL THEN
     -- Creando encabezado
     -- Asignando datos
     LET wlnktra = 0;
     LET xnumdoc = "";
     LET wobserv = "INGRESOS POR PESO";
     LET wmestra = MONTH(wfecemi);
     LET waniotr = YEAR(wfecemi);
     LET xcorrel = 1;
     LET xbarcod = "";
     LET westado = "V";
     LET xnserie = "";
     LET xpreuni = 0;

     -- Insertando encabezado
     SET LOCK MODE TO WAIT;
     INSERT INTO inv_mtransac
     VALUES (0                     ,  -- Id unico
             wcodemp               ,  -- Empresa
             wcodsuc               ,  -- Sucursal
             wcodbod               ,  -- Bodega
             xtipmov               ,  -- Tipo de movimiento
             wcodori               ,  -- Origen
             wnomori               ,  -- Nombre del origen
             0                     ,  -- Destino
             ""                    ,  -- Nombre del Destino
             wlnktra               ,  -- Numero id de transaccion que origino el consumo
             xnumdoc               ,  -- Numero de documento
             0                     ,  -- Movimiento es externo
             waniotr               ,  -- Anio
             wmestra               ,  -- Mes
             wfecemi               ,  -- Fecha de emision
             xtipope               ,  -- Tipo de operacion del movimiento
             wobserv               ,  -- Observaciones
             westado               ,  -- Estado V=igente
             wlnktra               ,  -- Numero id de transaccion que origino el consumo
             0                     ,  -- Consumo automatico
             NULL                  ,  -- Motivo de la anulacion
             NULL                  ,  -- Fecha de anulacion
             NULL                  ,  -- Hora de la anulacion
             NULL                  ,  -- Usuario anulo
             "S"                   ,  -- Documento impreso
             USER                  ,  -- Usuario registro
             CURRENT               ,  -- Fecha de registro
             CURRENT HOUR TO SECOND); -- Hora de registro

     -- Obteniendo serial de la tabla encabezado
     SELECT UNIQUE DBINFO("SQLCA.SQLERRD1")
      INTO  xlnktra
      FROM  inv_mtransac;
    ELSE
     -- Asignando datos
     LET wmestra = MONTH(wfecemi);
     LET waniotr = YEAR(wfecemi);
     LET xbarcod = "";
     LET westado = "V";
     LET xnserie = "";
     LET xpreuni = 0;

     -- Obteniendo correlativo de producto de la transaccion
     SELECT (NVL(MAX(a.correl),0)+1)
      INTO  xcorrel
      FROM  inv_dtransac a
      WHERE a.lnktra = xlnktra;
    END IF;

    --- Creando detalle
    -- Calculando cantidad y valor para calculo de saldo
    LET xcanuni = wcanuni;
    LET xcanepq = xcanuni;
    LET xtotpro = 0;
    LET xopeuni = xcanuni;
    LET xopeval = xtotpro;

    -- Insertando detalle
    SET LOCK MODE TO WAIT;
    INSERT INTO inv_dtransac
    VALUES (xlnktra               ,  -- id transaccion
            wcodemp               ,  -- empresa
            wcodsuc               ,  -- sucursal
            wcodbod               ,  -- bodega
            xtipmov               ,  -- tipo de movimiento
            wcodori               ,  -- origen
            0                     ,  -- destino
            wfecemi               ,  -- fecha de emision
            waniotr               ,  -- anio
            wmestra               ,  -- mes
            wcditem               ,  -- codigo de producto
            wcodabr               ,  -- codigo abreviado
            xnserie               ,  -- numero de serie
            0                     ,  -- codigo de empaque
            xcanepq               ,  -- cantidad de empaque
            xcanuni               ,  -- cantidad
            xpreuni               ,  -- precio unitario
            xtotpro               ,  -- total del producto
            0                     ,  -- precio promedio
            xcorrel               ,  -- correlativo
            xbarcod               ,  -- codigo de barra
            xtipope               ,  -- tipo de operacion del movimniento
            westado               ,  -- estado
            xopeval               ,  -- total
            xopeuni               ,  -- cantidad
            0                     ,  -- Numero de pedido
            wnumetq               ,  -- Numero de etiqueta
            USER                  ,  -- Usuario registro
            CURRENT               ,  -- Fecha de registro
            CURRENT HOUR TO SECOND); -- Hora de registro
  END IF;

 END IF;
END PROCEDURE;
