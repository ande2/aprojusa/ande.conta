. /opt/informix/ifx_apro.sh
. /opt/4js/gst310/rt/envgenero

LD_LIBRARY_PATH=$INFORMIXDIR/lib:$INFORMIXDIR/lib/esql
export LD_LIBRARY_PATH

BASEDIR=/app/Conta.apro/ande.conta
export BASEDIR

#LOGNAME=ximena
#export LOGNAME

PATH=/app/cmd:$PATH
export PATH

FGLDBPATH=$BASEDIR/sch
export FGLDBPATH

DBPATH=$BASEDIR/lib:$BASEDIR/std
export DBPATH

FGLLDPATH=$BASEDIR/lib:/opt/4js/gst320/rt/gre/lib/
export FGLLDPATH

SPOOLDIR=$HOME/spl
export SPOOLDIR

PATH=$BASEDIR/cmd:$PATH
export PATH

FGLIMAGEPATH=$BASEDIR/bin/img
export FGLIMAGEPATH

REMOTEHOST=`who -m | cut -d '(' -f2 | cut -d ')' -f1 | cut -d ':' -f1`
export REMOTEHOST

FGLSERVER=$REMOTEHOST
export FGLSERVER

#unset LANG
LANG=en_US.ISO8859-1
export LANG

cd $BASEDIR/bin

fglrun cajachica.42r
