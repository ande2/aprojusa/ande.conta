# @(#) runapp.sh 
# @(#) Mynor Ramirez
# @(#) Marzo 201       
# @(#) Shell utilizado para correr el un ejecutable ubicado fuera del directorio base 

# Asignando valores
DIR=`pwd`
PROGRAM=$2
export DIRBASE=$1 
. $DIRBASE/cmd/enva.sh 
cd $DIRBASE/src/bin
fglrun $PROGRAM 1
cd $DIR 
