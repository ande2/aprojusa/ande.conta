# @(#) runapp.sh 
# @(#) Mynor Ramirez
# @(#) Marzo 201       
# @(#) Shell utilizado para ejecutar el aplicatuvo 

# Asignando valores
DIR=`pwd`
export DIRBASE=/app/erpjur 

# @(#) Variables del Desarollador (Genero)
. /opt/4js/gst310/rt/envgenero

# @(#) 2. Variables de Base de datos (Informix)
. /opt/informix/ifmx.sh

# @(#) 3. Variables de Aplicacion 
. $DIRBASE/cmd/enva.sh 
cd $DIRBASE/src/bin
fglrun mainmenu.42r
cd $DIR 
