# @(#) enva.sh 
# @(#) Mynor Ramirez
# @(#) Diciembre 2010 
# @(#) Shell utilizado para cargar las variables de aplicacion 

# Asignando valores
export TERM=vt100
export DIRCMD=/app/cmd                 
export BASEDIR=/app/erpjur 
export FGLDBPATH=$BASEDIR/sch 
export FGLLDPATH=$BASEDIR/lib:/opt/4js/gst310/dev/gre/lib:. 
export DBPATH=$BASEDIR/lib:.
export FGLSOURCEPATH=$BASEDIR/lib:$BASEDIR/std:. 
export FGLRESOURCEPATH=$BASEDIR/lib:$BASEDIR/std:.
export FGLIMAGEPATH=$BASEDIR/img:$BASEDIR/pic:.
export SPOOLDIR=$HOME/spl
export POI_HOME=/opt/poi/java/poi-3.10.1
export CLASSPATH=$POI_HOME/poi-3.10.1-20140818.jar:$POI_HOME/poi-ooxml-3.10.1-20140818.jar:$POI_HOME/poi-ooxml-schemas-3.10.1-20140818.jar:$POI_HOME/ooxml-lib/dom4j-1.6.1.jar:$POI_HOME/ooxml-lib/xmlbeans-2.6.0.jar:$CLASSPATH
export JRE_HOME=/opt/poi/java/jdk-10.0.1
export LD_LIBRARY_PATH=$JRE_HOME/lib/server:$LD_LIBRARY_PATH
export PATH=$DIRCMD:$BASEDIR/cmd:$JRE_HOME/bin:$PATH
export USER=`logname` 
export LANG=en_US.iso88591

# Creando directorios de reportes
[ ! -d $SPOOLDIR ] && mkdir $SPOOLDIR 
[ ! -d $DIRWWW ] && mkdir $DIRWWW/$HOME 

# Mascara de creacion
umask 0000
