{ 
ADM
Programa : librut001.4gl 
Fecha    : Junio 2010            
Programo : Mynor Ramirez 
Objetivo : Programa de subrutinas de libreria.
}

DEFINE subject CHAR(50) 

{ Subrutina para dibujar border en pantalla }

FUNCTION draw_border(c,d,b,a,msg)
 DEFINE a,b,c,d SMALLINT,
	msg     CHAR(78),
        orilla  CHAR(78)

 -- Dibujando bordes
 CALL fgl_drawbox(b,a,c,d)

 -- Desplegando Titulos
 IF (msg="/") THEN
    LET orilla = a SPACES 
    DISPLAY orilla AT 1,2 ATTRIBUTE(REVERSE) 
 END IF

 CALL messages(c,0,1,a,msg)
END FUNCTION

{ Subrutina para los nombres de los meses del anio }

FUNCTION nombre_meses(n_mes,idioma)
 DEFINE array_mes    ARRAY[12] OF CHAR(10),
        n_mes,idioma SMALLINT,
	nombre_mes   CHAR(10)

 -- Verificando idioma
 CASE (idioma)
  WHEN 0 -- Ingles
   LET array_mes[1] ="January"
   LET array_mes[2] ="February"
   LET array_mes[3] ="March"
   LET array_mes[4] ="April"
   LET array_mes[5] ="May"  
   LET array_mes[6] ="June"  
   LET array_mes[7] ="July" 
   LET array_mes[8] ="August"
   LET array_mes[9] ="September"
   LET array_mes[10]="October" 
   LET array_mes[11]="November"
   LET array_mes[12]="December" 
   LET nombre_mes   = array_mes[n_mes]
  WHEN 1 -- Espaniol
   LET array_mes[1] ="Enero"
   LET array_mes[2] ="Febrero"
   LET array_mes[3] ="Marzo"
   LET array_mes[4] ="Abril"
   LET array_mes[5] ="Mayo" 
   LET array_mes[6] ="Junio"
   LET array_mes[7] ="Julio"
   LET array_mes[8] ="Agosto"
   LET array_mes[9] ="Septiembre"
   LET array_mes[10]="Octubre"
   LET array_mes[11]="Noviembre"
   LET array_mes[12]="Diciembre"
   LET nombre_mes   = array_mes[n_mes]
 END CASE
 RETURN nombre_mes
END FUNCTION

{ Subrutina para desplegar una linea con la fecha del dia }

FUNCTION linea_fecha(ln,lang,pais)
 DEFINE ln,col        SMALLINT,
        lang          SMALLINT,
        pais          STRING, 
        w_date,w_fill CHAR(78)

 -- Desplegnado fecha actual
 LET w_fill  = 78 SPACES
 LET w_date  = formato_fecha(CURRENT,lang,pais)
 LET w_date  = w_date CLIPPED," ",TIME    
 LET col     = (78-LENGTH(w_date)) 
 DISPLAY w_fill         AT ln,1   ATTRIBUTE(REVERSE) 
 DISPLAY w_date CLIPPED AT ln,col ATTRIBUTE(REVERSE)
END FUNCTION

{ Subrutina para formatear una fecha }

FUNCTION formato_fecha(w_fecha,idioma,pais)
 DEFINE w_mname,w_dname CHAR(10),
        w_month,w_day   SMALLINT,
        idioma          SMALLINT,
        w_date          CHAR(78),
        pais            CHAR(30),
        abbr            CHAR(2),
        w_fecha         DATE

 -- Obteniendo datos de la fecha
 LET w_date  = NULL
 LET w_month = MONTH(w_fecha)
 LET w_mname = nombre_meses(w_month,idioma)
 LET w_day   = WEEKDAY(w_fecha)  
 LET w_dname = nombre_dias(w_day,idioma)

 -- Verificando pais
 IF (LENGTH(pais)>0) THEN
    LET pais = pais CLIPPED,","
 END IF 

 -- Obteniendo fecha formateada  
 CASE (idioma)
  WHEN 0 -- Ingles
   -- Verificando numero de dia
   CASE (DAY(w_fecha))
    WHEN 1    LET abbr = "st"
    WHEN 2    LET abbr = "nd"
    WHEN 3    LET abbr = "rd"
    OTHERWISE LET abbr = "th"
   END CASE 

   -- Construyendo fecha
   LET w_date = pais CLIPPED," ",w_mname CLIPPED," ",
                DAY(w_fecha) USING "<<<<<",abbr,", ",
                YEAR(w_fecha) USING "<<<<","."

  WHEN 1 -- Espaniol
   LET w_date = pais CLIPPED," ",
                DAY(w_fecha) USING"<<<<<"," de ",
                w_mname CLIPPED," de ",
                YEAR(w_fecha) USING "<<<<","."
 END CASE
 RETURN w_date
END FUNCTION

{ Subrutina para los nombre de los dias de la semana }

FUNCTION nombre_dias(w_day,idioma)
 DEFINE w_arrayw ARRAY[7] OF CHAR(10),
	w_dname  CHAR(9),
	w_day    SMALLINT,
	idioma   SMALLINT        

 -- Verificando dias de la semana
 IF (w_day=0) THEN
    LET w_day = 7
 END IF

 -- Verificando idioma   
 CASE (idioma)
  WHEN 0 -- Ingles
   LET w_arrayw[1] = "Monday" 
   LET w_arrayw[2] = "Tuesday"
   LET w_arrayw[3] = "Wednesday"
   LET w_arrayw[4] = "Thrusday"
   LET w_arrayw[5] = "Friday"
   LET w_arrayw[6] = "Saturday"
   LET w_arrayw[7] = "Sunday"
   LET w_dname     = w_arrayw[w_day]   
  WHEN 1 -- Espaniol
   LET w_arrayw[1] = "Lunes"  
   LET w_arrayw[2] = "Martes"
   LET w_arrayw[3] = "Miercoles"
   LET w_arrayw[4] = "Jueves"
   LET w_arrayw[5] = "Viernes"
   LET w_arrayw[6] = "Sabado"
   LET w_arrayw[7] = "Domingo"
   LET w_dname     = w_arrayw[w_day]   
 END CASE

 RETURN w_dname
END FUNCTION

{ Subrutina para desplegar el encabezados del sistema }

FUNCTION headermenu(lin,msg1)
 DEFINE lin,d     SMALLINT,
        msg1,msg2 CHAR(80)

 -- Obteniendo nombre del host
 LET msg2 = FGL_GETENV("NODE")

 -- Justificando nombre del host
 LET d = ((78-LENGTH(msg2))+1)

 -- Desplegando nombre del sistema y el host
 DISPLAY msg1 CLIPPED AT lin,1 ATTRIBUTE(GREEN) 
 DISPLAY msg2 CLIPPED AT lin,d ATTRIBUTE(MAGENTA)
END FUNCTION

{ Subrutina para desplegar el usuario y el numero de terminal }

FUNCTION loginterm(fil,col)
 DEFINE fil,col,np    SMALLINT,
        nterm,usuario CHAR(15)

 LET usuario = FGL_GETENV("LOGNAME") 
 LET nterm   = FGL_GETENV("TERMINAL")
 DISPLAY usuario CLIPPED," - ",nterm CLIPPED AT fil,col ATTRIBUTE(REVERSE)
END FUNCTION

{ Subrutina para detener la pantalla }

FUNCTION alto_screen()
 MENU "Para continuar presione"
  COMMAND "ENTER"
          EXIT MENU
 END MENU
END FUNCTION

{ Subrutina para detener la pantalla dento de una ventana }

FUNCTION detiene_screen(lin)
 DEFINE lin SMALLINT

 OPEN WINDOW wstop AT lin,1    
  WITH 2 ROWS,78 COLUMNS 
  CALL alto_screen()
 CLOSE WINDOW wstop
END FUNCTION

{ Subrutina para desplegar mensajes }

FUNCTION messages(lin,col,atributo,lf,msg)
 DEFINE lin,col,lf,pos,atributo SMALLINT,
        msg                     CHAR(78)

 -- Verificando columna 
 IF (col=0) THEN 
    IF (LENGTH(msg) >78) THEN
       LET msg = msg[1,78]
    END IF
    LET pos = ((lf-LENGTH(msg))/2)+1
 ELSE
    LET pos = col
 END IF

 -- Verificando el atributo
 CASE atributo
  WHEN  0 DISPLAY msg CLIPPED AT lin,pos
  WHEN  1 DISPLAY msg CLIPPED AT lin,pos ATTRIBUTE(REVERSE) 
  WHEN  2 DISPLAY msg CLIPPED AT lin,pos ATTRIBUTE(CYAN) 
  WHEN  3 DISPLAY msg CLIPPED AT lin,pos ATTRIBUTE(UNDERLINE) 
  WHEN  4 DISPLAY msg CLIPPED AT lin,pos ATTRIBUTE(BLINK) 
  WHEN  5 DISPLAY msg CLIPPED AT lin,pos ATTRIBUTE(GREEN) 
  WHEN  6 DISPLAY msg CLIPPED AT lin,pos ATTRIBUTE(CYAN,REVERSE) 
  WHEN  7 DISPLAY msg CLIPPED AT lin,pos ATTRIBUTE(YELLOW,REVERSE) 
  WHEN  8 DISPLAY msg CLIPPED AT lin,pos ATTRIBUTE(CYAN,UNDERLINE) 
  WHEN  9 DISPLAY msg CLIPPED AT lin,pos ATTRIBUTE(RED,REVERSE) 
  WHEN 10 DISPLAY msg CLIPPED AT lin,pos ATTRIBUTE(GREEN,REVERSE) 
 END CASE
END FUNCTION

{ Subrutina para setear teclas en modo on } 

FUNCTION teclas_on()
 -- Definicion de Teclas 
 OPTIONS PREVIOUS KEY CONTROL-P,
             NEXT KEY CONTROL-N,
           DELETE KEY CONTROL-T,
           INSERT KEY CONTROL-I
END FUNCTION

{ Subrutina para setear teclas en modo off } 

FUNCTION teclas_off()
 -- Definicion de Teclas 
 OPTIONS INSERT   KEY CONTROL-A,
         DELETE   KEY CONTROL-A,
         NEXT     KEY CONTROL-A,
         PREVIOUS KEY CONTROL-A
END FUNCTION

{ Subrutina para obtener los dias de un mes }

FUNCTION dias_mes(wmes,wanio)
 DEFINE array_dias       ARRAY[12] OF SMALLINT,
        wmes,wanio,wdias SMALLINT,
        residuo          INTEGER
 
 -- Verificando si es anio bisiesto       
 LET residuo = (wanio mod 4)
 IF (residuo>0) THEN
    LET array_dias[2] = 28
 ELSE
    LET array_dias[2] = 29
 END IF

 -- Asigando dias del mes
 LET array_dias[1]  = 31
 LET array_dias[3]  = 31
 LET array_dias[4]  = 30
 LET array_dias[5]  = 31
 LET array_dias[6]  = 30
 LET array_dias[7]  = 31
 LET array_dias[8]  = 31
 LET array_dias[9]  = 30
 LET array_dias[10] = 31
 LET array_dias[11] = 30
 LET array_dias[12] = 31
 LET wdias = array_dias[wmes]
 RETURN wdias
END FUNCTION

{ Subrutina para crear un subshell }

FUNCTION subshell()   
 DEFINE cmd CHAR(80),
        x   CHAR(1)

 -- Verificando acceso
 --IF NOT secli001_acceso("ADM","subshell",0) THEN
 --   RETURN 
 --END IF 

 OPTIONS PROMPT LINE 24
 LET x = "!"
 WHILE x = "!"
  PROMPT "!" FOR cmd
  RUN cmd
  PROMPT "Presione ENTER para continuar." FOR CHAR x
 END WHILE
END FUNCTION

{ Subrutina para centrar titulos }

FUNCTION librut001_centrado(tl,wmsg)
 DEFINE wmsg   CHAR(80),
        tl,col SMALLINT

 -- Centrando
 LET col = ((tl-LENGTH(wmsg))/2) 
 IF (col<=0) THEN
    LET col = 1 
 END IF 
 RETURN col
END FUNCTION

{ Subrutina para desplegar mensaje de ayuda para traslapar la vista de window     anterior }

FUNCTION hidewin(wwin)
 DEFINE wwin SMALLINT

 -- Verificando ventana 
 CASE (wwin)
  WHEN 1 OPEN WINDOW hwin1 AT 3,1
          WITH 4 ROWS,78 COLUMNS 
  WHEN 2 OPEN WINDOW hwin2 AT 3,1
          WITH 4 ROWS,78 COLUMNS 
  WHEN 3 OPEN WINDOW hwin3 AT 4,1
          WITH 21 ROWS,78 COLUMNS 
 END CASE

 -- Desplegando mensajes 
 CALL librut001_lineahelp(1,1,66)
END FUNCTION

{ Subrutina para desplegar una linea de ayuda }

FUNCTION librut001_lineahelp(a,b,c)
 DEFINE a,b,c SMALLINT

 -- Dibujando linea
 CALL librut001_lineahor(a,b)

 -- Desplegando mensaje 
 CALL messages(a,c,2,0,"Ctrl-W Ayuda")
END FUNCTION 

{ Subrutina para desplegar una linea horizontal }

FUNCTION librut001_lineahor(a,b)
 DEFINE a,b SMALLINT
 
 -- Dibujando linea horizontal
 DISPLAY "---------------------------------------",
         "---------------------------------------" AT a,b 
END FUNCTION

{ Subrutina para desplegar mensajes de error al usuario }

FUNCTION msgerror(msg1,msg2) 
 DEFINE msg1,msg2 CHAR(70), 
        fill      CHAR(70)

 -- Desplegando mensaje
 OPEN WINDOW wsguperr AT 21,5 
  WITH 2 ROWS,70 COLUMNS ATTRIBUTE(BORDER,REVERSE)
  LET fill = 70 SPACES

  DISPLAY fill AT 1,1 ATTRIBUTE(REVERSE)
  DISPLAY fill AT 2,1 ATTRIBUTE(REVERSE)
  DISPLAY msg1 AT 1,1 ATTRIBUTE(REVERSE)
  DISPLAY msg2 AT 2,1 ATTRIBUTE(REVERSE)
  CALL detiene_screen(1) 

 CLOSE WINDOW wsguperr 
END FUNCTION

{ Subrutina para desplegar mensaje de que un usuario no existe registrado
  en el sistema } 

FUNCTION no_registrado()
 DEFINE alto CHAR(1)

 OPTIONS PROMPT LINE 1
 CLEAR SCREEN
 DISPLAY "                            " AT  7,26 ATTRIBUTE(REVERSE) 
 DISPLAY "       ** ATENCION **       " AT  8,26 ATTRIBUTE(REVERSE) 
 DISPLAY "                            " AT  9,26 ATTRIBUTE(REVERSE) 
 DISPLAY "    Usuario No Registrado   " AT 10,26 ATTRIBUTE(REVERSE)
 DISPLAY "        En el Sistema       " AT 11,26 ATTRIBUTE(REVERSE)
 DISPLAY "                            " AT 12,26 ATTRIBUTE(REVERSE)
 DISPLAY "       Acceso Denegado      " AT 13,26 ATTRIBUTE(REVERSE)
 DISPLAY "    Sistema de Seguridad    " AT 14,26 ATTRIBUTE(REVERSE)
 DISPLAY "         Activado           " AT 15,26 ATTRIBUTE(REVERSE)
 DISPLAY "                            " AT 16,26 ATTRIBUTE(REVERSE)
 DISPLAY "    Enter para continuar    " AT 17,26 ATTRIBUTE(REVERSE)
 DISPLAY "                            " AT 18,26 ATTRIBUTE(REVERSE)
 PROMPT "" FOR alto 
END FUNCTION 

{ Subrutina para abrir ventanas para rotulos de ayuda }

FUNCTION msgwhlp(lin,msg,opc)
 DEFINE lin SMALLINT,
        opc SMALLINT,
        msg CHAR(78)

 -- Verificando ventana
 CASE (opc)
  WHEN 1 OPEN WINDOW wmsgup1 AT lin,1    
          WITH 1 ROWS,78 COLUMNS 
          DISPLAY msg AT 1,1 ATTRIBUTE(CYAN) 
  WHEN 2 OPEN WINDOW wmsgup2 AT lin,1
          WITH 1 ROWS,78 COLUMNS 
          DISPLAY msg AT 1,1 ATTRIBUTE(CYAN) 
  WHEN 3 OPEN WINDOW wmsgup3 AT lin,1
          WITH 1 ROWS,78 COLUMNS 
          DISPLAY msg AT 1,1 ATTRIBUTE(CYAN) 
  WHEN 4 OPEN WINDOW wmsgup4 AT lin,1
          WITH 1 ROWS,78 COLUMNS 
          DISPLAY msg AT 1,1 ATTRIBUTE(CYAN) 
  WHEN 5 OPEN WINDOW wmsgup5 AT lin,1
          WITH 1 ROWS,78 COLUMNS 
          DISPLAY msg AT 1,1 ATTRIBUTE(CYAN) 
  WHEN 6 OPEN WINDOW wmsgup6 AT lin,1
          WITH 1 ROWS,78 COLUMNS 
          DISPLAY msg AT 1,1 ATTRIBUTE(CYAN) 
  WHEN 7 OPEN WINDOW wmsgup7 AT lin,1
          WITH 1 ROWS,78 COLUMNS 
          DISPLAY msg AT 1,1 ATTRIBUTE(CYAN) 
  WHEN 8 OPEN WINDOW wmsgup8 AT lin,1
          WITH 1 ROWS,78 COLUMNS 
          DISPLAY msg AT 1,1 ATTRIBUTE(CYAN) 
  WHEN 9 OPEN WINDOW wmsgup9 AT lin,1
          WITH 2 ROWS,78 COLUMNS 
 END CASE
END FUNCTION

{ Subrutina para seleccionar el dispositivo de salida para un reporte }

FUNCTION outputdevice(a,b,x,y)
 DEFINE pipeline CHAR(40),
        msg      CHAR(78),
        x,y,a,b  SMALLINT

 -- Abriendo ventana de seleccion
 OPEN WINDOW wdevice AT a,b 
  WITH 2 ROWS,56 COLUMNS ATTRIBUTE(BORDER,PROMPT LINE LAST)
  LET pipeline = NULL 

  MENU " Reporte en"
   COMMAND "Pantalla"
           " Envia el reporte a la pantalla." 
           LET pipeline = "screen"
           EXIT MENU
   COMMAND "Impresora"
           " Envia el reporte a la impresora seleccionada." 

           -- Seleccionando impresora  
           LET pipeline = menu_prm(x,y,"PRINTERS","I","Impresoras") 
           IF pipeline IS NULL THEN
              CONTINUE MENU
           END IF 
           EXIT MENU
   COMMAND "E-mail"
           " Envia el reporte al buzon de correo del usuario."
           -- Ingresando subject del email
           LET subject = "Archivo Texto"
           PROMPT " Asunto: " FOR subject 
            ON KEY(INTERRUPT)
             ERROR "Tecla Invalida."
           END PROMPT 
           IF (LENGTH(subject)<=0) THEN
              CONTINUE MENU
           END IF 
           LET subject = UPSHIFT(subject) 

           -- Verificando buzon de correo
           LET pipeline = librut001_checkemail(FGL_GETENV("LOGNAME")) 
           IF pipeline IS NULL THEN
            LET msg = " Atencion: usuario (",FGL_GETENV("LOGNAME") CLIPPED,
                      ") sin buzon de correo definido."
            CALL msgerror( msg," (VERIFICA)")
            CONTINUE MENU 
           END IF 

           EXIT MENU 
   COMMAND "Cancelar"
           " Cancela la emision del reporte."
           LET pipeline = NULL
           EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
           LET pipeline = NULL
           EXIT MENU
   COMMAND KEY ("!") 
           CALL subshell()
  END MENU
 CLOSE WINDOW wdevice  
 RETURN pipeline  
END FUNCTION 

{ Subrutina para cargar parametros del sistema operativo }

FUNCTION menu_prm(fil,col,val,opcion,titulo)         
 DEFINE v_param           ARRAY[33] OF RECORD
          num             CHAR(1),
          opc             CHAR(20)
       END RECORD,
       parametros         CHAR(450),
       cntprm,sal,i,j,k   SMALLINT,
       arr,scr,flag_fin   SMALLINT,
       fil,col,lin,arr1   SMALLINT,
       parameter,val      CHAR(25),
       parametro          CHAR(40),
       titulo             CHAR(20),
       opcion             CHAR(1)

 -- Cargando los parametros del sistema 
 LET parameter  = NULL
 LET parametros = FGL_GETENV(val) 
 IF parametros IS NULL THEN
    CALL msgerror
    (" Atencion: parametros no pueden ser cargados al sistema.",
    " (Verifica variable de ambiente utilizada)")
    RETURN parameter
 END IF 

 -- Inicializando el vector (v_param) 
 FOR i = 1 TO 33 
  INITIALIZE v_param[i].* TO NULL
 END FOR

 -- Cargando los parametros
 LET j = 2
 LET parametro = NULL 
 FOR i = 1 TO LENGTH(parametros)
  IF (parametros[i,i]=";") THEN
     EXIT FOR
  END IF
  IF (parametros[i,i]=" ") THEN
     LET parametros[i,i]="~"
  END IF 

  IF (j<=33) THEN
     IF (parametros[i,i]!=":") THEN
        LET parametro = parametro CLIPPED,parametros[i,i]
     ELSE
        LET v_param[j].opc = parametro
        LET parametro      = NULL 
        LET j = (j+1)
     END IF 
  END IF 
 END FOR   

 -- Depurando filas del vector de parametros
 FOR i = 1 TO j
  LET parametro = v_param[i].opc
  FOR k = 1 TO LENGTH(parametro)
   IF (parametro[k,k]="~") THEN
      LET parametro[k,k] = " "
   END IF 
  END FOR 
  LET v_param[i].opc = parametro
 END FOR 

 -- Desplegando el menu de opciones
 OPEN WINDOW windparm AT fil,col
  WITH FORM "formparm" ATTRIBUTE(FORM LINE 1)
  CALL draw_border(1,1,13,17,titulo)

  -- Cambiando el ESC por el ENTER en el input 
  OPTIONS ACCEPT   KEY CONTROL-M,
          INSERT   KEY CONTROL-A,
          DELETE   KEY CONTROL-A,
          NEXT     KEY CONTROL-A,
          PREVIOUS KEY CONTROL-A

  -- Inicio del bucle para el menu
  LET cntprm = j
  WHILE TRUE 
   LET flag_fin   = TRUE
   CALL SET_COUNT(cntprm)
  
   -- Capturando la opciones del menu 
   INPUT ARRAY v_param WITHOUT DEFAULTS FROM sparamtr.* 
    ON KEY(F4,CONTROL-E,INTERRUPT)
     LET arr=0
     EXIT INPUT

    BEFORE FIELD num 
     LET arr=ARR_CURR()
     LET scr=SCR_LINE()
     IF (arr>1) THEN 
        IF (arr=cntprm) THEN
	   LET flag_fin=FALSE
           EXIT INPUT
        END IF
        LET v_param[arr].num="*"
        IF (scr>=11) THEN
           LET lin=11
        ELSE
           LET lin=(scr+1)
        END IF
        DISPLAY " " AT lin,3 ATTRIBUTE(REVERSE)
        DISPLAY v_param[arr].num,v_param[arr].opc 
        TO      sparamtr[scr].num,sparamtr[scr].opc ATTRIBUTE(REVERSE)
     ELSE 
        NEXT FIELD opc
     END IF
    AFTER FIELD num
     LET arr=ARR_CURR()
     LET scr=SCR_LINE()

     LET v_param[arr].num=NULL
     IF (scr>=11) THEN
        LET lin=11
     ELSE
        LET lin=(scr+1)
     END IF
     DISPLAY " " AT lin,3 
     DISPLAY v_param[arr].num,v_param[arr].opc 
     TO      sparamtr[scr].num,sparamtr[scr].opc  
   END INPUT
   IF flag_fin THEN
      EXIT WHILE
   END IF 
  END WHILE
 CLOSE WINDOW windparm 

 -- Retornando el ESC por el ENTER en el input
 OPTIONS ACCEPT   KEY ESCAPE,
         PREVIOUS KEY CONTROL-P,
         NEXT     KEY CONTROL-N,
         DELETE   KEY CONTROL-T,
         INSERT   KEY CONTROL-I

 IF (arr=0) THEN
    LET parameter = NULL
    RETURN parameter
 END IF

 -- Verificando opcion seleccionada 
 CASE (opcion)
  WHEN "I" -- Impresoras 
   -- Verificando tipo de impresoras
   CASE (v_param[arr].opc)
    WHEN "local" LET parameter = v_param[arr].opc CLIPPED
    WHEN "laser" LET parameter = v_param[arr].opc CLIPPED
    OTHERWISE    LET parameter = "lp -s -d",v_param[arr].opc CLIPPED
   END CASE 
  OTHERWISE 
   LET parameter = v_param[arr].opc CLIPPED 
 END CASE 
 RETURN parameter
END FUNCTION

{ Subrutina para configurar los tipos de letras }

FUNCTION fonts(pipeline)
 DEFINE fnt     RECORD
        cmp,nrm   CHAR(12),
        tbl,fbl   CHAR(12),
	t88,t66   CHAR(12),
        p12,p10   CHAR(12),
        srp       CHAR(12), 
        twd,fwd   CHAR(12),
	tda,fda   CHAR(12),
	ini       CHAR(12)
       END RECORD,
       pipeline    CHAR(40)

 -- Definiendo tipos de fonts
 INITIALIZE fnt.* TO NULL

 -- Verificando si salida es pantalla (screen) o buzon de correo (*@*)
 IF (pipeline="screen") OR
    (pipeline="pdf") OR
    (pipeline MATCHES "*@*") THEN
    RETURN fnt.*
 END IF 

 -- Verificando tipos de impresora
 CASE (pipeline) 
  WHEN "local" 
   -- Salida a impresora EPSON ESC/2
   LET fnt.cmp = ASCII 15
   LET fnt.nrm = ASCII 18
   LET fnt.tbl = ASCII 27,ASCII 71
   LET fnt.fbl = ASCII 27,ASCII 72
   LET fnt.twd = ASCII 27,ASCII 87,"1"
   LET fnt.fwd = ASCII 27,ASCII 87,"0" 
   LET fnt.t88 = ASCII 27,ASCII 48
   LET fnt.t66 = ASCII 27,ASCII 50
   LET fnt.p12 = ASCII 27,ASCII 77
   LET fnt.p10 = ASCII 27,ASCII 80
   LET fnt.tda = ASCII 27,ASCII 119,"1"
   LET fnt.fda = ASCII 27,ASCII 119,"0"
   LET fnt.ini = ASCII 27,"@" 
   LET fnt.srp = ASCII 27,ASCII 56 
  WHEN "laser"
   -- Salida a impresora HP laser
   LET fnt.cmp = "\033&k2S"
   LET fnt.nrm = "\033(s10H"
   LET fnt.tbl = "\033(s1B"
   LET fnt.fbl = "\033(s0B"
   LET fnt.twd = "\033(s6H"
   LET fnt.fwd = "\033(s12H"
   LET fnt.t88 = "\033&l88F"
   LET fnt.t66 = "\033&l66F"
   LET fnt.p12 = "\033(s14H"
   LET fnt.p10 = "\033(s10H"
   LET fnt.tda = "\033&l1O" 
   LET fnt.fda = NULL
   LET fnt.ini = "\033E" 

   #LET fnt.ini = "\033E\33&k2G" 
   #150P\33&l10O\33\3316D\33(s10H\33(s14H"
   LET fnt.srp = NULL
  OTHERWISE 
   -- Salida a impresora EPSON ESC/2
   LET fnt.cmp = ASCII 15
   LET fnt.nrm = ASCII 18
   LET fnt.tbl = ASCII 27,ASCII 71
   LET fnt.fbl = ASCII 27,ASCII 72
   LET fnt.twd = ASCII 27,ASCII 87,"1"
   LET fnt.fwd = ASCII 27,ASCII 87,"0" 
   LET fnt.t88 = ASCII 27,ASCII 48
   LET fnt.t66 = ASCII 27,ASCII 50
   LET fnt.p12 = ASCII 27,ASCII 77
   LET fnt.p10 = ASCII 27,ASCII 80
   LET fnt.tda = ASCII 27,ASCII 119,"1"
   LET fnt.fda = ASCII 27,ASCII 119,"0"
   LET fnt.ini = ASCII 27,"@" 
   LET fnt.srp = ASCII 27,ASCII 56 
 END CASE 

 RETURN fnt.*
END FUNCTION 

{ Subrutina para imprimir la salida de un reporte } 

FUNCTION printreport(filename,pipeline)
 DEFINE wcommand CHAR(200),
        pipeline CHAR(90),
        filename CHAR(90)

 -- Tipos de dispositivo (pipeline)
 -- screen = pantalla
 -- *@*    = buzon de correo
 -- Otros  = local,laser = impresora

 -- Seleccionando dispositivo 
 CASE 
  WHEN (pipeline="screen")
   LET wcommand = "viewfile ",filename CLIPPED
  WHEN (pipeline MATCHES "*@*")
   LET wcommand = "mailto '",subject  CLIPPED,"' ",
                             pipeline CLIPPED," ",
                             filename CLIPPED
  WHEN (pipeline="local") 
   LET wcommand = "prtty ",filename CLIPPED
  WHEN (pipeline="laser") 
   LET wcommand = "prtty ",filename CLIPPED
  OTHERWISE
   LET wcommand = "cat -s ",filename CLIPPED,"|",pipeline CLIPPED
 END CASE

 -- Ejecutando commando
 RUN wcommand 
END FUNCTION

{ Subrutina para desplegar mensajes de error al usuario (3 lineas) }

FUNCTION msgerror3(msg1,msg2,msg3) 
 DEFINE msg1,msg2,msg3 CHAR(70), 
        fill           CHAR(70)

 -- Desplegando mensaje
 OPEN WINDOW wsguperr AT 20,5 
  WITH 3 ROWS,70 COLUMNS ATTRIBUTE(BORDER,REVERSE)
  LET fill = 70 SPACES

  DISPLAY fill AT 1,1 ATTRIBUTE(REVERSE)
  DISPLAY fill AT 2,1 ATTRIBUTE(REVERSE)
  DISPLAY fill AT 3,1 ATTRIBUTE(REVERSE)
  DISPLAY msg1 AT 1,1 ATTRIBUTE(REVERSE)
  DISPLAY msg2 AT 2,1 ATTRIBUTE(REVERSE)
  DISPLAY msg3 AT 3,1 ATTRIBUTE(REVERSE)
  CALL detiene_screen(1) 

 CLOSE WINDOW wsguperr 
END FUNCTION

{ Subrutina para configurar los tipos de letras para un mini-impresot TMU }

FUNCTION fontstmu()
DEFINE fnt     RECORD
        cmp,nrm   CHAR(12),
        tbl,fbl   CHAR(12),
	t88,t66   CHAR(12),
        p12,p10   CHAR(12),
        srp       CHAR(12), 
        twd,fwd   CHAR(12),
	tda,fda   CHAR(12),
	ini       CHAR(12)
       END RECORD

  -- Definiendo tipos de fonts
  INITIALIZE fnt.* TO NULL

  -- TM-U200B
  LET fnt.cmp = ASCII 15
  LET fnt.nrm = ASCII 27,"@" 
  LET fnt.tbl = ASCII 27,ASCII 71, ASCII  1
  LET fnt.fbl = ASCII 27,ASCII 72, ASCII  0
  LET fnt.twd = ASCII 27,ASCII 87,"1"
  LET fnt.fwd = ASCII 27,ASCII 87,"0" 
  LET fnt.t88 = ASCII 27,ASCII 48
  LET fnt.t66 = ASCII 27,ASCII 50
  LET fnt.p12 = ASCII 27,ASCII 77
  LET fnt.p10 = ASCII 27,ASCII 80
  LET fnt.tda = ASCII 27,ASCII 119,"1"
  LET fnt.fda = ASCII 27,ASCII 119,"0"
  LET fnt.ini = ASCII 27,ASCII 99, ASCII 48,ASCII 4 
  LET fnt.srp = ASCII 27,ASCII 56 
  RETURN fnt.*
END FUNCTION 

{ Subrutina para desplegar mensajes de error al usuario (4 lineas) }

FUNCTION msgerror4(msg1,msg2,msg3,msg4) 
 DEFINE msg1,msg2,msg3,msg4 CHAR(70), 
        fill                CHAR(70)

 -- Desplegando mensaje
 OPEN WINDOW wsguperr AT 19,5 
  WITH 4 ROWS,70 COLUMNS ATTRIBUTE(BORDER,REVERSE)
  LET fill = 70 SPACES

  DISPLAY fill AT 1,1 ATTRIBUTE(REVERSE)
  DISPLAY fill AT 2,1 ATTRIBUTE(REVERSE)
  DISPLAY fill AT 3,1 ATTRIBUTE(REVERSE)
  DISPLAY fill AT 4,1 ATTRIBUTE(REVERSE)
  DISPLAY msg1 AT 1,1 ATTRIBUTE(REVERSE)
  DISPLAY msg2 AT 2,1 ATTRIBUTE(REVERSE)
  DISPLAY msg3 AT 3,1 ATTRIBUTE(REVERSE)
  DISPLAY msg4 AT 4,1 ATTRIBUTE(REVERSE)
  CALL detiene_screen(1) 

 CLOSE WINDOW wsguperr 
END FUNCTION

{ Subrutina para recuperar un archivo ya impreso }

FUNCTION reprint(filename)
 DEFINE filename CHAR(90),
        pipeline CHAR(50)

 -- Verificando si hay ultimo archivo a reimprimir
 IF NOT findfile(filename) THEN
    CALL msgerror(
    " Atencion: no existe reporte a reimprimir.",
    " (Debe haberse generado al menos una vez para poder reimprimir)")
    RETURN 
 END IF 

 -- Seleccionando impresora para la reimpresion
 LET pipeline = menu_prm(6,60,"PRINTERS","I","Impresoras") 
 IF pipeline IS NULL THEN
    RETURN
 END IF 

 MESSAGE " Recuperando Impresion ..." ATTRIBUTE(CYAN) 
 CALL printreport(filename,pipeline)
END FUNCTION

{ Subrutina para buscar un archivo en UNIX }

FUNCTION findfile(filename)
 DEFINE filename CHAR(60),
        wcmd     CHAR(70),
        existe   SMALLINT

 -- Buscando el archivo
 LET wcmd = "findfile ",filename CLIPPED
 RUN wcmd RETURNING existe
 
 RETURN existe
END FUNCTION

{ Subrutina para depurar caracteres en lineas } 

FUNCTION depuralin(linea,caracter)
 DEFINE caracter CHAR(1),
        linea    CHAR(250),
        i        SMALLINT

 -- Depurando lineas
 FOR i = 1 TO LENGTH(linea)
  IF (linea[i,i]=caracter) THEN
     LET linea[i,i] = " "
  END IF 
 END FOR
 RETURN linea 
END FUNCTION

{ Subrutina para convertir los numeros a letras }

FUNCTION librut001_numtolet(valor)
DEFINE f_tentxt ARRAY[8] OF RECORD
         tentxt         CHAR(10)
       END RECORD,
       f_unittxt ARRAY[20] OF RECORD
         unittxt        CHAR(11)
       END RECORD,
       f_gruptxt ARRAY[3] OF RECORD
         gruptxt        CHAR(7)
       END RECORD,
       f_letras,letras  CHAR(100),
       i,j,esp SMALLINT,
       f_cox            CHAR(20),
       f_deci           DECIMAL(3,0),
       valor            DECIMAL(11,2),
       f_numero, f_stp, f_divisor, f_grup, f_unit, f_ten, f_kk  INTEGER,
       f_deci2          CHAR(1)
LET f_tentxt[1].tentxt = "VEINTI"
LET f_tentxt[2].tentxt = "TREINTA "
LET f_tentxt[3].tentxt = "CUARENTA "
LET f_tentxt[4].tentxt = "CINCUENTA "
LET f_tentxt[5].tentxt = "SESENTA "
LET f_tentxt[6].tentxt = "SETENTA "
LET f_tentxt[7].tentxt = "OCHENTA "
LET f_tentxt[8].tentxt = "NOVENTA "

LET f_unittxt[1].unittxt = "UN "
LET f_unittxt[2].unittxt = "DOS "
LET f_unittxt[3].unittxt = "TRES "
LET f_unittxt[4].unittxt = "CUATRO "
LET f_unittxt[5].unittxt = "CINCO "
LET f_unittxt[6].unittxt = "SEIS "
LET f_unittxt[7].unittxt = "SIETE "
LET f_unittxt[8].unittxt = "OCHO "
LET f_unittxt[9].unittxt = "NUEVE "
LET f_unittxt[10].unittxt = "DIEZ "
LET f_unittxt[11].unittxt = "ONCE "
LET f_unittxt[12].unittxt = "DOCE "
LET f_unittxt[13].unittxt = "TRECE "
LET f_unittxt[14].unittxt = "CATORCE "
LET f_unittxt[15].unittxt = "QUINCE "
LET f_unittxt[16].unittxt = "DIECISEIS "
LET f_unittxt[17].unittxt = "DIECISIETE "
LET f_unittxt[18].unittxt = "DIECIOCHO "
LET f_unittxt[19].unittxt = "DIECINUEVE "
LET f_unittxt[20].unittxt = "VEINTE "

LET f_gruptxt[1].gruptxt = "MILLON "
LET f_gruptxt[2].gruptxt = "MIL "
LET f_gruptxt[3].gruptxt = " "


LET f_numero = valor
LET f_letras = NULL
LET f_stp = 1
LET f_divisor = 1000000
LET f_grup = 0
LET f_unit = 0
LET f_ten  = 0

WHILE f_stp <= 3

        LET f_grup = (f_numero / f_divisor)
        LET f_grup = f_grup * f_divisor
        LET f_numero = f_numero - f_grup
        LET f_grup = f_grup / f_divisor

        IF f_grup > 0
           THEN LET f_kk = f_grup
                LET f_ten = 0
           IF f_grup > 99
              THEN LET f_unit = (f_grup / 100)
                   LET f_grup = f_grup - (f_unit * 100)
                   LET f_cox = f_unittxt[f_unit].unittxt CLIPPED,"CIENTOS "
                   IF f_unit = 1
                      THEN IF f_grup = 0
                              THEN LET f_cox = "CIEN "
                              ELSE LET f_cox = "CIENTO "
                           END IF
                      END IF
                   IF f_unit = 5
                      THEN LET f_letras = f_letras CLIPPED," ","QUINIENTOS "
                      ELSE IF f_unit = 7
                              THEN 
                           LET f_letras = f_letras CLIPPED," ","SETECIENTOS "
                              ELSE IF f_unit = 9
                                      THEN
                           LET f_letras = f_letras CLIPPED," ","NOVECIENTOS "
                                      ELSE 
                           LET f_letras = f_letras CLIPPED," ",f_cox
                                   END IF
                           END IF
                   END IF
           END IF
        IF f_grup > 30
           THEN LET f_ten = (f_grup / 10)
                LET f_grup = f_grup - (f_ten * 10)
                LET f_letras = f_letras CLIPPED," ",f_tentxt[f_ten-1].tentxt
                IF ((f_grup > 0) AND (f_letras IS NOT NULL))
                   THEN LET f_letras = f_letras CLIPPED," ","Y "
                END IF
           ELSE IF f_grup > 20
                 THEN LET f_ten = (f_grup / 10)
                      LET f_grup = f_grup - (f_ten * 10)
                      LET f_letras = f_letras CLIPPED," ",f_tentxt[f_ten-1].tentxt
                END IF
         END IF
        IF f_grup > 0
           THEN IF f_ten = 2
           THEN LET f_letras = f_letras CLIPPED,f_unittxt[f_grup].unittxt
           ELSE LET f_letras = f_letras CLIPPED," ",f_unittxt[f_grup].unittxt
                END IF
        END IF
        LET f_letras = f_letras CLIPPED," ",f_gruptxt[f_stp].gruptxt
        IF ((f_stp = 1) AND (f_grup > 1))
           THEN LET f_letras = f_letras CLIPPED,"ES "
        END IF
        END IF

        LET f_stp = f_stp + 1
        LET f_divisor = f_divisor /1000

END WHILE
    LET f_numero = valor
    LET f_deci = (valor - f_numero)*100
    IF valor >= 2
       THEN IF f_deci = 0
             THEN LET f_cox = " EXACTOS"
             ELSE LET f_cox = " CON ",f_deci,"/100"
            END IF
            IF f_deci > 0 AND f_deci < 9
              THEN LET f_deci2 = f_deci
                   LET f_cox = " CON 0",f_deci2,"/100" 
            END IF 

            LET f_letras = "*",f_letras CLIPPED," ",f_cox CLIPPED," *"
       ELSE IF valor < 1
             THEN IF f_deci > 1
                   THEN LET f_letras = "* ",f_deci," CENTAVOS *"
                   ELSE LET f_letras = "* ",f_deci," CENTAVO *"
                  END IF
            ELSE IF f_deci = 0
                  THEN LET f_cox = " EXACTO"
                  ELSE LET f_cox = " CON ",f_deci,"/100"
                  END IF

                  IF f_deci > 0 AND f_deci < 9
                     THEN LET f_deci2 = f_deci
                          LET f_cox = " CON 0",f_deci2,"/100" 
                  END IF 

                  LET f_letras="* ",f_letras CLIPPED," ",f_cox CLIPPED," *"

            END IF
    END IF

    LET esp  = 0
    LET j    = 1
    FOR i = 1 TO LENGTH(f_letras)
     IF (f_letras[i,i] = " ") THEN
        LET esp = (esp+1)
     ELSE
        LET esp = 0 
     END IF

     IF (esp<=1) THEN
        LET letras[j,j] = f_letras[i,i] 
        LET j = (j+1)
     END IF
    END FOR
    RETURN letras  
END FUNCTION

{ Subrutina para preguntar Si o No }

FUNCTION yesornot(x,y,msg)
 DEFINE msg CHAR(45),
        x,y SMALLINT,
        res SMALLINT 

 OPEN WINDOW wstop AT x,y
  WITH 1 ROWS,60 COLUMNS ATTRIBUTE(BORDER,MENU LINE 1) 
  MENU msg
   COMMAND "Si"
    LET res = TRUE
    EXIT MENU 
   COMMAND "No"
    LET res = FALSE
    EXIT MENU 
  END MENU 
 CLOSE WINDOW wstop

 RETURN res 
END FUNCTION

{ Subrutina para chequear el buzon de correo de un usuario }

FUNCTION librut001_checkemail(wusuario)
 DEFINE wusuario CHAR(10),
        wbemail  CHAR(50)

 SELECT a.bemail 
  INTO  wbemail
  FROM  globales:glb_usuarios a
  WHERE (a.userid = wusuario) 
  IF (status=NOTFOUND) THEN
     LET wbemail = NULL
  END IF
  RETURN wbemail 
END FUNCTION 

{ Subrutina para ingresar un periodo de fechas }

FUNCTION periodofechas()   
 DEFINE lastkey,regreso       SMALLINT,
        w_fechaini,w_fechafin DATE

 -- Inicializando datos
 INITIALIZE w_fechaini,w_fechafin TO NULL
 LET regreso = FALSE
    
 -- Ingresando periodo de fechas
 INPUT BY NAME w_fechaini,w_fechafin WITHOUT DEFAULTS
  ATTRIBUTE(CANCEL=FALSE) 
  ON ACTION regreso 
   -- Salida 
   LET regreso = TRUE
   EXIT INPUT 
  AFTER FIELD w_fechaini
   LET lastkey = FGL_LASTKEY()
   IF (lastkey = FGL_KEYVAL("up")) OR (lastkey = FGL_KEYVAL("left")) THEN
      LET regreso = TRUE
      EXIT INPUT
   END IF

   -- Verificando fecha inicial 
   IF w_fechaini IS NULL THEN
      ERROR "Error: debe ingresarse la fecha inicial del periodo. (VERIFICA)"
      NEXT FIELD w_fechaini
   END IF 

   IF (w_fechaini>w_fechafin) THEN
      ERROR "Atencion: fecha inicial debe ser menor a fecha final. (VERIFICA)"
      NEXT FIELD w_fechaini
   END IF 
   DISPLAY BY NAME w_fechaini
  AFTER FIELD w_fechafin
   LET lastkey = FGL_LASTKEY()
   IF (lastkey = FGL_KEYVAL("up")) OR (lastkey = FGL_KEYVAL("left")) THEN
      NEXT FIELD w_fechaini
   END IF

   -- Verificando fecha inicial 
   IF w_fechafin IS NULL THEN
      ERROR "Error: debe ingresarse la fecha final del periodo. (VERIFICA)"
      NEXT FIELD w_fechafin
   END IF 

   -- Verificando fecha final
   IF (w_fechafin<w_fechaini) THEN
      ERROR "Atencion: fecha final debe ser mayor a fecha inicial. (VERIFICA)"
      NEXT FIELD w_fechafin
   END IF 
   DISPLAY BY NAME w_fechafin 
  AFTER INPUT
   IF w_fechafin IS NULL THEN
      NEXT FIELD w_fechafin
   END IF 
 END INPUT 
 RETURN regreso,w_fechaini,w_fechafin
END FUNCTION 

{ Subrutina para desplegar mensajes de advertencia }

FUNCTION librut001_msg(msg,msg2)
 DEFINE msg,msg2  CHAR(80) 

 MENU "Advertencia"
 ATTRIBUTE(STYLE="dialog",COMMENT=msg CLIPPED,IMAGE="stop")
  COMMAND "Aceptar"
 END MENU
END FUNCTION 

{ Subrutina para el menu de grabacion }

FUNCTION librut001_menugraba(msg,opc1,opc2,opc3,opc4)
 DEFINE ope  SMALLINT,
        msg,
        opc1,
        opc2,
        opc3,
        opc4 STRING

  MENU msg 
  ATTRIBUTE(STYLE="dialog",COMMENT=" What do you want ?",IMAGE="question")
  BEFORE MENU
   IF opc1 IS NULL THEN
      HIDE OPTION opc1
   END IF
   IF opc2 IS NULL THEN
      HIDE OPTION opc2
   END IF
   IF opc3 IS NULL THEN
      HIDE OPTION opc3
   END IF
   IF opc4 IS NULL THEN
      HIDE OPTION opc4
   END IF
  COMMAND opc1
   LET ope = 1
  COMMAND opc2
   LET ope = 2
  COMMAND opc3
   LET ope = 0
  COMMAND opc4 
   LET ope = 3 
  END MENU
  RETURN ope
END FUNCTION

{ Subrutina para el menu de confirmacion a una pregunta }

FUNCTION librut001_yesornot(title,msg,opc1,opc2,icon)
 DEFINE ope  SMALLINT,
        title,
        msg,
        opc1,
        opc2,
        icon STRING

  MENU title
  ATTRIBUTE(STYLE="dialog",COMMENT=msg,IMAGE=icon)
  COMMAND opc1
   LET ope = 1
  COMMAND opc2
   LET ope = 0
  END MENU
  RETURN ope
END FUNCTION

{ Subrutina para seleccionar el destino del reporte }

FUNCTION librut001_dreporte(x,y,title)
 DEFINE pipeline,msg,title STRING,
        x,y                SMALLINT

 -- Abriendo ventana de seleccion
 OPEN WINDOW wdestino WITH FORM "formrept"
  ATTRIBUTE(TEXT="Destino del Reporte") 
  LET pipeline = NULL 

  MENU "" 
   COMMAND "pantalla"
           "Envia el reporte a la pantalla." 
           LET pipeline = "screen"
           EXIT MENU
   COMMAND "impresora"
           "Envia el reporte a la impresora seleccionada." 

           -- Seleccionando impresora  
           LET pipeline = menu_prm(x,y,"PRINTERS","I","Impresoras") 
           IF pipeline IS NULL THEN
              CONTINUE MENU
           END IF 
           EXIT MENU
   COMMAND "pdf"
           "Envia el reporte a un archivo pdf." 
           LET pipeline = "pdf"
           EXIT MENU
   COMMAND "email"
           "Envia el reporte al buzon de correo del usuario."
           -- Aignando asunto
           LET subject = title

           -- Verificando buzon de correo
           LET pipeline = librut001_checkemail("koko") --FGL_GETENV("LOGNAME")) 
           IF pipeline IS NULL THEN
              ERROR "Atencion:  usuario (",FGL_GETENV("LOGNAME") CLIPPED,
                    ") sin buzon de correo definido."
              CONTINUE MENU 
           END IF 

           EXIT MENU 
   COMMAND KEY(F4,CONTROL-E,INTERRUPT) "cancelar" 
           "Cancela la emision del reporte."
           LET pipeline = NULL
           EXIT MENU
   COMMAND KEY ("!") 
           CALL subshell()
  END MENU
 CLOSE WINDOW wdestino 
 RETURN pipeline  
END FUNCTION 

{ Subrutina para ejecutar la salida de un reporte }

FUNCTION librut001_sreporte(filename,pipeline,title)
 DEFINE wcommand,pipeline,filename,title,wdocto STRING

 -- Tipos de dispositivo (pipeline)
 -- screen = pantalla
 -- *@*    = buzon de correo
 -- Otros  = local,laser = impresora

 -- Seleccionando dispositivo 
 CASE 
  WHEN (pipeline="screen")                      
   CALL librut001_visor(filename,title)
   -- Ejecutando commando
   RUN wcommand 
  WHEN (pipeline MATCHES "*@*")
   LET wcommand = "mailto '",subject  CLIPPED,"' ",
                             pipeline CLIPPED," ",
                             filename CLIPPED
   -- Ejecutando commando
   RUN wcommand 
  WHEN (pipeline="local") 
   LET wcommand = "prtty ",filename CLIPPED
   -- Ejecutando commando
   RUN wcommand 
  WHEN (pipeline="laser") 
   LET wcommand = "prtty ",filename CLIPPED
   -- Ejecutando commando
   RUN wcommand 
  WHEN (pipeline="pdf") 
   LET wcommand = "txt2pdf ",filename CLIPPED
   --Ejecutando comando
   RUN wcommand 
   LET filename = librut001_basename(filename,"pdf") 
   LET wdocto   = "http://",FGL_GETENV("IPWEB") CLIPPED,"/",
                  FGL_GETENV("LOGNAME") CLIPPED,"/",filename CLIPPED
   IF NOT winshellexec(wdocto) THEN
      CALL librut001_dsperror("acrobat reader no instalado")
   END IF 
  OTHERWISE
   LET wcommand = "cat -s ",filename CLIPPED,"|",pipeline CLIPPED
   -- Ejecutando commando
   RUN wcommand 
 END CASE

END FUNCTION

{ Subrutina para enviar un arhivo a un visor de pantalla }

FUNCTION librut001_visor(fn,title)
 DEFINE visordata,fn,title STRING

 -- Asignando titulo
 LET title = " Visor de Reporte - ",title CLIPPED

 -- Llenando visor de datos
 LET visordata = librut001_readfile(fn) 

 -- Abriendo la ventana del visor
 OPEN WINDOW visor WITH FORM "formvisr"
  ATTRIBUTE(TEXT=title) 

  -- Desplegando datos en el visor
  DISPLAY BY NAME visordata
  MENU ""
   COMMAND KEY(INTERRUPT) "salida" 
    EXIT MENU
  END MENU

 -- Cerrando ventana del visor 
 CLOSE WINDOW visor
END FUNCTION

{ Subrutina para leerr las lineas de un archivo }

FUNCTION librut001_readfile(fn)
 DEFINE fn  STRING
 DEFINE txt STRING
 DEFINE ln  STRING
 DEFINE ch  base.Channel

 LET ch=base.Channel.create()
 CALL ch.openfile(fn,"r")
 CALL ch.setDelimiter("")
 WHILE ch.read(ln)
  IF txt IS NULL THEN
     IF ln IS NULL THEN
        LET txt = "\n"
     ELSE  
        LET txt = ln
     END IF
  ELSE
     IF ln IS NULL THEN
        LET txt = txt || "\n"
     ELSE
        LET txt = txt || "\n" || ln
     END IF
  END IF
 END WHILE
 CALL ch.close()
 RETURN txt
END FUNCTION

{ Subrutina para cambiar el atributo TEXT de algun objeto en un forma }

FUNCTION librut001_dpelement(ne,st)
 DEFINE st,ne STRING,
        w     ui.Window,
        f     ui.Form

 -- Obteniendo datos de la ventana
 LET w = ui.Window.getCurrent()

 -- Obteniendo datos de la forma
 LET f = w.getForm()

 -- Desplegando elemento
 CALL f.setElementText(ne,st)
END FUNCTION

{ Subrutina para abri un archivo y hacer una conexion DDE }

FUNCTION librut001_opendde(doc)
 CONSTANT prog = "EXCEL" 
 DEFINE doc STRING 

 -- Abriendo documento 
 IF NOT winshellexec(doc) THEN 
    display doc 
    CALL librut001_dsperror("WinShellexec")
 END IF 

 -- Demora entre apertura del archivo y conexion 
 SLEEP 3 

 -- Abriendo documento por medio de conexion dde 
 IF NOT DDEConnect(prog,doc) THEN
    CALL librut001_dsperror("DDEConnect")
 END IF
END FUNCTION

{ Subrutina para tranferir datos a un archivo excel por medio 
  de una conexion DDEPoke }

FUNCTION librut001_toexcel(doc,valcel,filrow)
 CONSTANT prog = "EXCEL" 
 DEFINE doc,valcel,filrow STRING 

 -- Trasladando informacion 
 IF NOT DDEPoke(prog,doc,filrow,valcel) THEN
    CALL librut001_dsperror("DDEPoke")
 END IF
END FUNCTION 

{ Subrutina para cerrar una conexion DDE }

FUNCTION librut001_closedde(doc) 
 CONSTANT prog = "EXCEL" 
 DEFINE doc STRING 

 -- Salvando documento
 IF NOT DDEExecute(prog,doc,"save") THEN
    CALL librut001_dsperror("DDEExecute") 
 END IF 

 -- Cerrando conexion dde 
 IF NOT DDEFinish(prog,doc) THEN
    CALL librut001_dsperror("DDEFinish")
 END IF

 -- Finalizando conexion 
 CALL DDEFinishAll() 
END FUNCTION

{ Subrutina para desplegar mensajes de error de conexion DDE }

FUNCTION librut001_dsperror(f)
  DEFINE f VARCHAR(20)
  DISPLAY "Error: ", f,"():",DDEGetError()
  EXIT PROGRAM
END FUNCTION

{ Subrutina para seleccionar el destino del reporte }

FUNCTION librut001_desreporte(x,y,title)
 DEFINE pipeline,msg,title STRING,
        x,y                SMALLINT

 -- Abriendo ventana de seleccion
  LET pipeline = NULL 
  MENU "" 
   ON ACTION pantalla
           LET pipeline = "screen"
           EXIT MENU
   ON ACTION impresora

           -- Seleccionando impresora  
           LET pipeline = menu_prm(x,y,"PRINTERS","I","Impresoras") 
           IF pipeline IS NULL THEN
              CONTINUE MENU
           END IF 
           EXIT MENU
   ON ACTION correo
           -- Aignando asunto
           LET subject = title

           -- Verificando buzon de correo
           LET pipeline = librut001_checkemail("koko") --FGL_GETENV("LOGNAME")) 
           IF pipeline IS NULL THEN
              ERROR "Atencion:  usuario (",FGL_GETENV("LOGNAME") CLIPPED,
                    ") sin buzon de correo definido."
              CONTINUE MENU 
           END IF 

           EXIT MENU 
  ON ACTION exel
    LET pipeline="exel"
    EXIT MENU 
  ON ACTION cancelar  
           LET pipeline = NULL
           EXIT MENU
  END MENU
 RETURN pipeline  
END FUNCTION 

{ Subrutina para ejecutar la salida de un reporte }

FUNCTION librut001_sendreporte(filename,pipeline,title,fn,fil,col)
 DEFINE wcommand,pipeline,filename,title,fn STRING,
        fil,col  SMALLINT

 -- Tipos de dispositivo (pipeline)
 -- screen = pantalla
 -- *@*    = buzon de correo
 -- Otros  = local,laser = impresora

 -- Seleccionando dispositivo 
 CASE 
  WHEN (pipeline="screen")                      
   CALL librut001_visor(filename,title)
  WHEN (pipeline MATCHES "*@*")
   LET wcommand = "mailto '",subject  CLIPPED,"' ",
                             pipeline CLIPPED," ",
                             filename CLIPPED
  WHEN (pipeline="local") 
   LET wcommand = "prtty ",filename CLIPPED
  WHEN (pipeline="laser") 
   LET wcommand = "prtty ",filename CLIPPED
  WHEN (pipeline="exel") 
   CALL librut001_sendexcel(fil,col,fn)
  OTHERWISE
   LET wcommand = "cat -s ",filename CLIPPED,"|",pipeline CLIPPED
 END CASE

 -- Ejecutando commando
 RUN wcommand 
END FUNCTION

{ Subrutina para cargar los datos a excel } 

FUNCTION librut001_sendexcel(filini,colini,filename) 
 CONSTANT ncols = 58
 DEFINE valcel,filrow                           DYNAMIC ARRAY OF STRING,
        totlin,numfil,numcol,filini,colini,totcol,i   SMALLINT,
        filename                                      STRING,
        vcols                                         RECORD 
         numrep                                       SMALLINT,
         col001                                       CHAR(50),
         col002                                       CHAR(50),
         col003                                       CHAR(50),
         col004                                       CHAR(50),
         col005                                       CHAR(50),
         col006                                       CHAR(50),
         col007                                       CHAR(50),
         col008                                       CHAR(50),
         col009                                       CHAR(50),
         col010                                       CHAR(50),
         col011                                       CHAR(50),
         col012                                       CHAR(50),
         col013                                       CHAR(50),
         col014                                       CHAR(50),
         col015                                       CHAR(50),
         col016                                       CHAR(50),
         col017                                       CHAR(50),
         col018                                       CHAR(50),
         col019                                       CHAR(50),
         col020                                       CHAR(50),
         col021                                       CHAR(50),
         col022                                       CHAR(50),
         col023                                       CHAR(50),
         col024                                       CHAR(50),
         col025                                       CHAR(50),
         col026                                       CHAR(50),
         col027                                       CHAR(50),
         col028                                       CHAR(50),
         col029                                       CHAR(50),
         col030                                       CHAR(50),
         col031                                       CHAR(50),
         col032                                       CHAR(50),
         col033                                       CHAR(50),
         col034                                       CHAR(50),
         col035                                       CHAR(50),
         col036                                       CHAR(50),
         col037                                       CHAR(50),
         col038                                       CHAR(50), 
         col039                                       CHAR(50),
         col040                                       CHAR(50),
         col041                                       CHAR(50),
         col042                                       CHAR(50),
         col043                                       CHAR(50),
         col044                                       CHAR(50),
         col045                                       CHAR(50),
         col046                                       CHAR(50),
         col047                                       CHAR(50),
         col048                                       CHAR(50),
         col049                                       CHAR(50),
         col050                                       CHAR(50),
         col051                                       CHAR(50),
         col052                                       CHAR(50),
         col053                                       CHAR(50),
         col054                                       CHAR(50),
         col055                                       CHAR(50),
         col056                                       CHAR(50),
         col057                                       CHAR(50),
         col058                                       CHAR(50)
        END RECORD   

 -- Inicializando el vector
 CALL valcel.clear()
 CALL filrow.clear()

 -- Seleccionando columnas 
 DECLARE c1 CURSOR FOR
 SELECT a.*
  FROM  globales:glb_datosexc a
  ORDER BY a.numlin 

  LET totlin = 1
  LET numfil = filini 
  LET numcol = colini
  LET totcol = (colini+ncols) 
  FOREACH c1 INTO vcols.*
   LET valcel[totlin] = vcols.numrep,"\\t",vcols.col001,"\\t",
                        vcols.col002,"\\t",vcols.col003,"\\t",
                        vcols.col004,"\\t",vcols.col005,"\\t",
                        vcols.col006,"\\t",vcols.col007,"\\t",
                        vcols.col008,"\\t",vcols.col009,"\\t",
                        vcols.col010,"\\t",vcols.col011,"\\t",
                        vcols.col012,"\\t",vcols.col013,"\\t",
                        vcols.col014,"\\t",vcols.col015,"\\t",
                        vcols.col016,"\\t",vcols.col017,"\\t",
                        vcols.col018,"\\t",vcols.col019,"\\t",
                        vcols.col020,"\\t",vcols.col021,"\\t",
                        vcols.col022,"\\t",vcols.col023,"\\t",
                        vcols.col024,"\\t",vcols.col025,"\\t",
                        vcols.col026,"\\t",vcols.col027,"\\t",
                        vcols.col028,"\\t",vcols.col029,"\\t",
                        vcols.col030,"\\t",vcols.col031,"\\t",
                        vcols.col032,"\\t",vcols.col033,"\\t",
                        vcols.col034,"\\t",vcols.col035,"\\t",
                        vcols.col036,"\\t",vcols.col037,"\\t",
                        vcols.col038,"\\t",vcols.col039,"\\t",
                        vcols.col040,"\\t",vcols.col041,"\\t",
                        vcols.col042,"\\t",vcols.col043,"\\t",
                        vcols.col044,"\\t",vcols.col045,"\\t",
                        vcols.col046,"\\t",vcols.col047,"\\t",
                        vcols.col048,"\\t",vcols.col049,"\\t",
                        vcols.col050,"\\t",vcols.col051,"\\t",
                        vcols.col052,"\\t",vcols.col053,"\\t",
                        vcols.col054,"\\t",vcols.col055,"\\t",
                        vcols.col056,"\\t",vcols.col057,"\\t",
                        vcols.col058,"\\t"

   LET filrow[totlin] = "F",numfil USING "<<<<<","C",numcol USING "<<<",":",
                        "F",numfil USING "<<<<<","C",totcol USING "<<<<<" 

   LET totlin = (totlin+1)
   LET numfil = (numfil+1) 
  END FOREACH
  CLOSE c1
  FREE  c1
  LET totlin = (totlin-1)
 
 -- Abriendo archivo y conexion DDE
 CALL librut001_opendde(filename)

 FOR i = 1 TO totlin 
  -- Transfiriendo datos a excel por medio de DDE
  CALL librut001_toexcel(filename,valcel[i],filrow[i])
 END FOR 

 -- Cerrando conexion DDE
 CALL librut001_closedde(filename) 
END FUNCTION 

{ Subrutina para quitarle la extension a un nombre de archivo y ponerle 
  otra }

FUNCTION librut001_basename(filename,extension) 
 DEFINE filename,extension,newfile CHAR(200), 
        i                          SMALLINT 

 -- Asignando nueva extension
 LET newfile = NULL
 FOR i = 1 TO LENGTH(filename) 
  IF (filename[i,i]=".") THEN
     EXIT FOR 
  END IF 
  LET newfile = newfile CLIPPED,filename[i,i] 
 END FOR 
 LET filename = newfile CLIPPED,".",extension CLIPPED

 -- Quitando directorio base 
 LET newfile = NULL
 FOR i = LENGTH(filename) TO 1 STEP -1 
  IF (filename[i,i]="/") THEN
     EXIT FOR 
  END IF 
  LET newfile = newfile CLIPPED,filename[i,i] 
 END FOR 
 LET filename = newfile

 -- Armando nuevo nombre
 LET newfile = NULL
 FOR i = LENGTH(filename) TO 1 STEP -1 
  LET newfile = newfile CLIPPED,filename[i,i] 
 END FOR 

 RETURN newfile 
END FUNCTION 

{ Subrutina para un menu de opciones }

FUNCTION librut001_menuopcs(msg1,msg2,opc1,opc2,opc3,opc4)
 DEFINE ope  SMALLINT,
        msg1,
        msg2,
        opc1,
        opc2,
        opc3,
        opc4 STRING

  MENU msg1 
  ATTRIBUTE(STYLE="dialog",COMMENT=msg2,IMAGE="question")
  BEFORE MENU
   IF opc1 IS NULL THEN
      HIDE OPTION opc1
   END IF
   IF opc2 IS NULL THEN
      HIDE OPTION opc2
   END IF
   IF opc3 IS NULL THEN
      HIDE OPTION opc3
   END IF
   IF opc4 IS NULL THEN
      HIDE OPTION opc4
   END IF
  COMMAND opc1
   LET ope = 1
  COMMAND opc2
   LET ope = 2
  COMMAND opc3
   LET ope = 3 
  COMMAND opc4 
   LET ope = 0
  END MENU
  RETURN ope
END FUNCTION

{ Subrutina para reemplazar una parte de una cadeca de caracteres }

FUNCTION librut001_replace(hilera,pattern,replace,ntimes)
 DEFINE buf                    base.StringBuffer,       
        ntimes                 SMALLINT, 
        hilera,replace,pattern STRING 

 -- Creando buffer
 LET buf = base.StringBuffer.create()

 -- Agregando hilera al buffer
 CALL buf.append(hilera)

 -- Reemplazando string buscado x nuevo string en la hilera x n veces 
 CALL buf.replace(pattern,replace,ntimes)

 -- Returnando el string
 RETURN buf.toString()
END FUNCTION 

{ Subrutina para armar dos fechas de un mes }

FUNCTION librut001_inifinmes(wmes,wanio)
 DEFINE wmes,wanio SMALLINT,
        wfecini    DATE,
        wfecfin    DATE

 -- Creando fechas
 LET wfecini = MDY(wmes,1,wanio)
 LET wfecfin = wfecini + 1 UNITS MONTH
 LET wfecfin = (wfecfin-1)

 RETURN wfecini,wfecfin
END FUNCTION

