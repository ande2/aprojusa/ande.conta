{
ADM
Programa : librut02.4gl
Fecha    : Junio 2010
Programo : Mynor Ramirez
Objetivo : Programa de subrutinas de libreria.
}

--Subrutina para crear combo box dinamicos 

FUNCTION librut02_combobox(vl_2_campo,vl_2_query)
 DEFINE vl_2_lista_nodb base.StringTokenizer
 DEFINE vl_2_combo      ui.ComboBox
 DEFINE vl_2_campo      STRING
 DEFINE vl_2_query      STRING
 DEFINE vl_2_metodo     STRING
 DEFINE vl_2_valor      STRING
 DEFINE vl_2_valor2     STRING
 DEFINE vl_2_lista      CHAR(500)
 DEFINE vl_2_lista2     CHAR(500)

 -- Quitando espacioes en blanco
 LET vl_2_campo = vl_2_campo.trim()
 LET vl_2_query = vl_2_query.trim()

 -- Obtiene el nodo del metodo combo de la clase ui
 LET vl_2_combo = ui.ComboBox.forName(vl_2_campo)
 IF vl_2_combo IS NULL THEN
    RETURN
 END IF

 -- Inicializa el combo
 CALL vl_2_combo.clear()

 -- Convirtiendo todo a minusculas
 LET vl_2_metodo = vl_2_query.toUpperCase()

 -- Verificando metodo
 IF vl_2_metodo matches "SELECT*" THEN
    --Preparando el query
    PREPARE q_comboList2 from vl_2_query
    DECLARE c_comboList2 cursor FOR q_comboList2

    -- Obteniendo la lista de valores de la tabla segun el query enviado
    FOREACH c_comboList2 INTO vl_2_lista,vl_2_lista2
     LET vl_2_valor  = vl_2_lista  CLIPPED
     LET vl_2_valor2 = vl_2_lista2 CLIPPED
     CALL vl_2_combo.addItem(vl_2_valor,vl_2_valor2)
    END FOREACH
 ELSE
    -- Obteniendo la lista de valores que no son de base de datos
    LET vl_2_lista_nodb = base.StringTokenizer.create(vl_2_query,"|")

    WHILE vl_2_lista_nodb.hasMoreTokens()
     LET  vl_2_valor = vl_2_lista_nodb.nextToken()
     CALL vl_2_combo.addItem(vl_2_valor,vl_2_valor)
    END WHILE
 END IF
END FUNCTION
