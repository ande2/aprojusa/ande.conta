{
Programa : librut003.4gl 
Programo : Mynor Ramirez
Objetivo : Subrutinas de librerias -- chequeo de integridad de datos
}

DATABASE erpjuridico 

-- Subrutina para verificar si los usuarios ya tienen registros

FUNCTION librut004_IntegridadUsuarios(wuserid)
 DEFINE wuserid  LIKE glb_usuarios.userid,
        conteo   INTEGER   

 -- Verificando pesaje
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_mtransac a 
  WHERE (a.userid = wuserid)
  IF (conteo>0) THEN
     RETURN 1
  ELSE
     RETURN 0
  END IF
END FUNCTION

-- Subrutina para verificar si el perfil ya tiene registros 

FUNCTION librut004_IntegridadPerfiles(wroleid) 
 DEFINE wroleid  LIKE glb_usuarios.roleid,
        conteo   INTEGER    

 -- Verificando usuarios 
 SELECT COUNT(*)
  INTO  conteo
  FROM  glb_usuarios a
  WHERE (a.roleid = wroleid)
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF
END FUNCTION

