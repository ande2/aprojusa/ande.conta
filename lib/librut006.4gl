{
Programa : librut006.4gl 
Programo : Mynor Ramirez
Objetivo : Subruitinas para operacion de insert,delete,updte 
}

DATABASE erpjuridico 

-- Subrutina para grabar el correlativo de etiquetas

FUNCTION librut006_CorrelativoEtiquetas() 
 DEFINE wnumetq INTEGER 

 -- Generando numero correlativo automatico de etiqueta
 INSERT INTO inv_ncorretq
 VALUES (0)
 LET wnumetq = SQLCA.SQLERRD[2]

 RETURN wnumetq
END FUNCTION
