DATABASE erpjuridico
 
MAIN
 DEFINE retorna      RECORD  
         codigo      VARCHAR(20),
         descripcion VARCHAR(100)
	END RECORD, 
        regreso      SMALLINT 

 CALL picklist_2("Busqueda","Categorias","Datos","codcat","nomcat","glb_categors","1=1",1,0)
 RETURNING retorna.*,regreso 
END MAIN 


FUNCTION picklist_2(Title0,Title1,Title2,Field1,Field2,Tabname,Condicion,OrderNum,opci)
 DEFINE Title0       STRING,
	Title1,  
	Title2       STRING,  
	Field1, 
	Field2,
	Condicion    STRING,
	Tabname      STRING,
	OrderNum     STRING, 
        retorna      RECORD  
         codigo      VARCHAR(20),
         descripcion VARCHAR(100)
	END RECORD,
	la_busqueda  DYNAMIC ARRAY OF RECORD 	
         codigo      VARCHAR(20),
         descripcion VARCHAR(100)
        END RECORD,
        i,aborta     SMALLINT,
        w            ui.Window,
        f            ui.Form,
        comia        CHAR(2),
        sqlstmnt     STRING,
        refe         VARCHAR(100),
        buscar       VARCHAR(105),
        opci         SMALLINT, 
        scr_cnt      SMALLINT


 -- Definiendo tecla de aceptar
 OPTIONS ACCEPT KEY escape

 LET int_flag = 0  
 LET comia = ASCII 34

 -- Abriendo la ventana                                                                
 OPEN WINDOW find_ayuda 
  WITH FORM "picklist_2"

  -- Obteniendo datos de la ventana
  LET w = ui.Window.getcurrent()
  LET f = w.getForm()

  -- Desplegandp titulso
  -- CALL f.loadActionDefaults("defaultpick")
  CALL ui.Interface.loadStyles("styles")
  CALL f.setElementText("filtro",title0)
  CALL f.setElementText("formonly.codigo",title1)
  CALL f.setElementText("formonly.descripcion",title2)
  CALL fgl_settitle("Buaqueda Dinamica")

  -- Inicializando datos
  CALL la_busqueda.CLEAR()
  LET aborta = 0  

  -- Buscando datos 
  WHILE aborta = 0
   INITIALIZE retorna.* TO NULL 
   LET refe = "Ingrese informacion a buscar ..."
   LET int_flag = FALSE  
   LET buscar = refe 
   LET i = 1

   -- Ingresando datos a buscar
   INPUT BY NAME refe WITHOUT DEFAULTS 
    BEFORE INPUT
     -- Desplegando teclas
     CALL fgl_dialog_setkeylabel("ACCEPT","Detalle")
     CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")

    BEFORE FIELD refe
     -- Si no hay ingreso de datos a buscar 
     IF opci=0 THEN
       -- Creando busqueda
       LET sqlstmnt = " SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum

       display "query", sqlstmnt

       -- Preparando busqueda
       PREPARE ex_sqlst FROM sqlstmnt

       -- Seleccionando datos
       DECLARE estos0 CURSOR FOR ex_sqlst
       CALL la_busqueda.CLEAR()
       LET i =1
       FOREACH estos0 INTO la_busqueda[i].*
        LET i=i+1
       END FOREACH
       FREE estos0
       FREE ex_sqlst

      -- Desplegando numero de datos seleccionados
      CALL f.setElementText("registros",'Registros encontrados '||(i-1))

      -- Desplegnando datos
      DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
       BEFORE DISPLAY
        -- Desplegando teclas
        --CALL fgl_dialog_setkeylabel("ACCEPT","")
        --CALL fgl_dialog_setkeylabel("INTERRUPT","")
        --CALL fgl_dialog_setkeylabel("insert", "")
        --CALL fgl_dialog_setkeylabel("Append", "")
        --CALL fgl_dialog_setkeylabel("Delete", "")

       BEFORE ROW
        EXIT DISPLAY
      END DISPLAY

      LET buscar = refe
      EXIT INPUT
     END IF

    AFTER FIELD refe
     -- Verificando rotulo
     IF refe = "Ingrese informacion a buscar ..." THEN
        LET refe = "*"
     END IF
     LET buscar = "*"||refe||"*"

     -- Si se ingresan datos a buscar
     IF opci=1 THEN 
        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
                        " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                        " AND upper(", field2 CLIPPED, ") MATCHES upper(",comia CLIPPED, buscar CLIPPED, comia CLIPPED, ")",
                        " ORDER BY ",OrderNum
     ELSE
        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum
     END IF

     display "query", sqlstmnt

     -- Preparando busqueda
     PREPARE ex_sqlst1 FROM sqlstmnt

     -- Seleccionando datos
     DECLARE estos1 CURSOR FOR ex_sqlst1
     CALL la_busqueda.CLEAR()
     LET i =1
     FOREACH estos1 INTO la_busqueda[i].*
      LET i=i+1
     END FOREACH
     FREE estos1
     FREE ex_sqlst1

     -- Desplegando numero de datos seleccionados
     CALL f.setElementText("registros",'Registros encontrados '||(i-1))

     -- Desplegnando datos
     DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
      BEFORE DISPLAY
       -- Desplegando teclas
       ----CALL fgl_dialog_setkeylabel("ACCEPT","")
       --CALL fgl_dialog_setkeylabel("INTERRUPT","")
       --CALL fgl_dialog_setkeylabel("insert", "")
       --CALL fgl_dialog_setkeylabel("Append", "")
      -- CALL fgl_dialog_setkeylabel("Delete", "")

      BEFORE ROW
       EXIT DISPLAY
     END DISPLAY
     LET buscar = refe
     EXIT INPUT

    ON IDLE 1
     -- Leyendo datos del buffer cada segundo 
     LET refe = GET_FLDBUF(refe)
     IF LENGTH(refe)=0 THEN
        LET refe = ""
     END IF

     IF buscar<>refe OR 
        opci=0 THEN
        LET buscar = "*"||refe||"*"

      -- Si se ingresan datos a buscar
      IF opci=1 THEN 
        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
                        " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                        " AND upper(", field2 CLIPPED, ") MATCHES upper(",comia CLIPPED, buscar CLIPPED, comia CLIPPED, ")",
                        " ORDER BY ",OrderNum
      ELSE
        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum
      END IF

      display "query", sqlstmnt

      -- Preparando busqueda
      PREPARE ex_sqlst2 FROM sqlstmnt

      -- Seleccionando datos
      DECLARE estos2 CURSOR FOR ex_sqlst2
      CALL la_busqueda.CLEAR()
      LET i =1
      FOREACH estos2 INTO la_busqueda[i].*
       LET i=i+1
      END FOREACH
      FREE estos2
      FREE ex_sqlst2

      -- Desplegando numero de datos seleccionados
      CALL f.setElementText("registros",'Registros encontrados '||(i-1))
 
      -- Desplegnando datos
      LET aborta = 0  
      DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
       BEFORE DISPLAY
        -- Desplegando teclas
        --CALL fgl_dialog_setkeylabel("ACCEPT","")
        --CALL fgl_dialog_setkeylabel("INTERRUPT","")
        --CALL fgl_dialog_setkeylabel("insert", "")
        --CALL fgl_dialog_setkeylabel("Append", "")
        --CALL fgl_dialog_setkeylabel("Delete", "")
 
       BEFORE ROW
        EXIT DISPLAY
      END DISPLAY
      LET buscar = refe

      -- Si no se pedian datos de busqueda
      IF opci=0 THEN  
         EXIT INPUT
      END IF
     END IF 

    AFTER INPUT
     IF int_flag THEN
        LET aborta = 1
        EXIT INPUT
     END IF
   END INPUT
   IF aborta THEN
      LET int_flag = TRUE
      EXIT WHILE
   END IF

   -- Definiendo tecla de aceptacion
   OPTIONS ACCEPT KEY RETURN

   -- Desplegnado datos
   CALL f.setElementText("registros",'Registros encontrados '||(i-1)|| "    Presione [ENTER] para seleccionar ")

   DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
    BEFORE DISPLAY
     -- Desplegando teclas
     CALL fgl_dialog_setkeylabel("ACCEPT","Seleccionar\nRegistro")
     --CALL fgl_dialog_setkeylabel("insert", "")
     --CALL fgl_dialog_setkeylabel("Append", "")
     --CALL fgl_dialog_setkeylabel("Delete", "")

     -- Si opcion es ingresar datos
     IF opci=1 THEN
        CALL fgl_dialog_setkeylabel("INTERRUPT","Nueva busqueda")
     ELSE
        CALL fgl_dialog_setkeylabel("INTERRUPT","")
     END IF

     -- Si no se ecnontraron datos
     IF i=1 THEN
        --CALL box_valdato("No se encontraron datos.")
        display "NO HAY DATOS" 
     END IF
  
    BEFORE ROW
     LET i       = ARR_CURR()
     LET scr_cnt = SCR_LINE()
     LET retorna.* = la_busqueda[i].*
     IF LENGTH(la_busqueda[i].codigo)=0 THEN
        CALL fgl_dialog_setcurrline(scr_cnt,i-1)
     END IF

    ON KEY(RETURN)
     LET aborta = 1
     EXIT DISPLAY

    ON KEY(INTERRUPT)
     IF opci=1 THEN
        LET aborta = 0
        EXIT DISPLAY
     END IF

    ON KEY(ACCEPT)
     LET aborta = 1
     EXIT DISPLAY

    AFTER DISPLAY
     CONTINUE DISPLAY
   END DISPLAY
   IF aborta=1 THEN
      EXIT WHILE
   END IF
  END WHILE
 CLOSE WINDOW find_ayuda

 -- Definiendo tecla de aceptacion
 OPTIONS ACCEPT KEY ESCAPE

 RETURN retorna.*,int_flag 
END FUNCTION
