
SELECT   rpad(x.numcor,2,' ')||' - '||TRIM(x.numcta)||
   ' - '||a.nombco||' - '||x.nomcta[1,10]
    FROM  bco_mcuentas x,glb_mtbancos a
    WHERE a.codbco = x.codbco
    ORDER BY x.numcor
