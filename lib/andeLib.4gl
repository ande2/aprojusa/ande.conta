IMPORT FGL fgl_excel

{CS FUNCTION gral_reporte(tamanio, posicion, tipo, columnas, nom_reporte)
DEFINE tamanio     STRING
DEFINE posicion    STRING
DEFINE tipo        STRING
DEFINE ancho       STRING
DEFINE alto        STRING
DEFINE columnas    SMALLINT
DEFINE nom_reporte STRING
DEFINE myReporte   om.SaxDocumentHandler

        IF tamanio = "carta" OR tamanio = "CARTA" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "11in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "11in"
           END IF
        END IF

        IF tamanio = "oficio" OR tamanio = "OFICIO" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "13in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "13in"
           END IF       
        END IF CS}

        {IF tamanio = "unapagina" THEN
           LET ancho = "10000"
           LET alto  = "10000"  
        END IF}

        {CS IF NOT fgl_report_loadCurrentSettings(NULL) THEN
            RETURN
         END IF
         DISPLAY "ancho y alto ", ancho, alto
         CALL fgl_report_configurePageSize(ancho,alto)
DISPLAY "Columnas ", columnas         
         --topMargin STRING, bottomMargin STRING, leftMargin STRING, rightMargin STRING) 
         CALL fgl_report_setPageMargins("0","0","0.25inch","0.0inch")
        -- topMargin STRING, bottomMargin STRING, leftMargin STRING, rightMargin STRING )         
        --CALL fgl_report_setPaperMargins("0","0","0","0")
        -- pageWidthInCharacters, fontName, fidelity, reportName, reportCategory, systemId
         CALL fgl_report_configureCompatibilityOutput("124","Monospaced",false,null,"","")
         --CALL fgl_report_configureCompatibilityOutput(2,"Monospaced",false,null,"","")
         --CALL fgl_report_configureCompatibilityOutput(columnas,"Monospaced",false,nom_reporte,"","")
         --CALL fgl_report_configureMultipageOutput(0, 0, FALSE)
        --type STRING COMPATIBILITY - FLAT  LIST - NEW LIST
         CALL fgl_report_selectDevice("PDF")
         --CALL fgl_report_setAutoformatType("NEW LIST")
         --CALL fgl_report_setAutoformatType("NEW LIST")
         --CALL fgl_report_selectDevice(tipo)
         IF tamanio = "unapagina" THEN
           CALL fgl_report_configureXLSDevice(1,1,1,1,1,1,0)  
        END IF 
         LET myReporte = fgl_report_commitCurrentSettings()
RETURN myReporte
END FUNCTION}


FUNCTION gral_reporte(tamanio, posicion, tipo, columnas, nom_reporte)
DEFINE tamanio     STRING
DEFINE posicion    STRING
DEFINE tipo        STRING
DEFINE ancho       STRING
DEFINE alto        STRING
DEFINE columnas    SMALLINT
DEFINE nom_reporte STRING
DEFINE myReporte   om.SaxDocumentHandler

        IF tamanio = "carta" OR tamanio = "CARTA" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "11in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "11in"
           END IF
        END IF

        IF tamanio = "oficio" OR tamanio = "OFICIO" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "13in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "13in"
           END IF
        END IF

        IF NOT fgl_report_loadCurrentSettings(NULL) THEN
            RETURN
         END IF
         DISPLAY "ancho y alto ", ancho, alto
         --CS CALL fgl_report_configurePageSize(ancho,alto)
         --CS CALL fgl_report_configureCompatibilityOutput(124,"Monospaced",false,nom_reporte,"","")
         --CS CALL fgl_report_configurePageSize("14in","8.5in")
         --CALL fgl_report_configurePageSize("a4length","a4width") 
         CALL fgl_report_configurePageSize("8.5in","11in") 
         {CALL fgl_report_configureAutoformatOutput(
            "Monospaced",   --fontName STRING,
            12,             --fontSize INTEGER,
            FALSE,          --fidelity BOOLEAN,
            "Reporte",      --reportTitle STRING,
            "",             --fieldNamePatterns STRING,
            "" )            --systemId STRING )}
         CALL fgl_report_configureCompatibilityOutput(124,"Monospaced",FALSE,nom_reporte,"","")
         DISPLAY "Con 124"
         --CALL fgl_report_configureCompatibilityOutput(columnas,"Monospaced",false,nom_reporte,"","")
         --CALL fgl_report_configureMultipageOutput(0, 0, FALSE)
         CALL fgl_report_selectDevice(tipo)
         IF tamanio = "unapagina" THEN
           CALL fgl_report_configureXLSDevice(1,1,1,1,1,1,0)
        END IF
         LET myReporte = fgl_report_commitCurrentSettings()
RETURN myReporte
END FUNCTION

#########################################################################
## Function  : getPeso()
##
## Parameters: tabValida    << Tabla donde va a validar
##             colValida    << Columna que va a ir a validar
##             valKey       << Variable local que tiene la llave que va a validar
##
## Returnings: True / False
##
## Comments  : Recibe como parámetro el nombre del parametro global y
##             retorna el valor del parámetro
##             El nombre del parametro se ingresa en el catalogo general
##             de parámetros glbm0003.42r
#########################################################################

FUNCTION gral_reporte_all(tamanio, posicion, tipo, columnas, nom_reporte)
DEFINE tamanio     STRING
DEFINE posicion    STRING
DEFINE tipo        STRING
DEFINE ancho       STRING
DEFINE alto        STRING
DEFINE columnas    SMALLINT
DEFINE nom_reporte STRING
DEFINE myReporte   om.SaxDocumentHandler

DISPLAY "Entre a AndeLib"
        IF tamanio = "carta" OR tamanio = "CARTA" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "11in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "11in"
           END IF
        END IF

        IF tamanio = "oficio" OR tamanio = "OFICIO" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "13in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "13in"
           END IF       
        END IF
        
        IF NOT fgl_report_loadCurrentSettings(NULL) THEN
            RETURN
         END IF
        DISPLAY "columnas ", columnas
         --CALL fgl_report_configurePageSize(ancho,alto)
         CALL fgl_report_configureCompatibilityOutput(columnas,"fixed-10",false,nom_reporte,"","")
         --CALL fgl_report_configureCompatibilityOutput(columnas,"Monospaced",false,nom_reporte,"","")
         --CALL fgl_report_configureMultipageOutput(0, 0, FALSE)
         CALL fgl_report_selectDevice(tipo)
         CALL fgl_report_configureXLSDevice(NULL,NULL,NULL,NULL,TRUE ,NULL,TRUE)
         LET myReporte = fgl_report_commitCurrentSettings()
RETURN myReporte
END FUNCTION

FUNCTION round(dval,dpos)

DEFINE 
  dval DECIMAL ,
  dpos SMALLINT ,
  ival DECIMAL (16,0)
  
LET ival = dval * (10 ** dpos)
LET dval = ival / (10 ** dpos)
RETURN dval

END FUNCTION 

#------------------------------------------------------
# Funcion:  numsem
# Recibe una fecha y devuelve el numero de la semana
#
# Let lNumSem = numSem(lfecha)
#
#------------------------------------------------------
FUNCTION numsem(fec1)
  DEFINE fec1, fec2 DATE 

  DEFINE numday SMALLINT 
  DEFINE numwday TINYINT 
  DEFINE numsem SMALLINT 

  LET fec2 = MDY(1,1,YEAR(TODAY))
  
  LET numday = (fec1 - fec2) + 1

  LET numwday = WEEKDAY (TODAY)

  LET numsem = (numday - numwday + 10) / 7

  RETURN numsem
  
END FUNCTION

FUNCTION sql_to_excel(sql STRING, filename STRING, header BOOLEAN) RETURNS BOOLEAN
DEFINE hdl base.SqlHandle
DEFINE row_idx, col_idx INTEGER 

DEFINE workbook     fgl_excel.workbookType 
DEFINE sheet        fgl_excel.sheetType  
DEFINE row          fgl_excel.rowType  
DEFINE cell         fgl_excel.cellType 
DEFINE header_style fgl_excel.cellStyleType
DEFINE header_font  fgl_excel.fontType

DEFINE datatype STRING
    
    LET hdl = base.SqlHandle.create()
    TRY
        CALL hdl.prepare(sql)
        CALL hdl.open()
    CATCH
        RETURN FALSE
    END TRY

    CALL fgl_excel.workbook_create() RETURNING workbook

    -- create a worksheet
    CALL fgl_excel.workbook_createsheet(workbook) RETURNING sheet

    -- create data rows
    LET row_idx = 0 
    
    WHILE TRUE
        CALL hdl.fetch()
        IF STATUS=NOTFOUND THEN
            EXIT WHILE
        END IF
        LET row_idx = row_idx + 1

        IF row_idx = 1 AND header THEN
            -- create a font, will be used in header
            CALL fgl_excel.font_create(workbook) RETURNING header_font
            CALL fgl_excel.font_set(header_font, "weight", "bold")

            -- create a style, will be used in header
            CALL fgl_excel.style_create(workbook) RETURNING header_style
            CALL fgl_excel.style_set(header_style, "alignment","center")
            CALL fgl_excel.style_font_set(header_style, header_font)
   
            -- Add column headers
            CALL fgl_excel.sheet_createrow(sheet, 0) RETURNING row
            FOR col_idx = 1 TO hdl.getResultCount()
                CALL fgl_excel.row_createcell(row, col_idx-1) RETURNING cell
                CALL fgl_excel.cell_value_set(cell, hdl.getResultName(col_idx))
                CALL fgl_excel.cell_style_set(cell, header_style)
            END FOR
        END IF
        CALL fgl_excel.sheet_createrow(sheet, IIF(header,row_idx, row_idx-1)) RETURNING row

        FOR col_idx = 1 TO hdl.getResultCount()
            CALL fgl_excel.row_createcell(row, col_idx-1) RETURNING cell
            LET datatype = hdl.getResultType(col_idx) 
            CASE 
                WHEN datatype =  "INTEGER" -- TODO check logic
                  OR datatype MATCHES "DECIMAL*"
                  OR datatype MATCHES "FLOAT*"
                  OR datatype MATCHES "*INT*"
                    CALL fgl_excel.cell_number_set(cell, hdl.getResultValue(col_idx))
                OTHERWISE
                    CALL fgl_excel.cell_value_set(cell, hdl.getResultValue(col_idx))
            END CASE
        END FOR
    END WHILE

    -- TODO this code should automatically size the columns
    -- However it is very very slow for reasons I can't determine
    -- Uncomment and test at your leisure
    --IF hdl IS NOT NULL THEN
        --FOR col_idx = 1 TO hdl.getResultCount()
            --CALL fgl_excel.sheet_autosizecolumn(sheet, col_idx-1)
        --END FOR
    --END IF

    -- Write to File
    CALL fgl_excel.workbook_writeToFile(workbook, filename)

    RETURN TRUE   
END FUNCTION



FUNCTION populate()
DEFINE idx INTEGER
DEFINE rec RECORD
    integer_type INTEGER,
    date_type DATE,
    char_type CHAR(20),
    float_type FLOAT
END RECORD

    WHENEVER ERROR CONTINUE 
      DROP TABLE test_data
   WHENEVER ERROR STOP 
    CREATE TABLE test_data 
        (integer_type INTEGER,
         date_type DATE,
         char_type VARCHAR(20),
         float_type FLOAT)

    FOR idx = 1 TO 26
        LET rec.integer_type = idx
        LET rec.date_type = TODAY+idx
        LET rec.char_type = ASCII(64+idx)
        LET rec.float_type = 1/idx
        
        INSERT INTO test_data VALUES(rec.*)
    END FOR
END FUNCTION

FUNCTION nommes(vmes)
DEFINE vmes SMALLINT;
DEFINE nomMes STRING

CASE  vmes
   WHEN 1  LET nomMes = "ENERO"
   WHEN 2  LET nomMes = "FEBRERO"
   WHEN 3  LET nomMes = "MARZO"
   WHEN 4  LET nomMes = "ABRIL"
   WHEN 5  LET nomMes = "MAYO"
   WHEN 6  LET nomMes = "JUNIO"
   WHEN 7  LET nomMes = "JULIO"
   WHEN 8  LET nomMes = "AGOSTO"
   WHEN 9  LET nomMes = "SEPTIEMBRE"
   WHEN 10 LET nomMes = "OCTUBRE"
   WHEN 11 LET nomMes = "NOVIEMBRE"
   WHEN 12 LET nomMes = "DICIEMBRE"
END CASE

RETURN nomMes

END FUNCTION

#########################################################################
## Function  : isGDC()
## Parametros: Ninguno
## Retorna   : False o True
## Comments  : Verifica si el ambiente es GDC
#########################################################################
FUNCTION isGDC()
   RETURN ui.Interface.getFrontEndName() == "GDC"
END FUNCTION