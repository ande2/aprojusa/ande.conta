#+ A string to reference an individual cell
FUNCTION excel_cell(col, row, property)
 DEFINE col STRING
 DEFINE row INTEGER
 DEFINE property STRING
 DEFINE result STRING

 LET result = SFMT('activesheet.Range("%1%2").%3', col, row USING "<<<<", property)
 RETURN result
END FUNCTION

#+ A string to return a range of columns
FUNCTION excel_column(col1, col2, property)
 DEFINE col1, col2 STRING
 DEFINE property STRING
 DEFINE result STRING

 LET result = SFMT('activesheet.Columns("%1:%2").%3', col1, col2, property)
 RETURN result
END FUNCTION

#+ A string to return a range of rows
FUNCTION excel_row(row1, row2, property)
 DEFINE row1, row2 INTEGER
 DEFINE property STRING

 RETURN SFMT('activesheet.Rows("%1:%2").%3', 
             row1 USING "<<<<", 
             row2 USING "<<<<", 
             property)
END FUNCTION

#+ A string to return a range of cells
FUNCTION excel_range(column1, row1, column2, row2, property)
 DEFINE column1, column2 STRING
 DEFINE row1, row2 INTEGER
 DEFINE property STRING

 RETURN SFMT('activesheet.Range("%1%2:%3%4").%5', 
              column1, 
              row1 USING "<<<<", 
              column2, 
              row2 USING "<<<<", 
              property)
END FUNCTION

#+ Call an Excel method
FUNCTION excel_call_method(xlapp, xlwb, method)
 DEFINE xlapp INTEGER
 DEFINE xlwb INTEGER
 DEFINE method STRING
 DEFINE result STRING

 CALL ui.interface.frontCall("WinCOM", "CallMethod", [xlapp, method], [result])
 CALL CheckError(xlapp, xlwb, result)
END FUNCTION

#+ Set the property of an Excel selection
FUNCTION excel_set_property(xlapp, xlwb, selection, value)
 DEFINE xlapp INTEGER
 DEFINE xlwb INTEGER
 DEFINE selection STRING
 DEFINE value STRING
 DEFINE result STRING

 CALL ui.Interface.frontCall("WinCOM", 
                             "SetProperty", 
                             [xlwb, selection, value],[result])   
 CALL CheckError(xlapp, xlwb, result)
END FUNCTION

#+ Set the property of an Excel selection when the value is an integer
FUNCTION excel_set_property_integer(xlapp, xlwb, selection, value)
 DEFINE xlapp INTEGER
 DEFINE xlwb INTEGER
 DEFINE selection STRING
 DEFINE value INTEGER
 DEFINE result STRING

 CALL ui.Interface.frontCall("WinCOM", "SetProperty", [xlwb, selection, value],[result])
 CALL CheckError(xlapp, xlwb, result)
END FUNCTION

#+ Read the value of a particular cell
FUNCTION excel_get_cellvalue(xlapp, xlwb, col, row)
 DEFINE xlapp, xlwb INTEGER
 DEFINE col STRING
 DEFINE row INTEGER
 DEFINE result STRING

 CALL ui.Interface.frontCall("WinCOM", 
                             "GetProperty", 
                             [xlwb, 
                             SFMT('activesheet.Range("%1%2").Value',col, row)], [result])
 CALL CheckError(xlapp, xlwb, result)
 RETURN result
END FUNCTION

#+ Free Memory at the end
FUNCTION freeMemory(xlapp, xlwb)
 DEFINE xlapp INTEGER
 DEFINE xlwb INTEGER
 DEFINE result INTEGER

 IF xlwb != -1 THEN
    CALL ui.Interface.frontCall("WinCOM","ReleaseInstance", [xlwb], [result] )
 END IF
 IF xlapp != -1 THEN
    CALL ui.Interface.frontCall("WinCOM","ReleaseInstance", [xlapp], [result] )
 END IF
END FUNCTION

#+ Handle errors
FUNCTION checkError(xlapp, xlwb, result)
 DEFINE xlapp INTEGER
 DEFINE xlwb INTEGER
 DEFINE result INTEGER
 DEFINE error_text STRING

 IF result = -1 THEN
    DISPLAY "COM Error "
    DISPLAY base.Application.getstacktrace()
    CALL ui.Interface.frontCall("WinCOM","GetError",[],[error_text])
    DISPLAY error_text
    --let's release the memory on the GDC side
    CALL freeMemory(xlapp, xlwb)
    DISPLAY "Exit with COM Error."
    EXIT PROGRAM (-1)
 END IF
END FUNCTION

-- Subrutina para exportar el reporte a excel usando WinCOM 

FUNCTION librut005_excel(filename) 
 DEFINE sb          base.StringBuffer
 DEFINE ch          base.channel
 DEFINE result      INTEGER
 DEFINE i           INTEGER
 DEFINE xlapp       INTEGER
 DEFINE xlwb        INTEGER
 DEFINE linea       STRING 
 DEFINE filename    STRING 
 DEFINE separador   CHAR(1) 
 DEFINE nuevalinea  CHAR(1) 

 -- Inicializando variables                            
 LET xlapp = -1
 LET xlwb = -1

 -- Creando la instancia de Excel
 CALL ui.Interface.frontCall("WinCOM", "CreateInstance", ['Excel.Application'], [xlapp])
 CALL CheckError(xlapp, xlwb, xlapp)
   
 -- Agregando un libro al documento de excel               
 CALL ui.interface.frontCall("WinCOM", "CallMethod", [xlapp, 'WorkBooks.Add'], [xlwb])
 CALL CheckError(xlapp, xlwb, xlwb)
   
 -- Haciendo visible el libro                 
 CALL excel_set_property(xlapp, xlapp, "Visible","True")
  
 -- Llenando celdas desde el FrontCall 
 -- Construyendo celdas delimitando columnas por TAB (ASCII(9)), 
 -- y filas por CR (ASCII(10)
 -- Copia al clipboard via el Frontcall y luego instruye a excel para pegar el contenido
 -- de la celdas dentro del libro 
 LET sb = base.StringBuffer.create()

 -- Leyendo el archivo que contiene el reporte y copiandolo al clipboard 
 LET ch = base.channel.create()
 call ch.openfile(filename,"r")

 let i = 1 
 let separador  = ascii(9) 
 let nuevalinea = ascii(10)
 while true
  let linea = ch.readLine() 
  if ch.isEof() then exit while end if
  if linea.getindexof(separador,1)>0 then
     CALL sb.append(linea)
     CALL sb.append(nuevalinea)
  end if 
  let i = i+1
 end while
 call ch.close() 

 -- Pegando el contenido de las celdas en el clipboard al libro de excel
 CALL ui.Interface.FrontCall("standard","cbset",sb.toString(),result)
 CALL excel_call_method(xlapp, xlwb, 'ActiveSheet.Paste')
 CALL excel_call_method(xlapp, xlwb, 'Columns("A:A").EntireColumn.AutoFit')
 CALL excel_set_property(xlapp, xlwb, excel_column("B","Z","NumberFormat"), "0.00")

 -- Liberando informacion del clipboard    
 CALL freeMemory(xlapp, xlwb)
END FUNCTION
