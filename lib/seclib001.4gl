{
Programa : seclib001
Programo : Mynor Ramirez 
Objetivo : Programa de subrutinas de seguridad.
}

DATABASE erpjuridico 

{ Subrutina para ingreso de usuario y password de seguridad }

FUNCTION seclib001_passwd(wcodpro,wprogid,wuserid) 
 DEFINE wcodpro          LIKE glb_permxusr.codpro,
        wprogid          LIKE glb_permxusr.progid, 
        wuserid          LIKE glb_permxusr.userid, 
        passwd,username  CHAR(20),
        loop,veces       SMALLINT,
        result           SMALLINT

 -- Desplegando pantalla de seguridad
 OPEN WINDOW wsecurity AT 9,27
  WITH FORM "formpass"

   -- Ingresando el usuario y el password
   LET veces = 0
   LET loop = TRUE
   WHILE loop
    -- Inicializando variables
    CLEAR FORM
    INITIALIZE username,passwd TO NULL

    INPUT BY NAME username,passwd
     ATTRIBUTE(UNBUFFERED)

     ON ACTION cancel 
      -- Salida
      LET loop   = FALSE
      LET result = FALSE
      EXIT INPUT

     BEFORE FIELD username
      IF (LENGTH(wuserid)>0) THEN
         LET username = wuserid 
         DISPLAY BY NAME username
         NEXT FIELD passwd
      END IF

     AFTER FIELD username
      -- Verificando ingreso de usuario
      IF (LENGTH(username)<=0) THEN
         ERROR "Usuario invalido ..."
         NEXT FIELD username
      END IF

      NEXT FIELD passwd
     AFTER FIELD passwd
      -- Verificando ingreso de password
      IF (FGL_LASTKEY()  = FGL_KEYVAL("up")) OR
         (FGL_LASTKEY()  = FGL_KEYVAL("left")) OR
         (LENGTH(passwd) <=0) THEN
         LET passwd = NULL
         NEXT FIELD passwd
      END IF

      -- Verificando numero de intentos de ingresar el password
      IF (veces=5) THEN
         ERROR "Demasiados intentos ..."
         LET passwd = NULL
         CLEAR passwd
         NEXT FIELD passwd
      END IF

      -- Verificando password
      IF seclib001_chkpasswd(username,passwd,wcodpro,wprogid) THEN
         LET result = TRUE
         LET loop   = FALSE
         EXIT INPUT
      ELSE
         -- Verificando acceso denegado
         LET veces = (veces+1)
         ERROR "Password incorrecto ..."
         LET passwd = NULL
         CLEAR passwd
         NEXT FIELD passwd
      END IF
    END INPUT
  END WHILE
 CLOSE WINDOW wsecurity
 RETURN result,username
END FUNCTION

-- Subrutina para validar el acceso a un programa por medio del usuario y password 

FUNCTION seclib001_chkpasswd(wuserid,wpasswd,wcodpro,wprogid)
 DEFINE wuserid LIKE glb_permxusr.userid,
        wpasswd LIKE glb_permxusr.passwd,
        wcodpro LIKE glb_permxusr.codpro,
        wprogid LIKE glb_permxusr.progid,
        cnt     SMALLINT

 -- Seleccionando rol del usuario
 SELECT COUNT(*) 
  INTO  cnt
  FROM  glb_permxusr a
  WHERE a.codpro = wcodpro
    AND a.progid = wprogid
    AND a.userid = wuserid
    AND a.activo = 1 
    AND a.passwd = wpasswd
  IF (cnt>0) THEN
     RETURN TRUE 
  ELSE
     RETURN FALSE
  END IF 
END FUNCTION 

-- Subrutina para validar el acceso a una opcion de un programa por medio del usuario

FUNCTION seclib001_accesos(wcodpro,wprogid,wuserid)
 DEFINE wuserid LIKE glb_permxusr.userid,
        wcodpro LIKE glb_permxusr.codpro,
        wprogid LIKE glb_permxusr.progid,
        cnt     SMALLINT

 -- Verificando accesos 
 SELECT COUNT(*)
  INTO  cnt
  FROM  glb_permxusr a
  WHERE (a.codpro = wcodpro OR a.codpro = 'manager')
    AND (a.progid = wprogid OR a.progid = 0)
    AND a.userid = wuserid
    AND a.activo = 1
  IF (cnt>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF
END FUNCTION

-- Subrutina para validar el acceso a las opciones del menu principal 

FUNCTION seclib001_AccesosMenu(wcodpro,wuserid)
 DEFINE wuserid LIKE glb_permxusr.userid,
        wcodpro LIKE glb_permxusr.codpro,
        cnt     SMALLINT

 -- Verificando accesos 
 SELECT COUNT(*)
  INTO  cnt
  FROM  glb_permxusr a
  WHERE (a.codpro = wcodpro OR a.codpro = 'manager')
    AND a.userid = wuserid
    AND a.activo = 1
  IF (cnt>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF
END FUNCTION
