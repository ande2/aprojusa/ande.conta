{
librut002.4gl
Mynor Ramirez
Subrutinas de libreria 
} 

DATABASE erpjuridico 

-- Subrutina excelreport
-- Parametros  :  qry      << string de busqueda
--                cols     << numero de columnas en excel 
--                im       << modo 0: interactivo, 1: no interactivo
--                lbls     << emcabezados de columnas
-- Regresa                 >> Number of lines processed
--
-- Comentarios : Envia un reporte a una hoja de excel           

FUNCTION librut002_excelreport(title1,qry,cols,im,lbls)
 -- Etiquetas
 CONSTANT resWindow   = "Reporte Generado"
 CONSTANT resLabel    = "Total de registros: "
 CONSTANT resButton   = "Aceptar"

 -- Variables
 DEFINE lbls          DYNAMIC ARRAY OF VARCHAR(255)
 DEFINE rc            DYNAMIC ARRAY OF VARCHAR(255)
 DEFINE sb            base.StringBuffer
 DEFINE ch            base.channel
 DEFINE tmp           VARCHAR(10)
 DEFINE pp            DEC(14,6)
 DEFINE w             ui.Window
 DEFINE f             ui.Form
 DEFINE pb            SMALLINT
 DEFINE i,cols,im,x   SMALLINT
 DEFINE result        INTEGER
 DEFINE xlapp         INTEGER
 DEFINE xlwb          INTEGER
 DEFINE total         INTEGER
 DEFINE linea         STRING
 DEFINE title1        STRING
 DEFINE registro      STRING
 DEFINE encabezado    STRING
 DEFINE qry,ln,cnt    STRING
 DEFINE separador     CHAR(1)
 DEFINE nuevalinea    CHAR(1)
 DEFINE s             CHAR(1)

 -- Verificando si el modo es interactivo
 IF im THEN
  -- Abriendo ventana para mostrar avance
  OPEN WINDOW progressBar AT 1,1 
   WITH FORM "progrebar" ATTRIBUTES(TEXT=title1)

   -- Obteniendo datos de la ventana
   LET w = ui.Window.getcurrent()
   LET f = w.getForm()

   -- Desplegando mensaje
   CALL f.setElementText("formonly.mensaje","Generando Reporte a EXCEL") 

   -- Inicializando ventana de progreso 
   CALL librut002_inicioprogreso()
 END IF 

 -- Contando registros del query 
 LET total = 1
 PREPARE prpPb FROM qry
 DECLARE enc CURSOR FOR prpPb
 FOREACH enc INTO rc[01],rc[02],rc[03],rc[04],rc[05],rc[06],rc[07],rc[08],rc[09],rc[10],
                  rc[11],rc[12],rc[13],rc[14],rc[15],rc[16],rc[17],rc[18],rc[19],rc[20],
                  rc[21],rc[22],rc[23],rc[24],rc[25],rc[26],rc[27],rc[28],rc[29],rc[30],
                  rc[31],rc[32],rc[33],rc[34],rc[35],rc[36],rc[37],rc[38],rc[39],rc[40],
                  rc[41],rc[42],rc[43],rc[44],rc[45],rc[46],rc[47],rc[48],rc[49],rc[50],
                  rc[51],rc[52],rc[53],rc[54],rc[55],rc[56],rc[57],rc[58],rc[59],rc[60]
  LET total = total+1
 END FOREACH
 CLOSE enc 
 FREE  enc
 LET total = total-1

 -- Inicializando variables
 LET xlapp      = -1
 LET xlwb       = -1
 LET s          = ASCII(9)
 lET nuevalinea = ASCII(10) 

 -- Creando la instancia de Excel
 CALL ui.Interface.frontCall("WinCOM", "CreateInstance", ['Excel.Application'], [xlapp])
 CALL CheckError(xlapp, xlwb, xlapp)

 -- Agregando un libro al documento de excel
 CALL ui.interface.frontCall("WinCOM", "CallMethod", [xlapp, 'WorkBooks.Add'], [xlwb])
 CALL CheckError(xlapp, xlwb, xlwb)

 -- Haciendo visible el libro
 CALL excel_set_property(xlapp, xlapp, "Visible","True")

 -- Llenando celdas desde el FrontCall
 -- Construyendo celdas delimitando columnas por TAB (ASCII(9)),
 -- y filas por CR (ASCII(10)
 -- Copia al clipboard via el Frontcall y luego instruye a excel para pegar el contenido
 -- de la celdas dentro del libro
 LET sb = base.StringBuffer.create()

 -- Titulo del reporte
 -- Llenando buffer
 CALL sb.append(title1)
 CALL sb.append(nuevalinea)
 CALL sb.append(nuevalinea)

 -- Llegando celdas con encabezados
 IF lbls.getlength()>0 THEN
    LET encabezado = NULL
    FOR x = 1 TO cols
     -- Creando encabezado 
     LET encabezado = encabezado CLIPPED,lbls[x] CLIPPED,s 
    END FOR

    -- Llenando buffer
    CALL sb.append(encabezado)
    CALL sb.append(nuevalinea)
 END IF

 -- Llegnando celdas con registros 
 -- Preparando query
 PREPARE prp FROM qry
 DECLARE cur CURSOR FOR prp
 CALL rc.clear() 
 LET i = 1 
 FOREACH cur INTO rc[01],rc[02],rc[03],rc[04],rc[05],rc[06],rc[07],rc[08],rc[09],rc[10],
                  rc[11],rc[12],rc[13],rc[14],rc[15],rc[16],rc[17],rc[18],rc[19],rc[20],
                  rc[21],rc[22],rc[23],rc[24],rc[25],rc[26],rc[27],rc[28],rc[29],rc[30],
                  rc[31],rc[32],rc[33],rc[34],rc[35],rc[36],rc[37],rc[38],rc[39],rc[40],
                  rc[41],rc[42],rc[43],rc[44],rc[45],rc[46],rc[47],rc[48],rc[49],rc[50],
                  rc[51],rc[52],rc[53],rc[54],rc[55],rc[56],rc[57],rc[58],rc[59],rc[60]

  -- Creando registros
  LET registro = NULL
  FOR x = 1 TO cols
   LET registro = registro CLIPPED,rc[x] CLIPPED,s
  END FOR 

  -- Llenando buffer
  CALL sb.append(registro)
  CALL sb.append(nuevalinea)

  -- Si es modo interactivo
  IF im THEN
     -- Calculando progreso de avance          
     LET pp  = (i*100)/total
     LET pb  = pp
     LET tmp = pb
     LET tmp = tmp CLIPPED,"%"
     DISPLAY BY NAME pb,tmp
     CALL ui.Interface.refresh()
  END IF

  LET i = i+1
 END FOREACH
 CLOSE cur
 FREE  cur 
 LET i = i-1

 -- Total de registros
 -- Llenando buffer
 CALL sb.append(nuevalinea)
 CALL sb.append(reslabel||s||total) 
 CALL sb.append(nuevalinea)

 -- Pegando el contenido de las celdas en el clipboard al libro de excel
 CALL ui.Interface.FrontCall("standard","cbset",sb.toString(),result)
 CALL excel_call_method(xlapp, xlwb, 'ActiveSheet.Paste')
 CALL excel_call_method(xlapp, xlwb, 'Columns("A:Z").EntireColumn.AutoFit')

 -- Liberando informacion del clipboard
 CALL freeMemory(xlapp, xlwb)

 -- Verificando si es modo interactivo
 IF im THEN
    -- Desplegando registros encontrados
    CALL f.setElementText("formonly.mensaje",resWindow) 
    CALL f.setElementText("formonly.registros",resLabel||i)

    MENU resWindow 
     COMMAND resButton
      EXIT MENU
    END MENU
    CLOSE WINDOW progressBar
 END IF

 RETURN i
END FUNCTION

-- Subrutina para limpiar la ventana de progreso

FUNCTION librut002_inicioprogreso()
 DEFINE screenToBeCleaned  om.DomNode
 DEFINE doc                om.DomDocument
 DEFINE l                  om.NodeList
 DEFINE att                STRING

 CONSTANT nodeName = "Window"
 CONSTANT nodeAtt  = "screen"

 LET doc               = ui.Interface.getDocument()
 LET screenToBeCleaned = doc.getDocumentElement()
 LET l                 = screenToBeCleaned.selectByTagName(nodeName)
 LET screenToBeCleaned = l.item(1)
 LET att               = screenToBeCleaned.getAttribute("name")

 IF (att=nodeAtt) THEN
    CALL doc.removeElement(screenToBeCleaned)
 END IF
END FUNCTION

-- Subrutina para crear combobox dinamicos 

FUNCTION librut002_combobox(vl_2_campo,vl_2_query)
 DEFINE vl_2_lista_nodb base.StringTokenizer
 DEFINE vl_2_combo      ui.ComboBox
 DEFINE vl_2_campo      STRING
 DEFINE vl_2_query      STRING
 DEFINE vl_2_metodo     STRING
 DEFINE vl_2_valor      STRING
 DEFINE vl_2_valor2     STRING
 DEFINE vl_2_lista      CHAR(500)
 DEFINE vl_2_lista2     CHAR(500)

 -- Quitando espacioes en blanco
 LET vl_2_campo = vl_2_campo.trim()
 LET vl_2_query = vl_2_query.trim()

 -- Obtiene el nodo del metodo combo de la clase ui
 LET vl_2_combo = ui.ComboBox.forName(vl_2_campo)
 IF vl_2_combo IS NULL THEN
    RETURN
 END IF

 -- Inicializa el combo
 CALL vl_2_combo.clear()

 -- Convirtiendo todo a minusculas
 LET vl_2_metodo = vl_2_query.toUpperCase()

 -- Verificando metodo
 IF vl_2_metodo matches "SELECT*" THEN
    --Preparando el query
    PREPARE q_comboList2 from vl_2_query
    DECLARE c_comboList2 cursor FOR q_comboList2

    -- Obteniendo la lista de valores de la tabla segun el query enviado
    FOREACH c_comboList2 INTO vl_2_lista,vl_2_lista2
     LET vl_2_valor  = vl_2_lista  CLIPPED
     LET vl_2_valor2 = vl_2_lista2 CLIPPED
     CALL vl_2_combo.addItem(vl_2_valor,vl_2_valor2)
    END FOREACH
 ELSE
    -- Obteniendo la lista de valores que no son de base de datos
    LET vl_2_lista_nodb = base.StringTokenizer.create(vl_2_query,"|")

    WHILE vl_2_lista_nodb.hasMoreTokens()
     LET  vl_2_valor = vl_2_lista_nodb.nextToken()
     CALL vl_2_combo.addItem(vl_2_valor,vl_2_valor)
    END WHILE
 END IF

 -- Verificanod is hay datos
 IF (vl_2_combo.getItemCount()=0) THEN 
    ERROR " Atencion: no existe datos registrados. (VERIFICA listas de datos)"
 END IF 
END FUNCTION

-- Subrutina para seleccionar datos de una lista 

FUNCTION librut002_picklist(Title0,Title1,Title2,Field1,Field2,Tabname,Condicion,OrderNum,opci)
 DEFINE Title0       STRING,
	Title1,  
	Title2       STRING,  
	Field1, 
	Field2,
	Condicion    STRING,
	Tabname      STRING,
	OrderNum     STRING, 
        retorna      RECORD  
         codigo      VARCHAR(20),
         descripcion VARCHAR(100)
	END RECORD,
	la_busqueda  DYNAMIC ARRAY OF RECORD 	
         codigo      VARCHAR(20),
         descripcion VARCHAR(100)
        END RECORD,
        i,aborta     SMALLINT,
        w            ui.Window,
        f            ui.Form,
        comia        CHAR(2),
        sqlstmnt     STRING,
        refe         VARCHAR(100),
        buscar       VARCHAR(105),
        opci         SMALLINT, 
        scr_cnt      SMALLINT

 -- Definiendo tecla de aceptar
 OPTIONS ACCEPT KEY escape

 LET int_flag = 0  
 LET comia = ASCII 34

 -- Abriendo la ventana                                                                
 OPEN WINDOW find_ayuda 
  WITH FORM "picklist"

  -- Obteniendo datos de la ventana
  LET w = ui.Window.getcurrent()
  LET f = w.getForm()

  -- Desplegandp titulso
  CALL f.setElementText("filtro",title0)
  CALL f.setElementText("formonly.codigo",title1)
  CALL f.setElementText("formonly.descripcion",title2)
  CALL fgl_settitle("Buaqueda Dinamica")

  -- Inicializando datos
  CALL la_busqueda.CLEAR()
  LET aborta = 0  

  -- Buscando datos 
  WHILE aborta = 0
   INITIALIZE retorna.* TO NULL 
   LET refe = "Ingrese informacion a buscar ..."
   LET int_flag = FALSE  
   LET buscar = refe 
   LET i = 1

   -- Ingresando datos a buscar
   INPUT BY NAME refe WITHOUT DEFAULTS 
    BEFORE INPUT
     -- Desplegando teclas
     CALL fgl_dialog_setkeylabel("ACCEPT","Detalle")
     CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")

    BEFORE FIELD refe
     -- Si no hay ingreso de datos a buscar 
     IF opci=0 THEN
       -- Creando busqueda
       LET sqlstmnt = " SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum

       -- Preparando busqueda
       PREPARE ex_sqlst FROM sqlstmnt

       -- Seleccionando datos
       DECLARE estos0 CURSOR FOR ex_sqlst
       CALL la_busqueda.CLEAR()
       LET i =1
       FOREACH estos0 INTO la_busqueda[i].*
        LET i=i+1
       END FOREACH
       FREE estos0
       FREE ex_sqlst

      -- Desplegando numero de datos seleccionados
      CALL f.setElementText("registros",'Registros encontrados '||(i-1))

      -- Desplegnando datos
      DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
       BEFORE ROW
        EXIT DISPLAY
      END DISPLAY

      LET buscar = refe
      EXIT INPUT
     END IF

    AFTER FIELD refe
     -- Verificando rotulo
     IF refe = "Ingrese informacion a buscar ..." THEN
        LET refe = "*"
     END IF
     LET buscar = "*"||refe||"*"

     -- Si se ingresan datos a buscar
     IF opci=1 THEN 
        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
                        " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                        " AND upper(", field2 CLIPPED, ") MATCHES upper(",comia CLIPPED, buscar CLIPPED, comia CLIPPED, ")",
                        " ORDER BY ",OrderNum
     ELSE
        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum
     END IF

     -- Preparando busqueda
     PREPARE ex_sqlst1 FROM sqlstmnt

     -- Seleccionando datos
     DECLARE estos1 CURSOR FOR ex_sqlst1
     CALL la_busqueda.CLEAR()
     LET i =1
     FOREACH estos1 INTO la_busqueda[i].*
      LET i=i+1
     END FOREACH
     FREE estos1
     FREE ex_sqlst1

     -- Desplegando numero de datos seleccionados
     CALL f.setElementText("registros",'Registros encontrados '||(i-1))

     -- Desplegnando datos
     DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
      BEFORE ROW
       EXIT DISPLAY
     END DISPLAY
     LET buscar = refe
     EXIT INPUT

    ON IDLE 1
     -- Leyendo datos del buffer cada segundo 
     LET refe = GET_FLDBUF(refe)
     IF LENGTH(refe)=0 THEN
        LET refe = ""
     END IF

     IF buscar<>refe OR 
        opci=0 THEN
        LET buscar = "*"||refe||"*"

      -- Si se ingresan datos a buscar
      IF opci=1 THEN 
        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
                        " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                        " AND upper(", field2 CLIPPED, ") MATCHES upper(",comia CLIPPED, buscar CLIPPED, comia CLIPPED, ")",
                        " ORDER BY ",OrderNum
      ELSE
        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum
      END IF

      -- Preparando busqueda
      PREPARE ex_sqlst2 FROM sqlstmnt

      -- Seleccionando datos
      DECLARE estos2 CURSOR FOR ex_sqlst2
      CALL la_busqueda.CLEAR()
      LET i =1
      FOREACH estos2 INTO la_busqueda[i].*
       LET i=i+1
      END FOREACH
      FREE estos2
      FREE ex_sqlst2

      -- Desplegando numero de datos seleccionados
      CALL f.setElementText("registros",'Registros encontrados '||(i-1))
 
      -- Desplegnando datos
      LET aborta = 0  
      DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
       BEFORE ROW
        EXIT DISPLAY
      END DISPLAY
      LET buscar = refe

      -- Si no se pedian datos de busqueda
      IF opci=0 THEN  
         EXIT INPUT
      END IF
     END IF 

    AFTER INPUT
     IF int_flag THEN
        LET aborta = 1
        EXIT INPUT
     END IF
   END INPUT
   IF aborta THEN
      LET int_flag = TRUE
      EXIT WHILE
   END IF

   -- Definiendo tecla de aceptacion
   OPTIONS ACCEPT KEY RETURN

   -- Desplegnado datos
   CALL f.setElementText("registros",'Registros encontrados '||(i-1)|| "    Presione [ENTER] para seleccionar ")

   DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
    BEFORE DISPLAY
     -- Desplegando teclas
     CALL fgl_dialog_setkeylabel("ACCEPT","Seleccionar\nRegistro")

     -- Si opcion es ingresar datos
     IF opci=1 THEN
        CALL fgl_dialog_setkeylabel("INTERRUPT","Nueva busqueda")
     ELSE
        CALL fgl_dialog_setkeylabel("INTERRUPT","")
     END IF

     -- Si no se ecnontraron datos
     IF i=1 THEN
        --CALL box_valdato("No se encontraron datos.")
        display "NO HAY DATOS" 
     END IF
  
    BEFORE ROW
     LET i       = ARR_CURR()
     LET scr_cnt = SCR_LINE()
     LET retorna.* = la_busqueda[i].*
     IF LENGTH(la_busqueda[i].codigo)=0 THEN
        CALL fgl_dialog_setcurrline(scr_cnt,i-1)
     END IF

    ON KEY(RETURN)
     LET aborta = 1
     EXIT DISPLAY

    ON KEY(INTERRUPT)
     IF opci=1 THEN
        LET aborta = 0
        EXIT DISPLAY
     END IF

    ON KEY(ACCEPT)
     LET aborta = 1
     EXIT DISPLAY

    AFTER DISPLAY
     CONTINUE DISPLAY
   END DISPLAY
   IF aborta=1 THEN
      EXIT WHILE
   END IF
  END WHILE
 CLOSE WINDOW find_ayuda

 -- Definiendo tecla de aceptacion
 OPTIONS ACCEPT KEY ESCAPE

 RETURN retorna.*,int_flag 
END FUNCTION

-- Subrutina para seleccionar datos de una lista 

FUNCTION librut002_formlist(Title0,Title1,Title2,Title3,Field1,Field2,Field3,Tabname,Condicion,OrderNum,opci,Tpf)
 DEFINE Title0       STRING,
	Title1,  
	Title2,
        Title3        STRING,  
	Field1, 
	Field2,
	Field3,
	Condicion    STRING,
	Tabname      STRING,
	OrderNum     STRING, 
        retorna      RECORD  
         codigo      VARCHAR(20),
         descripcion VARCHAR(100)
	END RECORD,
	la_busqueda  DYNAMIC ARRAY OF RECORD 	
         codigo      VARCHAR(20),
         descripcion VARCHAR(100),
         precio      DEC(12,2)
        END RECORD,
        i,aborta     SMALLINT,
        w            ui.Window,
        f            ui.Form,
        sqlstmnt     STRING,
        strfil       STRING,
        refe         VARCHAR(100),
        buscar       VARCHAR(105),
        tipofiltro   SMALLINT, 
        opci,tpf     SMALLINT, 
        scr_cnt      SMALLINT

 -- Cargando acciones default
 CALL ui.Interface.loadActionDefaults("../std/formlist")

 -- Definiendo tecla de aceptar
 OPTIONS ACCEPT KEY escape

 LET int_flag = 0  
 LET tipofiltro = 0 

 -- Abriendo la ventana                                                                
 OPEN WINDOW find_ayuda 
  WITH FORM "formlist"

  -- Obteniendo datos de la ventana
  LET w = ui.Window.getcurrent()
  LET f = w.getForm()

  -- Desplegandp titulso
  CALL fgl_settitle(title0)
  CALL f.setElementText("formonly.codigo",title1)
  CALL f.setElementText("formonly.descripcion",title2)
  CALL f.setElementText("formonly.precio",title3)

  -- Inicializando datos
  CALL la_busqueda.clear()
  LET aborta = 0  

  -- Buscando datos 
  WHILE aborta = 0
   INITIALIZE retorna.* TO NULL 
   DISPLAY BY NAME tipofiltro 
   LET refe = "Ingrese informacion a buscar ..."
   LET int_flag = FALSE  
   LET buscar = refe 
   LET i = 1

   -- Ingresando datos a buscar
   INPUT BY NAME refe WITHOUT DEFAULTS 
    ON ACTION codigo
     -- Seleccionanado filtro por codigo
     LET tipofiltro = 1 
     DISPLAY BY NAME tipofiltro 

    ON ACTION descrip 
     -- Seleccionanado filtro por descripcion 
     LET tipofiltro = 0 
     DISPLAY BY NAME tipofiltro 

    BEFORE INPUT
     -- Desabilitando tipo de filtro cuando seleccion no aplica 
     IF NOT tpf THEN
        CALL Dialog.SetActionHidden("codigo",1) 
        CALL Dialog.SetActionHidden("descrip",1) 
     END IF 

    BEFORE FIELD refe
     -- Si no hay ingreso de datos a buscar 
     IF opci=0 THEN
       -- Creando busqueda
       LET sqlstmnt =  "SELECT ",Field1 CLIPPED,",",Field2 CLIPPED,",",Field3 CLIPPED,
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum

       -- Preparando busqueda
       PREPARE ex2_sqlst FROM sqlstmnt

       -- Seleccionando datos
       DECLARE westos0 CURSOR FOR ex2_sqlst
       CALL la_busqueda.CLEAR()
       LET i =1
       FOREACH westos0 INTO la_busqueda[i].*
        LET i=i+1
       END FOREACH
       FREE westos0
       FREE ex2_sqlst

      -- Desplegando numero de datos seleccionados
      CALL f.setElementText("registros",'Detalle de Registros Encontrados '||(i-1))

      -- Desplegnando datos
      DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
       BEFORE ROW
        EXIT DISPLAY
      END DISPLAY

      LET buscar = refe
      EXIT INPUT
     END IF

    AFTER FIELD refe
     -- Verificando rotulo
     LET refe = refe CLIPPED
     IF (refe="Ingrese informacion a buscar ...") OR
        (LENGTH(refe)=0) THEN
        LET refe = "*"
     END IF
     LET buscar = "*"||refe||"*"

     -- Si se ingresan datos a buscar
     IF opci=1 THEN 
        -- Verificando fitro
        CASE (tipofiltro)
         WHEN 0 LET strfil = " AND UPPER(",field2 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"
         WHEN 1 LET strfil = " AND UPPER(",field1 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"
        END CASE

        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED,",",Field2 CLIPPED,",",Field3 CLIPPED,
                        " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                        strfil CLIPPED,
                        " ORDER BY ",OrderNum
     ELSE
        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED,",",Field2 CLIPPED,",",Field3 CLIPPED,
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum
     END IF

     -- Preparando busqueda
     PREPARE ex0_sqlst1 FROM sqlstmnt

     -- Seleccionando datos
     DECLARE yestos1 CURSOR FOR ex0_sqlst1
     CALL la_busqueda.CLEAR()
     LET i =1
     FOREACH yestos1 INTO la_busqueda[i].*
      LET i=i+1
     END FOREACH
     FREE yestos1
     FREE ex0_sqlst1

     -- Desplegando numero de datos seleccionados
     CALL f.setElementText("registros",'Detalle de Registros encontrados '||(i-1))

     -- Desplegnando datos
     DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
      BEFORE ROW
       EXIT DISPLAY
     END DISPLAY
     LET buscar = refe
     EXIT INPUT

    ON IDLE 1
     -- Leyendo datos del buffer cada segundo 
     LET refe = GET_FLDBUF(refe)
     IF LENGTH(refe)=0 THEN
        LET refe = ""
     END IF

     IF buscar<>refe OR 
        opci=0 THEN
        LET buscar = "*"||refe||"*"

      -- Si se ingresan datos a buscar
      IF opci=1 THEN 
        -- Verificando fitro
        CASE (tipofiltro)
         WHEN 0 LET strfil = " AND UPPER(",field2 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"
         WHEN 1 LET strfil = " AND UPPER(",field1 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"
        END CASE

        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED,",",Field2 CLIPPED,",",Field3 CLIPPED,
                        " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                        strfil CLIPPED,
                        " ORDER BY ",OrderNum
      ELSE
        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED,",",Field2 CLIPPED,",",Field3 CLIPPED,
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum
      END IF

      -- Preparando busqueda
      PREPARE ex1_sqlst2 FROM sqlstmnt

      -- Seleccionando datos
      DECLARE xestos2 CURSOR FOR ex1_sqlst2
      CALL la_busqueda.CLEAR()
      LET i =1
      FOREACH xestos2 INTO la_busqueda[i].*
       LET i=i+1
      END FOREACH
      FREE xestos2
      FREE ex1_sqlst2

      -- Desplegando numero de datos seleccionados
      CALL f.setElementText("registros",'Detalle de Registros Encontrados '||(i-1))
 
      -- Desplegnando datos
      LET aborta = 0  
      DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
       BEFORE ROW
        EXIT DISPLAY
      END DISPLAY
      LET buscar = refe

      -- Si no se pedian datos de busqueda
      IF opci=0 THEN  
         EXIT INPUT
      END IF
     END IF 

    AFTER INPUT
     IF int_flag THEN
        LET aborta = 1
        EXIT INPUT
     END IF
   END INPUT
   IF aborta THEN
      LET int_flag = TRUE
      EXIT WHILE
   END IF

   -- Definiendo tecla de aceptacion
   OPTIONS ACCEPT KEY RETURN

   -- Desplegnado datos
   CALL f.setElementText("registros","Detalle de Registros Encontrados "||(i-1)) 
   MESSAGE "Presione [ENTER] para seleccionar"

   DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
    ATTRIBUTE(ACCEPT=FALSE) 
    BEFORE DISPLAY
     -- Desabilitando botom
     IF (opci=0) THEN
        CALL Dialog.SetActionHidden("consulta",1) 
     ELSE 
        CALL Dialog.SetActionHidden("cancel",1) 
     END IF  

     -- Si no se ecnontraron datos
     IF i=1 THEN
        MESSAGE "Atencion: no existen datos registrados." 
     END IF
  
    BEFORE ROW
     LET i       = ARR_CURR()
     LET scr_cnt = SCR_LINE()
     LET retorna.codigo      = la_busqueda[i].codigo
     LET retorna.descripcion = la_busqueda[i].descripcion
     IF LENGTH(la_busqueda[i].codigo)=0 THEN
        CALL fgl_dialog_setcurrline(scr_cnt,i-1)
     END IF

    ON ACTION consulta
     IF opci=1 THEN
        LET aborta = 0
        EXIT DISPLAY
     END IF

    ON ACTION seleccion
     LET aborta = 1
     EXIT DISPLAY

    ON ACTION cancel
     LET aborta = 1
     EXIT DISPLAY 

    ON KEY(RETURN)
     LET aborta = 1
     EXIT DISPLAY

    AFTER DISPLAY
     CONTINUE DISPLAY
   END DISPLAY
   MESSAGE "" 

   IF aborta=1 THEN
      EXIT WHILE
   END IF
  END WHILE
 CLOSE WINDOW find_ayuda

 -- Definiendo tecla de aceptacion
 OPTIONS ACCEPT KEY ESCAPE

 -- Inicializando datos
 CALL la_busqueda.clear()

 -- Cargando acciones default
 CALL ui.Interface.loadActionDefaults("../std/actiondefaults")

 RETURN retorna.*,int_flag 
END FUNCTION

-- Subrutina para seleccionar datos de una lista 

FUNCTION librut002_formlistcli(Title0,Title1,Title2,Title3,Field1,Field2,Field3,Tabname,Condicion,OrderNum,opci,Tpf)
 DEFINE Title0       STRING,
	Title1,  
	Title2,
	Title3       STRING,  
	Field1, 
	Field2,
	Field3,
	Condicion    STRING,
	Tabname      STRING,
	OrderNum     STRING, 
        retorna      RECORD  
         codigo      VARCHAR(20),
         nombre      VARCHAR(100),
         numtri      VARCHAR(30) 
	END RECORD,
	la_busqueda  DYNAMIC ARRAY OF RECORD 	
         codigo      VARCHAR(20),
         nombre      VARCHAR(100),
         numtri      VARCHAR(30) 
        END RECORD,
        i,aborta     SMALLINT,
        w            ui.Window,
        f            ui.Form,
        sqlstmnt     STRING,
        strfil       STRING,
        refe         VARCHAR(100),
        buscar       VARCHAR(105),
        tipofiltro   SMALLINT, 
        opci,tpf     SMALLINT, 
        scr_cnt      SMALLINT

 -- Cargando acciones default
 CALL ui.Interface.loadActionDefaults("../std/formlist")

 -- Definiendo tecla de aceptar
 OPTIONS ACCEPT KEY escape

 LET int_flag = 0  
 LET tipofiltro = 0 

 -- Abriendo la ventana                                                                
 OPEN WINDOW find_ayuda 
  WITH FORM "formlist2"

  -- Obteniendo datos de la ventana
  LET w = ui.Window.getcurrent()
  LET f = w.getForm()

  -- Desplegandp titulso
  CALL fgl_settitle(title0)
  CALL f.setElementText("formonly.codigo",title1)
  CALL f.setElementText("formonly.nombre",title2)
  CALL f.setElementText("formonly.numtri",title3)

  -- Inicializando datos
  CALL la_busqueda.clear()
  LET aborta = 0  

  -- Buscando datos 
  WHILE aborta = 0
   INITIALIZE retorna.* TO NULL 
   DISPLAY BY NAME tipofiltro 
   LET refe = "Ingrese informacion a buscar ..."
   LET int_flag = FALSE  
   LET buscar = refe 
   LET i = 1

   -- Ingresando datos a buscar
   INPUT BY NAME refe WITHOUT DEFAULTS 
    ON ACTION nit          
     -- Seleccionanado filtro por numero tributario 
     LET tipofiltro = 1 
     DISPLAY BY NAME tipofiltro 

    ON ACTION nombre  
     -- Seleccionanado filtro por nombre
     LET tipofiltro = 0 
     DISPLAY BY NAME tipofiltro 

    BEFORE INPUT
     -- Desabilitando tipo de filtro cuando seleccion no aplica 
     IF NOT tpf THEN
        CALL Dialog.SetActionHidden("nit",1) 
        CALL Dialog.SetActionHidden("nombre",1) 
     END IF 

    BEFORE FIELD refe
     -- Si no hay ingreso de datos a buscar 
     IF opci=0 THEN
       -- Creando busqueda
       LET sqlstmnt = " SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED, ",", Field3 CLIPPED, 
                       " ORDER BY ",OrderNum

       -- Preparando busqueda
       PREPARE ex4_sqlst FROM sqlstmnt

       -- Seleccionando datos
       DECLARE westos4 CURSOR FOR ex4_sqlst
       CALL la_busqueda.CLEAR()
       LET i =1
       FOREACH westos4 INTO la_busqueda[i].*
        LET i=i+1
       END FOREACH
       FREE westos4
       FREE ex4_sqlst

      -- Desplegando numero de datos seleccionados
      CALL f.setElementText("registros",'Detalle de Registros Encontrados '||(i-1))

      -- Desplegnando datos
      DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
       BEFORE ROW
        EXIT DISPLAY
      END DISPLAY

      LET buscar = refe
      EXIT INPUT
     END IF

    AFTER FIELD refe
     -- Verificando rotulo
     LET refe = refe CLIPPED
     IF (refe="Ingrese informacion a buscar ...") OR
        (LENGTH(refe)=0) THEN
        LET refe = "*"
     END IF
     LET buscar = "*"||refe||"*"

     -- Si se ingresan datos a buscar
     IF opci=1 THEN 
        -- Verificando fitro
        CASE (tipofiltro)
         WHEN 0 LET strfil = " AND UPPER(",field2 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"
         WHEN 1 LET strfil = " AND UPPER(",field3 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"
        END CASE

        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED, ",", Field3 CLIPPED, 
                        " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                        strfil CLIPPED,
                        " ORDER BY ",OrderNum
     ELSE
        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED, ",", Field3 CLIPPED, 
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum
     END IF

     -- Preparando busqueda
     PREPARE ex5_sqlst1 FROM sqlstmnt

     -- Seleccionando datos
     DECLARE yestos5 CURSOR FOR ex5_sqlst1
     CALL la_busqueda.CLEAR()
     LET i =1
     FOREACH yestos5 INTO la_busqueda[i].*
      LET i=i+1
     END FOREACH
     FREE yestos5
     FREE ex5_sqlst1

     -- Desplegando numero de datos seleccionados
     CALL f.setElementText("registros",'Detalle de Registros encontrados '||(i-1))

     -- Desplegnando datos
     DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
      BEFORE ROW
       EXIT DISPLAY
     END DISPLAY
     LET buscar = refe
     EXIT INPUT

    ON IDLE 1
     -- Leyendo datos del buffer cada segundo 
     LET refe = GET_FLDBUF(refe)
     IF LENGTH(refe)=0 THEN
        LET refe = ""
     END IF

     IF buscar<>refe OR 
        opci=0 THEN
        LET buscar = "*"||refe||"*"

      -- Si se ingresan datos a buscar
      IF opci=1 THEN 
        -- Verificando fitro
        CASE (tipofiltro)
         WHEN 0 LET strfil = " AND UPPER(",field2 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"
         WHEN 1 LET strfil = " AND UPPER(",field3 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"
        END CASE

        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED, ",", Field3 CLIPPED, 
                        " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                        strfil CLIPPED,
                        " ORDER BY ",OrderNum
      ELSE
        -- Creando busqueda
        LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED, ",", Field3 CLIPPED, 
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum
      END IF

      -- Preparando busqueda
      PREPARE ex1_sqlst6 FROM sqlstmnt

      -- Seleccionando datos
      DECLARE xestos6 CURSOR FOR ex1_sqlst6
      CALL la_busqueda.CLEAR()
      LET i =1
      FOREACH xestos6 INTO la_busqueda[i].*
       LET i=i+1
      END FOREACH
      FREE xestos6
      FREE ex1_sqlst6

      -- Desplegando numero de datos seleccionados
      CALL f.setElementText("registros",'Detalle de Registros Encontrados '||(i-1))
 
      -- Desplegnando datos
      LET aborta = 0  
      DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
       BEFORE ROW
        EXIT DISPLAY
      END DISPLAY
      LET buscar = refe

      -- Si no se pedian datos de busqueda
      IF opci=0 THEN  
         EXIT INPUT
      END IF
     END IF 

    AFTER INPUT
     IF int_flag THEN
        LET aborta = 1
        EXIT INPUT
     END IF
   END INPUT
   IF aborta THEN
      LET int_flag = TRUE
      EXIT WHILE
   END IF

   -- Definiendo tecla de aceptacion
   OPTIONS ACCEPT KEY RETURN

   -- Desplegnado datos
   CALL f.setElementText("registros","Detalle de Registros Encontrados "||(i-1)) 
   MESSAGE "Presione [ENTER] para seleccionar"

   DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
    ATTRIBUTE(ACCEPT=FALSE) 
    BEFORE DISPLAY
     -- Desabilitando botom
     IF (opci=0) THEN
        CALL Dialog.SetActionHidden("consulta",1) 
     ELSE 
        CALL Dialog.SetActionHidden("cancel",1) 
     END IF  

     -- Si no se ecnontraron datos
     IF i=1 THEN
        MESSAGE "Atencion: no existen datos registrados." 
     END IF
  
    BEFORE ROW
     LET i       = ARR_CURR()
     LET scr_cnt = SCR_LINE()
     LET retorna.* = la_busqueda[i].*
     IF LENGTH(la_busqueda[i].codigo)=0 THEN
        CALL fgl_dialog_setcurrline(scr_cnt,i-1)
     END IF

    ON ACTION consulta
     IF opci=1 THEN
        LET aborta = 0
        EXIT DISPLAY
     END IF

    ON ACTION seleccion
     LET aborta = 1
     EXIT DISPLAY

    ON ACTION cancel
     LET aborta = 1
     EXIT DISPLAY 

    ON KEY(RETURN)
     LET aborta = 1
     EXIT DISPLAY

    AFTER DISPLAY
     CONTINUE DISPLAY
   END DISPLAY
   MESSAGE "" 

   IF aborta=1 THEN
      EXIT WHILE
   END IF
  END WHILE
 CLOSE WINDOW find_ayuda

 -- Definiendo tecla de aceptacion
 OPTIONS ACCEPT KEY ESCAPE

 -- Inicializando datos
 CALL la_busqueda.clear()

 -- Cargando acciones default
 CALL ui.Interface.loadActionDefaults("../std/actiondefaults")

 RETURN retorna.*,int_flag 
END FUNCTION

-- Subrutina para seleccionar datos de una lista 

FUNCTION librut002_selectlist(Title0,Title1,Title2,Field1,Field2,Tabname,Condicion,OrderNum,
                              HiddenField1)
 DEFINE Title0       STRING,
	Title1,  
	Title2       STRING,  
	Field1, 
	Field2,
	Condicion    STRING,
	Tabname      STRING,
	OrderNum     STRING, 
        retorna      RECORD  
         codigo      VARCHAR(20),
         nombre      VARCHAR(100)
	END RECORD,
	la_busqueda  DYNAMIC ARRAY OF RECORD 	
         codigo      VARCHAR(20),
         nombre      VARCHAR(100)
        END RECORD,
        i            SMALLINT,
        w            ui.Window,
        f            ui.Form,
        sqlstmnt     STRING,
        refe         VARCHAR(100),
        buscar       VARCHAR(105),
        scr_cnt      SMALLINT, 
        HiddenField1 SMALLINT 

 -- Cargando acciones default
 CALL ui.Interface.loadActionDefaults("../std/formlist")

 -- Definiendo tecla de aceptar
 OPTIONS ACCEPT KEY escape

 LET int_flag = 0  

 -- Abriendo la ventana                                                                
 OPEN WINDOW find_ayuda 
  WITH FORM "formlist3"

  -- Obteniendo datos de la ventana
  LET w = ui.Window.getcurrent()
  LET f = w.getForm()

  -- Desplegandp titulso
  CALL fgl_settitle(title0)
  CALL f.setElementText("formonly.codigo",title1)
  CALL f.setElementText("formonly.nombre",title2)

  -- Inicializando datos
  CALL la_busqueda.clear()
  LET int_flag = FALSE  
  LET i = 1

  -- Creando busqueda
  LET sqlstmnt = " SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED, 
                  " FROM ",tabname CLIPPED,
                  " WHERE ",condicion CLIPPED, 
                  " ORDER BY ",OrderNum

  -- Preparando busqueda
  PREPARE esqlst FROM sqlstmnt

  -- Seleccionando datos
  DECLARE cursel CURSOR FOR esqlst
  CALL la_busqueda.CLEAR()
  LET i =1
  FOREACH cursel INTO la_busqueda[i].*
   LET i=i+1
  END FOREACH
  FREE cursel 
  FREE esqlst

  -- Definiendo tecla de aceptacion
  OPTIONS ACCEPT KEY RETURN

  -- Desplegnado datos
  MESSAGE "Presione [ENTER] para seleccionar"

  DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
   ATTRIBUTE(ACCEPT=FALSE) 
    BEFORE DISPLAY
     -- Si no se ecnontraron datos
     IF i=1 THEN
        MESSAGE "Atencion: no existen datos registrados." 
     END IF
  
    BEFORE ROW
     LET i       = ARR_CURR()
     LET scr_cnt = SCR_LINE()
     LET retorna.* = la_busqueda[i].*
     IF LENGTH(la_busqueda[i].codigo)=0 THEN
        CALL fgl_dialog_setcurrline(scr_cnt,i-1)
     END IF

    ON ACTION seleccion
     EXIT DISPLAY

    ON ACTION cancel
     EXIT DISPLAY 

    ON KEY(RETURN)
     EXIT DISPLAY

    AFTER DISPLAY
     CONTINUE DISPLAY
  END DISPLAY
  MESSAGE "" 
 CLOSE WINDOW find_ayuda

 -- Definiendo tecla de aceptacion
 OPTIONS ACCEPT KEY ESCAPE

 -- Inicializando datos
 CALL la_busqueda.clear()

 -- Cargando acciones default
 CALL ui.Interface.loadActionDefaults("../std/actiondefaults")

 RETURN retorna.*,int_flag 
END FUNCTION

-- Subrutina para crear combobox dinamicos con mensaje de respuesta

FUNCTION librut002_cbxdin(vl_2_campo,vl_2_query)
 DEFINE vl_2_lista_nodb base.StringTokenizer
 DEFINE vl_2_combo      ui.ComboBox
 DEFINE vl_2_campo      STRING
 DEFINE vl_2_query      STRING
 DEFINE vl_2_metodo     STRING
 DEFINE vl_2_valor      STRING
 DEFINE vl_2_valor2     STRING
 DEFINE vl_2_lista      CHAR(500)
 DEFINE vl_2_lista2     CHAR(500)

 -- Quitando espacioes en blanco
 LET vl_2_campo = vl_2_campo.trim()
 LET vl_2_query = vl_2_query.trim()

 -- Obtiene el nodo del metodo combo de la clase ui
 LET vl_2_combo = ui.ComboBox.forName(vl_2_campo)
 IF vl_2_combo IS NULL THEN
    RETURN
 END IF

 -- Inicializa el combo
 CALL vl_2_combo.clear()

 -- Convirtiendo todo a minusculas
 LET vl_2_metodo = vl_2_query.toUpperCase()

 -- Verificando metodo
 IF vl_2_metodo matches "SELECT*" THEN
    --Preparando el query
    PREPARE q_comboList3 from vl_2_query
    DECLARE c_comboList3 cursor FOR q_comboList3

    -- Obteniendo la lista de valores de la tabla segun el query enviado
    FOREACH c_comboList3 INTO vl_2_lista,vl_2_lista2
     LET vl_2_valor  = vl_2_lista  CLIPPED
     LET vl_2_valor2 = vl_2_lista2 CLIPPED
     CALL vl_2_combo.addItem(vl_2_valor,vl_2_valor2)
    END FOREACH
 ELSE
    -- Obteniendo la lista de valores que no son de base de datos
    LET vl_2_lista_nodb = base.StringTokenizer.create(vl_2_query,"|")

    WHILE vl_2_lista_nodb.hasMoreTokens()
     LET  vl_2_valor = vl_2_lista_nodb.nextToken()
     display vl_2_valor 
     CALL vl_2_combo.addItem(vl_2_valor,vl_2_valor)
    END WHILE
 END IF

 -- Retornando numero de datos 
 RETURN vl_2_combo.getItemCount() 
END FUNCTION

-- Subrutina para seleccionar los datos del producto 

FUNCTION librut002_formlistpro(Title0,Title1,Title2,Title3,Title4,
                               Field1,Field2,Field3,Tabname,Condicion,OrderNum,opci,Tpf)

 DEFINE Title0       STRING,
	Title1,  
	Title2,
	Title3,
        Title4        STRING,  
	Field1, 
	Field2,
	Field3,
	Condicion    STRING,
	Tabname      STRING,
	OrderNum     STRING, 
        retorna      RECORD  
         codigo      VARCHAR(20),
         descripcion VARCHAR(100)
	END RECORD,
	la_busqueda  DYNAMIC ARRAY OF RECORD 	
         codigo      VARCHAR(20),
         descripcion VARCHAR(100),
         precio      DEC(12,2),
         existencia  DEC(12,2)
        END RECORD,
        wcditem      LIKE inv_products.cditem,
        i,aborta     SMALLINT,
        w            ui.Window,
        f            ui.Form,
        sqlstmnt     STRING,
        strfil       STRING,
        refe         VARCHAR(100),
        buscar       VARCHAR(105),
        tipofiltro   SMALLINT, 
        opci,tpf     SMALLINT, 
        scr_cnt      SMALLINT

 -- Cargando acciones default
 CALL ui.Interface.loadActionDefaults("../std/formlist")

 -- Definiendo tecla de aceptar
 OPTIONS ACCEPT KEY escape

 LET int_flag = 0  
 LET tipofiltro = 0 

 -- Abriendo la ventana                                                                
 OPEN WINDOW find_ayuda 
  WITH FORM "formlistpro"

  -- Obteniendo datos de la ventana
  LET w = ui.Window.getcurrent()
  LET f = w.getForm()

  -- Desplegandp titulso
  CALL fgl_settitle(title0)
  CALL f.setElementText("formonly.codigo",title1)
  CALL f.setElementText("formonly.descripcion",title2)
  CALL f.setElementText("formonly.precio",title3)
  CALL f.setElementText("formonly.existencia",title4)

  -- Inicializando datos
  CALL la_busqueda.clear()
  LET aborta = 0  

  -- Buscando datos 
  WHILE aborta = 0
   INITIALIZE retorna.* TO NULL 
   DISPLAY BY NAME tipofiltro 
   LET refe = "Ingrese informacion a buscar ..."
   LET int_flag = FALSE  
   LET buscar = refe 
   LET i = 1

   -- Ingresando datos a buscar
   INPUT BY NAME refe WITHOUT DEFAULTS 
    ON ACTION codigo
     -- Seleccionanado filtro por codigo
     LET tipofiltro = 1 
     DISPLAY BY NAME tipofiltro 

    ON ACTION descrip 
     -- Seleccionanado filtro por descripcion 
     LET tipofiltro = 0 
     DISPLAY BY NAME tipofiltro 

    BEFORE INPUT
     -- Desabilitando tipo de filtro cuando seleccion no aplica 
     IF NOT tpf THEN
        CALL Dialog.SetActionHidden("codigo",1) 
        CALL Dialog.SetActionHidden("descrip",1) 
     END IF 

    BEFORE FIELD refe
     -- Si no hay ingreso de datos a buscar 
     IF opci=0 THEN
       -- Creando busqueda
       LET sqlstmnt =  "SELECT cditem,",Field1 CLIPPED,",",Field2 CLIPPED,",",Field3 CLIPPED,",0",
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum

       -- Preparando busqueda
       PREPARE ex2_sqlstx FROM sqlstmnt

       -- Seleccionando datos
       DECLARE westos0x CURSOR FOR ex2_sqlstx
       CALL la_busqueda.CLEAR()
       LET i =1
       FOREACH westos0x INTO wcditem,la_busqueda[i].*
        -- Obteniendo existencia del producto
        SELECT NVL(SUM(a.exican),0)
         INTO  la_busqueda[i].existencia
         FROM  inv_proenbod a
         WHERE a.cditem = wcditem 

        -- Incrementando contador
        LET i=i+1
       END FOREACH
       FREE westos0x
       FREE ex2_sqlstx

      -- Desplegando numero de datos seleccionados
      CALL f.setElementText("registros",'Detalle de Registros Encontrados '||(i-1))

      -- Desplegnando datos
      DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
       BEFORE ROW
        EXIT DISPLAY
      END DISPLAY

      LET buscar = refe
      EXIT INPUT
     END IF

    AFTER FIELD refe
     -- Verificando rotulo
     LET refe = refe CLIPPED
     IF (refe="Ingrese informacion a buscar ...") OR
        (LENGTH(refe)=0) THEN
        LET refe = "*"
     END IF
     LET buscar = "*"||refe||"*"

     -- Si se ingresan datos a buscar
     IF opci=1 THEN 
        -- Verificando fitro
        CASE (tipofiltro)
         WHEN 0 LET strfil = " AND UPPER(",field2 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"
         WHEN 1 LET strfil = " AND UPPER(",field1 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"
        END CASE

        -- Creando busqueda
        LET sqlstmnt = "SELECT cditem,",Field1 CLIPPED,",",Field2 CLIPPED,",",Field3 CLIPPED,",0",
                        " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                        strfil CLIPPED,
                        " ORDER BY ",OrderNum
     ELSE
        -- Creando busqueda
        LET sqlstmnt = "SELECT cditem,",Field1 CLIPPED,",",Field2 CLIPPED,",",Field3 CLIPPED,",0",
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum
     END IF

     -- Preparando busqueda
     PREPARE ex0_sqlst1y FROM sqlstmnt

     -- Seleccionando datos
     DECLARE yestos1y CURSOR FOR ex0_sqlst1y
     CALL la_busqueda.CLEAR()
     LET i =1
     FOREACH yestos1y INTO wcditem,la_busqueda[i].*
      -- Obteniendo existencia del producto
      SELECT NVL(SUM(a.exican),0)
       INTO  la_busqueda[i].existencia
       FROM  inv_proenbod a
       WHERE a.cditem = wcditem 

      -- Incrementando contador
      LET i=i+1
     END FOREACH
     FREE yestos1y
     FREE ex0_sqlst1y

     -- Desplegando numero de datos seleccionados
     CALL f.setElementText("registros",'Detalle de Registros encontrados '||(i-1))

     -- Desplegnando datos
     DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
      BEFORE ROW
       EXIT DISPLAY
     END DISPLAY
     LET buscar = refe
     EXIT INPUT

    ON IDLE 1
     -- Leyendo datos del buffer cada segundo 
     LET refe = GET_FLDBUF(refe)
     IF LENGTH(refe)=0 THEN
        LET refe = ""
     END IF

     IF buscar<>refe OR 
        opci=0 THEN
        LET buscar = "*"||refe||"*"

      -- Si se ingresan datos a buscar
      IF opci=1 THEN 
        -- Verificando fitro
        CASE (tipofiltro)
         WHEN 0 LET strfil = " AND UPPER(",field2 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"
         WHEN 1 LET strfil = " AND UPPER(",field1 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"
        END CASE

        -- Creando busqueda
        LET sqlstmnt = "SELECT cditem,",Field1 CLIPPED,",",Field2 CLIPPED,",",Field3 CLIPPED,",0",
                        " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                        strfil CLIPPED,
                        " ORDER BY ",OrderNum
      ELSE
        -- Creando busqueda
        LET sqlstmnt = "SELECT cditem,",Field1 CLIPPED,",",Field2 CLIPPED,",",Field3 CLIPPED,",0",
                       " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                       " ORDER BY ",OrderNum
      END IF

      -- Preparando busqueda
      PREPARE ex1_sqlst2z FROM sqlstmnt

      -- Seleccionando datos
      DECLARE xestos2z CURSOR FOR ex1_sqlst2z
      CALL la_busqueda.CLEAR()
      LET i =1
      FOREACH xestos2z INTO wcditem,la_busqueda[i].*
       -- Obteniendo existencia del producto
       SELECT NVL(SUM(a.exican),0)
        INTO  la_busqueda[i].existencia
        FROM  inv_proenbod a
        WHERE a.cditem = wcditem 

       -- Incrementando contador
       LET i=i+1
      END FOREACH
      FREE xestos2z
      FREE ex1_sqlst2z

      -- Desplegando numero de datos seleccionados
      CALL f.setElementText("registros",'Detalle de Registros Encontrados '||(i-1))
 
      -- Desplegnando datos
      LET aborta = 0  
      DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
       BEFORE ROW
        EXIT DISPLAY
      END DISPLAY
      LET buscar = refe

      -- Si no se pedian datos de busqueda
      IF opci=0 THEN  
         EXIT INPUT
      END IF
     END IF 

    AFTER INPUT
     IF int_flag THEN
        LET aborta = 1
        EXIT INPUT
     END IF
   END INPUT
   IF aborta THEN
      LET int_flag = TRUE
      EXIT WHILE
   END IF

   -- Definiendo tecla de aceptacion
   OPTIONS ACCEPT KEY RETURN

   -- Desplegnado datos
   CALL f.setElementText("registros","Detalle de Registros Encontrados "||(i-1)) 
   MESSAGE "Presione [ENTER] para seleccionar"

   DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
    ATTRIBUTE(ACCEPT=FALSE) 
    BEFORE DISPLAY
     -- Desabilitando botom
     IF (opci=0) THEN
        CALL Dialog.SetActionHidden("consulta",1) 
     ELSE 
        CALL Dialog.SetActionHidden("cancel",1) 
     END IF  

     -- Si no se ecnontraron datos
     IF i=1 THEN
        MESSAGE "Atencion: no existen datos registrados." 
     END IF
  
    BEFORE ROW
     LET i       = ARR_CURR()
     LET scr_cnt = SCR_LINE()
     LET retorna.codigo      = la_busqueda[i].codigo
     LET retorna.descripcion = la_busqueda[i].descripcion
     IF LENGTH(la_busqueda[i].codigo)=0 THEN
        CALL fgl_dialog_setcurrline(scr_cnt,i-1)
     END IF

    ON ACTION consulta
     IF opci=1 THEN
        LET aborta = 0
        EXIT DISPLAY
     END IF

    ON ACTION seleccion
     LET aborta = 1
     EXIT DISPLAY

    ON ACTION cancel
     LET aborta = 1
     EXIT DISPLAY 

    ON KEY(RETURN)
     LET aborta = 1
     EXIT DISPLAY

    AFTER DISPLAY
     CONTINUE DISPLAY
   END DISPLAY
   MESSAGE "" 

   IF aborta=1 THEN
      EXIT WHILE
   END IF
  END WHILE
 CLOSE WINDOW find_ayuda

 -- Definiendo tecla de aceptacion
 OPTIONS ACCEPT KEY ESCAPE

 -- Inicializando datos
 CALL la_busqueda.clear()

 -- Cargando acciones default
 CALL ui.Interface.loadActionDefaults("../std/actiondefaults")

 RETURN retorna.*,int_flag 
END FUNCTION

-- Subrutina para crear combobox dinamicos 

FUNCTION librut002_comboboxdin(vl_2_campo,vl_2_query)
 DEFINE vl_2_lista_nodb base.StringTokenizer
 DEFINE vl_2_combo      ui.ComboBox
 DEFINE vl_2_campo      STRING
 DEFINE vl_2_query      STRING
 DEFINE vl_2_metodo     STRING
 DEFINE vl_2_valor      STRING
 DEFINE vl_2_valor2     STRING
 DEFINE vl_2_lista      CHAR(500)
 DEFINE vl_2_lista2     CHAR(500)
 DEFINE dato            CHAR(100) 
 DEFINE i               INTEGER 

 -- Quitando espacios en blanco
 LET vl_2_campo = vl_2_campo.trim()
 LET vl_2_query = vl_2_query.trim()
 LET dato = NULL  

 -- Obtiene el nodo del metodo combo de la clase ui
 LET vl_2_combo = ui.ComboBox.forName(vl_2_campo)
 IF vl_2_combo IS NULL THEN
    RETURN dato 
 END IF

 -- Inicializa el combo
 CALL vl_2_combo.clear()

 -- Convirtiendo todo a minusculas
 LET vl_2_metodo = vl_2_query.toUpperCase()

 -- Verificando metodo
 IF vl_2_metodo matches "SELECT*" THEN
    --Preparando el query
    PREPARE q_comboList4 from vl_2_query
    DECLARE c_comboList4 cursor FOR q_comboList4

    -- Obteniendo la lista de valores de la tabla segun el query enviado
    LET i=1 
    FOREACH c_comboList4 INTO vl_2_lista,vl_2_lista2
     LET vl_2_valor  = vl_2_lista  CLIPPED
     LET vl_2_valor2 = vl_2_lista2 CLIPPED
     CALL vl_2_combo.addItem(vl_2_valor,vl_2_valor2)

     IF (i=1) THEN
        LET dato = vl_2_valor  
     END IF 
     display vl_2_valor  
     LET i = i+1 
    END FOREACH
 ELSE
    -- Obteniendo la lista de valores que no son de base de datos
    LET vl_2_lista_nodb = base.StringTokenizer.create(vl_2_query,"|")

    WHILE vl_2_lista_nodb.hasMoreTokens()
     LET  vl_2_valor = vl_2_lista_nodb.nextToken()
     CALL vl_2_combo.addItem(vl_2_valor,vl_2_valor)
    END WHILE
 END IF

 -- Verificanod is hay datos
 IF (vl_2_combo.getItemCount()=0) THEN 
    ERROR " Atencion: no existe datos registrados. (VERIFICA listas de datos)"
 END IF 
 RETURN dato 
END FUNCTION

-- Subrutina para listas de busqueda de datos 

FUNCTION librut002_ListaBusqueda(Title0,Title1,Title2,Title3,Field1,
                                 Field2,Field3,Tabname,Condicion,OrderNum)

 DEFINE Title0,
	Title1,  
	Title2,
        Title3       STRING,  
	Field1, 
	Field2,
	Field3,
	Condicion    STRING,
	Tabname      STRING,
	OrderNum     STRING, 
        retorna      RECORD  
         codigo      VARCHAR(20),
         descripcion VARCHAR(100)
	END RECORD,
	la_busqueda  DYNAMIC ARRAY OF RECORD 	
         codigo      VARCHAR(20),
         descripcion VARCHAR(100),
         precio      DEC(12,2)
        END RECORD,
        i,aborta     SMALLINT,
        w            ui.Window,
        f            ui.Form,
        sqlstmnt     STRING,
        strfil       STRING,
        refe         VARCHAR(100),
        buscar       VARCHAR(105),
        scr_cnt      SMALLINT

 -- Cargando acciones default
 CALL ui.Interface.loadActionDefaults("../std/formlist")

 -- Definiendo tecla de aceptar
 OPTIONS ACCEPT KEY escape
 LET int_flag = 0  

 -- Abriendo la ventana 
 OPEN WINDOW find_ayuda 
  WITH FORM "formbusqueda" 

  -- Obteniendo datos de la ventana
  LET w = ui.Window.getcurrent()
  LET f = w.getForm()

  -- Desplegando titulso
  CALL fgl_settitle(title0)
  CALL f.setElementText("formonly.codigo",title1)
  CALL f.setElementText("formonly.descripcion",title2)
  CALL f.setElementText("formonly.precio",title3)

  -- Inicializando datos
  CALL la_busqueda.clear()
  LET aborta = 0  

  -- Buscando datos 
  WHILE aborta = 0
   INITIALIZE retorna.* TO NULL 
   LET refe = "Ingrese informacion a buscar ..."
   LET int_flag = FALSE  
   LET buscar = refe 
   LET i = 1

   -- Ingresando datos a buscar
   INPUT BY NAME refe WITHOUT DEFAULTS 
    AFTER FIELD refe
     -- Verificando rotulo
     LET refe = refe CLIPPED
     IF (refe="Ingrese informacion a buscar ...") OR
        (LENGTH(refe)=0) THEN
        LET refe = "*"
     END IF
     LET buscar = "*"||refe||"*"

     -- Si se ingresan datos a buscar
     LET strfil=" AND UPPER(",field2 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"

     -- Creando busqueda
     LET sqlstmnt = "SELECT ",Field1 CLIPPED,
                          ","||Field2 CLIPPED,
                          ","||Field3 CLIPPED,
                     " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                     strfil CLIPPED,
                     " ORDER BY ",OrderNum

     -- Preparando busqueda
     PREPARE bex0_sqlst1y FROM sqlstmnt

     -- Seleccionando datos
     DECLARE byestos1y CURSOR FOR bex0_sqlst1y
     CALL la_busqueda.CLEAR()
     LET i =1
     FOREACH byestos1y INTO la_busqueda[i].*
      -- Incrementando contador
      LET i=i+1
     END FOREACH
     FREE byestos1y
     FREE bex0_sqlst1y

     -- Desplegando numero de datos seleccionados
     CALL f.setElementText("registros",'Detalle de Registros encontrados '||(i-1))

     -- Desplegnando datos
     DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
      BEFORE ROW
       EXIT DISPLAY
     END DISPLAY
     LET buscar = refe
     EXIT INPUT

    ON KEY (DOWN)
     -- Mover hacia abajo 
     EXIT INPUT  

    ON IDLE 1
     -- Leyendo datos del buffer cada segundo 
     LET refe = GET_FLDBUF(refe)
     IF LENGTH(refe)=0 THEN
        LET refe = ""
     END IF

     IF buscar<>refe THEN
      LET buscar = "*"||refe||"*"

      -- Si se ingresan datos a buscar
      LET strfil =
       " AND UPPER(",field2 CLIPPED,") MATCHES UPPER('",buscar CLIPPED,"')"

      -- Creando busqueda
      LET sqlstmnt = "SELECT ",Field1 CLIPPED,
                          ","||Field2 CLIPPED,
                          ","||Field3 CLIPPED,
                      " FROM ",Tabname CLIPPED," WHERE ",Condicion CLIPPED,
                      strfil CLIPPED,
                      " ORDER BY ",OrderNum

      -- Preparando busqueda
      PREPARE bex1_sqlst2z FROM sqlstmnt

      -- Seleccionando datos
      DECLARE bxestos2z CURSOR FOR bex1_sqlst2z
      CALL la_busqueda.CLEAR()
      LET i =1
      FOREACH bxestos2z INTO la_busqueda[i].*
       -- Incrementando contador
       LET i=i+1
      END FOREACH
      FREE bxestos2z
      FREE bex1_sqlst2z

      -- Desplegando numero de datos seleccionados
      CALL f.setElementText("registros",'Detalle de Registros Encontrados '||(i-1))
 
      -- Desplegnando datos
      LET aborta = 0  
      DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
       BEFORE ROW
        EXIT DISPLAY
      END DISPLAY
      LET buscar = refe
     END IF 

    AFTER INPUT
     IF int_flag THEN
        LET aborta = 1
        EXIT INPUT
     END IF
   END INPUT
   IF aborta THEN
      LET int_flag = TRUE
      EXIT WHILE
   END IF

   -- Definiendo tecla de aceptacion
   OPTIONS ACCEPT KEY RETURN

   -- Desplegnado datos
   CALL f.setElementText("registros","Detalle de Registros Encontrados "||(i-1)) 
   MESSAGE "Presione [ENTER] para seleccionar"

   DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
    ATTRIBUTE(ACCEPT=FALSE) 
    BEFORE DISPLAY
     -- Desabilitando botom
     CALL Dialog.SetActionHidden("cancel",1) 

     -- Si no se ecnontraron datos
     IF i=1 THEN
        MESSAGE "Atencion: no existen datos registrados." 
     END IF
  
    BEFORE ROW
     LET i       = ARR_CURR()
     LET scr_cnt = SCR_LINE()
     LET retorna.codigo      = la_busqueda[i].codigo
     LET retorna.descripcion = la_busqueda[i].descripcion
     IF LENGTH(la_busqueda[i].codigo)=0 THEN
        CALL fgl_dialog_setcurrline(scr_cnt,i-1)
     END IF

    ON ACTION consulta
     LET aborta = 0
     EXIT DISPLAY

    ON KEY (CONTROL-I) 
     LET aborta = 0
     EXIT DISPLAY

    ON ACTION seleccion
     LET aborta = 1
     EXIT DISPLAY

    ON ACTION cancel
     LET aborta = 1
     EXIT DISPLAY 

    ON KEY(RETURN)
     LET aborta = 1
     EXIT DISPLAY

    AFTER DISPLAY
     CONTINUE DISPLAY
   END DISPLAY
   MESSAGE "" 

   IF aborta=1 THEN
      EXIT WHILE
   END IF
  END WHILE
 CLOSE WINDOW find_ayuda

 -- Definiendo tecla de aceptacion
 OPTIONS ACCEPT KEY ESCAPE

 -- Inicializando datos
 CALL la_busqueda.clear()

 -- Cargando acciones default
 CALL ui.Interface.loadActionDefaults("../std/actiondefaults")

 RETURN retorna.*,int_flag 
END FUNCTION
