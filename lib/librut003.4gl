{ 
Programa : librut003.4gl 
Programo : Mynor Ramirez
Objetivo : Subruitinas para crear combobox 
}

DATABASE erpjuridico 

DEFINE qrytext STRING 

-- Subrutina que carga el combobox de tipos de gasto  

FUNCTION librut003_CbxTiposGasto() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.tipgto,a.nomgto ",
               " FROM glb_tipgasto a ",
               " ORDER BY 2 "

 CALL librut002_combobox("tipgto",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de concepto de notas  

FUNCTION librut003_CbxConceptoNotas() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.codnta,a.nomnta ",
               " FROM fac_tiponota a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codnta",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las empresas  

FUNCTION librut003_CbxEmpresas() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.codemp,a.nomemp ",
               " FROM glb_empresas a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codemp",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las empresas  

FUNCTION librut003_DCbxEmpresas() 
 DEFINE dato CHAR(10)  

 -- Llenando combobox 
 LET qrytext = "SELECT a.codemp,a.nomemp ",
               " FROM glb_empresas a ",
               " ORDER BY 2 "

 LET dato = librut002_comboboxdin("codemp",qrytext)
 RETURN dato 
END FUNCTION

-- Subrutina que carga el combobox de las empresas x usuario 

FUNCTION librut003_DCbxEmpresasXUsuario() 
 DEFINE dato CHAR(10)
 DEFINE usuario STRING 

   LET usuario = fgl_getenv("LOGNAME")

 -- Llenando combobox 
 LET qrytext = "SELECT a.codemp,a.nomemp ",
               " FROM glb_empresas a ",
               " WHERE EXISTS (SELECT y.codemp FROM glb_permxemp y 
                                WHERE y.codemp = a.codemp
                                  AND y.userid = '", usuario CLIPPED, "') ", -- USER) ",
               " ORDER BY 2 "

 LET dato = librut002_comboboxdin("codemp",qrytext)
 RETURN dato 
END FUNCTION

-- Subrutina que carga el combobox de la empresa en uso x un usuario 

FUNCTION librut003_DCbxEmpresaEnUsoXUsuario()              
 DEFINE dato CHAR(10)  
 DEFINE usuario STRING 

 LET usuario = FGL_GETENV("LOGNAME")
 -- Llenando combobox 
 LET qrytext = "SELECT a.codemp,a.nomemp ",
               " FROM glb_empresas a ",
               " WHERE EXISTS (SELECT y.codemp FROM glb_empenuso y 
                                WHERE y.codemp = a.codemp
                                  AND y.userid = '", usuario CLIPPED, "') ",
               " ORDER BY 2 "

 LET dato = librut002_comboboxdin("codemp",qrytext)
 RETURN dato 
END FUNCTION

-- Subrutina que carga el combobox de las cuentas de banco por empresa 

FUNCTION librut003_DCbxCuentasBanco(wcodemp)
 DEFINE wcodemp LIKE bco_mcuentas.codemp,
        dato    CHAR(40)  

 -- Llenando combobox
 LET qrytext = 
   "SELECT x.numcta,rpad(x.numcor,2,' ')||' - '||",
          "TRIM(x.numcta)||' - '||a.nombco||' - '||x.nomcta ",
   " FROM  bco_mcuentas x,glb_mtbancos a ",
   " WHERE x.codemp = ",wcodemp,
   "   AND a.codbco = x.codbco ",
   " ORDER BY x.numcor "

 LET dato = librut002_comboboxdin("numcta",qrytext)
 RETURN dato 
END FUNCTION

-- Subrutina que carga el combobox de las categorias 

FUNCTION librut003_cbxcategorias()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codcat,a.nomcat ",
               " FROM glb_categors a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codcat",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de caja 

FUNCTION librut003_CbxTiposCaja(lusername)
DEFINE lusername         LIKE glb_permxusr.userid,
         lroleid         LIKE glb_rolesusr.roleid

 --Obteniendo el perfil
 SELECT roleid INTO lroleid FROM glb_usuarios WHERE userid = lusername
 
 -- Llenando combobox 
 LET qrytext = "SELECT a.tipcaj,a.nomtip ",
               " FROM glb_tiposcaj a ",
               " WHERE tipcaj IN ", 
               " (SELECT tipcaj FROM glb_permxtipcaj WHERE roleid = ", lroleid, ")",
               " ORDER BY 2 "
               
 CALL librut002_combobox("tipcaj",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los vehiculos mostrando descripcion y placa

FUNCTION librut003_CbxVehiculos()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codveh,a.desveh||' ('||TRIM(a.numpla)||')' ",
               " FROM glb_vehiculs a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codveh",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los vehiculos mostrando solo placas

FUNCTION librut003_CbxVehiculosPlacas()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codveh,a.numpla ",
               " FROM glb_vehiculs a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codveh",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los vehiculos mostrando placa y descripcion

FUNCTION librut003_CbxVehiculosPlacasDescripcion()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codveh,a.numpla||' ('||TRIM(a.desveh)||')' ",
               " FROM glb_vehiculs a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codveh",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de combustible para ordenes
-- de combustible

FUNCTION librut003_CbxTiposCombustible() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.tipcmb,a.descmb ",
               " FROM veh_tipocmbt a ",
               " ORDER BY 2 "

 CALL librut002_combobox("tipcmb",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las rutas

FUNCTION librut003_CbxRutas() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.noruta,a.nomrut ",
               " FROM vta_rutasmer a ",
               " ORDER BY 2 "

 CALL librut002_combobox("noruta",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las rutas x punto de venta

FUNCTION librut003_CbxRutasxPos(wnumpos,ventas)
 DEFINE wnumpos   LIKE vta_rutaxpos.numpos,
        ventas    SMALLINT,
        strventas STRING

 -- Llenando combobox
 LET qrytext = "SELECT a.noruta,a.nomrut ",
               " FROM vta_rutasmer a ",
               " WHERE EXISTS (SELECT x.noruta FROM vta_rutaxpos x ",
                               "WHERE x.noruta = a.noruta ",
                                 "AND x.numpos = ",wnumpos,") ",
               " ORDER BY 2 "

 CALL librut002_combobox("noruta",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las marcas 

FUNCTION librut003_CbxMarcas()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codmar,a.nommar ",
               " FROM inv_marcapro a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codmar",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los centros de costo

FUNCTION librut003_CbxCentroscost()
 -- Llenando combobox
 LET qrytext = "SELECT a.codcos,a.nomcen ",
               " FROM glb_cencosto a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codcos",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los centros de costo 
FUNCTION librut003_CbxCentrosCosto(campoforma) 
 DEFINE campoforma CHAR(10) 

 -- Llenando combobox
 LET qrytext = "SELECT a.codcos,a.nomcen ",
               " FROM glb_cencosto a ",
               " ORDER BY 2 "

 CALL librut002_combobox(campoforma,qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los centros de costo 
FUNCTION librut003_CbxCentrosCosto2(campoforma, cuserid) 
 DEFINE campoforma CHAR(10) 
 DEFINE cuserid  LIKE glb_usuarioch.userid

 -- Llenando combobox
 LET qrytext = "SELECT a.codcos,b.nomcen ",
               " FROM glb_usuarioch a, glb_cencosto b  ",
               " WHERE a.userid = '", cuserid CLIPPED, "'",
               " AND   b.codcos = a.codcos",
               " ORDER BY 2 "
 CALL librut002_combobox(campoforma,qrytext)
END FUNCTION


-- Subrutina que carga el combobox de los centros de costo 

FUNCTION librut003_DCbxCentrosCosto(campoforma) 
 DEFINE campoforma,dato CHAR(10) 

 -- Llenando combobox
 LET qrytext = "SELECT a.codcos,a.nomcen ",
               " FROM glb_cencosto a ",
               " ORDER BY 2 "

 LET dato = librut002_comboboxdin(campoforma,qrytext)
 RETURN dato
END FUNCTION

-- Subrutina que carga el combobox de los centros de costo x usuario

FUNCTION librut003_CbxCentrosCostoXUsuario(campoforma)
 DEFINE campoforma CHAR(10)

 -- Llenando combobox
 LET qrytext = 
  "SELECT a.codcos,a.nomcen ",
   " FROM glb_cencosto a ",
   " WHERE exists (SELECT x.cencos FROM fac_permxcct x ",
                  " WHERE x.cencos = a.codcos ",
                  "   AND x.userid = USER) ",
   "   AND a.haycch = 1 ",
   " ORDER BY 2 "

 CALL librut002_combobox(campoforma,qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los autorizantes

FUNCTION librut003_CbxAutorizantes()
 -- Llenando combobox
 LET qrytext = "SELECT a.codaut,a.nomaut ",
               " FROM glb_userauth a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codaut",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los autorizantes

FUNCTION librut003_DCbxAutorizantes()
 DEFINE dato CHAR(10)

 -- Llenando combobox
 LET qrytext = "SELECT a.codaut,a.nomaut ",
               " FROM glb_userauth a ",
               " ORDER BY 2 "

 LET dato = librut002_comboboxdin("codaut",qrytext)
 RETURN dato 
END FUNCTION

-- Subrutina que carga el combobox de las listas de precio 

FUNCTION librut003_CbxListasPrecio()
 -- Llenando combobox 
 LET qrytext = "SELECT a.numlis,a.numlis||' - '||TRIM(a.nomlis)",
               " FROM vta_lispreen a ",
               " ORDER BY 2 "

 CALL librut002_combobox("numlis",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las compras pendientes de pagar x empresa 
-- y socio de negocios 

FUNCTION librut003_CbxComprasPendientes(wcodemp,wcodsoc)
 DEFINE wcodemp LIKE cmp_mtransac.codemp,
        wcodsoc LIKE cmp_mtransac.codsoc 

 -- Llenando combobox 
 LET qrytext = 
    "SELECT a.lnkcmp,a.fecemi||'   '||a.numdoc[1,20]||'   '||a.totdoc ",
      "FROM cmp_mtransac a,glb_sociosng p ",
      "WHERE a.codemp = ",wcodemp,
        "AND a.codsoc = ",wcodsoc,
        "AND a.estado = 1 ",
        "AND p.codsoc = a.codsoc ",
        "ORDER BY 1 "

 CALL librut002_combobox("lnkcmp",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las compras pagadas

FUNCTION librut003_CbxComprasPagadas(wcodemp,wcodsoc) 
 DEFINE wcodemp LIKE cmp_mtransac.codemp,
        wcodsoc LIKE cmp_mtransac.codsoc 

 -- Llenando combobox 
 LET qrytext = 
    "SELECT a.lnkcmp,a.fecemi||'   '||a.numdoc[1,20]||'   '||a.totdoc ",
      "FROM cmp_mtransac a,glb_sociosng p ",
      "WHERE a.codemp = ",wcodemp,
        "AND a.codsoc = ",wcodsoc,
        "AND a.estado IS NOT NULL ",
        "AND p.codsoc = a.codsoc ",
        "ORDER BY 1 "

 CALL librut002_combobox("lnkcmp",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los clientes con creditos 

FUNCTION librut003_CbxClientesCredito()
 -- Llenando combobox 
 LET qrytext = "SELECT UNIQUE a.codsoc,TRIM(a.nomsoc)||' ('||a.codsoc||')' ",
               " FROM vis_balanceapa a ",
               " WHERE a.salact >0 ",
               " ORDER BY 2 "

 CALL librut002_combobox("codsoc",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los descuentos 

FUNCTION librut003_cbxdescuentos(opcion)
 DEFINE opcion INT

 -- Llenando combobox
 CASE (opcion)
  WHEN 1 LET qrytext =
             "SELECT a.lnkdes,a.nomdes||' [ '||a.pordes||'% ]' ",
              " FROM fac_mdesctos a ",
              " WHERE a.pordes >0 ", 
              " ORDER BY 1 "
  WHEN 2 LET qrytext =
             "SELECT a.lnkdes,a.nomdes||' [ '||a.pordes||'% ]' ",
              " FROM fac_mdesctos a ",
              " ORDER BY 1 "
 END CASE 

 CALL librut002_combobox("lnkdes",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los departamentos por empresa

FUNCTION librut003_CbxDepartamentosEmpresa() 
 -- Llenando combobox
 LET qrytext = "SELECT a.coddep,a.nomdep ",
               " FROM  glb_departos a ", 
               " ORDER BY 2 "

 CALL librut002_combobox("coddep",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de diligencias 

FUNCTION librut003_CbxTiposDiligencias()              
 -- Llenando combobox
 LET qrytext = "SELECT a.tipdlg,a.nomdlg ",
               " FROM  glb_tiposdlg a ", 
               " ORDER BY 2 "

 CALL librut002_combobox("tipdlg",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los socios de negocio 

FUNCTION librut003_CbxProveedores()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codsoc,a.nomsoc ",
               " FROM glb_sociosng a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codsoc",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los socios de negocio con pagos 
-- pendientes por compras

FUNCTION librut003_CbxSociosCompras()
 -- Llenando combobox 
 LET qrytext = "SELECT UNIQUE a.codsoc,a.nomsoc ",
               " FROM glb_sociosng a ",
               " WHERE EXISTS (SELECT x.codsoc FROM cmp_mtransac x ",
                               "WHERE x.codsoc = a.codsoc ",
                                 "AND x.estado = 1)",
               " ORDER BY 2 "

 CALL librut002_combobox("codsoc",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los socios de negocio de combustible

FUNCTION librut003_cbxsocioscombustible()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codsoc,a.nomsoc ",
               " FROM glb_sociosng a ",
               " WHERE a.codalt = '1' ",
               " ORDER BY 2 "

 CALL librut002_combobox("codsoc",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los origenes

FUNCTION librut003_cbxOrigenesMovtos()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codsoc,a.nomsoc ",
               " FROM glb_sociosng a,glb_gruprovs g ",
               " WHERE g.codgru = a.codgru ", 
                  "AND g.afecmp = 1 ", 
               " ORDER BY 2 "

 CALL librut002_combobox("codori",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los responsables

FUNCTION librut003_CbxResponsables()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codres,a.nomres ",
               " FROM glb_responsb a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codres",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los rubros de gastos 

FUNCTION librut003_CbxRubrosGasto()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codrub,a.nomrub ",
               " FROM fac_rubgasto a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codrub",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de establecimientos sat  

FUNCTION librut003_CbxEstablecimientosSAT() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.numest,a.numest||'-'||TRIM(a.nomest) ",
               " FROM glb_estabsat a ",
               " ORDER BY 2 "

 CALL librut002_combobox("numest",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los rangos de peso de productos - pedidos 

FUNCTION librut003_CbxRangosPeso()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codran,a.desran ",
               " FROM inv_rangpeso a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codran",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los especificaciones de productos - pedidos 

FUNCTION librut003_CbxEspecificacionesProducto() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.codesp,a.desesp ",
               " FROM inv_especpro a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codesp",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las presentaciones de productos - pedidos

FUNCTION librut003_CbxPresentacionesProducto() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.codpre,a.despre ",
               " FROM inv_presnpro a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codpre",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las actividades 

FUNCTION librut003_CbxActividades() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.codact,a.desact ",
               " FROM glb_activids a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codact",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las undiades de medida 

FUNCTION librut003_cbxunidadesmedida(campoforma)
 DEFINE campoforma VARCHAR(15)

 -- Llenando combobox 
 LET qrytext = "SELECT a.unimed,a.nommed ",
               " FROM inv_unimedid a ",
               " ORDER BY 2 "

 CALL librut002_combobox(campoforma,qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las subcategorias 

FUNCTION librut003_cbxsubcategorias(wcodcat) 
 DEFINE wcodcat LIKE glb_subcateg.codcat 

 -- Llenando combobox 
 LET qrytext = "SELECT a.subcat,a.nomsub ",
               " FROM  glb_subcateg a ",
               " WHERE a.codcat = ",wcodcat,
               " ORDER BY 2 "

 CALL librut002_combobox("subcat",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las subcategorias sin categoria 

FUNCTION librut003_cbxsubcateg() 
 DEFINE wcodcat LIKE glb_subcateg.codcat 

 -- Llenando combobox 
 LET qrytext = "SELECT a.subcat,a.nomsub ",
               " FROM  glb_subcateg a ",
               " WHERE a.lonmed = 1 ",
               " ORDER BY 2 "

 CALL librut002_combobox("subcat",qrytext)
END FUNCTION

-- Subrutina que carga el combobox los perfiles

FUNCTION librut003_cbxperfiles()
 -- Llenando combobox
 LET qrytext = "SELECT a.roleid,a.nmrole ",
               " FROM glb_rolesusr a ",
               " ORDER BY 2 "

 CALL librut002_combobox("roleid",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de saldos 

FUNCTION librut003_cbxtipossaldo() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.tipsld,a.destip ",
               " FROM  inv_mtiposal a ",
               " WHERE haymov = 1 ",
               " ORDER BY 2 "

 CALL librut002_combobox("tipsld",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las cuentas bancarias

FUNCTION librut003_CbxCuentasBanco()
 -- Llenando combobox
 LET qrytext = 
   "SELECT x.numcta,rpad(x.numcor,2,' ')||' - '||",
          "TRIM(x.numcta)||' - '||a.nombco||' - '||x.nomcta ",
   " FROM bco_mcuentas x,glb_mtbancos a ",
   " WHERE a.codbco = x.codbco ",
   " ORDER BY x.numcor "

 CALL librut002_combobox("numcta",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de otra cuentas bancarias

FUNCTION librut003_CbxOtrasCuentasBanco()
 -- Llenando combobox
 LET qrytext = 
   "SELECT x.numcta,rpad(x.numcor,2,' ')||' - '||",
          "TRIM(x.numcta)||' - '||a.nombco||' - '||x.nomcta ",
   " FROM bco_mcuentas x,glb_mtbancos a ",
   " WHERE a.codbco = x.codbco ",
   " ORDER BY x.numcor "

 CALL librut002_combobox("ctaref",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las cuentas contables x empresa

FUNCTION librut003_CbxCuentasContablesEmpresa(wcodemp,wcampo) 
 DEFINE wcodemp LIKE ctb_mcuentas.codemp,
        wcampo  CHAR(20) 

 -- Llenando combobox
 LET qrytext = "SELECT x.numcta,TRIM(x.numcta)||' - '||x.nomcta ",
               " FROM ctb_mcuentas x ",
               " WHERE x.codemp = ",wcodemp, 
               "   AND x.tipcta = 'D' ", 
               " ORDER BY 1"

 CALL librut002_combobox(wcampo,qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las cuentas contables x empresa

FUNCTION librut003_CbxCuentasContablesEmpresaDos(wcodemp,wcampo)
 DEFINE wcodemp LIKE ctb_mcuentas.codemp,
        wcampo  CHAR(20)

 -- Llenando combobox
 LET qrytext = "SELECT x.numcta,TRIM(x.numcta)||' - '||x.nomcta ",
               " FROM ctb_mcuentas x ",
               " WHERE x.codemp = ",wcodemp,
               "   AND x.tipcta = 'D' ",
               " ORDER BY 1"

 CALL librut002_combobox(wcampo,qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las cuentas bancarias con emision de cheques vouchers 

FUNCTION librut003_CbxCuentasBancoVouchers(wcodemp)
 DEFINE wcodemp LIKE bco_mcuentas.codemp 

 -- Llenando combobox
 LET qrytext = 
   "SELECT x.numcta,rpad(x.numcor,2,' ')||' - '||",
          "TRIM(x.numcta)||' - '||a.nombco||' - '||x.nomcta ",
   " FROM  bco_mcuentas x,glb_mtbancos a ",
   " WHERE x.codemp = ",wcodemp,
   "   AND a.codbco = x.codbco ",
   " ORDER BY x.numcor "

 CALL librut002_combobox("numcta",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de productos 

FUNCTION librut003_cbxproductosabr() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.cditem,a.codabr ",
               " FROM  inv_products a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codabr",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de descripciones de producto 

FUNCTION librut003_cbxproductosdes(wcodcat,wsubcat)
 DEFINE wcodcat   LIKE inv_products.codcat,
        wsubcat   LIKE inv_products.subcat,
        strcodcat STRING, 
        strsubcat STRING

 -- Verificando categoria
 LET strcodcat = NULL
 IF wcodcat IS NOT NULL THEN
    LET strcodcat = " AND a.codcat = ",wcodcat
 END IF

 -- Verificando subcategoria
 LET strsubcat = NULL
 IF wsubcat IS NOT NULL THEN
    LET strsubcat = " AND a.subcat = ",wsubcat
 END IF

 -- Llenando combobox
 LET qrytext = "SELECT a.cditem,trim(a.dsitem)||' ('||trim(a.codabr)||')' ",
               " FROM  inv_products a ",
               " WHERE a. estado = 1 ",
               strcodcat CLIPPED,
               strsubcat CLIPPED,
               " ORDER BY 2 "

 CALL librut002_combobox("cditem",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de productos que estan marcados como pesaje en bascula 

FUNCTION librut003_CbxProductosPesaje()
 -- Llenando combobox
 LET qrytext = "SELECT a.cditem,trim(a.dsitem)||' ('||trim(a.codabr)||')' ",
               " FROM  inv_products a ",
               " WHERE a.pesado = 1 ",
               " ORDER BY 2 "

 CALL librut002_combobox("cditem",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de bodegas

FUNCTION librut003_CbxBodegass(wcamfor)
DEFINE wcamfor STRING
 -- Llenando combobox
 LET qrytext = "SELECT a.codbod,a.nombod ",
               " FROM inv_mbodegas a ",
               " ORDER BY 2 "

 CALL librut002_combobox(wcamfor,qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las bodegas x usuario 

FUNCTION librut003_cbxbodegasxusuario(wuserid) 
 DEFINE wuserid LIKE inv_permxbod.userid 

 -- Llenando combobox 
 LET qrytext = "SELECT a.codbod,a.nombod ",
               " FROM  inv_mbodegas a ",
               " WHERE EXISTS (SELECT b.userid FROM inv_permxbod b ",
                               "WHERE b.codbod = a.codbod ",
                                " AND b.userid = '",wuserid CLIPPED,"')",
               " ORDER BY 2 "

 CALL librut002_combobox("codbod",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las bodegas 

FUNCTION librut003_cbxbodegas(whayfac)
 DEFINE whayfac SMALLINT,
        wstrfac STRING

 -- Verificando condicion de facturacion
 LET wstrfac = NULL
 CASE (whayfac)
  WHEN 0 LET wstrfac = "WHERE a.hayfac = 0"
  WHEN 1 LET wstrfac = "WHERE a.hayfac = 1"
  WHEN 2 LET wstrfac = NULL
 END CASE 

 -- Llenando combobox
 LET qrytext = "SELECT a.codbod,a.nombod ",
               " FROM  inv_mbodegas a ",
               wstrfac CLIPPED,
               " ORDER BY 2 "

 CALL librut002_combobox("codbod",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los paises 

FUNCTION librut003_cbxpaises()
 -- Llenando combobox
 LET qrytext = "SELECT a.codpai,nompai ",
               " FROM  glb_mtpaises a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codpai",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de movimientos de inventario x usuario 

FUNCTION librut003_cbxtipomovxusuario(wuserid) 
 DEFINE wuserid LIKE inv_permxtmv.userid 

 -- Llenando combobox 
 LET qrytext = "SELECT a.tipmov,a.nommov ",
               " FROM  inv_tipomovs a ",
               " WHERE EXISTS (SELECT b.userid FROM inv_permxtmv b ",
                               "WHERE b.tipmov = a.tipmov ",
                                " AND b.userid = '",wuserid CLIPPED,"')",
               " ORDER BY 2 "

 CALL librut002_combobox("tipmov",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de movimientos de banco x usuario 

FUNCTION librut003_CbxTipoMovBcoxUsuario(wtipmov,wuserid) 
 DEFINE wuserid LIKE inv_permxtmv.userid,
        wtipmov LIKE bco_tipomovs.tipmov

 -- Llenando combobox
 LET qrytext = "SELECT a.tipmov,a.nommov||' - '||",
                      "case (a.tipope) when 1 then 'CARGO' when 0 then 'ABONO' end",
               " FROM bco_tipomovs a ",
               " WHERE a.tipmov >=",wtipmov,
               "   AND EXISTS (SELECT b.userid FROM bco_permxtmv b ",
                               "WHERE b.tipmov = a.tipmov ",
                                " AND b.userid = '",wuserid CLIPPED,"')",
               " ORDER BY 2 "

 CALL librut002_combobox("tipmov",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de movimientos 

FUNCTION librut003_CbxTiposMovimiento() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.tipmov,a.nommov ",
               " FROM  inv_tipomovs a ",
               " ORDER BY 2 "

 CALL librut002_combobox("tipmov",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los grupos de tipos de movimientos 

FUNCTION librut003_CbxGruposTiposMovimiento() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.codgru,a.nomgru ",
               " FROM  inv_grupomov a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codgru",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los grupos de cuentas contables

FUNCTION librut003_CbxGruposCuentasContables() 
 -- Llenando combobox
 LET qrytext = "SELECT a.codgru,a.nomgru ",
               " FROM  ctb_grupocta a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codgru",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los empaques x producto 

FUNCTION librut003_cbxempaques(wcditem,filtro)
 DEFINE wcditem LIKE inv_epqsxpro.cditem,
        filtro  SMALLINT

 -- Llenando combobox
 IF filtro THEN 
    LET qrytext = "SELECT a.codepq,a.nomepq ",
                  " FROM  inv_epqsxpro a ",
                  " WHERE a.cditem = ",wcditem, 
                  " ORDER BY 2 "
 ELSE
    LET qrytext = "SELECT a.codepq,a.nomepq ",
                  " FROM  inv_epqsxpro a ",
                  " ORDER BY 2 "
 END IF 

 CALL librut002_combobox("codepq",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los vendedores. Vendedores activos unicamente

FUNCTION librut003_cbxvendedores()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codven,a.nomven ",
               " FROM glb_vendedrs a ",
               " WHERE a.estado = 1 ", 
               " ORDER BY 2 "

 CALL librut002_combobox("codven",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los segmentos. Segmentos activos unicamente

FUNCTION librut003_CbxSegmentos()
 -- Llenando combobox
 LET qrytext = "SELECT a.codseg,a.nomseg ",
               " FROM vta_segmntos a ",
               " WHERE a.estado = 1 ",
               " ORDER BY 2 "

 CALL librut002_combobox("codseg",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los destinos del producto

FUNCTION librut003_CbxDestinos()
 -- Llenando combobox 
 LET qrytext = "SELECT a.tipdes,a.nomdes ",
               " FROM inv_destipro a ",
               " ORDER BY 2 "

 CALL librut002_combobox("tipdes",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los puntos de venta 

FUNCTION librut003_cbxpuntosventa()
 -- Llenando combobox 
 LET qrytext = "SELECT a.numpos,a.nompos ",
               " FROM fac_puntovta a ",
               " ORDER BY 2 "

 CALL librut002_combobox("numpos",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los puntos de venta x usuario

FUNCTION librut003_CbxPuntoVentaXUsuario(userid) 
 DEFINE userid LIKE fac_permxpos.userid 

 -- Llenando combobox
 LET qrytext = "SELECT a.numpos,a.nompos ",
               " FROM fac_puntovta a ",
               " WHERE exists (SELECT x.numpos FROM fac_permxpos x ",
                              " WHERE x.numpos = a.numpos ",
                              "   AND x.userid = '",userid CLIPPED,"')",
               " ORDER BY 2 "

 CALL librut002_combobox("numpos",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de nomenclatura contable 

FUNCTION librut003_CbxTiposNomenclaturaContable() 
 DEFINE wcodemp LIKE glb_empresas.codemp 

 -- Llenando combobox
 LET qrytext = "SELECT a.tipnom,TRIM(a.desnom)||' '||TRIM(a.format) ",
               " FROM ctb_tiposnom a ",
               " ORDER BY 1 "

 CALL librut002_combobox("tipnom",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las fases de una orden de trabajo

FUNCTION librut003_cbxfasesot()
 -- Llenando combobox 
 LET qrytext = "SELECT a.tippar,a.valchr ",
               " FROM glb_paramtrs a ",
               " WHERE a.numpar = 100 ", 
               " ORDER BY 1 "

 -- Llenando combobox 
 CALL librut002_combobox("nofase",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los cajeros, x puntos de venta, empresa y fecha de corte

FUNCTION librut003_cbxcajeroscorte(wnumpos,wcodemp,ts)
 DEFINE wnumpos      LIKE fac_mtransac.numpos,
        wcodemp      LIKE fac_mtransac.codemp,
        numreg       SMALLINT,
        ts           SMALLINT,
        strfeccorte  STRING

 -- Condicion de corte
 CASE (ts)
  WHEN 0 LET strfeccorte = " AND a.feccor IS NOT NULL " 
  WHEN 1 LET strfeccorte = " AND a.feccor IS NULL " 
  WHEN 2 LET strfeccorte = NULL 
 END CASE 

 -- Llenando combobox 
 LET qrytext = "SELECT UNIQUE a.userid,b.nomusr ",
               " FROM  fac_mtransac a,glb_usuarios b ",
               " WHERE a.numpos = ",wnumpos,
                 " AND a.codemp = ",wcodemp,
                 strfeccorte CLIPPED,
                 " AND b.userid = a.userid ",
               " ORDER BY 2 "

 LET numreg = librut002_cbxdin("cajero",qrytext)
 RETURN numreg
END FUNCTION

-- Subrutina que carga el combobox de rutas, x puntos de venta, empresa y fecha de corte

FUNCTION librut003_CbxRutasCorte(wnumpos,wcodemp,ts)
 DEFINE wnumpos      LIKE fac_mtransac.numpos,
        wcodemp      LIKE fac_mtransac.codemp,
        numreg       SMALLINT,
        ts           SMALLINT,
        strfeccor    STRING,
        strcodemp    STRING

 -- Condicion de corte
 CASE (ts)
  WHEN 0 LET strfeccor = " AND a.feccor IS NOT NULL " 
  WHEN 1 LET strfeccor = " AND a.feccor IS NULL " 
  WHEN 2 LET strfeccor = NULL 
 END CASE 

 -- Condicion de empresa
 LET strcodemp = NULL 
 IF wcodemp IS NOT NULL THEN 
    LET strcodemp = " AND a.codemp = ",wcodemp
 END IF

 -- Llenando combobox 
 LET qrytext = "SELECT UNIQUE a.noruta,b.nomrut ",
               " FROM  fac_mtransac a,vta_rutasmer b ",
               " WHERE a.numpos = ",wnumpos,
               strcodemp CLIPPED,
               strfeccor CLIPPED,
               " AND b.noruta = a.noruta ",
               " ORDER BY 2 "

 LET numreg = librut002_cbxdin("noruta",qrytext)
 RETURN numreg
END FUNCTION

-- Subrutina que carga el combobox de los cajeros con corte de ventas

FUNCTION librut003_cbxcajeros()
 DEFINE numreg SMALLINT 

 -- Llenando combobox
 LET qrytext = "SELECT UNIQUE a.userid,b.nomusr ",
               " FROM  fac_vicortes a,glb_usuarios b ",
               " WHERE b.userid = a.userid ",
               " ORDER BY 2 "

 LET numreg = librut002_cbxdin("cajero",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los puntos de venta x cajero 

FUNCTION librut003_cbxpuntosventaxcajero(wuserid)
 DEFINE wuserid LIKE fac_usuaxpos.userid

 -- Llenando combobox 
 LET qrytext = "SELECT a.numpos,a.nompos ",
               " FROM fac_puntovta a,fac_usuaxpos b ",
               " WHERE a.numpos = b.numpos ",
                 " AND b.userid = '",wuserid CLIPPED,"'",
               " ORDER BY 2 "

 CALL librut002_combobox("numpos",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de documento 

FUNCTION librut003_cbxtiposdocumento(wtipqry)
 DEFINE wtipqry SMALLINT,
        wwhere  STRING 
 
 -- Llenando combobox 
 IF wtipqry=100 THEN 
  LET qrytext = "SELECT a.tipdoc,a.nomdoc ",
                " FROM  fac_tipodocs a ",
                " WHERE a.tipdoc = 1 ", 
                " ORDER BY 2 "
 ELSE
  LET qrytext = "SELECT a.tipdoc,a.nomdoc ",
                " FROM fac_tipodocs a ",
                " ORDER BY 2 "
 END IF 

 CALL librut002_combobox("tipdoc",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los bancos o entidades financieras

FUNCTION librut003_CbxBancos()
 DEFINE wwhere STRING

 -- Llenando combobox
 LET qrytext = "SELECT a.codbco,a.nombco ",
               " FROM glb_mtbancos a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codbco",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de documento por punto de venta 
-- Solamente los que estan en estado activo 

FUNCTION librut003_cbxtiposdocxpos(wnumpos)
 DEFINE wnumpos LIKE fac_tdocxpos.numpos
 
 -- Llenando combobox 
 LET qrytext = "SELECT a.lnktdc,a.nomdoc ",
                "FROM  vis_tipdocxpos a ",
                "WHERE a.numpos = ",wnumpos,
                "  AND exists (SELECT x.lnktdc FROM fac_permxtdc x ",
                              " WHERE x.lnktdc = a.lnktdc ",
                              "   AND x.userid = USER) ",
                " ORDER BY 2"

 CALL librut002_combobox("lnktdc",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de documento con impuesto por punto de venta

FUNCTION librut003_cbxtiposdocxposimp(wnumpos)
 DEFINE wnumpos LIKE fac_tdocxpos.numpos
 
 -- Llenando combobox 
 LET qrytext = "SELECT a.lnktdc,a.nomdoc ",
                "FROM  vis_tipdocxpos a ",
                "WHERE a.numpos = ",wnumpos,
                " AND  a.hayimp = 1", 
                " ORDER BY 2"

 CALL librut002_combobox("lnktdc",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de documento por punto de venta 
-- Todos los tipos de documento 

FUNCTION librut003_cbxtiposdocxpostodos(wnumpos)
 DEFINE wnumpos LIKE fac_tdocxpos.numpos
 
 -- Llenando combobox 
 LET qrytext = "SELECT a.lnktdc,a.nomdoc ",
                "FROM  vis_tipdocxpostodos a ",
                "WHERE a.numpos = ",wnumpos,
                " ORDER BY 2"

 CALL librut002_combobox("lnktdc",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de las taras

FUNCTION librut003_CbxTaras()
 -- Llenando combobox
 LET qrytext = "SELECT a.codtar,TRIM(a.destar)||' ('||a.canpes||')'",
               " FROM  glb_pesotara a ",
               " ORDER BY 1 "

 CALL librut002_combobox("codtar",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de movimiento de banco

FUNCTION librut003_CbxTiposMovimientoBco(wtipmov) 
 DEFINE wtipmov LIKE bco_tipomovs.tipmov

 -- Llenando combobox 
 LET qrytext = "SELECT a.tipmov,a.nommov||' - '||",
                      "case (a.tipope) when 1 then 'CARGO' when 0 then 'ABONO' end",
               " FROM bco_tipomovs a ",
               " WHERE a.tipmov >=",wtipmov,
               " ORDER BY 2 "

 CALL librut002_combobox("tipmov",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de documento del regimen fiscal 

FUNCTION librut003_CbxTiposDocRegimenFis(wcodgru) 
 DEFINE wcodgru LIKE glb_dregmfis.codgru

 -- Llenando combobox 
 LET qrytext = "SELECT a.tipdoc,a.tipdoc ", 
               " FROM  glb_dregmfis a ",
               " WHERE a.codgru =",wcodgru, 
               " ORDER BY 2 "

 CALL librut002_combobox("tipdoc",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de documento del regimen fiscal TODOS 

FUNCTION librut003_CbxTiposDocRegimenFisTodos() 
 -- Llenando combobox 
 LET qrytext = "SELECT UNIQUE a.tipdoc,a.tipdoc ", 
               " FROM  glb_dregmfis a ",
               " ORDER BY 2 "

 CALL librut002_combobox("tipdoc",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los tipos de movimiento de pagos
-- de socios de negocio

FUNCTION librut003_DCbxTiposMovimientoPagoBco()
 DEFINE dato CHAR(10) 

 -- Llenando combobox
 LET qrytext="SELECT a.tipmov,a.nommov||' - '||",
                    "case (a.tipope) when 1 then 'CARGO' when 0 then 'ABONO' end",
             " FROM bco_tipomovs a ",
             " WHERE a.pagpro = 1 ", 
             " ORDER BY 2 "

 LET dato = librut002_comboboxdin("tipmov",qrytext)
 RETURN dato 
END FUNCTION

-- Subrutina que carga el combobox de los tipos de movimiento de compras 

FUNCTION librut003_DCbxTiposMovimientoCompras()
 DEFINE dato CHAR(10) 

 -- Llenando combobox
 LET qrytext = "SELECT a.tipmov,a.nommov||' - '||",
                      "case (a.tipope) when 1 then 'CARGO' when 0 then 'ABONO' end",
               " FROM cmp_tipomovs a ",
               " ORDER BY 2 "

 LET dato = librut002_comboboxdin("tipmov",qrytext)
 RETURN dato 
END FUNCTION

-- Subrutina que carga el combobox de los tipos de documento de ventas   

FUNCTION librut003_DCbxTiposDocumentoVentas()
 DEFINE dato CHAR(10) 

 -- Llenando combobox
 LET qrytext = "SELECT a.tipdoc,a.nomdoc||' - '||",
                      "case (a.tipope) when 1 then 'CARGO' when 0 then 'ABONO' end",
               " FROM vta_tipodocs a ",
               " ORDER BY 2 "

 LET dato = librut002_comboboxdin("tipdoc",qrytext)
 RETURN dato 
END FUNCTION

-- Subrutina que llena el combobox de anios 

FUNCTION librut003_cbxanio(wannioc)
 DEFINE cba       ui.ComboBox,
        wannioc,i INT

 -- Asignando anio presente y el anterior
 LET cba = ui.ComboBox.forname("aniotr")
 CALL cba.clear()

 -- Verificando si anio tiene valor
 IF (wannioc=0) THEN
    LET wannioc = YEAR(CURRENT)-1
    FOR i=1 TO 2
     CALL cba.addItem(wannioc,wannioc)
     LET wannioc = (wannioc+1)
    END FOR
 ELSE
    CALL cba.addItem(wannioc,wannioc)
 END IF
END FUNCTION

-- Subrutina que carga el combobox de las fechas de inventario fisico 

FUNCTION librut003_cbxfecfisico(wcodemp,wcodbod)
 DEFINE wcodemp LIKE inv_tofisico.codemp,
        wcodbod LIKE inv_tofisico.codbod

 -- Llenando combobox
 LET qrytext = "SELECT UNIQUE a.fecinv,a.fecinv ",
               " FROM  inv_tofisico a ",
               " WHERE a.codemp = ",wcodemp,
                 " AND a.codbod = ",wcodbod,
               " ORDER BY 1 DESC"

 CALL librut002_combobox("fecinv",qrytext)
END FUNCTION

-- Subrutina para buscar los datos de un tipo de movimiento (inv_tipomovs) 

FUNCTION librut003_btipomovimiento(wtipmov)
 DEFINE w_tip_mov RECORD LIKE inv_tipomovs.*,
        wtipmov   LIKE inv_tipomovs.tipmov

 INITIALIZE w_tip_mov.* TO NULL 
 SELECT a.*
  INTO  w_tip_mov.*
  FROM  inv_tipomovs a
  WHERE (a.tipmov = wtipmov)
  IF (status=NOTFOUND) THEN
     RETURN w_tip_mov.*,FALSE
  ELSE
     RETURN w_tip_mov.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una empresa (glb_empresas) 

FUNCTION librut003_bempresa(wcodemp)
 DEFINE w_mae_emp RECORD LIKE glb_empresas.*,
        wcodemp   LIKE glb_empresas.codemp

 INITIALIZE w_mae_emp.* TO NULL 
 SELECT a.*
  INTO  w_mae_emp.*
  FROM  glb_empresas a
  WHERE (a.codemp = wcodemp)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_emp.*,FALSE
  ELSE
     RETURN w_mae_emp.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una bodega (inv_mbodegas) 

FUNCTION librut003_bbodega(wcodbod)
 DEFINE w_mae_bod RECORD LIKE inv_mbodegas.*,
        wcodbod   LIKE inv_mbodegas.codbod

 INITIALIZE w_mae_bod.* TO NULL 
 SELECT a.*
  INTO  w_mae_bod.*
  FROM  inv_mbodegas a
  WHERE (a.codbod = wcodbod)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_bod.*,FALSE
  ELSE
     RETURN w_mae_bod.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un producto (inv_products) 

FUNCTION librut003_BProducto(wcditem)
 DEFINE w_inv_pro RECORD LIKE inv_products.*,
        wcditem   LIKE inv_products.cditem

 INITIALIZE w_inv_pro.* TO NULL 
 SELECT a.*
  INTO  w_inv_pro.*
  FROM  inv_products a
  WHERE (a.cditem = wcditem)
  IF (status=NOTFOUND) THEN
     RETURN w_inv_pro.*,FALSE
  ELSE
     RETURN w_inv_pro.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un producto x su codigo abreviado(inv_products) 

FUNCTION librut003_bproductoabr(wcodabr)
 DEFINE w_inv_pro RECORD LIKE inv_products.*,
        wcodabr   LIKE inv_products.codabr

 INITIALIZE w_inv_pro.* TO NULL 
 SELECT a.*
  INTO  w_inv_pro.*
  FROM  inv_products a
  WHERE (a.codabr= wcodabr)
  IF (status=NOTFOUND) THEN
     RETURN w_inv_pro.*,FALSE
  ELSE
     RETURN w_inv_pro.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un socio de negocio (glb_sociosng) 

FUNCTION librut003_BSociosNeg(wcodsoc)
 DEFINE w_mae_prv RECORD LIKE glb_sociosng.*,
        wcodsoc   LIKE glb_sociosng.codsoc

 INITIALIZE w_mae_prv.* TO NULL 
 SELECT a.*
  INTO  w_mae_prv.*
  FROM  glb_sociosng a
  WHERE (a.codsoc= wcodsoc)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_prv.*,FALSE
  ELSE
     RETURN w_mae_prv.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una unidad de medida (inv_unimedid) 

FUNCTION librut003_bumedida(wunimed)
 DEFINE w_mae_reg RECORD LIKE inv_unimedid.*,
        wunimed   LIKE inv_unimedid.unimed

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  inv_unimedid a
  WHERE (a.unimed= wunimed)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un peso tara (glb_pesotara) 

FUNCTION librut003_bpesotara(wcodtar)
 DEFINE w_mae_reg RECORD LIKE glb_pesotara.*,
        wcodtar   LIKE glb_pesotara.codtar

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  glb_pesotara a
  WHERE (a.codtar = wcodtar)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un movimiento (inv_mtransac) 

FUNCTION librut003_bmovimiento(wlnktra)
 DEFINE w_mae_tra RECORD LIKE inv_mtransac.*,
        wlnktra   LIKE inv_mtransac.lnktra

 INITIALIZE w_mae_tra.* TO NULL 
 SELECT a.*
  INTO  w_mae_tra.*
  FROM  inv_mtransac a
  WHERE (a.lnktra= wlnktra)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_tra.*,FALSE
  ELSE
     RETURN w_mae_tra.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un documento de facturacion (fac_mtransac)

FUNCTION librut003_bfacturacion(wlnktra)
 DEFINE w_mae_tra RECORD LIKE fac_mtransac.*,
        wlnktra   LIKE fac_mtransac.lnktra

 INITIALIZE w_mae_tra.* TO NULL
 SELECT a.*
  INTO  w_mae_tra.*
  FROM  fac_mtransac a
  WHERE (a.lnktra= wlnktra)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_tra.*,FALSE
  ELSE
     RETURN w_mae_tra.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una caja admon (fac_cajchica)

FUNCTION librut003_BCajaAdmon(wlnkcaj)
 DEFINE w_mae_tra RECORD LIKE fac_cajchica.*,
        wlnkcaj   LIKE fac_cajchica.lnkcaj

 INITIALIZE w_mae_tra.* TO NULL
 SELECT a.*
  INTO  w_mae_tra.*
  FROM  fac_cajchica a
  WHERE (a.lnkcaj= wlnkcaj)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_tra.*,FALSE
  ELSE
     RETURN w_mae_tra.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un punto de venta (fac_puntovta) 

FUNCTION librut003_bpuntovta(wnumpos)
 DEFINE w_mae_reg RECORD LIKE fac_puntovta.*,
        wnumpos   LIKE fac_puntovta.numpos

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  fac_puntovta a
  WHERE (a.numpos= wnumpos)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un usuario (glb_usuarios) 

FUNCTION librut003_busuario(wuserid)
 DEFINE w_mae_reg RECORD LIKE glb_usuarios.*,
        wuserid   LIKE glb_usuarios.userid

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  glb_usuarios a
  WHERE (a.userid= wuserid)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un tipo de movimiento de bancos(bco_tipomovs)

FUNCTION librut003_BTiposMovimientoBco(wtipmov)
 DEFINE w_mae_reg RECORD LIKE bco_tipomovs.*,
        wtipmov   LIKE bco_tipomovs.tipmov

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  bco_tipomovs a
  WHERE (a.tipmov= wtipmov)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un tipo de documento x punto de venta (fac_tdocxpos) 

FUNCTION librut003_btdocxpos(wlnktdc)
 DEFINE w_mae_reg RECORD LIKE fac_tdocxpos.*,
        wlnktdc   LIKE fac_tdocxpos.lnktdc

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  fac_tdocxpos a
  WHERE (a.lnktdc= wlnktdc)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un tipo de documento (fac_tipodocs) 

FUNCTION librut003_btipodoc(wtipdoc)
 DEFINE w_mae_reg RECORD LIKE fac_tipodocs.*,
        wtipdoc   LIKE fac_tipodocs.tipdoc

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  fac_tipodocs a
  WHERE (a.tipdoc= wtipdoc)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END function

-- Subrutina para buscar los datos de una orden de combustible (veh_mordenes)

FUNCTION librut003_BOrdenCombustible(wlnkord) 
 DEFINE w_mae_reg RECORD LIKE veh_mordenes.*, 
        wlnkord   LIKE veh_mordenes.lnkord  
      
 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  veh_mordenes a
  WHERE (a.lnkord= wlnkord)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una transcciones de banco (bco_mtransac)

FUNCTION librut003_BTransaccionBanco(wlnkbco) 
 DEFINE w_mae_reg RECORD LIKE bco_mtransac.*, 
        wlnkbco   LIKE bco_mtransac.lnkbco  
      
 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  bco_mtransac a
  WHERE (a.lnkbco= wlnkbco)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un movimiento de compra (cmp_mtransac)

FUNCTION librut003_BMovimientoCompras(wlnkcmp)
 DEFINE w_mae_reg RECORD LIKE cmp_mtransac.*,
        wlnkcmp   LIKE cmp_mtransac.lnkcmp 

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  cmp_mtransac a
  WHERE (a.lnkcmp= wlnkcmp)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un movimiento de ventas (vta_ldocutec)

FUNCTION librut003_BMovimientoVentas(wlnkvta)
 DEFINE w_mae_reg RECORD LIKE vta_ldocutec.*,
        wlnkvta   LIKE vta_ldocutec.lnkvta 

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  vta_ldocutec a
  WHERE (a.lnkvta= wlnkvta)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un tipo de movimiento de compra (cmp_tipomovs)

FUNCTION librut003_BTipoMovimientoCompras(wtipmov)
 DEFINE w_mae_reg RECORD LIKE cmp_tipomovs.*,
        wtipmov   LIKE cmp_mtransac.tipmov 

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  cmp_tipomovs a
  WHERE (a.tipmov= wtipmov)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una cuenta bancaria (bco_mcuentas) 

FUNCTION librut003_BCuentaBanco(wnumcta)
 DEFINE w_mae_reg RECORD LIKE bco_mcuentas.*,
        wnumcta   LIKE bco_mcuentas.numcta

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  bco_mcuentas a
  WHERE (a.numcta= wnumcta)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un empaque x producto

FUNCTION librut003_bempaquexpro(wcditem,wcodepq)            
 DEFINE w_mae_reg RECORD LIKE inv_epqsxpro.*,
        wcditem   LIKE inv_epqsxpro.cditem,
        wcodepq   LIKE inv_epqsxpro.codepq

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  inv_epqsxpro a
  WHERE (a.cditem = wcditem)
    AND (a.codepq = wcodepq)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para obtener parametros del sistema

FUNCTION librut003_parametros(wnumpar,wtippar)
 DEFINE wnumpar LIKE glb_paramtrs.numpar,
        wtippar LIKE glb_paramtrs.tippar,
        wvalchr LIKE glb_paramtrs.valchr

 -- Seleccionando parametros
 INITIALIZE wvalchr TO NULL
 SELECT a.valchr
  INTO  wvalchr
  FROM  glb_paramtrs a
  WHERE (a.numpar = wnumpar)
    AND (a.tippar = wtippar)
  IF (status!=NOTFOUND) THEN
     RETURN TRUE,wvalchr
  ELSE
     RETURN FALSE,wvalchr
  END IF
END FUNCTION

-- Subrutina para obtener el correlativo maximo de un documento por empresa serie y tipo documento de venta 

FUNCTION librut003_correltipdoc(wr)
 DEFINE wr RECORD
         codemp LIKE fac_mtransac.codemp,
         tipdoc LIKE fac_mtransac.tipdoc, 
         nserie LIKE fac_mtransac.nserie,
         correl INTEGER, 
         conteo SMALLINT 
        END RECORD

 -- Capturando errores
 WHENEVER ERROR CONTINUE
 WHILE TRUE
  -- Seleccionando el numero maximo
  SELECT MAX(i.numdoc)
   INTO  wr.correl
   FROM  fac_mtransac i
   WHERE (i.codemp = wr.codemp)
     AND (i.tipdoc = wr.tipdoc)
     AND (i.nserie = wr.nserie)
     AND (i.numdoc IS NOT NULL)
   IF wr.correl IS NULL THEN
      LET wr.correl = 1
   ELSE
      LET wr.correl = (wr.correl+1)
   END IF

  -- Verificando que correlativo maximo no exista
  SELECT COUNT(*)
   INTO  wr.conteo
   FROM  fac_mtransac
   WHERE (i.codemp = wr.codemp)
     AND (i.tipdoc = wr.tipdoc)
     AND (i.nserie = wr.nserie)
     AND (i.numdoc = wr.correl)
   IF (wr.conteo>0) THEN
      CONTINUE WHILE
   END IF
   EXIT WHILE
 END WHILE

 -- Retornando captura de errores
 WHENEVER ERROR STOP

 -- Retornando correlativo
 RETURN wr.correl
END FUNCTION

-- Subrutina para obtener los movimientos de un producto por empresa, 
-- bodega, anio, mes y producto

FUNCTION librut003_saldosxmovxmes(wcodemp,wcodbod,waniotr,wmestra,wcditem,wtipope)
 DEFINE wcodemp LIKE inv_dtransac.codemp, 
        wcodbod LIKE inv_dtransac.codbod, 
        waniotr LIKE inv_dtransac.aniotr, 
        wmestra LIKE inv_dtransac.mestra, 
        wcditem LIKE inv_dtransac.cditem, 
        wtipope LIKE inv_dtransac.tipope, 
        wsaluni LIKE inv_dtransac.canuni,
        wsalval LIKE inv_dtransac.totpro 

 -- Obteniendo saldos
 SELECT a.saluni,a.salval
  INTO  wsaluni,wsalval 
  FROM  vis_saldosxmovxmes a
  WHERE a.codemp = wcodemp
    AND a.codbod = wcodbod
    AND a.aniotr = waniotr
    AND a.mestra = wmestra
    AND a.cditem = wcditem 
    AND a.tipope = wtipope

 IF wsaluni IS NULL THEN LET wsaluni = 0 END IF 
 IF wsalval IS NULL THEN LET wsalval = 0 END IF 

 RETURN wsaluni,wsalval
END FUNCTION 

-- Subrutina para obtener el saldo fisico de un producto

FUNCTION librut003_fisicoxproxmes(wcodemp,wcodbod,waniotr,wmestra,wcditem)
 DEFINE wcodemp LIKE inv_tofisico.codemp, 
        wcodbod LIKE inv_tofisico.codbod, 
        waniotr LIKE inv_tofisico.aniotr, 
        wmestra LIKE inv_tofisico.mestra, 
        wcditem LIKE inv_tofisico.cditem, 
        wfisuni LIKE inv_tofisico.canuni,
        wfisval LIKE inv_tofisico.totpro,
        wfecinv DATE 
 
 -- Obteniendo saldos
 SELECT NVL(SUM(a.canuni),0),NVL(SUM(a.totpro),0)
  INTO  wfisuni,wfisval 
  FROM  inv_tofisico a
  WHERE a.codemp = wcodemp
    AND a.codbod = wcodbod
    AND a.aniotr = waniotr
    AND a.mestra = wmestra
    AND a.cditem = wcditem 
    AND a.finmes = 1

 IF wfisuni IS NULL THEN LET wfisuni = 0 END IF 
 IF wfisval IS NULL THEN LET wfisval = 0 END IF 

 RETURN wfisuni,wfisval
END FUNCTION 

-- Subrutina para chequear la existencia de un producto

FUNCTION librut003_chkexistencia(wcodemp,wcodbod,wcditem)
 DEFINE wcodemp LIKE inv_proenbod.codemp, 
        wcodbod LIKE inv_proenbod.codbod, 
        wcditem LIKE inv_proenbod.cditem, 
        wexican LIKE inv_proenbod.exican 

 -- Obteniendo existencia
 SELECT NVL(a.exican,0)
  INTO  wexican
  FROM  inv_proenbod a
  WHERE a.codemp = wcodemp
    AND a.codbod = wcodbod
    AND a.cditem = wcditem 

 IF (wexican IS NULL) THEN LET wexican = 0 END IF

 RETURN wexican 
END FUNCTION 

-- Subrutina para buscar el saldo anterior de un producto x empresa, 
-- bodega y fecha

FUNCTION librut003_saldoantxpro(wcodemp,wcodbod,wcditem,wfecini)
 DEFINE wcodemp LIKE inv_proenbod.codemp, 
        wcodbod LIKE inv_proenbod.codbod, 
        wcditem LIKE inv_proenbod.cditem, 
        wmovcan LIKE inv_proenbod.exican,
        wmovval LIKE inv_proenbod.exival,
        wfecini DATE,
        wfecfin DATE 

 -- Calculando saldo de movimientos antes de la fecha inicial
 SELECT NVL(SUM(a.opeuni),0),
        NVL(SUM(a.opeval),0)
  INTO  wmovcan,
        wmovval
  FROM  inv_dtransac a
  WHERE a.codemp  = wcodemp
    AND a.codbod  = wcodbod
    AND a.cditem  = wcditem
    AND a.fecemi  < wfecini
    AND a.estado  = "V"
    AND a.actexi  = 1 

 IF (wmovcan IS NULL) THEN LET wmovcan = 0 END IF
 IF (wmovval IS NULL) THEN LET wmovval = 0 END IF

 RETURN wmovcan,wmovval 
END FUNCTION 

-- Subrutina para obtener la cantidad equivalente de un empaque

FUNCTION librut003_canempaque(wcditem,wcodepq)
 DEFINE wcanepq  LIKE inv_epqsxpro.cantid,
        wcditem  LIKE inv_epqsxpro.cditem,
        wcodepq  LIKE inv_epqsxpro.codepq

 -- Obteniendo cantidad de empaque
 SELECT NVL(a.cantid,1)
  INTO  wcanepq
  FROM  inv_epqsxpro a
  WHERE a.cditem = wcditem
    AND a.codepq = wcodepq
  IF wcanepq IS NULL THEN
     LET wcanepq = 1
  END IF

 RETURN wcanepq
END FUNCTION

-- Subrutina para buscar los datos de un programa (glb_programs)

FUNCTION librut003_bprograma(wcodpro)
 DEFINE w_mae_reg RECORD LIKE glb_programs.*,
        wcodpro   LIKE glb_programs.codpro

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  glb_programs a
  WHERE (a.codpro= wcodpro)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un programa (glb_programs)

FUNCTION librut003_bpermprog(wcodpro,wprogid)
 DEFINE wcodpro   LIKE glb_dprogram.codpro,
        wprogid   LIKE glb_dprogram.progid

 SELECT UNIQUE a.progid
  FROM  glb_dprogram a
  WHERE (a.codpro= wcodpro)
    AND (a.progid= wprogid)
  IF (status=NOTFOUND) THEN
     RETURN FALSE
  ELSE
     RETURN TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un apartado por medio de su documento
-- de facturacion (fac_creditos)

FUNCTION librut003_bapartado(wlnktra)
 DEFINE w_mae_reg RECORD LIKE fac_creditos.*,
        wlnktra   LIKE fac_creditos.lnktra

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  fac_creditos a
  WHERE (a.lnktra= wlnktra)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una subcategoria (glb_subcateg)

FUNCTION librut003_BSubCategoria(wsubcat)
 DEFINE w_mae_reg RECORD LIKE glb_subcateg.*,
        wsubcat   LIKE glb_subcateg.subcat

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  glb_subcateg a
  WHERE (a.subcat= wsubcat)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una ruta (vta_rutasmer)

FUNCTION librut003_BRuta(wnoruta)
 DEFINE w_mae_reg RECORD LIKE vta_rutasmer.*,
        wnoruta   LIKE vta_rutasmer.noruta

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  vta_rutasmer a
  WHERE (a.noruta = wnoruta)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un liquidacion (fac_liqrutas)

FUNCTION librut003_BLiquidacion(wlnkliq)
 DEFINE w_mae_reg RECORD LIKE fac_liqrutas.*,
        wlnkliq   LIKE fac_liqrutas.lnkliq

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  fac_liqrutas a
  WHERE (a.lnkliq = wlnkliq)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un vendedor (vta_vendedrs)

FUNCTION librut003_BVendedor(wcodven)
 DEFINE w_mae_reg RECORD LIKE glb_vendedrs.*,
        wcodven   LIKE glb_vendedrs.codven

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  glb_vendedrs a
  WHERE (a.codven = wcodven)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un vendedor de una ruta (vis_vendedoresxruta)

FUNCTION librut003_BVendedorRuta(wnoruta)
 DEFINE w_mae_reg RECORD LIKE vis_vendedoresxruta.*,
        wnoruta   LIKE vta_rutasmer.noruta

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  vis_vendedoresxruta a
  WHERE (a.noruta = wnoruta)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para calcular el saldo de un apartado

FUNCTION librut003_saldoapartado(wlnkapa)
 DEFINE wlnkapa LIKE fac_creditos.lnkapa, 
        wtotpro LIKE fac_creditos.totpro,
        wabonos LIKE fac_creditos.totabo,
        wsaldos LIKE fac_creditos.totsal

 -- Seleccoonando valor original
 SELECT SUM(a.totsal)
  INTO  wtotpro
  FROM  fac_creditos a
  WHERE a.lnkapa = wlnkapa
    AND a.lnknid = 0 
    AND a.estado = 1 

 -- Sumando abonos 
 SELECT NVL(SUM(a.totabo),0)
  INTO  wabonos  
  FROM  fac_creditos a
  WHERE a.lnkapa > 0
    AND a.lnknid = wlnkapa
    AND a.estado = 1 

 -- Calculando saldo
 LET wsaldos = (wtotpro-wabonos) 

 RETURN wsaldos 
END FUNCTION 

-- Subrutina para contar los registros seleccionados de un qry

FUNCTION librut003_NumeroRegistros(qry)
 DEFINE qry     STRING,
        xdato   CHAR(1),
        totreg  INT

 -- Contando registros
 PREPARE nrows FROM qry
 DECLARE nrg CURSOR FOR nrows
 LET totreg = 1
 FOREACH nrg INTO xdato
  LET totreg = totreg+1
 END FOREACH
 CLOSE nrg
 FREE  nrg
 LET totreg = totreg-1
 RETURN totreg
END FUNCTION

-- Subrutina para calcular el saldo anterior de una cuenta bancaria

FUNCTION librut003_SaldoAnteriorCuentaBanco(wnumcta,wfecini) 
 DEFINE wnumcta    LIKE bco_mcuentas.numcta,
        wsalant    DEC(14,2),
        wfecini    DATE 

 -- Obteniedo saldo anterior de la cuenta
 SELECT NVL(SUM(a.totsal),0)
  INTO  wsalant
  FROM  bco_mtransac a 
  WHERE (a.numcta = wnumcta)
    AND (a.feccob < wfecini)
    AND (a.estado = 1)
    AND (a.nofase = 1) 

 IF wsalant IS NULL THEN LET wsalant = 0 END IF

 RETURN wsalant 
END FUNCTION

-- Subrutina para la libreria de grupos de centros de costo

FUNCTION librut003_CbxGruposCen()
 --llenando combobox
 LET qrytext = "SELECT a.codgru,a.nomgru ",
                "FROM glb_grucenct a " ,
                "ORDER BY 2 "
 CALL librut002_combobox("codgru",qrytext)
END FUNCTION

-- Subrutina para la libreria de grupos de socios de negocio 

FUNCTION librut003_CbxGruposSocios() 
 --llenando combobox
 LET qrytext = "SELECT a.codgru,a.nomgru ",
                "FROM glb_gruprovs a " ,
                "ORDER BY 1 "
 CALL librut002_combobox("codgru",qrytext)
END FUNCTION

-- Subrutina para buscar los datos de un tipo de nomenclatura contable (ctb_tiposnom) 

FUNCTION librut003_BTiposNomenclaturaCtb(wtipnom)
 DEFINE w_mae_reg RECORD LIKE ctb_tiposnom.*,
        wtipnom   LIKE ctb_tiposnom.tipnom

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  ctb_tiposnom a
  WHERE (a.tipnom= wtipnom)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una cuenta contable (ctb_mcuentas)

FUNCTION librut003_BCuentaContable(wcodemp,wnumcta)
 DEFINE w_mae_reg RECORD LIKE ctb_mcuentas.*,
        wcodemp   LIKE ctb_mcuentas.codemp,
        wnumcta   LIKE ctb_mcuentas.numcta

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  ctb_mcuentas a
  WHERE (a.codemp = wcodemp)
    AND (a.numcta = wnumcta)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina que carga el combobox de los tipos de transacciones contables 

FUNCTION librut003_CbxTiposTransaccionesContables() 
 -- Llenando combobox
 LET qrytext = "SELECT a.tiptrn,a.nomtrn ",
               " FROM  ctb_tipostrn a ",
               " ORDER BY 2 "

 CALL librut002_combobox("tiptrn",qrytext)
END FUNCTION

-- Subrutina para buscar los datos de un tipo de transaccion contable (ctb_tipostrn)

FUNCTION librut003_BTipoTransaccionContable(wtiptrn)
 DEFINE w_mae_reg RECORD LIKE ctb_tipostrn.*,
        wtiptrn   LIKE ctb_tipostrn.tiptrn

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  ctb_tipostrn a
  WHERE (a.tiptrn = wtiptrn)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una transaccion contable (ctb_mtransac)

FUNCTION librut003_BTransaccionContable(wlnktra)
 DEFINE w_mae_reg RECORD LIKE ctb_mtransac.*,
        wlnktra   LIKE ctb_mtransac.lnktra

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  ctb_mtransac a
  WHERE (a.lnktra = wlnktra)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un grupo de cuenta contable (ctb_grupocta)

FUNCTION librut003_BGrupoCuentaContable(wcodgru)
 DEFINE w_mae_reg RECORD LIKE ctb_grupocta.*,
        wcodgru   LIKE ctb_grupocta.codgru

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  ctb_grupocta a
  WHERE (a.codgru = wcodgru) 
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para encontrar el numero de documento por empresa, fecha y tipo de
-- transaccion de una transaccion contable automatica 

FUNCTION librut003_NumeroPolizaAutomatica(wcodemp,wtiptrn,wfecemi)
 DEFINE wcodemp LIKE ctb_mtransac.codemp,
        wtiptrn LIKE ctb_mtransac.tiptrn,
        wfecemi LIKE ctb_mtransac.fecemi,
        wnumdoc LIKE ctb_mtransac.numdoc, 
        xcorrel INTEGER

-- Encontrando correlativo maximo
 SELECT NVL(COUNT(*),0)+1
  INTO  xcorrel
  FROM  ctb_mtransac a
  WHERE a.codemp                       = wcodemp
    AND a.tiptrn                       = wtiptrn
    AND EXTEND(a.fecemi,YEAR TO MONTH) = EXTEND(wfecemi,YEAR TO MONTH)

 -- Construyendo numero
 LET wnumdoc = wtiptrn        USING "&&&",
               YEAR(wfecemi)  USING "&&&&",
               MONTH(wfecemi) USING "&&",
               xcorrel        USING "&&&&&"

 RETURN wnumdoc
END FUNCTION

-- Subrutina para generar meses de hoy hacia atraz un anio

FUNCTION librut003_12MesesAtras()
 DEFINE v_mes    ARRAY[12] OF RECORD
         nummes  DATETIME YEAR TO MONTH,
         fecmes  DATE
        END RECORD, 
        xfecmes  DATETIME YEAR TO MONTH,
        i        SMALLINT,
        qrytxt   STRING

 -- Obteniendo mes anterior
 LET xfecmes = EXTEND(TODAY,YEAR TO MONTH)
 LET qrytxt = NULL
 FOR i = 1 TO 12 
  LET v_mes[i].nummes = xfecmes - 1 UNITS MONTH
  LET xfecmes = v_mes[i].nummes 
  LET v_mes[i].fecmes = MDY(MONTH(xfecmes),
                        librut001_diasmes(MONTH(xfecmes),
                        YEAR(xfecmes)),
                        YEAR(xfecmes))
  LET qrytxt = qrytxt CLIPPED,v_mes[i].fecmes,"|"
 END FOR 

 -- Generando combobox 
 CALL librut002_combobox("fecemi",qrytxt)
END FUNCTION 

-- Subrutina para actualizar las cuentas padre de una cuenta contable 

FUNCTION librut003_CuentasPadre(wlnktra,wordcta,wcodemp,wfecemi,wnumcta,wtipnom,
                                wtipope,xtotval,wtotdeb,wtothab,wconcil,wopenue)

 DEFINE w_tip_nom       RECORD LIKE ctb_tiposnom.*, 
        v_nivel         ARRAY[10] OF RECORD   
         digits         CHAR(20),
         datos          LIKE ctb_tiposnom.format, 
         suma           SMALLINT
        END RECORD,
        wlnktra         LIKE ctb_mtransac.lnktra,  
        wtipsal         LIKE ctb_mcuentas.tipsal, 
        wordcta         LIKE ctb_ctransac.correl,     
        wfecemi         LIKE ctb_mtransac.fecemi,       
        wcodemp         LIKE ctb_mcuentas.codemp,  
        wnumcta         LIKE ctb_mcuentas.numcta,  
        wtipnom         LIKE ctb_tiposnom.tipnom,       
        wnumctb         LIKE ctb_mcuentas.numcta,
        whilera         LIKE ctb_mcuentas.numcta,
        wctapad         LIKE ctb_mcuentas.numcta,   
        wformat         LIKE ctb_tiposnom.format,   
        wtipope         LIKE ctb_dtransac.tipope,    
        wtotdeb         LIKE ctb_mtransac.totdoc, 
        wtothab         LIKE ctb_mtransac.totdoc, 
        xtotval         LIKE ctb_mtransac.totdoc, 
        wsalact         LIKE ctb_mcuentas.salact, 
        wcargos         LIKE ctb_mcuentas.cargos, 
        wcaracu         LIKE ctb_mcuentas.caracu, 
        wabonos         LIKE ctb_mcuentas.abonos, 
        waboacu         LIKE ctb_mcuentas.aboacu, 
        wcorrel         SMALLINT, 
        i,j,pos,ini,acu SMALLINT,
        dig,tn          SMALLINT,
        existe          SMALLINT,
        wconcil         CHAR(1),
        wopenue         CHAR(1)

 -- Obteniendo datos del tipo de nomenclatura 
 INITIALIZE w_tip_nom.* TO NULL
 CALL librut003_BTiposNomenclaturaCtb(wtipnom)
 RETURNING w_tip_nom.*,existe

 -- Quitando separadores del formato 
 LET wformat = librut001_QuitarSeparadores(w_tip_nom.format,w_tip_nom.sepdor)

 -- Inicializando variables
 LET wnumctb = NULL
 LET ini     = 1
 LET pos     = 0

 -- Inicializando vector de niveles
 FOR i = 1 TO 10
  INITIALIZE v_nivel[i].* TO NULL
  LET v_nivel[i].suma = 0
 END FOR 

 -- Asignando niveles 
 LET tn = 0
 IF w_tip_nom.dig001 >0 THEN LET v_nivel[1].digits  = w_tip_nom.dig001 LET tn = tn+1 END IF
 IF w_tip_nom.dig002 >0 THEN LET v_nivel[2].digits  = w_tip_nom.dig002 LET tn = tn+1 END IF
 IF w_tip_nom.dig003 >0 THEN LET v_nivel[3].digits  = w_tip_nom.dig003 LET tn = tn+1 END IF
 IF w_tip_nom.dig004 >0 THEN LET v_nivel[4].digits  = w_tip_nom.dig004 LET tn = tn+1 END IF
 IF w_tip_nom.dig005 >0 THEN LET v_nivel[5].digits  = w_tip_nom.dig005 LET tn = tn+1 END IF
 IF w_tip_nom.dig006 >0 THEN LET v_nivel[6].digits  = w_tip_nom.dig006 LET tn = tn+1 END IF
 IF w_tip_nom.dig007 >0 THEN LET v_nivel[7].digits  = w_tip_nom.dig007 LET tn = tn+1 END IF
 IF w_tip_nom.dig008 >0 THEN LET v_nivel[8].digits  = w_tip_nom.dig008 LET tn = tn+1 END IF
 IF w_tip_nom.dig009 >0 THEN LET v_nivel[9].digits  = w_tip_nom.dig009 LET tn = tn+1 END IF
 IF w_tip_nom.dig010 >0 THEN LET v_nivel[10].digits = w_tip_nom.dig010 LET tn = tn+1 END IF

 -- Eliminando separadores de la cuenta
 FOR i = 1 TO LENGTH(wnumcta)
  IF (wnumcta[i,i]=w_tip_nom.sepdor) THEN
     CONTINUE FOR
  END IF 
  LET wnumctb = wnumctb CLIPPED,wnumcta[i,i]
 END FOR 
 LET wnumcta  = wnumctb CLIPPED

 -- Escaneando niveles de la cuenta 
 FOR i = 1 TO tn 
  IF v_nivel[i].digits IS NULL THEN
     CONTINUE FOR
  END IF 
  LET pos     = (pos+v_nivel[i].digits)
  LET whilera = wnumcta[ini,pos]

  LET acu = 0
  FOR j = 1 TO LENGTH(whilera)
   LET dig = whilera[j,j]
   LET acu = (acu+dig)
  END FOR 
  LET v_nivel[i].datos = wnumcta[ini,pos]

  IF (acu>0) THEN
     LET v_nivel[i].suma = 1
  ELSE
     LET v_nivel[i].suma = 0
  END IF 

  LET ini = (pos+1)
  LET pos = (ini-1)
 END FOR

 -- Hallando cuentas en cascada por niveles
 LET wcorrel = 0  
 FOR i =  1 TO tn             

  -- Formando cuenta
  LET wctapad = NULL
  FOR j = 1 TO i
   LET wctapad = wctapad CLIPPED,v_nivel[j].datos CLIPPED
  END FOR

  -- Formateando la cuenta padre al formato del tipo de nomenclatura
  LET wctapad = librut001_FormatoCuentaContable(
                         wctapad,
                         wformat,   
                         (LENGTH(w_tip_nom.format)-(w_tip_nom.numniv-w_tip_nom.nivsep)),
                         "0")
  LET wctapad = librut001_PonerSeparadores(wctapad,w_tip_nom.sepdor)
  LET wcorrel = (wcorrel+1)

  -- Obteniendo tipo de saldo de la cuenta
  INITIALIZE wtipsal TO NULL
  SELECT x.tipsal 
   INTO  wtipsal 
   FROM  ctb_mcuentas x 
   WHERE x.numcta = wctapad  
     AND x.codemp = wcodemp 

  -- Inicializando datos
  LET wcargos = 0 
  LET wcaracu = 0 
  LET wabonos = 0 
  LET waboacu = 0 

  -- Actualiza cuentas normales
  CASE (wtipope)
   WHEN "D" -- Debe

    -- Verificando tipo de saldo
    CASE wtipsal    
     WHEN "D" -- Deudor 
      LET wsalact = xtotval 
      LET wcargos = xtotval
      LET wcaracu = xtotval  
     WHEN "A" -- Acreedor 
      LET wsalact = (xtotval*(-1)) 
      LET wcargos = xtotval
      LET wcaracu = xtotval  
    END CASE

   WHEN "H" -- Haber 

    -- Verificando tipo de saldo
    CASE wtipsal     
     WHEN "D" -- Deudor 
      LET wsalact = (xtotval*(-1)) 
      LET wabonos = xtotval
      LET waboacu = xtotval  
 
     WHEN "A" -- Acreedor 
      LET wsalact = xtotval 
      LET wabonos = xtotval
      LET waboacu = xtotval  
    END CASE 
  END CASE 

  -- Creando linea de transaccion por cuenta
  SET LOCK MODE TO WAIT
  INSERT INTO ctb_ctransac
  VALUES (wlnktra, 
          wordcta, 
          wcorrel, 
          wcodemp,
          wfecemi,
          wctapad,
          wtotdeb,
          wtothab, 
          wtipope,
          wconcil,                 
          wopenue,                  
          wtipsal,
          wsalact,
          wcargos,
          wcaracu,
          wabonos,
          waboacu)

   -- Actualizando cuentas en cascada
   CALL librut003_SaldoCuentasCascada(wcodemp,wctapad,wtipope,wtipsal,xtotval) 
 END FOR
END FUNCTION

-- Subrutina para actualizar los saldos de una cuenta contable 

FUNCTION librut003_SaldoCuentasCascada(wcodemp,wnumcta,wtipope,wtipsal,wtotval) 
 DEFINE wtipsal  LIKE ctb_mcuentas.tipsal,
        wcodemp  LIKE ctb_mcuentas.codemp, 
        wnumcta  LIKE ctb_mcuentas.numcta,  
        wtipope  LIKE ctb_dtransac.tipope,    
        wtotval  DECIMAL(14,2)

 -- Actualiza cuentas normales
 CASE (wtipope)
  WHEN "D" -- Debe

   -- Verificando tipo de saldo
   CASE wtipsal    
    WHEN "D" -- Deudor 
     SET LOCK MODE TO WAIT 
     UPDATE ctb_mcuentas
        SET ctb_mcuentas.salact = (ctb_mcuentas.salact+wtotval),
            ctb_mcuentas.cargos = (ctb_mcuentas.cargos+wtotval),
            ctb_mcuentas.caracu = (ctb_mcuentas.caracu+wtotval)
      WHERE ctb_mcuentas.numcta = wnumcta  
        AND ctb_mcuentas.codemp = wcodemp  

    WHEN "A" -- Acreedor 
     SET LOCK MODE TO WAIT 
     UPDATE ctb_mcuentas
        SET ctb_mcuentas.salact = (ctb_mcuentas.salact-wtotval),
            ctb_mcuentas.cargos = (ctb_mcuentas.cargos+wtotval),
            ctb_mcuentas.caracu = (ctb_mcuentas.caracu+wtotval)
      WHERE ctb_mcuentas.numcta = wnumcta  
        AND ctb_mcuentas.codemp = wcodemp   
   END CASE

  WHEN "H" -- Haber 

   -- Verificando tipo de saldo
   CASE wtipsal     
    WHEN "D" -- Deudor 
     SET LOCK MODE TO WAIT 
     UPDATE ctb_mcuentas
        SET ctb_mcuentas.salact = (ctb_mcuentas.salact-wtotval),
            ctb_mcuentas.abonos = (ctb_mcuentas.abonos+wtotval),
            ctb_mcuentas.aboacu = (ctb_mcuentas.aboacu+wtotval)
      WHERE ctb_mcuentas.numcta = wnumcta  
        AND ctb_mcuentas.codemp = wcodemp   

    WHEN "A" -- Acreedor 
     SET LOCK MODE TO WAIT 
     UPDATE ctb_mcuentas
        SET ctb_mcuentas.salact = (ctb_mcuentas.salact+wtotval),
            ctb_mcuentas.abonos = (ctb_mcuentas.abonos+wtotval),
            ctb_mcuentas.aboacu = (ctb_mcuentas.aboacu+wtotval)
      WHERE ctb_mcuentas.numcta = wnumcta 
        AND ctb_mcuentas.codemp = wcodemp  

   END CASE 
 END CASE 
END FUNCTION

-- Subrutina para calcular el saldo anterior de un socio de negocios en
-- las cuentas por pagar 

FUNCTION librut003_SaldoAnteriorCXP(wcodemp,wcodsoc,wfecini)
 DEFINE wcodsoc    LIKE cmp_mtransac.codsoc,
        wcodemp    LIKE cmp_mtransac.codemp,
        wsalant    DEC(14,2),
        wtotcmp    DEC(14,2),
        wtotpag    DEC(14,2),
        wtotnot    DEC(14,2),
        wtotefe    DEC(14,2),
        wfecini    DATE

 -- Totalizando saldo compras
 SELECT NVL(SUM(a.totval),0)
  INTO  wtotcmp
  FROM  cmp_mtransac a
  WHERE (a.codemp = wcodemp)
    AND (a.codsoc = wcodsoc)
    AND (a.fecemi < wfecini)
    AND (a.frmpag IN (1,2))
    AND (a.estado = 1)

 -- Totalizando saldo pagos
 SELECT NVL(SUM(y.totpag),0)
  INTO  wtotpag
  FROM  bco_mtransac x,bco_dtransac y
  WHERE (x.lnkbco = y.lnkbco)
    AND (x.codemp = wcodemp) 
    AND (x.codsoc = wcodsoc)
    AND (x.feccob < wfecini)
    AND (x.estado = 1)

 -- Totalizando abonos notas de debito
 SELECT NVL(SUM(y.totpag),0)
  INTO  wtotnot     
  FROM  cmp_notasdeb x,bco_dtransac y
  WHERE (x.lnknot  = y.lnknot)
    AND (x.codemp  = wcodemp)
    AND (x.codsoc  = wcodsoc) 
    AND (x.fecemi  < wfecini) 
    AND (x.estado  = 1)

 -- Totalizando abonos efectivo
 SELECT NVL(SUM(a.totval),0)
  INTO  wtotefe 
  FROM  cmp_mtransac a
  WHERE (a.codemp = wcodemp)
    AND (a.codsoc = wcodsoc)
    AND (a.fecemi < wfecini)
    AND (a.frmpag IN (1))
    AND (a.estado = 1)

 -- Calculando saldo anterior
 LET wsalant = (wtotcmp-wtotpag-wtotnot-wtotefe)

 RETURN wsalant
END FUNCTION

-- Subrutina para buscar ultima fecha de absorcion

FUNCTION librut003_FechaUltimaAbsorcion(wcodemp,wcodbod)
 DEFINE wcodemp    LIKE inv_mtransac.codemp,
        wcodbod    LIKE inv_mtransac.codbod,
        fecultabs  DATE

 -- Verificando fecha de ultima absorcion
 SELECT MAX(a.fecemi)
  INTO  fecultabs
  FROM  inv_mtransac a
  WHERE a.codemp = wcodemp
    AND a.codbod = wcodbod
    AND a.tipmov IN (2,3)
    AND a.aniotr IS NOT NULL
    AND a.mestra IS NOT NULL
    AND a.fecemi IS NOT NULL

 RETURN fecultabs
END FUNCTION

-- Subrutina para buscar los datos de una nota de debito de cuentas por pagar (cmp_notasdeb)

FUNCTION librut003_BNotaDebitoCxP(wlnknot) 
 DEFINE w_mae_reg RECORD LIKE cmp_notasdeb.*, 
        wlnknot   LIKE cmp_notasdeb.lnknot  
      
 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  cmp_notasdeb a
  WHERE (a.lnknot= wlnknot)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina que carga el combobox de los modulos de un sistema

FUNCTION librut003_CbxModulos()
 -- Llenando combobox
 LET qrytext = "SELECT a.codmod,a.nommod,a.numord ",
               " FROM glb_mmodulos a ",
               " WHERE a.estado = 1 ",
               " ORDER BY 3 "

 CALL librut002_combobox("codmod",qrytext)
END FUNCTION

-- Subrutina que carga el combobox de los ids de las cajas ya 
-- liquidadas 

FUNCTION librut003_CbxIdsCajas(strlnkbco)   
 DEFINE strlnkbco STRING 

 -- Llenando combobox 
 LET qrytext = 
  "SELECT a.lnkcaj,","'ID#: '||",
         "rpad(a.lnkcaj,8,' ')||'    CAJA#: '||", 
         "rpad(a.numdoc,8,' ')||'    FECHA: '||",
         "lpad(a.fecemi,12,' ')||'   VALOR: '||",
         "lpad(a.totgto,15,' ') ",
   "from fac_cajchica a ",
   "WHERE a.estado = 1 ",
    strlnkbco CLIPPED, 
   "ORDER BY a.lnkcaj" 

 CALL librut002_combobox("lnkcaj",qrytext)
END FUNCTION

-- Subrutina para buscar los datos de una caja (fac_cajchica)

FUNCTION librut003_BCajas(wlnkcaj)
 DEFINE w_mae_reg RECORD LIKE fac_cajchica.*,
        wlnkcaj   LIKE fac_cajchica.lnkcaj 

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  fac_cajchica a
  WHERE (a.lnkcaj= wlnkcaj)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION


-- Subrutina que carga el combobox de las sedes
FUNCTION librut003_CbxSedes() 
 -- Llenando combobox
 LET qrytext = "SELECT city_num, city_country ",
               " FROM  aprojusa@lnxdesa_tcp:city a ",
               " ORDER BY 2 "
 CALL librut002_combobox("ch_sede",qrytext)
END FUNCTION

--Para notas de debito
FUNCTION librut003_CbxTiposMovimientoPagoBco()
 -- Llenando combobox
 LET qrytext="SELECT a.tipmov,a.nommov||' - '||",
                    "case (a.tipope) when 1 then 'CARGO' when 0 then 'ABONO' end",
             " FROM bco_tipomovs a ",
             " WHERE a.pagpro = 1 ",
             " ORDER BY 2 "

 CALL librut002_combobox("tipmov",qrytext)
END FUNCTION