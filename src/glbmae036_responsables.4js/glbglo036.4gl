{ 
glbglo036.4gl
Mynor Ramirez
Mantenimiento de responsables 
Octubre 2011 
-- invmae0012a.per 
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "glbmae036"
DEFINE w_mae_pro       RECORD LIKE glb_responsb.*,
       v_responsables  DYNAMIC ARRAY OF RECORD
        tcodres        LIKE glb_responsb.codres,
        tnomres        LIKE glb_responsb.nomres 
       END RECORD,
       username        VARCHAR(15)
END GLOBALS
