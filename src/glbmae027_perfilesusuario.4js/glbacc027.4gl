{ 
glbacc027.4gl 
Mynor Ramirez
Mantenimiento de perfiles de usuario 
}

-- Definicion de variables globales 

GLOBALS "glbglo027.4gl" 
CONSTANT root_image = "mars_nodogrupo"
CONSTANT node_image = "mars_nodogrupo"
CONSTANT leaf_image = "mars_nodohoja" 
CONSTANT root_name  = "0"

TYPE t_program             RECORD
       code                VARCHAR(50),
       name                VARCHAR(50),
       lnkpro              INTEGER,
       ordopc              SMALLINT,
       acceso              SMALLINT, 
       expand              SMALLINT
     END RECORD

TYPE t_accesos             RECORD
       name                STRING,
       image               STRING,
       pid                 VARCHAR(50),
       id                  VARCHAR(50),
       expand              BOOLEAN,
       lnkpro              INTEGER,
       ordopc              SMALLINT,
       acceso              SMALLINT,
       rellen              CHAR(1) 
     END RECORD

TYPE t_colores             RECORD
       name                STRING,
       image               STRING,
       pid                 STRING,
       id                  STRING,
       expand              STRING,
       lnkpro              STRING,
       ordopc              STRING,
       acceso              STRING,
       rellen              CHAR(1) 
     END RECORD

DEFINE w_datos RECORD
        codmod LIKE glb_mmodulos.codmod
       END RECORD

DEFINE treecol DYNAMIC ARRAY OF t_colores
DEFINE treeacc DYNAMIC ARRAY OF t_accesos
DEFINE xroleid LIKE glb_rolesusr.roleid 

-- Subrutina para definir los accesos a programas x perfil 

FUNCTION glbacc027_accesos_perfil() 
 DEFINE loop SMALLINT 

 -- Abriendo ventana
 OPEN WINDOW wacc027 
  WITH FORM "glbmae027b" ATTRIBUTE(BORDER)

  -- Cargando combobox de modulos
  CALL librut003_CbxModulos() 

  -- Asginando perfil
  LET xroleid = w_mae_pro.roleid 
  CALL librut001_dpelement("perfil",w_mae_pro.nmrole) 

  LET loop = TRUE
  WHILE loop
   -- Inicializando arbol
   CALL treeacc.clear()

   -- Seleccionando modulo 
   INPUT BY NAME w_datos.codmod WITHOUT DEFAULTS 
    ON ACTION cancelar
      DISPLAY "action cancelar"
     LET loop = FALSE 
     EXIT INPUT

    ON CHANGE codmod 
     -- Cargando programas y accesos
     CALL glbacc027_carga_accesos(root_name)

     -- Definiendo accesos del perfil 
     CALL glbacc027_define_accesos()
   END INPUT 
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Cargando programas y accesos
   CALL glbacc027_carga_accesos(root_name)

   -- Definiendo accesos del perfil 
   CALL glbacc027_define_accesos()
  END WHILE 

 CLOSE WINDOW wacc027
END FUNCTION 

-- Subrutina para cargar el arbol con las opciones para la definicion de accesos 

FUNCTION glbacc027_carga_accesos(parentid)
 DEFINE parentid  VARCHAR(50)
 DEFINE arr       DYNAMIC ARRAY OF t_program 
 DEFINE rec       t_program 
 DEFINE i,j,n,x   INT
 DEFINE conteo    INT

 -- Seleccionando datos para llenar los nodos
 DECLARE cu1 CURSOR FOR
 SELECT a.codpro, -- codigo programa
        a.nompro, -- nombre opcion 
        a.lnkpro, -- serial opcion 
        a.ordopc, -- posicion
        0,        -- acceso 
        0         -- expandido
  FROM vis_programas a
  WHERE a.parent = parentid
    and a.codmod = w_datos.codmod  
  ORDER BY 4

 LET n = 0
 FOREACH cu1 INTO rec.*
  LET n = n + 1

  -- Verificando si el acceso ya existe registrado en el perfil
  SELECT COUNT(*)
   INTO  conteo
   FROM  glb_permxrol a
   WHERE a.roleid = xroleid 
     AND a.lnkpro = rec.lnkpro 
   IF (conteo>0) THEN
      LET rec.acceso = 1 
      LET rec.expand = 1 
   END IF 

  -- Asignando datos 
  LET arr[n].* = rec.*
 END FOREACH

 -- Si es seleccion de nodos padre inicializa el arbol
 IF parentid == root_name THEN
    CALL treeacc.clear()
 END IF

 -- Obteniendo largo
 LET x = treeacc.getLength()

 -- Verificando si hay datos para llenar el nodo y si el numero de nodos >0 
 IF n>0 AND x>0 THEN 
    -- Si es primer nodo 
    IF x==1 THEN
       LET treeacc[x].image = root_image
    ELSE
       -- Si no es primer nodo
       LET treeacc[x].image = node_image
    END IF 

    -- Verificando si nodo tiene subnodos con acceso 
    SELECT COUNT(*)
     INTO  conteo
     FROM  glb_permxrol a
     WHERE a.roleid = xroleid 
       AND a.codpro = parentid 

    IF (conteo>0) THEN
       LET treeacc[x].expand = 1
       LET treecol[x].name   = "darkred"
    ELSE 
       LET treeacc[x].expand = 0 
       LET treecol[x].name   = ""
    END IF 
 END IF

 -- Llenando nodos
 FOR i = 1 TO n
  LET j                 = treeacc.getlength()+1
  LET treeacc[j].name   = arr[i].name
  LET treeacc[j].image  = leaf_image
  LET treeacc[j].pid    = parentid
  LET treeacc[j].id     = arr[i].code
  LET treeacc[j].lnkpro = arr[i].lnkpro
  LET treeacc[j].ordopc = arr[i].ordopc              
  LET treeacc[j].acceso = arr[i].acceso 
  LET treeacc[j].expand = arr[i].expand
  LET treecol[j].acceso = "lightgray reverse"

  -- Llenando sub-nodos
  CALL glbacc027_carga_accesos(arr[i].code) 
 END FOR  
END FUNCTION

-- Desplegando y asignando accesos de programas 

FUNCTION glbacc027_define_accesos()
 DIALOG ATTRIBUTES(UNBUFFERED)

  -- Desplegando arbol
  DISPLAY ARRAY treeacc TO tvsr.*
   BEFORE ROW
    -- Configurando acciones 
    CALL glbacc027_configura_acciones(DIALOG)

    -- Totalizando accesos seleccionados
    CALL glbacc027_TotalAccesos()
   END DISPLAY

  BEFORE DIALOG
   -- Obteniendo fila actual
   CALL DIALOG.setCurrentRow("tvsr",FALSE)
   -- Asignando colores 
   CALL DIALOG.setArrayAttributes("tvsr",treecol)
   -- Configurando acciones cuando no hay nodos en el arbol
   CALL glbacc027_configura_acciones(DIALOG) 
   -- Totalizando accesos seleccionados
   CALL glbacc027_TotalAccesos()

  ON ACTION tree_accesos
   -- Agregando o quitando accesos 
   CALL glbacc027_agregarquitar_nodos(DIALOG.getCurrentRow("tvsr"))
   CALL glbacc027_configura_acciones(DIALOG)

   -- Totalizando accesos seleccionados
   CALL glbacc027_TotalAccesos()

  ON KEY (space) 
   -- Agregando o quitando accesos 
   IF NOT glbacc027_nodo_es_padre(DIALOG.getCurrentRow("tvsr")) THEN 
      CALL glbacc027_agregarquitar_nodos(DIALOG.getCurrentRow("tvsr"))
      CALL glbacc027_configura_acciones(DIALOG)

      -- Totalizando accesos seleccionados
      CALL glbacc027_TotalAccesos()
   END IF 

  ON ACTION tree_exptodo
   -- Expandiendo nodos 
   CALL glbacc027_expcolnodos(DIALOG.getCurrentRow("tvsr"),TRUE)
   CALL glbacc027_configura_acciones(DIALOG)

  ON ACTION tree_coltodo
   -- Colapsando nodos 
   CALL glbacc027_expcolnodos(DIALOG.getCurrentRow("tvsr"),FALSE)
   CALL glbacc027_configura_acciones(DIALOG)

  ON ACTION aceptar 
   -- Grabando definicion de accesos 
   CALL glbacc027_grabar_accesos(xroleid)
   CALL fgl_winmessage(
   " Atencion:"," Definicion de accesos registrada.\n"||
   " Los accesos registrados en los usuarios con este perfil son afectados.",
   "information")
   EXIT DIALOG 

  ON ACTION cancelar 
   -- Salida
   EXIT DIALOG 

  ON KEY(CONTROL-Z) 
   -- Desplegando nodos
   CALL glbacc027_muestra_nodos()

 END DIALOG
END FUNCTION

-- Subrutina para configurar acciones de la definicion de accesos 

FUNCTION glbacc027_configura_acciones(d)
 DEFINE d     ui.Dialog
 DEFINE n,c   INT

 -- Obteniendo largo del arreglo tvsr y fila actual 
 LET n = d.getArrayLength("tvsr")
 LET c = d.getCurrentRow("tvsr")

 -- Si la fila actual es menor o igual a cero desactiva acciones
 IF (c<=0) THEN
  CALL d.setActionActive("tree_accesos",FALSE)
  CALL d.setActionActive("tree_exptodo",FALSE)
  CALL d.setActionActive("tree_coltodo",FALSE)
 ELSE
  -- Activando las acciones
  CALL d.setActionActive("tree_accesos",NOT glbacc027_nodo_es_padre(d.getCurrentRow("tvsr")))
  CALL d.setActionActive("tree_exptodo",glbacc027_nodo_es_padre(c) AND NOT treeacc[c].expand)
  CALL d.setActionActive("tree_coltodo",glbacc027_nodo_es_padre(c) AND treeacc[c].expand)
 END IF
END FUNCTION

-- Subrutina para verificar si el nodo es padre 

FUNCTION glbacc027_nodo_es_padre(c)
 DEFINE c INT

 IF c<=0 THEN RETURN FALSE END IF
 IF c==treeacc.getLength() THEN RETURN FALSE END IF
 RETURN (treeacc[c+1].pid==treeacc[c].id)
END FUNCTION

-- Subrutina para expandir o colapsar nodos 

FUNCTION glbacc027_expcolnodos(c,v)
 DEFINE c,v,i INT

 FOR i=c TO treeacc.getLength()
  IF i!=c AND treeacc[i].pid == treeacc[c].pid THEN EXIT FOR END IF
  LET treeacc[i].expand = v
 END FOR
END FUNCTION

-- Subrutina para agregar o quitar accesos de la definicion de programas

FUNCTION glbacc027_agregarquitar_nodos(c)
 DEFINE c INT

 -- Verificando si acceso esta seleccionado
 IF (treeacc[c].acceso=1) THEN
    -- Quita
    LET treeacc[c].acceso = 0
 ELSE 
    -- Agrega 
    LET treeacc[c].acceso = 1
 END IF
END FUNCTION

-- Subrutina para contar los accesos seleccionados

FUNCTION glbacc027_TotalAccesos()
 DEFINE n,i INT 

 -- Contando el numero de accesos
 LET n = 0 
 FOR i=1 TO treeacc.getLength()
  IF treeacc[i].acceso THEN 
     LET n = n+1
  END IF 
 END FOR 

 -- Desplegando numero de accesos
 CALL librut001_dpelement("naccesos","Accesos ["||n||"]")
END FUNCTION

-- Subrutina para grabar la definicion de accesos 

FUNCTION glbacc027_grabar_accesos(wroleid)
 DEFINE w_mae_per RECORD LIKE glb_permxrol.*
 DEFINE wroleid   INT
 DEFINE i         INT

 -- Grabando
 ERROR "Grabando accessos ..." ATTRIBUTE(CYAN) 

 -- Iniciando transaccion
 BEGIN WORK 

 FOR i=1 TO treeacc.getLength()
  LET w_mae_per.lnkpro = treeacc[i].lnkpro     -- id unico del acceso 

  -- Verificando si es nodo padre no se graba
  IF treeacc[i].id = 0 THEN
     CONTINUE FOR
  END IF 

  -- Verificando si existe acceso 
  SET LOCK MODE TO WAIT 
  DELETE FROM glb_permxrol 
  WHERE glb_permxrol.roleid = wroleid
    AND glb_permxrol.lnkpro = w_mae_per.lnkpro 

  -- Seleccionando datos del programa
  SELECT a.codpro,a.progid
   INTO  w_mae_per.codpro,
         w_mae_per.progid
   FROM  glb_dprogram a
   WHERE a.lnkpro = w_mae_per.lnkpro
   IF (status=NOTFOUND) THEN
      CONTINUE FOR
   END IF 

  -- Obteniendo datos del programa
  LET w_mae_per.lnkacc = 0                      -- id unico del permiso
  LET w_mae_per.roleid = wroleid                -- Codigo del perfil 
  LET w_mae_per.userid = FGL_GETENV("LOGNAME")  -- usuario registra
  LET w_mae_per.fecsis = CURRENT                -- fecha registro
  LET w_mae_per.horsis = CURRENT HOUR TO SECOND -- hora registro 

  -- Grabando
  IF treeacc[i].acceso THEN
     SET LOCK MODE TO WAIT
     INSERT INTO glb_permxrol
     VALUES (w_mae_per.*) 
  END IF 
 END FOR

 -- Todos los accesos de este perfil que fueron eliminados seran tambien eliminados
 -- de los accesos de los usuarios que correspoden al mismo perfil 
 DELETE FROM glb_permxusr 
 WHERE glb_permxusr.roleid = wroleid 
   AND NOT EXISTS (SELECT b.lnkpro FROM glb_permxrol b
                    WHERE b.roleid = glb_permxusr.roleid
                      AND b.lnkpro = glb_permxusr.lnkpro)

 -- Todos los accesos de este perfil que fueron agregados seran tambien agregados
 -- a los accesos de usuario 
 INSERT INTO glb_permxusr 
 SELECT 0,
        b.roleid,
        b.lnkpro,
        a.userid,
        c.codpro,c.progid,
        1,
        "",
        "",
        "",
        USER,
        CURRENT,
        CURRENT HOUR TO SECOND 
  FROM glb_usuarios a,glb_permxrol b,glb_dprogram c
  WHERE a.roleid = b.roleid
    AND a.roleid = wroleid 
    AND c.lnkpro = b.lnkpro
    AND b.progid >0
    AND NOT EXISTS (SELECT c.lnkpro FROM glb_permxusr c
                    WHERE c.roleid = b.roleid
                      AND c.lnkpro = b.lnkpro
                      AND c.userid = a.userid)

 -- Finalizando transaccion
 COMMIT WORK
END FUNCTION

-- Subrutina para mostrar los datos de los nodos 

FUNCTION glbacc027_muestra_nodos()
 DEFINE i INT

 DISPLAY "------------------------------------------------------------"
 FOR i=1 TO treeacc.getLength()
  DISPLAY treeacc[i].pid, "/", treeacc[i].id
 END FOR
END FUNCTION
