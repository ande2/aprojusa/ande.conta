

drop table glb_permxrol;
create table "sistemas".glb_permxrol
  (
    lnkacc serial not null ,
    roleid smallint not null ,
    lnkpro integer not null ,
    codpro char(15) not null ,
    progid smallint not null ,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (roleid,lnkpro)  constraint "sistemas".uqglbpermrol,
    primary key (lnkacc)  constraint "sistemas".pkglbpermxrol
  );

drop table glb_permxusr;
create table "sistemas".glb_permxusr
  (
    lnkper serial not null ,
    roleid smallint not null ,
    lnkpro integer not null ,
    userid char(15) not null ,
    codpro char(15) not null ,
    progid smallint not null ,
    activo smallint not null ,
    passwd char(20),
    fecini date,
    fecfin date,
    usuaid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    unique (roleid,lnkpro,userid)  constraint "sistemas".uqglbpermxusr,
    primary key (lnkper)  constraint "sistemas".pkglbpermxusr
  );

DROP PROCEDURE "sistemas".sptcrearaccesosxusuario;
CREATE PROCEDURE "sistemas".sptcrearaccesosxusuario(xroleid INT,xuserid VARCHAR(15))
 -- Procedimiento para crear los accesos x usuario de acuerdo al perfil
 -- Mynor Ramirez
 -- Octubre 2011

 DEFINE splnkpro LIKE glb_permxrol.lnkpro;
 DEFINE spcodpro LIKE glb_permxrol.codpro;
 DEFINE spprogid LIKE glb_permxrol.progid;
 DEFINE conteo   INT;

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO COMMITTED READ;

 -- Insertando los accesos por perfil que no existan
 FOREACH SELECT a.lnkpro,
                a.codpro,
                a.progid
          INTO  splnkpro,
                spcodpro,
                spprogid
          FROM  glb_permxrol a
          WHERE a.roleid = xroleid
            AND a.progid >0
          ORDER BY a.lnkpro

    -- Verificando si permiso ya existe
    SELECT COUNT(*)
     INTO  conteo
     FROM  glb_permxusr a
     WHERE a.roleid = xroleid
       AND a.lnkpro = splnkpro
       AND a.userid = xuserid;

    -- Si existe no lo grabar para mantener datos actualzados desde el usuario
    IF (conteo>0) THEN
       CONTINUE FOREACH;
    END IF;

    -- Insertando accesos
    SET LOCK MODE TO WAIT;
    INSERT INTO glb_permxusr
    VALUES (0,                           -- id unico del permiso
            xroleid,                     -- id del perfil
            splnkpro,                    -- id del acceso
            xuserid,                     -- usuario del perfil
            spcodpro,                    -- codigo de la opcion
            spprogid,                    -- id de la opcion
            1,                           -- marcando perfil de activo
            NULL,                        -- sin password
            NULL,                        -- sin fecha de inicio
            NULL,                        -- sin fecha de finalizacion
            USER,                        -- usuario registro
            CURRENT,                     -- fecha de registro
            CURRENT HOUR TO SECOND);     -- ghora de registro
 END FOREACH;

 -- Elimina todos aquellos permisos que no son del usuario por si fue cambio de perfil
 SET LOCK MODE TO WAIT;
 DELETE FROM glb_permxusr
 WHERE glb_permxusr.roleid != xroleid
   AND glb_permxusr.userid  = xuserid;
END PROCEDURE;


drop table glb_usuarios;
create table "sistemas".glb_usuarios
  (
    userid varchar(15,1) not null ,
    nomusr varchar(50,1) not null ,
    email1 varchar(50,1),
    roleid smallint,
    usuaid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (userid)  constraint "sistemas".pkglbusuarios
  );

create trigger "sistemas".trgcrearaccesosxusuario insert on "sistemas"
    .glb_usuarios referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptcrearaccesosxusuario(pos.roleid
    ,pos.userid ));

create trigger "sistemas".trgupdateaccesosxusuario update of
    roleid on "sistemas".glb_usuarios referencing new as pos
    for each row
        (
        execute procedure "sistemas".sptcrearaccesosxusuario(pos.roleid
    ,pos.userid ));

create trigger "sistemas".trgdeleteaccesosxusuario delete on
    "sistemas".glb_usuarios referencing old as pre
    for each row
        (
        delete from "sistemas".glb_permxusr  where (userid =
    pre.userid ) );
