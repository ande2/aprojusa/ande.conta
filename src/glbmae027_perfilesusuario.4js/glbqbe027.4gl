{ 
glbqbe027.4gl 
Mynor Ramirez
Mantenimiento de perfiles de usuario 
}

{ Definicion de variables globales }

GLOBALS "glbglo027.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION glbqbe027_perfiles(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,
        msg                 STRING,
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL glbqbe027_EstadoMenu(operacion," ") 
   INITIALIZE qrytext,qrypart TO NULL
   CALL glbmae027_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.roleid,a.nmrole,a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel       
     -- Salida
     CALL glbmae027_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.roleid,a.nmrole ",
                 " FROM glb_rolesusr a ",
                 " WHERE ",qrytext CLIPPED,
                 " ORDER BY 1 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_perfilpro SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_rolesusr.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_perfilpro INTO v_rolesusr[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_perfilpro
   FREE  c_perfilpro
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL glbqbe027_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_rolesusr TO s_perfilpro.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel
      -- Salida
      CALL glbmae027_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscando
      CALL glbmae027_inival(1)
      EXIT DISPLAY

     ON ACTION modificar
      -- Modificando 
      IF glbmae027_perfiles(2) THEN
         EXIT DISPLAY 
      ELSE 
         -- Desplegando datos
         CALL glbqbe027_datos(v_rolesusr[ARR_CURR()].troleid)
      END IF 

     ON KEY (CONTROL-M)
      -- Modificando 
      IF (operacion=2) THEN 
       IF glbmae027_perfiles(2) THEN
          EXIT DISPLAY 
       ELSE 
          -- Desplegando datos
          CALL glbqbe027_datos(v_rolesusr[ARR_CURR()].troleid)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF librut004_IntegridadPerfiles(w_mae_pro.roleid) THEN
         CALL fgl_winmessage(
         " Atencion",
         " Este perfil existe en algun usuario. \n Perfil no puede borrarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de Borrar este perfil ? "
      LET opc = librut001_menuopcs("Confirmacion",
                                  msg,
                                  "Si",
                                  "No",
                                  NULL,
                                  NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL glbmae027_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION accesos
      -- Definicion de accesos a programas 
      CALL glbacc027_accesos_perfil()

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Perfil"
      LET arrcols[2] = "Nombre del Perfil"
      LET arrcols[3] = "Usuario Registro"
      LET arrcols[4] = "Fecha Registro"
      LET arrcols[5] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry        = "SELECT a.roleid,a.nmrole,a.userid,a.fecsis,a.horsis ", 
                       " FROM glb_rolesusr a ",
                       " WHERE ",qrytext CLIPPED,
                       " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res        = librut002_excelreport("Perfiles de Usuario",qry,5,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a la opcion de accesos 
         IF NOT seclib001_accesos(progname,6,username) THEN
            CALL DIALOG.setActionActive("accesos",FALSE)
         ELSE
            CALL DIALOG.setActionActive("accesos",TRUE)
         END IF

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
	 CALL DIALOG.setActionActive("accesos",FALSE)
	 CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
	 CALL DIALOG.setActionActive("accesos",FALSE)
	 CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
         CALL glbqbe027_datos(v_rolesusr[ARR_CURR()].troleid)
      END IF 

    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen perfiles con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Desplegando estado del menu
  CALL glbqbe027_EstadoMenu(0,"") 
END FUNCTION

{ Subrutina para desplegar los datos del mantenimiento }

FUNCTION glbqbe027_datos(wroleid)
 DEFINE wroleid LIKE glb_rolesusr.roleid,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  glb_rolesusr a "||
              "WHERE a.roleid = "||wroleid||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_perfilprot SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_perfilprot INTO w_mae_pro.*
  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.nmrole
  DISPLAY BY NAME w_mae_pro.roleid,w_mae_pro.userid THRU w_mae_pro.horsis
 END FOREACH
 CLOSE c_perfilprot
 FREE  c_perfilprot

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nmrole
 DISPLAY BY NAME w_mae_pro.roleid,w_mae_pro.userid THRU w_mae_pro.horsis
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION glbqbe027_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Perfiles - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Perfiles - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Perfiles - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Perfiles - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Perfiles - NUEVO")
 END CASE
END FUNCTION
