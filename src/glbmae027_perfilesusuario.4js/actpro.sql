update glb_programs
set codmod = (select x.codmod from tempi x
              where x.codpro = glb_programs.codpro), 
    ordpro = (select x.ordpro from tempi x
              where x.codpro = glb_programs.codpro)
where exists (select x.codpro from tempi x
              where x.codpro = glb_programs.codpro)
