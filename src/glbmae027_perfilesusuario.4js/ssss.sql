create trigger "sistemas".trgupdtipomovimiento update of tipmov
    on "sistemas".inv_mtransac referencing new as pos
    for each row
        (
        update "sistemas".inv_dtransac set "sistemas".inv_dtransac.tipmov
    = pos.tipmov  where (lnktra = pos.lnktra ) );
