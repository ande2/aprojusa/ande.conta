{
glbmae027.4gl 
Mynor Ramirez
Mantenimiento de perfiles de usuario 
}

-- Definicion de variables globales 

GLOBALS "glbglo027.4gl"

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 IF isGDC() THEN 
   CALL ui.Interface.loadStyles("styles")
 END IF   
 CALL ui.Interface.loadToolbar("toolbar3")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    IF isGDC() THEN 
       CALL ui.Interface.setContainer("mainmenu")
       CALL ui.Interface.setName("perfiles")
       CALL ui.Interface.setType("child")
   END IF 
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo usuario del sistema
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de principal 
 CALL glbmae027_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION glbmae027_mainmenu()
 DEFINE titulo   STRING, 
        wpais    VARCHAR(255),
        existe   SMALLINT,
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "glbmae027a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header(progname,wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox de modulos
  CALL librut003_CbxModulos() 

  -- Menu de opciones
  MENU "Perfiles"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,4,username) THEN
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Modificar"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    "Busqueda de perfiles."
    CALL glbqbe027_perfiles(1) 
   COMMAND "Nuevo"
    "Ingreso de un nuevo perfil."
    LET savedata = glbmae027_perfiles(1) 
   COMMAND "Modificar"
    "Modificacion de un perfil existente."
    CALL glbqbe027_perfiles(2) 
   COMMAND "Borrar"
    "Eliminacion de un perfil existente."
    CALL glbqbe027_perfiles(3) 
   COMMAND "Salir"
    "Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION glbmae027_perfiles(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL glbqbe027_EstadoMenu(4,"")
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL glbmae027_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nmrole
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   AFTER FIELD nmrole  
    --Verificando nombre de la perfil
    IF (LENGTH(w_mae_pro.nmrole)=0) THEN
       ERROR "Error: nombre del  perfil invalido, VERIFICA"
       LET w_mae_pro.nmrole = NULL
       NEXT FIELD nmrole  
    END IF

    -- Verificando que no exista otro perfil con el mismo nombre
    SELECT UNIQUE (a.roleid)
     FROM  glb_rolesusr a
     WHERE (a.roleid != w_mae_pro.roleid) 
       AND (a.nmrole  = w_mae_pro.nmrole) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion",
        " Existe otro perfil con el mismo nombre, VERIFICA ...",
        "information")
        NEXT FIELD nmrole
     END IF 
   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.nmrole IS NULL THEN 
       NEXT FIELD nmrole
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL glbmae027_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando perfil
    CALL glbmae027_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL glbqbe027_EstadoMenu(0,"")
    CALL glbmae027_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un perfil

FUNCTION glbmae027_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando perfil ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.roleid),0)
    INTO  w_mae_pro.roleid
    FROM  glb_rolesusr a
    IF (w_mae_pro.roleid IS NULL) THEN
       LET w_mae_pro.roleid = 1
    ELSE 
       LET w_mae_pro.roleid = (w_mae_pro.roleid+1)
    END IF 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO glb_rolesusr   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.roleid 

   --Asignando el mensaje 
   LET msg = "Perfil (",w_mae_pro.roleid USING "<<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE glb_rolesusr
   SET    glb_rolesusr.*      = w_mae_pro.*
   WHERE  glb_rolesusr.roleid = w_mae_pro.roleid 

   --Asignando el mensaje 
   LET msg = "Perfil (",w_mae_pro.roleid USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando perfiles
   DELETE FROM glb_rolesusr 
   WHERE (glb_rolesusr.roleid = w_mae_pro.roleid)

   --Asignando el mensaje 
   LET msg = "Perfil (",w_mae_pro.roleid USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL glbmae027_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION glbmae027_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.roleid = 0 
   LET w_mae_pro.userid = username 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.roleid THRU w_mae_pro.nmrole 
 DISPLAY BY NAME w_mae_pro.roleid,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION
