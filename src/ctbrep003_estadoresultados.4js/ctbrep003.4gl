{ 
Programa : ctbrep003.4gl                   
Programo : Mynor Ramirez 
Objetivo : Reporte de estado de resultados contable 
}

DATABASE erpjuridico 

-- Definicion de variables globales 

CONSTANT progname               = "ctbrep003" 
CONSTANT CuentasResultado       = 2 
TYPE    datosreporte        RECORD 
         codemp             LIKE ctb_mcuentas.codemp, 
         codgru             LIKE ctb_mcuentas.codgru, 
         numcta             LIKE ctb_mcuentas.numcta,
         nomcta             LIKE ctb_mcuentas.nomcta,
         numniv             LIKE ctb_mcuentas.numniv,
         tipsal             LIKE ctb_mcuentas.tipsal,
         tipope             LIKE ctb_paramrpt.tipope, 
         salact             LIKE ctb_mcuentas.salact, 
         cargos             LIKE ctb_mcuentas.cargos, 
         caracu             LIKE ctb_mcuentas.caracu, 
         abonos             LIKE ctb_mcuentas.abonos, 
         aboacu             LIKE ctb_mcuentas.aboacu,
         salant             DEC(14,2), 
         carant             DEC(14,2), 
         aboant             DEC(14,2) 
        END RECORD 
DEFINE  datos               RECORD 
         codemp             LIKE ctb_mcuentas.codemp, 
         numniv             LIKE ctb_mcuentas.numniv, 
         fecini             DATE, 
         fecfin             DATE,
         ctadet             SMALLINT 
        END RECORD
DEFINE  p                   RECORD
         length             SMALLINT,
         topmg              SMALLINT,
         botmg              SMALLINT,
         lefmg              SMALLINT,
         rigmg              SMALLINT
        END RECORD
DEFINE  w_mae_emp           RECORD LIKE glb_empresas.*
DEFINE  gcodemp             LIKE glb_empresas.codemp 
DEFINE  porcenimptos        DEC(5,2) 
DEFINE  saldo,imptos        DEC(14,2) 
DEFINE  existe              SMALLINT
DEFINE  tituloreporte       STRING 
DEFINE  filename            STRING
DEFINE  pipeline            STRING
DEFINE  s,d                 CHAR(1) 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarReportes1")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("restadoresultados")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL ctbrep003_EstadoResultados() 
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION ctbrep003_EstadoResultados() 
 DEFINE imp1      datosreporte,
        wpais     VARCHAR(255),
        qrytxt    STRING,
        qryemp    STRING,
        qrypart   STRING,
        strctadet STRING,
        loop      SMALLINT,
        w         ui.Window, 
        f         ui.Form   

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep003a AT 5,2
  WITH FORM "ctbrep003a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/estadoresultadoscontable.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox de empresas
  LET gcodemp = librut003_DCbxEmpresaEnUsoXUsuario()
  IF gcodemp IS NULL THEN
     CALL fgl_winmessage(
     "Atencion","Usuario sin empresa asignada, VERIFICA.","stop")
     CLOSE WINDOW wrep003a 
     RETURN
  END IF

  -- Inicio del loop
  OPTIONS INPUT WRAP 
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   LET porcenimptos = 0.25 
   INITIALIZE pipeline,datos.* TO NULL
   LET datos.codemp = gcodemp 
   LET datos.ctadet = 0 
   LET s = 1 SPACE
   LET saldo = 0 
   CLEAR FORM
   DISPLAY BY NAME datos.codemp  

   -- Ingresando datos 
   DIALOG ATTRIBUTES(UNBUFFERED,FIELD ORDER FORM)

    -- Construyendo busqueda
    INPUT BY NAME datos.fecini, 
                  datos.fecfin,
                  datos.ctadet 
     ATTRIBUTE(WITHOUT DEFAULTS) 
    END INPUT 

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT DIALOG

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 
     LET p.length = 40
     LET p.topmg  = 3
     LET p.botmg  = 2
     LET p.lefmg  = 2
     LET p.rigmg  = 2

     -- Verificando ingreso de filtros
     IF NOT ctbrep003_FiltrosCompletos() THEN 
        CONTINUE DIALOG 
     END IF 
     EXIT DIALOG 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 
     LET p.length = 51
     LET p.topmg  = 0
     LET p.botmg  = 0
     LET p.lefmg  = 0
     LET p.rigmg  = 0

     -- Verificando ingreso de filtros
     IF NOT ctbrep003_FiltrosCompletos() THEN 
        CONTINUE DIALOG  
     END IF 
     EXIT DIALOG 

    ON ACTION excel 
     -- Asignando dispositivo 
     LET pipeline = "excel" 
     LET s        = ASCII(9) 
     LET p.length = 51
     LET p.topmg  = 0
     LET p.botmg  = 0
     LET p.lefmg  = 0
     LET p.rigmg  = 0

     -- Verificando ingreso de filtros
     IF NOT ctbrep003_FiltrosCompletos() THEN 
        CONTINUE DIALOG 
     END IF 
     EXIT DIALOG

   END DIALOG 
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Obteniendo datos de la empresa
   INITIALIZE w_mae_emp.* TO NULL
   CALL librut003_BEmpresa(datos.codemp) 
   RETURNING w_mae_emp.*,existe 

   -- Calculando maximo nivel de la nomenclatura
   IF datos.numniv IS NULL THEN 
      SELECT (NVL(a.numniv,1)-0) 
       INTO  datos.numniv
       FROM  ctb_tiposnom a
       WHERE a.tipnom = w_mae_emp.tipnom  
   END IF 

   -- Verificando condicion de incluir cuentas de detalle 
   LET strctadet = " AND a.tipcta IN  ('M') "
   IF datos.ctadet=1 THEN
      LET strctadet = " AND a.tipcta IN  ('M','D') "
   END IF 

   -- Construyendo seleccion 
   LET qrytxt = 
    "SELECT a.codemp,a.codgru,a.numcta,a.nomcta,a.numniv,",
           "a.tipsal,0,0,0,0,0,0,0,0,0 ",
     "FROM  ctb_mcuentas a ",
     "WHERE a.codemp = ",datos.codemp,
      strctadet CLIPPED,
      " AND a.tipcat = ",CuentasResultado, 
      " AND a.numniv <= "||datos.numniv, 
      " ORDER BY a.codemp,a.codgru,a.numcta" 

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep003 FROM qrytxt 
   DECLARE c_crep003 CURSOR FOR c_rep003
   LET existe = FALSE
   FOREACH c_crep003 INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       LET existe = TRUE

       -- Iniciando reporte 
       START REPORT ctbrep003_ImprimirDatos TO filename 
    END IF 

    -- Obteniendo saldos de la cuenta
    CALL ctbrep003_SaldoCuentasContables(imp1.codemp,
                                         imp1.numcta,
                                         datos.fecini,
                                         datos.fecfin)
    RETURNING imp1.salant,imp1.salact,imp1.cargos,imp1.abonos

    -- Llenando el reporte
    OUTPUT TO REPORT ctbrep003_ImprimirDatos(imp1.*)
   END FOREACH
   CLOSE c_crep003 
   FREE  c_crep003 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT ctbrep003_ImprimirDatos 

      -- Transfiriendo reporte a excel
      IF pipeline = "excel" THEN
        CALL ctbrep003_excel(filename)
      ELSE
        -- Enviando reporte al destino seleccionado
        CALL librut001_sendreport
        (filename,pipeline,tituloreporte,
        "--noline-numbers "||
        "--nofooter "||
        "--font-size 8 "||
        "--page-width 842 --page-height 595  "||
        "--left-margin 35 --right-margin 15 "||
        "--top-margin 30 --bottom-margin 35 "||
        "--title Contabilidad")
      END IF

      ERROR "" 
      CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(
      " Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF  
  END WHILE

  OPTIONS INPUT NO WRAP 

 CLOSE WINDOW wrep003a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION ctbrep003_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE

 -- Verificando datos 
 IF datos.codemp IS NULL OR
    datos.fecini IS NULL OR 
    datos.fecfin IS NULL THEN 
    CALL fgl_winmessage(
    " Atencion",
    " Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte 

REPORT ctbrep003_ImprimirDatos(imp1)
 DEFINE imp1                datosreporte,
        utibru,utinet       DEC(14,2), 
        linea               CHAR(110),
        i,col,lg            INTEGER,       
        fechareporte        STRING,
        signo               CHAR(5)

  OUTPUT LEFT MARGIN   p.lefmg
         TOP MARGIN    p.topmg
         BOTTOM MARGIN p.botmg
         PAGE LENGTH   p.length

  FORMAT 
   PAGE HEADER

    -- Llenando linea
    LET lg = 110 
    LET linea = NULL
    FOR i = 1 TO lg
     LET linea = linea CLIPPED,"-"
    END FOR  

    -- Imprimiendo Encabezado
    LET col = librut001_centrado(tituloreporte,lg) 
    PRINT COLUMN   1,"Contabilidad",
          COLUMN col,UPSHIFT(tituloreporte) CLIPPED,  
	  COLUMN (lg-20),PAGENO USING "Pagina: <<<<"

    LET col = librut001_centrado("Expresado en Quetzales",lg) 
    PRINT COLUMN   1,"Ctbrep003",
          COLUMN col,"Expresado en Quetzales",
          COLUMN (lg-20),"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    LET fechareporte = "DEL [",datos.fecini,"] AL [ ",datos.fecfin," ]" 
    LET col = librut001_centrado(fechareporte,lg) 

    PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
          COLUMN col,fechareporte CLIPPED,
          COLUMN (lg-20),"Hora  : ",TIME 

    -- Verificando detalle
    IF (datos.ctadet=0) THEN 
       PRINT "Cuentas de Detalle No Incluidas"
    ELSE 
       PRINT "Cuentas de Detalle Incluidas" 
    END IF    

    PRINT linea 
    PRINT w_mae_emp.nomemp CLIPPED
    SKIP 1 LINES 

  BEFORE GROUP OF imp1.codemp 
   -- Verificando si reporte es a excel
   IF (PAGENO=1) THEN 
    IF (pipeline="excel") THEN
      -- Imprimiendo datos 
      PRINT tituloreporte CLIPPED,s
      PRINT w_mae_emp.nomemp CLIPPED,s
      PRINT "EXRESADO EN QUETZALES",s 
      PRINT "PERIODO DEL [",datos.fecini,"] AL [ ",datos.fecfin," ]",s
      PRINT s
    END IF 
   END IF
   PRINT COLUMN  95,s,"ACUMULADO",s 

  BEFORE GROUP OF imp1.codgru
   LET signo = NULL 
   CASE (imp1.codgru)
    WHEN 4 PRINT "( + ) INGRESOS",s
    WHEN 5 PRINT "( - ) GASTOS",s 
   END CASE 

  ON EVERY ROW 
   IF (imp1.numniv=1) THEN 
    CASE (imp1.codgru)
     WHEN 4 LET saldo = (saldo+imp1.salact) 
     WHEN 5 LET saldo = (saldo-imp1.salact) 
    END CASE 
   END IF 

   -- Verificando si reporte no es a excel 
   IF (pipeline!="excel") THEN 
      PRINT COLUMN   1,imp1.numcta,
            COLUMN 20+imp1.numniv,imp1.nomcta,
            COLUMN  90,imp1.salact USING "----,---,--&.&&"  
   ELSE
      PRINT "'",imp1.numcta CLIPPED,s, 
            imp1.nomcta CLIPPED,s,
            imp1.salact,s
   END IF 

  AFTER GROUP OF imp1.codgru
   PRINT s

  AFTER GROUP OF imp1.codemp 
   -- Inicializando totales 
   LET utibru = saldo 
   LET imptos = (utibru*porcenimptos) 
   LET utinet = (utibru-imptos) 

   -- Verificando si reporte no es a excel 
   IF (pipeline!="excel") THEN 
    -- Imprimiendo Utilidad Bruta 
    PRINT COLUMN   1,"( = ) UTILIDAD (PERDIDA) BRUTA",
          COLUMN  90,utibru USING "----,---,--&.&&" 

    -- Imprimiendo Utilidad Neta  
    PRINT COLUMN  89," _______________"
    PRINT COLUMN   1,"( = ) UTILIDAD (PERDIDA) ANTES DE IMPUESTOS",
          COLUMN  90,utibru USING "----,---,--&.&&"
    PRINT COLUMN   1,"( - ) IMPUESTOS A LA UTILIDAD",
          COLUMN  90,imptos USING "----,---,--&.&&"
    PRINT COLUMN  89," _______________"
    PRINT COLUMN   1,"( = ) UTILIDAD (PERDIDA) NETA",
          COLUMN  90,utinet USING "----,---,--&.&&"
    PRINT COLUMN  89," ===============" 
   ELSE
    -- Imprimiendo Utilidad Bruta 
    PRINT "( = ) UTILIDAD (PERDIDA) BRUTA",s,s,
          utibru USING "----,---,--&.&&" 

    -- Imprimiendo Utilidad Neta  
    PRINT s
    PRINT "( = ) UTILIDAD (PERDIDA) ANTES DE IMPUESTOS",s,s,
          utibru USING "----,---,--&.&&"
    PRINT "( - ) IMPUESTOS A LA UTILIDAD",s,s,
          imptos USING "----,---,--&.&&"
    PRINT s
    PRINT "( = ) UTILIDAD (PERDIDA) NETA",s,s,
          utinet USING "----,---,--&.&&"
   END IF 
END REPORT 

-- Subrutina para exportar el reporte a excel usando WinCOM 

FUNCTION ctbrep003_excel(filename) 
 DEFINE sb          base.StringBuffer
 DEFINE ch          base.channel
 DEFINE lista       base.StringTokenizer
 DEFINE result      INTEGER
 DEFINE i           INTEGER
 DEFINE xlapp       INTEGER
 DEFINE xlwb        INTEGER
 DEFINE linea       STRING 
 DEFINE lineadep    STRING 
 DEFINE filename    STRING 
 DEFINE columna     STRING 
 DEFINE separador   CHAR(1) 
 DEFINE nuevalinea  CHAR(1) 

 -- Inicializando variables                            
 LET xlapp = -1
 LET xlwb = -1

 -- Creando la instancia de Excel
 CALL ui.Interface.frontCall("WinCOM", "CreateInstance", ['Excel.Application'], [xlapp])
 CALL CheckError(xlapp, xlwb, xlapp)
   
 -- Agregando un libro al documento de excel               
 CALL ui.interface.frontCall("WinCOM", "CallMethod", [xlapp, 'WorkBooks.Add'], [xlwb])
 CALL CheckError(xlapp, xlwb, xlwb)
   
 -- Haciendo visible el libro                 
 CALL excel_set_property(xlapp, xlapp, "Visible","True")
  
 -- Llenando celdas desde el FrontCall 
 -- Construyendo celdas delimitando columnas por TAB (ASCII(9)), 
 -- y filas por CR (ASCII(10)
 -- Copia al clipboard via el Frontcall y luego instruye a excel para pegar el contenido
 -- de la celdas dentro del libro 
 LET sb = base.StringBuffer.create()

 -- Leyendo el archivo que contiene el reporte y copiandolo al clipboard 
 LET ch = base.channel.create()
 call ch.openfile(filename,"r")

 let i = 1 
 let separador  = ascii(9) 
 let nuevalinea = ascii(10)
 while true
  let linea = ch.readLine() 
  if ch.isEof() then exit while end if
  if linea.getindexof(separador,1)>0 then
     CALL sb.append(linea)
     CALL sb.append(nuevalinea)
  end if 
  let i = i+1
 end while
 call ch.close() 

 -- Pegando el contenido de las celdas en el clipboard al libro de excel
 CALL ui.Interface.FrontCall("standard","cbset",sb.toString(),result)
 CALL excel_call_method(xlapp, xlwb, 'ActiveSheet.Paste')
 CALL excel_call_method(xlapp, xlwb, 'Columns("A:Z").EntireColumn.AutoFit')
 CALL excel_set_property(xlapp, xlwb, excel_column("C","C","NumberFormat"), "0.00")

 -- Liberando informacion del clipboard    
 CALL freeMemory(xlapp, xlwb)
END FUNCTION

-- Subrutina para encontrar los saldos de las cuentas contables en base a un un periodo 

FUNCTION ctbrep003_SaldoCuentasContables(w_codemp,w_numcta,w_fecini,w_fecfin)
 DEFINE w_codemp  LIKE ctb_mcuentas.codemp,
        w_numcta  LIKE ctb_mcuentas.numcta,
        w_salant  LIKE ctb_mcuentas.salant,
        w_saldos  LIKE ctb_mcuentas.salact, 
        w_salcom  LIKE ctb_mcuentas.salact, 
        w_cargos  LIKE ctb_mcuentas.cargos,   
        w_abonos  LIKE ctb_mcuentas.abonos,   
        w_fecini  LIKE ctb_mtransac.fecemi,
        w_fecfin  LIKE ctb_mtransac.fecemi,
        existe    SMALLINT

 -- Inicializando datos
 LET w_saldos = 0 
 LET w_cargos = 0
 LET w_abonos = 0

 -- Calculando saldo anterior 
 SELECT NVL(SUM(x.salact),0) 
  INTO  w_salant 
  FROM  ctb_ctransac x,ctb_mtransac a,ctb_tipostrn t 
  WHERE (a.lnktra = x.lnktra) AND
        (x.codemp = w_codemp) AND
        (x.numcta = w_numcta) AND
        (x.fecemi < w_fecini) AND
        (t.tiptrn = a.tiptrn) AND 
        (t.tipope = 1) -- Movimiento 

 -- Sumando cargos, abonos y saldo actual 
 SELECT NVL(SUM(x.cargos),0),
        NVL(SUM(x.abonos),0), 
        NVL(SUM(x.salact),0) 
  INTO  w_cargos,
        w_abonos,
        w_salcom 
  FROM  ctb_ctransac x,ctb_mtransac a,ctb_tipostrn t 
  WHERE (a.lnktra =  x.lnktra) AND
        (x.codemp =  w_codemp) AND
        (x.numcta =  w_numcta) AND
        (x.fecemi >= w_fecini) AND
        (x.fecemi <= w_fecfin) AND
        (t.tiptrn =  a.tiptrn) AND 
        (t.tipope = 1) -- Movimiento 

 LET w_saldos = w_salcom+w_salant

 RETURN w_salant,w_saldos,w_cargos,w_abonos
END FUNCTION
