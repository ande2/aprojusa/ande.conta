{ 
Credicorp
Programa : ctbre070.4gl
Autor    : Francisco Rivera 
Fecha    : Marzo 1995. 
Objetivo : Reporte estado de perdidas y ganancias.
}

{ Definicion de variables globales }

GLOBALS "../cta/ctagb001.4gl"
 DEFINE w_mae_emp         RECORD LIKE empresas.*,
        w_ult_cie         RECORD LIKE gb_mcierres.*,
        w_ult_cta         RECORD LIKE cb_saldomes.*, 
        w_femision        LIKE cb_mtransac.f_emision,
        w_iniper          LIKE cb_mcuentas.f_apertura,
        w_ganancia_acum,
        w_ganacia_mes,
        w_operacion_acum,
        w_operacion_mes,
        w_otros_acum,
        w_otros_mes,
        w_total_acum,
        w_diferencia_mes,
        w_diferencia_tmp,
        w_total_mes       DECIMAL(10,2),
        w_periodo1        CHAR(50)

{ Subrutina para seleccionar las cuentas del estado  de perdidas y ganancias }

FUNCTION re070_pergan() 
 DEFINE w_mae_ctb       RECORD 
         cuenta          LIKE cb_mcuentas.cuenta,
         empresa         LIKE cb_mcuentas.empresa,
         nombre          LIKE cb_mcuentas.nombre,
         abreviado       LIKE cb_mcuentas.abreviado,
         saldo_ini       LIKE cb_mcuentas.saldo_ini,
         saldo_act       LIKE cb_mcuentas.saldo_act,
         saldo_ant       LIKE cb_mcuentas.saldo_ant,
         saldo_eje       LIKE cb_mcuentas.saldo_eje,
         f_apertura      LIKE cb_mcuentas.f_apertura,
         hora            LIKE cb_mcuentas.hora,
         usuario         LIKE cb_mcuentas.usuario,
         terminal        LIKE cb_mcuentas.terminal,
         nivel           LIKE cb_mcuentas.nivel,
         empcta          LIKE cb_mcuentas.empcta,
         catalogo        LIKE cb_mcuentas.catalogo,
         tipo_cta        LIKE cb_mcuentas.tipo_cta,
         tipo_saldo      LIKE cb_mcuentas.tipo_saldo,
         grupo           LIKE cb_mcuentas.grupo,
         cargos          LIKE cb_mcuentas.cargos,
         abonos          LIKE cb_mcuentas.abonos,
         cargos_a        LIKE cb_mcuentas.cargos_a,
         abonos_a        LIKE cb_mcuentas.abonos_a,
         cuenta_pa       LIKE cb_mcuentas.cuenta_pa,
         cuenta_p1       LIKE cb_mcuentas.cuenta_p1,
         afecto_iva      LIKE cb_mcuentas.afecto_iva,
         aplica_tc       LIKE cb_mcuentas.aplica_tc,
         operacion       LIKE cb_creareps.operacion
        END RECORD, 
        w_inimes        LIKE cb_mcuentas.f_apertura,
        loop,exis       SMALLINT,
        w_query,w_texto CHAR(500),
        msg             CHAR(80),
        hilera          CHAR(1),
        w_separa        SMALLINT,
        w_ctb_mae       RECORD LIKE cb_mcuentas.*,
        w_mae_ctr       RECORD LIKE cb_ctransac.*,
        w_mae_tra       RECORD LIKE cb_mtransac.*,
        w_mae_trn       RECORD LIKE cb_tipotrns.*,
        w_mae_agr       RECORD LIKE cb_agructas.*,
        w_salcta        RECORD
           empresa         LIKE cb_mcuentas.empresa,
           cuenta          LIKE cb_mcuentas.cuenta,
           debe            DECIMAL(14,2),
           haber           DECIMAL(14,2),
           operacion       LIKE cb_ctransac.operacion
                        END RECORD

 # Abriendo la forma para el reporte
 OPEN WINDOW wre070 AT 6,3
  WITH FORM "ctbfre70a" ATTRIBUTE(BORDER,FORM LINE 1,MESSAGE LINE LAST)
  CALL messages(1,0,3,78,"Emision Estado de Perdidas y Ganancias")

  # Definiendo archivo de impresion
  LET filename = ARG_VAL(3) CLIPPED,"/ctbre070.spl"

  LET msg = "Ingrese seleccion y presione Esc. F4 Regreso Ctrl-I Reimpresion"
  CALL msgup(1,msg,3)
  CURRENT WINDOW is wre070

  # Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   # Inicializando datos
   INITIALIZE w_mae_ctb.*,w_femision,w_query,w_texto TO NULL
   CALL re070_inivar()
   LET w_femision = TODAY
   CLEAR FORM
   DISPLAY BY NAME w_femision

   # Construyendo la seleccion
   CONSTRUCT BY NAME w_query ON c.empresa,       
                                c.cuenta,    
                                c.nombre,    
                                c.nivel    
     
    ON KEY(F4)
     # Salida
     LET loop = FALSE
     EXIT CONSTRUCT 
    ON KEY(F1)
     # Lista de empresas
     IF INFIELD(empresa) THEN
        LET hilera = view_empresas()
     END IF 
    ON KEY(CONTROL-I)
     # Recuperando ultimo reporte
     IF acceso("CB","reprint") THEN 
        CALL reprint(filename)
     END IF 
    ON KEY(INTERRUPT)
     ERROR "Tecla invalida."
    AFTER CONSTRUCT
     IF NOT FIELD_TOUCHED(c.empresa) THEN
        CALL msgerr(" Atencion: debes ingresar la empresa a listar.")
        # Inicializando datos
        INITIALIZE w_mae_ctb.*,w_femision,w_query,w_texto TO NULL
        LET w_femision = TODAY
        CLEAR FORM
        DISPLAY BY NAME w_femision
        NEXT FIELD empresa 
     END IF 

     # Ingresando fecha a la cual se desea el reporte
     IF re070_ingfecha() THEN
        CONTINUE CONSTRUCT
     END IF 
     # ======= Criterios de busqueda =================
     LET wc_empresa       = GET_FLDBUF(c.empresa)
     LET wc_cuenta        = GET_FLDBUF(c.cuenta)
     LET wc_nombre        = GET_FLDBUF(c.nombre)
     LET wc_nivel         = GET_FLDBUF(c.nivel)
     LET wc_periodo       = "AL ",w_femision USING "dd/mmm/yyyy" 
   END CONSTRUCT
   LET w_periodo1 = "AL ",w_femision USING "dd/mmm/yyyy" 

   # Seleccionando dispositivo
   IF loop THEN
      LET impress = device(6,60)
      IF impress IS NULL THEN
         CONTINUE WHILE 
      END IF
   ELSE
      EXIT WHILE 
   END IF 

   # Construyendo seleccion 
   LET w_texto = "SELECT c.*,r.operacion ",
                  "FROM  cb_mcuentas c,cb_creareps r ",
                  "WHERE c.empresa     = c.empcta",
                  #" AND  c.f_apertura <= \"",w_femision,"\"",
                  " AND  c.tipo_cta    = \"M\" ",
                  " AND  c.cuenta_p1   = r.cuenta_p1 ",
                  " AND  r.reporte     = 2 ",
                  " AND  ",w_query CLIPPED,
                  " ORDER BY 2, 1, 23, 24 "

   # Preparando seleccion
   PREPARE c_re070 FROM w_texto
   DECLARE c_cre070 CURSOR FOR c_re070
   LET existe = FALSE
   LET w_separa = TRUE
   FOREACH c_cre070 INTO w_mae_ctb.*

    # Iniciando reporte
    IF NOT existe THEN

       # Creando tabla temporal para separar transacciones del periodo
       CREATE TEMP TABLE cb_saldocta
        (empresa     CHAR(2),
         cuenta      CHAR(16),
         debe        DECIMAL(14,2),
         haber       DECIMAL(14,2),
         operacion   CHAR(01)) WITH NO LOG
        CREATE INDEX ix_001ss ON cb_saldocta(empresa,cuenta)

       # Creando tabla temporal para sumas
        CREATE TEMP TABLE cb_sumasrep
        (empresa   CHAR(2),
         cuenta    CHAR(16),
         carabo    DECIMAL(10,2),
         acumulado DECIMAL(10,2),
         saldo     CHAR(1), 
         nivel     SMALLINT,
         operacion CHAR(2)) WITH NO LOG
        CREATE INDEX ix_001s1 ON cb_sumasrep(operacion,saldo,nivel)
 
       # Seleccionando fonts
       CALL fonts(impress) RETURNING fnt.* 

       LET existe = TRUE
       START REPORT re070_irepgan TO filename
       ERROR "Atencion: seleccionando datos ... por favor espere ..."
    END IF 

    # Buscando ultimo cierre de la empresa de la cuenta
    INITIALIZE w_ult_cie.* TO NULL
    CALL dlast_cierre("CB",1,w_mae_ctb.empresa,w_femision)
    RETURNING w_ult_cie.*,exis

    # Verificando si existe cierre
    IF NOT exis THEN
       LET w_mae_ctb.saldo_ant = w_mae_ctb.saldo_ini
       LET w_mae_ctb.cargos_a  = 0
       LET w_mae_ctb.abonos_a  = 0
       LET w_inimes = 
       MDY(MONTH(w_mae_ctb.f_apertura),1,YEAR(w_mae_ctb.f_apertura))
       LET w_iniper = NULL 
    ELSE
       IF (w_ult_cie.default = "S") THEN 
          LET w_mae_ctb.saldo_ant = w_mae_ctb.saldo_ini
          LET w_mae_ctb.cargos_a  = 0
          LET w_mae_ctb.abonos_a  = 0
          LET w_inimes = 
          MDY(MONTH(w_mae_ctb.f_apertura),1,YEAR(w_mae_ctb.f_apertura))
          LET w_iniper = NULL 
       ELSE
          LET w_inimes=(w_ult_cie.f_cierre+1)
          LET w_iniper=w_inimes 

          # Buscando saldos del ultimo cierre
          INITIALIZE w_ult_cta.* TO NULL
          CALL last_cierrecta(w_mae_ctb.empresa,w_mae_ctb.cuenta,
                              w_ult_cie.f_cierre,"M")
          RETURNING w_ult_cta.*,exis

          LET w_mae_ctb.saldo_ant  = w_ult_cta.saldo_mes 
          LET w_mae_ctb.saldo_eje  = w_ult_cta.saldo_eje
       END IF 
    END IF 


    # Separando las transacciones del periodo
    IF w_separa THEN

       # Preparando seleccion

       INITIALIZE w_ctb_mae.* TO NULL
       PREPARE c_txt FROM w_texto
       DECLARE c_ctas CURSOR FOR c_txt
       FOREACH c_ctas INTO w_ctb_mae.*

        # Busca transacciones desglodas
        DECLARE c_ctransper CURSOR FOR
         SELECT d.*
          FROM  cb_ctransac d
          WHERE (d.empresa  = w_ctb_mae.empresa)
            AND (d.cuenta   = w_ctb_mae.cuenta)
            AND (d.f_emision>=w_inimes)
            AND (d.f_emision<=w_femision)
          FOREACH c_ctransper INTO w_mae_ctr.*
     
           SELECT a.*
            INTO  w_mae_tra.*
            FROM  cb_mtransac a
            WHERE (a.link = w_mae_ctr.link)
            IF (STATUS=NOTFOUND) THEN
               CONTINUE FOREACH
            END IF
      
            # Busca tipo de transaccion
            CALL busca_tiptrncb(w_mae_tra.tipo_trns) 
            RETURNING w_mae_trn.*,existe
            IF (w_mae_trn.funcion MATCHES "[RC]") THEN 
               CONTINUE FOREACH 
            END IF 
    
            MESSAGE "Procesando cuenta ",w_mae_ctr.cuenta
    
            INITIALIZE w_salcta.* TO NULL
            SELECT c.*
             INTO  w_salcta.*
             FROM  cb_saldocta c
             WHERE (c.empresa   = w_mae_ctr.empresa)
               AND (c.cuenta    = w_mae_ctr.cuenta)
               AND (c.operacion = w_mae_ctr.operacion)
    
             IF (STATUS=NOTFOUND) THEN
                 INSERT INTO cb_saldocta
                  VALUES(w_mae_ctr.empresa,w_mae_ctr.cuenta,w_mae_ctr.debe,
                         w_mae_ctr.haber,w_mae_ctr.operacion)
             ELSE 
                 LET w_salcta.debe = (w_salcta.debe +w_mae_ctr.debe)
                 LET w_salcta.haber= (w_salcta.haber+w_mae_ctr.haber)
                 IF (w_mae_ctr.operacion="D") THEN
                     UPDATE cb_saldocta
                      SET   cb_saldocta.debe = w_salcta.debe
                     WHERE (cb_saldocta.empresa   = w_mae_ctr.empresa)
                       AND (cb_saldocta.cuenta    = w_mae_ctr.cuenta)
                       AND (cb_saldocta.operacion = w_mae_ctr.operacion)
                 ELSE 
                     UPDATE cb_saldocta
                      SET   cb_saldocta.haber = w_salcta.haber
                     WHERE (cb_saldocta.empresa   = w_mae_ctr.empresa)
                       AND (cb_saldocta.cuenta    = w_mae_ctr.cuenta)
                       AND (cb_saldocta.operacion = w_mae_ctr.operacion)
                 END IF 
             END IF 
          END FOREACH 
          CLOSE c_ctransper 
          FREE  c_ctransper 

          # Busca empresa
          INITIALIZE w_mae_ctr.*,w_mae_trn.*,w_salcta.* TO NULL
   
{ 
          # Selecciona cuentas del consolidado
          DECLARE c_ctascon3 CURSOR FOR
           SELECT a.*
            FROM  cb_agructas a
            WHERE (a.emp_consol = w_ctb_mae.empresa)
              AND (a.cta_consol = w_ctb_mae.cuenta)
              AND (a.empresa    IS NOT NULL)
              AND (a.cta_normal IS NOT NULL)
          ORDER BY a.empresa,a.cta_normal
          FOREACH c_ctascon3 INTO w_mae_agr.*
     
           # Verifica si ya fue actualizada cuenta del debe
           SELECT x.*
            FROM  cb_saldocta x
            WHERE (x.empresa = w_mae_agr.empresa)
              AND (x.cuenta  = w_mae_agr.cta_normal)
              AND (x.operacion="D")
           IF (STATUS!=NOTFOUND) THEN
              CONTINUE FOREACH
           END IF
    
           # Verifica si ya fue actualizada cuenta del haber
           SELECT x.*
            FROM  cb_saldocta x
            WHERE (x.empresa = w_mae_agr.empresa)
              AND (x.cuenta  = w_mae_agr.cta_normal)
              AND (x.operacion="H")
           IF (STATUS!=NOTFOUND) THEN
              CONTINUE FOREACH
           END IF
    
           # Busca transacciones desglodas
           DECLARE c_ctransper1 CURSOR FOR
            SELECT d.*
              FROM  cb_ctransac d
             WHERE (d.empresa  = w_mae_agr.empresa)
               AND (d.cuenta   = w_mae_agr.cta_normal)
               AND (d.f_emision>=w_inimes)
               AND (d.f_emision<=w_femision)
           FOREACH c_ctransper1 INTO w_mae_ctr.*
    
            MESSAGE "Actualizando cuenta ",w_mae_ctb.empresa CLIPPED," ",
                                           w_mae_ctb.cuenta  CLIPPED," ",
                                           w_mae_agr.empresa CLIPPED," ",
                                           w_mae_agr.cta_normal
             
            INITIALIZE w_mae_tra.* TO NULL
            SELECT a.*
             INTO  w_mae_tra.*
             FROM  cb_mtransac a
             WHERE (a.link = w_mae_ctr.link)
            IF (STATUS=NOTFOUND) THEN
               CONTINUE FOREACH
            END IF
    
            # Busca tipo de transaccion
            CALL busca_tiptrncb(w_mae_tra.tipo_trns) 
            RETURNING w_mae_trn.*,existe
            IF (w_mae_trn.funcion MATCHES "[RC]") THEN 
                CONTINUE FOREACH 
            END IF 
            
              
            # Verifica si ya ha sido actualizada
            INITIALIZE w_salcta.* TO NULL
            SELECT c.*
             INTO  w_salcta.*
             FROM  cb_saldocta c
             WHERE (c.empresa   = w_mae_ctr.empresa)
               AND (c.cuenta    = w_mae_ctr.cuenta)
               AND (c.operacion = w_mae_ctr.operacion)
            
            IF (STATUS=NOTFOUND) THEN
                INSERT INTO cb_saldocta
                VALUES(w_mae_ctr.empresa,w_mae_ctr.cuenta,w_mae_ctr.debe,
                       w_mae_ctr.haber,w_mae_ctr.operacion)
            ELSE 
                LET w_salcta.debe = (w_salcta.debe +w_mae_ctr.debe)
                LET w_salcta.haber= (w_salcta.haber+w_mae_ctr.haber)
                IF (w_mae_ctr.operacion="D") THEN
                     UPDATE cb_saldocta
                      SET   cb_saldocta.debe = w_salcta.debe
                     WHERE (cb_saldocta.empresa   = w_mae_ctr.empresa)
                       AND (cb_saldocta.cuenta    = w_mae_ctr.cuenta)
                       AND (cb_saldocta.operacion = w_mae_ctr.operacion)
                ELSE 
                     UPDATE cb_saldocta
                      SET   cb_saldocta.haber = w_salcta.haber
                     WHERE (cb_saldocta.empresa   = w_mae_ctr.empresa)
                       AND (cb_saldocta.cuenta    = w_mae_ctr.cuenta)
                       AND (cb_saldocta.operacion = w_mae_ctr.operacion)
                END IF 
            END IF 
           END FOREACH 
           CLOSE c_ctransper1 
           FREE  c_ctransper1 
          END FOREACH 
          CLOSE c_ctascon3 
          FREE  c_ctascon3 
}

       END FOREACH 
       CLOSE c_ctas 
       FREE  c_ctas 
       LET w_separa = FALSE
    END IF 


    # Verifica datos
    IF (w_mae_ctb.saldo_ant IS NULL) THEN
       LET w_mae_ctb.saldo_ant = 0
    END IF 
    IF (w_mae_ctb.cargos_a IS NULL) THEN
       LET w_mae_ctb.cargos_a = 0
    END IF 
    IF (w_mae_ctb.abonos_a IS NULL) THEN
       LET w_mae_ctb.abonos_a = 0
    END IF 
    IF (w_mae_ctb.saldo_eje IS NULL) THEN
       LET w_mae_ctb.saldo_eje = 0
    END IF 
{
    # Encontrando saldo al dia
    CALL saldo_cuenta(w_mae_ctb.empresa,w_mae_ctb.cuenta,w_mae_ctb.tipo_saldo,
                      w_mae_ctb.saldo_ant,w_inimes,w_femision)
    RETURNING w_mae_ctb.saldo_act,w_mae_ctb.cargos,w_mae_ctb.abonos 
}
    # Encontrando saldo al dia
    CALL saldo_ctasemp(w_mae_ctb.empresa,w_mae_ctb.cuenta,w_mae_ctb.tipo_saldo,
                       w_mae_ctb.saldo_ant,w_inimes,w_femision)
    RETURNING w_mae_ctb.saldo_act,w_mae_ctb.cargos,w_mae_ctb.abonos 

    # Validando que no sean nulos
    IF w_mae_ctb.saldo_act IS NULL THEN
       LET w_mae_ctb.saldo_act = 0
    END IF 
    IF w_mae_ctb.cargos IS NULL THEN
       LET w_mae_ctb.cargos = 0
    END IF 
    IF w_mae_ctb.abonos IS NULL THEN
       LET w_mae_ctb.abonos = 0
    END IF 

    # Diferencia de total y colocarle el signo dependiendo de su tipo de saldo
    # ya sea acreedor o deudor de acuerdo al mayor de cargo y abono

    LET w_diferencia_mes = 0
    LET w_diferencia_mes = (w_mae_ctb.cargos-w_mae_ctb.abonos)
 
    IF ((w_diferencia_mes>0) AND (w_mae_ctb.tipo_saldo = "D")) THEN
       LET w_diferencia_mes= w_diferencia_mes
    ELSE  
       IF ((w_diferencia_mes<0) AND (w_mae_ctb.tipo_saldo = "D")) THEN
          LET w_diferencia_mes= (w_diferencia_mes)
       ELSE  
          IF ((w_diferencia_mes>0) AND (w_mae_ctb.tipo_saldo = "A")) THEN
             LET w_diferencia_mes= (w_diferencia_mes*-1)
          ELSE
             IF ((w_diferencia_mes<0) AND (w_mae_ctb.tipo_saldo = "A")) THEN
                 LET w_diferencia_mes= (w_diferencia_mes*-1)
             ELSE 
                 LET w_diferencia_mes= (w_diferencia_mes*-1)
             END IF
          END IF
       END IF
    END IF

    # Llenando tabla temporal
    IF (w_mae_ctb.nivel = 1) THEN 
    INSERT INTO cb_sumasrep
    VALUES (w_mae_ctb.empresa,w_mae_ctb.cuenta,w_diferencia_mes,
            w_mae_ctb.saldo_act,w_mae_ctb.tipo_saldo,w_mae_ctb.nivel,
            w_mae_ctb.operacion)
    END IF

    # Llenando el reporte
    OUTPUT TO REPORT re070_irepgan(w_mae_ctb.*)
   END FOREACH
   IF existe THEN
      # Finalizando el reporte
      FINISH REPORT re070_irepgan 

      # Borrando la tabla temporal
       DROP TABLE cb_saldocta
       DROP TABLE cb_sumasrep

      # Imprimiendo el reporte
      CALL to_printer(filename,impress,"132") 
   ELSE
      # Notificar que no existen datos
      CALL msgbusqueda()
   END IF 
  END WHILE
 CLOSE WINDOW wre070
 CLOSE WINDOW wmsgup3 
END FUNCTION 

{ Subrutina para ingresar la fecha de emision del reporte }    

FUNCTION re070_ingfecha()
 DEFINE regreso,lastkey SMALLINT

 # Ingresando fecha
 LET regreso    = FALSE
 LET w_femision = TODAY 
 DISPLAY BY NAME w_femision 
 INPUT BY NAME w_femision WITHOUT DEFAULTS
  ON KEY(F4)
   # Salida 
   LET regreso = TRUE
   EXIT INPUT 
  ON KEY(INTERRUPT)
   # Atrapandp errores
   ERROR "Tecla Invalida."
  AFTER FIELD w_femision
   LET lastkey = FGL_LASTKEY()
   IF (lastkey = FGL_KEYVAL("up")) OR (lastkey = FGL_KEYVAL("left")) THEN
      LET regreso = TRUE 
      EXIT INPUT
   END IF

   IF w_femision IS NULL THEN
      LET w_femision = TODAY
   END IF 
   DISPLAY BY NAME w_femision 
 END INPUT 
 RETURN regreso
END FUNCTION 

{ Subrutina para imprimir el reporte }

REPORT re070_irepgan(imp1)
 DEFINE imp1                         RECORD 
         cuenta                            LIKE cb_mcuentas.cuenta,
         empresa                           LIKE cb_mcuentas.empresa,
         nombre                            LIKE cb_mcuentas.nombre,
         abreviado                         LIKE cb_mcuentas.abreviado,
         saldo_ini                         LIKE cb_mcuentas.saldo_ini,
         saldo_act                         LIKE cb_mcuentas.saldo_act,
         saldo_ant                         LIKE cb_mcuentas.saldo_ant,
         saldo_eje                         LIKE cb_mcuentas.saldo_eje,
         f_apertura                        LIKE cb_mcuentas.f_apertura,
         hora                              LIKE cb_mcuentas.hora,
         usuario                           LIKE cb_mcuentas.usuario,
         terminal                          LIKE cb_mcuentas.terminal,
         nivel                             LIKE cb_mcuentas.nivel,
         empcta                            LIKE cb_mcuentas.empcta,
         catalogo                          LIKE cb_mcuentas.catalogo,
         tipo_cta                          LIKE cb_mcuentas.tipo_cta,
         tipo_saldo                        LIKE cb_mcuentas.tipo_saldo,
         grupo                             LIKE cb_mcuentas.grupo,
         cargos                            LIKE cb_mcuentas.cargos,
         abonos                            LIKE cb_mcuentas.abonos,
         cargos_a                          LIKE cb_mcuentas.cargos_a,
         abonos_a                          LIKE cb_mcuentas.abonos_a,
         cuenta_pa                         LIKE cb_mcuentas.cuenta_pa,
         cuenta_p1                         LIKE cb_mcuentas.cuenta_p1,
         afecto_iva                        LIKE cb_mcuentas.afecto_iva,
         aplica_tc                         LIKE cb_mcuentas.aplica_tc,
         operacion                         LIKE cb_creareps.operacion
        END RECORD, 
        w_mes_acreedor,w_mes_deudor        LIKE cb_mcuentas.saldo_act,
        w_acum_acreedor,w_acum_deudor      LIKE cb_mcuentas.saldo_act,
        w_dif_saldos_acum,w_dif_saldos_mes LIKE cb_mcuentas.saldo_act,
        w_diferencia                       LIKE cb_mcuentas.saldo_act,
        exis,col1                          SMALLINT,
        col2,col                           SMALLINT,
        w_mensaje                          CHAR(50),
        linea                              CHAR(152)

  OUTPUT LEFT   MARGIN 0 
         PAGE   LENGTH 88
         TOP    MARGIN 0 
         BOTTOM MARGIN 8 

  FORMAT 
  PAGE HEADER
   # Definiendo una linea
   LET linea = "-------------------------------------------------------------",
               "-------------------------------------------------------------",
               "------------------------------"

   PRINT fnt.ini CLIPPED,fnt.t88 CLIPPED,fnt.cmp CLIPPED,fnt.c12 CLIPPED
   PRINT COLUMN   1,"Contabilidad",
         COLUMN  50,fnt.twd CLIPPED,"ESTADO PERDIDAS Y GANANCIAS",
                    fnt.fwd CLIPPED,
         COLUMN 109,PAGENO USING "Pagina: <<"

   LET col1 = centrado(142,w_periodo1)
   PRINT COLUMN    1,"Ctbre070",
         COLUMN col1,w_periodo1 CLIPPED,
         COLUMN  134,"Fecha : ",TODAY USING "dd/mmm/yyyy" 
   PRINT COLUMN  134,"Hora  : ",TIME 

   # Imprimiendo encabezado
   PRINT linea 
   PRINT COLUMN  1,"Cuenta               Nombre cuenta",
         COLUMN 93,"Acumulado"
   PRINT linea 

  BEFORE GROUP OF imp1.empresa
   SKIP TO TOP OF PAGE
   
   # Buscando datos de la empresa
   INITIALIZE w_mae_emp.* TO NULL
   CALL busca_empresa(imp1.empresa)
   RETURNING w_mae_emp.*,exis

   IF w_mae_emp.consolida MATCHES "[Ss]" THEN 
      PRINT fnt.twd CLIPPED,
            imp1.empresa CLIPPED," ",w_mae_emp.nombre,"C O N S O L I D A D O",
            fnt.fwd CLIPPED
   ELSE
      PRINT fnt.twd CLIPPED,
            imp1.empresa CLIPPED," ",w_mae_emp.nombre,
            fnt.fwd CLIPPED
   END IF
   SKIP 1 LINES

  ON EVERY ROW
   # Colocando en casacada los nombres de las cuentas
   LET col1 = (20+imp1.nivel)
   LET col2 = (88+(10*imp1.nivel))

   # Si el saldo actual y cargos y abonos es igual a cero no entra al ciclo
   IF (imp1.saldo_act!=0) OR 
      (imp1.cargos!=0) OR 
      (imp1.abonos!=0) THEN

      # Diferencia de total y colocarle el signo dependiendo de su tipo de saldo
      # ya sea acreedor o deudor y la posicion que ocupa cargo o abono 
      LET w_diferencia = (imp1.cargos-imp1.abonos)
      IF ((w_diferencia>0) AND (imp1.tipo_saldo = "D")) THEN
         LET w_diferencia= w_diferencia
      ELSE  
         IF ((w_diferencia<0) AND (imp1.tipo_saldo = "D")) THEN
            LET w_diferencia= w_diferencia
         ELSE  
            IF ((w_diferencia>0) AND (imp1.tipo_saldo = "A")) THEN
               LET w_diferencia= (w_diferencia*-1)
            ELSE
               IF ((w_diferencia<0) AND (imp1.tipo_saldo = "A")) THEN
                  LET w_diferencia= w_diferencia*-1
               ELSE 
                  LET w_diferencia= (w_diferencia*-1)
               END IF
            END IF
         END IF
      END IF

      PRINT COLUMN    1,imp1.cuenta,
            COLUMN col1,imp1.nombre[1,50],
            COLUMN   88,imp1.saldo_act USING "---,---,--&.&&",
            COLUMN col2,w_diferencia   USING "---,---,--&.&&"
   END IF 

  AFTER GROUP OF imp1.operacion  
   # Hallando maximo nivel de cuentas por empresa y sumando cuentas
   # con saldo acreedor
   SELECT SUM(c.carabo),SUM(c.acumulado)
    INTO  w_mes_acreedor,w_acum_acreedor
    FROM  cb_sumasrep c
    WHERE (c.operacion = imp1.operacion) AND
          (c.saldo   = "A") AND
          (c.nivel   = (SELECT MAX(d.nivel)
                         FROM  cb_sumasrep d
                         WHERE (d.operacion = imp1.operacion) AND
                               (d.saldo   = "A") AND
                               (d.nivel   IS NOT NULL)))

   # Validando totales de movimientos y acumulados de tipo de saldo acreedor
   IF (w_mes_acreedor IS NULL) THEN
      LET w_mes_acreedor = 0
   END IF
   IF (w_acum_acreedor IS NULL) THEN
      LET w_acum_acreedor = 0
   END IF

   # Hallando maximo nivel de cuentas por empresa y sumando cuentas 
   # con saldo deudor
   SELECT SUM(c.carabo),SUM(c.acumulado)
    INTO  w_mes_deudor,w_acum_deudor
    FROM  cb_sumasrep c
    WHERE (c.operacion = imp1.operacion) AND
          (c.saldo   = "D") AND
          (c.nivel   = (SELECT MAX(d.nivel)
                           FROM  cb_sumasrep d
                           WHERE (d.operacion = imp1.operacion) AND
                                 (d.saldo   = "D") AND
                                 (d.nivel   IS NOT NULL)))

   # Validando totales de movimientos y acumulados de tipo de saldo deudor
   IF (w_mes_deudor IS NULL) THEN
      LET w_mes_deudor = 0
   END IF
   IF (w_acum_deudor IS NULL) THEN
      LET w_acum_deudor = 0
   END IF

   # Diferencia entre acreedor y deudor acumulado y movimientos del mes
   LET w_dif_saldos_acum = w_acum_acreedor-w_acum_deudor # total acumulado
   LET w_dif_saldos_mes  = w_mes_acreedor-w_mes_deudor   # total del mes

   # Totalizando perdida o ganancia bruta
   IF imp1.operacion = "TB" THEN

      # Registra ganancia del mes y acumulado
      LET w_ganancia_acum   = w_dif_saldos_acum
      LET w_ganacia_mes     = w_dif_saldos_mes
      # Asigna ganancia bruta de los movimientos del periodo
      LET w_diferencia_tmp  = w_dif_saldos_mes

      LET w_mensaje         = "GANANCIA BRUTA"
      # Total del reporte por rubros
      LET w_total_acum      = w_ganancia_acum 
      LET w_total_mes       = w_diferencia_tmp
   END IF 

   # Totalizando perdida o ganancia en operacion
   IF imp1.operacion = "TO" THEN
      LET w_operacion_acum  = w_dif_saldos_acum
      LET w_operacion_mes   = w_dif_saldos_mes

      # Asigna ganancia o perdida en operacion  se cambia el signo por la
      # diferencia arriba obtenida
      LET w_diferencia_tmp  = (w_ganacia_mes+w_operacion_mes)

      LET w_mensaje         = "GANANCIA O PERDIDA EN OPERACION"
      # Total del reporte por rubros
      LET w_total_acum      = (w_ganancia_acum+w_operacion_acum) 
      LET w_total_mes       = w_diferencia_tmp
   END IF 

   # Totalizando ingresos de otros productos y los egresos de otros gastos
   IF imp1.operacion = "OT" THEN
      LET w_otros_acum      = w_dif_saldos_acum
      LET w_otros_mes       = w_dif_saldos_mes
      # Asigna ganancia antes de impuesto o perdida neta
      # se cambia el signo por la operacion arriba realizada 
      LET w_diferencia_tmp  = (w_ganacia_mes+w_operacion_mes+w_otros_mes)

      LET w_mensaje         = "GANANCIA ANTES DE IMPUESTO O PERDIDA NETA"
      # Total del reporte por rubros
      LET w_total_acum      = (w_ganancia_acum+w_operacion_acum+w_otros_acum)
      LET w_total_mes       = w_diferencia_tmp
   END IF 

   # Totalizando empresa
   SKIP 1 LINES

   PRINT COLUMN   1,fnt.tbl CLIPPED,w_mensaje,
         COLUMN  90,(w_total_acum)  USING "---,---,--&.&&",
         COLUMN 102,(w_total_mes)   USING "---,---,--&.&&",
                    fnt.fbl CLIPPED 
   LET w_total_acum = 0
   LET w_total_mes= 0
   SKIP 2 LINES

  ON LAST ROW
   # ======== Imprimiendo los criterios de seleccion =======================
   LET w_crit = 0 
   SKIP 5 LINES
   LET col = centrado(152,"CRITERIOS DE BUSQUEDA DEL REPORTE")
   PRINT COLUMN col, "CRITERIOS DE BUSQUEDA DEL REPORTE"
   PRINT COLUMN col, "---------------------------------"
   SKIP 1 LINE
   PRINT COLUMN 005, "Los criterios ingresados son:"
   SKIP 1 LINE

   WHILE TRUE
        LET w_crit = w_crit + 1 
        CASE
           WHEN w_crit = 1 AND wc_empresa  IS NOT NULL AND wc_empresa  != " " 
                PRINT COLUMN 005, "Numero Empresa  : ",
                      COLUMN 021, wc_empresa   CLIPPED
           WHEN w_crit = 2 AND wc_cuenta   IS NOT NULL AND wc_cuenta   != " " 
                PRINT COLUMN 005, "Numero Cuenta   : ",
                      COLUMN 021, wc_cuenta    CLIPPED
           WHEN w_crit = 3 AND wc_nombre   IS NOT NULL  AND wc_nombre    != " " 
                PRINT COLUMN 005, "Nombre          : ",
                      COLUMN 021, wc_nombre    CLIPPED
           WHEN w_crit = 4 AND wc_nivel    IS NOT NULL  AND wc_nivel     != " " 
                PRINT COLUMN 005, "Nivel           : ",
                      COLUMN 021, wc_nivel     CLIPPED
           WHEN w_crit = 5 AND wc_periodo IS NOT NULL  AND wc_periodo != " " 
                PRINT COLUMN 005, "Periodo         : ",
                      COLUMN 021, wc_periodo     CLIPPED
           WHEN w_crit = 6 EXIT WHILE
        END CASE
   END WHILE

   SKIP 3 LINES
   PRINT COLUMN 005, fnt.tbl CLIPPED, "Simbologia : ",fnt.fbl CLIPPED,
         COLUMN 029, "*  Todos",
         COLUMN 049, "!= Diferente a",
         COLUMN 069, "<= Menor igual a",
         COLUMN 089, ">= Mayor igual a "
   PRINT COLUMN 025, "|  Ambos",
         COLUMN 045, ":  Rango",
         COLUMN 065, "=  Igual a",
         COLUMN 085, "?  Equivalente"

END REPORT

{ Inicializo totales del reporte para rubros }

FUNCTION re070_inivar()

  LET w_ganancia_acum   = 0
  LET w_ganacia_mes     = 0
  LET w_operacion_acum  = 0
  LET w_operacion_mes   = 0
  LET w_otros_acum      = 0
  LET w_otros_mes       = 0
  LET w_total_acum      = 0
  LET w_total_mes       = 0
  LET w_diferencia_tmp  = 0

END FUNCTION
