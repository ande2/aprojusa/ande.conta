

drop   table "sistemas".cmp_notasdeb;  
create table "sistemas".cmp_notasdeb
  (
    lnknot serial not null ,
    codemp smallint not null ,
    tipmov smallint not null ,
    numdoc char(20) not null ,
    codsoc integer not null ,
    fecemi date not null ,
    descrp char(255) not null ,
    totdoc decimal(14,2) not null ,
    estado smallint not null ,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    motanl char(100),
    usranl varchar(15),
    fecanl date,
    horanl datetime hour to second,
    primary key (lnknot)  constraint "sistemas".pkcmpnotasdeb
  )  extent size 16 next size 16 lock mode row;

create trigger "sistemas".trgdeletenotdebitocxp delete on "sistemas"
    .cmp_notasdeb referencing old as pre
    for each row
        (
        delete from "sistemas".bco_dtransac  where (lnknot =
    pre.lnknot ) );
