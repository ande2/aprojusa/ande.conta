{
Programo : Mynor Ramirez        
Objetivo : Impresion de notas de debito de cuentas por pagar.
}

GLOBALS "cmpglb003.4gl" 
DEFINE w_mae_usr     RECORD LIKE glb_usuarios.*,
       fnt           RECORD
        cmp          CHAR(12),
        nrm          CHAR(12),
        tbl          CHAR(12),
        fbl,t88      CHAR(12),
        t66,p12      CHAR(12),
        p10,srp      CHAR(12),
        twd          CHAR(12),
        fwd          CHAR(12),
        tda,fda      CHAR(12),
        ini          CHAR(12)
       END RECORD, 
       existe        SMALLINT, 
       filename      STRING,   
       pipeline      STRING,   
       i             INT

-- Subrutina para generar la nota de debito                 

FUNCTION cmprpt003_GenerarNotaDebito(reimpresion)
 DEFINE reimpresion SMALLINT 

 -- Definiendo archivo de impresion
 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/NotaDebitoCXP.spl"

 -- Asignando valores
 LET pipeline = "local"   

 -- Seleccionando fonts para impresora epson
 CALL librut001_fontsprn(pipeline,"epson") 
 RETURNING fnt.* 

 -- Iniciando reporte
 START REPORT cmprpt003_ImprimirNotaDebito TO filename 
  
  -- Llenando reporte
  OUTPUT TO REPORT cmprpt003_ImprimirNotaDebito(w_mae_tra.*,reimpresion)

 -- Finalizando reporte 
 FINISH REPORT cmprpt003_ImprimirNotaDebito 

 -- Imprimiendo el reporte
 CALL librut001_sendreport(filename,pipeline,"","")
END FUNCTION 

-- Subrutinar para imprimir la nota de debito                   

REPORT cmprpt003_ImprimirNotaDebito(imp1,reimpresion) 
 DEFINE imp1        RECORD LIKE cmp_notasdeb.*,
        exis,i,col  SMALLINT, 
        reimpresion SMALLINT,
        xnomemp     CHAR(50), 
        xnombco     CHAR(50), 
        xnomsoc     CHAR(50), 
        lht         CHAR(86),
        esd,esi,eid CHAR(1), 
        eii,lvt     CHAR(1), 
        wcanletras  STRING,
        numero,tl   INT 

  OUTPUT LEFT   MARGIN  2 
         PAGE   LENGTH 44 
         TOP    MARGIN  3 
         BOTTOM MARGIN  2 

 FORMAT 
  BEFORE GROUP OF imp1.lnknot 
   -- Definiendo caracteres
   LET esd = "+" --ASCII(191)
   LET esi = "+" --ASCII(218)
   LET eid = "+" --ASCII(217)
   LET eii = "+" --ASCII(192) 
   LET lvt = "|" --ASCII(179)

   INITIALIZE lht TO NULL 
   FOR i = 1 TO 86 
    LET lht = lht CLIPPED,"-" 
   END FOR 

   -- Imprimiendo encabezado
   LET wcanletras = librut001_numtolet(imp1.totdoc) 

   -- Inicializando impresora
   PRINT fnt.ini CLIPPED,fnt.t88 CLIPPED,fnt.nrm CLIPPED,fnt.p12 CLIPPED

   -- Obteniendo datos de la empresa
   INITIALIZE xnomemp TO NULL 
   SELECT a.nomemp INTO xnomemp FROM glb_empresas a
    WHERE a.codemp = w_mae_tra.codemp 

   -- Obteniendo datos del socio 
   INITIALIZE xnomsoc TO NULL 
   SELECT a.nomsoc INTO xnomsoc FROM glb_sociosng a
    WHERE a.codsoc = w_mae_tra.codsoc 

   -- Imprimiendo datos
   PRINT fnt.tda CLIPPED,
         "NOTA DE DEBITO A SOCIOS - NUMERO DE MOVIMIENTO (",
         w_mae_tra.lnknot USING "<<<<<<<<<<",")",
         fnt.fda CLIPPED 
   PRINT esi,lht,esd 
   PRINT COLUMN   1,lvt,
         COLUMN   3,"Empresa                 : ",xnomemp CLIPPED,
         COLUMN  88,lvt
   PRINT COLUMN   1,lvt,
         COLUMN   3,"Socio de Negocios       : ",xnomsoc CLIPPED,
         COLUMN  88,lvt
   PRINT COLUMN   1,lvt,
         COLUMN   3,"Numero Nota Debito      : ",w_mae_tra.numdoc CLIPPED,
         COLUMN  50,"Fecha de Emision  : ",w_mae_tra.fecemi,
         COLUMN  88,lvt
   PRINT COLUMN   1,lvt,
         COLUMN   3,"Descripcion Nota Debito : ",w_mae_tra.descrp[1,50],
         COLUMN  88,lvt
   PRINT COLUMN   1,lvt,
         COLUMN   3,"                          ",w_mae_tra.descrp[51,100],
         COLUMN  88,lvt
   PRINT COLUMN   1,lvt,
         COLUMN   3,"Valor Nota Debito       : ",w_mae_tra.totdoc USING "###,###,##&.&&",
         COLUMN  50,"Usuario Registro  : ",w_mae_tra.userid,
         COLUMN  88,lvt
   PRINT COLUMN   1,lvt,
         COLUMN   3,"Fecha de Registro : ",w_mae_tra.fecsis,
         COLUMN  50,"Hora de Registro  : ",w_mae_tra.horsis, 
         COLUMN  88,lvt
   PRINT esi,lht,esd 
   PRINT COLUMN   1,lvt,
         COLUMN   3,"Numero Factura         Fecha Emision      Total Factura     Total Nota", 
         COLUMN  88,lvt
   PRINT COLUMN   1,lvt,
         COLUMN  88,lvt

   -- Imprmiendo detalle de facturas afectadas
   LET tl = 10
   FOR i = 1 TO totpag
    IF v_pagos[i].pfecven IS NULL OR
       v_pagos[i].ptotpag IS NULL OR 
       v_pagos[i].ptotpag = 0 THEN
       CONTINUE FOR
    END IF

    PRINT COLUMN   1,lvt,
          COLUMN   3,v_pagos[i].pnumdoc                       ,3  SPACES,
                     v_pagos[i].pfecemi                       ,9  SPACES, 
                     v_pagos[i].ptotdoc  USING "##,###,##&.&&",2  SPACES,
                     v_pagos[i].ptotpag  USING "##,###,##&.&&",
          COLUMN  88,lvt 

    LET tl = tl-1
   END FOR 
   
   FOR i = 1 TO tl
    PRINT COLUMN   1,lvt,
          COLUMN  88,lvt 
   END FOR 
   PRINT COLUMN   1,lvt,
         COLUMN  88,lvt 
   PRINT esi,lht,esd 
   PRINT COLUMN   1,lvt,
         COLUMN  88,lvt 
   PRINT COLUMN   1,lvt,
         COLUMN  88,lvt 
   PRINT COLUMN   1,lvt,
         COLUMN  88,lvt 
   PRINT COLUMN   1,lvt,
         COLUMN  10,"__________________________",
         COLUMN  50,"__________________________",
         COLUMN  88,lvt 
   PRINT COLUMN   1,lvt,
         COLUMN  10,"   Nombre Persona Recibe",
         COLUMN  50,"    Numero de Identidad",
         COLUMN  88,lvt 
   PRINT esi,lht,esd 

   -- Si es reimpresion
   IF reimpresion THEN
      PRINT fnt.tbl CLIPPED,
            "Re-impresion ",TODAY," ",TIME," ",username CLIPPED,
            fnt.fbl CLIPPED 
   END IF 
END REPORT 
