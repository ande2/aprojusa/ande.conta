{ 
Programo : Mynor Ramirez
Objetivo : Programa de criterios de seleccion para:
           Consultar/anular notas de debito cuentas por pagar 
} 

-- Definicion de variables globales 
GLOBALS "cmpglb003.4gl"

-- Subrutina para consultar/anular notas e debito 

FUNCTION cmpqbx003_NotasDebito(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart,qry STRING, 
        loop,existe         SMALLINT,
        operacion           SMALLINT,
        qryres,msg          CHAR(80),
        wherestado          STRING,
        opc,totreg,res      INTEGER 

 -- Verificando operacion
 CASE (operacion)
  WHEN 1 LET wherestado = NULL
  WHEN 2 LET wherestado = " AND a.estado = 1" 
 END CASE 

 -- Definiendo nivel de aislamiento 
 SET ISOLATION TO DIRTY READ 

 -- Cargando combobox de socios 
 CALL librut003_CbxSociosCompras() 

 -- Buscando datos 
 LET loop = TRUE 
 WHILE loop 
  -- Inicializando las variables 
  CALL cmping003_EstadoMenu(operacion," ")
  INITIALIZE qrytext,qrypart TO NULL
  LET int_flag = 0 
  CLEAR FORM
                                                     
  -- Construyendo busqueda 
  CONSTRUCT BY NAME qrytext 
                 ON a.fecemi,
                    a.codsoc,
                    a.totdoc,
                    a.numdoc, 
                    a.descrp,
                    a.lnknot, 
                    a.estado,
                    a.userid,
                    a.fecsis, 
                    a.horsis,
                    a.usranl,
                    a.fecanl,
                    a.horanl,
                    a.motanl

   ON ACTION cancel
    -- Salida
    LET loop = FALSE
    EXIT CONSTRUCT
  END CONSTRUCT
  IF NOT loop THEN
     EXIT WHILE
  END IF 

  -- Preparando la busqueda 
  ERROR " Seleccionando datos ... por favor espere ..." ATTRIBUTE(CYAN) 

  -- Creando la busqueda 
  LET qrypart = "SELECT a.lnknot ",
                 "FROM  cmp_notasdeb a ",
                 "WHERE a.codemp = ",gcodemp,
                 " AND ",qrytext CLIPPED,
                 wherestado CLIPPED,
                 " ORDER BY a.lnknot DESC" 

  -- Contando registros
  LET totreg = librut003_NumeroRegistros(qrypart)

  -- Declarando el cursor 
  PREPARE estqbx001 FROM qrypart
  DECLARE c_notdeb SCROLL CURSOR WITH HOLD FOR estqbx001  
  opeN c_notdeb 
  FETCH FIRST c_notdeb INTO w_mae_tra.lnknot
  IF (status = NOTFOUND) THEN
     INITIALIZE w_mae_tra.* TO NULL
     ERROR ""
     CALL fgl_winmessage(
     "Atencion",
     "No existen notas de debito con el criterio seleccionado.",
     "stop") 
  ELSE
     ERROR "" 

     -- Desplegando datos 
     LET msg = " - Registros [ ",totreg||" ]"
     CALL cmping003_EstadoMenu(operacion,msg)
     CALL cmpqbx003_datos()
    
     -- Fetchando notas 
     MENU "Opciones" 
      BEFORE MENU 
       -- Verificando tipo de operacion 
       CASE (operacion)
        WHEN 1 HIDE OPTION "Anular" 
       END CASE 	

      ON ACTION consultar  
       EXIT MENU

      ON ACTION anterior   
       FETCH NEXT c_notdeb INTO w_mae_tra.lnknot 
        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas notas de debito anteriores en lista.", 
           "information")
           FETCH LAST c_notdeb INTO w_mae_tra.lnknot
        END IF 

       -- Desplegando datos 
       CALL cmpqbx003_datos()

      ON ACTION siguiente  
       FETCH PREVIOUS c_notdeb INTO w_mae_tra.lnknot
        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas notas de debito siguientes en lista.", 
           "information")
           FETCH FIRST c_notdeb INTO w_mae_tra.lnknot
        END IF

       -- Desplegando datos 
       CALL cmpqbx003_datos()

      ON ACTION primero  
       FETCH FIRST c_notdeb INTO w_mae_tra.lnknot
        -- Desplegando datos 
        CALL cmpqbx003_datos()

      ON ACTION ultimo  
       FETCH LAST c_notdeb INTO w_mae_tra.lnknot
        -- Desplegando datos
        CALL cmpqbx003_datos()

      ON ACTION anular 
       -- Verificando si nota de debito ya fue anulada
       IF w_mae_tra.estado=0 THEN
          CALL fgl_winmessage(
          "Atencion:", 
          "Nota de debito ya fue anulada, VERIFICA.",
          "stop") 
          CONTINUE MENU 
       END IF 

       -- Anulando nota de debito 
       CALL cmpqbx003_AnularNotaDebito()      

       -- Desplegando datos
       CALL cmpqbx003_datos()

      ON ACTION imprimir 
       -- Reimpresion de nota de debito
       CALL cmprpt003_GenerarNotaDebito(1)

      ON ACTION reporte
       -- Reporte de datos seleccionados a excel
       CALL fgl_winmessage(
       "Atencion:","Opcion no aun no disponible.","information") 

      ON ACTION cancel
       LET loop = FALSE
       EXIT MENU

      COMMAND KEY(F4,CONTROL-E)
       EXIT MENU
     END MENU
   END IF     
  CLOSE c_notdeb
 END WHILE

 -- Desplegando estado del menu 
 CALL cmping003_estadoMenu(0,"")

 -- Inicializando datos 
 CALL cmping003_inival(1)
END FUNCTION 

-- Subrutina para desplegar los datos de la nota 

FUNCTION cmpqbx003_datos()
 DEFINE existe SMALLINT

 -- Desplegando datos 
 CLEAR FORM

 -- Obteniendo datos de la nota 
 CALL librut003_BNotaDebitoCxP(w_mae_tra.lnknot) 
 RETURNING w_mae_tra.*,existe

 -- Desplegando datos del documento 
 CLEAR FORM 
 DISPLAY BY NAME w_mae_tra.codemp, w_mae_tra.fecemi, 
                 w_mae_tra.numdoc, w_mae_tra.totdoc, 
                 w_mae_tra.descrp, w_mae_tra.userid, 
                 w_mae_tra.fecsis, w_mae_tra.horsis,
                 w_mae_tra.motanl, w_mae_tra.usranl,
                 w_mae_tra.fecanl, w_mae_tra.horanl,
                 w_mae_tra.codsoc, w_mae_tra.lnknot,
                 w_mae_tra.estado 

 -- Desplegando las facturas afectadas con la nota de debito 
 -- Inicializando datos 
 CALL cmping003_inivec() 
 CALL cmpqbx003_FacturasNotaDebito() 
END FUNCTION 

-- Subrutina para desplegar las facturas canceladas con la nota de debito 

FUNCTION cmpqbx003_FacturasNotaDebito() 
 DEFINE qrytxt STRING 
                                                             
 -- Selecionando datos
 LET qrytxt = "SELECT a.fecven,",
                     "b.diaven,", 
                     "a.fecemi,",
                     "a.nomcen,", 
                     "a.nomrub,", 
                     "a.numdoc,",
                     "a.totdoc,",
                     "b.totsal,",
                     "b.totpag,", 
                     "a.lnkcmp,",
                     "'' ",
               "FROM  vis_movtoscompra a,bco_dtransac b ",
               "WHERE b.lnknot = ",w_mae_tra.lnknot, 
                " AND a.lnkcmp = b.lnkcmp ", 
               "ORDER BY a.fecven,a.numdoc" 

 PREPARE c_prepare1 FROM qrytxt
 DECLARE cfacturas CURSOR FOR c_prepare1
 LET totpag = 1 
 FOREACH cfacturas INTO v_pagos[totpag].*
  LET totpag=totpag+1 
 END FOREACH
 CLOSE cfacturas
 FREE cfacturas 
 LET totpag =totpag-1 

 -- Desplegando facturas
 DISPLAY ARRAY v_pagos TO s_pagos.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION 

-- Subrutina para anular la nota de debito 

FUNCTION cmpqbx003_AnularNotaDebito() 
 DEFINE anula SMALLINT 

 -- Ingresando motivo de la anulacion
 LET anula = TRUE 
 INPUT BY NAME w_mae_tra.motanl WITHOUT DEFAULTS 
  ON ACTION accept 
   EXIT INPUT 
  ON ACTION cancel
   LET anula = FALSE 
   EXIT INPUT 
 END INPUT 
 IF NOT anula THEN
    RETURN
 END IF 

 -- Confirmando anulacion 
 IF NOT librut001_yesornot("Confirmacion",
                           "Desea Anular la Nota de Debito ?",
                           "Si",
                           "No",
                           "question") THEN
    RETURN 
 END IF

 -- Iniciando Transaccion
 BEGIN WORK

   -- Anulando nota de debito 
   SET LOCK MODE TO WAIT
   UPDATE cmp_notasdeb
   SET    cmp_notasdeb.estado = 0,
          cmp_notasdeb.motanl = w_mae_tra.motanl, 
          cmp_notasdeb.fecanl = CURRENT,
          cmp_notasdeb.horanl = CURRENT HOUR TO SECOND, 
          cmp_notasdeb.usranl = USER 
   WHERE  cmp_notasdeb.lnknot = w_mae_tra.lnknot 

   -- Anulando efecto de la nota en facturas afectadas 
   SET LOCK MODE TO WAIT
   UPDATE bco_dtransac
   SET    bco_dtransac.estado = 0
   WHERE  bco_dtransac.lnknot = w_mae_tra.lnknot 
   
 -- Finalizando Transaccion
 COMMIT WORK

 CALL fgl_winmessage(
 " Atencion",
 " Nota de debito # "||w_mae_tra.numdoc CLIPPED||" anulada.",
 "information")
END FUNCTION 
