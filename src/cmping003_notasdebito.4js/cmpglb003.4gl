{ 
Programo : Mynor Ramirez  
Objetivo : Programa de variables globales
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "cmping003"
DEFINE w_mae_tra   RECORD LIKE cmp_notasdeb.*,
       w_mae_pro   RECORD LIKE glb_sociosng.*,
       username    LIKE glb_permxusr.userid,
       v_pagos     DYNAMIC ARRAY OF RECORD  
        pfecven    LIKE cmp_mtransac.fecemi,
        pdiaven    SMALLINT,  
        pfecemi    LIKE cmp_mtransac.fecemi,
        pnomcen    CHAR(25), 
        pnomrub    CHAR(25), 
        pnumdoc    CHAR(20),
        ptotdoc    LIKE cmp_mtransac.totdoc,
        ptotsal    LIKE cmp_mtransac.totsal,
        ptotpag    LIKE cmp_mtransac.totdoc, 
        plnkcmp    LIKE cmp_mtransac.lnkcmp, 
        prellen    CHAR(1) 
       END RECORD,
       gcodemp     LIKE cmp_mtransac.codemp, 
       b           ui.ComboBox,
       cba         ui.ComboBox,
       w           ui.Window,
       f           ui.Form,
       totlin      INTEGER,  
       totpag      INTEGER 
END GLOBALS
