{
Programo : Mynor Ramirez
Objetivo : Mantenimiento de notas de debito cuentas por pagar. 
}

-- Definicion de variables globales

GLOBALS "cmpglb003.4gl"
CONSTANT MaximoValorPago = 3000000
DEFINE wpais       VARCHAR(255), 
       maxdiaing   SMALLINT,
       maxdiapos   SMALLINT,
       regreso     SMALLINT,
       existe      SMALLINT,
       msg         STRING 

-- Subrutina principal

MAIN
 -- Atrapando interrupts 
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/ToolBarNotasDebitoCompras") 

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("notasdebitocxp")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo usuario del sistema
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de opciones
 CALL cmping003_menu()
END MAIN

-- Subutina para el menu de notas de debito de cuentas por pagar 

FUNCTION cmping003_menu()
 -- Abriendo la ventana del mantenimiento
 OPEN WINDOW wing004a AT 5,2  
  WITH FORM "cmping003a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ 

  -- Desplegando datos del encabezado
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("cmping003",wpais,1)

  -- Obteniendo parametro de maximo numero de dias para ingresar una nota 
  CALL librut003_parametros(17,1)
  RETURNING existe,maxdiaing 
  IF NOT existe THEN
     CALL fgl_winmessage(
     "Atencion:",
     "Parametro maximo de dias hacia atras para ingresar notas de debito no definido.\n"||
     "Debe definirse antes de poder ingresar notas de debito.",
     "stop")
     RETURN
  END IF

  -- Cargando combobox de empresas
  LET gcodemp = librut003_DCbxEmpresaEnUsoXUsuario()
  IF gcodemp IS NULL THEN
     CALL fgl_winmessage(
     "Atencion","Usuario sin empresa asignada, VERIFICA.","stop")
     RETURN
  END IF

  -- Inicializando datos 
  CALL cmping003_inival(1)

  -- Cargando combobox de empresas
  CALL librut003_CbxEmpresas()
  -- Cargando tipo de nota de debito 
  CALL librut003_CbxTiposMovimientoPagoBco()

  -- Menu de opciones
  MENU "Opciones" 
   BEFORE MENU
    -- Verificando accesos a opciones
    -- Consultar
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "Consultar"
    END IF
    -- Emitir  
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Emitir"
    END IF
    -- Anular
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Anular"
    END IF
   COMMAND "Consultar" 
    "Consulta de notas de debito existentes."
    CALL cmpqbx003_NotasDebito(1)
   COMMAND "Emitir" 
    "Emision de nuevas notas de debito."
    CALL cmping003_NotasDebito(1) 
   COMMAND "Anular"
    "Anulacion de notas de debito existentes."
    CALL cmpqbx003_NotasDebito(2) 
   COMMAND "Salir"
    "Abandona el menu de notas de debito." 
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU  
  END MENU
 CLOSE WINDOW wing004a
END FUNCTION

-- Subrutina para el ingreso de los datos de la nota de debito 

FUNCTION cmping003_NotasDebito(operacion)
 DEFINE retroceso         SMALLINT,
        operacion,opc     SMALLINT,
        loop,existe,i     SMALLINT,
        conteo            INTEGER 

 -- Desplegando estado del menu
 CALL cmping003_EstadoMenu(4,"")

 -- Desactivando salida del input
 OPTIONS INPUT WRAP 

 -- Inicio del loop
 LET loop      = TRUE
 LET retroceso = FALSE
 WHILE loop   
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Verificando si operacion es ingreso
     IF (operacion=1) THEN 
        -- Inicializando datos 
        CALL cmping003_inival(1) 
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_tra.codsoc, 
                w_mae_tra.descrp 
                WITHOUT DEFAULTS
    ATTRIBUTE(UNBUFFERED,CANCEL=FALSE)

   ON ACTION cancel 
    -- Salida 
    IF (operacion=1) THEN  -- Ingreso 
     IF INFIELD(codsoc) THEN
        LET loop = FALSE
        EXIT INPUT
     ELSE
        -- Inicializando 
        CALL cmping003_inival(1)
        LET retroceso = FALSE 
        NEXT FIELD codsoc 
     END IF 
    ELSE                   -- Modifidacion 
     LET loop = FALSE 
     EXIT INPUT 
    END IF 

   ON KEY (CONTROL-F,F3)
    -- Cambiando fecha de emision
    IF INFIELD(codsoc) THEN
       CALL cmping003_FechaEmision()
    ELSE
       CALL fgl_winmessage(
       "Atencion:",
       "Para cambiar fecha posicionarse en dato Socio de Negocios.",
       "information")  
    END IF 

   ON ACTION calculator 
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   BEFORE FIELD codsoc 
    -- Verificando operacion
    IF retroceso THEN
       CALL Dialog.SetFieldActive("codsoc",0) 
       NEXT FIELD descrp 
    ELSE 
       CALL Dialog.SetFieldActive("codsoc",1) 
    END IF 

    -- Desabilitando accion de accept
    CALL Dialog.SetActionActive("accept",0) 

    -- Cargando combobox de socios con pagos pendientes por compras 
    CALL librut003_CbxSociosCompras() 

   ON CHANGE codsoc
    -- Obteniendo datos del socio 
    INITIALIZE w_mae_pro.* TO NULL 
    CALL librut003_BSociosNeg(w_mae_tra.codsoc)
    RETURNING w_mae_pro.*,existe 

   AFTER FIELD codsoc 
    -- Verificando codsoc
    IF w_mae_tra.codsoc IS NULL THEN 
       ERROR "Error: socio de negocios invalido, VERIFICA."
       NEXT FIELD codsoc 
    END IF  

    -- Obteniendo datos del socio 
    INITIALIZE w_mae_pro.* TO NULL 
    CALL librut003_BSociosNeg(w_mae_tra.codsoc)
    RETURNING w_mae_pro.*,existe 

    -- Cargando pagos pendientes del socio 
    IF cmping003_PagosPendientesSocios() THEN
       NEXT FIELD codsoc 
    END IF 

    -- Verificando pagos 
    IF (totpag=0) THEN 
       CALL fgl_winmessage(
       "Atencion:","Socio de negocios sin pagos pendientes.","stop")
       NEXT FIELD codsoc  
    END IF 
    LET retroceso = TRUE

   BEFORE FIELD descrp 
    -- Habilitando accion de accept
    CALL Dialog.SetActionActive("accept",1)

    -- Encontrando maximo numero de nota de debito 
    SELECT NVL(COUNT(*),0)+1 
     INTO  conteo 
     FROM  cmp_notasdeb a
     WHERE a.tipmov = w_mae_tra.tipmov 

    -- Asignando y desplegando numero de nota de debito
    LET w_mae_tra.numdoc = conteo 
    DISPLAY BY NAME w_mae_tra.numdoc 

   AFTER FIELD descrp
    -- Verificando descrp 
    IF LENGTH(w_mae_tra.descrp)=0 THEN
       ERROR "Error: descripcion de la nota invalida, VERIFICA."
       NEXT FIELD descrp 
    END IF

  END INPUT 
  IF NOT loop THEN
     EXIT WHILE 
  END IF 

  -- Menu de opciones 
  LET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")
  -- Verificando opcion 
  CASE (opc)
   WHEN 0 -- Cancelando
    LET retroceso = FALSE
   WHEN 1 -- Grabando
    LET retroceso = FALSE 

    -- Grabando 
    CALL cmping003_grabar(operacion)
   WHEN 2 -- Modificando
    LET retroceso = TRUE 
  END CASE
 END WHILE

 -- Activando salida del input
 OPTIONS INPUT NO WRAP 

 -- Inicializando datos 
 IF (operacion=1) THEN 
    CALL cmping003_inival(1) 
 END IF 

 -- Desplegando estado del menu
 CALL cmping003_EstadoMenu(0,"")
END FUNCTION

-- Subrutina para grabar la nota de debito 

FUNCTION cmping003_grabar(operacion)
 DEFINE i,correl,operacion SMALLINT,
        msg                STRING

 -- Grabando transaccion
 CASE (operacion)
  WHEN 1 ERROR " Registrando nota de debito ..." ATTRIBUTE(CYAN)
 END CASE 

 -- Iniciando la transaccion
 BEGIN WORK
   -- 1. Encabezado
   CASE (operacion)
    WHEN 1 -- Grabando 
     -- Grabando maestro de notas de debito
     SET LOCK MODE TO WAIT 
     INSERT INTO cmp_notasdeb  
     VALUES (w_mae_tra.*)
     LET w_mae_tra.lnknot = SQLCA.SQLERRD[2]  

     -- Grabando detalle de facturas pagadas 
     FOR i = 1 TO totpag 
      IF v_pagos[i].pfecven IS NULL OR
         v_pagos[i].ptotpag IS NULL OR 
         v_pagos[i].ptotpag = 0 THEN      
         CONTINUE FOR
      END IF 

      SET LOCK MODE TO WAIT
      INSERT INTO bco_dtransac 
      VALUES (0,
              w_mae_tra.lnknot,
              v_pagos[i].plnkcmp,
              w_mae_tra.fecemi, 
              v_pagos[i].pdiaven,
              v_pagos[i].ptotsal,
              v_pagos[i].ptotpag,
              w_mae_tra.estado)
     END FOR 
   END CASE 

 -- Finalizando la transaccion
 COMMIT WORK

 -- Confirmando grabacion 
 CASE (operacion)
  WHEN 1 LET msg = " Nota Debito ("||w_mae_tra.lnknot||") registrada."
 END CASE
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Imprimiendo Pago 
 ERROR " Imprimiendo Nota Debito ... por favor esperar ..." ATTRIBUTE(CYAN)
 CALL cmprpt003_GenerarNotaDebito(0)               
 ERROR ""
END FUNCTION  

-- Subrutina para buscar los pagos pendientes del socio 

FUNCTION cmping003_PagosPendientesSocios() 
 DEFINE arr,scr,regreso,loop SMALLINT,
        qrytxt               STRING 
                                                             
 -- Inicializando datos
 CALL cmping003_inivec()
 LET totpag = 1 

 -- Selecionando datos
 LET qrytxt = "SELECT a.fecven,",
                      "0,", 
                      "a.fecemi,",
                      "a.nomcen,", 
                      "a.nomrub,", 
                      "a.numdoc,",
                      "a.totdoc,",
                      "a.totsal,",
                      "0,", 
                      "a.lnkcmp,",
                      "'' ",
                "FROM  vis_movtoscompra a ",
                "WHERE a.fecven <= TODAY+30 ", 
                  "AND a.codsoc = ",w_mae_tra.codsoc,  
                  "AND a.totsal > 0 ",
                  "AND a.estado = 1 ",
                  "AND a.frmpag = 2 ",
                "ORDER BY a.fecven,a.numdoc" 

 PREPARE c_prepare FROM qrytxt
 DECLARE cpagos CURSOR FOR c_prepare 
 FOREACH cpagos INTO v_pagos[totpag].*
  -- Calculando dias vencidos despues de la fecha de vencimiento del pago 
  LET v_pagos[totpag].pdiaven = (TODAY-v_pagos[totpag].pfecven) 
  IF (v_pagos[totpag].pdiaven<0) THEN
     LET v_pagos[totpag].pdiaven = 0 
  END IF 
  LET totpag=totpag+1 
 END FOREACH
 CLOSE cpagos 
 FREE cpagos  
 LET totpag =totpag-1 
 IF (totpag=0) THEN
    RETURN totpag
 END IF 

 -- Totalizando pagos
 CALL cmping003_TotalPagos()

 -- Seleccionando pagos 
 CALL librut001_dpelement("labeltp","Detalle de Facturas [ "||totpag||" ]")

 LET loop = TRUE 
 WHILE loop
  LET regreso = FALSE 

  INPUT ARRAY v_pagos WITHOUT DEFAULTS FROM s_pagos.*
   ATTRIBUTE(MAXCOUNT=totpag,INSERT ROW=FALSE,APPEND ROW=FALSE,ACCEPT=FALSE,CANCEL=FALSE,
             DELETE ROW=FALSE,UNBUFFERED=TRUE,FIELD ORDER FORM)

   ON ACTION cancel 
    -- Regreso
    LET loop    = FALSE 
    LET regreso = TRUE 
    EXIT INPUT

   ON ACTION accept
    -- Salida
    EXIT INPUT 

   BEFORE FIELD ptotpag 
    -- Totalizando pagos
    CALL cmping003_TotalPagos()

   AFTER FIELD ptotpag 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE() 

    -- Verificando total a pagar
    IF v_pagos[arr].ptotpag IS NULL OR
       v_pagos[arr].ptotpag>v_pagos[arr].ptotsal THEN
       LET v_pagos[arr].ptotpag = v_pagos[arr].ptotsal 
    END IF 

    -- Totalizando pagos
    CALL cmping003_TotalPagos()
  END INPUT   
  IF NOT loop THEN
     EXIT WHILE
  END IF 

  -- Totalizando pagos
  CALL cmping003_TotalPagos()

  -- Verificando total de pagos 
  IF (w_mae_tra.totdoc<=0) THEN
   CALL fgl_winmessage(
   "Atencion:",
   "Valor total de la notade debito no puede ser cero.\n"|| 
   "Deben seleccionarse facturas.",
   "stop")
   LET loop = TRUE 
   CONTINUE WHILE 
  END IF
  LET loop = FALSE 
 END WHILE 

 RETURN regreso 
END FUNCTION 

-- Subrutina para totalizar el detalle de pagos a aplicar a la nota de debito 

FUNCTION cmping003_TotalPagos()
 DEFINE i SMALLINT

 -- Totalizando pagos 
 LET w_mae_tra.totdoc = 0
 FOR i = 1 TO totpag 
  IF v_pagos[i].pfecven IS NULL THEN
     CONTINUE FOR
  END IF
  
  LET w_mae_tra.totdoc = w_mae_tra.totdoc+v_pagos[i].ptotpag 
 END FOR 
 DISPLAY BY NAME w_mae_tra.totdoc 
END FUNCTION 

-- Subrutina para ingresar la fecha de emision

FUNCTION cmping003_FechaEmision()
 -- Habilitando salida del input 
 OPTIONS INPUT NO WRAP 

 -- Ingresando fecha
 INPUT BY NAME w_mae_tra.fecemi WITHOUT DEFAULTS
  ATTRIBUTE(UNBUFFERED,CANCEL=FALSE)

  ON ACTION cancel
   EXIT INPUT  

  AFTER FIELD fecemi
   -- Verificando fecha
   IF w_mae_tra.fecemi >TODAY OR 
      w_mae_tra.fecemi <(TODAY-maxdiaing) THEN
      CALL fgl_winmessage(
      "Atencion:",
      "Fecha de emision de la nota de debito INVALIDA.",
      "stop") 
      NEXT FIELD fecemi 
   END IF 
 END INPUT   

 -- Deshabilitando salida del input 
 OPTIONS INPUT WRAP 
END FUNCTION 

-- Subrutina para inicializar las variables de trabajo 

FUNCTION cmping003_inival(i)
 DEFINE i       SMALLINT,
        wfecha  VARCHAR(80) 
 
 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1  
   INITIALIZE w_mae_tra.* TO NULL 
   CALL cmping003_inivec()
   CLEAR FORM 
 END CASE 

 -- Inicializando datos
 LET w_mae_tra.lnknot = 0                    
 LET w_mae_tra.codemp = gcodemp 
 LET w_mae_tra.tipmov = 1  
 LET w_mae_tra.fecemi = CURRENT  
 LET w_mae_tra.totdoc = 0
 LET w_mae_tra.estado = 1
 LET w_mae_tra.userid = username 
 LET w_mae_tra.fecsis = CURRENT 
 LET w_mae_tra.horsis = CURRENT HOUR TO SECOND

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_tra.fecemi,
                 w_mae_tra.userid,
                 w_mae_tra.fecsis,
                 w_mae_tra.horsis,
                 w_mae_tra.estado,
                 w_mae_tra.codemp 

 -- Desplegando fecha 
 LET wfecha = librut001_formatofecha(TODAY,1,wpais) CLIPPED
 CALL librut001_dpelement("labelz",wfecha)
END FUNCTION

-- Subrutina para inicializar el vector de trabajo

FUNCTION cmping003_inivec()
 -- Inicializando pagos facturas 
 CALL v_pagos.clear() 
 LET totpag = 0 
 DISPLAY ARRAY v_pagos TO s_pagos.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION  

-- Subrutina para desplegar los estados del menu 

FUNCTION cmping003_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("group1","Datos del Pago - MENU")
  WHEN 1 CALL librut001_dpelement("group1","Datos del Pago - CONSULTAR"||msg CLIPPED)
  WHEN 2 CALL librut001_dpelement("group1","Datos del Pago - ANULAR"||msg CLIPPED)
  WHEN 4 CALL librut001_dpelement("group1","Datos del Pago - EMITIR")
 END CASE
END FUNCTION
