  drop view vis_gastosxtipodil;

create view vis_gastosxtipodil
(lnkcaj,codemp,nomemp,tipdlg,nomdlg,fecgto,tipdoc,nserie,ndocto,
 totgra,totexe,totedp,totigt,totded,totisv,valgto,nomcom,descrp
) as
 select x0.lnkcaj,x0.codemp,x3.nomemp,x1.tipdlg,x2.nomdlg,
        x1.fecgto,x1.tipdoc,x1.nserie,x1.ndocto,x1.totgra,
        x1.totexe,x1.totedp,x1.totigt,x1.totded,x1.totisv,
        x1.valgto,
        case (x1.tipcom)
         WHEN 1 THEN "COMPRA" WHEN 2 THEN "SERVICIO" WHEN 3 then "COMBUSTIBLE"
        end,
        x1.descrp
 from   fac_cajchica x0,fac_dcachica x1,glb_tiposdlg x2,glb_empresas x3
 where  x0.lnkcaj = x1.lnkcaj
   AND  x0.estado IN (0,1) 
   AND  x3.codemp = x0.codemp
   AND  x2.tipdlg = x1.tipdlg;

grant select on vis_gastosxtipodil to public;
