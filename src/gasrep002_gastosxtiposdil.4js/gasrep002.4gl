{ 
Programa : gasrep002.4gl                   
Objetivo : Reporte de gastos por tipos de diligencia.
}

DATABASE erpjuridico 

-- Definicion de variables globales 

CONSTANT progname = "gasrep002" 
TYPE    datosreporte        RECORD 
         lnkcaj             LIKE fac_cajchica.lnkcaj,
         codemp             LIKE fac_cajchica.codemp, 
         nomemp             CHAR(50), 
         tipdlg             LIKE fac_dcachica.tipdlg,
         nomdlg             CHAR(40), 
         fecgto             LIKE fac_dcachica.fecgto,
         tipdoc             LIKE fac_dcachica.tipdoc,
         nserie             LIKE fac_dcachica.nserie,
         ndocto             LIKE fac_dcachica.ndocto,
         totgra             LIKE fac_dcachica.totgra,
         totexe             LIKE fac_dcachica.totexe,
         totedp             LIKE fac_dcachica.totedp,
         totigt             LIKE fac_dcachica.totigt,
         totded             LIKE fac_dcachica.totded,
         totisv             LIKE fac_dcachica.totisv,
         valgto             LIKE fac_dcachica.valgto,
         nomcom             CHAR(12), 
         descrp             LIKE fac_dcachica.descrp
        END RECORD 
DEFINE  datos               RECORD 
         codemp             LIKE fac_cajchica.codemp, 
         tipdlg             LIKE fac_dcachica.tipdlg,
         fecini             DATE,
         fecfin             DATE
        END RECORD
DEFINE  p                   RECORD
         length             SMALLINT,
         topmg              SMALLINT,
         botmg              SMALLINT,
         lefmg              SMALLINT,
         rigmg              SMALLINT
        END RECORD
DEFINE  gcodemp             LIKE fac_cajchica.codemp 
DEFINE  existe              SMALLINT
DEFINE  tituloreporte       STRING 
DEFINE  filename            STRING
DEFINE  pipeline            STRING
DEFINE  s,d,nc,apostrofe    CHAR(1) 
DEFINE  np                  CHAR(3) 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarReportes1")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rgastosxtipodil")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL gasrep002_GastosTiposDiligencia() 
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION gasrep002_GastosTiposDiligencia() 
 DEFINE imp1      datosreporte,
        wpais     VARCHAR(255),
        qrytxt    STRING,
        qrypart   STRING,
        loop      SMALLINT,
        w         ui.Window, 
        f         ui.Form   

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep002a AT 5,2
  WITH FORM "gasrep002a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/GastosTiposDiligencia.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox de empresas
  LET gcodemp = librut003_DCbxEmpresaEnUsoXUsuario()
  IF gcodemp IS NULL THEN
     CALL fgl_winmessage(
     "Atencion","Usuario sin empresa asignada, VERIFICA.","stop")
     CLOSE WINDOW wrep002a 
     RETURN
  END IF

  -- Llenando combobox de tipos de diligencia 
  CALL librut003_CbxTiposDiligencias()

  -- Inicio del loop
  OPTIONS INPUT WRAP 
  LET apostrofe = "'" 
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE pipeline TO NULL
   LET datos.codemp = gcodemp 
   LET s  = 1 SPACES 
   LET nc = "E"
   CLEAR FORM
   DISPLAY BY NAME datos.codemp
   
   -- Ingresando datos 
   DIALOG ATTRIBUTES(UNBUFFERED,FIELD ORDER FORM)

     -- Construyendo busqueda
     INPUT BY NAME datos.tipdlg,
                   datos.fecini,
                   datos.fecfin
     END INPUT 

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT DIALOG

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 
     LET p.length = 57  
     LET p.topmg  = 3
     LET p.botmg  = 2
     LET p.lefmg  = 2
     LET p.rigmg  = 2

     -- Verificando ingreso de filtros
     IF NOT gasrep002_FiltrosCompletos() THEN 
        NEXT FIELD tipdlg 
     END IF 
     EXIT DIALOG 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 
     LET p.length = 57  
     LET p.topmg  = 0
     LET p.botmg  = 0
     LET p.lefmg  = 0
     LET p.rigmg  = 0

     -- Verificando ingreso de filtros
     IF NOT gasrep002_FiltrosCompletos() THEN 
        NEXT FIELD tipdlg 
     END IF 
     EXIT DIALOG 

    ON ACTION excel 
     -- Asignando dispositivo 
     LET pipeline = "excel" 
     LET s = ASCII(9) 
     LET p.length = 57  
     LET p.topmg  = 0
     LET p.botmg  = 0
     LET p.lefmg  = 0
     LET p.rigmg  = 0

     -- Verificando ingreso de filtros
     IF NOT gasrep002_FiltrosCompletos() THEN 
        NEXT FIELD tipdlg 
     END IF 
     EXIT DIALOG

   END DIALOG 
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Construyendo seleccion 
   LET qrytxt = "SELECT a.* ",
                 "FROM  vis_gastosxtipodil a ",
                 "WHERE a.codemp = ",datos.codemp,
                 "  AND a.tipdlg = "||datos.tipdlg,
                 "  AND a.fecgto >= '",datos.fecini,"'", 
                 "  AND a.fecgto <= '",datos.fecfin,"'", 
                 " ORDER BY a.codemp,a.tipdlg,a.fecgto" 

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep002 FROM qrytxt 
   DECLARE c_crep002 CURSOR FOR c_rep002
   LET existe = FALSE
   FOREACH c_crep002 INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       LET existe = TRUE
       START REPORT gasrep002_ImprimirGastosDiligencias TO filename 
    END IF 

    -- Llenando el reporte
    OUTPUT TO REPORT gasrep002_ImprimirGastosDiligencias(imp1.*)
   END FOREACH
   CLOSE c_crep002 
   FREE  c_crep002 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT gasrep002_ImprimirGastosDiligencias 

      -- Transfiriendo reporte a excel
      IF pipeline = "excel" THEN
         CALL gasrep002_excel(filename)
      ELSE
       -- Enviando reporte al destino seleccionado
       CALL librut001_sendreport
       (filename,pipeline,tituloreporte, 
        "--noline-numbers "||
        "--nofooter "||
        "--font-size 7 "||
        "--page-width 842 --page-height 595  "||
        "--left-margin 35 --right-margin 15 "||
        "--top-margin 30 --bottom-margin 35 "||
        "--title Gastos")
      END IF

      ERROR "" 
      CALL fgl_winmessage("Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(
      "Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 
  END WHILE

  OPTIONS INPUT NO WRAP 

 CLOSE WINDOW wrep002a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION gasrep002_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE

 -- Verificando datos 
 IF datos.fecini IS NULL OR 
    datos.fecfin IS NULL THEN 
    CALL fgl_winmessage(
    "Atencion",
    "Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte 

REPORT gasrep002_ImprimirGastosDiligencias(imp1)
 DEFINE imp1              datosreporte,
        linea             CHAR(171),
        lineaexcel        CHAR(171), 
        col,i,nmovs,lg    INTEGER,
        xnomdlg           CHAR(40),
        espacios          CHAR(4), 
        fechareporte      STRING

  OUTPUT LEFT MARGIN   p.lefmg
         TOP MARGIN    p.topmg
         BOTTOM MARGIN p.botmg
         PAGE LENGTH   p.length

  FORMAT 
   PAGE HEADER

    -- Llenando linea
    LET lg = 171 
    LET linea = NULL
    FOR i = 1 TO lg
     LET linea = linea CLIPPED,"-"
    END FOR  

    -- Imprimiendo Encabezado
    LET col = librut001_centrado(tituloreporte,lg) 
    PRINT COLUMN   1,"Gastos",
          COLUMN col,UPSHIFT(tituloreporte) CLIPPED,  
	  COLUMN (lg-20),PAGENO USING "Pagina: <<<<"

    LET fechareporte = "PERIODO DE FECHAS ",
                       "DEL ",datos.fecini," AL ",datos.fecfin 
    LET col = librut001_centrado(fechareporte,lg) 
    PRINT COLUMN   1,"gasrep002",
          COLUMN col,fechareporte CLIPPED,  
          COLUMN (lg-20),"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    LET col = librut001_centrado(imp1.nomemp,lg) 
    PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
          COLUMN col,imp1.nomemp CLIPPED,
          COLUMN (lg-20),"Hora  : ",TIME 

    PRINT linea CLIPPED 
    PRINT "Fecha       Tipo  Numero               Numero                       ",
          "Total         Total         Total         Total         Total       ",
          "  Total          Total  Tipo  "
    PRINT "Gasto       Docto Serie                Documento                  GR",
          "AVADO        EXENTO         IDP          INGUAT     OTRAS/DED       ",
          "    IVA          Gasto  Compra"
    PRINT linea

  BEFORE GROUP OF imp1.codemp 
   -- Verificando si reporte es a excel 
   IF (pipeline="excel") THEN 
      -- Imprimiendo datos de la cuenta 
      PRINT tituloreporte CLIPPED,s 
      PRINT imp1.nomemp CLIPPED,s
      PRINT "Periodo Del ",datos.fecini," Al ",datos.fecfin,s
      PRINT linea
      PRINT s 
      PRINT "Fecha Gasto",s,
            "Tipo Docto",s, 
            "Numero Serie",s,
            "Numero Documento",s,
            "Total Gasto",s,
            "Total EXENTO",s,
            "Total IDP",s,
            "Total INGUAT",s,
            "Total OTRAS DED",s,
            "Total IVA",s,
            "Total Gasto",s,
            "Tipo Compra",s,
            "Descriocion",s
    PRINT s 
   END IF 

  BEFORE GROUP OF imp1.tipdlg 
   PRINT "DILIGENCIA [ ",imp1.nomdlg CLIPPED," ]",s 
   LET nmovs = 0 

  ON EVERY ROW 
   -- Imprimiendo movimientos 
   LET nmovs = (nmovs+1)
   LET espacios = s,s,s,s 
   PRINT s 

   -- Verificando si reportes es a excel 
   IF (pipeline="excel") THEN 
    PRINT imp1.fecgto                           ,s,
          imp1.tipdoc                           ,s,
          apostrofe CLIPPED,imp1.nserie         ,s,
          apostrofe CLIPPED,imp1.ndocto         ,s,
          imp1.totgra     USING  "##,###,##&.&&",s,
          imp1.totexe     USING  "##,###,##&.&&",s,
          imp1.totedp     USING  "##,###,##&.&&",s,
          imp1.totigt     USING  "##,###,##&.&&",s,
          imp1.totded     USING  "##,###,##&.&&",s,
          imp1.totisv     USING  "##,###,##&.&&",s,
          imp1.valgto     USING "###,###,##&.&&",s,
          imp1.nomcom     CLIPPED               ,s,
          imp1.descrp     CLIPPED               ,s 
   ELSE
    PRINT linea 
    PRINT imp1.fecgto                           ,s,1 SPACES,
          imp1.tipdoc                           ,s,
          imp1.nserie                           ,s,
          imp1.ndocto                           ,s,
          imp1.totgra     USING  "##,###,##&.&&",s,
          imp1.totexe     USING  "##,###,##&.&&",s,
          imp1.totedp     USING  "##,###,##&.&&",s,
          imp1.totigt     USING  "##,###,##&.&&",s,
          imp1.totded     USING  "##,###,##&.&&",s,
          imp1.totisv     USING  "##,###,##&.&&",s,
          imp1.valgto     USING "###,###,##&.&&",s,1 SPACES, 
          imp1.nomcom     CLIPPED 
    PRINT s
    PRINT COLUMN  1,"Descripcion: ",imp1.descrp[1,156] 

    -- Verifica si continua descripcion
    IF (LENGTH(imp1.descrp)>156) THEN
       PRINT COLUMN 14,espacios CLIPPED,imp1.descrp[157,312],s
    END IF
    -- Verifica si continua descripcion 
    IF (LENGTH(imp1.descrp)>312) THEN
       PRINT COLUMN 14,espacios CLIPPED,imp1.descrp[313,468],s
    END IF
    -- Verifica si continua descripcion 
    IF (LENGTH(imp1.descrp)>468) THEN
       PRINT COLUMN 14,espacios CLIPPED,imp1.descrp[469,624],s
    END IF
    -- Verifica si continua descripcion 
    IF (LENGTH(imp1.descrp)>624) THEN
       PRINT COLUMN 14,espacios CLIPPED,imp1.descrp[625,780],s
    END IF
    -- Verifica si continua descripcion 
    IF (LENGTH(imp1.descrp)>780) THEN
       PRINT COLUMN 14,espacios CLIPPED,imp1.descrp[781,936],s
    END IF
    -- Verifica si continua descripcion 
    IF (LENGTH(imp1.descrp)>936) THEN
       PRINT COLUMN 14,espacios CLIPPED,imp1.descrp[937,1092],s
    END IF
    -- Verifica si continua descripcion 
    IF (LENGTH(imp1.descrp)>1092) THEN
       PRINT COLUMN 14,espacios CLIPPED,imp1.descrp[1093,1200],s
    END IF
   END IF 
   PRINT linea 

  AFTER GROUP OF imp1.tipdlg 
   -- Totalizando
   PRINT s
   PRINT COLUMN   1,"TOTAL [ ",imp1.nomdlg CLIPPED," ]",espacios, 
         COLUMN  61,GROUP SUM(imp1.totgra)    USING  "##,###,##&.&&",s,
                    GROUP SUM(imp1.totexe)    USING  "##,###,##&.&&",s,
                    GROUP SUM(imp1.totedp)    USING  "##,###,##&.&&",s,
                    GROUP SUM(imp1.totigt)    USING  "##,###,##&.&&",s,
                    GROUP SUM(imp1.totded)    USING  "##,###,##&.&&",s,
                    GROUP SUM(imp1.totisv)    USING  "##,###,##&.&&",s,
                    GROUP SUM(imp1.valgto)    USING "###,###,##&.&&",s 
   PRINT s

  ON LAST ROW
   -- Totalizando
   PRINT COLUMN   1,"TOTAL GASTOS POR TIPOS DE DILIGENCIA:",espacios,
         COLUMN  61,SUM(imp1.totgra)          USING  "##,###,##&.&&",s,
                    SUM(imp1.totexe)          USING  "##,###,##&.&&",s,
                    SUM(imp1.totedp)          USING  "##,###,##&.&&",s,
                    SUM(imp1.totigt)          USING  "##,###,##&.&&",s,
                    SUM(imp1.totded)          USING  "##,###,##&.&&",s,
                    SUM(imp1.totisv)          USING  "##,###,##&.&&",s,
                    SUM(imp1.valgto)          USING "###,###,##&.&&",s 
   PRINT s

   -- Imprimiendo filtros
   PRINT "** Filtros **",s
   IF datos.tipdlg IS NOT NULL THEN
    SELECT a.nomdlg INTO xnomdlg FROM glb_tiposdlg a WHERE a.tipdlg = datos.tipdlg 
    PRINT "Tipo de Diligencia :",s,xnomdlg,s
   END IF
END REPORT 

-- Subrutina para exportar el reporte a excel usando WinCOM 

FUNCTION gasrep002_excel(filename) 
 DEFINE sb          base.StringBuffer
 DEFINE ch          base.channel
 DEFINE lista       base.StringTokenizer
 DEFINE result      INTEGER
 DEFINE i           INTEGER
 DEFINE xlapp       INTEGER
 DEFINE xlwb        INTEGER
 DEFINE linea       STRING 
 DEFINE lineadep    STRING 
 DEFINE filename    STRING 
 DEFINE columna     STRING 
 DEFINE separador   CHAR(1) 
 DEFINE nuevalinea  CHAR(1) 

 -- Inicializando variables                            
 LET xlapp = -1
 LET xlwb = -1

 -- Creando la instancia de Excel
 CALL ui.Interface.frontCall("WinCOM", "CreateInstance", ['Excel.Application'], [xlapp])
 CALL CheckError(xlapp, xlwb, xlapp)
   
 -- Agregando un libro al documento de excel               
 CALL ui.interface.frontCall("WinCOM", "CallMethod", [xlapp, 'WorkBooks.Add'], [xlwb])
 CALL CheckError(xlapp, xlwb, xlwb)
   
 -- Haciendo visible el libro                 
 CALL excel_set_property(xlapp, xlapp, "Visible","True")
  
 -- Llenando celdas desde el FrontCall 
 -- Construyendo celdas delimitando columnas por TAB (ASCII(9)), 
 -- y filas por CR (ASCII(10)
 -- Copia al clipboard via el Frontcall y luego instruye a excel para pegar el contenido
 -- de la celdas dentro del libro 
 LET sb = base.StringBuffer.create()

 -- Leyendo el archivo que contiene el reporte y copiandolo al clipboard 
 LET ch = base.channel.create()
 call ch.openfile(filename,"r")

 let i = 1 
 let separador  = ascii(9) 
 let nuevalinea = ascii(10)
 while true
  let linea = ch.readLine() 
  if ch.isEof() then exit while end if
  if linea.getindexof(separador,1)>0 then
     CALL sb.append(linea)
     CALL sb.append(nuevalinea)
  end if 
  let i = i+1
 end while
 call ch.close() 

 -- Pegando el contenido de las celdas en el clipboard al libro de excel
 CALL ui.Interface.FrontCall("standard","cbset",sb.toString(),result)
 CALL excel_call_method(xlapp, xlwb, 'ActiveSheet.Paste')
 CALL excel_call_method(xlapp, xlwb, 'Columns("A:Z").EntireColumn.AutoFit')
 CALL excel_set_property(xlapp, xlwb, excel_column(nc,"K","NumberFormat"), "0.00")

 -- Liberando informacion del clipboard    
 CALL freeMemory(xlapp, xlwb)
END FUNCTION
