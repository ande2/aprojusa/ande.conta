{ 
Programo : Mynor Ramirez
Fecha    : Abril 2014   
Objetivo : Programa de criterios de seleccion para:
           Consulta/Eliminar transacciones contables   
}

-- Definicion de variables globales 
GLOBALS "ctbglb001.4gl"
CONSTANT maxdiaeli = 500 
CONSTANT maxdiamod = 500 
CONSTANT maxdiaanu = 500 

-- Subrutina para consultar/eliminar 

FUNCTION ctbqbx001_transacciones(operacion,xlnkapl,xcodapl)
 DEFINE xcodapl             LIKE ctb_mtransac.codapl,
        xlnkapl             LIKE ctb_mtransac.lnkapl, 
        qrytext,qrypart     CHAR(500),
        loop,existe         SMALLINT,
        operacion           SMALLINT,
        acceso,haydoctos    SMALLINT, 
        titmenu             CHAR(15),
        qryres,msg,titqry   CHAR(80),
        opc,totreg          INTEGER, 
        userauth            CHAR(15) 

 -- Verificando operacion
 CASE (operacion)
  WHEN 1 LET titmenu     = "Consultar"
  WHEN 2 LET titmenu     = "Modificar" 
  WHEN 3 LET titmenu     = "Eliminar" 
  WHEN 4 LET titmenu     = "Eliminar" 
 END CASE 

 -- Definiendo nivel de aislamiento 
 SET ISOLATION TO DIRTY READ 

 -- Buscando datos 
 LET loop = TRUE 
 WHILE loop 
  -- Inicializando las variables 
  CALL ctbing001_EstadoMenu(operacion," ")
  INITIALIZE qrytext,qrypart TO NULL
  LET int_flag = 0 
  CLEAR FORM

  -- Verificando si consulta es ingresada
  IF (xlnkapl=0) THEN
   -- Construyendo busqueda 
   CONSTRUCT BY NAME qrytext 
                  ON a.lnktra,
                     a.tiptrn,
                     a.fecemi,
                     a.numdoc,
                     a.concep, 
                     a.userid,
                     a.fecsis, 
                     a.horsis,
                     a.codapl,
                     a.lnkapl, 
                     a.infadi

    ON ACTION cancel
     -- Salida
     LET loop = FALSE
     EXIT CONSTRUCT
   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Preparando la busqueda 
   ERROR " Seleccionando datos ... por favor espere ..." ATTRIBUTE(CYAN) 

   -- Creando la busqueda 
   LET qrypart = "SELECT a.lnktra ",
                  "FROM  ctb_mtransac a ",
                  "WHERE a.codemp = ",gcodemp," AND ",qrytext CLIPPED, 
                  " ORDER BY a.codemp,a.fecemi DESC,a.lnktra DESC" 


  ELSE
   -- Creando la busqueda 
   LET qrypart = "SELECT a.lnktra ",
                  "FROM  ctb_mtransac a ",
                  "WHERE a.codapl = '",xcodapl,"' ", 
                   " AND a.lnkapl = ",xlnkapl  
  END IF 

  -- Contando registros
  LET totreg = librut003_NumeroRegistros(qrypart)

  -- Declarando el cursor 
  PREPARE estqbe001 FROM qrypart
  DECLARE c_transac SCROLL CURSOR WITH HOLD FOR estqbe001  
  OPEN c_transac 
  FETCH FIRST c_transac INTO w_mae_tra.lnktra
  IF (status = NOTFOUND) THEN
     INITIALIZE w_mae_tra.* TO NULL
     ERROR ""
     CALL fgl_winmessage(
     "Atencion",
     "No existen transacciones con el criterio seleccionado.",
     "stop") 

     -- Verificando si es consulta automatica de otro auxiliar
     IF (xlnkapl>0) THEN
        EXIT WHILE 
     END IF 
  ELSE
    ERROR "" 

    -- Desplegando datos 
    LET msg = " - Registros [ ",totreg||" ]"
    CALL ctbing001_EstadoMenu(operacion,msg)
    CALL ctbqbx001_datos()

    -- Verificando si consulta es ingresada
    IF (xlnkapl=0) THEN 
     -- Fetchando documentos 
     MENU titmenu 
      BEFORE MENU 
       -- Verificando tipo de operacion 
       CASE (operacion)
        WHEN 1 HIDE OPTION "Modificar" 
               HIDE OPTION "Anular" 
               HIDE OPTION "Eliminar" 
        WHEN 2 HIDE OPTION "Eliminar" 
               HIDE OPTION "Anular" 
               HIDE OPTION "Cuentas" 
        WHEN 3 HIDE OPTION "Modificar" 
               HIDE OPTION "Anular" 
               HIDE OPTION "Cuentas" 
        WHEN 4 HIDE OPTION "Modificar" 
               HIDE OPTION "Eliminar" 
       END CASE 

      COMMAND "Anterior"
       "Visualiza la transaccion anterior en la lista."
       FETCH NEXT c_transac INTO w_mae_tra.lnktra 
        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas transacciones siguientes en lista.", 
           "information")
           FETCH LAST c_transac INTO w_mae_tra.lnktra
        END IF 

       -- Desplegando datos 
       CALL ctbqbx001_datos()

      COMMAND "Siguiente"
       "Visualiza la siguiente transaccion en la lista."
       FETCH PREVIOUS c_transac INTO w_mae_tra.lnktra
        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas transacciones anteriores en lista.", 
           "information")
           FETCH FIRST c_transac INTO w_mae_tra.lnktra
        END IF

       -- Desplegando datos 
       CALL ctbqbx001_datos()

      COMMAND "Primera" 
       "Visualiza la primer transaccion en la lista."
       FETCH FIRST c_transac INTO w_mae_tra.lnktra
        -- Desplegando datos 
        CALL ctbqbx001_datos()

      COMMAND "Ultima" 
       "Visualiza la ultima transaccion en la lista."
       FETCH LAST c_transac INTO w_mae_tra.lnktra
        -- Desplegando datos
        CALL ctbqbx001_datos()

      COMMAND "Modificar" 
       "Permite modificar la transaccion en pantalla." 

       -- Verificando si transaccion ya fue anulada
       IF (w_mae_tra.estado=0) THEN
           CALL fgl_winmessage(
           " Atencion",
           " Transaccion anulada o de anulacion.\n"||
           " Transaccion no puede modificarse.", 
           "stop") 
           CONTINUE MENU 
       END IF 

       -- Verificando fecha
       IF (w_mae_tra.fecemi<(TODAY-maxdiamod)) THEN
          CALL fgl_winmessage(
          " Atencion",
          " Maximo de "||maxdiamod||" dias para modificar excedido, VERIFICA. \n"||
          " Transaccion no puede modificarse.",
          "stop")
          CONTINUE MENU 
       END IF

       -- Verificando si transaccion fue originada desde algun auxiliar 
       IF (w_mae_tra.lnkapl>0) THEN
          CALL fgl_winmessage(
          " Atencion",
          " Transaccion originada desde un auxiliar.\n"||
          " Transaccion no puede modificarse desde la contabilidad.", 
          "stop") 
          CONTINUE MENU 
       END IF 

       -- Modificando transaccion 
       CALL ctbing001_Transacciones(2)

       -- Desplegando datos 
       CALL ctbqbx001_datos()

      COMMAND "Anular"    
       "Permite anular la transaccion en pantalla." 

       -- Verificando si transaccion ya fue anulada
       IF (w_mae_tra.estado=0) THEN
           CALL fgl_winmessage(
           " Atencion",
           " Transaccion anulada o de anulacion.\n"||
           " Transaccion no puede anularse.", 
           "stop") 
           CONTINUE MENU 
       END IF 

       -- Verificando fecha
       IF (w_mae_tra.fecemi<(TODAY-maxdiaanu)) THEN
          CALL fgl_winmessage(
          " Atencion",
          " Maximo de "||maxdiaanu||" dias para anular excedido, VERIFICA. \n"||
          " Transaccion no puede anularse.", 
          "stop") 
          CONTINUE MENU 
       END IF

       -- Verificando si transaccion fue originada desde algun auxiliar 
       IF (w_mae_tra.lnkapl>0) THEN
           CALL fgl_winmessage(
           " Atencion",
           " Transaccion originada desde un auxiliar.\n"||
           " Transaccion no puede anularse desde la contabilidad.", 
           "stop") 
           CONTINUE MENU 
       END IF 

       -- Anulando transaccion 
       IF ctbing001_Anular(1) THEN 
          EXIT MENU
       END IF 

      COMMAND "Eliminar"  
       "Permite eliminar la transaccion en pantalla." 

       -- Verificando si transaccion ya fue anulada
       IF (w_mae_tra.estado=0) THEN
           CALL fgl_winmessage(
           " Atencion",
           " Transaccion anulada o de anulacion.\n"||
           " Transaccion no puede eliminarse.", 
           "stop") 
           CONTINUE MENU 
       END IF 

       -- Verificando fecha
       IF (w_mae_tra.fecemi<(TODAY-maxdiaeli)) THEN
          CALL fgl_winmessage(
          " Atencion",
          " Maximo de "||maxdiaeli||" dias para eliminar excedido, VERIFICA. \n"||
          " Transaccion no puede eliminarse.", 
          "stop") 
          CONTINUE MENU 
       END IF

       -- Verificando si transaccion fue originada desde algun auxiliar 
       IF (w_mae_tra.lnkapl>0) THEN
           CALL fgl_winmessage(
           " Atencion",
           " Transaccion originada desde un auxiliar.\n"||
           " Transaccion no puede eliminarse desde la contabilidad.", 
           "stop") 
           CONTINUE MENU 
       END IF 

       -- Eliminando documento 
       IF ctbqbx001_EliminarTransaccion() THEN
          EXIT MENU
       END IF 

      COMMAND "Cuentas"
       "Permite visualizar el detalle completo de cuentas de la transaccion." 
       CALL ctbqbx001_VerCuentas()   

      COMMAND "Consultar" 
       "Regresa a la pantalla de seleccion."
       EXIT MENU

      ON ACTION cancel
       LET loop = FALSE
       EXIT MENU

      COMMAND KEY(F4,CONTROL-E)
       EXIT MENU
     END MENU
    ELSE
     -- Consulta automatica desplegando cuentas  
     CALL ctbqbx001_VerCuentas()   
     LET loop = FALSE
    END IF 
   END IF     
  CLOSE c_transac
 END WHILE

 -- Inicializando datos 
 CALL ctbing001_inival(1)

 -- Desplegando estado del menu
 CALL ctbing001_EstadoMenu(0,"")
END FUNCTION 

-- Subrutina para desplegar los datos del documento 

FUNCTION ctbqbx001_datos()
 DEFINE existe SMALLINT

 -- Desplegando datos 
 CLEAR FORM

 -- Obteniendo datos del documento 
 CALL librut003_BTransaccionContable(w_mae_tra.lnktra)
 RETURNING w_mae_tra.*,existe

 -- Desplegando datos del documento 
 CLEAR FORM 
 DISPLAY BY NAME w_mae_tra.lnktra, 
                 w_mae_tra.codemp,
                 w_mae_tra.tiptrn, 
                 w_mae_tra.fecemi,
                 w_mae_tra.numdoc,
                 w_mae_tra.concep, 
                 w_mae_tra.userid,
                 w_mae_tra.fecsis,  
                 w_mae_tra.horsis,
                 w_mae_tra.codapl, 
                 w_mae_tra.lnkapl, 
                 w_mae_tra.infadi

 -- Seleccionando detalle de cuentas de la transaccion 
 CALL ctbqbx001_DetalleCuentas(w_mae_tra.lnktra)
END FUNCTION 

-- Subrutina para seleccionar datos del detalle de la transaccion 

FUNCTION ctbqbx001_DetalleCuentas(wlnktra)
 DEFINE existe     SMALLINT,
        wlnktra    INT 

 -- Inicializando vector de cuentas 
 CALL ctbing001_inivec()

 -- Seleccionando detalle de la transaccion 
 DECLARE cdet CURSOR FOR
 SELECT x.numcta,
        y.nomcta, 
        x.totdeb, 
        x.tothab,
        x.concep, 
        "",
        x.correl
  FROM  ctb_dtransac x,ctb_mcuentas y 
  WHERE (x.lnktra = w_mae_tra.lnktra) 
    AND (y.codemp = w_mae_tra.codemp)
    AND (y.numcta = x.numcta)
  ORDER BY x.correl 

  LET totlin = 1 
  FOREACH cdet INTO v_partida[totlin].*
   -- Acumulando contador
   LET totlin = (totlin+1) 
  END FOREACH
  CLOSE cdet
  FREE  cdet
  LET totlin = (totlin-1)  

  -- Despelgando datos del detalle
  DISPLAY ARRAY v_partida TO s_partida.*
   ATTRIBUTE(COUNT=totlin) 
   BEFORE DISPLAY
    EXIT DISPLAY
  END DISPLAY 

  -- Desplegando totales
  CALL ctbing001_TotalCuentas()
END FUNCTION 

-- Subrutina para ver las cuentas completas de una transaccion contable 

FUNCTION ctbqbx001_VerCuentas()
 -- Desplegando cuentas 
 DISPLAY ARRAY v_partida TO s_partida.*
  ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE)

  ON ACTION cancel 
   -- Salida
   EXIT DISPLAY 

  BEFORE ROW 
   -- Verificando control del total de lineas
   IF (ARR_CURR()>totlin) THEN
      CALL FGL_SET_ARR_CURR(1)
   END IF
 END DISPLAY 
END FUNCTION 

-- Subrutina para eliminar la transaccion 

FUNCTION ctbqbx001_EliminarTransaccion()
 DEFINE opc,elimina SMALLINT,
        msg         STRING 

 -- Verificando operacion
 LET msg = "Desea Eliminar la Transaccion"
 IF librut001_yesornot("Confirmacion",msg,"Si","No","question") THEN
  LET elimina = TRUE

  -- Iniciando Transaccion
  BEGIN WORK

   -- Eliminando documento
   DELETE FROM ctb_mtransac 
   WHERE (ctb_mtransac.lnktra = w_mae_tra.lnktra) 

  -- Finalizando Transaccion
  COMMIT WORK

  LET msg = "Numero de Transaccion ["||w_mae_tra.lnktra||"] Eliminada."
 ELSE 
  LET elimina = FALSE 
  LET msg     = "Eliminacion Cancelada."
 END IF 

 -- Desplegando mensaje de accion 
 CALL fgl_winmessage("Atencion",msg,"information")

 RETURN elimina 
END FUNCTION 
