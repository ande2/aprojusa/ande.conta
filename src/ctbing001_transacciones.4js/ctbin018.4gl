{ 
CREDICORP
Programa: ctbin018.4gl
Programo: Mynor Ramirez Seijas
Fecha   : Febrero 1995.     
Objetivo: Emision de transacciones contables.
}

{ Definiendo variables globales }

GLOBALS "../cta/ctagb001.4gl"
DEFINE w_mae_trn        RECORD LIKE cb_mtransac.*,
       w_mae_apl        RECORD LIKE gb_aplicacs.*,
       w_mae_ctb        RECORD LIKE cb_mcuentas.*,
       w_trn_ctb        RECORD LIKE cb_tipotrns.*,
       w_mae_ncl        RECORD LIKE cb_mcatalog.*,
       w_mae_emp        RECORD LIKE empresas.*,
       v_partida        ARRAY[200] OF RECORD
        cuenta          LIKE cb_dtransac.cuenta,
        nombre          LIKE cb_mcuentas.abreviado,
        debe            LIKE cb_dtransac.debe,
        haber           LIKE cb_dtransac.haber,
        concepto        LIKE cb_dtransac.concepto
       END RECORD,
       totdebe,tothaber LIKE cb_mtransac.monto,
       tot_cta          SMALLINT

{ Subrituna para ingresar la transaccion }

FUNCTION in018_transacc()
 # Menu de Opciones
 OPEN WINDOW win018x AT 3,1
  WITH 4 ROWS,78 COLUMNS 
   CALL draw_border(1,1,4,78,"")
   CALL messages(1,66,2,0,"Ctrl-W Ayuda")
   OPEN WINDOW win018y AT 4,3
    WITH 2 ROWS,75 COLUMNS
    MENU "Transacciones"  
     COMMAND "Consultar" "Consulta de transacciones existentes."
      IF acceso("CB","ctbin018c") THEN
         CALL hidewin(1)
         CALL in018_consulta() 
         CLOSE WINDOW hwin1
      END IF 
     COMMAND "Ingresar" "Ingreso de nuevas transacciones."
      IF acceso("CB","ctbin018i") THEN
         CALL hidewin(1)
         CALL in018_ingtrns()
         CLOSE WINDOW hwin1
      END IF 
     COMMAND "Regreso" "Regresa al menu anterior."
      EXIT MENU
     COMMAND KEY (F4) 
      EXIT MENU
     COMMAND KEY("!")
      CALL subshell() 
    END MENU
  CLOSE WINDOW win018y
 CLOSE WINDOW win018x
END FUNCTION 

{ Subrutina para ingresar las transacciones }

FUNCTION in018_ingtrns()
 DEFINE w_transac                           RECORD LIKE cb_mtransac.*,
        loop,retroceso,detalle,arr,scr,ln,i SMALLINT,
        lastkey,wacceso,regreso             SMALLINT,
        anclaje                             SMALLINT,
        hilera                              CHAR(80), 
        wmsg                                CHAR(80),
        wusr                                CHAR(10)

 OPEN WINDOW win018a AT 4,1 WITH FORM "ctbfin18a"
  ATTRIBUTE (FORM LINE 1,PROMPT LINE LAST,MESSAGE LINE LAST)
  CALL messages(1,0,3,78,"Emision de Transacciones")
  CALL draw_border(2,1,4,78,"")
  CALL draw_border(6,1,13,78,"")
  DISPLAY " P A R T I D A " AT 11,3  ATTRIBUTE(REVERSE) 
  DISPLAY "Cuenta"          AT 12,3  ATTRIBUTE(BOLD)
  DISPLAY "Concepto"        AT 13,3  ATTRIBUTE(BOLD)
  DISPLAY "Abreviatura"     AT 13,20 ATTRIBUTE(BOLD)
  DISPLAY "Debe"            AT 13,60 ATTRIBUTE(BOLD) 
  DISPLAY "Haber"           AT 13,73 ATTRIBUTE(BOLD) 

  CALL msgup(1,"F4 Regreso  Ctrl-T Calendario  Ctrl-B Calculadora",5)
  CURRENT WINDOW IS win018a

  # Inicio del loop
  LET filename  = ARG_VAL(3) CLIPPED,"/bcoin018.spl"
  LET loop      = TRUE
  LET retroceso = FALSE  
  LET anclaje   = FALSE
  WHILE loop 
   IF NOT retroceso THEN
      # Inicializando datos
      INITIALIZE w_mae_trn.* TO NULL 
      CALL in018_inidat(1)
   END IF
   # Ingresando datos de la transaccion
   INPUT BY NAME w_mae_trn.empresa,
                 w_mae_trn.tipo_trns,
                 w_mae_trn.documento,
                 w_mae_trn.f_emision,
                 w_mae_trn.concepto1,
                 w_mae_trn.concepto2,
                 w_mae_trn.concepto3
                 WITHOUT DEFAULTS
     ON KEY(F4)
      # Salida
      IF INFIELD(empresa) THEN
         LET anclaje   = FALSE
         LET loop = FALSE
         EXIT INPUT
      ELSE
         IF INFIELD(tipo_trns) THEN
            # Inicializando datos
            INITIALIZE w_mae_trn.* TO NULL 
            LET anclaje = FALSE
            CALL in018_inidat(1)
            NEXT FIELD empresa
         ELSE
            IF INFIELD(documento) THEN
               # Inicializando datos
               INITIALIZE w_mae_trn.tipo_trns 
               THRU w_mae_trn.linkapl TO NULL 
               CALL in018_inidat(2)
               LET retroceso = FALSE
               LET anclaje = FALSE
               NEXT FIELD tipo_trns 
            ELSE
               INITIALIZE w_mae_trn.f_emision 
               THRU w_mae_trn.linkapl TO NULL 
               CALL in018_inidat(3)
               LET retroceso = FALSE
               LET anclaje = FALSE
               NEXT FIELD documento
            END IF
         END IF 
      END IF 
     ON KEY(F1)
      # Lista de empresas
      IF INFIELD(empresa) THEN
         LET hilera = view_empresas()
         IF hilera IS NOT NULL THEN
            LET w_mae_trn.empresa = hilera[1,2] 
            DISPLAY BY NAME w_mae_trn.empresa
         END IF 
      END IF  
      # Lista de tipos de transaccion
      IF INFIELD(tipo_trns) THEN
         LET hilera = view_tiptrns()
         IF hilera IS NOT NULL THEN
            LET w_mae_trn.tipo_trns = hilera[1,6] 
            DISPLAY BY NAME w_mae_trn.tipo_trns     
         END IF 
      END IF  
     ON KEY(CONTROL-T)
      # Calendario de ayuda
      CALL calendario(6,4)
     ON KEY(CONTROL-B)
      # Calculadora
      RUN "op calculator"
     ON KEY(INTERRUPT)
      # Atrapando interrupts
      ERROR "Tecla Invalida."
     BEFORE FIELD empresa
      IF retroceso THEN
         IF NOT anclaje THEN
            NEXT FIELD f_emision
         ELSE
            # Inicializando datos
            INITIALIZE w_mae_trn.f_emision 
            THRU w_mae_trn.linkapl TO NULL 
            CALL in018_inidat(3)
            LET retroceso = FALSE
            LET anclaje = FALSE
            NEXT FIELD documento 
         END IF 
      END IF 
     AFTER FIELD empresa
      IF w_mae_trn.empresa IS NULL THEN
         ERROR "Error: empresa invalida."
         CLEAR nempresa
         NEXT FIELD empresa
      END IF

      # Verificando si la empresa existe                 
      INITIALIZE w_mae_emp.* TO NULL
      CALL busca_empresa(w_mae_trn.empresa) 
      RETURNING w_mae_emp.*,existe
      IF NOT existe THEN
         ERROR "Error: empresa no existe."
         LET w_mae_trn.empresa = NULL
         CLEAR nempresa
         NEXT FIELD empresa
      END IF 
      DISPLAY w_mae_emp.nombre TO nempresa

      # Verificando si usuario tiene permiso para operar la empresa
      IF NOT authorization("GB",2,w_mae_trn.empresa,0) THEN
        LET wmsg=" Atencion: no tienes autorizacion para trabajar esta empresa."
        CALL msgerr(wmsg)
        LET w_mae_trn.empresa = NULL
        CLEAR nempresa
        NEXT FIELD empresa
      END IF 

      # Verificando si empresa ya tiene definido el tipo de nomenclatura
      IF w_mae_emp.nomenclatura IS NULL THEN
       CALL msgerr
       (" Atencion: empresa no tiene definido tipo de nomenclatura. (Definalo)")
       LET w_mae_trn.empresa = NULL
       CLEAR nempresa
       NEXT FIELD empresa
      END IF

      # Obteniendo tipo de catalogo a utilizar
      INITIALIZE w_mae_ncl.* TO NULL
      CALL busca_tipnomencl(w_mae_emp.nomenclatura)
      RETURNING w_mae_ncl.*,existe
     AFTER FIELD tipo_trns
      LET lastkey = FGL_LASTKEY()
      IF (lastkey = FGL_KEYVAL("up")) OR (lastkey = FGL_KEYVAL("left")) THEN
         NEXT FIELD tipo_trns
      END IF

      IF w_mae_trn.tipo_trns IS NULL OR 
         (w_mae_trn.tipo_trns <0) OR
         (w_mae_trn.tipo_trns >999) THEN
         ERROR "Error: tipo de transaccion invalida."
         CLEAR ntransac
         NEXT FIELD tipo_trns
      END IF

      # Verificando si el tipo de transaccion existe
      INITIALIZE w_trn_ctb.* TO NULL
      CALL busca_tiptrncb(w_mae_trn.tipo_trns)
      RETURNING w_trn_ctb.*,existe
      IF NOT existe THEN
         ERROR "Error: tipo de transaccion ",w_mae_trn.tipo_trns USING "<<<",
               " no existe."
         LET w_mae_trn.tipo_trns = NULL
         CLEAR tipo_trns,ntransac
         NEXT FIELD tipo_trns
      END IF 
      DISPLAY w_trn_ctb.descripcion TO ntransac

      # Verificando la funcion del tipo de transaccion
      IF (w_trn_ctb.funcion = "R") THEN 
        LET wmsg=" Atencion: tipo de transaccion de referencia. ",
                 "(Emision Cancelada)."
        CALL msgerr(wmsg)
        LET w_mae_trn.tipo_trns = NULL
        CLEAR tipo_trns,ntransac
        NEXT FIELD tipo_trns
      END IF

      # Verificando la funcion del tipo de transaccion
      IF (w_trn_ctb.funcion = "C") OR (w_trn_ctb.tipo_trns = 8) THEN
        LET wmsg=" Atencion: tipo de transaccion de cierre fiscal. ",
                 "(Emision Cancelada)."
        CALL msgerr(wmsg)
        LET w_mae_trn.tipo_trns = NULL
        CLEAR tipo_trns,ntransac
        NEXT FIELD tipo_trns
      END IF

     AFTER FIELD documento
      LET lastkey = FGL_LASTKEY()
      IF (lastkey = FGL_KEYVAL("up")) OR (lastkey = FGL_KEYVAL("left")) THEN
         NEXT FIELD documento 
      END IF

      IF w_mae_trn.documento IS NULL OR
         (w_mae_trn.documento <0) OR
         (w_mae_trn.documento >9999999) THEN
         ERROR "Error: documento invalido."
         NEXT FIELD documento 
      END IF 

      # Verificando si la transaccion existe
      IF (w_trn_ctb.chequeo="N") THEN
         INITIALIZE w_transac.* TO NULL
         CALL busca_transac("N",w_mae_trn.empresa,w_mae_trn.tipo_trns,
                            w_mae_trn.documento,w_mae_trn.f_emision)
         RETURNING w_transac.*,existe

         IF existe THEN
            LET wmsg = " Atencion: documento ",
                       w_mae_trn.documento USING "<<<<<<<<<<",
                       " ya existe. (Verifica)"
            CALL msgerr(wmsg)
            LET w_mae_trn.documento = NULL
            CLEAR documento
            NEXT FIELD documento
         END IF
      END IF 
     BEFORE FIELD f_emision
      LET retroceso = TRUE 
     AFTER FIELD f_emision
      LET lastkey = FGL_LASTKEY()
      IF (lastkey = FGL_KEYVAL("up")) OR (lastkey = FGL_KEYVAL("left")) THEN
         NEXT FIELD f_emision 
      END IF

      IF w_mae_trn.f_emision IS NULL THEN
         ERROR "Error: debes ingresar la fecha de emision de la transaccion."
         NEXT FIELD f_emision
      END IF 

      # Verificando fecha de ultimo cierre
      IF (w_mae_trn.f_emision <= w_mae_emp.f_ultcierre) THEN
         LET wmsg=" Atencion: F/emision menor o igual a F/ultimo cierre ",
                  w_mae_emp.f_ultcierre USING "dd/mmm/yyyy","."
         CALL msgerr(wmsg)
         LET w_mae_trn.f_emision = NULL
         CLEAR f_emision
         NEXT FIELD f_emision
      END IF

      # Verificando fecha de ultimo ejercicio
      IF (w_mae_trn.f_emision <= w_mae_emp.f_ultejerci) THEN
         LET wmsg=" Atencion: F/emision menor o igual a F/ultimo ejercicio ",
                  w_mae_emp.f_ultcierre USING "dd/mmm/yyyy","."
         CALL msgerr(wmsg)
         LET w_mae_trn.f_emision = NULL
         CLEAR f_emision
         NEXT FIELD f_emision
      END IF

      # Verificando si la transaccion existe
      IF (w_trn_ctb.chequeo="S") THEN
         INITIALIZE w_transac.* TO NULL
         CALL busca_transac("S",w_mae_trn.empresa,w_mae_trn.tipo_trns,
                            w_mae_trn.documento,w_mae_trn.f_emision)
         RETURNING w_transac.*,existe

         IF existe THEN
            LET wmsg = " Atencion: documento ",
                       w_mae_trn.documento USING "<<<<<<<<<<",
                       " de fecha ",
                       w_mae_trn.f_emision USING "dd/mmm/yyyy",
                       " ya existe. (Verifica)"
            CALL msgerr(wmsg)
            LET w_mae_trn.f_emision = NULL
            CLEAR f_emision 
            NEXT FIELD f_emision 
         END IF
      END IF 
     AFTER FIELD concepto1
      IF w_mae_trn.concepto1 IS NULL THEN
         ERROR "Error: debes ingresar el concepto de la transaccion."
         NEXT FIELD concepto1
      END IF 
     AFTER INPUT      
      IF w_mae_trn.tipo_trns IS NULL THEN
         NEXT FIELD tipo_trns
      END IF
      IF w_mae_trn.f_emision IS NULL THEN
         NEXT FIELD f_emision
      END IF 
      IF w_mae_trn.documento IS NULL THEN
         NEXT FIELD documento
      END IF 
      IF w_mae_trn.concepto1 IS NULL THEN
         NEXT FIELD concepto1
      END IF 
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   # Ingresando detalle
   LET wmsg = "F4 Regreso  Esc Continuar  Ctrl-E Borra Linea  ",
             "Ctrl-I Inserta linea  Ctrl-B Calculadora"
   CALL msgup(1,wmsg,6)  
   CURRENT WINDOW IS win018a

   LET detalle = TRUE 
   WHILE detalle
    # Seteando cuentas   
    CALL SET_COUNT(tot_cta)

    # Ingresando partida 
    INPUT ARRAY v_partida WITHOUT DEFAULTS FROM s_partida.* 
     ON KEY(F4) 
      # Salida
      LET detalle = FALSE 
      EXIT INPUT
     ON KEY(F1)
      # Lista de cuentas contables 
      IF INFIELD(cuenta) THEN
         LET hilera = view_ctascon(7,6,w_mae_trn.empresa)
         IF hilera IS NOT NULL THEN
            LET v_partida[arr].cuenta = hilera[1,10]
            DISPLAY v_partida[arr].cuenta TO s_partida[scr].cuenta
         END IF 
      END IF 
     ON KEY(CONTROL-B)
      # Calculadora
      RUN "op calculator"
     ON KEY(INTERRUPT)
      # Atrapando interrupts
      ERROR "Tecla Invalida."
     BEFORE FIELD cuenta
      LET arr = ARR_CURR()
      LET scr = SCR_LINE()
     BEFORE FIELD debe    
      LET arr = ARR_CURR()
      LET scr = SCR_LINE()

      IF v_partida[arr].cuenta IS NULL THEN
         ERROR "Error: debes ingresar el numero de la cuenta."
         INITIALIZE v_partida[arr].* TO NULL 
         CLEAR s_partida[scr].*
         NEXT FIELD cuenta
      END IF 

      # Buscando si la cuenta existe
      INITIALIZE w_mae_ctb.* TO NULL
      CALL busca_ctascon(w_mae_trn.empresa,v_partida[arr].cuenta) 
      RETURNING w_mae_ctb.*,existe  
      IF NOT existe THEN
         ERROR "Error: cuenta ",v_partida[arr].cuenta CLIPPED," no existe."
         INITIALIZE v_partida[arr].* TO  NULL
         CLEAR s_partida[scr].* 
         NEXT FIELD cuenta
      END IF

      # Verificando el nivel de la cuenta
      IF (w_mae_ctb.nivel <w_mae_ncl.niveles) THEN
         ERROR "Error: unicamente pueden ingresarse cuentas de ultimo nivel (",
               w_mae_ncl.niveles USING "<<",")."
         INITIALIZE v_partida[arr].* TO  NULL
         CLEAR s_partida[scr].* 
         NEXT FIELD cuenta
      END IF

      # Verificando que cuenta haya sido creada o habilitada para la 
      # empresa de la transaccion
      IF (w_mae_ctb.empcta!=w_mae_trn.empresa) THEN
         ERROR "Atencion: cuenta no ha sido creada o habilitada para ",
               "esta empresa."
         INITIALIZE v_partida[arr].* TO  NULL
         CLEAR s_partida[scr].* 
         NEXT FIELD cuenta
      END IF 

      LET v_partida[arr].nombre = w_mae_ctb.abreviado
      DISPLAY v_partida[arr].nombre TO s_partida[scr].nombre 
     AFTER FIELD debe
      LET arr = ARR_CURR()
      LET scr = SCR_LINE()
      IF v_partida[arr].debe IS NULL OR
         (v_partida[arr].debe  >99999999.99) OR  
         (v_partida[arr].haber <0) THEN 
         LET v_partida[arr].debe = 0
         DISPLAY v_partida[arr].debe TO s_partida[scr].debe
      END IF 

      # Totalizando cuentas
      CALL in018_total()
     AFTER FIELD haber 
      LET arr = ARR_CURR()
      LET scr = SCR_LINE()
      IF (v_partida[arr].debe >0) THEN
         LET v_partida[arr].haber = 0
         DISPLAY v_partida[arr].haber TO s_partida[scr].haber 
      ELSE
         IF v_partida[arr].haber IS NULL OR 
            (v_partida[arr].haber > 99999999.99) OR  
            (v_partida[arr].haber <0) THEN 
            LET v_partida[arr].haber = 0
            DISPLAY v_partida[arr].haber TO s_partida[scr].haber 
         END IF 
      END IF 

      IF (v_partida[arr].debe =0) AND (v_partida[arr].haber=0) THEN
         ERROR "Error: columnas del debe y haber no pueden ser ceros."
         NEXT FIELD debe 
      END IF 
     
      # Totalizando cuentas
      CALL in018_total()
     BEFORE DELETE 
      LET arr = ARR_CURR()
      LET scr = SCR_LINE()
      INITIALIZE v_partida[arr].* TO NULL
      DISPLAY v_partida[arr].* TO s_partida[scr].*
     AFTER INSERT,DELETE,ROW 
      LET tot_cta = ARR_COUNT()
      # Totalizando cuentas
      CALL in018_total()
     AFTER INPUT
      LET tot_cta = ARR_COUNT()
      # Totalizando cuentas
      CALL in018_total()
    END INPUT
    IF NOT detalle THEN
       EXIT WHILE
    END IF 

    # Verificando si hay lineas ingresadas
    IF (in018_nulos()<=0) THEN 
       CALL msgerr(" Atencion: debes ingresar la partida de la transaccion.")
       CONTINUE WHILE
    END IF

    # Encontrando lineas incompletas
    LET ln = in018_lincomp()
    IF (ln>0) THEN
       LET wmsg = " Atencion: linea ",ln USING "<<"," incompleta."
       CALL msgerr(wmsg)
       CONTINUE WHILE 
    END IF 

    # Verificando cuadre de debe y haber
    IF (totdebe != tothaber) OR 
       (totdebe=0 AND tothaber =0) THEN 
       ERROR "Atencion: la suma del HABER debe ser ", 
             "igual a la del DEBE, sin ser ceros." 
       CONTINUE WHILE
    END IF

    # Opciones de menu
    OPEN WINDOW win018b AT 22,13
     WITH 2 ROWS,52 COLUMNS 
     ATTRIBUTE(BORDER,MESSAGE LINE LAST,PROMPT LINE LAST)
     MENU " Que desea hacer"
      COMMAND "Grabar" " Graba la transaccion ingresada."
       MESSAGE " Emitiendo transaccion ..." ATTRIBUTE(BOLD,BLINK)

       # Iniciando transaccion
       CALL begin_work()

       # Grabando documento
       CALL in018_grabatrn()

       # Cerrando transaccion
       CALL commit_work()

       MESSAGE ""
       CALL msgerr(" Atencion: emision de la transaccion concluida.")
       LET detalle   = FALSE 
       LET anclaje   = TRUE
       EXIT MENU 
      COMMAND "Modificar" " Permite modificar los datos de la transaccion."
       LET retroceso = TRUE
       EXIT MENU 
      COMMAND "Cancelar" " Cancela la emision de la transaccion ingresada."
       CALL msgerr(" Atencion: emision de la transaccion cancelada.")
       LET detalle   = FALSE 
       LET retroceso = FALSE
       EXIT MENU
      COMMAND KEY(F4)
       LET retroceso = FALSE
       EXIT MENU
      COMMAND KEY("!")
       CALL subshell()
     END MENU 
    CLOSE WINDOW win018b  
   END WHILE
   CLOSE WINDOW wmsgup6
 END WHILE 
 CLOSE WINDOW wmsgup5 
 CLOSE WINDOW win018a
END FUNCTION

{ Subrutina para totaliza las filas del voucher }

FUNCTION in018_total()
 DEFINE i SMALLINT

 LET totdebe  = 0
 LET tothaber = 0
 FOR i = 1 TO tot_cta
  IF (v_partida[i].debe IS NOT NULL) THEN
     LET totdebe = (totdebe+v_partida[i].debe)
  END IF
  IF (v_partida[i].haber IS NOT NULL) THEN
     LET tothaber= (tothaber+v_partida[i].haber)
  END IF 
 END FOR
 DISPLAY BY NAME totdebe,tothaber ATTRIBUTE(REVERSE)  
END FUNCTION

{ Subrutina para verificar si la partida esta en blanco }

FUNCTION in018_nulos()
 DEFINE i,j SMALLINT

 LET j = 0
 FOR i = 1 TO 200  
  IF v_partida[i].cuenta IS NULL THEN
     EXIT FOR     
  END IF  
  LET j = (j+1) 
 END FOR
 RETURN j
END FUNCTION

{ Subrutina para encontrar lineas incompletas }

FUNCTION in018_lincomp()  
 DEFINE i,flag_err SMALLINT

 LET flag_err = 0 
 FOR i = 1 TO 200 
  IF v_partida[i].cuenta IS NOT NULL THEN
     
     IF v_partida[i].debe IS NULL THEN 
        LET flag_err = i
        EXIT FOR 
     END IF 
     IF v_partida[i].haber IS NULL THEN 
        LET flag_err = i
        EXIT FOR 
     END IF 
  END IF
 END FOR
 RETURN flag_err
END FUNCTION

{ Subrutina para grabar las transacciones }

FUNCTION in018_grabatrn()
 DEFINE w_mae_agr   RECORD LIKE cb_agructas.*,
        w_tmp_emp   RECORD LIKE empresas.*,
        w_monto     LIKE cb_mtransac.monto, 
        i,exis      SMALLINT,
        w_operacion CHAR(1)

 # Grabando encabezado
 LET w_mae_trn.link     = 0 
 LET w_mae_trn.monto    = totdebe
 LET w_mae_trn.usuario  = ARG_VAL(1)
 LET w_mae_trn.terminal = ARG_VAL(2)
 LET w_mae_trn.linkapl  = 0
 LET w_mae_trn.division = "UN"
 LET w_mae_trn.faciva   = "N"

 #SET LOCK MODE TO WAIT
 INSERT INTO cb_mtransac
 VALUES (w_mae_trn.*)
 LET w_mae_trn.link = SQLCA.SQLERRD[2]

 # Grabando detalle (Cuentas Detalle)
 FOR i = 1 TO tot_cta
  IF v_partida[i].cuenta IS NULL THEN
     CONTINUE FOR
  END IF

  # Verificando tipo de operacion
  IF (v_partida[i].debe>0) THEN
     LET w_operacion = "D"
  ELSE
     LET w_operacion = "H"
  END IF 

  # Creando cuentas de detalle
  #SET LOCK MODE TO WAIT
  INSERT INTO cb_dtransac
  VALUES (w_mae_trn.link,i,v_partida[i].cuenta,v_partida[i].concepto,
          v_partida[i].debe,v_partida[i].haber,w_operacion)

  # Creando cuentas padre 
  CALL cuentas_padre(w_mae_trn.link,i,w_mae_trn.empresa,w_mae_trn.f_emision,
                     v_partida[i].cuenta,w_mae_emp.nomenclatura,w_operacion,
                     v_partida[i].debe,v_partida[i].haber,"N","N")
 END FOR 

 # Actualizando cuentas contables en cascada
 FOR i = 1 TO tot_cta
  IF v_partida[i].cuenta IS NULL THEN
     CONTINUE FOR
  END IF

  # Verificando tipo de operacion
  IF (v_partida[i].debe>0) THEN
     LET w_operacion = "D"
     LET w_monto     = v_partida[i].debe  
  ELSE
     LET w_operacion = "H"
     LET w_monto     = v_partida[i].haber 
  END IF 

  # Actualizando cuentas en cascada
  CALL saldo_cascada(w_mae_trn.empresa,v_partida[i].cuenta,
                     w_mae_emp.nomenclatura,w_operacion,w_monto,"M","")

  # Actualiza cuentas del consolidado
  IF (w_mae_emp.consolida="N") THEN 
     # Selecciona cuentas del consolidado
     DECLARE c_ctascons CURSOR FOR
      SELECT b.*
      FROM  cb_agructas b
      WHERE (b.empresa    = w_mae_trn.empresa)
       AND  (b.cta_normal = v_partida[i].cuenta)
     FOREACH c_ctascons INTO w_mae_agr.*

      # Busca empresa de cuenta agrupada
      INITIALIZE w_tmp_emp.* TO NULL
      CALL busca_empresa(w_mae_agr.emp_consol)
      RETURNING w_tmp_emp.*,existe

      # Verifica si es empresa de consolidacion para actualizar
      IF (w_tmp_emp.consolida="S") THEN 
         # Actualizando cuentas en cascada
         CALL saldo_cascada(w_mae_agr.emp_consol,w_mae_agr.cta_consol,
                            w_tmp_emp.nomenclatura,w_operacion,w_monto,
                            "M","")
      END IF
     END FOREACH 
  ELSE
     # Selecciona cuentas  para consolidar
     DECLARE c_ctasconsi CURSOR FOR
      SELECT b.*
      FROM  cb_agructas b
      WHERE (b.emp_consol = w_mae_trn.empresa)
       AND  (b.cta_consol = v_partida[i].cuenta)
     FOREACH c_ctasconsi INTO w_mae_agr.*

      # Busca empresa de cuenta agrupada
      INITIALIZE w_tmp_emp.* TO NULL
      CALL busca_empresa(w_mae_agr.empresa)
      RETURNING w_tmp_emp.*,existe

      # Verifica si es empresa de consolidacion para actualizar
      IF (w_tmp_emp.consolida="S") THEN 
         # Actualizando cuentas en cascada
         CALL saldo_cascada(w_mae_agr.empresa,w_mae_agr.cta_normal,
                            w_tmp_emp.nomenclatura,w_operacion,w_monto,
                            "M","")
      END IF
     END FOREACH 
  END IF
 END FOR
END FUNCTION

{ Subrutina para consultar las transacciones }

FUNCTION in018_consulta() 
 DEFINE qrytext,qrypart CHAR(700),
        loop            SMALLINT,
        hilera          CHAR(1) 

 # Abriendo la ventana para la consulta
 OPEN WINDOW win018a AT 3,1 WITH FORM "ctbfin18a"
  ATTRIBUTE (FORM LINE 1,PROMPT LINE LAST,MESSAGE LINE LAST)
  CALL messages(1,0,3,78,"Consulta de Transacciones")
  CALL draw_border(2,1,4,78,"")
  CALL draw_border(6,1,13,78,"")

  DISPLAY " P A R T I D A " AT 11,3  ATTRIBUTE(REVERSE) 
  DISPLAY "Cuenta"          AT 13,3  ATTRIBUTE(BOLD)
  DISPLAY "Abreviatura"     AT 13,20 ATTRIBUTE(BOLD)
  DISPLAY "Debe"            AT 13,60 ATTRIBUTE(BOLD) 
  DISPLAY "Haber"           AT 13,73 ATTRIBUTE(BOLD) 

  CALL msgup(1,"Ingrese el criterio de seleccion y presione ESC. F4 Regreso",5)
  CURRENT WINDOW IS win018a 

  # Buscando datos
  LET loop = TRUE 
  WHILE loop 
   INITIALIZE qrytext,qrypart TO NULL
   LET int_flag = FALSE
   LET totdebe  = 0
   LET tothaber = 0
   CLEAR FORM
   DISPLAY BY NAME totdebe,tothaber 

   # Construyendo la consulta
   CONSTRUCT BY NAME qrytext ON t.aplicacion,
                                t.empresa,
                                t.tipo_trns,
                                t.documento,
                                t.usuario,
                                t.f_emision,
                                t.terminal,
                                t.concepto1,
                                t.concepto2,
                                t.concepto3 
    ON KEY(F4)
     # Salida
     LET loop = FALSE
     EXIT CONSTRUCT 
    ON KEY(F1)
     # Lista de aplicaciones
     IF INFIELD(aplicacion) THEN
        LET hilera = view_aplicas()
     END IF 
     # Lista de empresas
     IF INFIELD(empresa) THEN
        LET hilera = view_empresas()
     END IF  
     # Lista de tipos de transacciones
     IF INFIELD(tipo_trns) THEN
        LET hilera = view_tiptrns()
     END IF  
    ON KEY(INTERRUPT)
     # Interrupts
     ERROR "Tecla invalida."
   END CONSTRUCT 
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   # Creando la busqueda
   LET qrypart = "SELECT * FROM cb_mtransac t ",
   	         "WHERE ",qrytext CLIPPED,
	         " ORDER BY t.f_emision,t.documento"

   # Preparando la busqueda
   PREPARE estin018 FROM qrypart
   DECLARE c_transa SCROLL CURSOR FOR estin018  
   OPEN c_transa 
   FETCH FIRST c_transa INTO w_mae_trn.*
    IF (status = NOTFOUND) THEN
       # Notificar que no existen datos
       CALL msgbusqueda()
    ELSE
       # Desplegando datos
       CALL in018_datos()

       # Fetchando transacciones 
       OPEN WINDOW win018e AT 22,5 
        WITH 2 ROWS,70 COLUMNS ATTRIBUTE(MESSAGE LINE LAST,BORDER) 
        
        MENU " Consulta"
         COMMAND "Siguiente" 
         " Visualiza la siguiente transaccion en la lista."
         FETCH NEXT c_transa INTO w_mae_trn.*
          IF (status = NOTFOUND) THEN
             DISPLAY "" AT 1,1 
             MESSAGE " No existen mas transacciones siguientes en lista."
             ATTRIBUTE(BOLD)
             CALL detiene_screen(1)
	     MESSAGE ""
             FETCH LAST c_transa INTO w_mae_trn.*
          END IF 

          # Desplegando datos
          CURRENT WINDOW IS win018a
          CALL in018_datos()
          CURRENT WINDOW IS win018e
         COMMAND "Anterior" 
         " Visualiza la transaccion anterior en la lista."
         FETCH PREVIOUS c_transa INTO w_mae_trn.*
          IF (status = NOTFOUND) THEN
             DISPLAY "" AT 1,1 
             MESSAGE " No existen mas transacciones anteriores en lista."
             ATTRIBUTE(BOLD)
             CALL detiene_screen(1)
             MESSAGE ""
             FETCH FIRST c_transa INTO w_mae_trn.*
          END IF
          # Desplegando datos
          CURRENT WINDOW IS win018a
          CALL in018_datos()
          CURRENT WINDOW IS win018e
         COMMAND "Primero"
         " Visualiza la primer transaccion en la lista."
         FETCH FIRST c_transa INTO w_mae_trn.*
          # Desplegando datos
          CURRENT WINDOW IS win018a
          CALL in018_datos()
          CURRENT WINDOW IS win018e
         COMMAND "Ultimo"
         " Visualiza la ultima transaccion en la lista."
         FETCH LAST c_transa INTO w_mae_trn.*
          # Desplegando datos
          CURRENT WINDOW IS win018a
          CALL in018_datos()
          CURRENT WINDOW IS win018e
         COMMAND "Dpartida"
         " Visualiza en detalle la partida de la transaccion en la lista."
         CURRENT WINDOW IS win018a

         # Desplegando partida
         CALL SET_COUNT(tot_cta)
         DISPLAY ARRAY v_partida TO s_partida.*
          ON KEY(INTERRUPT)
           # Atrapando interrupts
           ERROR "Tecla Invalida."
          ON KEY(F4)
           # Salida
           EXIT DISPLAY
         END DISPLAY

         CURRENT WINDOW IS win018e
         COMMAND "Imprimir"
         " Imprime la transaccion visualizada en pantalla."
         LET impress = menu_lp(10,57)
         IF impress IS NULL THEN
            CONTINUE MENU 
         END IF

         
         MESSAGE " Imprimiendo transaccion ..." ATTRIBUTE(BOLD,BLINK)
         CALL in018_imptransac()
         MESSAGE ""
         
         COMMAND "Regreso"
         " Regresa a la pantalla de busqueda."
         EXIT MENU
         COMMAND KEY ("!")
         CALL subshell()
         COMMAND KEY(F4)
         EXIT MENU
        END MENU
       CLOSE WINDOW win018e
    END IF     
   CLOSE c_transa
  END WHILE
 CLOSE WINDOW wmsgup5
 CLOSE WINDOW win018a
END FUNCTION 

{ Subrutina para desplegar los datos de las transacciones } 

FUNCTION in018_datos()
 DEFINE i SMALLINT

 # Obteniendo datos de la transaccion
 INITIALIZE w_mae_emp.*,w_trn_ctb.* TO NULL

 CALL busca_empresa(w_mae_trn.empresa)
 RETURNING w_mae_emp.*,existe
 CALL busca_tiptrncb(w_mae_trn.tipo_trns) 
 RETURNING w_trn_ctb.*,existe
 CALL busca_aplicacion(w_mae_trn.aplicacion)
 RETURNING w_mae_apl.*,existe 

 # Inicializando vector 
 CALL in018_inivec()

 # Cargando datos de la partida
 CALL in018_cpartida()

 # Desplegando datos de la transaccion
 DISPLAY BY NAME w_mae_trn.empresa,w_mae_trn.tipo_trns,w_mae_trn.documento,
                 w_mae_trn.f_emision,w_mae_trn.f_operacion,w_mae_trn.aplicacion,
                 w_mae_trn.concepto1,w_mae_trn.concepto2,w_mae_trn.concepto3,
                 w_mae_trn.usuario,w_mae_trn.terminal

 DISPLAY w_mae_emp.nombre,w_trn_ctb.descripcion
 TO      nempresa,ntransac

 DISPLAY w_mae_apl.nombre TO norigen ATTRIBUTE(BOLD)

 # Desplegando datos de la partida
 FOR i = 1 TO 2 
  DISPLAY v_partida[i].* TO s_partida[i].*
 END FOR

 # Totalizando cuentas
 CALL in018_total()
END FUNCTION 

{ Subrutina para imprimir la transaccion visualizada en pantalla }

FUNCTION in018_imptransac()
 # Creando el archivo de impresion
 LET filename = ARG_VAL(3) CLIPPED,"/ctbin018.spl"

 # Obteniendo tipos de fonts
 CALL fonts(impress) RETURNING fnt.*

 # Iniciando el reporte
 START REPORT in018_imptrn TO filename
  # Llenando el reporte
  OUTPUT TO REPORT in018_imptrn()
 # Finalizando el reporte
 FINISH REPORT in018_imptrn

 # Imprimiendo transaccion
 CALL to_printer(filename,impress,"132") 
END FUNCTION

{ Subrutina para cargar la partida de la transaccion }

FUNCTION in018_cpartida()
 DEFINE w_correl,exis SMALLINT
 
 # Seleccionando datos 
 DECLARE cpar CURSOR FOR
 SELECT d.correlativo,
        d.cuenta,
        d.concepto,
        d.debe,
        d.haber
  FROM  cb_dtransac d
  WHERE (d.link = w_mae_trn.link)
  ORDER BY 1

  LET tot_cta = 1
  FOREACH cpar INTO w_correl,
                    v_partida[tot_cta].cuenta,
                    v_partida[tot_cta].concepto,
                    v_partida[tot_cta].debe,
                    v_partida[tot_cta].haber

   # Buscando descripcion de la cuenta
   INITIALIZE w_mae_ctb.* TO NULL
   CALL busca_ctascon(w_mae_trn.empresa,v_partida[tot_cta].cuenta) 
   RETURNING w_mae_ctb.*,exis
   LET v_partida[tot_cta].nombre = w_mae_ctb.abreviado

   # Verificando rebalse
   LET tot_cta = (tot_cta+1)
   IF (tot_cta>200) THEN
      EXIT FOREACH
   END IF 
  END FOREACH
  LET tot_cta = (tot_cta-1)
END FUNCTION

{ Subrutina para inicializar datos principales }

FUNCTION in018_inidat(c)
 DEFINE c,i smallint

 # Inicializando datos
 LET w_mae_trn.f_operacion = CURRENT
 LET w_mae_trn.aplicacion  = "CB"
 LET totdebe               = 0
 LET tothaber              = 0
 LET tot_cta               = 0

 # Inicializando vector 
 CALL in018_inivec()

 # Limpiando vector
 IF (c>1) THEN
    FOR i = 1 TO 2   
     DISPLAY v_partida[i].* TO s_partida[i].*
    END FOR 
 END IF 

 # Verificando limpieza de pantalla
 CASE (c) 
  WHEN 1 CLEAR FORM 
  WHEN 2 CLEAR tipo_trns,documento,f_emision,
               concepto1,concepto2,concepto3,ntransac
  WHEN 3 CLEAR documento,f_emision,
               concepto1,concepto2,concepto3
 END CASE

 # Buscando aplicacion/origen
 CALL busca_aplicacion(w_mae_trn.aplicacion)
 RETURNING w_mae_apl.*,existe 

 # Desplegando datos iniciales
 DISPLAY w_mae_apl.nombre TO norigen ATTRIBUTE(BOLD)
 DISPLAY BY NAME w_mae_trn.aplicacion,w_mae_trn.f_operacion,totdebe,tothaber
END FUNCTION

{ Subrutina para inicializar el vector de trabajo }

FUNCTION in018_inivec()
 DEFINE i SMALLINT

 # Inicializando vector
 FOR i = 1 TO 200
  INITIALIZE v_partida[i].* TO NULL
 END FOR 
END FUNCTION

{ Subrutina para imprimir una transaccion seleccionada }

REPORT in018_imptrn()
 DEFINE w_mae_nca  RECORD LIKE cb_mcuentas.*,
        i,exis     SMALLINT,
        linea      CHAR(132),
        w_concepto CHAR(65)

 OUTPUT PAGE LENGTH 88
        TOP MARGIN 2
        BOTTOM MARGIN 3

 FORMAT
  ON EVERY ROW
   # Definiendo una linea
   LET linea = "-------------------------------------------------------------",
               "-------------------------------------------------------------"
               
   # Inicializando
   PRINT fnt.ini CLIPPED,fnt.t88 CLIPPED,fnt.cmp CLIPPED 

   # Imprimiendo encabezado
   PRINT COLUMN   1,fnt.twd CLIPPED,
                    w_mae_trn.empresa CLIPPED," ",w_mae_emp.nombre CLIPPED,
                    fnt.fwd CLIPPED
   PRINT COLUMN   1,"# Documento            : ",
                    fnt.tbl CLIPPED,
                    w_mae_trn.documento USING "<<<<<<<<&",
                    fnt.fbl CLIPPED,
         COLUMN  86,"Fecha Emision  : ",w_mae_trn.f_emision USING "dd/mmm/yyyy"
   PRINT COLUMN   1,"Tipo de Transaccion    : ",
                    w_mae_trn.tipo_trns USING "<<"," ",
                    w_trn_ctb.descripcion CLIPPED,
         COLUMN  82,"Fecha Operacion: ",w_mae_trn.f_operacion 
   PRINT COLUMN   1,"C O N C E P T O        : ",
                    w_mae_trn.concepto1,w_mae_trn.concepto2
   PRINT COLUMN   1,w_mae_trn.concepto3

   # Imprimiendo detalle
   PRINT linea 
   PRINT "# Cuenta          Descripcion                             ",
         "                             D E B E        H A B E R"
   PRINT linea 

   # Imprimiendo partida
   FOR i = 1 TO tot_cta
    IF v_partida[i].cuenta IS NULL THEN
       CONTINUE FOR
    END IF

    # Obteniendo nombre de la cuenta
    INITIALIZE w_mae_nca.* TO NULL
    CALL busca_ctascon(w_mae_trn.empresa,v_partida[i].cuenta) 
    RETURNING w_mae_nca.*,exis
    PRINT v_partida[i].cuenta                      ,2 SPACES,
          w_mae_nca.nombre                         ,2 SPACES,
          v_partida[i].debe  USING "###,###,##&.&&",3 SPACES,
          v_partida[i].haber USING "###,###,##&.&&"
    PRINT COLUMN 18,v_partida[i].concepto
   END FOR 

   # Totalizando
   PRINT linea 
   PRINT "Total --> ",70 SPACES,
         totdebe            USING "###,###,##&.&&",3 SPACES,
         tothaber           USING "###,###,##&.&&"
   SKIP 1 LINES
   PRINT "Usuario emitio   : ",w_mae_trn.usuario,65 SPACES,
         "Usuario imprimio : ",ARG_VAL(1) CLIPPED
END REPORT
