{ 
Programo : Mynor Ramirez 
Fecha    : Mayo 2013             
Objetivo : Programa de variables globales facturacion distribuidora 
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname  = "ctbing001"
DEFINE w_mae_pro   RECORD LIKE ctb_mcuentas.*,    
       w_mae_emp   RECORD LIKE glb_empresas.*,
       w_mae_trn   RECORD LIKE ctb_tipostrn.*,
       w_mae_tra   RECORD LIKE ctb_mtransac.*,
       v_partida   DYNAMIC ARRAY OF RECORD
        numcta     LIKE ctb_dtransac.numcta,
        nomcta     LIKE ctb_mcuentas.nomcta,     
        totdeb     LIKE ctb_dtransac.totdeb,
        tothab     LIKE ctb_dtransac.tothab,
        concpt     LIKE ctb_dtransac.concep,
        rellen     CHAR(1) 
       END RECORD, 
       gcodemp     LIKE ctb_mtransac.codemp,
       totaldeb    DEC(14,2),
       totalhab    DEC(14,2),
       reimpresion SMALLINT,
       totlin      SMALLINT,
       b           ui.ComboBox,
       cba         ui.ComboBox,
       w           ui.Window,
       f           ui.Form
END GLOBALS
