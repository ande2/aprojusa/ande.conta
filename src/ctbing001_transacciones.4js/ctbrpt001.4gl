{
Programo : Mynor Ramirez 
Objetivo : Impresion de facturacion distribuidora 
Fecha    : Mayo 2013          
}

-- Definicion de variables globales  

GLOBALS "ctbglb001.4gl" 
DEFINE w_mae_usr    RECORD LIKE glb_usuarios.*,
       fnt          RECORD
        cmp         CHAR(12),
        nrm         CHAR(12),
        tbl         CHAR(12),
        fbl,t88     CHAR(12),
        t66,p12     CHAR(12),
        p10,srp     CHAR(12),
        twd         CHAR(12),
        fwd         CHAR(12),
        tda,fda     CHAR(12),
        ini         CHAR(12)
       END RECorD,
       existe       SMALLINT, 
       filename     CHAR(90),
       pipeline     CHAR(90),
       i            INT

-- Subrutina para imprimir un movimiento de producto

FUNCTION ctbrpt001_facturacion(reimpresion)
 DEFINE reimpresion SMALLINT 

 -- Preparando impresion
 CALL fgl_winmessage(" Atencion","Presione [ENTER] para imprimir el documento.","information")

 -- Definiendo archivo de impresion
 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/ctbrpt001.spl"

 -- Asignando valores
 LET pipeline = "local"   

 -- Seleccionando fonts para impresora epson
 CALL librut001_fontsprn(pipeline,"epson") 
 RETURNING fnt.* 

 -- Iniciando reporte
 START REPORT ctbrpt001_printfac TO filename 
  OUTPUT TO REPORT ctbrpt001_printfac(1,reimpresion)
 FINISH REPORT ctbrpt001_printfac 

 -- Imprimiendo el reporte
 CALL librut001_enviareporte(filename,
                             pipeline,
                             "")
END FUNCTION 

-- Subrutinar para generar la impresion de la facturacion 

REPORT ctbrpt001_printfac(numero,reimpresion) 
 DEFINE imp1        RECORD LIKE fac_formaimp.*,
        xnomcaj     VARCHAR(40), 
        xnomven     VARCHAR(40), 
        exis,i,col  SMALLINT, 
        nl          SMALLINT, 
        reimpresion SMALLINT,
        xnumdoc     CHAR(40), 
        xdsitem1    CHAR(64), 
        xdsitem2    CHAR(64), 
        xdsitem3    CHAR(64), 
        linea       CHAR(93),
        wcanletras  STRING,
        moneda      CHAR(3), 
        totlen      INT,
        numero      INT 

  OUTPUT LEFT   MARGIN 2
         PAGE   LENGTH 44
         TOP    MARGIN 0 
         BOTTOM MARGIN 2

 FORMAT 
  BEFORE GROUP OF numero 
   -- Inicializando impresora
   PRINT fnt.ini CLIPPED,fnt.t88 CLIPPED,fnt.nrm CLIPPED
   LET totlen = 95
   LET linea  = "--------------------------------------------------",
                "-------------------------------------------"

   -- Imprimiendo encabezado
   LET wcanletras = librut001_numtolet(w_mae_tra.totdoc) 

   -- Obteniendo datos del tipo de documento de facturacion
   INITIALIZE imp1.* TO NULL 
   SELECT a.* 
    INTO  imp1.* 
    FROM  fac_formaimp a
    WHERE a.lnktdc = w_mae_tra.lnktdc

   -- Obteniendo nombre del vendedor
   INITIALIZE xnomven TO NULL 
   SELECT a.nomven 
    INTO  xnomven 
    FROM  glb_vendedrs a
    WHERE a.codven = w_mae_tra.codven

   -- Obteniendo nombre del cajero
   INITIALIZE xnomcaj TO NULL 
   SELECT a.nomusr 
    INTO  xnomcaj 
    FROM  glb_usuarios a
    WHERE a.userid = w_mae_tra.userid 

   PRINT COLUMN  1,fnt.tbl CLIPPED,fnt.twd CLIPPED,"B", 
         COLUMN 18,fnt.tda CLIPPED,
                   imp1.top001 CLIPPED,
                   fnt.fda CLIPPED,fnt.fwd CLIPPED,fnt.fbl CLIPPED,
                   fnt.p12 CLIPPED

   IF LENGTH(imp1.top002) >0 THEN 
      LET col = librut001_centrado(imp1.top002,totlen) 
      PRINT COLUMN col,imp1.top002 CLIPPED 
   END IF 
   IF LENGTH(imp1.top003) >0 THEN 
      LET col = librut001_centrado(imp1.top003,totlen) 
      PRINT COLUMN col,imp1.top003 CLIPPED 
   END IF 
   IF LENGTH(imp1.top004) >0 THEN 
      LET col = librut001_centrado(imp1.top004,totlen) 
      PRINT COLUMN col,imp1.top004 CLIPPED 
   END IF 
   IF LENGTH(imp1.top005) >0 THEN 
      LET col = librut001_centrado(imp1.top005,totlen) 
      PRINT COLUMN col,imp1.top005 CLIPPED 
   END IF 

   -- Numero del documento 
   LET xnumdoc = w_tip_doc.title1 CLIPPED,
                 " SERIE ",w_mae_tra.nserie CLIPPED," ",
                 w_mae_tra.numdoc USING "<<<<<<<<<<"

   PRINT COLUMN   1,"Cliente:   ",fnt.tbl CLIPPED,
                                  w_mae_tra.nomcli CLIPPED,
                                  fnt.fbl CLIPPED, 
         COLUMN  65,fnt.tbl CLIPPED,xnumdoc CLIPPED,fnt.fbl CLIPPED 
   PRINT COLUMN   1,"Direccion: ",fnt.tbl CLIPPED,
                                  w_mae_tra.dircli[1,50] CLIPPED,
                                  fnt.fbl CLIPPED, 
         COLUMN  65,"Fecha:     ",fnt.tbl CLIPPED,
                                  w_mae_tra.fecemi,
                                  fnt.fbl CLIPPED
   PRINT COLUMN   1,"Vendedor:  ",fnt.tbl CLIPPED,
                                  xnomven CLIPPED,
                                  fnt.fbl CLIPPED

   -- Verificando forma de pago
   CASE (w_mae_tra.credit)
    WHEN 1 PRINT COLUMN   1,fnt.tbl CLIPPED,
                            "CREDITO ",xdiacre USING "<<&"," Dia[s]",
                            fnt.fbl CLIPPED
    WHEN 0 PRINT COLUMN   1,fnt.tbl CLIPPED,
                            "CONTADO",
                            fnt.fbl CLIPPED
   END CASE 
   SKIP 1 LINES 
    
   -- Detalle de Productos
   PRINT "Cant.  Descripcion                                                          Precio",
         "       Total" 
   PRINT linea 
 
  ON EVERY ROW
   -- Imprimiendo detalle 

   LET nl = 10 
   LET moneda = "L. " 
   FOR i = 1 TO totlin 
    IF v_partida[i].tothab IS NULL THEN
       CONTINUE FOR
    END IF 

    -- Asignando descripcion
    LET xdsitem1 = v_partida[i].dsitem[1,64]
    LET xdsitem2 = v_partida[i].dsitem[65,128]
    LET xdsitem3 = v_partida[i].dsitem[129,200]

    PRINT v_partida[i].tothab  USING "#,##&"     ,2 SPACES, 
          xdsitem1                                ,1 SPACES, 
          v_partida[i].totdeb  USING "###,##&.&&",2 SPACES, 
          v_partida[i].totpro  USING "###,##&.&&" 
    LET nl = nl-1

    -- Imoprimiendo descripcion 
    IF LENGTH(xdsitem2) >0 THEN 
       PRINT COLUMN 8,xdsitem2 CLIPPED
       LET nl = nl-1
    END IF 
    IF LENGTH(xdsitem3) >0 THEN 
       PRINT COLUMN 8,xdsitem3 CLIPPED
       LET nl = nl-1
    END IF 
   END FOR   

   -- Saltando linea restantes
   SKIP nl LINES 

  ON LAST ROW
   -- Totales 
   PRINT linea 
   PRINT COLUMN   1,
         COLUMN  69,"SUB-TOTAL: ",fnt.tbl CLIPPED,
                                  moneda, 
                                  w_mae_tra.subtot   USING "#,###,##&.&&",
                                  fnt.fbl CLIPPED 
   PRINT COLUMN   1,imp1.foot01 CLIPPED,
         COLUMN  69,"IMPUESTO:  ",fnt.tbl CLIPPED,
                                  moneda, 
                                  w_mae_tra.totiva   USING "#,###,##&.&&",
                                  fnt.fbl CLIPPED 
   PRINT COLUMN   1,imp1.foot02 CLIPPED,
         COLUMN  69,"TOTAL:     ",fnt.tbl CLIPPED,
                                  moneda, 
                                  w_mae_tra.totdoc   USING "#,###,##&.&&",
                                  fnt.fbl CLIPPED 

   -- Pie de pagina 
   SKIP 1 LINES 
   IF LENGTH(imp1.foot03) >0 THEN 
      LET col = librut001_centrado(imp1.foot03,totlen) 
      PRINT COLUMN col,imp1.foot03 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot04) >0 THEN 
      LET col = librut001_centrado(imp1.foot04,totlen) 
      PRINT COLUMN col,imp1.foot04 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot05) >0 THEN 
      LET col = librut001_centrado(imp1.foot05,totlen) 
      PRINT COLUMN col,imp1.foot05 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot06) >0 THEN 
      LET col = librut001_centrado(imp1.foot06,totlen) 
      PRINT COLUMN col,imp1.foot06 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot07) >0 THEN 
      LET col = librut001_centrado(imp1.foot07,totlen) 
      PRINT COLUMN col,imp1.foot07 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot08) >0 THEN 
      LET col = librut001_centrado(imp1.foot08,totlen) 
      PRINT COLUMN col,imp1.foot08 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot09) >0 THEN 
      LET col = librut001_centrado(imp1.foot09,totlen) 
      PRINT COLUMN col,imp1.foot09 CLIPPED 
   END IF 
   IF LENGTH(imp1.foot10) >0 THEN 
      LET col = librut001_centrado(imp1.foot10,totlen) 
      PRINT COLUMN col,imp1.foot10 CLIPPED 
   END IF 

   -- Firma de aceptado del cliente
   SKIP 3 LINES 
   PRINT COLUMN  40,fnt.cmp CLIPPED,wcanletras CLIPPED,fnt.nrm CLIPPED
   PRINT COLUMN   1,"__________________________________"
   PRINT COLUMN   1,"        Aceptado Cliente          " 
END REPORT 
