{
Programo : Mynor Ramirez
Fecha    : Abril 2015  
Objetivo : Programa de transacciones contables 
}

-- Definicion de variables globales

GLOBALS "ctbglb001.4gl"
CONSTANT programa         = "ctbing001" 
CONSTANT CantidadMaxima   = 99999999.99 
CONSTANT PermiteRepetidos = TRUE 
DEFINE username          LIKE glb_permxusr.userid,
       wpais             VARCHAR(255), 
       conteo            SMALLINT,
       regreso           SMALLINT,
       existe            SMALLINT,
       msg,filename      STRING, 
       qrytext           STRING 

-- Subrutina principal

MAIN
 -- Atrapando interrupts 
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarTransaccionesCtb")

 -- Verificando parametro para agregar programa a un container
 IF ARG_VAL(1)=1 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("transaccionesctb")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ 

 -- Obteniendo datos del pais 
 CALL librut003_parametros(1,0)
 RETURNING existe,wpais

 -- Obteniendo usuario del sistema
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de opciones
 CALL ctbing001_menu()
END MAIN

-- Subutina para el menu de transacciones

FUNCTION ctbing001_menu()
 DEFINE regreso    SMALLINT, 
        titulo     STRING 

 -- Abriendo la ventana del mantenimiento
 OPEN WINDOW wing005a AT 5,2  
  WITH FORM "ctbing001a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL librut001_header("ctbing001",wpais,1)

  -- Inicializando datos 
  CALL ctbing001_inival(1)

  -- Cargando combobox de empresas
  LET gcodemp = librut003_DCbxEmpresaEnUsoXUsuario()
  IF gcodemp IS NULL THEN
     CALL fgl_winmessage(
     "Atencion","Usuario sin empresa asignada, VERIFICA.","stop")
     CLOSE WINDOW wing005a 
     RETURN
  END IF

  -- Verificando parametros 
  IF (ARG_VAL(2)>0) THEN 
   -- Consulta de transaccion 
   CALL ctbqbx001_transacciones(1,ARG_VAL(2),ARG_VAL(3))
  ELSE 
   -- Menu de opciones
   MENU " Opciones" 
    BEFORE MENU
     -- Verificando accesos a opciones
     -- Consultar
     IF NOT seclib001_accesos(progname,4,username) THEN
        HIDE OPTION "Consultar"
     END IF
     -- Ingresar
     IF NOT seclib001_accesos(progname,1,username) THEN
        HIDE OPTION "Ingresar"
     END IF
     -- Modificar 
     IF NOT seclib001_accesos(progname,2,username) THEN
        HIDE OPTION "Modificar" 
     END IF 
     -- Anular        
     IF NOT seclib001_accesos(progname,5,username) THEN
        HIDE OPTION "Anular" 
     END IF 
     -- Eliminar 
     IF NOT seclib001_accesos(progname,3,username) THEN
        HIDE OPTION "Eliminar"
     END IF 
    ON ACTION consultar 
     CALL ctbqbx001_transacciones(1,0,"")
    ON ACTION ingresar  
     CALL ctbing001_transacciones(1) 
    ON ACTION modificar 
     CALL ctbqbx001_transacciones(2,0,"")
    ON ACTION eliminar 
     CALL ctbqbx001_transacciones(3,0,"")
    ON ACTION anular  
     CALL ctbqbx001_transacciones(4,0,"")
    ON ACTION salir 
     EXIT MENU
    COMMAND KEY(F4,CONTROL-E)
     EXIT MENU  
   END MENU
  END IF 
 CLOSE WINDOW wing005a
END FUNCTION

-- Subrutina para el ingreso de los datos del encabezado de la transaccion 

FUNCTION ctbing001_transacciones(operacion)
 DEFINE userauth          LIKE glb_permxusr.userid,
        xsaltotcre        DEC(14,2), 
        xdocto            CHAR(20), 
        retroceso         SMALLINT,
        operacion,acceso  SMALLINT,
        loop,existe,i     SMALLINT,
        yregreso          SMALLINT,
        xparam            INTEGER, 
        qrystr            STRING 

 -- Inicio del loop
 IF (operacion=1) THEN
    CALL ctbing001_EstadoMenu(4,"")
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF 

 LET loop  = TRUE
 WHILE loop   
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos 
     CALL ctbing001_inival(1) 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_tra.tiptrn,
                w_mae_tra.fecemi, 
                w_mae_tra.concep 
                WITHOUT DEFAULTS
    ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=FALSE)

   ON ACTION cancel 
    -- Salida 
    IF (operacion=1) THEN  -- Ingreso 
     IF INFIELD(tiptrn) THEN
        LET loop = FALSE
        EXIT INPUT
     ELSE
        -- Inicializando 
        CALL Dialog.SetFieldActive("tiptrn",TRUE)
        CALL ctbing001_inival(1)
        NEXT FIELD tiptrn 
     END IF 
    ELSE                 -- Modifidacion 
     LET loop = FALSE 
     EXIT INPUT 
    END IF 

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   BEFORE INPUT                 
    -- Verificando retroceso 
    IF retroceso THEN
       CALL Dialog.SetFieldActive("tiptrn",FALSE)

       -- Si es modificacion de poliza
       IF (operacion=2) THEN
          CALL Dialog.SetFieldActive("fecemi",FALSE)
       END IF 
    ELSE
       CALL Dialog.SetFieldActive("tiptrn",TRUE)
    END IF 

   ON CHANGE tiptrn 
    -- Obteniendo datos del tipo de transaccion 
    INITIALIZE w_mae_trn.* TO NULL 
    CALL librut003_BTipoTransaccionContable(w_mae_tra.tiptrn)
    RETURNING w_mae_trn.*,existe 

   AFTER FIELD tiptrn
    -- Verificando tipo transaccion 
    IF w_mae_tra.tiptrn IS NULL THEN
       ERROR "Error: tipo de transaccion invalida, VERIFICA."
       NEXT FIELD tiptrn 
    END IF 

    -- Obteniendo datos del tipo de transaccion 
    INITIALIZE w_mae_trn.* TO NULL 
    CALL librut003_BTipoTransaccionContable(w_mae_tra.tiptrn)
    RETURNING w_mae_trn.*,existe 

   ON CHANGE fecemi 
    -- Obteniendo correlativo de transaccion 
    CALL ctbing001_NumeroDocumento() 

   AFTER FIELD fecemi
    -- Verificando fecha 
    IF w_mae_tra.fecemi IS NULL OR 
       w_mae_tra.fecemi >TODAY THEN
       ERROR "Error: fecha invalida, VERIFICA." 
       LET w_mae_tra.fecemi = NULL
       DISPLAY BY NAME w_mae_tra.fecemi 
       NEXT FIELD fecemi 
    END IF 

    -- Obteniendo correlativo de transaccion 
    CALL ctbing001_NumeroDocumento() 
  
   AFTER FIELD concep
    -- Verificando concepto
    IF (LENGTH(w_mae_tra.concep)<=0) THEN 
       ERROR "Error: concepto invalido, VERIFICA." 
       NEXT FIELD concep 
    END IF 

   AFTER INPUT
    -- Verificando nulos
    IF w_mae_tra.tiptrn IS NULL THEN
       NEXT FIELD tiptrn
    END IF 
    IF w_mae_tra.fecemi IS NULL THEN
       NEXT FIELD fecemi
    END IF
    IF w_mae_tra.numdoc IS NULL THEN
       NEXT FIELD numdoc
    END IF
    IF w_mae_tra.concep IS NULL THEN
       NEXT FIELD concep
    END IF 
  END INPUT 
  IF NOT loop THEN
     EXIT WHILE 
  END IF 

  -- Ingresando detalle de cuentas de la transaccion 
  LET retroceso = ctbing001_DetalleTransaccion(operacion)

  -- Verificando operacion 
  IF (operacion=2) AND 
     (retroceso=FALSE) THEN
     LET loop = FALSE
  END IF 
 END WHILE

 -- Inicializando datos 
 IF (operacion=1) THEN 
    CALL ctbing001_EstadoMenu(0,"")
    CALL ctbing001_inival(1) 
 END IF 
END FUNCTION

-- Subutina para el ingreso del detalle de cuentas de la transaccion 

FUNCTION ctbing001_DetalleTransaccion(operacion)
 DEFINE w_mae_cta  RECORD LIKE ctb_mcuentas.*,
        operacion  SMALLINT, 
        loop,scr   SMALLINT,
        opc,arr,i  SMALLINT, 
        linea      SMALLINT, 
        retroceso  SMALLINT, 
        acceso     SMALLINT, 
        hayexi     SMALLINT, 
        lastkey    INTEGER,  
        repetido   SMALLINT

 -- Iniciando el loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop

  -- Ingresando productos
  INPUT ARRAY v_partida WITHOUT DEFAULTS FROM s_partida.*
   ATTRIBUTE(COUNT=totlin,INSERT ROW=FALSE,ACCEPT=FALSE,CANCEL=FALSE,
             UNBUFFERED=TRUE,FIELD ORDER FORM)

   ON ACTION accept 
    -- Aceptar
    EXIT INPUT

   ON ACTION cancel 
    -- Cancelar
    LET retroceso = TRUE
    LET loop      = FALSE
    EXIT INPUT

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   ON ACTION listacuenta  
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Seleccionado lista de cuentas contables
    CALL librut002_ListaBusqueda(
                            "Consulta de Cuentas Contables ",
                            "# Cuenta",
                            "Nombre de la Cuenta",
                            "",
                            "numcta",
                            "nomcta",
                            "",
                            "ctb_mcuentas",
                            "codemp = "||w_mae_tra.codemp||" AND tipcta='D'",
                            1)
    RETURNING v_partida[arr].numcta,v_partida[arr].nomcta,regreso 
    IF regreso THEN
       NEXT FIELD numcta
    ELSE 
       -- Asignando descripcion
       DISPLAY v_partida[arr].numcta TO s_partida[scr].numcta 
       DISPLAY v_partida[arr].nomcta TO s_partida[scr].nomcta 
    END IF  
   
   BEFORE INPUT 
    CALL DIALOG.setActionHidden("append",TRUE)

    -- Totalizando cuentas
    CALL ctbing001_TotalCuentas() 

   BEFORE ROW
    LET totlin = ARR_COUNT()

   BEFORE FIELD numcta 
    -- Habilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",TRUE)

   AFTER FIELD numcta 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) THEN 
       NEXT FIELD numcta 
    END IF
 
   BEFORE FIELD totdeb 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    IF v_partida[arr].numcta IS NULL THEN 
      -- Seleccionado lista de cuentas contables
      CALL librut002_ListaBusqueda(
                            "Consulta de Cuentas Contables ",
                            "# Cuenta",
                            "Nombre de la Cuenta",
                            "",
                            "numcta",
                            "nomcta",
                            "",
                            "ctb_mcuentas",
                            "codemp = "||w_mae_tra.codemp||" AND tipcta='D'",
                            1)
     RETURNING v_partida[arr].numcta,v_partida[arr].nomcta,regreso 
     IF regreso THEN
        NEXT FIELD numcta
     ELSE 
        -- Asignando descripcion
        DISPLAY v_partida[arr].numcta TO s_partida[scr].numcta 
        DISPLAY v_partida[arr].nomcta TO s_partida[scr].nomcta 
     END IF  
    END IF  

    -- Verificando numero de cuenta 
    IF (LENGTH(v_partida[arr].numcta)<=0) THEN 
       CALL fgl_winmessage(
       "Atencion",
       "Numero de cuenta invalida, VERIFICA.",
       "stop")
       INITIALIZE v_partida[arr].numcta THRU v_partida[arr].concpt TO NULL
       CLEAR s_partida[scr].numcta,s_partida[scr].nomcta, 
             s_partida[scr].totdeb,s_partida[scr].tothab, 
             s_partida[scr].concpt 
       NEXT FIELD numcta 
    END IF 

    -- Verificando si la cuenta existe   
    INITIALIZE w_mae_cta.* TO NULL
    CALL librut003_BCuentaContable(w_mae_tra.codemp,v_partida[arr].numcta)
    RETURNING w_mae_cta.*,existe
    IF NOT existe THEN
       CALL fgl_winmessage(
       "Atencion",
       "Numero de cuenta no registrada, VERIFICA.",
       "stop")
       INITIALIZE v_partida[arr].numcta THRU v_partida[arr].concpt TO NULL
       CLEAR s_partida[scr].numcta,s_partida[scr].nomcta, 
             s_partida[scr].totdeb,s_partida[scr].tothab, 
             s_partida[scr].concpt
       NEXT FIELD numcta 
    END IF 

    -- Asignando descripcion
    LET v_partida[arr].nomcta = w_mae_cta.nomcta 
    DISPLAY v_partida[arr].nomcta TO s_partida[scr].nomcta 

    -- Verificando que cuenta se de detalle 
    IF w_mae_cta.tipcta="M" THEN 
       CALL fgl_winmessage(
       "Atencion",
       "Numero de cuenta no es de detalle, VERIFICA.",
       "stop")
       INITIALIZE v_partida[arr].numcta THRU v_partida[arr].concpt TO NULL
       CLEAR s_partida[scr].numcta,s_partida[scr].nomcta, 
             s_partida[scr].totdeb,s_partida[scr].tothab, 
             s_partida[scr].concpt
       NEXT FIELD numcta
    END IF 

    -- Verificando productos repetidos
    IF NOT PermiteRepetidos THEN 
     LET repetido = FALSE
     FOR i = 1 TO totlin
      IF v_partida[i].numcta IS NULL THEN
         CONTINUE FOR
      END IF

      -- Verificando producto
      IF (i!=arr) THEN 
       IF (v_partida[i].numcta = v_partida[arr].numcta) THEN
          LET repetido = TRUE
          EXIT FOR
       END IF 
      END IF
     END FOR

     -- Si hay repetido
     IF repetido THEN
        LET msg = "Numero de cuenta ya registrada en el detalle. \n"||
                  "Linea (",i USING "<<<",") VERIFICA." 
        CALL fgl_winmessage("Atencion",msg,"stop")
        INITIALIZE v_partida[arr].numcta THRU v_partida[arr].concpt TO NULL
       CLEAR s_partida[scr].numcta,s_partida[scr].nomcta, 
             s_partida[scr].totdeb,s_partida[scr].tothab, 
             s_partida[scr].concpt
        NEXT FIELD numcta 
     END IF
    END IF 

    -- Deshabilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",FALSE)

    -- Totalizando cuentas
    CALL ctbing001_TotalCuentas() 

   ON CHANGE totdeb 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Totalizando cuentas
    CALL ctbing001_TotalCuentas() 

   AFTER FIELD totdeb
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD totdeb 
    END IF

    -- Verificando total del debe
    IF v_partida[arr].totdeb IS NULL OR
       (v_partida[arr].totdeb <0) OR
       (v_partida[arr].totdeb >CantidadMaxima) THEN 
       LET v_partida[arr].totdeb = 0 
    END IF 
    
    -- Totalizando cuentas
    CALL ctbing001_TotalCuentas() 

   ON CHANGE tothab 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Totalizando cuentas
    CALL ctbing001_TotalCuentas() 

   AFTER FIELD tothab 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD tothab 
    END IF

    -- Verificando total del haber 
    IF v_partida[arr].tothab IS NULL OR
       (v_partida[arr].tothab <0) OR
       (v_partida[arr].tothab >CantidadMaxima) THEN 
       LET v_partida[arr].tothab = 0 
    END IF 

    -- Verificando totales de debe y haber = 0
    IF (v_partida[arr].totdeb=0) AND (v_partida[arr].tothab=0) THEN
       LET msg = "Tota del debe y haber no pueden ser ambos ceros, VERIFICA."
       CALL fgl_winmessage("Atencion",msg,"stop")
       NEXT FIELD totdeb 
    END IF

    -- Verificando totales de debe y haber >0 
    IF (v_partida[arr].totdeb>0) AND (v_partida[arr].tothab>0) THEN
     LET msg="Total del debe y haber no pueden ser ambos mayores a cero, VERIFICA."
     CALL fgl_winmessage("Atencion",msg,"stop")
     NEXT FIELD totdeb 
    END IF

    -- Totalizando cuentas
    CALL ctbing001_TotalCuentas() 

   AFTER FIELD concpt 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD concpt 
    END IF

   AFTER ROW,INSERT  
    LET totlin = ARR_COUNT()

    -- Totalizando cuentas
    CALL ctbing001_TotalCuentas() 

   ON ACTION DELETE 
    -- Eliminando linea seleccionada
    LET arr = ARR_CURR() 
    CALL v_partida.deleteElement(arr)

    -- Totalizando cuentas
    CALL ctbing001_TotalCuentas() 
    NEXT FIELD numcta 

   ON ROW CHANGE 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Totalizando cuentas
    CALL ctbing001_TotalCuentas() 

   AFTER INPUT 
    LET totlin = ARR_COUNT()

    -- Totalizando cuentas
    CALL ctbing001_TotalCuentas() 
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Recalculando totales
  FOR i = 1 TO totlin
   IF v_partida[i].numcta IS NULL THEN
      CONTINUE FOR
   END IF 

   -- Calculando cantidad total en unidades totales
   --CALL ctbing001_cantidadtotal(i,i,0)
  END FOR

  -- Desplegando datos 
  DISPLAY ARRAY v_partida TO s_partida.*
   BEFORE DISPLAY 
    EXIT DISPLAY 
  END DISPLAY

  -- Totalizando cuentas
  CALL ctbing001_TotalCuentas() 

  -- Verificando lineas incompletas 
  LET linea = ctbing001_incompletas()
  IF (linea>0) THEN
     LET msg = " Linea ("||linea||") incompleta. \n VERIFICA."
     CALL fgl_winmessage(
     "Atencion",msg,"stop")
     CONTINUE WHILE
  END IF 

  -- Verificando cuadre de debe y haber 
  IF (totaldeb!=totalhab) OR
     ((totaldeb+totalhab)=0) THEN
     CALL fgl_winmessage(
     "Atencion",
     "Total del debe y haber invalidos. "||
     "\n VERIFICA que ambos totales sean iguales sin ser ceros.",
     "stop")
     CONTINUE WHILE
  END IF

  -- Menu de opciones
  LET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  -- Verificando opcion 
  CASE (opc)
   WHEN 0 -- Cancelando
    LET loop      = FALSE
    LET retroceso = FALSE
   WHEN 1 -- Grabando
    LET loop      = FALSE
    LET retroceso = FALSE

    -- Grabando 
    CALL ctbing001_grabar(operacion)
   WHEN 2 -- Modificando
    LET loop = TRUE 
  END CASE
 END WHILE

 RETURN retroceso
END FUNCTION

-- Subrutina para totalizar el detalle de cuentas

FUNCTION ctbing001_TotalCuentas()
 DEFINE i SMALLINT

 -- Totalizando
 LET totaldeb = 0
 LET totalhab = 0
 FOR i = 1 TO totlin  
  IF v_partida[i].totdeb IS NOT NULL THEN
     LET totaldeb = (totaldeb+v_partida[i].totdeb)
  END IF
  IF (v_partida[i].tothab IS NOT NULL) THEN
     LET totalhab = (totalhab+v_partida[i].tothab)
  END IF
 END FOR
 DISPLAY BY NAME totaldeb,totalhab,totlin 
END FUNCTION

-- Subrutina para verificar si hay cuentas incompletas

FUNCTION ctbing001_incompletas()
 DEFINE i,linea SMALLINT 

 LET linea = 0 
 FOR i = 1 TO 100 
  IF v_partida[i].numcta IS NULL THEN
     CONTINUE FOR
  END IF

  -- Verificando lineas
  IF v_partida[i].nomcta IS NULL OR
     v_partida[i].totdeb IS NULL OR
     v_partida[i].tothab IS NULL THEN 
     LET linea = i 
     EXIT FOR 
  END IF 
 END FOR

 RETURN linea 
END FUNCTION

-- Subrutina para grabar la transaccion 

FUNCTION ctbing001_grabar(operacion)
 DEFINE i,correl   SMALLINT,
        operacion  SMALLINT,
        xtipope    LIKE ctb_dtransac.tipope,
        xtotval    LIKE ctb_mtransac.totdoc 

 -- Grabando transaccion
 ERROR " Registrando Transaccion ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK
   IF (operacion=1) THEN -- grabar 
    -- 1. Grababdo encabezado de la transaccion 
    -- Asignando datos
    LET w_mae_tra.lnktra = 0 
    LET w_mae_tra.totdoc = totaldeb
    LET w_mae_tra.codapl = "CTB" 
    LET w_mae_tra.lnkapl = 0 
    LET w_mae_tra.coddiv = 1  

    -- Obteniendo correlativo de transaccion 
    CALL ctbing001_NumeroDocumento() 

    -- Grabando
    INSERT INTO ctb_mtransac
    VALUES (w_mae_tra.*)
    LET w_mae_tra.lnktra = SQLCA.SQLERRD[2] 
   ELSE
    -- Actualizando transaccion 
    UPDATE ctb_mtransac
    SET    ctb_mtransac.concep = w_mae_tra.concep 
    WHERE  ctb_mtransac.lnktra = w_mae_tra.lnktra 
   END IF 

   -- Eliminando detalle antes de grabar
   DELETE FROM ctb_dtransac 
   WHERE ctb_dtransac.lnktra = w_mae_tra.lnktra
   DELETE FROM ctb_ctransac 
   WHERE ctb_ctransac.lnktra = w_mae_tra.lnktra

   -- 2. Grabando detalle de cuentas de la transaccion 
   LET correl = 0
   FOR i = 1 TO totlin 
     IF v_partida[i].numcta IS NULL OR 
        v_partida[i].totdeb IS NULL OR  
        v_partida[i].tothab IS NULL THEN 
        CONTINUE FOR 
     END IF 
     LET correl = (correl+1) 
 
     -- Verificando debe y haber
     IF (v_partida[i].totdeb>0) THEN
        LET xtipope = "D"
        LET xtotval = v_partida[i].totdeb 
     ELSE
        LET xtipope = "H"
        LET xtotval = v_partida[i].tothab 
     END IF 

     -- Grabando detalle de cuentas 
     INSERT INTO ctb_dtransac 
     VALUES (w_mae_tra.lnktra    , -- link del encabezado
             correl              , -- correlativo 
             v_partida[i].numcta , -- numero de cuenta 
             v_partida[i].concpt , -- concepto de la linea 
             v_partida[i].totdeb , -- total debe 
             v_partida[i].tothab , -- total haber 
             xtipope)            

     -- Creando cuentas padre
     CALL ctbing001_CuentasPadre(
             w_mae_tra.lnktra, 
             i,
             w_mae_tra.codemp,
             w_mae_tra.fecemi,      
             v_partida[i].numcta, 
             w_mae_emp.tipnom, 
             xtipope,             
             xtotval, 
             v_partida[i].totdeb,
             v_partida[i].tothab, 
             "N",
             "N")
   END FOR 

 -- Finalizando la transaccion
 COMMIT WORK
 
 -- Verificando operacion
 CASE (operacion)
  WHEN 1 LET msg = "Numero de Transaccion ["||w_mae_tra.lnktra||"] Registrada."
  WHEN 2 LET msg = "Numero de Transaccion ["||w_mae_tra.lnktra||"] Modificada."
 END CASE

 CALL fgl_winmessage("Atencion",msg,"information")
 ERROR "" 
END FUNCTION 

-- Subrutina para inicializar las variables de trabajo 

FUNCTION ctbing001_inival(i)
 DEFINE i       SMALLINT,
        wfecha  VARCHAR(80) 
 
 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1  
   INITIALIZE w_mae_tra.* TO NULL 
   LET w_mae_tra.codemp = gcodemp 
   CLEAR FORM 
 END CASE 

 -- Inicializando datos
 LET w_mae_tra.estado = 1          
 LET w_mae_tra.fecemi = TODAY 
 LET w_mae_tra.userid = username 
 LET w_mae_tra.fecsis = CURRENT 
 LET w_mae_tra.horsis = CURRENT HOUR TO SECOND
 LET totlin           = 0 
 LET totaldeb         = 0
 LET totalhab         = 0

 -- Obteniendo datos de la empresa 
 INITIALIZE w_mae_emp.* TO NULL 
 CALL librut003_BEmpresa(w_mae_tra.codemp)
 RETURNING w_mae_emp.*,existe 

 -- Llenando combo de tipos de transacciones
 CALL librut003_CbxTiposTransaccionesContables()

 -- Inicializando vectores de datos
 CALL ctbing001_inivec() 

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_tra.codemp,w_mae_tra.fecemi,
                 w_mae_tra.userid,w_mae_tra.fecsis,
                 w_mae_tra.horsis
 DISPLAY BY NAME totaldeb,totalhab,totlin 

 -- Desplegando fecha 
 LET wfecha = librut001_formatofecha(TODAY,1,wpais) CLIPPED
 CALL librut001_dpelement("labelz",wfecha)
END FUNCTION

-- Subrutina para inicializar y limpiar los vectores de trabajo 

FUNCTION ctbing001_inivec()
 DEFINE i SMALLINT

 -- Inicializando vectores
 CALL v_partida.clear()

 LET totlin = 0 
 FOR i = 1 TO 8
  -- Limpiando vector       
  CLEAR s_partida[i].*
 END FOR 
END FUNCTION 

-- Subrutina para extraer cuentas maestras en cascada de una cuenta detalle y
-- grabarlas en los archivos de transacciones 

FUNCTION ctbing001_CuentasPadre(w_link,w_orden,wcodemp,w_fecha,w_cuenta,
                       w_nomenclatura,w_opera,xtotval,w_debe,w_haber,w_conciliado,
                       w_opernueva)

 DEFINE w_tip_nom       RECORD LIKE ctb_tiposnom.*, 
        w_cuentas       RECORD LIKE ctb_mcuentas.*,
        v_nivel         ARRAY[10] OF RECORD   
         digits          CHAR(20),
         datos           LIKE ctb_tiposnom.format, 
         suma            SMALLINT
        END RECORD,
        w_link          LIKE ctb_mtransac.lnktra,  
        w_orden         LIKE ctb_ctransac.correl,     
        w_fecha         LIKE ctb_mtransac.fecemi,       
        wcodemp         LIKE ctb_mcuentas.codemp,  
        w_cuenta        LIKE ctb_mcuentas.numcta,  
        w_nomenclatura  LIKE ctb_tiposnom.tipnom,       
        w_ncuenta       LIKE ctb_mcuentas.numcta,
        hil             LIKE ctb_mcuentas.numcta,
        w_ctapad        LIKE ctb_mcuentas.numcta,   
        w_formato       LIKE ctb_tiposnom.format,   
        w_opera         LIKE ctb_dtransac.tipope,    
        w_debe,w_haber  LIKE ctb_mtransac.totdoc, 
        xtotval         LIKE ctb_mtransac.totdoc, 
        wsalact         LIKE ctb_mcuentas.salact, 
        wcargos         LIKE ctb_mcuentas.cargos, 
        wcaracu         LIKE ctb_mcuentas.caracu, 
        wabonos         LIKE ctb_mcuentas.abonos, 
        waboacu         LIKE ctb_mcuentas.aboacu, 
        w_conciliado    CHAR(1),
        w_opernueva     CHAR(1),
        w_msg           CHAR(80),
        i,j,pos,ini,acu SMALLINT,
        dig,tn          SMALLINT,
        w_lengft        SMALLINT,
        existe,nm       SMALLINT

 -- Obteniendo datos del tipo de nomenclatura 
 INITIALIZE w_tip_nom.* TO NULL
 CALL librut003_BTiposNomenclaturaCtb(w_nomenclatura)
 RETURNING w_tip_nom.*,existe

 -- Quitando separadores del formato 
 LET w_formato = librut001_QuitarSeparadores(w_tip_nom.format,w_tip_nom.sepdor)
 LET w_lengft  = LENGTH(w_tip_nom.format)

 -- Inicializando variables
 LET w_ncuenta = NULL
 LET ini       = 1
 LET pos       = 0

 -- Inicializando vector de niveles
 FOR i = 1 TO 10
  INITIALIZE v_nivel[i].* TO NULL
  LET v_nivel[i].suma = 0
 END FOR 

 -- Asignando niveles 
 LET tn = 0
 IF w_tip_nom.dig001 >0 THEN LET v_nivel[1].digits  = w_tip_nom.dig001 LET tn = tn+1 END IF
 IF w_tip_nom.dig002 >0 THEN LET v_nivel[2].digits  = w_tip_nom.dig002 LET tn = tn+1 END IF
 IF w_tip_nom.dig003 >0 THEN LET v_nivel[3].digits  = w_tip_nom.dig003 LET tn = tn+1 END IF
 IF w_tip_nom.dig004 >0 THEN LET v_nivel[4].digits  = w_tip_nom.dig004 LET tn = tn+1 END IF
 IF w_tip_nom.dig005 >0 THEN LET v_nivel[5].digits  = w_tip_nom.dig005 LET tn = tn+1 END IF
 IF w_tip_nom.dig006 >0 THEN LET v_nivel[6].digits  = w_tip_nom.dig006 LET tn = tn+1 END IF
 IF w_tip_nom.dig007 >0 THEN LET v_nivel[7].digits  = w_tip_nom.dig007 LET tn = tn+1 END IF
 IF w_tip_nom.dig008 >0 THEN LET v_nivel[8].digits  = w_tip_nom.dig008 LET tn = tn+1 END IF
 IF w_tip_nom.dig009 >0 THEN LET v_nivel[9].digits  = w_tip_nom.dig009 LET tn = tn+1 END IF
 IF w_tip_nom.dig010 >0 THEN LET v_nivel[10].digits = w_tip_nom.dig010 LET tn = tn+1 END IF

 -- Eliminando separadores de la cuenta
 FOR i = 1 TO LENGTH(w_cuenta)
  IF (w_cuenta[i,i]=w_tip_nom.sepdor) THEN
     CONTINUE FOR
  END IF 
  LET w_ncuenta = w_ncuenta CLIPPED,w_cuenta[i,i]
 END FOR 
 LET w_cuenta   = w_ncuenta CLIPPED

 -- Escaneando niveles de la cuenta 
 FOR i = 1 TO tn 
  IF v_nivel[i].digits IS NULL THEN
     CONTINUE FOR
  END IF 

  LET pos = (pos+v_nivel[i].digits)
  LET hil = w_cuenta[ini,pos]

  LET acu = 0
  FOR j = 1 TO LENGTH(hil)
   LET dig = hil[j,j]
   LET acu = (acu+dig)
  END FOR 
  LET v_nivel[i].datos = w_cuenta[ini,pos]

  IF (acu>0) THEN
     LET v_nivel[i].suma = 1
  ELSE
     LET v_nivel[i].suma = 0
  END IF 

  LET ini = (pos+1)
  LET pos = (ini-1)
 END FOR

 -- Hallando cuentas en cascada por niveles
 LET nm = 0  
 FOR i =  1 TO tn             

  -- Formando cuenta
  LET w_ctapad = NULL
  FOR j = 1 TO i
   LET w_ctapad = w_ctapad CLIPPED,v_nivel[j].datos CLIPPED
  END FOR

  {IF (v_nivel[j].suma = 0) THEN
     CONTINUE FOR
  END IF}

  -- Formateando la cuenta padre al formato del tipo de nomenclatura
  LET w_ctapad = librut001_FormatoCuentaContable(
                         w_ctapad,
                         w_formato,
                         (LENGTH(w_tip_nom.format)-(w_tip_nom.numniv-w_tip_nom.nivsep)),
                         "0")
  LET w_ctapad = librut001_PonerSeparadores(w_ctapad,w_tip_nom.sepdor)
  LET nm       = (nm+1)

  -- Obteniendo datos de la cuenta   
  INITIALIZE w_cuentas.* TO NULL
  CALL librut003_BCuentaContable(wcodemp,w_ctapad) 
  RETURNING w_cuentas.*,existe

  -- Inicializando datos
  LET wcargos = 0 
  LET wcaracu = 0 
  LET wabonos = 0 
  LET waboacu = 0 

  -- Actualiza cuentas normales
  CASE (w_opera)
   WHEN "D" -- Debe

    -- Verificando tipo de saldo
    CASE w_cuentas.tipsal    
     WHEN "D" -- Deudor 
      LET wsalact = xtotval 
      LET wcargos = xtotval
      LET wcaracu = xtotval  
     WHEN "A" -- Acreedor 
      LET wsalact = (xtotval*(-1)) 
      LET wcargos = xtotval
      LET wcaracu = xtotval  
    END CASE

   WHEN "H" -- Haber 

    -- Verificando tipo de saldo
    CASE w_cuentas.tipsal     
     WHEN "D" -- Deudor 
      LET wsalact = (xtotval*(-1)) 
      LET wabonos = xtotval
      LET waboacu = xtotval  
 
     WHEN "A" -- Acreedor 
      LET wsalact = xtotval 
      LET wabonos = xtotval
      LET waboacu = xtotval  
    END CASE 
  END CASE 

  -- Creando linea de transaccion por cuenta
  INSERT INTO ctb_ctransac
  VALUES (w_link,
          w_orden,
          nm,
          wcodemp,
          w_fecha,
          w_ctapad,
          w_debe,
          w_haber,
          w_opera,
          w_conciliado,
          w_opernueva,
          w_cuentas.tipsal,
          wsalact,
          wcargos,
          wcaracu,
          wabonos,
          waboacu)

   -- Actualizando cuentas en cascada
   CALL ctbing001_SaldoCuentasCascada(wcodemp,                    
                                      w_ctapad,                   
                                      w_opera,  
                                      xtotval) 
 END FOR
END FUNCTION

-- Subrutina para actualizar los saldos de una cuenta 

FUNCTION ctbing001_SaldoCuentasCascada(wcodemp,wnumcta,wtipope,wtotval) 
 DEFINE w_cuentas       RECORD LIKE ctb_mcuentas.*,
        wcodemp         LIKE ctb_mcuentas.codemp, 
        wnumcta         LIKE ctb_mcuentas.numcta,  
        wtipope         LIKE ctb_dtransac.tipope,    
        wtotval         DECIMAL(14,2),
        existe          SMALLINT

 -- Obteniendo datos de la cuenta   
 INITIALIZE w_cuentas.* TO NULL
 CALL librut003_BCuentaContable(wcodemp,wnumcta)                    
 RETURNING w_cuentas.*,existe

 -- Actualiza cuentas normales
 CASE (wtipope)
  WHEN "D" -- Debe

   -- Verificando tipo de saldo
   CASE w_cuentas.tipsal    
    WHEN "D" -- Deudor 
     UPDATE ctb_mcuentas
        SET ctb_mcuentas.salact = (ctb_mcuentas.salact+wtotval),
            ctb_mcuentas.cargos = (ctb_mcuentas.cargos+wtotval),
            ctb_mcuentas.caracu = (ctb_mcuentas.caracu+wtotval)
      WHERE ctb_mcuentas.numcta = wnumcta  
        AND ctb_mcuentas.codemp = wcodemp  

    WHEN "A" -- Acreedor 
     UPDATE ctb_mcuentas
        SET ctb_mcuentas.salact = (ctb_mcuentas.salact-wtotval),
            ctb_mcuentas.cargos = (ctb_mcuentas.cargos+wtotval),
            ctb_mcuentas.caracu = (ctb_mcuentas.caracu+wtotval)
      WHERE ctb_mcuentas.numcta = wnumcta  
        AND ctb_mcuentas.codemp = wcodemp   
   END CASE

  WHEN "H" -- Haber 

   -- Verificando tipo de saldo
   CASE w_cuentas.tipsal     
    WHEN "D" -- Deudor 
     UPDATE ctb_mcuentas
        SET ctb_mcuentas.salact = (ctb_mcuentas.salact-wtotval),
            ctb_mcuentas.abonos = (ctb_mcuentas.abonos+wtotval),
            ctb_mcuentas.aboacu = (ctb_mcuentas.aboacu+wtotval)
      WHERE ctb_mcuentas.numcta = wnumcta  
        AND ctb_mcuentas.codemp = wcodemp   

    WHEN "A" -- Acreedor 
     UPDATE ctb_mcuentas
        SET ctb_mcuentas.salact = (ctb_mcuentas.salact+wtotval),
            ctb_mcuentas.abonos = (ctb_mcuentas.abonos+wtotval),
            ctb_mcuentas.aboacu = (ctb_mcuentas.aboacu+wtotval)
      WHERE ctb_mcuentas.numcta = wnumcta 
        AND ctb_mcuentas.codemp = wcodemp  

   END CASE 
 END CASE 
END FUNCTION

-- Subrutina para encontrar el numero maximo de documento por empresa, fecha y tipo de
-- transaccion

FUNCTION ctbing001_NumeroDocumento()
 DEFINE xcorrelativo INTEGER

 -- Encontrando correlativo maximo
 SELECT NVL(COUNT(*),0)+1 
  INTO  xcorrelativo 
  FROM  ctb_mtransac a
  WHERE a.codemp                       = w_mae_tra.codemp
    AND a.tiptrn                       = w_mae_tra.tiptrn
    AND EXTEND(a.fecemi,YEAR TO MONTH) = EXTEND(w_mae_tra.fecemi,YEAR TO MONTH) 

 -- Construyendo numero
 LET w_mae_tra.numdoc = w_mae_tra.tiptrn        USING "&&&",
                        YEAR(w_mae_tra.fecemi)  USING "&&&&",
                        MONTH(w_mae_tra.fecemi) USING "&&",
                        xcorrelativo            USING "&&&&&" 
 
 DISPLAY BY NAME w_mae_tra.numdoc 
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION ctbing001_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement(
         "group1","Datos de la Transaccion - MENU")
  WHEN 1 CALL librut001_dpelement(
         "group1","Datos de la Transaccion - CONSULTAR"||msg CLIPPED) 
  WHEN 2 CALL librut001_dpelement(
         "group1","Datos de la Transaccion - MODIFICAR"||msg CLIPPED)
  WHEN 3 CALL librut001_dpelement(
         "group1","Datos de la Transaccion - ELIMINAR"||msg CLIPPED)
  WHEN 4 CALL librut001_dpelement(
         "group1","Datos de la Transaccion - INGRESAR")
 END CASE
END FUNCTION

-- Subrutina para grabar la transaccion de anulada 

FUNCTION ctbing001_Anular(xtiptrn)                
 DEFINE i,correl   SMALLINT,
        xtipope    LIKE ctb_dtransac.tipope,
        xtotval    LIKE ctb_mtransac.totdoc, 
        xtmptot    LIKE ctb_mtransac.totdoc, 
        xtiptrn    LIKE ctb_mtransac.tiptrn,
        msg        STRING 

 LET msg = "Desea Anular la Transaccion"
 IF librut001_yesornot("Confirmacion",msg,"Si","No","question") THEN

  -- Grabando transaccion
  ERROR " Anulando Transaccion ..." ATTRIBUTE(CYAN)

  -- Iniciando la transaccion
  BEGIN WORK
   -- 1. Grabando encabezado de la transaccion 
   -- Asignando datos
   LET w_mae_tra.tiptrn = xtiptrn 
   LET w_mae_tra.totdoc = totaldeb
   LET w_mae_tra.codapl = "CTB" 
   LET w_mae_tra.fecemi = TODAY 
   LET w_mae_tra.userid = username 
   LET w_mae_tra.fecsis = CURRENT 
   LET w_mae_tra.horsis = CURRENT HOUR TO SECOND
   LET w_mae_tra.lnkapl = w_mae_tra.lnktra 
   LET w_mae_tra.coddiv = 1  
   LET w_mae_tra.lnktra = 0 
   LET w_mae_tra.estado = 0 

   -- Obteniendo correlativo de transaccion 
   CALL ctbing001_NumeroDocumento() 

   -- Actualizando transaccion original
   UPDATE ctb_mtransac
   SET    ctb_mtransac.estado = 0                     
   WHERE  ctb_mtransac.lnktra = w_mae_tra.lnkapl 

   -- Grabando
   INSERT INTO ctb_mtransac
   VALUES (w_mae_tra.*)
   LET w_mae_tra.lnktra = SQLCA.SQLERRD[2] 

   -- 2. Grabando detalle de cuentas de la transaccion 
   LET correl = 0
   FOR i = 1 TO totlin 
     IF v_partida[i].numcta IS NULL OR 
        v_partida[i].totdeb IS NULL OR  
        v_partida[i].tothab IS NULL THEN 
        CONTINUE FOR 
     END IF 
     LET correl = (correl+1) 

     -- Verificando debe y haber
     LET xtmptot = v_partida[i].totdeb
     LET v_partida[i].totdeb = v_partida[i].tothab
     LET v_partida[i].tothab = xtmptot

     -- Verificando debe y haber
     IF (v_partida[i].totdeb>0) THEN
        LET xtipope = "D"
        LET xtotval = v_partida[i].totdeb 
     ELSE
        LET xtipope = "H"
        LET xtotval = v_partida[i].tothab 
     END IF 

     -- Grabando detalle de cuentas 
     INSERT INTO ctb_dtransac 
     VALUES (w_mae_tra.lnktra    , -- link del encabezado
             correl              , -- correlativo 
             v_partida[i].numcta , -- numero de cuenta 
             v_partida[i].concpt , -- concepto de la linea 
             v_partida[i].totdeb , -- total debe 
             v_partida[i].tothab , -- total haber 
             xtipope)            

     -- Creando cuentas padre
     CALL ctbing001_CuentasPadre(
             w_mae_tra.lnktra, 
             i,
             w_mae_tra.codemp,
             w_mae_tra.fecemi,      
             v_partida[i].numcta, 
             w_mae_emp.tipnom, 
             xtipope,             
             xtotval, 
             v_partida[i].totdeb,
             v_partida[i].tothab, 
             "N",
             "N")
   END FOR 

  -- Finalizando la transaccion
  COMMIT WORK
 
  -- Verificando operacion
  LET msg = "Numero de Transaccion ["||w_mae_tra.lnktra||"] Anulada."

  CALL fgl_winmessage("Atencion",msg,"information")
  ERROR "" 
  RETURN TRUE   
 ELSE
  RETURN FALSE 
 END IF 
END FUNCTION 
