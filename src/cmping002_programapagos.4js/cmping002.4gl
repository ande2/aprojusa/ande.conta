{
cmping002.4gl 
Objetivo : Programacion de pagos a socio de negocios
}

-- Definicion de variables globales

GLOBALS "cmpglb002.4gl"
CONSTANT DiasConsulta = 60 
DEFINE wpais            VARCHAR(255), 
       regreso          SMALLINT,
       existe           SMALLINT,
       totpag           INTEGER,
       inomsoc          CHAR(50), 
       msg              STRING,
       s                CHAR(1),
       tituloreporte    STRING,
       periodo          STRING, 
       empresa          STRING, 
       ifecven          DATE,
       lg               INT        

-- Subrutina principal

MAIN
 -- Atrapando interrupts 
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarProgramacionPagos")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("programacionpagos")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ 

 -- Obteniendo datos del pais 
 CALL librut003_parametros(1,0)
 RETURNING existe,wpais

 -- Obteniendo usuario del sistema
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de opciones
 CALL cmping002_menu()
END MAIN

-- Subutina para el menu de programacion de pagos 

FUNCTION cmping002_menu()
 -- Abriendo la ventana
 OPEN WINDOW wing002a AT 5,2  
  WITH FORM "cmping002a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL librut001_header("cmping002",wpais,1)

  -- Cargando combobox de empresas
  LET gcodemp = librut003_DCbxEmpresaEnUsoXUsuario()
  IF gcodemp IS NULL THEN
     CALL fgl_winmessage(
     "Atencion","Usuario sin empresa asignada, VERIFICA.","stop")
     CLOSE WINDOW wing002a 
     RETURN
  END IF

  -- Inicializando datos 
  CALL cmping002_inival()

  -- Menu de opciones
  MENU "Opciones" 
   BEFORE MENU
    -- Verificando accesos a opciones
    -- Consultar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Consultar"
    END IF
   COMMAND "Consultar" 
    "Consulta de programacion de pagos a socios de negocios."
    CALL cmping002_ProgramacionPagos()
   COMMAND "Salir"
    "Abandona el menu de programacion de pagos a socios de negocios." 
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU  
  END MENU
 CLOSE WINDOW wing002a
END FUNCTION

-- Subrutina para seleccionar la programacion de pagos 

FUNCTION cmping002_ProgramacionPagos()
 DEFINE vtotval         DEC(14,2), 
        vtotcan,res     INTEGER, 
        loop,i          SMALLINT,
        qrytxt          STRING 

 -- Desplegando estado del menu
 CALL cmping002_EstadoMenu(1)

 -- Asignando datos default
 LET ifecven = TODAY
 LET inomsoc = NULL

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Inicializando datos
  CALL v_pagos.clear() 
  LET totpag  = 1 

  -- Selecionando datos
  LET qrytxt = "SELECT a.fecven,",
                      "0,", 
                      "a.diacre,",
                      "a.fecemi,",
                      "a.nomsoc,",
                      "a.nomaut,",
                      "a.nomrub,", 
                      "a.numdoc,",
                      "a.totsal,",
                      "a.fecsis,",
                      "a.userid,",
                      "a.lnkinv,",
                      "'' ",
                "FROM  vis_movtoscompra a ",
                "WHERE a.codemp = ",gcodemp," ", 
                  "AND a.fecven <= '",ifecven,"' ",
                  "AND a.nomsoc MATCHES '"||inomsoc CLIPPED||"' ",
                  "AND a.totsal > 0 ",
                  "AND a.estado = 1 ",
                  "AND a.frmpag = 2 ", 
                "ORDER BY a.nomsoc,a.fecven,a.numdoc" 

   PREPARE c_prepare FROM qrytxt
   DECLARE cpagos CURSOR FOR c_prepare 
   LET vtotcan = 0
   LET vtotval = 0 
   FOREACH cpagos INTO v_pagos[totpag].*
    -- Calculando dias vencidos despues de la fecha de vencimiento del pago 
    LET v_pagos[totpag].diaven = (ifecven-v_pagos[totpag].fecven) 
    IF (v_pagos[totpag].diaven<0) THEN
       LET v_pagos[totpag].diaven = 0 
    END IF 

    -- Totalizando 
    LET vtotval=vtotval+v_pagos[totpag].totdoc   
    LET totpag=totpag+1 
   END FOREACH
   CLOSE cpagos 
   FREE cpagos  
   LET totpag =totpag-1 
   LET vtotcan=totpag

  -- Ingresando parametros 
  DISPLAY BY NAME ifecven,vtotcan,vtotval 
  DIALOG ATTRIBUTES(UNBUFFERED,FIELD ORDER FORM)

     -- Desplegando pagos 
     DISPLAY ARRAY v_pagos TO s_pagos.* 
      ATTRIBUTE(COUNT=totpag) 
      
      BEFORE DISPLAY 
      CALL DIALOG.setActionHidden("close",TRUE)
     END DISPLAY
 
     -- Seleccionando parametros
     INPUT BY NAME ifecven,inomsoc 
      ATTRIBUTE(WITHOUT DEFAULTS=TRUE) 
     END INPUT 

     ON ACTION excel
      -- Enviando reporte a excel 
      LET s = ASCII(9)
      CALL cmping002_ReportePagos("excel") 

     ON ACTION pdf 
      -- Enviando reporte a pdf 
      LET s = 1 SPACE
      CALL cmping002_ReportePagos("pdf") 

     ON ACTION cancel
      -- Salida 
      LET loop = FALSE 
      EXIT DIALOG 

  END DIALOG 
 END WHILE 

 -- Inicializando datos
 CALL v_pagos.clear() 

 -- Desplegando estado del menu
 CALL cmping002_EstadoMenu(0)
END FUNCTION 

-- Subrutina para inicializar las variables de trabajo 

FUNCTION cmping002_inival()
 DEFINE i       SMALLINT,
        wfecha  VARCHAR(80) 
 
 -- Desplegando empresa
 CLEAR FORM 
 DISPLAY gcodemp TO codemp 

 -- Desplegando fecha 
 LET wfecha = librut001_formatofecha(TODAY,1,wpais) CLIPPED
 CALL librut001_dpelement("labelz",wfecha)
END FUNCTION

-- Subrutina para desplegar los estados del menu 

FUNCTION cmping002_EstadoMenu(operacion)
 DEFINE operacion SMALLINT

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Programacion de Pagos - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Programacion de Pagos - CONSULTAR")
 END CASE
END FUNCTION

-- Subrutina para generar el reporte de pagos en pdf

FUNCTION cmping002_ReportePagos(pipeline) 
 DEFINE filename,pipeline STRING, 
        i                 SMALLINT 

 -- Definiendo archivo de impresion
 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/ProgramacionPagos.spl"

 -- Asignando parametros default 
 LET tituloreporte = "PROGRAMACION DE PAGOS A SOCIOS DE NEGOCIOS"
 LET periodo       = "Fecha de Vencimiento Al [ ",ifecven," ] "  
 LET lg            = 155 

 -- Obteniendo nombre de la empresa
 INITIALIZE empresa TO NULL 
 SELECT a.nomemp
  INTO  empresa
  FROM  glb_empresas a
  WHERE a.codemp = gcodemp   

 -- Iniciando reporte
 START REPORT cmping002_GeneraReporte TO filename  

  -- Llenando reporte
  FOR i = 1 TO totpag
   IF v_pagos[i].fecven IS NULL THEN
      CONTINUE FOR
   END IF 

   OUTPUT TO REPORT cmping002_GeneraReporte(pipeline,1,v_pagos[i].*) 
  END FOR 

 -- Finalizando reporte 
 FINISH REPORT cmping002_GeneraReporte 

 -- Imprimiendo el reporte
 IF pipeline = "excel" THEN
    CALL cmping002_excel(filename)
 ELSE
    -- Enviando reporte al destino seleccionado
    CALL librut001_sendreport(filename,pipeline,"","-l -p 8")
 END IF
END FUNCTION

-- Subrutina para imprimir el reporte de pagos

REPORT cmping002_GeneraReporte(pipeline,imp) 
 DEFINE imp            RECORD  
         codemp        LIKE cmp_mtransac.codemp, 
         fecven        LIKE cmp_mtransac.fecemi,
         diaven        SMALLINT, 
         diacre        LIKE cmp_mtransac.diacre, 
         fecemi        LIKE cmp_mtransac.fecemi,
         nomsoc        CHAR(50),
         nomaut        CHAR(20), 
         nomrub        CHAR(46), 
         numdoc        CHAR(20),
         totdoc        LIKE cmp_mtransac.totdoc,
         fecsis        DATE, 
         userid        CHAR(15),
         lnkinv        LIKE cmp_mtransac.lnkinv, 
         rellen        CHAR(1) 
        END RECORD, 
        linea          CHAR(155),
        pipeline       STRING,
        col            INT        

  OUTPUT LEFT   MARGIN 2 
         PAGE   LENGTH 72
         TOP    MARGIN 2
         BOTTOM MARGIN 2

 ORDER EXTERNAL BY imp.codemp,imp.nomsoc,imp.fecven,imp.numdoc 

 FORMAT 
  PAGE HEADER

   -- Definiendo linea de impresion 
   LET linea = "__________________________________________________",
               "__________________________________________________",
               "__________________________________________________",
               "____"


   -- Imprimiendo Encabezado
   LET col = librut001_centrado(empresa,lg)
   PRINT COLUMN       1,"Cuentas x Pagar",
         COLUMN     col,empresa CLIPPED, 
         COLUMN (lg-20),PAGENO USING  "Pagina: <<"

   LET col = librut001_centrado(tituloreporte,lg)
   PRINT COLUMN       1,"cmpqbe002",
         COLUMN     col,tituloreporte CLIPPED, 
         COLUMN (lg-20),"Fecha : ",TODAY USING "dd/mmm/yyyy"

   LET col = librut001_centrado(periodo,lg)
   PRINT COLUMN       1,FGL_GETENV("LOGNAME") CLIPPED,
         COLUMN     col,periodo CLIPPED, 
         COLUMN (lg-20),"Hora  : ",TIME

   PRINT linea
   PRINT "Fecha        Dias      Dias     Fecha       ",
         "Rubro de Gasto                                  ", 
         "Numero                          Valor   Fecha       Usuario"  
   PRINT "Vencimiento  Vencidos  Credito  Emision     ",
         "                                                ",
         "Documento                       Total   Registro    Registro"  
   PRINT linea

  BEFORE GROUP OF imp.codemp
   -- Verificando si reporte es a excel
   IF (pipeline="excel") THEN
      PRINT Empresa,s
      PRINT tituloreporte CLIPPED,s
      PRINT periodo CLIPPED,s
      PRINT s 
      PRINT "Fecha Vencimiento",s,
            "Dias Vencidos",s,
            "Dias Credito",s,
            "Fecha Emision",s,
            "Rubro de Gasto",s,
            "Numero Documento",s,
            "Valor Total",s,
            "Fecha Registro",s,
            "Usuario Registro",s
      PRINT s 
   END IF 

  BEFORE GROUP OF imp.nomsoc
   PRINT "Socio de Negocios: ",s,imp.nomsoc CLIPPED 

  ON EVERY ROW 
   -- Imprimiendo detalle de pagos 
   -- Verificando si reporte es a excel
   IF (pipeline="excel") THEN
     PRINT imp.fecven                              ,s,
           imp.diaven                              ,s,
           imp.diacre                              ,s,
           imp.fecemi                              ,s,
           imp.nomrub                              ,s,"'",
           imp.numdoc                              ,s,
           imp.totdoc       USING "##,###,##&.&&"  ,s,
           imp.fecsis                              ,s,
           imp.userid                                
   ELSE
     PRINT imp.fecven                              ,2 SPACES,
           imp.diaven                              ,4 SPACES,
           imp.diacre                              ,4 SPACES,
           imp.fecemi                              ,2 SPACES,
           imp.nomrub                              ,2 SPACES,
           imp.numdoc                              ,4 SPACES,
           imp.totdoc       USING "##,###,##&.&&"  ,3 SPACES, 
           imp.fecsis                              ,2 SPACES, 
           imp.userid                                
   END IF 

  AFTER GROUP OF imp.nomsoc
   -- Imprimiendo totales
   IF (pipeline="excel") THEN
      PRINT "Valor Total Socio --> ",s,s,s,s,s,s, 
                       GROUP SUM(imp.totdoc) USING "##,###,##&.&&"
   ELSE
      PRINT COLUMN   1,"Valor Total Socio --> ",
            COLUMN 117,GROUP SUM(imp.totdoc) USING "##,###,##&.&&"
   END IF 
   PRINT s

  ON LAST ROW
   -- Imprimiendo totales 
   -- Verificando si reporte es a excel
   IF (pipeline="excel") THEN
      PRINT "Valor Total Reporte   --> ",s,s,s,s,s,s,
                       SUM(imp.totdoc) USING "##,###,##&.&&"
   ELSE
      PRINT COLUMN   1,"Valor Total Reporte   --> ",
            COLUMN 117,SUM(imp.totdoc) USING "##,###,##&.&&"
   END IF 
END REPORT 

-- Subrutina para exportar el reporte a excel usando WinCOM 

FUNCTION cmping002_excel(filename) 
 DEFINE sb          base.StringBuffer
 DEFINE ch          base.channel
 DEFINE lista       base.StringTokenizer
 DEFINE result      INTEGER
 DEFINE i           INTEGER
 DEFINE xlapp       INTEGER
 DEFINE xlwb        INTEGER
 DEFINE linea       STRING 
 DEFINE lineadep    STRING 
 DEFINE filename    STRING 
 DEFINE columna     STRING 
 DEFINE separador   CHAR(1) 
 DEFINE nuevalinea  CHAR(1) 

 -- Inicializando variables                            
 LET xlapp = -1
 LET xlwb = -1

 -- Creando la instancia de Excel
 CALL ui.Interface.frontCall("WinCOM", "CreateInstance", ['Excel.Application'], [xlapp])
 CALL CheckError(xlapp, xlwb, xlapp)
   
 -- Agregando un libro al documento de excel               
 CALL ui.interface.frontCall("WinCOM", "CallMethod", [xlapp, 'WorkBooks.Add'], [xlwb])
 CALL CheckError(xlapp, xlwb, xlwb)
   
 -- Haciendo visible el libro                 
 CALL excel_set_property(xlapp, xlapp, "Visible","True")
  
 -- Llenando celdas desde el FrontCall 
 -- Construyendo celdas delimitando columnas por TAB (ASCII(9)), 
 -- y filas por CR (ASCII(10)
 -- Copia al clipboard via el Frontcall y luego instruye a excel para pegar el contenido
 -- de la celdas dentro del libro 
 LET sb = base.StringBuffer.create()

 -- Leyendo el archivo que contiene el reporte y copiandolo al clipboard 
 LET ch = base.channel.create()
 call ch.openfile(filename,"r")

 let i = 1 
 let separador  = ascii(9) 
 let nuevalinea = ascii(10)
 while true
  let linea = ch.readLine() 
  if ch.isEof() then exit while end if
  if linea.getindexof(separador,1)>0 then
     CALL sb.append(linea)
     CALL sb.append(nuevalinea)
  end if 
  let i = i+1
 end while
 call ch.close() 

 -- Pegando el contenido de las celdas en el clipboard al libro de excel
 CALL ui.Interface.FrontCall("standard","cbset",sb.toString(),result)
 CALL excel_call_method(xlapp, xlwb, 'ActiveSheet.Paste')
 CALL excel_call_method(xlapp, xlwb, 'Columns("A:Z").EntireColumn.AutoFit')

 -- Liberando informacion del clipboard    
 CALL freeMemory(xlapp, xlwb)
END FUNCTION
