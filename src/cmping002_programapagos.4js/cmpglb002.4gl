{ 
Fecha    : Julio 2014 
Programo : Mynor Ramirez 
Objetivo : Programa de variables globales
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname  = "cmping002"
DEFINE w_mae_emp   RECORD LIKE glb_empresas.*,
       v_pagos     DYNAMIC ARRAY OF RECORD  
        fecven     LIKE cmp_mtransac.fecemi,
        diaven     SMALLINT,  
        diacre     LIKE cmp_mtransac.diacre, 
        fecemi     LIKE cmp_mtransac.fecemi,
        nomsoc     CHAR(46),
        nomaut     CHAR(20), 
        nomrub     CHAR(50), 
        numdoc     CHAR(20),
        totdoc     LIKE cmp_mtransac.totdoc,
        fecsis     DATE, 
        userid     CHAR(15),
        lnkinv     LIKE cmp_mtransac.lnkinv, 
        rellen     CHAR(1) 
       END RECORD,
       username    LIKE glb_permxusr.userid,
       gcodemp     SMALLINT, 
       w           ui.Window,
       f           ui.Form
END GLOBALS
