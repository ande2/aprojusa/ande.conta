  SELECT a.fecemi,
         a.diacre,
         a.fecven,
         a.nomsoc,
         a.numdoc,
         a.totdoc,
         a.lnkinv
   FROM  vis_movtoscompra a
   WHERE (a.fecven >=today-500)
     AND (a.fecven <= today)
     AND (a.estado = 1)
     AND (a.nofase = 0)
