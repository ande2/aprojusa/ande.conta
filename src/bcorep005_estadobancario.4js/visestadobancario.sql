drop view vis_estadobancario;

create view vis_estadobancario (lnkbco,codemp,nomemp,numcta,nomcta,nombco,
                                feccob,tipmov,nommov,numdoc,nomchq,descrp,
                                totdoc,tipope,estado,nofase,userid,fecsis,
                                horsis)
as
 select x0.lnkbco,x0.codemp,x4.nomemp,x0.numcta,x1.nomcta,
        x2.nombco,x0.feccob,x0.tipmov,x3.nomabr,x0.numdoc,
        x0.nomchq,x0.descrp,x0.totdoc,x0.tipope,x0.estado,
        x0.nofase,x0.userid,x0.fecsis,x0.horsis
  from bco_mtransac x0,bco_mcuentas x1,glb_mtbancos x2,
       bco_tipomovs x3,glb_empresas x4
  where x4.codemp = x0.codemp
    and x1.numcta = x0.numcta
    and x1.codbco = x2.codbco
    and x3.tipmov = x0.tipmov;

grant select on vis_estadobancario to public;
