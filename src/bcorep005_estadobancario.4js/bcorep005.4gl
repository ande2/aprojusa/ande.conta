{ 
Programa : bcorep005.4gl                   
Programo : Mynor Ramirez 
Objetivo : Reporte de estado de cuenta bancario 
}

DATABASE erpjuridico 

-- Definicion de variables globales 

CONSTANT progname = "bcorep005" 
TYPE    datosreporte        RECORD 
         lnkbco             LIKE bco_mtransac.lnkbco,
         codemp             LIKE bco_mtransac.codemp, 
         nomemp             CHAR(50), 
         numcta             LIKE bco_mtransac.numcta,
         nomcta             LIKE bco_mcuentas.nomcta,
         nombco             CHAR(30),
         feccob             LIKE bco_mtransac.feccob,
         tipmov             LIKE bco_mtransac.tipmov,
         nommov             CHAR(6), 
         numdoc             CHAR(15),                       
         nomchq             CHAR(35), 
         descrp             CHAR(300),
         totdoc             LIKE bco_mtransac.totdoc,
         tipope             LIKE bco_mtransac.tipope,
         estado             SMALLINT, 
         nofase             SMALLINT, 
         userid             CHAR(10), 
         fecsis             LIKE bco_mtransac.fecsis, 
         horsis             LIKE bco_mtransac.horsis,
         salant             DEC(14,2) 
        END RECORD 
DEFINE  datos               RECORD 
         codemp             LIKE bco_mtransac.codemp, 
         numcta             LIKE bco_mtransac.numcta,
         tipmov             LIKE bco_mtransac.tipmov,
         fecini             DATE,
         fecfin             DATE,
         tipope             LIKE bco_mtransac.tipope 
        END RECORD
DEFINE  p                   RECORD 
         length             SMALLINT,
         topmg              SMALLINT, 
         botmg              SMALLINT,
         lefmg              SMALLINT, 
         rigmg              SMALLINT 
        END RECORD 
DEFINE  gcodemp             LIKE bco_mtransac.codemp
DEFINE  gnumcta             LIKE bco_mtransac.numcta 
DEFINE  existe              SMALLINT
DEFINE  tituloreporte       STRING 
DEFINE  filename            STRING
DEFINE  pipeline            STRING
DEFINE  s,d,apostrofe       CHAR(1) 
DEFINE  np                  CHAR(3) 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarReportes1")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("restadobancario")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL bcorep005_EstadoBancario() 
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION bcorep005_EstadoBancario()
 DEFINE w_mae_cta RECORD LIKE bco_mcuentas.*,
        myHandler om.SaxDocumentHandler, 
        imp1      datosreporte,
        wpais     VARCHAR(255),
        qrytxt    STRING,
        qrypart   STRING,
        loop      SMALLINT,
        w         ui.Window, 
        f         ui.Form   

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep005a AT 5,2
  WITH FORM "bcorep005a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/EstadoBancario.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Creando tabla temporal
  CREATE TEMP TABLE tmp_movtos 
  (tipope SMALLINT,
   tipmov SMALLINT,
   totdoc DEC(14,2)) WITH NO LOG 

  -- Cargando combobox de empresas
  LET gcodemp = librut003_DCbxEmpresaEnUsoXUsuario()
  IF gcodemp IS NULL THEN
     CALL fgl_winmessage(
     "Atencion","Usuario sin empresa asignada, VERIFICA.","stop")
     CLOSE WINDOW wrep002a
     RETURN
  END IF

  -- Cargando cuentas
  LET gnumcta = librut003_DCbxCuentasBanco(gcodemp)
  IF gnumcta IS NULL THEN
     CALL fgl_winmessage(
     "Atencion","Empresa sin cuentas de banco asignadas, VERIFICA.","stop")
     CLOSE WINDOW wrep002a
     RETURN
  END IF

  -- Llenando combobox de tipos de movimiento
  CALL librut003_CbxTiposMovimientoBco(1)

  -- Inicio del loop
  OPTIONS INPUT WRAP 
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE pipeline TO NULL
   LET s = 1 SPACES
   LET apostrofe = NULL 
   CLEAR FORM
   DELETE FROM tmp_movtos 
   DISPLAY gcodemp TO codemp 

   -- Ingresando datos 
   DIALOG ATTRIBUTES(UNBUFFERED,FIELD ORDER FORM)

    -- Construyendo busqueda
    INPUT BY NAME datos.numcta,
                  datos.tipmov,
                  datos.tipope,
                  datos.fecini,
                  datos.fecfin
    END INPUT 

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT DIALOG

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 
     LET p.length = 55 
     LET p.topmg  = 3
     LET p.botmg  = 2
     LET p.lefmg  = 2
     LET p.rigmg  = 2 

     -- Verificando ingreso de filtros
     IF NOT bcorep005_FiltrosCompletos() THEN 
        NEXT FIELD numcta 
     END IF 
     EXIT DIALOG 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 
     LET p.length = 55 
     LET p.topmg  = 0
     LET p.botmg  = 0
     LET p.lefmg  = 0
     LET p.rigmg  = 0

     -- Verificando ingreso de filtros
     IF NOT bcorep005_FiltrosCompletos() THEN 
        NEXT FIELD numcta 
     END IF 
     EXIT DIALOG 

    ON ACTION excel 
     -- Asignando dispositivo 
     LET pipeline  = "excel" 
     LET apostrofe = "'"
     LET s = ASCII(9) 
     LET p.length = 55 
     LET p.topmg  = 0
     LET p.botmg  = 0
     LET p.lefmg  = 0
     LET p.rigmg  = 0

     -- Verificando ingreso de filtros
     IF NOT bcorep005_FiltrosCompletos() THEN 
        NEXT FIELD numcta 
     END IF 
     EXIT DIALOG

   END DIALOG 
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Obeniendo datos de la cuenta 
   INITIALIZE w_mae_cta.* TO NULL
   CALL librut003_BCuentaBanco(datos.numcta)
   RETURNING w_mae_cta.*,existe
   LET datos.codemp = w_mae_cta.codemp 

   -- Construyendo seleccion 
   LET qrytxt = "SELECT a.*,0 ",
                 "FROM  vis_estadobancario a ",
                 "WHERE a.codemp = ",datos.codemp,
                 "  AND a.numcta = '",datos.numcta,"'",
                 "  AND a.tipmov = "||datos.tipmov,
                 "  AND a.feccob >= '",datos.fecini,"'", 
                 "  AND a.feccob <= '",datos.fecfin,"'", 
                 "  AND a.tipope = "||datos.tipope,
                 " ORDER BY a.codemp,a.numcta,a.feccob,a.numdoc" 

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep005 FROM qrytxt 
   DECLARE c_crep005 CURSOR FOR c_rep005
   LET existe = FALSE
   FOREACH c_crep005 INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       LET existe = TRUE
       START REPORT bcorep005_GenerarReporte TO filename 
    END IF 

    -- Verificando si documento esta anulado 
    IF (imp1.estado=0) THEN 
       LET imp1.totdoc = 0 
    END IF 

    -- Llenando el reporte
    OUTPUT TO REPORT bcorep005_GenerarReporte(imp1.*)
   END FOREACH
   CLOSE c_crep005 
   FREE  c_crep005 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT bcorep005_GenerarReporte 

      -- Transfiriendo reporte a excel
      IF pipeline = "excel" THEN
         CALL bcorep005_excel(filename)
      ELSE
       -- Enviando reporte al destino seleccionado
       CALL librut001_sendreport
       (filename,pipeline,tituloreporte, 
        "--noline-numbers "||
        "--nofooter "||
        "--font-size 7 "||
        "--page-width 842 --page-height 595 "||
        "--left-margin 35 --right-margin 15 "||
        "--top-margin 30 --bottom-margin 35 "||
        "--title Bancos")
      END IF

      ERROR "" 
      CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(
      " Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 
  END WHILE

  -- Dropeando tabla temporal
  DROP TABLE tmp_movtos 

  OPTIONS INPUT NO WRAP 

 CLOSE WINDOW wrep005a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION bcorep005_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE

 -- Verificando datos 
 IF datos.numcta IS NULL OR 
    datos.fecini IS NULL OR 
    datos.fecfin IS NULL THEN 
    CALL fgl_winmessage(
    " Atencion",
    " Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte 

REPORT bcorep005_GenerarReporte(imp1)
 DEFINE imp1              datosreporte,
        w_cargou          DEC(14,2),
        w_cartou,w_abonou DEC(12,2),
        w_abotou          DEC(14,2),
        linea             CHAR(306),
        xnommov           CHAR(40),
        xtipmov           SMALLINT,
        xtipope           SMALLINT,
        xtotdoc           DEC(14,2), 
        xnumdoc           INTEGER,       
        col,i,nmovs,lg    INTEGER,
        espacios          CHAR(4), 
        marca             CHAR(1), 
        fechareporte      STRING

  OUTPUT LEFT MARGIN   p.lefmg
         TOP MARGIN    p.topmg 
         BOTTOM MARGIN p.botmg 
         PAGE LENGTH   p.length

  FORMAT 
   PAGE HEADER

    -- Llenando linea
    LET linea = NULL
    LET lg = 170 
    FOR i = 1 TO lg
     LET linea = linea CLIPPED,"-"
    END FOR  

    -- Imprimiendo Encabezado
    LET col = librut001_centrado(tituloreporte,lg) 
    PRINT COLUMN   1,"Bancos",
	  COLUMN (lg-20),PAGENO USING "Pagina: <<<<"

    PRINT COLUMN   1,"Bcorep005",
          COLUMN col,UPSHIFT(tituloreporte) CLIPPED,  
          COLUMN (lg-20),"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    LET fechareporte = "PERIODO DE FECHAS ",
                       "DEL [ ",datos.fecini," ] AL [ ",datos.fecfin," ]" 
    LET col = librut001_centrado(fechareporte,lg) 

    PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
          COLUMN col,fechareporte CLIPPED,
          COLUMN (lg-20),"Hora  : ",TIME 
    PRINT linea CLIPPED 
    PRINT "Fecha       Tipo    Beneficiario                         Numero de",
          "       Descripcon del Movimiento                      ",
          "                          Fecha      Usuario"
    PRINT "Bancaria    Movto                                        Documento",
          "                                                      ",
          "   Cargos         Abonos  Registro   Registro"
    PRINT linea

    -- Imprimiendo datos de la cuenta 
    PRINT imp1.nomemp CLIPPED
    PRINT "# Cuenta Bancaria [ ",imp1.numcta CLIPPED," ]"
    PRINT "Banco ",imp1.nombco CLIPPED
    PRINT linea   

  BEFORE GROUP OF imp1.numcta 
   SKIP TO TOP OF PAGE

   -- Obteniendo saldo anterior 
   LET imp1.salant = librut003_SaldoAnteriorCuentaBanco(imp1.numcta,datos.fecini)

   -- Verificando si reporte es a excel 
   IF (pipeline="excel") THEN 
      -- Imprimiendo datos de la cuenta 
      PRINT tituloreporte CLIPPED,s 
      PRINT imp1.nomemp CLIPPED,s
      PRINT "# Cuenta Bancaria [ ",imp1.numcta CLIPPED," ]",s
      PRINT "Banco ",imp1.nombco CLIPPED,s
      PRINT "Periodo Del ",datos.fecini," Al ",datos.fecfin,s
      PRINT linea
      PRINT s 
      PRINT "Fecha Bancaria",s,
            "Tipo Movimiento",s, 
            "Beneficiario",s,  
            "Numero Documento",s,
            "Descripcion del Movimiento",s,
            "Cargos",s,
            "Abonos",s,
            "Fecha Registro",s,
            "Usuario Registro",s 
     PRINT s 
   END IF 

   -- Caculando Saldo Anterior 
   PRINT "Saldos Anterior --> ",s,8  SPACES,
                                s,15 SPACES,
                                s,15 SPACES,
                                s,52 SPACES,
                                s,imp1.salant USING "---,---,--&.&&"
   PRINT s 

   -- Inicializando datos
   LET nmovs    = 0 
   LET w_abotou = 0
   LET w_cartou = imp1.salant 

  ON EVERY ROW 
   -- Insertanto dato en tabla temporal
   INSERT INTO tmp_movtos 
   VALUES (imp1.tipope,imp1.tipmov,imp1.totdoc) 

   LET nmovs = (nmovs+1)
   LET w_abonou = 0
   LET w_cargou = 0

   -- Verificando tipo de operacion del movimiento
   CASE (imp1.tipope)
    WHEN 1 -- Cargos
     LET w_cargou  = imp1.totdoc
     LET w_cartou  = (w_cartou+imp1.totdoc)
    WHEN 0 -- Abonos
     LET w_abonou  = imp1.totdoc
     LET w_abotou  = (w_abotou+imp1.totdoc)
   END CASE

   -- Marcando de anulado el cheque
   LET marca = NULL 
   IF imp1.estado=0 THEN
      LET marca = "A"
   END IF 

   -- Imprimiendo movimientos 
   LET espacios = s,s,s,s 

   -- Verificando si reportes es a excel 
   IF (pipeline="excel") THEN 
    PRINT imp1.feccob                                     ,s,
          imp1.nommov                                     ,s,
          imp1.nomchq                                     ,s,apostrofe, 
          imp1.numdoc                                     ,s,apostrofe,
          imp1.descrp               CLIPPED               ,s,
          w_cargou                  USING "###,###,##&.&&",s,          
          w_abonou                  USING "###,###,##&.&&",s,
          imp1.fecsis                                     ,s,
          imp1.userid                                     ,s,
          marca                                           ,s 
   ELSE
    PRINT imp1.feccob                                     ,s,1 SPACES,
          imp1.nommov                                     ,s,1 SPACES,
          imp1.nomchq                                     ,s,1 SPACES, 
          imp1.numdoc                                     ,s,
          imp1.descrp[1,40]                               ,s,1 SPACES,
          w_cargou                  USING "###,###,##&.&&",s,          
          w_abonou                  USING "###,###,##&.&&",s,1 SPACES,
          imp1.fecsis                                     ,s,
          imp1.userid                                     ,s,
          marca                                           ,s 

    -- Verificando si tiene descripcion
    IF (LENGTH(imp1.descrp)>40) THEN
       PRINT COLUMN 74,espacios CLIPPED,imp1.descrp[41,80] 
    END IF
    -- Verifica si tiene descripcion
    IF (LENGTH(imp1.descrp)>80) THEN
       PRINT COLUMN 74,espacios CLIPPED,imp1.descrp[81,120] 
    END IF
    -- Verifica si tiene descripcion
    IF (LENGTH(imp1.descrp)>120) THEN
       PRINT COLUMN 74,espacios CLIPPED,imp1.descrp[121,150] 
    END IF
    -- Verifica si tiene descripcion
    IF (LENGTH(imp1.descrp)>150) THEN
       PRINT COLUMN 74,espacios CLIPPED,imp1.descrp[151,180] 
    END IF
    -- Verifica si tiene descripcion
    IF (LENGTH(imp1.descrp)>180) THEN
       PRINT COLUMN 74,espacios CLIPPED,imp1.descrp[181,210] 
    END IF
    -- Verifica si tiene descripcion
    IF (LENGTH(imp1.descrp)>210) THEN
       PRINT COLUMN 74,espacios CLIPPED,imp1.descrp[211,240] 
    END IF
    -- Verifica si tiene descripcion
    IF (LENGTH(imp1.descrp)>240) THEN
       PRINT COLUMN 74,espacios CLIPPED,imp1.descrp[241,270] 
    END IF
    -- Verifica si tiene descripcion
    IF (LENGTH(imp1.descrp)>271) THEN
       PRINT COLUMN 74,espacios CLIPPED,imp1.descrp[271,300] 
    END IF
   END IF 

  AFTER GROUP OF imp1.numcta  
   -- Totalizando
   PRINT s
   PRINT COLUMN   1,nmovs USING "<<<<<<"," Movimiento[s] ",s,s,s,s, 
         COLUMN  81,"Totales -->",s, 
         COLUMN 116,w_cartou                  USING "###,###,##&.&&",s,
                    w_abotou                  USING "###,###,##&.&&",s
   PRINT s
   PRINT COLUMN   1,"Total [ Cargos - Abonos ] -->",s, 
         COLUMN 116,(w_cartou-w_abotou)       USING "---,---,--&.&&",s 

  ON LAST ROW
   -- Resumen por tipo de movimiento 
   IF (pipeline="excel") THEN 
    PRINT s 
    PRINT "Resumen Por Tipo de Movimiento",s,"Total",s,"# Doctos",s 
   ELSE
    PRINT s 
    PRINT "Resumen Por Tipo de Movimiento                      Total       # Doctos"
   END IF 

   PRINT "------------------------------------------------------------------------" 
   DECLARE ctip CURSOR FOR
   SELECT a.tipope,a.tipmov,b.nommov,SUM(a.totdoc),COUNT(*)
    FROM  tmp_movtos a,bco_tipomovs b 
    WHERE b.tipmov = a.tipmov 
    GROUP BY 1,2,3
    ORDER BY 1 DESC,2 
    FOREACH ctip INTO xtipope,xtipmov,xnommov,xtotdoc,xnumdoc
     PRINT xnommov,s,2 SPACES,xtotdoc USING "###,###,##&.&&",s,2 SPACES,xnumdoc,s
    END FOREACH
    CLOSE ctip
    FREE  ctip 
   PRINT "------------------------------------------------------------------------" 
   
   -- Imprimiendo filtros
   PRINT s
   PRINT "** Filtros **",s
   IF datos.tipmov IS NOT NULL THEN
      SELECT a.nommov INTO xnommov FROM bco_tipomovs a WHERE a.tipmov = datos.tipmov 
      PRINT "Tipo de Movimiento :",s,xnommov,s
   END IF
   IF datos.tipope IS NOT NULL THEN
      CASE (datos.tipope)
       WHEN 1 PRINT "Tipo de Operacion  :",s,"Cargos",s
       WHEN 0 PRINT "Tipo de Operacion  :",s,"Abonos",s
      END CASE
   END IF
END REPORT 

-- Subrutina para exportar el reporte a excel usando WinCOM 

FUNCTION bcorep005_excel(filename) 
 DEFINE sb          base.StringBuffer
 DEFINE ch          base.channel
 DEFINE lista       base.StringTokenizer
 DEFINE result      INTEGER
 DEFINE i           INTEGER
 DEFINE xlapp       INTEGER
 DEFINE xlwb        INTEGER
 DEFINE linea       STRING 
 DEFINE lineadep    STRING 
 DEFINE filename    STRING 
 DEFINE columna     STRING 
 DEFINE separador   CHAR(1) 
 DEFINE nuevalinea  CHAR(1) 

 -- Inicializando variables                            
 LET xlapp = -1
 LET xlwb = -1

 -- Creando la instancia de Excel
 CALL ui.Interface.frontCall("WinCOM", "CreateInstance", ['Excel.Application'], [xlapp])
 CALL CheckError(xlapp, xlwb, xlapp)
   
 -- Agregando un libro al documento de excel               
 CALL ui.interface.frontCall("WinCOM", "CallMethod", [xlapp, 'WorkBooks.Add'], [xlwb])
 CALL CheckError(xlapp, xlwb, xlwb)
   
 -- Haciendo visible el libro                 
 CALL excel_set_property(xlapp, xlapp, "Visible","True")
  
 -- Llenando celdas desde el FrontCall 
 -- Construyendo celdas delimitando columnas por TAB (ASCII(9)), 
 -- y filas por CR (ASCII(10)
 -- Copia al clipboard via el Frontcall y luego instruye a excel para pegar el contenido
 -- de la celdas dentro del libro 
 LET sb = base.StringBuffer.create()

 -- Leyendo el archivo que contiene el reporte y copiandolo al clipboard 
 LET ch = base.channel.create()
 call ch.openfile(filename,"r")

 let i = 1 
 let separador  = ascii(9) 
 let nuevalinea = ascii(10)
 while true
  let linea = ch.readLine() 
  if ch.isEof() then exit while end if
  if linea.getindexof(separador,1)>0 then
     CALL sb.append(linea)
     CALL sb.append(nuevalinea)
  end if 
  let i = i+1
 end while
 call ch.close() 

 -- Pegando el contenido de las celdas en el clipboard al libro de excel
 CALL ui.Interface.FrontCall("standard","cbset",sb.toString(),result)
 CALL excel_call_method(xlapp, xlwb, 'ActiveSheet.Paste')
 CALL excel_call_method(xlapp, xlwb, 'Columns("A:Z").EntireColumn.AutoFit')
 --CALL excel_set_property(xlapp, xlwb, excel_column(nc,"Z","NumberFormat"), "0.00")

 -- Liberando informacion del clipboard    
 CALL freeMemory(xlapp, xlwb)
END FUNCTION
