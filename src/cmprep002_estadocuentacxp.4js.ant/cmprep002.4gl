{ 
Programa : cmprep002.4gl                   
Objetivo : Reporte de estado de cuenta por proveedor.
}

DATABASE foodstore 

-- Definicion de variables globales 

CONSTANT progname = "cmprep002" 
TYPE    datosreporte        RECORD 
         origen             SMALLINT, 
         codprv             LIKE inv_provedrs.codprv,
         nomprv             CHAR(50),                
         salant             DEC(14,2),     
         tipmov             SMALLINT,
         nommov             CHAR(20),
         fecemi             DATE,
         fecven             DATE,
         numdoc             CHAR(20),
         totdoc             DEC(14,2),
         descrp             CHAR(40),
         tipope             SMALLINT,
         numfac             CHAR(20)
        END RECORD 
DEFINE  datos               RECORD 
         codprv             LIKE inv_provedrs.codprv, 
         fecini             DATE,
         fecfin             DATE
        END RECORD
DEFINE  existe,nl,lg        SMALLINT
DEFINE  primeravez          SMALLINT
DEFINE  totsalant           DEC(14,2) 
DEFINE  totsalrep           DEC(14,2) 
DEFINE  totcarrep           DEC(14,2) 
DEFINE  totaborep           DEC(14,2) 
DEFINE  tituloreporte       STRING 
DEFINE  filename            STRING
DEFINE  pipeline            STRING
DEFINE  s,d,nc              CHAR(1) 
DEFINE  np                  CHAR(3) 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/ToolBarReportes1")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rantiguedadsaldosprov")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL cmprep002_EstadoCuentaProveedor() 
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION cmprep002_EstadoCuentaProveedor() 
 DEFINE imp1      datosreporte,
        wpais     VARCHAR(255),
        loop      SMALLINT,
        w         ui.Window, 
        f         ui.Form,
        qrytxt    STRING

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep002a AT 5,2
  WITH FORM "cmprep002a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/EstadoCuentaCXP.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Llenando combobox de proveedores                    
  CALL librut003_CbxProveedores() 

  -- Inicio del loop
  OPTIONS INPUT WRAP 
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE pipeline,datos.* TO NULL
   LET primeravez = TRUE
   LET s          = 1 SPACE
   LET totsalant  = 0
   LET totsalrep  = 0
   LET totcarrep  = 0
   LET totaborep  = 0
   CLEAR FORM

   -- Ingresando datos 
   DIALOG ATTRIBUTES(UNBUFFERED,FIELD ORDER FORM)

    -- Construyendo busqueda
    INPUT BY NAME datos.codprv,
                  datos.fecini,
                  datos.fecfin
    END INPUT 

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT DIALOG

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 

     -- Verificando ingreso de filtros
     IF NOT cmprep002_FiltrosCompletos() THEN 
        NEXT FIELD codprv 
     END IF 
     EXIT DIALOG 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf" 

     -- Verificando ingreso de filtros
     IF NOT cmprep002_FiltrosCompletos() THEN 
        NEXT FIELD codprv 
     END IF 
     EXIT DIALOG 

    ON ACTION excel 
     -- Asignando dispositivo 
     LET pipeline = "excel" 
     LET s = ASCII(9) 

     -- Verificando ingreso de filtros
     IF NOT cmprep002_FiltrosCompletos() THEN 
        NEXT FIELD codprv 
     END IF 
     EXIT DIALOG

   END DIALOG 
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Calculando numero de largo del reporte
   LET lg = 160 
   LET np = 8    
   LET nl = 72 

   -- Construyendo seleccion 
   LET qrytxt = "SELECT UNIQUE a.codprv,a.nomprv,0 ",
                 "FROM  inv_provedrs a ",
                 "WHERE EXISTS (SELECT p.codprv FROM cmp_mtransac p ", 
                                "WHERE p.codprv = a.codprv ",
                                  "AND p.estado = 1) ",
                   "AND a.codprv = "||datos.codprv, 
                 " ORDER BY a.nomprv" 

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep002 FROM qrytxt 
   DECLARE c_crep002 CURSOR FOR c_rep002
   LET existe = FALSE
   FOREACH c_crep002 INTO imp1.codprv,imp1.nomprv,imp1.salant 
    -- Iniciando reporte
    IF NOT existe THEN
       LET existe = TRUE
       START REPORT cmprep002_GenerarReporte TO filename 
    END IF 

    -- Calculando Saldo Anterior 
    LET imp1.salant = librut003_SaldoAnteriorProveedor(imp1.codprv,datos.fecini) 

    -- Llenando el reporte
    OUTPUT TO REPORT cmprep002_GenerarReporte(imp1.*)
   END FOREACH
   CLOSE c_crep002 
   FREE  c_crep002 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT cmprep002_GenerarReporte 

      -- Transfiriendo reporte a excel
      IF pipeline = "excel" THEN
         CALL cmprep002_excel(filename)
      ELSE
         -- Enviando reporte al destino seleccionado
         CALL librut001_sendreport(filename,pipeline,tituloreporte,"-l -p "||np)
      END IF

      ERROR "" 
      CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(
      " Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 
  END WHILE
  OPTIONS INPUT NO WRAP 

 CLOSE WINDOW wrep002a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION cmprep002_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE

 -- Verificando datos 
 IF datos.fecini IS NULL OR 
    datos.fecfin IS NULL THEN 
    CALL fgl_winmessage(
    " Atencion",
    " Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte 

REPORT cmprep002_GenerarReporte(imp1)
 DEFINE imp1              datosreporte,
        linea             CHAR(306),
        salact            DEC(14,2), 
        cargos            DEC(14,2), 
        abonos            DEC(14,2), 
        totcar            DEC(14,2),
        totabo            DEC(14,2),
        totsal            DEC(14,2), 
        col,i             INTEGER,
        espacios          CHAR(4), 
        xnumdoc           CHAR(45), 
        fechareporte      STRING,
        f                 CHAR(1),
        xfrmpag           CHAR(1) 

  OUTPUT LEFT MARGIN 3
         TOP MARGIN 2 
         BOTTOM MARGIN 2 
         PAGE LENGTH nl 

  FORMAT 
   PAGE HEADER

    -- Llenando linea
    LET linea = NULL
    FOR i = 1 TO lg
     LET linea = linea CLIPPED,"-"
    END FOR  

    -- Imprimiendo Encabezado
    LET col = librut001_centrado(tituloreporte,lg) 
    PRINT COLUMN   1,"Cuentas X Pagar",
	  COLUMN (lg-20),PAGENO USING "Pagina: <<<<"

    PRINT COLUMN   1,"Cmprep002",
          COLUMN col,UPSHIFT(tituloreporte) CLIPPED,  
          COLUMN (lg-20),"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    LET fechareporte = "PERIODO DE FECHAS ",
                       "DEL [ ",datos.fecini," ] AL [ ",datos.fecfin," ]" 
    LET col = librut001_centrado(fechareporte,lg) 

    PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
          COLUMN col,fechareporte CLIPPED,
          COLUMN (lg-20),"Hora  : ",TIME 
    PRINT linea 
    PRINT "MOVIMIENTO           FECHA        FECHA        NUMERO DOCUMENTO - NUMERO FACTURA",
          "        S A L D O           A B O N O S     C A R G O S       S A L D O"
    PRINT "                     EMISION      VENCIMIENTO                                   ",
          "        A N T E R I O R                                       A C T U A L"
    PRINT linea 

  BEFORE GROUP OF imp1.codprv 
   -- Imprimiendo encabezado a excel
   IF (pipeline="excel") THEN
      IF primeravez THEN
         PRINT UPSHIFT(tituloreporte) CLIPPED,s
         PRINT fechareporte CLIPPED,s
         PRINT s
         PRINT "MOVIMIENTO",s,
               "FECHA EMISION",s,
               "FECHA VENCIMIENTO",s,
               "NUMERO DOCUMENTO - NUMERO FACTURA",s,
               "SALDO ANTERIOR",s,
               "ABONOS",s,
               "CARGOS",s,
               "SALDO ACTUAL",s
         PRINT s
         LET primeravez = FALSE
      END IF
   END IF

   -- Imprimiendo datos del proveedor 
   PRINT "Proveedor: [ ",imp1.nomprv CLIPPED," ]",s 
   PRINT s 
   PRINT 88 SPACES,imp1.salant USING "----,---,--&.&&",s  
   PRINT s 

   -- Inicializando totales  
   LET totcar = 0
   LET totabo = 0 
   LET totsal = 0 
   LET salact = imp1.salant  
 
  ON EVERY ROW 
   -- Seleccionando cargos y abonos 
   DECLARE cmovs CURSOR FOR 
   SELECT a.origen,  -- Cargos 
          a.tipmov,
          a.nommov,
          a.fecemi,
          a.fecven,
          a.numdoc,
          a.totdoc,
          a.descrp,
          a.tipope,
          a.numfac
    FROM  vis_estadocuentacxp a
    WHERE a.codprv  = imp1.codprv 
      AND a.fecemi >= datos.fecini 
      AND a.fecemi <= datos.fecfin 
      AND a.origen  = 1  

    UNION ALL

   SELECT a.origen,  -- Abonos 
          a.tipmov,
          a.nommov,
          a.fecemi,
          a.fecven,
          a.numdoc,
          a.totdoc,
          a.descrp,
          a.tipope,
          a.numfac 
    FROM  vis_estadocuentacxp a
    WHERE a.codprv  = imp1.codprv 
      AND a.fecemi >= datos.fecini 
      AND a.fecemi <= datos.fecfin 
      AND a.origen  = 2  

    UNION ALL

   SELECT a.origen,  -- Pagos en Efectivo 
          a.tipmov,
          a.nommov,
          a.fecemi,
          a.fecven,
          a.numdoc,
          a.totdoc,
          a.descrp,
          a.tipope,
          a.numfac 
    FROM  vis_estadocuentacxp a
    WHERE a.codprv  = imp1.codprv 
      AND a.fecemi >= datos.fecini 
      AND a.fecemi <= datos.fecfin 
      AND a.origen  = 3  
    ORDER BY a.fecemi 

    FOREACH cmovs INTO imp1.origen,
                       imp1.tipmov,
                       imp1.nommov,
                       imp1.fecemi,
                       imp1.fecven,
                       imp1.numdoc,
                       imp1.totdoc,
                       imp1.descrp, 
                       imp1.tipope,
                       imp1.numfac 

     -- Verificando tipo de movimiento segun su origen 
     LET xfrmpag = NULL 
     CASE (imp1.origen)
      -- Compras 
      WHEN 1 
       -- Verificando tipo de movimiento
       CASE (imp1.tipope)
        WHEN 1 -- Cargos 
         LET cargos = imp1.totdoc  
         LET abonos = 0  
         LET salact = (salact+imp1.totdoc)
         LET totcar = (totcar+cargos)
        WHEN 0 -- Abonos 
         LET abonos = imp1.totdoc
         LET cargos = 0  
         LET salact = (salact-imp1.totdoc)
         LET totabo = (totabo+abonos)
       END CASE 

       LET xnumdoc= imp1.numdoc CLIPPED

      -- Bancos 
      WHEN 2 
       LET abonos = imp1.totdoc
       LET cargos = 0  
       LET salact = (salact-imp1.totdoc)
       LET totabo = (totabo+abonos)
       LET xnumdoc= imp1.numdoc CLIPPED," - ",imp1.numfac CLIPPED

      -- Compras Efectivo
      WHEN 3 
       -- Cancelando pagos en efectivo 
       LET xfrmpag= "E" 
       LET cargos = imp1.totdoc  
       LET abonos = imp1.totdoc 
       LET salact = 0
       LET totcar = (totcar+cargos)
       LET totabo = (totabo+abonos)
       LET xnumdoc= imp1.numdoc CLIPPED
     END CASE 

     -- Imprimiendo movimientos 
     IF (pipeline="excel") THEN
      PRINT imp1.nommov                                      ,s,
            imp1.fecemi                                      ,s,
            imp1.fecven                                      ,s,
            xnumdoc                                          ,s,
            abonos      USING "----,---,--&.&&"              ,s,
            cargos      USING "----,---,--&.&&"              ,s,
            salact      USING "----,---,--&.&&"              ,s,xfrmpag 
     ELSE
      PRINT imp1.nommov                                      ,s,
            imp1.fecemi                                      ,2  SPACES,s,
            imp1.fecven                                      ,2  SPACES,s,
            xnumdoc                                          ,11 SPACES,s,
            abonos      USING "----,---,--&.&&"              ,s,
            cargos      USING "----,---,--&.&&"              ,s,
            salact      USING "-----,---,--&.&&"             ,s,xfrmpag 
     END IF 

   END FOREACH 
   CLOSE cmovs
   FREE  cmovs 

  AFTER GROUP OF imp1.codprv  
   -- Totalizando proveedor 
   LET totsal = (imp1.salant+totcar-totabo) 

   PRINT s 
   PRINT "Total Proveedor -->"                            ,69 SPACES, 
         imp1.salant USING "----,---,--&.&&"              ,s,
         totabo      USING "----,---,--&.&&"              ,s, 
         totcar      USING "----,---,--&.&&"              ,s,
         totsal      USING "-----,---,--&.&&"             ,s  
   PRINT s 

   -- Totalizando reporte
   LET totsalant = totsalant+imp1.salant  
   LET totcarrep = totcarrep+totcar 
   LET totaborep = totaborep+totabo 
   LET totsalrep = totsalrep+totsal 
   
  ON LAST ROW
   -- Totalizando reporte 
   PRINT "Total Reporte ---->"                            ,69 SPACES, 
         totsalant   USING "----,---,--&.&&"              ,s,
         totaborep   USING "----,---,--&.&&"              ,s, 
         totcarrep   USING "----,---,--&.&&"              ,s,
         totsalrep   USING "-----,---,--&.&&"             ,s  
END REPORT 

-- Subrutina para exportar el reporte a excel usando WinCOM 

FUNCTION cmprep002_excel(filename) 
 DEFINE sb          base.StringBuffer
 DEFINE ch          base.channel
 DEFINE lista       base.StringTokenizer
 DEFINE result      INTEGER
 DEFINE i           INTEGER
 DEFINE xlapp       INTEGER
 DEFINE xlwb        INTEGER
 DEFINE linea       STRING 
 DEFINE lineadep    STRING 
 DEFINE filename    STRING 
 DEFINE columna     STRING 
 DEFINE separador   CHAR(1) 
 DEFINE nuevalinea  CHAR(1) 

 -- Inicializando variables                            
 LET xlapp = -1
 LET xlwb = -1

 -- Creando la instancia de Excel
 CALL ui.Interface.frontCall("WinCOM", "CreateInstance", ['Excel.Application'], [xlapp])
 CALL CheckError(xlapp, xlwb, xlapp)
   
 -- Agregando un libro al documento de excel               
 CALL ui.interface.frontCall("WinCOM", "CallMethod", [xlapp, 'WorkBooks.Add'], [xlwb])
 CALL CheckError(xlapp, xlwb, xlwb)
   
 -- Haciendo visible el libro                 
 CALL excel_set_property(xlapp, xlapp, "Visible","True")
  
 -- Llenando celdas desde el FrontCall 
 -- Construyendo celdas delimitando columnas por TAB (ASCII(9)), 
 -- y filas por CR (ASCII(10)
 -- Copia al clipboard via el Frontcall y luego instruye a excel para pegar el contenido
 -- de la celdas dentro del libro 
 LET sb = base.StringBuffer.create()

 -- Leyendo el archivo que contiene el reporte y copiandolo al clipboard 
 LET ch = base.channel.create()
 call ch.openfile(filename,"r")

 let i = 1 
 let separador  = ascii(9) 
 let nuevalinea = ascii(10)
 while true
  let linea = ch.readLine() 
  if ch.isEof() then exit while end if
  if linea.getindexof(separador,1)>0 then
     CALL sb.append(linea)
     CALL sb.append(nuevalinea)
  end if 
  let i = i+1
 end while
 call ch.close() 

 -- Pegando el contenido de las celdas en el clipboard al libro de excel
 CALL ui.Interface.FrontCall("standard","cbset",sb.toString(),result)
 CALL excel_call_method(xlapp, xlwb, 'ActiveSheet.Paste')
 CALL excel_call_method(xlapp, xlwb, 'Columns("A:Z").EntireColumn.AutoFit')

 -- Liberando informacion del clipboard    
 CALL freeMemory(xlapp, xlwb)
END FUNCTION
