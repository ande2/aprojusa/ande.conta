drop view vis_estadocuentacxp;

create view vis_estadocuentacxp
(origen,codprv,nomprv,tipmov,nommov,fecemi,fecven,numdoc,totdoc,descrp,tipope,lnkcmp,numfac)

as

select 1,a.codprv,p.nomprv,a.tipmov,c.nommov,a.fecemi,a.fecven,a.numdoc,a.totdoc,a.descrp,
       a.tipope,a.lnkcmp,""
from cmp_mtransac a,cmp_tipomovs c,inv_provedrs p
where a.frmpag in (2,3)
  and a.estado = 1
  and c.tipmov = a.tipmov
  and p.codprv = a.codprv

union all

select 2,a.codprv,p.nomprv,a.tipmov,c.nommov,a.feccob,a.feccob,a.numdoc,y.totpag,a.descrp,
       a.tipope,m.lnkcmp,m.numdoc
from bco_mtransac a,bco_dtransac y,bco_tipomovs c,inv_provedrs p,cmp_mtransac m
where a.lnkbco = y.lnkbco
  and a.estado = 1
  and y.lnkcmp = m.lnkcmp
  and c.tipmov = a.tipmov
  and p.codprv = a.codprv

union all

select 3,a.codprv,p.nomprv,a.tipmov,c.nommov,a.fecemi,a.fecven,a.numdoc,a.totdoc,a.descrp,
       a.tipope,a.lnkcmp,""
from cmp_mtransac a,cmp_tipomovs c,inv_provedrs p
where a.frmpag in (1)
  and a.estado = 1
  and c.tipmov = a.tipmov
  and p.codprv = a.codprv;

grant select on vis_estadocuentacxp to public;
