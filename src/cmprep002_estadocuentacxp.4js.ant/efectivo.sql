
select 3,a.codprv,p.nomprv,a.tipmov,c.nommov,a.fecemi,a.fecven,a.numdoc,a.totdoc,a.descrp,
       a.tipope,a.lnkcmp,""
from cmp_mtransac a,cmp_tipomovs c,inv_provedrs p
where a.frmpag in (1)
  and a.estado = 1
  and c.tipmov = a.tipmov
  and p.codprv = a.codprv;
