{ 
glbglo038.4gl
Mynor Ramirez
Mantenimiento de autorizantes 
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "glbmae038"
DEFINE w_mae_pro      RECORD LIKE glb_userauth.*,
       v_autorizantes DYNAMIC ARRAY OF RECORD
        tcodaut       LIKE glb_userauth.codaut,
        tnomaut       LIKE glb_userauth.nomaut, 
        tendrec       CHAR(1) 
       END RECORD,
       username       VARCHAR(15)
END GLOBALS
