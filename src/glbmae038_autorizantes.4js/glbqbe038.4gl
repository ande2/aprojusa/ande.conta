{
glbqbe038.4gl 
Mynor Ramirez
Mantenimiento de autorizantes 
}

{ Definicion de variables globales }

GLOBALS "glbglo038.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION glbqbe038_autorizantes(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),    
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,   
        msg                 STRING,
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL glbqbe038_EstadoMenu(operacion," ") 
   INITIALIZE qrytext,qrypart TO NULL
   CALL glbmae038_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codaut,a.nomaut,a.estado,a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel
     -- Salida
     CALL glbmae038_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codaut,a.nomaut,'' ",
                 " FROM glb_userauth a ",
                 " WHERE ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_autorizantes SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_autorizantes.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_autorizantes INTO v_autorizantes[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_autorizantes
   FREE  c_autorizantes
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL glbqbe038_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_autorizantes TO s_autorizantes.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL glbmae038_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscando
      CALL glbmae038_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      LET res = glbmae038_autorizantes(2) 
      -- Desplegando datos
      CALL glbqbe038_datos(v_autorizantes[ARR_CURR()].tcodaut)

     ON KEY (CONTROL-M) 
      -- Modificando 
      IF (operacion=2) THEN 
         -- Modificando 
         LET res = glbmae038_autorizantes(2) 
         -- Desplegando datos
         CALL glbqbe038_datos(v_autorizantes[ARR_CURR()].tcodaut)
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF glbqbe038_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Este autorizante ya tiene registros.\n Autorizante no puede borrarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de Borrar este autorizante ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL glbmae038_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte 
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Autorizante" 
      LET arrcols[2] = "Nombre"
      LET arrcols[3] = "Activo"
      LET arrcols[4] = "Usuario Registro"
      LET arrcols[5] = "Fecha Registro"
      LET arrcols[6] = "Hora Registro" 

      -- Se aplica el mismo query de la seleccion para enviar al reporte 
      LET qry        = "SELECT a.codaut,a.nomaut,",
                              "CASE (a.estado) WHEN 1 THEN 'Si' WHEN 2 THEN 'No' END,",
                              "a.userid,a.fecsis,a.horsis ",
                       " FROM glb_userauth a ",
                       " WHERE ",qrytext CLIPPED,
                       " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res    = librut002_excelreport("Autorizantes",qry,6,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
         CALL glbqbe038_datos(v_autorizantes[ARR_CURR()].tcodaut)
      END IF 

    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen autorizantes con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Asignando estado del menu 
  CALL glbqbe038_EstadoMenu(0,"") 
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION glbqbe038_datos(wcodaut)
 DEFINE wcodaut LIKE glb_userauth.codaut,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  glb_userauth a "||
              "WHERE a.codaut = "||wcodaut||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_autorizantest SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_autorizantest INTO w_mae_pro.*
 END FOREACH
 CLOSE c_autorizantest
 FREE  c_autorizantest

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nomaut,w_mae_pro.estado 
 DISPLAY BY NAME w_mae_pro.codaut,w_mae_pro.userid THRU w_mae_pro.horsis
END FUNCTION 

-- Subrutina para verificar si la autorizante tiene registros

FUNCTION glbqbe038_integridad()
 DEFINE conteo INTEGER  

 -- Verificando compras 
 SELECT COUNT(*)
  INTO  conteo
  FROM  cmp_mtransac a
  WHERE (a.codaut = w_mae_pro.codaut) 
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     SELECT COUNT(*)
      INTO  conteo
      FROM  bco_mtransac a
      WHERE (a.codaut = w_mae_pro.codaut) 
      IF (conteo>0) THEN
         RETURN TRUE 
      ELSE
         RETURN FALSE
      END IF 
  END IF
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION glbqbe038_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Autorizantes - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Autorizantes - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Autorizantes - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Autorizantes - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Autorizantes - NUEVO")
 END CASE
END FUNCTION
