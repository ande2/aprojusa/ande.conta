{ 
facglo006.4gl
Mynor Ramirez
Mantenimiento de rubros de gasto 
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "facmae006"
DEFINE w_mae_pro      RECORD LIKE fac_rubgasto.*,
       v_rubros       DYNAMIC ARRAY OF RECORD
        tcodrub       LIKE fac_rubgasto.codrub,
        tnomrub       LIKE fac_rubgasto.nomrub, 
        tendrec       CHAR(1) 
       END RECORD,
       v_partida      DYNAMIC ARRAY OF RECORD 
        numcta        LIKE glb_ctasxrub.numcta,
        nomcta        CHAR(50), 
        tipcol        LIKE glb_ctasxrub.tipcol,
        rellen        CHAR(1) 
       END RECORD, 
       w_datos        RECORD
        codemp        LIKE glb_empenuso.codemp 
       END RECORD, 
       username       VARCHAR(15)
END GLOBALS
