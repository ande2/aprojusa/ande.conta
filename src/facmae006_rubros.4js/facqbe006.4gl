{ 
facqbe006.4gl 
Fernando Lontero
Mantenimiento de rubros de gasto 
}

{ Definicion de variables globales }

GLOBALS "facglo006.4gl" 
DEFINE totlin,totcta INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION facqbe006_rubros(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),    
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,   
        msg                 STRING,
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL facqbe006_EstadoMenu(operacion," ") 
   INITIALIZE qrytext,qrypart TO NULL
   CALL facmae006_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codrub,a.nomrub,a.pagisv, 
                                a.estado,a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel
     -- Salida
     CALL facmae006_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codrub,a.nomrub ",
                 " FROM fac_rubgasto a ",
                 " WHERE ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_rubros SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_rubros.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_rubros INTO v_rubros[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_rubros
   FREE  c_rubros
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL facqbe006_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_rubros TO s_rubros.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL facmae006_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscando
      CALL facmae006_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      LET res = facmae006_rubros(2) 
      -- Desplegando datos
      CALL facqbe006_datos(v_rubros[ARR_CURR()].tcodrub)

     ON KEY (CONTROL-M) 
      -- Modificando 
      IF (operacion=2) THEN 
         -- Modificando 
         LET res = facmae006_rubros(2) 
         -- Desplegando datos
         CALL facqbe006_datos(v_rubros[ARR_CURR()].tcodrub)
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF facqbe006_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Este rubro ya tiene registros.\n Rubro no puede borrarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de Borrar este rubro ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL facmae006_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION cuentas 
      -- Verificando si existe empresa asignada 
      IF w_datos.codemp IS NULL THEN
         CALL fgl_winmessage(
         "Atencion","Usuario sin empresa asignada, VERIFICA.","stop")
         CONTINUE DISPLAY  
      END IF

      -- Registro de detalle de cuentas contables del rubro 
      CALL facqbe006_DetalleCuentas() 
      CALL facqbe006_CuentasRubro()

     ON ACTION reporte 
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Rubro" 
      LET arrcols[2] = "Nombre del Rubro"
      LET arrcols[3] = "Rubro Activo"
      LET arrcols[4] = "Paga Impuesto"  
      LET arrcols[5] = "Usuario Registro"
      LET arrcols[6] = "Fecha Registro"
      LET arrcols[7] = "Hora Registro" 

      -- Se aplica el mismo query de la seleccion para enviar al reporte 
      LET qry        = "SELECT a.codrub,a.nomrub,",
                          "CASE (a.pagisv) WHEN 1 THEN 'Si' WHEN 0 THEN 'No' END,",
                          "CASE (a.estado) WHEN 1 THEN 'Si' WHEN 0 THEN 'No' END,",
                          "a.userid,a.fecsis,a.horsis ",
                       " FROM fac_rubgasto a ",
                       " WHERE ",qrytext CLIPPED,
                       " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res    = librut002_excelreport("Rubros de Gasto",qry,7,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
         CALL facqbe006_datos(v_rubros[ARR_CURR()].tcodrub)
         CALL facqbe006_CuentasRubro()
      END IF 

    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen rubros con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Asignando estado del menu 
  CALL facqbe006_EstadoMenu(0,"") 
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION facqbe006_datos(wcodrub)
 DEFINE wcodrub LIKE fac_rubgasto.codrub,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  fac_rubgasto a "||
              "WHERE a.codrub = "||wcodrub||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_rubrost SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_rubrost INTO w_mae_pro.*
 END FOREACH
 CLOSE c_rubrost
 FREE  c_rubrost

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.codrub,w_mae_pro.nomrub,
                 w_mae_pro.pagisv,w_mae_pro.estado  
 DISPLAY BY NAME w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION 

-- Subrutina para verificar si la rubro tiene registros

FUNCTION facqbe006_integridad()
 DEFINE conteo INTEGER 

 -- Verificando 
 SELECT COUNT(*)
  INTO  conteo
  FROM  fac_dcachica a 
  WHERE (a.codrub = w_mae_pro.codrub) 
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF
  RETURN FALSE  
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION facqbe006_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Rubros de Gasto - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Rubros de Gasto - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Rubros de Gasto - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Rubros de Gasto - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Rubros de Gasto - NUEVO")
 END CASE
END FUNCTION

-- Subutina para el ingreso de las cuentas contables de la poliza 

FUNCTION facqbe006_DetalleCuentas() 
 DEFINE w_mae_cta  RECORD LIKE ctb_mcuentas.*,
        loop,scr   SMALLINT,
        repetido   SMALLINT,
        opc,arr,i  SMALLINT, 
        linea      SMALLINT, 
        regreso    SMALLINT, 
        existe     SMALLINT, 
        correl     SMALLINT, 
        lastkey    INTEGER,
        msg        STRING 

 -- Iniciando el loop
 LET loop = TRUE
 WHILE loop

  -- Ingresando productos
  INPUT ARRAY v_partida WITHOUT DEFAULTS FROM s_partida.*
   ATTRIBUTE(COUNT=totcta,INSERT ROW=FALSE,ACCEPT=FALSE,CANCEL=FALSE,
             UNBUFFERED=TRUE,FIELD ORDER FORM)

   ON ACTION accept 
    -- Aceptar
    EXIT INPUT

   ON ACTION cancel 
    -- Cancelar
    LET loop = FALSE
    EXIT INPUT

   ON ACTION listacuenta  
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Seleccionado lista de cuentas contables
    CALL librut002_formlist("Consulta de Cuentas Contables ",
                            "# Cuenta",
                            "Nombre de la Cuenta",
                            "",
                            "numcta",
                            "nomcta",
                            "numniv", 
                            "ctb_mcuentas",
                            "codemp = "||w_datos.codemp||" AND tipcta='D'",
                            2,
                            1,
                            1)
    RETURNING v_partida[arr].numcta,v_partida[arr].nomcta,regreso 
    IF regreso THEN
       NEXT FIELD numcta
    ELSE 
       -- Asignando descripcion
       DISPLAY v_partida[arr].numcta TO s_partida[scr].numcta 
       DISPLAY v_partida[arr].nomcta TO s_partida[scr].nomcta 
    END IF  
   
   BEFORE INPUT 
    CALL DIALOG.setActionHidden("append",TRUE)
    CALL DIALOG.setActionHidden("delete",TRUE)

   BEFORE ROW
    LET totcta = ARR_COUNT()

   BEFORE FIELD numcta  
    -- Habilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",TRUE)

   AFTER FIELD numcta 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR    
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD numcta 
    END IF

   BEFORE FIELD tipcol 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    IF v_partida[arr].numcta IS NULL THEN 
     -- Seleccionado lista de cuentas contables
     CALL librut002_formlist("Consulta de Cuentas Contables ",
                             "# Cuenta",
                             "Nombre de la Cuenta",
                             "",
                             "numcta",
                             "nomcta",
                             "numniv", 
                             "ctb_mcuentas",
                             "codemp = "||w_datos.codemp||" AND tipcta='D'",
                             2,
                             1,
                             1)
     RETURNING v_partida[arr].numcta,v_partida[arr].nomcta,regreso 
     IF regreso THEN
        NEXT FIELD numcta
     ELSE 
        -- Asignando descripcion
        DISPLAY v_partida[arr].numcta TO s_partida[scr].numcta 
        DISPLAY v_partida[arr].nomcta TO s_partida[scr].nomcta 
     END IF  
    END IF  

    -- Verificando numero de cuenta 
    IF (LENGTH(v_partida[arr].numcta)<=0) THEN 
       CALL fgl_winmessage(
       "Atencion:",
       "Numero de cuenta invalida, VERIFICA.",
       "stop")
       INITIALIZE v_partida[arr].numcta THRU v_partida[arr].tipcol TO NULL
       CLEAR s_partida[scr].numcta,s_partida[scr].nomcta,s_partida[scr].tipcol
       NEXT FIELD numcta 
    END IF 

    -- Verificando si la cuenta existe   
    INITIALIZE w_mae_cta.* TO NULL
    CALL librut003_BCuentaContable(w_datos.codemp,v_partida[arr].numcta)
    RETURNING w_mae_cta.*,existe
    IF NOT existe THEN
       CALL fgl_winmessage(
       "Atencion:",
       "Numero de cuenta no registrada, VERIFICA.",
       "top")
       INITIALIZE v_partida[arr].numcta THRU v_partida[arr].tipcol TO NULL
       CLEAR s_partida[scr].numcta,s_partida[scr].nomcta,s_partida[scr].tipcol
       NEXT FIELD numcta 
    END IF 

    -- Asignando descripcion
    LET v_partida[arr].nomcta = w_mae_cta.nomcta 
    DISPLAY v_partida[arr].nomcta TO s_partida[scr].nomcta 

    -- Verificando que cuenta se de detalle 
    IF w_mae_cta.tipcta="M" THEN 
       CALL fgl_winmessage(
       "Atencion:",
       "Numero de cuenta no es de detalle, VERIFICA.",
       "stop")
       INITIALIZE v_partida[arr].numcta THRU v_partida[arr].tipcol TO NULL
       CLEAR s_partida[scr].numcta,s_partida[scr].nomcta,s_partida[scr].tipcol
       NEXT FIELD numcta
    END IF 

    -- Verificando cuentas repetidas   
    LET repetido = FALSE
    FOR i = 1 TO totcta 
     IF v_partida[i].numcta IS NULL THEN
        CONTINUE FOR
     END IF

     -- Verificando producto
     IF (i!=arr) THEN 
      IF (v_partida[i].numcta = v_partida[arr].numcta) THEN
         LET repetido = TRUE
         EXIT FOR
      END IF 
     END IF
    END FOR

    -- Si hay repetido
    IF repetido THEN
       LET msg = "Cuenta ya registrada en el detalle. "||
                 "Linea (",i USING "<<<",") \nVERIFICA." 
       CALL fgl_winmessage(" Atencion",msg,"stop")
       INITIALIZE v_partida[arr].numcta THRU v_partida[arr].tipcol TO NULL
       CLEAR s_partida[scr].numcta,s_partida[scr].nomcta,s_partida[scr].tipcol
       NEXT FIELD numcta
    END IF

    -- Deshabilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",FALSE)

   AFTER FIELD tipcol
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD tipcol 
    END IF

    -- Verificando tipo de columna 
    IF v_partida[arr].tipcol IS NULL THEN
       NEXT FIELD tipcol 
    END IF 

   AFTER ROW,INSERT  
    LET totcta = ARR_COUNT()

   ON ACTION borrar  
    -- Eliminando linea seleccionada
    LET arr = ARR_CURR() 
    CALL v_partida.deleteElement(arr)
    NEXT FIELD numcta 

   AFTER INPUT 
    LET totcta = ARR_COUNT()
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Desplegando datos 
  DISPLAY ARRAY v_partida TO s_partida.*
   BEFORE DISPLAY 
    EXIT DISPLAY 
  END DISPLAY

  -- Verificando lineas incompletas 
  LET linea = facqbe006_CuentasPolizaIncompletas()
  IF (linea>0) THEN
     LET msg = " Linea ("||linea||") incompleta. \n VERIFICA."
     CALL fgl_winmessage(
     " Atencion",msg,"stop")
     CONTINUE WHILE
  END IF 

  -- Menu de opciones
  LET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  -- Verificando opcion
  CASE (opc)
   WHEN 0 -- Cancelando
    EXIT WHILE                    
   WHEN 1 -- Grabando
    -- Borrando cuentas antes de grabar
    SET LOCK MODE TO WAIT 
    DELETE FROM glb_ctasxrub
    WHERE glb_ctasxrub.codrub = w_mae_pro.codrub 

    -- Grabando cuentas del rubro 
    LET correl = 0 
    FOR i = 1 TO totcta
     IF v_partida[i].numcta IS NULL THEN
        CONTINUE FOR
     END IF 
     LET correl=correl+1 

     -- Grabando cuentas
     SET LOCK MODE TO WAIT
     INSERT INTO glb_ctasxrub 
     VALUES (w_mae_pro.codrub,
             w_datos.codemp,                    
             v_partida[i].numcta, 
             v_partida[i].tipcol,
             correl) 
    END FOR 

    EXIT WHILE 
   WHEN 2 -- Modificando
    CONTINUE WHILE
  END CASE
 END WHILE
END FUNCTION

-- Subrutina para verificar si hay cuentas incompletas

FUNCTION facqbe006_CuentasPolizaIncompletas()
 DEFINE i,linea SMALLINT 

 LET linea = 0 
 FOR i = 1 TO totcta 
  IF v_partida[i].numcta IS NULL THEN
     CONTINUE FOR
  END IF

  -- Verificando lineas
  IF v_partida[i].numcta IS NULL OR
     v_partida[i].nomcta IS NULL OR
     v_partida[i].tipcol IS NULL THEN 
     LET linea = i 
     EXIT FOR 
  END IF 
 END FOR

 RETURN linea 
END FUNCTION

-- Seleccionando cuentas del rubro

FUNCTION facqbe006_CuentasRubro()
 -- Inicializando vector de datos
 CALL v_partida.clear() 

 -- Seleccionando cuentas
 DECLARE c1 CURSOR FOR
 SELECT a.numcta,
        y.nomcta, 
        a.tipcol,
        ""
  FROM  glb_ctasxrub a,ctb_mcuentas y 
  WHERE a.codrub = w_mae_pro.codrub 
    AND y.codemp = a.codemp 
    AND y.numcta = a.numcta
  ORDER BY a.numcor 
  LET totcta = 1 
  FOREACH c1 INTO v_partida[totcta].*
   LET totcta=totcta+1
  END FOREACH
  CLOSE c1
  FREE  c1 
  LET totcta=totcta-1
  
  -- Desplegando datos 
  DISPLAY ARRAY v_partida TO s_partida.*
   BEFORE DISPLAY 
    EXIT DISPLAY 
  END DISPLAY
END FUNCTION 
