{
glbqbe041.4gl 
Mynor Ramirez
Mantenimiento de clientes 
}

{ Definicion de variables globales }

GLOBALS "glbglo041.4gl" 
CONSTANT FACTURAS=1 
DEFINE totlin,totpre,totvta INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION glbqbe041_clientes(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        westado             LIKE fac_clientes.estado, 
        wstatus             LIKE fac_clientes.status,
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,
        msg                 STRING,
        w                   ui.Window,
        f                   ui.Form,
        qrytext,qrypart     STRING

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL glbqbe041_EstadoMenu(operacion," ") 
   INITIALIZE qrytext,qrypart TO NULL
   CALL glbmae041_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codcli,a.nomcli,a.numnit,
                                a.numtel,a.numfax,a.nmovil,
                                a.dircli,a.nompro,a.bemail,
                                a.noruta,a.codseg,a.numlis,
                                a.status,a.estado,a.juridi,
                                a.esempl,a.moncre,a.diacre,
                                a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel 
     -- Salida
     CALL glbmae041_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progres ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codcli,a.nomcli,a.numnit,a.estado,a.status,'' ",
                 " FROM fac_clientes a ",
                 " WHERE ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_clientes SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_clientes.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_clientes INTO v_clientes[totlin].tcodcli THRU 
                           v_clientes[totlin].tnumnit,
                           westado,
                           wstatus

    -- Verficando estado
    IF (westado=0) THEN
       LET v_clientes[totlin].testado = "mars_cancelar.png"
    ELSE
       LET v_clientes[totlin].testado = "mars_cheque.png"
    END IF

    -- Verficando estatus
    IF (wstatus=0) THEN
       LET v_clientes[totlin].tstatus = "mars_bloqueo.png"
    ELSE
       LET v_clientes[totlin].tstatus = "mars_cheque.png"
    END IF

    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_clientes
   FREE  c_clientes
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL glbqbe041_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_clientes TO s_clientes.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL glbmae041_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscar
      CALL glbmae041_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF glbmae041_clientes(2) THEN
         EXIT DISPLAY 
      ELSE
         -- Desplegando datos
         CALL glbqbe041_datos(v_clientes[ARR_CURR()].tcodcli)
      END IF 

     ON KEY (CONTROL-M)   
      -- Modificando 
      IF (operacion=2) THEN 
       IF glbmae041_clientes(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL glbqbe041_datos(v_clientes[ARR_CURR()].tcodcli)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF glbqbe041_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Este cliente ya tiene movimientos. Clientes no puede borrarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Este SEGURO de Borrar este cliente ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL glbmae041_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1]  = "Codigo"
      LET arrcols[2]  = "Nombre"
      LET arrcols[3]  = "Numero de NIT"
      LET arrcols[4]  = "Numero Telefono"
      LET arrcols[5]  = "Numero FAX"
      LET arrcols[6]  = "Numero Celular"
      LET arrcols[7]  = "Direccion"
      LET arrcols[8]  = "Contacto"
      LET arrcols[9]  = "Direccion de Correo"
      LET arrcols[10] = "Ruta"
      LET arrcols[11] = "Segmento"
      LET arrcols[12] = "Lista de Precio"
      LET arrcols[13] = "Dias Credito"
      LET arrcols[14] = "Limite Credito"
      LET arrcols[15] = "Estado Cliente"
      LET arrcols[16] = "Estatus Cliente"
      LET arrcols[17] = "Juridico"
      LET arrcols[18] = "Es Empleado"
      LET arrcols[19] = "Usuario Registro"
      LET arrcols[20] = "Fecha Registro"
      LET arrcols[21] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry  = 
      "SELECT a.codcli,a.nomcli,a.numnit,a.numtel,a.numfax,a.nmovil,",
       "a.dircli,a.nompro,a.bemail,c.nomrut,b.nomseg,a.numlis,a.diacre,a.moncre,",
       "CASE (a.estado) WHEN 1 THEN 'Alta' WHEN 0 THEN 'Baja' END CASE,",
       "CASE (a.status) WHEN 1 THEN 'Disponible' WHEN 0 THEN 'Bloqueado' END CASE,",
       "CASE (a.juridi) WHEN 1 THEN 'Si' WHEN 0 THEN 'No' END CASE,",
       "CASE (a.esempl) WHEN 1 THEN 'Si' WHEN 0 THEN 'No' END CASE,",
       "a.userid,a.fecsis,a.horsis ",
      " FROM fac_clientes a,vta_segmntos b,vta_rutasmer c ",
      " WHERE b.codseg = a.codseg AND c.noruta = a.noruta AND ",qrytext CLIPPED,
      " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res  = librut002_excelreport("Clientes",qry,21,0,arrcols)

     ON ACTION precios
      -- Cargando precios especiales                   
      CALL glbqbe041_CargaPreciosEspeciales(v_clientes[ARR_CURR()].tcodcli)
      -- Ingresando precios especiales del cliente
      CALL glbqbe041_PreciosEspeciales()
 
     ON ACTION ventasdetalle
      -- Visualizando las ventas a detalle 
      CALL glbqbe041_VentasDetalladas(w_mae_pro.codcli,1)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

      -- Verificando permisos 
      IF NOT seclib001_accesos(progname,7,username) THEN
         CALL DIALOG.setActionActive("precios",FALSE)
      END IF 

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
       -- Cargando datos del cliente 
       CALL glbqbe041_datos(v_clientes[ARR_CURR()].tcodcli)

       -- Cargando precios especiales                   
       CALL glbqbe041_CargaPreciosEspeciales(w_mae_pro.codcli)
      END IF 
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen clientes con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Asignando estado del menu
  CALL glbqbe041_EstadoMenu(0,"") 
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION glbqbe041_datos(wcodcli)
 DEFINE wcodcli LIKE fac_clientes.codcli,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  fac_clientes a "||
              "WHERE a.codcli = "||wcodcli||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_clientest SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_clientest INTO w_mae_pro.*
 END FOREACH
 CLOSE c_clientest
 FREE  c_clientest

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nomcli THRU w_mae_pro.esempl 
 DISPLAY BY NAME w_mae_pro.codcli,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION 

-- Subrutina para verificar si el cliente ya tiene movimientos  

FUNCTION glbqbe041_integridad()
 DEFINE conteo INTEGER 

 -- Verificando inventarios
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_mtransac a
  WHERE (a.coddes = w_mae_pro.codcli) 
  IF (conteo>0) THEN
     RETURN TRUE 
  ELSE 
     -- Verificando facturacion 
     SELECT COUNT(*)
      INTO  conteo
      FROM  fac_mtransac a
      WHERE (a.codcli = w_mae_pro.codcli) 
      IF (conteo>0) THEN
         RETURN TRUE
      ELSE
         RETURN FALSE 
      END IF 
  END IF
END FUNCTION 

-- Subrutina para cargas los precios especiales

FUNCTION glbqbe041_CargaPreciosEspeciales(xcodcli)
 DEFINE xcodcli LIKE fac_clientes.codcli 

 -- Inicializando datos
 CALL v_precios.clear() 

 -- Selecionando datos
 DECLARE cprecios CURSOR FOR
 SELECT a.cditem,
        a.codabr,
        p.despro,
        a.preuni, 
        "",
        a.correl 
  FROM  vta_presxcli a,inv_products p
  WHERE (a.codcli = xcodcli)
    AND (p.cditem = a.cditem) 
  ORDER BY a.codabr  
  LET totpre = 1
  FOREACH cprecios INTO v_precios[totpre].*
   LET totpre=totpre+1 
  END FOREACH
  CLOSE cprecios
  FREE cprecios 
  LET totpre=totpre-1 

  -- Desplegando precios
  DISPLAY ARRAY v_precios TO s_precios.*
   BEFORE DISPLAY
    EXIT DISPLAY
  END DISPLAY
END FUNCTION 

-- Subrutinar para el mantenimiento de precios especiales por cliente 

FUNCTION glbqbe041_PreciosEspeciales()
 DEFINE w_mae_art       RECORD LIKE inv_products.*,
        i,j,opc,correl  SMALLINT, 
        arr,scr,existe  SMALLINT,
        regreso         SMALLINT, 
        repetido        SMALLINT,
        msg             STRING, 
        lastkey         INT 

 -- Desplegando nombre del cliente
 CALL librut001_dpelement("Cliente "||w_mae_pro.nomcli CLIPPED,"labpre") 

 -- Ingresando productos 
 INPUT ARRAY v_precios WITHOUT DEFAULTS FROM s_precios.*
  ATTRIBUTE(COUNT=totpre,INSERT ROW=FALSE,ACCEPT=FALSE,
            CANCEL=FALSE,UNBUFFERED=TRUE,
            FIELD ORDER FORM)

  ON ACTION cancel
   -- Salir
   CALL v_precios.clear() 
   EXIT INPUT

  ON ACTION accept
   -- Chequeando si hay productos
   LET j = 0 
   FOR i = 1 TO totpre 
    IF v_precios[i].scodabr IS NOT NULL THEN
       LET j=j+1 
    END IF 
   END FOR 
   IF j=0 THEN
      CALL fgl_winmessage(
      " Atencion:",
      " Debe ingresarse al menos un producto, VERIFICA.",
      "stop")
      CONTINUE INPUT 
   END IF 

   -- Chequeando lineas incompletas
   LET j = 0
   FOR i = 1 TO totpre 
    IF v_precios[i].scodabr IS NOT NULL THEN
     IF v_precios[i].spreuni IS NULL THEN 
        LET j = i
        EXIT FOR
     END IF 
    END IF 
   END FOR 
   IF j>0 THEN
      CALL fgl_winmessage(
      " Atencion:",
      " Producto incompleto en linea "||j||".",
      "stop")
      CONTINUE INPUT 
   END IF 

   -- Grabar
   LET opc = librut001_menugraba("Confirmacion",
                                 "Que desea hacer?",
                                 "Guardar",
                                 "Modificar",
                                 "Cancelar",
                                 "")
  
   -- Verificando opcion  
   CASE (opc)
    WHEN 0 -- Salir sin guardar
           CALL v_precios.clear() 
           EXIT INPUT 
    WHEN 2 -- Modificar 
           CONTINUE INPUT
   END CASE 

   -- Iniciando transaccion
   BEGIN WORK

    -- Borrando antes de grabar
    SET LOCK MODE TO WAIT
    DELETE FROM vta_presxcli 
    WHERE (vta_presxcli.codcli = w_mae_pro.codcli)

    -- Grabando precios por producto 
    LET correl = 0 
    FOR i = 1 TO totpre 
     IF v_precios[i].scodabr IS NULL THEN
        CONTINUE FOR
     END IF
     LET correl = (correl+1) 
    
     -- Grabando 
     SET LOCK MODE TO WAIT
     INSERT INTO vta_presxcli 
     VALUES (0,                    -- ID unico del precio especial
             w_mae_pro.codcli,     -- Codigo del cliente 
             v_precios[i].scditem, -- Codigo del producto 
             v_precios[i].scodabr, -- Codigo de barras
             correl,               -- Correlativo de linea
             v_precios[i].spreuni, -- Precio unitario del producto 
             USER,                 -- Usuario registro precio 
             CURRENT,              -- Fecha registro precio 
             CURRENT)              -- Hora registro precio  
    END FOR 

   -- Finalizando transaccion
   COMMIT WORK 

   EXIT INPUT 

  BEFORE INPUT
   -- Deshabilitando tecla de append
   CALL Dialog.SetActionHidden("append",1) 

  ON ACTION listproducto
   LET arr = ARR_CURR()
   LET scr = SCR_LINE()

   -- Seleccionado lista de productos
   CALL librut002_formlist("Consulta de Productos",
                           "Producto",
                           "Descripcion del Producto",
                           "Precio Lista",
                           "codabr",
                           "despro",
                           "presug",
                           "inv_products",
                           "estado=1 and status=1",
                           2,
                           1,
                           1)
   RETURNING v_precios[arr].scodabr,v_precios[arr].sdespro,regreso
   IF regreso THEN
      NEXT FIELD scodabr
   ELSE
      -- Desplegando datos
      DISPLAY v_precios[arr].scodabr TO s_precios[scr].scodabr 
      DISPLAY v_precios[arr].sdespro TO s_precios[scr].sdespro
   END IF

  BEFORE FIELD scodabr 
   -- Habilitando tecla de grabar
   CALL Dialog.SetActionActive("accept",1) 

  AFTER FIELD scodabr 
   LET arr = ARR_CURR() 
   LET scr = SCR_LINE() 

   -- Verificando ultima tecla presionada
   LET lastkey = FGL_LASTKEY()
   IF (lastkey = FGL_KEYVAL("down")) THEN
      NEXT FIELD scodabr 
   END IF

  BEFORE FIELD spreuni 
   LET arr = ARR_CURR() 
   LET scr = SCR_LINE() 

   -- Verificando producto
   IF v_precios[arr].scodabr IS NULL THEN
      ERROR "Error: producto invalido, VERIFICA." 
      NEXT FIELD scodabr
   END IF 

   -- Verificando si el producto existe
   INITIALIZE w_mae_art.* TO NULL
   CALL librut003_BProductoAbr(v_precios[arr].scodabr) 
   RETURNING w_mae_art.*,existe
   IF NOT existe THEN 
      CALL fgl_winmessage(
      "Atencion:",
      "Producto no existe registrado, VERIFICA.",
      "stop") 
      INITIALIZE v_precios[arr].* TO NULL 
      CLEAR s_precios[scr].*  
      NEXT FIELD scodabr
   END IF  
   LET v_precios[arr].scditem = w_mae_art.cditem
   LET v_precios[arr].sdespro = w_mae_art.despro 
   DISPLAY v_precios[arr].sdespro TO s_precios[scr].sdespro 

   -- Chequeando productos repetidos
   LET repetido = FALSE
   FOR i = 1 TO totlin
    IF v_precios[i].scodabr IS NULL THEN
       CONTINUE FOR
    END IF

    -- Verificando producto
    IF (i!=arr) THEN
     IF (v_precios[i].scodabr = v_precios[arr].scodabr) THEN
        LET repetido = TRUE
        EXIT FOR
     END IF
    END IF
   END FOR

   -- Si hay repetido
   IF repetido THEN
      LET msg = "Producto ya registrado en el detalle. "||
                "Linea (",i USING "<<<",") \nVERIFICA."
      CALL fgl_winmessage(" Atencion",msg,"stop")
      INITIALIZE v_precios[arr].* TO NULL 
      CLEAR s_precios[scr].*  
      NEXT FIELD scodabr
   END IF

   -- Desabilitando tecla de grabar
   CALL Dialog.SetActionActive("accept",0) 

  AFTER FIELD spreuni 
   LET arr = ARR_CURR() 
   LET scr = SCR_LINE() 

   -- Verificando ultima tecla presionada
   LET lastkey = FGL_LASTKEY()
   IF (lastkey = FGL_KEYVAL("down")) THEN
      NEXT FIELD spreuni 
   END IF

   -- Verificando precio 
   IF v_precios[arr].spreuni IS NULL OR 
      v_precios[scr].spreuni <0 THEN
      ERROR "Error: precio invalido, VERIFICA."
      NEXT FIELD spreuni 
   END IF 

  BEFORE ROW 
   -- Asignando total de filas
   LET totpre = ARR_COUNT() 

  AFTER ROW,INSERT,DELETE
   -- Asignando total de filas
   LET totpre = ARR_COUNT() 

  AFTER INPUT 
   -- Asignando total de filas
   LET totpre = ARR_COUNT() 
 END INPUT 
END FUNCTION 

-- Subrutina para cargas las ventas detalladas   

FUNCTION glbqbe041_VentasDetalladas(xcodcli,operacion)
 DEFINE fx              ui.Form,
        xcodcli         LIKE fac_clientes.codcli,
        vtotcan         LIKE fac_dtransac.cantid, 
        vtotval         LIKE fac_dtransac.totpro, 
        loop,operacion  SMALLINT,
        idsitem         CHAR(100), 
        ifecini,ifecfin DATE

 -- Asignando periodo default
 LET ifecini = TODAY-30 
 LET ifecfin = TODAY
 LET idsitem  = "*" 

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Inicializando datos
  CALL v_ventas.clear() 
  LET totvta = 1 

  -- Verificando seleccion de clientes
  IF LENGTH(idsitem)=0 THEN
     LET idsitem = "*"
  END IF 

  -- Selecionando datos
  DECLARE cventas CURSOR FOR
  SELECT a.fecemi,
         a.nserie,
         a.numdoc, 
         y.cditem,
         y.codabr,
         p.despro,
         y.cantid,
         y.preuni, 
         y.totpro, 
         ""
   FROM  fac_mtransac a,fac_dtransac y,inv_products p
   WHERE (a.lnktra = y.lnktra)
     AND (a.tipdoc = FACTURAS) 
     AND (a.fecemi >=ifecini)
     AND (a.fecemi <=ifecfin)
     AND (a.codcli = xcodcli)
     AND (a.estado = "V") 
     AND (p.cditem = y.cditem) 
     AND (p.dsitem MATCHES idsitem) 
   ORDER BY a.fecemi DESC,y.codabr 

   LET vtotcan = 0
   LET vtotval = 0 
   FOREACH cventas INTO v_ventas[totvta].*
    LET vtotcan=vtotcan+v_ventas[totvta].vcanuni  
    LET vtotval=vtotval+v_ventas[totvta].vtotpro  
    LET totvta=totvta+1 
   END FOREACH
   CLOSE cventas 
   FREE cventas  
   LET totvta=totvta-1 

  -- Ingresando periodo de fechas 
  DISPLAY BY NAME ifecini,ifecfin,vtotcan,vtotval 
  DIALOG ATTRIBUTES(UNBUFFERED,FIELD ORDER FORM)

     -- Desplegando ventas 
     DISPLAY ARRAY v_ventas TO s_ventas.* 
      ATTRIBUTE(COUNT=totvta) 
    
      BEFORE DISPLAY 
       -- Verificando operacion 
       IF (operacion=0) THEN
          -- Regresando a datos generales 
          LET loop = FALSE 
          EXIT DIALOG 
       END IF 
        
       CALL DIALOG.setActionHidden("close",TRUE)
     END DISPLAY
 
     -- Seleccionando parametros
     INPUT BY NAME ifecini,ifecfin,idsitem
      ATTRIBUTE(WITHOUT DEFAULTS=TRUE) 
     END INPUT 

     ON ACTION cancel
      -- Salida 
      -- Regresando a datos generales 
      LET fx = DIALOG.getForm()
      CALL fx.ensureFieldVisible("formonly.codcli")
      LET loop = FALSE 
      EXIT DIALOG 
  END DIALOG 
 END WHILE 

 -- Inicializando datos
 CALL v_ventas.clear() 
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION glbqbe041_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Clientes - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Clientes - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Clientes - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Clientes - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Clientes - NUEVO")
 END CASE
END FUNCTION
