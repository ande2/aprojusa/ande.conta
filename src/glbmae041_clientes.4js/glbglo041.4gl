{ 
glbglo041.4gl
Mynor Ramirez
Mantenimiento de clientes
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "glbmae041"
DEFINE w_mae_pro   RECORD LIKE fac_clientes.*,
       v_clientes  DYNAMIC ARRAY OF RECORD
        tcodcli    LIKE fac_clientes.codcli,
        tnomcli    LIKE fac_clientes.nomcli, 
        tnumnit    LIKE fac_clientes.numnit,
        testado    CHAR(20),
        tstatus    CHAR(20),
        tendrec    CHAR(1) 
       END RECORD, 
       v_precios   DYNAMIC ARRAY OF RECORD
        scditem    LIKE inv_dproduct.cditem,
        scodabr    LIKE inv_dproduct.codabr,
        sdespro    LIKE inv_products.despro,
        spreuni    DEC(12,6),
        srellen    CHAR(1)
       END RECORD, 
       v_ventas    DYNAMIC ARRAY OF RECORD
        vfecemi    LIKE fac_mtransac.fecemi,
        vnserie    LIKE fac_mtransac.nserie,
        vnumdoc    LIKE fac_mtransac.numdoc,
        vcditem    LIKE inv_dproduct.cditem,
        vcodabr    LIKE inv_dproduct.codabr,
        vdespro    LIKE inv_products.despro,
        vcanuni    LIKE fac_dtransac.cantid, 
        vpreuni    LIKE fac_dtransac.preuni,
        vtotpro    LIKE fac_dtransac.totpro,
        vrellen    CHAR(1)
       END RECORD, 
       username    VARCHAR(15)
END GLOBALS
