


drop trigger "sistemas".trgupdateclientes; 
create trigger "sistemas".trgupdateclientes update on "sistemas"
    .fac_clientes referencing old as pre new as pos
    for each row
        (
        insert into "sistemas".his_clientes (lnkcli,codcli,nomcli,
    numnit,numtel,numfax,dircli,nompro,bemail,status,estado,userid,
    fecsis,horsis,usract,fecact,horact)  values 
    (0 ,pre.codcli ,pre.nomcli ,pre.numnit ,pre.numtel ,pre.numfax ,pre.dircli 
    ,pre.nompro ,pre.bemail , pre.status ,pre.estado ,pre.userid ,pre.fecsis ,
     pre.horsis ,USER ,CURRENT year to fraction(3) ,CURRENT hour to second ));


