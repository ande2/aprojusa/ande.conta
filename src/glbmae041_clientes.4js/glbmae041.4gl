{
Mantenimiento de clientes 
glbmae041.4gl 
MRS
Enero 2011 
}

-- Definicion de variables globales 

GLOBALS "glbglo041.4gl"
DEFINE existe SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/ToolBarClientes")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("clientes")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo usuario del sistema 
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de principal 
 CALL glbmae041_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION glbmae041_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255),
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "glbmae041a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header("glbmae041",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox de segmentos
  CALL librut003_CbxRutas() 
  CALL librut003_CbxSegmentos() 
  CALL librut003_CbxListasPrecio() 

  -- Menu de opciones
  MENU "Clientes"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,4,username) THEN
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Modificar"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    "Busqueda de clientes."
    CALL glbqbe041_clientes(1) 
   COMMAND "Nuevo"
    "Ingreso de un nuevo cliente."
    LET savedata = glbmae041_clientes(1) 
   COMMAND "Modificar"
    "Modificacion de un cliente existente."
    CALL glbqbe041_clientes(2) 
   COMMAND "Borrar"
    "Eliminacion de un cliente existente."
    CALL glbqbe041_clientes(3) 
   COMMAND "Salir"
    "Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION glbmae041_clientes(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL glbqbe041_EstadoMenu(4,"")
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL glbmae041_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nomcli,
                w_mae_pro.numnit,
                w_mae_pro.numtel,
                w_mae_pro.numfax,
                w_mae_pro.nmovil,
                w_mae_pro.dircli,
                w_mae_pro.nompro,
                w_mae_pro.bemail,
                w_mae_pro.noruta,
                w_mae_pro.codseg,
                w_mae_pro.numlis, 
                w_mae_pro.status, 
                w_mae_pro.estado, 
                w_mae_pro.juridi, 
                w_mae_pro.esempl, 
                w_mae_pro.moncre, 
                w_mae_pro.diacre
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   BEFORE INPUT
    -- Verificando si hay acceso a campos especiales
    IF NOT seclib001_accesos(progname,6,username) THEN
       CALL Dialog.SetFieldActive("estado",0)
       CALL Dialog.SetFieldActive("status",0)
       CALL Dialog.SetFieldActive("juridi",0)
       CALL Dialog.SetFieldActive("esempl",0)
       CALL Dialog.SetFieldActive("moncre",0)
       CALL Dialog.SetFieldActive("diacre",0)
    ELSE 
       CALL Dialog.SetFieldActive("estado",1)
       CALL Dialog.SetFieldActive("status",1)
       CALL Dialog.SetFieldActive("juridi",1)
       CALL Dialog.SetFieldActive("esempl",1)
       CALL Dialog.SetFieldActive("moncre",1)
       CALL Dialog.SetFieldActive("diacre",1)
    END IF

   AFTER FIELD nomcli  
    --Verificando nombre del cliente
    IF (LENGTH(w_mae_pro.nomcli)=0) THEN
       ERROR "Error: nombre del cliente invalido, VERIFICA."
       LET w_mae_pro.nomcli = NULL
       NEXT FIELD nomcli  
    END IF

    -- Verificando que no exista otro cliente con el mismo nombre
    SELECT UNIQUE (a.codcli)
     FROM  fac_clientes a
     WHERE (a.codcli != w_mae_pro.codcli) 
       AND (a.nomcli  = w_mae_pro.nomcli) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion",
        " Existe otro cliente con el mismo nombre, VERIFICA.",
        "information")
        NEXT FIELD nomcli
     END IF 
   AFTER FIELD numnit  
    --Verificando numero de registro tributario 
    IF (LENGTH(w_mae_pro.numnit)=0) THEN
       LET w_mae_pro.numnit = "C/F"
       DISPLAY BY NAME w_mae_pro.numnit 
    END IF

    -- Verificando que no exista otro registro tributario 
    SELECT UNIQUE (a.numnit)
     FROM  fac_clientes a
     WHERE (a.codcli != w_mae_pro.codcli) 
       AND (a.numnit  = w_mae_pro.numnit) 
       AND (a.numnit != "C/F")
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion",
        " Existe otro cliente con el mismo numero de registro tributario, VERIFICA.",
        "information")
        NEXT FIELD numnit
     END IF 

   AFTER FIELD dircli
    --Verificando direccion
    IF (LENGTH(w_mae_pro.dircli)=0) THEN
       ERROR "Error: direccion invalida, VERIFICA."
       LET w_mae_pro.dircli = NULL
       NEXT FIELD dircli  
    END IF

   AFTER FIELD noruta 
    --Verificando numero de ruta 
    IF w_mae_pro.noruta IS NULL THEN
       ERROR "Error: numero de ruta invalido, VERIFICA."
       NEXT FIELD noruta  
    END IF

   AFTER FIELD codseg 
    --Verificando segmento                         
    IF w_mae_pro.codseg IS NULL THEN
       ERROR "Error: segmento invalido, VERIFICA."
       NEXT FIELD codseg 
    END IF

   AFTER FIELD numlis  
    --Verificando numero de lista
    IF w_mae_pro.numlis IS NULL THEN
       ERROR "Error: numero de lista invalido, VERIFICA."
       NEXT FIELD numlis 
    END IF

   AFTER FIELD estado
    --Verificando estado del cliente
    IF w_mae_pro.estado IS NULL THEN
       ERROR "Error: estado del cliente invalido, VERIFICA."
       NEXT FIELD estado 
    END IF

   AFTER FIELD status
    --Verificando estatus del cliente
    IF w_mae_pro.status IS NULL THEN
       ERROR "Error: estatus del cliente invalido, VERIFICA."
       NEXT FIELD status 
    END IF

   AFTER FIELD juridi 
    --Verificando juridico 
    IF w_mae_pro.juridi IS NULL THEN
       ERROR "Error: cliente en juridico invalido, VERIFICA."
       NEXT FIELD juridi 
    END IF

   AFTER FIELD esempl 
    --Verificando es empleado 
    IF w_mae_pro.esempl IS NULL THEN
       ERROR "Error: cliente es empleado invalido, VERIFICA."
       NEXT FIELD esempl 
    END IF

   AFTER FIELD moncre 
    --Verificando limite de credito 
    IF w_mae_pro.moncre IS NULL OR
       w_mae_pro.moncre <0 THEN 
       ERROR "Error: limite de credito invalido, VERIFICA."
       NEXT FIELD moncre 
    END IF

   AFTER FIELD diacre 
    --Verificando dias de credito
    IF w_mae_pro.diacre IS NULL OR
       w_mae_pro.diacre <0 THEN 
       ERROR "Error: dias de credito invalido, VERIFICA."
       NEXT FIELD diacre 
    END IF

   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.nomcli IS NULL THEN 
       NEXT FIELD nomcli
    END IF
    IF w_mae_pro.numnit IS NULL THEN 
       NEXT FIELD numnit
    END IF
    IF w_mae_pro.dircli IS NULL THEN 
       NEXT FIELD dircli
    END IF
    IF w_mae_pro.noruta IS NULL THEN 
       NEXT FIELD noruta 
    END IF
    IF w_mae_pro.codseg IS NULL THEN 
       NEXT FIELD codseg 
    END IF
    IF w_mae_pro.numlis IS NULL THEN 
       NEXT FIELD numlis 
    END IF
    IF w_mae_pro.status IS NULL THEN 
       NEXT FIELD status 
    END IF
    IF w_mae_pro.estado IS NULL THEN 
       NEXT FIELD estado
    END IF
    IF w_mae_pro.juridi IS NULL THEN 
       NEXT FIELD juridi 
    END IF
    IF w_mae_pro.esempl IS NULL THEN 
       NEXT FIELD esempl 
    END IF
    IF w_mae_pro.moncre IS NULL THEN 
       NEXT FIELD moncre
    END IF
    IF w_mae_pro.diacre IS NULL THEN 
       NEXT FIELD diacre
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  LET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL glbmae041_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando cliente
    CALL glbmae041_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL glbqbe041_EstadoMenu(0,"")
    CALL glbmae041_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un cliente

FUNCTION glbmae041_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando cliente ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO fac_clientes   
   VALUES (w_mae_pro.*)
   LET w_mae_pro.codcli = SQLCA.SQLERRD[2] 
   DISPLAY BY NAME w_mae_pro.codcli 

   --Asignando el mensaje 
   LET msg = "Clientes (",w_mae_pro.codcli USING "<<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE fac_clientes
   SET    fac_clientes.*      = w_mae_pro.*
   WHERE  fac_clientes.codcli = w_mae_pro.codcli 

   --Asignando el mensaje 
   LET msg = "Clientes (",w_mae_pro.codcli USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando clientes
   DELETE FROM fac_clientes 
   WHERE (fac_clientes.codcli = w_mae_pro.codcli)

   --Asignando el mensaje 
   LET msg = "Clientes (",w_mae_pro.codcli USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL glbmae041_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION glbmae041_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.codcli = 0 
   LET w_mae_pro.moncre = 0 
   LET w_mae_pro.diacre = 0 
   LET w_mae_pro.estado = 1 
   LET w_mae_pro.status = 1 
   LET w_mae_pro.juridi = 0 
   LET w_mae_pro.esempl = 0 
   LET w_mae_pro.userid = username 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codcli,w_mae_pro.nomcli THRU w_mae_pro.esempl 
 DISPLAY BY NAME w_mae_pro.codcli,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION
