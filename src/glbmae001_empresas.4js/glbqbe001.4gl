{
glbqbe001.4gl 
MRS 
Mantenimiento de empresas 
}

{ Definicion de variables globales }

GLOBALS "glbglo001.4gl" 
DEFINE totlin,totusr INT,
       w             ui.Window,
       f             ui.Form


-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION glbqbe001_empresas(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,
        msg                 STRING

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando comboboxes 
  CALL librut003_CbxPaises() 
  CALL librut003_CbxDepartamentosEmpresa()

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL glbqbe001_EstadoMenu(operacion," ")
   INITIALIZE qrytext,qrypart TO NULL
   CALL glbmae001_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codemp,a.nomemp,a.nomabr,
                                a.numnit,a.numtel,a.numfax,
                                a.diremp,a.zondir,a.codpai,
                                a.coddep,a.tipnom,a.userid,
                                a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel
     -- Salida
     CALL glbmae001_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codemp,a.nomemp,a.numnit ",
                 " FROM glb_empresas a,glb_mtpaises b,glb_departos c ",
                 " WHERE b.codpai = a.codpai AND c.coddep = a.coddep ",
                 "   AND ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_empresas SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_empresas.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_empresas INTO v_empresas[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_empresas
   FREE  c_empresas
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL glbqbe001_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_empresas TO s_empresas.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel
      -- Salida
      CALL glbmae001_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscar 
      CALL glbmae001_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF glbmae001_empresas(2) THEN
         EXIT DISPLAY 
      ELSE
         -- Desplegando datos
         CALL glbqbe001_datos(v_empresas[ARR_CURR()].tcodemp)
         CALL glbqbe001_permxusr(v_empresas[ARR_CURR()].tcodemp,1)
      END IF 

     ON KEY (CONTROL-M) 
      -- Modificando 
      IF (operacion=2) THEN 
       IF glbmae001_empresas(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL glbqbe001_datos(v_empresas[ARR_CURR()].tcodemp)
          CALL glbqbe001_permxusr(v_empresas[ARR_CURR()].tcodemp,1)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      LET res = glbqbe001_integridad()
      IF (res>0) THEN  
         LET msg = " Esta empresa ya tiene registros. \n Empresa no puede borrarse."
         CALL fgl_winmessage(
         " Atencion",msg,"stop")
         CONTINUE DISPLAY 
      END IF 

      -- Confirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de Borrar esta empresa ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL glbmae001_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION accesos
      IF (totusr=0) THEN
         ERROR "Atencion: no existen usuarios con accesos autorizados."
      END IF

      -- Desplegando accesos x usuario
      CALL glbqbe001_detpermxusr(v_empresas[ARR_CURR()].tcodemp)

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1]  = "Empresa"
      LET arrcols[2]  = "Nombre"
      LET arrcols[3]  = "Nombre Anreviado" 
      LET arrcols[4]  = "Numero de NIT" 
      LET arrcols[5]  = "Numero Telefono" 
      LET arrcols[6]  = "Numero FAX" 
      LET arrcols[7]  = "Direccion" 
      LET arrcols[8]  = "Zona de la Direccion" 
      LET arrcols[9]  = "Pais" 
      LET arrcols[10] = "Departamento" 
      LET arrcols[11] = "Tipo Nomenclatura" 
      LET arrcols[12] = "Usuario Registro"
      LET arrcols[13] = "Fecha Registro"
      LET arrcols[14] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry ="SELECT a.codemp,a.nomemp,a.nomabr,",
                     "\"'\"||a.numnit,a.numtel,",
                     "a.numfax,a.diremp,a.zondir,b.nompai,c.nomdep,",
                     "\"'\"||d.format,a.userid,a.fecsis,a.horsis",
               " FROM glb_empresas a,glb_mtpaises b,glb_departos c,ctb_tiposnom d ",
               " WHERE b.codpai = a.codpai ",
                  "AND c.coddep = a.coddep ",
                  "AND d.tipnom = a.tipnom ",
                  "AND ",qrytext CLIPPED,
               " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res        = librut002_excelreport("Empresas",qry,13,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE 
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

         -- Verificando acceso a opcion de accesos
         IF NOT seclib001_accesos(progname,6,username) THEN
            CALL DIALOG.setActionActive("accesos",FALSE)
         ELSE
            CALL DIALOG.setActionActive("accesos",TRUE)
         END IF

       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("accesos",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("accesos",TRUE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
         CALL glbqbe001_datos(v_empresas[ARR_CURR()].tcodemp)
         CALL glbqbe001_permxusr(v_empresas[ARR_CURR()].tcodemp,1)
      END IF 

    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen empresas con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Desplegando estado del menu
  CALL glbqbe001_EstadoMenu(0,"")
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION glbqbe001_datos(wcodemp)
 DEFINE wcodemp LIKE glb_empresas.codemp,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  glb_empresas a "||
              "WHERE a.codemp = "||wcodemp||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_empresast SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_empresast INTO w_mae_pro.*
 END FOREACH
 CLOSE c_empresast
 FREE  c_empresast

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nomemp THRU w_mae_pro.empctb                        
 DISPLAY BY NAME w_mae_pro.codemp,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION 

-- Subrutina para verificar si la empresa ya tiene registros 

FUNCTION glbqbe001_integridad()
 DEFINE conteo INTEGER 

 -- Verificando cuentas contables 
 SELECT COUNT(*)
  INTO  conteo
  FROM  ctb_mtransac a
  WHERE (a.codemp = w_mae_pro.codemp)
  IF (conteo>0) THEN
     RETURN 1
  ELSE
     -- Verificando bancos    
     SELECT COUNT(*)
      INTO  conteo
      FROM  bco_mtransac a
      WHERE (a.codemp = w_mae_pro.codemp)
      IF (conteo>0) THEN
         RETURN 2
      ELSE
         -- Verificando compras 
         SELECT COUNT(*)
          INTO  conteo
          FROM  cmp_mtransac a
          WHERE (a.codemp = w_mae_pro.codemp)
          IF (conteo>0) THEN
             RETURN 3
          ELSE
             -- Verificando compras 
             SELECT COUNT(*)
              INTO  conteo
              FROM  ctb_mtransac a
              WHERE (a.codemp = w_mae_pro.codemp)
              IF (conteo>0) THEN 
                 RETURN 4 
              ELSE 
                 RETURN 0
              END IF 
          END IF
      END IF
  END IF
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION glbqbe001_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Empresas - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Empresas - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Empresas - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Empresas - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Empresas - NUEVO")
 END CASE
END FUNCTION

-- Subrutina para buscar los accesos a usuarios

FUNCTION glbqbe001_permxusr(wcodemp,opcion)
 DEFINE wcodemp   LIKE glb_empresas.codemp,
        i,opcion  SMALLINT,
        qrytext   STRING 

 -- Inicializando vector de datos
 CALL v_permxusr.clear()

 -- Seleccionando datos
 CASE (opcion)
  WHEN 1 -- Consulta de usuarios con acceso
   LET qrytext = "SELECT 1,a.userid,b.nomusr,a.usuaid,a.fecsis,a.horsis "||
                  "FROM  glb_permxemp a, glb_usuarios b " ||
                  "WHERE a.codemp = "||wcodemp||
                   " AND b.userid = a.userid "||
                   " ORDER BY a.fecsis DESC " 

  WHEN 2 -- Cargando usuarios para seleccionar acceso
   LET qrytext = "SELECT 0,a.userid,a.nomusr,USER,CURRENT,CURRENT HOUR TO SECOND "||
                  "FROM  glb_usuarios a " ||
                  "WHERE NOT exists (SELECT b.userid FROM glb_permxemp b "||
                                     "WHERE b.codemp = "||wcodemp||
                                      " AND b.userid = a.userid) "||
                   "ORDER BY 2" 
 END CASE 
  
 -- Seleccionando datos
 PREPARE cp1 FROM qrytext 
 DECLARE cperm CURSOR FOR cp1 
 LET totusr = 1
 FOREACH cperm INTO v_permxusr[totusr].* 
  LET totusr = (totusr+1) 
 END FOREACH
 CLOSE cperm
 FREE  cperm
 LET totusr = (totusr-1) 

 -- Desplegando datos
 DISPLAY ARRAY v_permxusr TO s_permxusr.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY
END FUNCTION 

-- Subrutina para visualizar el detalle de accesos a usuarios

FUNCTION glbqbe001_detpermxusr(wcodemp)
 DEFINE wcodemp    LIKE glb_empresas.codemp,
        loop,opc,i SMALLINT

 -- Desplegando usuarios
 LET loop = TRUE
 WHILE loop
  DISPLAY ARRAY v_permxusr TO s_permxusr.*
   ATTRIBUTE(COUNT=totusr,ACCEPT=FALSE,CANCEL=FALSE)
   ON ACTION cancel  
    -- Salida
    LET loop = FALSE
    EXIT DISPLAY 

   ON ACTION agregarusers
    -- Agregando usuarios 
    CALL glbqbe001_permxusr(wcodemp,2) 
    IF (totusr=0) THEN
        CALL fgl_winmessage(
        " Atencion",
        " No existen mas usuarios para agregar.\n Todos los usuarios ya tienen acceso.", 
        "stop")

       -- Cargando usuarios con accesos
       CALL glbqbe001_permxusr(wcodemp,1) 
       EXIT DISPLAY 
    ELSE  
      -- Mostrando campo de seleccion de accesos de usuario
      CALL f.setFieldHidden("tcheckb",0)

      -- Seleccionando usuarios
      INPUT ARRAY v_permxusr WITHOUT DEFAULTS FROM s_permxusr.*
       ATTRIBUTE(MAXCOUNT=totusr,INSERT ROW=FALSE,
                 APPEND ROW=FALSE,DELETE ROW=FALSE,
                 ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
       ON ACTION cancel
        -- Salida
        EXIT INPUT 

       ON ACTION accept
        -- Verificando si existen usuarios que agregar
        IF NOT glbqbe001_hayusuarios(1) THEN
           ERROR "Atencion: no existen usuarios marcados que agregar."
           CONTINUE INPUT
        END IF

        -- Grabando usuarios seleccionados
        LET opc = librut001_menugraba("Confirmacion de Accesos",
                                      "Que desea hacer ?",
                                      "Guardar",
                                      "Modificar",
                                      "Cancelar",
                                      NULL)

        CASE (opc)
         WHEN 0 -- Cancelando
          EXIT INPUT 
         WHEN 1 -- Grabando
          -- Iniciando la transaccion
          BEGIN WORK 

          -- Guardando accesos
          FOR i = 1 TO totusr
           IF (v_permxusr[i].tcheckb=0) OR
              (v_permxusr[i].tcheckb IS NULL) THEN 
              CONTINUE FOR
           END IF 

           -- Grabando  
           SET LOCK MODE TO WAIT
           INSERT INTO glb_permxemp 
           VALUES (wcodemp,
                   v_permxusr[i].tuserid, 
                   v_permxusr[i].tusuaid, 
                   v_permxusr[i].tfecsis, 
                   v_permxusr[i].thorsis)
          END FOR

          -- Terminando la transaccion
          COMMIT WORK 

          EXIT INPUT 
         WHEN 2 -- Modificando
          CONTINUE INPUT 
        END CASE

       BEFORE ROW
        -- Verificando control del total de lineas 
        IF (ARR_CURR()>totusr) THEN
           CALL FGL_SET_ARR_CURR(1)
        END IF 
      END INPUT 

      -- Escondiendo campo de seleccion de accesos de usuario
      CALL f.setFieldHidden("tcheckb",1)

      -- Cargando usuarios con accesos
      CALL glbqbe001_permxusr(wcodemp,1) 
      EXIT DISPLAY 
    END IF 

   ON ACTION eliminarusers
    IF (totusr=0) THEN
       ERROR "Atencion: no existen usuarios con accesos autorizados que borrar."
       EXIT DISPLAY 
    END IF  

    -- Eliminando usuarios 
    -- Mostrando campo de seleccion de accesos de usuario
    CALL f.setFieldHidden("tcheckb",0)

    -- Seleccionando usuarios
    INPUT ARRAY v_permxusr WITHOUT DEFAULTS FROM s_permxusr.*
     ATTRIBUTE(MAXCOUNT=totusr,INSERT ROW=FALSE,
               APPEND ROW=FALSE,DELETE ROW=FALSE,
               ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
     ON ACTION cancel
      -- Salida
      EXIT INPUT 

     ON ACTION accept   
      -- Verificando si existen usuarios que borrar
      IF NOT glbqbe001_hayusuarios(0) THEN
         ERROR "Atencion: no existen usuarios desmarcados que borrar."
         CONTINUE INPUT
      END IF

      -- Grabando usuarios seleccionados
      LET opc = librut001_menugraba("Confirmacion de Accesos",
                                    "Que desea hacer ?",
                                    "Borrar",
                                    "Modificar",
                                    "Cancelar",
                                    NULL)

      CASE (opc)
       WHEN 0 -- Cancelando
        EXIT INPUT 
       WHEN 1 -- Grabando
        -- Iniciando la transaccion
        BEGIN WORK 

        -- Guardando accesos
        FOR i = 1 TO totusr
         IF (v_permxusr[i].tcheckb=1) OR 
            (v_permxusr[i].tcheckb IS NULL) THEN 
            CONTINUE FOR
         END IF 

         -- Eliminando  
         SET LOCK MODE TO WAIT
         DELETE FROM glb_permxemp 
         WHERE glb_permxemp.codemp = wcodemp
           AND glb_permxemp.userid = v_permxusr[i].tuserid 
        END FOR

        -- Terminando la transaccion
        COMMIT WORK 

        EXIT INPUT 
       WHEN 2 -- Modificando
        CONTINUE INPUT 
      END CASE

     BEFORE ROW
      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totusr) THEN
         CALL FGL_SET_ARR_CURR(1)
      END IF 
    END INPUT 

    -- Escondiendo campo de seleccion de accesos de usuario
    CALL f.setFieldHidden("tcheckb",1)

    -- Cargando usuarios con accesos
    CALL glbqbe001_permxusr(wcodemp,1) 
    EXIT DISPLAY 

   BEFORE ROW 
    -- Verificando control del total de lineas 
    IF (ARR_CURR()>totusr) THEN
       CALL FGL_SET_ARR_CURR(1)
    END IF 
  END DISPLAY 
 END WHILE 
END FUNCTION 

-- Subrutina para verificar si existen usuarios seleccionados que agregar o que borrar

FUNCTION glbqbe001_hayusuarios(estado)
 DEFINE i,estado,hayusr SMALLINT

 -- Chequeando estado
 LET hayusr = 0
 FOR i = 1 TO totusr
  -- Verificando estado
  IF (v_permxusr[i].tcheckb=estado) THEN
     LET hayusr = 1
     EXIT FOR
  END IF
 END FOR

 RETURN hayusr
END FUNCTION
