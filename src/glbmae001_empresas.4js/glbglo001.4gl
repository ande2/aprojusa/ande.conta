{ 
glbglo001.4gl
MRS 
Mantenimiento de empresas
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "glbmae001"
DEFINE w_mae_pro   RECORD LIKE glb_empresas.*,
       v_empresas  DYNAMIC ARRAY OF RECORD
        tcodemp    LIKE glb_empresas.codemp,
        tnomemp    LIKE glb_empresas.nomemp, 
        tnumnit    LIKE glb_empresas.numnit 
       END RECORD,
       v_permxusr  DYNAMIC ARRAY OF RECORD
        tcheckb    SMALLINT,
        tuserid    LIKE inv_permxtmv.userid,
        tnomusr    VARCHAR(50),
        tusuaid    LIKE inv_permxtmv.usuaid,
        tfecsis    LIKE inv_permxtmv.fecsis,
        thorsis    LIKE inv_permxtmv.horsis,
        tendrec    CHAR(1)
       END RECORD,
       username    VARCHAR(15)
END GLOBALS
