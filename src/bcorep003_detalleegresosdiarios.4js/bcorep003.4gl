{ 
Programa : bcorep003.4gl                   
Programo : Mynor Ramirez 
Objetivo : Reporte de egresos diarios 
}

DATABASE erpjuridico 

-- Definicion de variables globales 

CONSTANT progname = "bcorep003" 
TYPE    datosreporte        RECORD 
         lnkbco             LIKE bco_mtransac.lnkbco,
         codemp             LIKE bco_mtransac.codemp, 
         nomemp             CHAR(40), 
         numcta             LIKE bco_mtransac.numcta,
         nomcta             LIKE bco_mcuentas.nomcta,
         nombco             CHAR(30),
         fecemi             LIKE bco_mtransac.fecemi,
         tipmov             LIKE bco_mtransac.tipmov,
         nommov             CHAR(6), 
         codrub             LIKE bco_mtransac.codrub,
         nomrub             CHAR(50), 
         numdoc             CHAR(15),                       
         nomsoc             CHAR(35), 
         descrp             CHAR(150),
         totdoc             LIKE bco_mtransac.totdoc,
         tipope             LIKE bco_mtransac.tipope,
         estado             SMALLINT, 
         nofase             SMALLINT,
         codcos             LIKE bco_mtransac.codcos,
         nomcen             CHAR(20) 
        END RECORD 
DEFINE  datos               RECORD 
         codemp             LIKE bco_mtransac.codemp, 
         fecini             DATE,
         fecfin             DATE
        END RECORD
DEFINE  w_mae_emp           RECORD LIKE glb_empresas.*
DEFINE  existe,nlines,lg,tc SMALLINT
DEFINE  haymov              SMALLINT
DEFINE  nmovs               INTEGER 
DEFINE  tituloreporte       STRING 
DEFINE  filename            STRING
DEFINE  pipeline            STRING
DEFINE  s,d                 CHAR(1) 
DEFINE  np                  CHAR(3) 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/ToolBarReportes1")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("regresosdiarios")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL bcorep003_EgresosDiarios()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION bcorep003_EgresosDiarios()
 DEFINE imp1     datosreporte,
        wpais    VARCHAR(255),
        qrytxt   STRING,
        qryemp   STRING,
        qrypart  STRING,
        loop     SMALLINT,
        w        ui.Window, 
        f        ui.Form   

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep003a AT 5,2
  WITH FORM "bcorep003a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/EgresosDiarios.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Llenando combobox de empresas         
  CALL librut003_CbxEmpresas() 

  -- Inicio del loop
  OPTIONS INPUT WRAP 
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE pipeline TO NULL
   LET s = 1 SPACE
   LET haymov = 1 
   CLEAR FORM

   -- Ingresando datos 
   DIALOG ATTRIBUTES(UNBUFFERED,FIELD ORDER FORM)

    -- Construyendo busqueda
    INPUT BY NAME datos.codemp,
                  datos.fecini,
                  datos.fecfin
    END INPUT 

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT DIALOG

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 

     -- Verificando ingreso de filtros
     IF NOT bcorep003_FiltrosCompletos() THEN 
        NEXT FIELD codemp
     END IF 
     EXIT DIALOG 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf" 

     -- Verificando ingreso de filtros
     IF NOT bcorep003_FiltrosCompletos() THEN 
        NEXT FIELD codemp
     END IF 
     EXIT DIALOG 

    ON ACTION excel 
     -- Asignando dispositivo 
     LET pipeline = "excel" 
     LET s = ASCII(9) 

     -- Verificando ingreso de filtros
     IF NOT bcorep003_FiltrosCompletos() THEN 
        NEXT FIELD codemp
     END IF 
     EXIT DIALOG

   END DIALOG 
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Obteniendo datos de la empresa
   INITIALIZE w_mae_emp.* TO NULL
   CALL librut003_BEmpresa(datos.codemp) 
   RETURNING w_mae_emp.*,existe 

   -- Calculando numero de largo del reporte
   LET lg    = 170 
   LET np    = 7.5
   LET tc    = 85   
   LET nmovs = 0 

   -- Seleccion de empresa
   IF datos.codemp IS NOT NULL THEN  
      LET qryemp = "WHERE a.codemp = ",datos.codemp," AND "
   ELSE
      LET qryemp = "WHERE " 
   END IF 

   -- Construyendo seleccion 
   LET qrytxt = "SELECT a.* ",
                 "FROM  vis_egresosdiarios a ",
                 qryemp CLIPPED, 
                 "      a.fecemi >= '",datos.fecini,"'", 
                 "  AND a.fecemi <= '",datos.fecfin,"'", 
                 " ORDER BY a.codrub,a.fecemi,a.lnkbco" 

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep003 FROM qrytxt 
   DECLARE c_crep003 CURSOR FOR c_rep003
   LET existe = FALSE
   LET haymov = 1 
   FOREACH c_crep003 INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       CASE pipeline
        WHEN "pdf" LET nlines = 76 
        OTHERWISE  LET nlines = 66
       END CASE 

       LET existe = TRUE
       START REPORT bcorep003_ImprimirEgresosDiarios TO filename 
    END IF 

    -- Llenando el reporte
    OUTPUT TO REPORT bcorep003_ImprimirEgresosDiarios(imp1.*)
   END FOREACH
   CLOSE c_crep003 
   FREE  c_crep003 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT bcorep003_ImprimirEgresosDiarios 

      -- Transfiriendo reporte a excel
      IF pipeline = "excel" THEN
         CALL bcorep003_excel(filename)
      ELSE
         -- Enviando reporte al destino seleccionado
         CALL librut001_sendreport(filename,pipeline,tituloreporte,"-l -p "||np)
      END IF

      ERROR "" 
      CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(
      " Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 
  END WHILE

  OPTIONS INPUT NO WRAP 

 CLOSE WINDOW wrep003a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION bcorep003_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE

 -- Verificando datos 
 IF datos.fecini IS NULL OR 
    datos.fecfin IS NULL THEN 
    CALL fgl_winmessage(
    " Atencion",
    " Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte 

REPORT bcorep003_ImprimirEgresosDiarios(imp1)
 DEFINE imp1              datosreporte,
        linea             CHAR(306),
        tmovs             INTEGER,       
        col,i             INTEGER,
        espacios          CHAR(4), 
        fechareporte      STRING,
        f                 CHAR(1) 

  OUTPUT LEFT MARGIN 3
         TOP MARGIN 3 
         BOTTOM MARGIN 3 
         PAGE LENGTH nlines

  FORMAT 
   PAGE HEADER

    -- Llenando linea
    LET linea = NULL
    FOR i = 1 TO lg
     LET linea = linea CLIPPED,"-"
    END FOR  

    -- Imprimiendo Encabezado
    LET col = librut001_centrado(tituloreporte,lg) 
    PRINT COLUMN   1,"Bancos",
	  COLUMN (lg-20),PAGENO USING "Pagina: <<<<"

    PRINT COLUMN   1,"Bcorep003",
          COLUMN col,UPSHIFT(tituloreporte) CLIPPED,  
          COLUMN (lg-20),"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    LET fechareporte = "PERIODO DE FECHAS ",
                       "DEL [ ",datos.fecini," ] AL [ ",datos.fecfin," ]" 
    LET col = librut001_centrado(fechareporte,lg) 

    PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
          COLUMN col,fechareporte CLIPPED,
          COLUMN (lg-20),"Hora  : ",TIME 
    PRINT linea CLIPPED 
    PRINT "Fecha de    Tipo    Proveedor                            Numero de",
          "       Descripcion del Egreso                         ",
          "                          Centro de Costo"
    PRINT "Emision     Movto                                        Documento",
          "                                                      ",
          "                   Total                 "
    PRINT linea
    IF datos.codemp IS NOT NULL THEN 
       PRINT "Empresa: ",w_mae_emp.nomemp CLIPPED
    ELSE
       PRINT "Empresa: TODAS"
    END IF 
    SKIP 1 LINES 

  BEFORE GROUP OF imp1.codrub
   -- Verificando si reporte es a excel
   IF (PAGENO=1) THEN 
    IF (pipeline="excel") THEN
      -- Imprimiendo datos 
      PRINT tituloreporte CLIPPED,s
      IF datos.codemp IS NOT NULL THEN 
         PRINT "Empresa: ",w_mae_emp.nomemp CLIPPED,s
      ELSE
         PRINT "Empresa: TODAS",s
      END IF 
      PRINT s
      PRINT "Fecha Emision",s,
            "Tipo Movimiento",s,
            "Proveedor",s,
            "Numero Documento",s,
            "Descripcion del Egreso",s,
            "Total",s,
            "Centro Costo",s
      PRINT s
    END IF 
   END IF

   IF (PAGENO>1) THEN 
      PRINT s 
   END IF 

   -- Imprimiendo datos del tipo de concepto 
   LET tmovs = 0 
   PRINT "Tipo de Concepto:",s,imp1.codrub,s,imp1.nomrub CLIPPED 
   PRINT s 

  ON EVERY ROW 
   -- Imprimiendo egresos 
   LET nmovs = (nmovs+1)
   LET tmovs = (tmovs+1)
   LET espacios = s,s,s,s 

   -- Verificando si reportes es a excel 
   IF (pipeline="excel") THEN 
    PRINT imp1.fecemi                                     ,s,1 SPACES,
          imp1.nommov                                     ,s,1 SPACES,
          imp1.nomsoc                                     ,s,1 SPACES,
          imp1.numdoc                                     ,s,
          imp1.descrp[1,75]                               ,s,1 SPACES,
          imp1.totdoc               USING "###,###,##&.&&",s,1 SPACES,
          imp1.nomcen                                     ,s

    -- Verificando si tiene descripcion
    IF (LENGTH(imp1.descrp)>75) THEN
       PRINT COLUMN 74,espacios CLIPPED,imp1.descrp[76,150] 
    END IF
   ELSE
    PRINT imp1.fecemi                                     ,s,1 SPACES,
          imp1.nommov                                     ,s,1 SPACES,
          imp1.nomsoc                                     ,s,1 SPACES,
          imp1.numdoc                                     ,s,
          imp1.descrp[1,55]                               ,s,1 SPACES,
          imp1.totdoc               USING "###,###,##&.&&",s,1 SPACES,
          imp1.nomcen                                     ,s

    -- Verificando si tiene descripcion
    IF (LENGTH(imp1.descrp)>55) THEN
       PRINT COLUMN 74,espacios CLIPPED,imp1.descrp[55,110] 
    END IF
    -- Verifica si tiene descripcion
    IF (LENGTH(imp1.descrp)>110) THEN
       PRINT COLUMN 74,espacios CLIPPED,imp1.descrp[111,150] 
    END IF
   END IF 

  AFTER GROUP OF imp1.codrub 
   -- Totalizando x tipo de concepto 
   PRINT s
   PRINT COLUMN   1,tmovs USING "<<<<<<"," Egreso[s] ",s,s,s,s, 
         COLUMN  81,"Totales -->",s, 
         COLUMN 131,GROUP SUM(imp1.totdoc)    USING "###,###,##&.&&",s

  ON LAST ROW 
   -- Totalizando
   PRINT s
   PRINT COLUMN   1,nmovs USING "<<<<<<"," Egreso[s] ",s,s,s,s, 
         COLUMN  81,"Totales -->",s, 
         COLUMN 131,SUM(imp1.totdoc)    USING "###,###,##&.&&",s
END REPORT 

-- Subrutina para exportar el reporte a excel usando WinCOM 

FUNCTION bcorep003_excel(filename) 
 DEFINE sb          base.StringBuffer
 DEFINE ch          base.channel
 DEFINE lista       base.StringTokenizer
 DEFINE result      INTEGER
 DEFINE i           INTEGER
 DEFINE xlapp       INTEGER
 DEFINE xlwb        INTEGER
 DEFINE linea       STRING 
 DEFINE lineadep    STRING 
 DEFINE filename    STRING 
 DEFINE columna     STRING 
 DEFINE separador   CHAR(1) 
 DEFINE nuevalinea  CHAR(1) 

 -- Inicializando variables                            
 LET xlapp = -1
 LET xlwb = -1

 -- Creando la instancia de Excel
 CALL ui.Interface.frontCall("WinCOM", "CreateInstance", ['Excel.Application'], [xlapp])
 CALL CheckError(xlapp, xlwb, xlapp)
   
 -- Agregando un libro al documento de excel               
 CALL ui.interface.frontCall("WinCOM", "CallMethod", [xlapp, 'WorkBooks.Add'], [xlwb])
 CALL CheckError(xlapp, xlwb, xlwb)
   
 -- Haciendo visible el libro                 
 CALL excel_set_property(xlapp, xlapp, "Visible","True")
  
 -- Llenando celdas desde el FrontCall 
 -- Construyendo celdas delimitando columnas por TAB (ASCII(9)), 
 -- y filas por CR (ASCII(10)
 -- Copia al clipboard via el Frontcall y luego instruye a excel para pegar el contenido
 -- de la celdas dentro del libro 
 LET sb = base.StringBuffer.create()

 -- Leyendo el archivo que contiene el reporte y copiandolo al clipboard 
 LET ch = base.channel.create()
 call ch.openfile(filename,"r")

 let i = 1 
 let separador  = ascii(9) 
 let nuevalinea = ascii(10)
 while true
  let linea = ch.readLine() 
  if ch.isEof() then exit while end if
  if linea.getindexof(separador,1)>0 then
     CALL sb.append(linea)
     CALL sb.append(nuevalinea)
  end if 
  let i = i+1
 end while
 call ch.close() 

 -- Pegando el contenido de las celdas en el clipboard al libro de excel
 CALL ui.Interface.FrontCall("standard","cbset",sb.toString(),result)
 CALL excel_call_method(xlapp, xlwb, 'ActiveSheet.Paste')
 CALL excel_call_method(xlapp, xlwb, 'Columns("A:Z").EntireColumn.AutoFit')
 --CALL excel_set_property(xlapp, xlwb, excel_column(nc,"Z","NumberFormat"), "0.00")

 -- Liberando informacion del clipboard    
 CALL freeMemory(xlapp, xlwb)
END FUNCTION
