
drop view vis_egresosdiarios;

create view vis_egresosdiarios
(lnkbco, codemp, nomemp, numcta, nomcta, nombco, fecemi, tipmov, nommov,
 codrub, nomrub, numdoc, nomsoc, descrp, totdoc, tipope, estado, nofase,
 codcos, nomcen)

as

select a.lnkbco,
       a.codemp,
       e.nomemp,
       a.numcta,
       b.nomcta,
       c.nombco,
       a.fecemi,
       a.tipmov,
       d.nomabr,
       a.codrub,
       f.nomrub,
       a.numdoc,
       a.nomsoc,
       a.descrp,
       a.totdoc,
       a.tipope,
       a.estado,
       a.nofase,
       a.codcos,
       h.nomcen 
from   bco_mtransac a,bco_mcuentas b,glb_mtbancos c,bco_tipomovs d,
       glb_empresas e,fac_rubgasto f,outer glb_cencosto h 
where  a.tipmov not in (5,7,25)
  and  a.tipope = 0
  and  a.estado = 1
  and  e.codemp = a.codemp
  and  b.numcta = a.numcta
  and  b.codbco = c.codbco
  and  d.tipmov = a.tipmov
  and  f.codrub = a.codrub
  and  h.codcos = a.codcos;

grant select on vis_egresosdiarios to public;
