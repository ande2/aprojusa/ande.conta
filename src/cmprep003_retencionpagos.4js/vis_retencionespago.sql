drop view vis_retencionespago;

create view vis_retencionespago

(lnkbco,tipsol,codemp,nomemp,fecemi,codsoc,nomsoc,numcta,numdoc,reisra,reishs)

as

select x.lnkbco,
       x.tipsol, 
       x.codemp,
       e.nomemp,
       x.feccob,
       x.codsoc,
       p.nomsoc, 
       x.numcta,
       x.numdoc,
       x.reisra,
       x.reishs
from bco_mtransac x,glb_empresas e,glb_sociosng p
where x.lnkbco is not null
  and x.tipsol is not null 
  and x.estado = 1
  and (x.reisra+x.reishs) >0
  and e.codemp = x.codemp 
  and p.codsoc = x.codsoc;

grant select on vis_retencionespago to public;
