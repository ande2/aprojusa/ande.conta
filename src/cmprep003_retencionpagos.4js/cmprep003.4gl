{ 
Programa : cmprep003.4gl                   
Objetivo : Reporte de retenciones de pago. 
}

DATABASE erpjuridico 

-- Definicion de variables globales 

CONSTANT progname = "cmprep003" 
TYPE    datosreporte        RECORD 
         lnkbco             LIKE bco_mtransac.lnkbco, 
         tipsol             LIKE bco_mtransac.tipsol, 
         codemp             LIKE bco_mtransac.codemp,
         nomemp             LIKE glb_empresas.nomemp,
         fecemi             LIKE bco_mtransac.fecemi,
         codsoc             LIKE bco_mtransac.codsoc,
         nomsoc             CHAR(40), 
         numcta             CHAR(15), 
         numdoc             CHAR(15),
         reisra             LIKE bco_mtransac.reisra,
         reishs             LIKE bco_mtransac.reishs 
        END RECORD 
DEFINE  datos               RECORD 
         codemp             LIKE bco_mtransac.codemp, 
         fecini             DATE,
         fecfin             DATE
        END RECORD
DEFINE  existe,nl,lg        SMALLINT
DEFINE  primeravez          SMALLINT 
DEFINE  tituloreporte       STRING 
DEFINE  filename            STRING
DEFINE  pipeline            STRING
DEFINE  s,d,apostrofe       CHAR(1) 
DEFINE  np                  CHAR(3) 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/ToolBarReportes1")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rretencionespago")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL cmprep003_RetencionesPago() 
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION cmprep003_RetencionesPago() 
 DEFINE imp1      datosreporte,
        wpais     VARCHAR(255),
        w         ui.Window, 
        loop      SMALLINT,
        f         ui.Form,
        qrytxt    STRING

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep005a AT 5,2
  WITH FORM "cmprep003a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/RetencionesPago.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Llenando combobox de empresas
  CALL librut003_CbxEmpresas()

  -- Inicio del loop
  OPTIONS INPUT WRAP 
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE pipeline,datos.* TO NULL
   LET datos.codemp = 1 
   LET s = 1 SPACE
   LET primeravez = TRUE
   CLEAR FORM

   -- Ingresando datos 
   DIALOG ATTRIBUTES(UNBUFFERED,FIELD ORDER FORM)

    -- Construyendo busqueda
    INPUT BY NAME datos.codemp,
                  datos.fecini,
                  datos.fecfin
     ATTRIBUTE(WITHOUT DEFAULTS) 
    END INPUT 

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT DIALOG

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "array" 

     -- Verificando ingreso de filtros
     IF NOT cmprep003_FiltrosCompletos() THEN 
        NEXT FIELD codemp 
     END IF 
     EXIT DIALOG 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf" 

     -- Verificando ingreso de filtros
     IF NOT cmprep003_FiltrosCompletos() THEN 
        NEXT FIELD codemp 
     END IF 
     EXIT DIALOG 

    ON ACTION excel 
     -- Asignando dispositivo 
     LET pipeline = "excel" 
     LET s = ASCII(9) 

     -- Verificando ingreso de filtros
     IF NOT cmprep003_FiltrosCompletos() THEN 
        NEXT FIELD codemp 
     END IF 
     EXIT DIALOG

   END DIALOG 
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Calculando numero de largo del reporte
   LET lg = 157 
   LET np = 8    
   LET nl = 72 

   -- Construyendo seleccion 
   LET qrytxt = "SELECT a.* ",
                 "FROM  vis_retencionespago a ",
                 "WHERE a.codemp = ",datos.codemp,
                 "  AND a.fecemi >= '",datos.fecini,"' ",
                 "  AND a.fecemi <= '",datos.fecfin,"' ",
                 " ORDER BY a.fecemi"

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep003 FROM qrytxt 
   DECLARE c_retpag CURSOR FOR c_rep003
   LET existe = FALSE
   FOREACH c_retpag INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       LET existe = TRUE
       START REPORT cmprep003_GenerarReporte TO filename 
    END IF 

    -- Llenando el reporte
    OUTPUT TO REPORT cmprep003_GenerarReporte(imp1.*)
   END FOREACH
   CLOSE c_retpag 
   FREE  c_retpag 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT cmprep003_GenerarReporte 

      -- Transfiriendo reporte a excel
      IF pipeline = "excel" THEN
         CALL cmprep003_excel(filename)
      ELSE
         -- Enviando reporte al destino seleccionado
         CALL librut001_sendreport(filename,pipeline,tituloreporte,"-l -p "||np)
      END IF

      ERROR "" 
      CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(
      " Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 
  END WHILE
  OPTIONS INPUT NO WRAP 

 CLOSE WINDOW wrep005a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION cmprep003_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE

 -- Verificando datos 
 IF datos.codemp IS NULL OR
    datos.fecini IS NULL OR 
    datos.fecfin IS NULL THEN 
    CALL fgl_winmessage(
    " Atencion",
    " Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte 

REPORT cmprep003_GenerarReporte(imp1)
 DEFINE imp1              datosreporte,
        imp2              RECORD
         fecemi           DATE,
         numdoc           CHAR(20),
         totpag           DEC(14,2)
        END RECORD, 
        linea             CHAR(157),
        totfac            SMALLINT, 
        col,i             INTEGER,
        periodo           STRING

  OUTPUT LEFT MARGIN 2
         TOP MARGIN 2
         BOTTOM MARGIN 2 
         PAGE LENGTH nl 

  FORMAT 
   PAGE HEADER

    -- Llenando linea
    LET linea = NULL
    FOR i = 1 TO lg
     LET linea = linea CLIPPED,"-"
    END FOR  

    -- Imprimiendo Encabezado
    LET col = librut001_centrado(tituloreporte,lg) 
    PRINT COLUMN   1,"Cuentas X Pagar",
          COLUMN col,UPSHIFT(tituloreporte) CLIPPED,  
	  COLUMN (lg-20),PAGENO USING "Pagina: <<<<"

    LET periodo = "PERIODO DEL ",datos.fecini," AL ",datos.fecfin 
    LET col = librut001_centrado(periodo,lg) 
    PRINT COLUMN   1,"Cmprep003",
          COLUMN col,periodo CLIPPED,
          COLUMN (lg-20),"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    LET col = librut001_centrado(imp1.nomemp,lg) 
    PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
          COLUMN col,imp1.nomemp CLIPPED,
          COLUMN (lg-20),"Hora  : ",TIME 
    PRINT linea 
    PRINT "P R O V E E D O R                         Fecha       Cuenta           ",
          "Numero-Documento  Valor RET    Valor RET  Fecha       Numero                     Total"
    PRINT "                                          Pago        Bancaria         ",
          "Bancario             I.S.R.     Hon/Serv  Factura     Factura                     Pago"
    PRINT linea 

  ON EVERY ROW 
   -- Imprimiendo encabezado a excel
   IF (pipeline="excel") THEN
      IF primeravez THEN 
         PRINT UPSHIFT(tituloreporte) CLIPPED,s
         PRINT periodo CLIPPED,s
         PRINT imp1.nomemp CLIPPED,s 
         PRINT s 
         PRINT "PROVEEDOR",s,"Fecha",s,"Cuenta",s,"Numero-Documento",s,"Valor RETENCION",s,
               "Valor RETENCION",s,"Fecha",s,"Numero",s,"Total" 
         PRINT s,"Pago",s,"Bancaria",s,"Bancario",s,"I.S.R.",s,"Honorarios/Servicios",s,
               "Factura",s,"Factura",s,"Pago" 
         PRINT s 
         LET primeravez = FALSE 
      END IF 

      -- Imprimiendo retenciones
      PRINT imp1.nomsoc                                      ,s,
            imp1.fecemi                                      ,s,
            imp1.numcta                                      ,s,
            imp1.numdoc                                      ,s,
            imp1.reisra                  USING "---,--&.&&"  ,s,
            imp1.reishs                  USING "---,--&.&&"  ,s; 
   ELSE
      -- Imprimiendo retenciones
      PRINT imp1.nomsoc                                      ,s,1 SPACES, 
            imp1.fecemi                                      ,s,1 SPACES,
            imp1.numcta                                      ,s,1 SPACES,
            imp1.numdoc                                      ,s,1 SPACES,
            imp1.reisra                  USING "---,--&.&&"  ,s,2 SPACES,           
            imp1.reishs                  USING "---,--&.&&"  ,s; 
   END IF 

   -- Verificando tipo de solicitud 
   IF (imp1.tipsol=1) THEN
      -- Buscando facturas de pago 
      DECLARE cfacs CURSOR FOR
      SELECT y.fecemi,
             y.numdoc,
             x.totpag 
       FROM  bco_dtransac x,cmp_mtransac y
       WHERE x.lnkbco = imp1.lnkbco
         AND x.lnkcmp = y.lnkcmp 
       ORDER BY 1 

       LET totfac = 1 
       FOREACH cfacs INTO imp2.fecemi,imp2.numdoc,imp2.totpag 
        -- Imprimiendo facturas de pago 
        IF (totfac=1) THEN 
         IF (pipeline!="excel") THEN
          PRINT                                                 1 SPACES, 
               imp2.fecemi                                   ,s,1 SPACES,
               imp2.numdoc                                   ,s,1 SPACES,
               imp2.totpag                USING "---,--&.&&" ,s
         ELSE
          PRINT                                                
               imp2.fecemi                                   ,s,
               imp2.numdoc                                   ,s,
               imp2.totpag                USING "---,--&.&&" ,s
         END IF 
        ELSE
         -- Verificando si envio es a excel
         IF (pipeline!="excel") THEN
            PRINT COLUMN 114,imp2.fecemi                     ,s,1 SPACES,
                             imp2.numdoc                     ,s,1 SPACES,
                             imp2.totpag  USING "---,--&.&&" ,s
         ELSE
            PRINT                                             s,s,s,s,s,s, 
                             imp2.fecemi                     ,s,
                             imp2.numdoc                     ,s,
                             imp2.totpag  USING "---,--&.&&" ,s
         END IF 
        END IF 
        LET totfac = totfac+1
       END FOREACH
       CLOSE cfacs
       FREE  cfacs 
   END IF 
   PRINT s 

  ON LAST ROW
   -- Totalizando reporte 
   IF (pipeline!="excel") THEN 
      PRINT "Total Pago de Retenciones -->",55 SPACES        ,s,3 SPACES,
            SUM(imp1.reisra)              USING "---,--&.&&" ,s,2 SPACES,           
            SUM(imp1.reishs)              USING "---,--&.&&" ,s
   ELSE
      PRINT "Total Pago de Retenciones -->",s,s,s,s, 
            SUM(imp1.reisra)              USING "---,--&.&&" ,s,
            SUM(imp1.reishs)              USING "---,--&.&&" ,s
   END IF 
END REPORT 

-- Subrutina para exportar el reporte a excel usando WinCOM 

FUNCTION cmprep003_excel(filename) 
 DEFINE sb          base.StringBuffer
 DEFINE ch          base.channel
 DEFINE lista       base.StringTokenizer
 DEFINE result      INTEGER
 DEFINE i           INTEGER
 DEFINE xlapp       INTEGER
 DEFINE xlwb        INTEGER
 DEFINE linea       STRING 
 DEFINE lineadep    STRING 
 DEFINE filename    STRING 
 DEFINE columna     STRING 
 DEFINE separador   CHAR(1) 
 DEFINE nuevalinea  CHAR(1) 

 -- Inicializando variables                            
 LET xlapp = -1
 LET xlwb = -1

 -- Creando la instancia de Excel
 CALL ui.Interface.frontCall("WinCOM", "CreateInstance", ['Excel.Application'], [xlapp])
 CALL CheckError(xlapp, xlwb, xlapp)
   
 -- Agregando un libro al documento de excel               
 CALL ui.interface.frontCall("WinCOM", "CallMethod", [xlapp, 'WorkBooks.Add'], [xlwb])
 CALL CheckError(xlapp, xlwb, xlwb)
   
 -- Haciendo visible el libro                 
 CALL excel_set_property(xlapp, xlapp, "Visible","True")
  
 -- Llenando celdas desde el FrontCall 
 -- Construyendo celdas delimitando columnas por TAB (ASCII(9)), 
 -- y filas por CR (ASCII(10)
 -- Copia al clipboard via el Frontcall y luego instruye a excel para pegar el contenido
 -- de la celdas dentro del libro 
 LET sb = base.StringBuffer.create()

 -- Leyendo el archivo que contiene el reporte y copiandolo al clipboard 
 LET ch = base.channel.create()
 call ch.openfile(filename,"r")

 let i = 1 
 let separador  = ascii(9) 
 let nuevalinea = ascii(10)
 while true
  let linea = ch.readLine() 
  if ch.isEof() then exit while end if
  if linea.getindexof(separador,1)>0 then
     CALL sb.append(linea)
     CALL sb.append(nuevalinea)
  end if 
  let i = i+1
 end while
 call ch.close() 

 -- Pegando el contenido de las celdas en el clipboard al libro de excel
 CALL ui.Interface.FrontCall("standard","cbset",sb.toString(),result)
 CALL excel_call_method(xlapp, xlwb, 'ActiveSheet.Paste')
 CALL excel_call_method(xlapp, xlwb, 'Columns("A:Z").EntireColumn.AutoFit')

 -- Liberando informacion del clipboard    
 CALL freeMemory(xlapp, xlwb)
END FUNCTION
