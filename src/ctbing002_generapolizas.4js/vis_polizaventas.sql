
drop view vis_polizaventas; 

create view vis_polizaventas (codemp,fecemi,codcat,nomcat,cantid,preuni,totpro,totisv) as 
  select x0.codemp,x0.fecemi,x2.codcat,x3.nomcat,x1.cantid,x1.preuni,x1.totpro,x1.totisv
   from fac_mtransac x0,fac_dtransac x1,inv_products x2,glb_categors x3,fac_tdocxpos x4 
   where x0.lnktra = x1.lnktra 
     AND x0.estado = 'V'
     AND x2.cditem = x1.cditem 
     AND x3.codcat = x2.codcat
     AND x4.lnktdc = x0.lnktdc 
     AND x4.haycor = 1;              

grant select on vis_polizaventas to public;
