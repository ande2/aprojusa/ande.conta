
  drop view vis_polizacompras;

create view vis_polizacompras (codemp,tipmov,fecemi,codcat,nomcat,totpro,totisv) as
  select x0.codemp,x0.tipmov,x0.fecemi,x2.codcat,x3.nomcat,x1.totpro,x1.totisv
   from inv_mtransac x0,inv_dtransac x1,inv_products x2,glb_categors x3
   where x0.lnktra = x1.lnktra
     AND x0.estado = 'V'
     AND x1.actexi = 1
     AND x2.cditem = x1.cditem
     AND x3.codcat = x2.codcat;

grant select on vis_polizacompras to public;
