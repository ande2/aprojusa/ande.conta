SELECT SUM(a.totpro),SUM(a.totisv),SUM(a.totpro)-SUM(a.totisv)
FROM inv_dtransac a
WHERE a.codemp = 1
  AND a.tipmov = 100
  AND a.fecemi >= "01012016"
  AND a.fecemi <= "31012016"
  AND a.estado = "V"
  AND a.actexi = 1
