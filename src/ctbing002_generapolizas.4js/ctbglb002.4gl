{ 
Programo : Mynor Ramirez 
Objetivo : Programa de variables globales para la generacion de polizas 
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname  = "ctbing002"
DEFINE w_mae_tra   RECORD LIKE ctb_mtransac.*,
       v_partida   DYNAMIC ARRAY OF RECORD
        numcta     LIKE ctb_dtransac.numcta,
        nomcta     LIKE ctb_mcuentas.nomcta,
        totdeb     LIKE ctb_dtransac.totdeb,
        tothab     LIKE ctb_dtransac.tothab,
        concpt     LIKE ctb_dtransac.concep,
        rellen     CHAR(1)
       END RECORD,
       username    LIKE glb_permxusr.userid,
       totaldeb    DEC(14,2),
       totalhab    DEC(14,2),
       totlin      SMALLINT,
       w           ui.Window,
       f           ui.Form
END GLOBALS
