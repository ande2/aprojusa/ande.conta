{
ctbing002.4gl 
Objetivo : generacion de polizas automaticas de varios auxiliares hacia la contabilidad 
}

-- Definicion de variables globales

GLOBALS "ctbglb002.4gl"
DEFINE w_mae_emp RECORD LIKE glb_empresas.*,
       wpais     VARCHAR(255), 
       existe    SMALLINT,
       msg       STRING

-- Subrutina principal

MAIN
 -- Atrapando interrupts 
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/ToolBarGenerarPolizas")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("generapolizas")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ 

 -- Obteniendo datos del pais 
 CALL librut003_parametros(1,0)
 RETURNING existe,wpais

 -- Obteniendo usuario del sistema
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de opciones
 CALL ctbing002_menu()
END MAIN

-- Subutina para el menu de generacion de polizas 

FUNCTION ctbing002_menu()
 DEFINE TipoPoliza LIKE ctb_mtransac.tiptrn 

 -- Abriendo la ventana
 OPEN WINDOW wing002a AT 5,2  
  WITH FORM "ctbing002a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL librut001_header("ctbing002",wpais,1)

  -- Inicializando datos 
  CALL ctbing002_inival()

  -- Menu de opciones
  MENU "Opciones" 
   BEFORE MENU
    -- Verificando accesos a opciones
    -- Consultar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Ventas"
    END IF
   COMMAND "Ventas" 
    "Generacion de poliza de ventas por mes."

    -- Generando poliza 
    LET TipoPoliza = 104 
    CALL ctbing002_EstadoMenu(1)
    CALL ctbing002_DatosPolizaVentas(TipoPoliza)               
    CALL ctbing002_EstadoMenu(0)
   COMMAND "Compras" 
    "Generacion de poliza de compras por mes."

    -- Generando poliza 
    LET TipoPoliza = 103 
    CALL ctbing002_EstadoMenu(2)
    CALL ctbing002_DatosPolizaCompras(TipoPoliza)               
    CALL ctbing002_EstadoMenu(0)
   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF
   COMMAND "Salir"
    "Abandona el menu de generacion de polizas." 
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU  
  END MENU
 CLOSE WINDOW wing002a
END FUNCTION

-- Subrutina para generar la poliza de ventas 

FUNCTION ctbing002_DatosPolizaVentas(wtiptrn)                
 DEFINE wtiptrn  LIKE ctb_mtransac.tiptrn,
        wnumcta  LIKE ctb_dtransac.numcta,
        wcodcat  LIKE glb_categors.codcat,
        wnomcat  LIKE glb_categors.nomcat,
        regreso  SMALLINT, 
        wtotcta  DEC(14,2),  
        wvtacre  DEC(14,2),  
        wvtacon  DEC(14,2),  
        wtotisv  DEC(14,2),  
        wimpvta  DEC(14,2),  
        wcobcre  DEC(14,2),  
        conteo   INTEGER, 
        wfecini  DATE,
        wfecfin  DATE 

 -- Inicializando datos 
 CALL ctbing002_inival()

 -- Cargando comboxbox de fechas
 CALL librut003_12MesesAtras()

 -- Asignando datos default 
 LET w_mae_tra.lnktra = 0
 LET w_mae_tra.codemp = 1 
 LET w_mae_tra.tiptrn = wtiptrn
 LET w_mae_tra.userid = username
 LET w_mae_tra.fecsis = CURRENT
 LET w_mae_tra.horsis = CURRENT HOUR TO SECOND
 LET w_mae_tra.infadi = "POLIZA GENERADA DEL AUXILIAR DE FACTURACION" 
 LET totlin           = 0
 LET totaldeb         = 0
 LET totalhab         = 0

 -- Desplegando datos
 DISPLAY BY NAME w_mae_tra.codemp,w_mae_tra.tiptrn,w_mae_tra.fecemi,
                 w_mae_tra.userid,w_mae_tra.fecsis,w_mae_tra.horsis,
                 w_mae_tra.infadi,totaldeb,totalhab,totlin

 -- Ingrensando datos   
 LET regreso = FALSE 
 INPUT BY NAME w_mae_tra.codemp,
               w_mae_tra.fecemi,
               w_mae_tra.tiptrn WITHOUT DEFAULTS
  ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE) 
  ON ACTION cancel
   LET regreso = TRUE 
   EXIT INPUT 

  ON CHANGE codemp 
   -- Obteniendo datos de la empresa
   INITIALIZE w_mae_emp.* TO NULL
   CALL librut003_BEmpresa(w_mae_tra.codemp)
   RETURNING w_mae_emp.*,existe

  AFTER FIELD codemp
   -- Obteniendo datos de la empresa
   INITIALIZE w_mae_emp.* TO NULL
   CALL librut003_BEmpresa(w_mae_tra.codemp)
   RETURNING w_mae_emp.*,existe
 END INPUT 
 IF regreso THEN
    -- Inicializando datos 
    CALL ctbing002_inival()
    RETURN
 END IF 

 -- Generando datos de la poliza 
 MESSAGE "Generando Poliza ... por favor esperar ..." 
 LET w_mae_tra.numdoc = librut003_NumeroPolizaAutomatica(w_mae_tra.codemp,
                                                         w_mae_tra.tiptrn,
                                                         w_mae_tra.fecemi)

 LET w_mae_tra.concep = "POLIZA DE VENTAS CORRESPONDIENTE AL MES DE ",
                        UPSHIFT(librut001_nombremeses(MONTH(w_mae_tra.fecemi),1)) CLIPPED,
                        " DEL ",YEAR(w_mae_tra.fecemi) USING "####" 
 LET w_mae_tra.codapl = "VTA" 
 LET w_mae_tra.coddiv = 2
 LET w_mae_tra.lnkapl = 1 
 LET wfecfin          = w_mae_tra.fecemi
 LET wfecini          = MDY(MONTH(wfecfin),1,YEAR(wfecfin)) 

 -- Desplegando datos
 DISPLAY BY NAME w_mae_tra.numdoc,w_mae_tra.concep

 -- Llenando cuentas 
 -- Totalizando ventas de credito
 SELECT NVL(SUM(a.totdoc),0)
  INTO  wvtacre 
  FROM  vis_liquidarutas a
  WHERE a.numpos IS NOT NULL 
    AND a.codemp = w_mae_tra.codemp
    AND a.noruta IS NOT NULL 
    AND a.fecemi >= wfecini
    AND a.fecemi <= wfecfin 
    AND a.credit = 1
    AND a.lnkapa = 0
    AND a.haycor = 1

 -- Totalizando ventas de contado
 SELECT NVL(SUM(a.totdoc),0)
  INTO  wvtacon                 
  FROM  vis_liquidarutas a
  WHERE a.numpos IS NOT NULL 
    AND a.codemp = w_mae_tra.codemp
    AND a.noruta IS NOT NULL 
    AND a.fecemi >= wfecini
    AND a.fecemi <= wfecfin 
    AND a.credit = 0
    AND a.lnkapa = 0
    AND a.haycor = 1

 -- VENTAS DE CREDITO  
 LET totlin   = totlin+1 
 LET wnumcta = "10-01-03-01-01" 
 CALL ctbing002_LlenaCuentaCuenta(w_mae_tra.codemp,
                                  wnumcta,
                                  wvtacre, 
                                  0,           
                                  "VENTAS CREDITO",
                                  totlin)

 -- VENTAS DE CONTADO 
 LET totlin   = totlin+1 
 LET wnumcta = "10-01-02-01-01" 
 CALL ctbing002_LlenaCuentaCuenta(w_mae_tra.codemp,
                                  wnumcta,
                                  wvtacon,
                                  0,           
                                  "VENTAS CONTADO", 
                                  totlin)

 -- DESGLOCE DE VENTAS POR CATEGORIA DE PRODUCTO 
 DECLARE c_catv CURSOR FOR
 SELECT a.codcat,
        a.nomcat,
        a.numcta
  FROM  glb_categors a
  WHERE a.numcta IS NOT NULL

  LET wimpvta = 0 
  FOREACH c_catv INTO wcodcat,wnomcat,wnumcta

   -- Totalizando por categoria 
   SELECT (NVL(SUM(a.totpro),0)-NVL(SUM(a.totisv),0)),NVL(SUM(a.totisv),0)
    INTO  wtotcta,wtotisv 
    FROM  vis_polizaventas a 
    WHERE a.codemp  = w_mae_tra.codemp
      AND a.fecemi >= wfecini
      AND a.fecemi <= wfecfin 
      AND a.codcat  = wcodcat 
   IF wtotcta=0 THEN
      CONTINUE FOREACH
   END IF 

   -- VENTAS POR CATEGORIA 
   LET totlin   = totlin+1 
   CALL ctbing002_LlenaCuentaCuenta(w_mae_tra.codemp,
                                    wnumcta,
                                    0,           
                                    wtotcta, 
                                    wnomcat,  
                                    totlin)

   LET wimpvta = (wimpvta+wtotisv) 
  END FOREACH
  CLOSE c_catv
  FREE  c_catv 

 -- IMPUESTO SOBRE VENTAS 
 LET totlin   = totlin+1 
 LET wnumcta = "20-01-05-02-01"
 CALL ctbing002_LlenaCuentaCuenta(w_mae_tra.codemp,
                                  wnumcta,
                                  0,           
                                  wimpvta,
                                  "IMPUESTO SOBRE VENTAS", 
                                  totlin)

 -- Totalizando poliza
 CALL ctbing002_TotalCuentas()

 -- Desplegando detalle de la poliza
 MESSAGE "" 
 DISPLAY ARRAY v_partida TO s_partida.*
  ATTRIBUTE(ACCEPT=FALSE) 
  ON ACTION transferir 
   -- Verificando si poliza esta descuadrada
   IF (totaldeb!=totalhab) THEN
      CALL fgl_winmessage(
      "Atencion:",
      "Poliza descuadrada, no puede transferirse a la contabilidad.\n"||
      "VERIFICA que el total del debe y el haber sean iguales.",
      "stop") 
      CONTINUE DISPLAY 
   END IF 

   -- Verificando si la poliza ya existe
   SELECT COUNT(*)
    INTO  conteo
    FROM  ctb_mtransac a
    WHERE a.codemp = w_mae_tra.codemp
      AND a.tiptrn = w_mae_tra.tiptrn
      AND a.fecemi = w_mae_tra.fecemi 
      AND a.numdoc = w_mae_tra.numdoc
    IF (conteo>0) THEN
      CALL fgl_winmessage(
      "Atencion:",
      "Poliza ya existe registrada.\n"||
      "La poliza solo puede transferirse una vez por mes.",
      "stop") 
      CONTINUE DISPLAY 
    END IF 

   -- Confirmando traslado 
   LET msg = "Esta seguro de trasladar la poliza ?" 
   IF NOT librut001_yesornot("Confirmacion",msg,"Si","No","question") THEN
      CONTINUE DISPLAY 
   END IF 
   
   -- Transferir a la contabilidad 
   CALL ctbing002_TransferirContabilidad() 

   -- Fin de la transferencia
   CALL fgl_winmessage(
   "Atencion:",
   "Poliza transferida exitosamente.",
   "information")
   EXIT DISPLAY 
 
  ON ACTION cancel 
   -- Regreso 
   EXIT DISPLAY

  ON ACTION calculator
   -- Cargando calculadora
   IF NOT winshellexec("calc") THEN
      ERROR "Atencion: calculadora no disponible."
   END IF
 END DISPLAY 

 -- Inicializando datos 
 CALL ctbing002_inival()
END FUNCTION 

-- Subrutina para generar la poliza de compras 

FUNCTION ctbing002_DatosPolizaCompras(wtiptrn)                
 DEFINE wtiptrn    LIKE ctb_mtransac.tiptrn,
        wnumcta    LIKE ctb_dtransac.numcta,
        wcodcat    LIKE glb_categors.codcat,
        wnomcat    LIKE glb_categors.nomcat,
        tipomovcmp SMALLINT, 
        regreso    SMALLINT, 
        wcmppro    DEC(14,2),  
        wtotcta    DEC(14,2),  
        wtotisv    DEC(14,2),  
        wimpvta    DEC(14,2),  
        conteo     INTEGER, 
        wfecini    DATE,
        wfecfin    DATE 

 -- Inicializando datos 
 CALL ctbing002_inival()

 -- Cargando comboxbox de fechas
 CALL librut003_12MesesAtras()

 -- Asignando datos default 
 LET tipomovcmp       = 100 
 LET w_mae_tra.lnktra = 0
 LET w_mae_tra.codemp = 1 
 LET w_mae_tra.tiptrn = wtiptrn
 LET w_mae_tra.userid = username
 LET w_mae_tra.fecsis = CURRENT
 LET w_mae_tra.horsis = CURRENT HOUR TO SECOND
 LET w_mae_tra.infadi = "POLIZA GENERADA DEL AUXILIAR DE INVENTARIOS" 
 LET totlin           = 0
 LET totaldeb         = 0
 LET totalhab         = 0

 -- Desplegando datos
 DISPLAY BY NAME w_mae_tra.codemp,w_mae_tra.tiptrn,w_mae_tra.fecemi,
                 w_mae_tra.userid,w_mae_tra.fecsis,w_mae_tra.horsis,
                 w_mae_tra.infadi,totaldeb,totalhab,totlin

 -- Ingrensando datos   
 LET regreso = FALSE 
 INPUT BY NAME w_mae_tra.codemp,
               w_mae_tra.fecemi,
               w_mae_tra.tiptrn WITHOUT DEFAULTS
  ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE) 
  ON ACTION cancel
   LET regreso = TRUE 
   EXIT INPUT 

  ON CHANGE codemp 
   -- Obteniendo datos de la empresa
   INITIALIZE w_mae_emp.* TO NULL
   CALL librut003_BEmpresa(w_mae_tra.codemp)
   RETURNING w_mae_emp.*,existe

  AFTER FIELD codemp
   -- Obteniendo datos de la empresa
   INITIALIZE w_mae_emp.* TO NULL
   CALL librut003_BEmpresa(w_mae_tra.codemp)
   RETURNING w_mae_emp.*,existe
 END INPUT 
 IF regreso THEN
    -- Inicializando datos 
    CALL ctbing002_inival()
    RETURN
 END IF 

 -- Generando datos de la poliza 
 MESSAGE "Generando Poliza ... por favor esperar ..." 
 LET w_mae_tra.numdoc = librut003_NumeroPolizaAutomatica(w_mae_tra.codemp,
                                                         w_mae_tra.tiptrn,
                                                         w_mae_tra.fecemi)

 LET w_mae_tra.concep = "POLIZA DE COMPRAS CORRESPONDIENTE AL MES DE ",
                        UPSHIFT(librut001_nombremeses(MONTH(w_mae_tra.fecemi),1)) CLIPPED,
                        " DEL ",YEAR(w_mae_tra.fecemi) USING "####" 
 LET w_mae_tra.codapl = "CMP" 
 LET w_mae_tra.coddiv = 2
 LET w_mae_tra.lnkapl = 1 
 LET wfecfin          = w_mae_tra.fecemi
 LET wfecini          = MDY(MONTH(wfecfin),1,YEAR(wfecfin)) 

 -- Desplegando datos
 DISPLAY BY NAME w_mae_tra.numdoc,w_mae_tra.concep

 -- Llenando cuentas 
 -- Totalizando compras de proveedores 
 SELECT NVL(SUM(a.totpro),0)
  INTO  wcmppro 
  FROM  vis_polizacompras a 
  WHERE a.codemp = w_mae_tra.codemp 
    AND a.tipmov = tipomovcmp            
    AND a.fecemi >= wfecini
    AND a.fecemi <= wfecfin 

 -- COMPRAS PROVEEDORES NACIONALES 
 LET totlin   = totlin+1 
 LET wnumcta = "20-01-01-01-01"
 CALL ctbing002_LlenaCuentaCuenta(w_mae_tra.codemp,
                                  wnumcta,
                                  wcmppro, 
                                  0,           
                                  "PROVEEDORES NACIONALES",
                                  totlin)

 -- DESGLOCE DE COMPRAS POR CATEGORIA DE PRODUCTO 
 DECLARE c_catc CURSOR FOR
 SELECT a.codcat,
        a.nomcat,
        a.numctc 
  FROM  glb_categors a
  WHERE a.numcta IS NOT NULL

  LET wimpvta = 0 
  FOREACH c_catc INTO wcodcat,wnomcat,wnumcta

   -- Totalizando por categoria 
   SELECT (NVL(SUM(a.totpro),0)-NVL(SUM(a.totisv),0)),NVL(SUM(a.totisv),0)
    INTO  wtotcta,wtotisv 
    FROM  vis_polizacompras a 
    WHERE a.codemp  = w_mae_tra.codemp
      AND a.tipmov  = tipomovcmp 
      AND a.fecemi >= wfecini
      AND a.fecemi <= wfecfin 
      AND a.codcat  = wcodcat 
   IF wtotcta=0 THEN
      CONTINUE FOREACH
   END IF 

   -- COMPRAS POR CATEGORIA 
   LET totlin   = totlin+1 
   CALL ctbing002_LlenaCuentaCuenta(w_mae_tra.codemp,
                                    wnumcta,
                                    0,           
                                    wtotcta, 
                                    wnomcat,  
                                    totlin)

   LET wimpvta = (wimpvta+wtotisv) 
  END FOREACH
  CLOSE c_catc
  FREE  c_catc 

 -- IMPUESTO SOBRE VENTAS 
 LET totlin   = totlin+1 
 LET wnumcta = "20-01-05-02-01"
 CALL ctbing002_LlenaCuentaCuenta(w_mae_tra.codemp,
                                  wnumcta,
                                  0,           
                                  wimpvta,
                                  "IMPUESTO SOBRE VENTAS", 
                                  totlin)

 -- Totalizando poliza
 CALL ctbing002_TotalCuentas()

 -- Desplegando detalle de la poliza
 MESSAGE "" 
 DISPLAY ARRAY v_partida TO s_partida.*
  ATTRIBUTE(ACCEPT=FALSE) 
  ON ACTION transferir 
   -- Verificando si poliza esta descuadrada
   IF (totaldeb!=totalhab) THEN
      CALL fgl_winmessage(
      "Atencion:",
      "Poliza descuadrada, no puede transferirse a la contabilidad.\n"||
      "VERIFICA que el total del debe y el haber sean iguales.",
      "stop") 
      CONTINUE DISPLAY 
   END IF 

   -- Verificando si la poliza ya existe
   SELECT COUNT(*)
    INTO  conteo
    FROM  ctb_mtransac a
    WHERE a.codemp = w_mae_tra.codemp
      AND a.tiptrn = w_mae_tra.tiptrn
      AND a.fecemi = w_mae_tra.fecemi 
      AND a.numdoc = w_mae_tra.numdoc
    IF (conteo>0) THEN
      CALL fgl_winmessage(
      "Atencion:",
      "Poliza ya existe registrada.\n"||
      "La poliza solo puede transferirse una vez por mes.",
      "stop") 
      CONTINUE DISPLAY 
    END IF 

   -- Confirmando traslado 
   LET msg = "Esta seguro de trasladar la poliza ?" 
   IF NOT librut001_yesornot("Confirmacion",msg,"Si","No","question") THEN
      CONTINUE DISPLAY 
   END IF 
   
   -- Transferir a la contabilidad 
   CALL ctbing002_TransferirContabilidad() 

   -- Fin de la transferencia
   CALL fgl_winmessage(
   "Atencion:",
   "Poliza transferida exitosamente.",
   "information")
   EXIT DISPLAY 
 
  ON ACTION cancel 
   -- Regreso 
   EXIT DISPLAY

  ON ACTION calculator
   -- Cargando calculadora
   IF NOT winshellexec("calc") THEN
      ERROR "Atencion: calculadora no disponible."
   END IF
 END DISPLAY 

 -- Inicializando datos 
 CALL ctbing002_inival()
END FUNCTION 

-- Subrutina para totalizar el detalle de cuentas

FUNCTION ctbing002_TotalCuentas()
 DEFINE i SMALLINT

 -- Totalizando
 LET totaldeb = 0
 LET totalhab = 0
 FOR i = 1 TO totlin
  IF v_partida[i].totdeb IS NOT NULL THEN
     LET totaldeb = (totaldeb+v_partida[i].totdeb)
  END IF
  IF (v_partida[i].tothab IS NOT NULL) THEN
     LET totalhab = (totalhab+v_partida[i].tothab)
  END IF
 END FOR
 DISPLAY BY NAME totaldeb,totalhab,totlin
END FUNCTION

-- Subrutina para asignar datos a una linea de cuenta contable 

FUNCTION ctbing002_LlenaCuentaCuenta(wcodemp,wnumcta,wtotdeb,wtothab,wconcep,idx)
 DEFINE wcodemp   LIKE ctb_mtransac.codemp,
        wnumcta   LIKE ctb_dtransac.numcta,
        wconcep   LIKE ctb_dtransac.concep,
        wtotdeb   DEC(14,2),
        wtothab   DEC(14,2),
        idx       SMALLINT

 -- Asignando datos
 LET v_partida[idx].numcta = wnumcta

 -- Obteniendo nombre de la cuenta
 LET v_partida[idx].nomcta = NULL 
 SELECT a.nomcta 
  INTO  v_partida[idx].nomcta 
  FROM  ctb_mcuentas a
  WHERE a.numcta = wnumcta 
    AND a.codemp = wcodemp

 LEt v_partida[idx].totdeb = wtotdeb
 LEt v_partida[idx].tothab = wtothab
 LEt v_partida[idx].concpt = wconcep
END FUNCTION

-- Subrutina para transferir la poliza a la contabilidad

FUNCTION ctbing002_TransferirContabilidad()
 DEFINE xtipope  LIKE ctb_dtransac.tipope,
        xtotval  LIKE ctb_mtransac.totdoc,
        i,correl SMALLINT

 -- Grabando poliza contable 
 -- Asignando datos 
 LET w_mae_tra.totdoc = totaldeb 

 -- 1. Grabando maestro
 SET LOCK MODE TO WAIT 
 INSERT INTO ctb_mtransac 
 VALUES (w_mae_tra.*) 
 LET w_mae_tra.lnktra = SQLCA.SQLERRD[2] 

 -- 2. Grabando detalle
 LET correl = 0
 FOR i = 1 TO totlin 
  IF v_partida[i].numcta IS NULL OR 
     v_partida[i].totdeb IS NULL OR  
     v_partida[i].tothab IS NULL THEN 
     CONTINUE FOR 
  END IF 
  LET correl = (correl+1) 
 
  -- Verificando debe y haber
  IF (v_partida[i].totdeb>0) THEN
     LET xtipope = "D"
     LET xtotval = v_partida[i].totdeb 
  ELSE
     LET xtipope = "H"
     LET xtotval = v_partida[i].tothab 
  END IF 

  -- Grabando detalle de cuentas 
  SET LOCK MODE TO WAIT
  INSERT INTO ctb_dtransac 
  VALUES (w_mae_tra.lnktra    , -- link del encabezado
          correl              , -- correlativo 
          v_partida[i].numcta , -- numero de cuenta 
          v_partida[i].concpt , -- concepto de la linea 
          v_partida[i].totdeb , -- total debe 
          v_partida[i].tothab , -- total haber 
          xtipope)            

  -- Creando cuentas padre
  CALL librut003_CuentasPadre(
          w_mae_tra.lnktra, 
          correl,                  
          w_mae_tra.codemp,
          w_mae_tra.fecemi,      
          v_partida[i].numcta, 
          w_mae_emp.tipnom, 
          xtipope,             
          xtotval, 
          v_partida[i].totdeb,
          v_partida[i].tothab,
          "N",
          "N")
 END FOR 
END FUNCTION 

-- Subrutina para inicializar las variables de trabajo 

FUNCTION ctbing002_inival()
 DEFINE wfecha VARCHAR(80) 
 
 -- Inicializando vectores de datos
 INITIALIZE w_mae_tra.* TO NULL  
 CALL ctbing002_inivec()
 CLEAR FORM 

 -- Llenando combo de empresas
 CALL librut003_CbxEmpresas()
 -- Llenando combo de tipos de transacciones
 CALL librut003_CbxTiposTransaccionesContables()

 -- Desplegando fecha 
 LET wfecha = librut001_formatofecha(TODAY,1,wpais) CLIPPED
 CALL librut001_dpelement("labelz",wfecha)
END FUNCTION

-- Subrutina para inicializar y limpiar los vectores de trabajo

FUNCTION ctbing002_inivec()
 DEFINE i SMALLINT

 -- Inicializando vectores
 CALL v_partida.clear()

 LET totlin = 0
 DISPLAY ARRAY v_partida TO s_partida.* 
  BEFORE DISPLAY 
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION

-- Subrutina para desplegar los estados del menu 

FUNCTION ctbing002_EstadoMenu(operacion)
 DEFINE operacion SMALLINT

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("group1","Generacion de Polizas - MENU")
  WHEN 1 CALL librut001_dpelement("group1","Generacion de Polizas - VENTAS")
  WHEN 2 CALL librut001_dpelement("group1","Generacion de Polizas - COMPRAS")
 END CASE
END FUNCTION

