{
Mantenimiento de socios de negocio 
glbmae004.4gl 
}

-- Definicion de variables globales 

GLOBALS "glbglo004.4gl"
CONSTANT MAXVALCOB         = 100000
CONSTANT MAXDIACOB         = 90
CONSTANT MAXDIAPAG         = 90 
CONSTANT ValidaNitRepetido = 0

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarSociosNegocio")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("sociosnegocio") 
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo usuario del sistema
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de principal 
 CALL glbmae004_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION glbmae004_mainmenu()
 DEFINE titulo   STRING,
        existe   SMALLINT, 
        wpais    VARCHAR(255),
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "glbmae004a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header(progname,wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox de grupos de socio 
  CALL librut003_CbxGruposSocios() 

  -- Cargando combobox de paises
  CALL librut003_CbxPaises() 

  -- Cargando combobox de segmentos del cliente 
  CALL librut003_CbxSegmentos() 

  -- Menu de opciones
  MENU " Socios-Negocio"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,4,username) THEN
       HIDE OPTION "Buscar"
    END IF
    --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Modificar"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de socios." 
    CALL glbqbe004_sociosnegocio(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo socio."
    LET savedata = glbmae004_sociosnegocio(1) 
   COMMAND "Modificar"
    " Modificacion de un socio existente."
    CALL glbqbe004_sociosnegocio(2) 
   COMMAND "Borrar"
    " Eliminacion de un socio existente."
    CALL glbqbe004_sociosnegocio(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION glbmae004_sociosnegocio(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL glbqbe004_EstadoMenu(4,"") 
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL glbmae004_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nomsoc,
                w_mae_pro.esgrup, 
                w_mae_pro.numnit,
                w_mae_pro.numtel,
                w_mae_pro.numfax,
                w_mae_pro.dirsoc,
                w_mae_pro.nompro, 
                w_mae_pro.nomcon,
                w_mae_pro.bemail,
                w_mae_pro.tipsoc,
                w_mae_pro.codpai,
                w_mae_pro.codgru,
                w_mae_pro.codseg, 
                w_mae_pro.observ,
                w_mae_pro.maxval, 
                w_mae_pro.maxdia, 
                w_mae_pro.diacre, 
                w_mae_pro.estado  
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

    AFTER FIELD nomsoc  
    --Verificando nombre del socio de negocio 
    IF (LENGTH(w_mae_pro.nomsoc)=0) THEN
       ERROR "Error: nombre del socio invalido, VERIFICA."
       LET w_mae_pro.nomsoc = NULL
       NEXT FIELD nomsoc  
    END IF

    -- Verificando que no exista otro socio de negocio con el mismo nombre
    SELECT UNIQUE (a.codsoc)
     FROM  glb_sociosng a
     WHERE (a.codsoc != w_mae_pro.codsoc) 
       AND (a.nomsoc  = w_mae_pro.nomsoc) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion",
        " Existe otro socio con el mismo nombre, VERIFICA.",
        "information")
        NEXT FIELD nomsoc
     END IF 

   AFTER FIELD esgrup 
    --Verificando si el socio es grupo  
    IF w_mae_pro.esgrup IS NULL THEN
       ERROR "Error: socio es grupo invalido, VERIFICA."
       NEXT FIELD esgrup 
    END IF

   AFTER FIELD numnit  
    --Verificando numero de nit 
    IF (LENGTH(w_mae_pro.numnit)=0) THEN
       LET w_mae_pro.numnit = "N/A"
       DISPLAY BY NAME w_mae_pro.numnit  
    END IF

    -- Verificando que no exista otro NIT                      
    IF ValidaNitRepetido THEN 
     SELECT UNIQUE (a.numnit)
      FROM  glb_sociosng a
      WHERE (a.codsoc != w_mae_pro.codsoc) 
        AND (a.numnit  = w_mae_pro.numnit) 
        AND (a.numnit  != "N/A") 
      IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        "Atencion",
        "Existe otro socio con el mismo numero de NIT, VERIFICA.",
        "information")
        NEXT FIELD numnit
      END IF 
     END IF 

   AFTER FIELD dirsoc
    --Verificando direccion
    IF (LENGTH(w_mae_pro.dirsoc)=0) THEN
       ERROR "Error: direccion invalida, VERIFICA."
       LET w_mae_pro.dirsoc = NULL
       NEXT FIELD dirsoc  
    END IF

   AFTER FIELD tipsoc
    --Verificando localidad     
    IF w_mae_pro.tipsoc IS NULL THEN
       ERROR "Error: localidad invalida, VERIFICA."
       NEXT FIELD tipsoc 
    END IF

   AFTER FIELD codpai
    --Verificando pais
    IF w_mae_pro.codpai IS NULL THEN
       ERROR "Error: pais de origen invalido, VERIFICA."
       NEXT FIELD codpai  
    END IF

   AFTER FIELD codgru  
    --Verificando codgru 
    IF w_mae_pro.codgru IS NULL THEN
       ERROR "Error: regimen invalido, VERIFICA."
       NEXT FIELD codgru  
    END IF

   AFTER FIELD codseg
    --Verificando segmento
    IF w_mae_pro.codseg IS NULL THEN
       ERROR "Error: tipo de socio invalido, VERIFICA."
       NEXT FIELD codseg 
    END IF

   AFTER FIELD maxval 
    --Verificando valor de credito cobros  
    IF w_mae_pro.maxval IS NULL OR 
       w_mae_pro.maxval <0 OR
       w_mae_pro.maxval >MAXVALCOB THEN
       LET w_mae_pro.maxval = 0
       DISPLAY BY NAME w_mae_pro.maxval 
    END IF

   AFTER FIELD maxdia 
    --Verificando dias de credito cobros  
    IF w_mae_pro.maxdia IS NULL OR 
       w_mae_pro.maxdia <0 OR
       w_mae_pro.maxdia >MAXDIACOB THEN
       LET w_mae_pro.maxdia = 0
       DISPLAY BY NAME w_mae_pro.maxdia 
    END IF

   AFTER FIELD diacre 
    --Verificando dias de credito pagos
    IF w_mae_pro.diacre IS NULL OR 
       w_mae_pro.diacre <0 OR
       w_mae_pro.diacre >MAXDIAPAG THEN
       LET w_mae_pro.diacre = 0
       DISPLAY BY NAME w_mae_pro.diacre 
    END IF

   AFTER FIELD estado 
    --Verificando estado
    IF w_mae_pro.estado IS NULL THEN
       ERROR "Error: socio activo invalido, VERIFICA."
       NEXT FIELD estado 
    END IF
    
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL glbmae004_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando socio 
    CALL glbmae004_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL glbqbe004_EstadoMenu(0,"") 
    CALL glbmae004_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un socio 

FUNCTION glbmae004_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando socio ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.codsoc),0)
    INTO  w_mae_pro.codsoc
    FROM  glb_sociosng a
    IF (w_mae_pro.codsoc IS NULL) THEN
       LET w_mae_pro.codsoc = 1
    ELSE 
       LET w_mae_pro.codsoc = (w_mae_pro.codsoc+1)
    END IF 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO glb_sociosng   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.codsoc 

   --Asignando el mensaje 
   LET msg = "Socio (",w_mae_pro.codsoc USING "<<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE glb_sociosng
   SET    glb_sociosng.*      = w_mae_pro.*
   WHERE  glb_sociosng.codsoc = w_mae_pro.codsoc 

   --Asignando el mensaje 
   LET msg = "Socio (",w_mae_pro.codsoc USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando socios 
   DELETE FROM glb_sociosng 
   WHERE (glb_sociosng.codsoc = w_mae_pro.codsoc)

   --Asignando el mensaje 
   LET msg = "Socio (",w_mae_pro.codsoc USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL glbmae004_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION glbmae004_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.codsoc = 0 
   LET w_mae_pro.esgrup = 0 
   LET w_mae_pro.codcat = 0 
   LET w_mae_pro.maxval = 0 
   LET w_mae_pro.maxdia = 0 
   LET w_mae_pro.diacre = 0
   LET w_mae_pro.estado = 1 
   LET w_mae_pro.userid = username 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.nomsoc THRU w_mae_pro.estado 
 DISPLAY BY NAME w_mae_pro.codsoc,w_mae_pro.codgru,w_mae_pro.userid 
 THRU w_mae_pro.horsis 
END FUNCTION
