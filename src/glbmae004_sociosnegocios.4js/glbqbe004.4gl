{
glbqbe004.4gl 
Mantenimiento de socios de negocio         
}

{ Definicion de variables globales }

GLOBALS "glbglo004.4gl" 
DEFINE totlin,totpag     INT
DEFINE filename,pipeline STRING 

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION glbqbe004_sociosnegocio(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        titmenu             STRING,
        qry                 STRING,
        msg                 STRING,
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox de paises
  CALL librut003_cbxpaises() 

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL glbqbe004_EstadoMenu(operacion," ") 
   INITIALIZE qrytext,qrypart TO NULL
   CALL glbmae004_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codsoc,a.nomsoc,a.esgrup,a.numnit,a.numtel,
                                a.numfax,a.dirsoc,a.nompro,a.nomcon,a.bemail,
                                a.tipsoc,a.codpai,a.codgru,a.codseg,a.observ,
                                a.maxval,a.maxdia,a.diacre,a.estado,a.userid,
                                a.fecsis,a.horsis
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel       
     -- Salida
     CALL glbmae004_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codsoc,a.nomsoc,a.numnit,c.nomgru,b.nompai ",
                 " FROM glb_sociosng a,glb_mtpaises b,glb_gruprovs c ",
                 " WHERE b.codpai = a.codpai AND c.codgru = a.codgru ",
                 " AND ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_sociosneg SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_sociosng.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_sociosneg INTO v_sociosng[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_sociosneg
   FREE  c_sociosneg
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL glbqbe004_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_sociosng TO s_sociosng.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL glbmae004_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscando
      CALL glbmae004_inival(1)
      EXIT DISPLAY

     ON ACTION modificar
      -- Modificando 
      IF glbmae004_sociosnegocio(2) THEN
         EXIT DISPLAY 
      ELSE
         -- Desplegando datos
         CALL glbqbe004_datos(v_sociosng[ARR_CURR()].tcodsoc)
      END IF 

     ON KEY (CONTROL-M)
      -- Modificando 
      IF (operacion=2) THEN
       IF glbmae004_sociosnegocio(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL glbqbe004_datos(v_sociosng[ARR_CURR()].tcodsoc)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF glbqbe004_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Este socio ya tiene registros. \n Socio no puede borrarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de Borrar este socio ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL glbmae004_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION pagos         
      -- Desplegando pagos x compras 
      CALL glbqbe004_DetallePagos()   

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1]  = "Codigo Socio"
      LET arrcols[2]  = "Nombre del Socio"
      LET arrcols[3]  = "Agrupa Socios"
      LET arrcols[4]  = "Numero de NIT"
      LET arrcols[5]  = "Numero Telefono"
      LET arrcols[6]  = "Numero FAX"
      LET arrcols[7]  = "Direccion"
      LET arrcols[8]  = "Nombre Propiestario" 
      LET arrcols[9]  = "Nombre Contacto"
      LET arrcols[10] = "Direccion Email"
      LET arrcols[11] = "Localidad"      
      LET arrcols[12] = "Pais Origen"
      LET arrcols[13] = "Tipo de Socio"
      LET arrcols[14] = "Regimen Fiscal"  
      LET arrcols[15] = "Limite Credito Cobros"
      LET arrcols[16] = "Dias Credito Cobros"
      LET arrcols[17] = "Dia Credito Pagos"
      LET arrcols[18] = "Socio Activo"
      LET arrcols[19] = "Usuario Registro"
      LET arrcols[20] = "Fecha Registro"
      LET arrcols[21] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry        = 
      "SELECT a.codsoc,TRIM(a.nomsoc),",
             "CASE (a.esgrup) WHEN 1 THEN 'Si' WHEN 0 THEN 'No' END,",
             "\"'\"||a.numnit,a.numtel,",
             "a.numfax,TRIM(a.dirsoc),a.nompro,a.nomcon,a.bemail,",
             "CASE (a.tipsoc) WHEN 1 THEN 'NACIONAL' WHEN 2 THEN 'EXTRANJERO' END,",
             "b.nompai,d.nomseg,c.nomgru,",
             "a.maxval,a.maxdia,a.diacre,",
             "CASE (a.estado) WHEN 1 THEN 'Si' WHEN 2 THEN 'No' END,",
             "a.userid,a.fecsis,a.horsis",
        " FROM glb_sociosng a,glb_mtpaises b,glb_gruprovs c,vta_segmntos d ",
        " WHERE b.codpai = a.codpai ",
           "AND c.codgru = a.codgru ",
           "AND d.codseg = a.codseg ",
           "AND ",qrytext CLIPPED,
        " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res = librut002_excelreport("Socios de Negocio",qry,21,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
         CALL glbqbe004_datos(v_sociosng[ARR_CURR()].tcodsoc)
      END IF 

    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen socios con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Desplegando estado del menu
  CALL glbqbe004_EstadoMenu(0,"") 
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION glbqbe004_datos(wcodsoc)
 DEFINE wcodsoc LIKE glb_sociosng.codsoc,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  glb_sociosng a "||
              "WHERE a.codsoc = "||wcodsoc||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_sociosnegt SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_sociosnegt INTO w_mae_pro.*
 END FOREACH
 CLOSE c_sociosnegt
 FREE  c_sociosnegt

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nomsoc THRU w_mae_pro.estado 
 DISPLAY BY NAME w_mae_pro.codsoc,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION 

-- Subrutina para cargar el detalle de pagos 

FUNCTION glbqbe004_DetallePagos()
 DEFINE ctotpag DEC(14,2), 
        fx      ui.form 

 -- Cargando pagos   
 CALL v_pagos.clear()
 LET totpag = 1

 DECLARE cmov CURSOR FOR
 SELECT y.fecemi,
        x.nomabr, 
        c.nombco,
        y.numdoc,
        y.totdoc, 
        y.descrp, 
        y.userid,
        y.fecsis,
        y.horsis,
        "", 
        y.lnkbco 
  FROM  bco_mtransac y,bco_tipomovs x,bco_mcuentas d,glb_mtbancos c 
  WHERE y.codsoc = w_mae_pro.codsoc 
    AND y.estado = 1 
    AND x.tipmov = y.tipmov
    AND x.pagpro = 1 
    AND d.numcta = y.numcta 
    AND c.codbco = d.codbco 
  ORDER BY y.fecemi DESC,y.lnkbco DESC 

 LET ctotpag = 0 
 FOREACH cmov INTO v_pagos[totpag].*
  -- Totalizando 
  LET ctotpag = (ctotpag+v_pagos[totpag].ctotdoc) 

  -- Incrementando contador
  LET totpag = (totpag+1)
 END FOREACH
 CLOSE cmov
 FREE  cmov
 LET totpag = (totpag-1)

 -- Desplegando totales
 DISPLAY BY NAME ctotpag 

 -- Desplegando pagos   
 DISPLAY ARRAY v_pagos TO s_pagos.*
  ATTRIBUTE(COUNT=totpag,ACCEPT=FALSE)
  BEFORE DISPLAY 
   -- Verificando si hay pagos
   IF (totpag=0) THEN 
      CALL fgl_winmessage(
      "Atencion",
      "Socio sin pagos registrados.",
      "information")
      LET fx = DIALOG.getForm()
      CALL fx.ensureFieldVisible("formonly.codsoc")
      EXIT DISPLAY 
   END IF 

  ON ACTION cancel
   -- Salida
   LET fx = DIALOG.getForm()
   CALL fx.ensureFieldVisible("formonly.codsoc")
   EXIT DISPLAY

  ON ACTION pdf
   -- Impresion
   CALL glbqbe004_ReportePagos() 
 END DISPLAY

 -- Limpiando pagos   
 CALL v_pagos.clear()
END FUNCTION

-- Subrutina para generar el reporte de pagos en pdf

FUNCTION glbqbe004_ReportePagos() 

 -- Definiendo archivo de impresion
 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/facrpt001.spl"

 -- Asignando valores
 LET pipeline = "pdf"

 -- Iniciando reporte
 START REPORT glbqbe004_GeneraReporte TO filename  
  OUTPUT TO REPORT glbqbe004_GeneraReporte() 
 FINISH REPORT glbqbe004_GeneraReporte 

 -- Imprimiendo el reporte
 CALL librut001_sendreport(filename,
                           pipeline,
                           "",
                           "-l -p 8")
END FUNCTION

-- Subrutina para imprimir el reporte de pagos

REPORT glbqbe004_GeneraReporte() 
 DEFINE linea     CHAR(121),
        i         INT        

  OUTPUT LEFT   MARGIN 2 
         PAGE   LENGTH 72
         TOP    MARGIN 2
         BOTTOM MARGIN 2

 FORMAT 
  PAGE HEADER

   -- Definiendo linea de impresion 
   LET linea = "_____________________________________________________________",
               "_____________________________________________________________",
               "_____________________________________________________________",
               "_____________"


   -- Imprimiendo Encabezado
   PRINT COLUMN   1,"Cuentas x Pagar",
         COLUMN 100,PAGENO USING  "Pagina: <<"
   PRINT COLUMN   1,"glbqbe001",
         COLUMN  48,"PAGOS A SOCIOS DE NEGOCIO",
         COLUMN 100,"Fecha : ",TODAY USING "dd/mmm/yyyy"
   PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
         COLUMN 100,"Hora  : ",TIME

   PRINT linea
   PRINT "Socio [ ",w_mae_pro.nomsoc CLIPPED 

  ON EVERY ROW
   -- Imprimiendo detalle de pagos 
   FOR i = 1 TO totpag  
    PRINT COLUMN   1,v_pagos[i].cfecemi                             , 2 SPACES,
                     v_pagos[i].cnomabr                             , 2 SPACES,
                     v_pagos[i].cnombco                             , 2 SPACES,
                     v_pagos[i].cnumdoc                             , 2 SPACES,
                     v_pagos[i].ctotdoc       USING "#,###,##&.&&"   
                           
   END FOR

  ON LAST ROW
   -- Imprimiendo totales y pie de pagina 
   PRINT linea
END REPORT 
 
-- Subrutina para verificar si el socio ya tiene registros

FUNCTION glbqbe004_integridad()
 DEFINE conteo SMALLINT

 -- Verificando productos
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_products a
  WHERE (a.codsoc = w_mae_pro.codsoc) 
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     SELECT COUNT(*)
      INTO  conteo
      FROM  inv_mtransac a
      WHERE (a.codsoc = w_mae_pro.codsoc) 
      IF (conteo>0) THEN
         RETURN TRUE
      ELSE
         RETURN FALSE
      END IF 
  END IF
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION glbqbe004_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Socios - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Socios - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Socios - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Socios - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Socios - NUEVO")
 END CASE
END FUNCTION
