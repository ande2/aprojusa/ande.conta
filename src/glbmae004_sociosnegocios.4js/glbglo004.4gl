{ 
glbglo004.4gl
Mantenimiento de socios de negocio         
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "glbmae004"
DEFINE w_mae_pro   RECORD LIKE glb_sociosng.*,
       v_sociosng  DYNAMIC ARRAY OF RECORD
        tcodsoc    LIKE glb_sociosng.codsoc,
        tnomsoc    LIKE glb_sociosng.nomsoc, 
        tnumnit    LIKE glb_sociosng.numnit, 
        tnomgru    CHAR(30),
        tnompai    CHAR(40),
        tendrec    CHAR(1)
       END RECORD, 
       v_pagos     DYNAMIC ARRAY OF RECORD
        cfecemi    LIKE inv_dtransac.fecemi,
        cnomabr    CHAR(6), 
        cnombco    CHAR(15), 
        cnumdoc    CHAR(30),
        ctotdoc    DEC(14,2), 
        cdescrp    CHAR(300),
        cuserid    LIKE inv_dtransac.userid,
        cfecsis    LIKE inv_dtransac.fecsis,
        chorsis    LIKE inv_dtransac.horsis,
        crellen    CHAR(1)
       END RECORD,
       username    VARCHAR(15)
END GLOBALS

