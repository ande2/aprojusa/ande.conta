{ 
glbglo039.4gl
Mynor Ramirez
Mantenimiento de centros de costo 
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "glbmae039"
DEFINE w_mae_pro      RECORD LIKE glb_cencosto.*,
       v_centros      DYNAMIC ARRAY OF RECORD
        tcodcos       LIKE glb_cencosto.codcos,
        tnomcen       LIKE glb_cencosto.nomcen, 
        tendrec       CHAR(1) 
       END RECORD,
       username       VARCHAR(15)
END GLOBALS
