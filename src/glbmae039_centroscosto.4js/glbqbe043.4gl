{
glbqbe043.4gl 
Mantenimiento de cuentas bancarias 
}

{ Definicion de variables globales }

GLOBALS "glbglo043.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION glbqbe043_cuentas(operacion)

 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,
        msg                 STRING,
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL glbqbe043_EstadoMenu(operacion," ")
   INITIALIZE qrytext,qrypart TO NULL
   CALL glbmae043_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.numcta,a.codemp,a.codbco,a.nomcta,
                                a.cargos,a.abonos,a.salact,a.fulcar, 
                                a.fulabo,a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel 
     -- Salida
     CALL glbmae043_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.numcta,b.nombco,a.nomcta ",
                 " FROM bco_mcuentas a,glb_mtbancos b ",
                 " WHERE b.codbco = a.codbco AND ",qrytext CLIPPED,
                 " ORDER BY 2,1"

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_cuentas SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_cuentas.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_cuentas INTO v_cuentas[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_cuentas
   FREE  c_cuentas
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL glbqbe043_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_cuentas TO s_cuentas.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL glbmae043_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscar
      CALL glbmae043_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      LET res = glbmae043_cuentas(2) 

      -- Desplegando datos
      CALL glbqbe043_datos(v_cuentas[ARR_CURR()].tnumcta)

     ON KEY (CONTROL-M)   
      -- Modificando 
      IF (operacion=2) THEN 
         -- Modificando 
         LET res = glbmae043_cuentas(2) 

         -- Desplegando datos
        CALL glbqbe043_datos(v_cuentas[ARR_CURR()].tnumcta)
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      LET res =  glbqbe043_Integridad() 
      IF (res>0) THEN
         LET msg = " Esta cuenta ya tiene movimientos."||
                   "\n Cuenta no puede borrarse."
         CALL fgl_winmessage(" Atencion",msg,"stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de Borrar esta cuenta ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL glbmae043_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Cuenta"
      LET arrcols[2] = "Empresa"
      LET arrcols[3] = "Banco"
      LET arrcols[4] = "Nombre"
      LET arrcols[5] = "Cuenta Registro"
      LET arrcols[6] = "Fecha Registro"
      LET arrcols[7] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry = "SELECT a.numcta,b.nomemp,c.nombco,a.nomcta,a.userid,a.fecsis,a.horsis ",
                " FROM bco_mcuentas a,glb_empresas b,glb_mtbancos c ",
                " WHERE b.codemp = a.codemp AND c.codbco = a.codbco AND ",qrytext CLIPPED,
                " ORDER BY 3,1"

      -- Ejecutando el reporte
      LET res = librut002_excelreport("Cuentas Bancarias",qry,7,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
         CALL glbqbe043_datos(v_cuentas[ARR_CURR()].tnumcta)
      END IF 

    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen cuentas con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Asignando estado del menu 
  CALL glbqbe043_EstadoMenu(0,"")
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION glbqbe043_datos(wnumcta)
 DEFINE wnumcta   LIKE bco_mcuentas.numcta,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  bco_mcuentas a "||
              "WHERE a.numcta = '"||wnumcta||"'"

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_cuentast SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_cuentast INTO w_mae_pro.*
  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.codemp THRU w_mae_pro.fulabo
  DISPLAY BY NAME w_mae_pro.numcta,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
 END FOREACH
 CLOSE c_cuentast
 FREE  c_cuentast

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.codemp THRU w_mae_pro.fulabo 
 DISPLAY BY NAME w_mae_pro.numcta,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
END FUNCTION 

-- Subrutina para verificar si la cuenta tiene registros 

FUNCTION glbqbe043_Integridad()
 DEFINE conteo SMALLINT

 -- Verificando empresas
 SELECT COUNT(*)
  INTO  conteo
  FROM  glb_empresas a 
  WHERE (a.numcta = w_mae_pro.numcta)
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     -- Verificando transacciones de banco
     SELECT COUNT(*)
      INTO  conteo
      FROM  bco_transacs a 
      WHERE (a.numcta = w_mae_pro.numcta)
      IF (conteo>0) THEN
         RETURN TRUE
      ELSE
         -- Verificando transacciones bascula
         SELECT COUNT(*)
          INTO  conteo
          FROM  transacciones a 
          WHERE (a.numcta = w_mae_pro.numcta)
          IF (conteo>0) THEN
             RETURN TRUE
          ELSE 
             RETURN FALSE
          END IF 
      END IF 
  END IF
END FUNCTION

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION glbqbe043_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Cuentas Bancarias - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Cuentas Bancarias - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Cuentas Bancarias - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Cuentas Bancarias - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Cuentas Bancarias - NUEVO")
 END CASE
END FUNCTION
