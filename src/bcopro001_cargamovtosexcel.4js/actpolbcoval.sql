update bco_dtpoliza 
set tothab = (select a.totdoc from bco_mtransac a
              where a.lnkbco = bco_dtpoliza.lnkbco 
                and a.tipmov = 5 and a.tipori = 1)
where tipope = "H" and numcta = "1120202" 
and exists (select a.lnkbco from bco_mtransac a
            where a.lnkbco = bco_dtpoliza.lnkbco 
              and a.tipmov = 5 and a.tipori = 1)
