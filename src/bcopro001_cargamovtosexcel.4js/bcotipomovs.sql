
DBSCHEMA Schema Utility       INFORMIX-SQL Version 12.10.FC6WE







{ TABLE "sistemas".bco_tipomovs row size = 135 number of columns = 13 index size = 7 }

create table "sistemas".bco_tipomovs 
  (
    tipmov smallint not null ,
    nommov varchar(40,1) not null ,
    nomabr char(6) not null ,
    tipope smallint not null ,
    pagpro smallint not null ,
    polctb smallint not null ,
    impdoc smallint not null ,
    ctaref smallint not null ,
    cargas smallint,
    refcar varchar(50),
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (tipmov)  constraint "sistemas".pkbcotipomovs
  ) extent size 16 next size 16 lock mode row;

revoke all on "sistemas".bco_tipomovs from "public" as "sistemas";




