{
Carga de plantilla excel de bancos al sistema 
Mayo 2018  
}

IMPORT FGL fgl_excel
IMPORT util
IMPORT os 

DATABASE erpjuridico 

-- Definicion de variables globales 

CONSTANT DirectorioBase       = "c:\\\\"
CONSTANT ExtensionFiles       = "*.xlsx *.xls" 
CONSTANT progname             = "bcopro001"
CONSTANT TIPOTRNPOLIZAINGRESO = 101 
CONSTANT TIPOTRNPOLIZAEGRESO  = 102

-- Datos de la carga
DEFINE datos  RECORD
    codemp    LIKE bco_mtransac.codemp, 
    numcta    LIKE bco_mtransac.numcta, 
    codcos    LIKE bco_mtransac.codcos,
    numctb    LIKE bco_mcuentas.numctb 
END RECORD

-- Registros cargados 
DEFINE v_registros DYNAMIC ARRAY OF RECORD
    fecemi    DATE,
    codofi    CHAR(10),  
    desmov    VARCHAR(50),
    numdoc    VARCHAR(20),
    codref    VARCHAR(20),
    totcre    DEC(10,2), 
    totdeb    DEC(10,2),
    rlleno    CHAR(1) 
END RECORD 

-- Detalle de cuentas poliza contable
DEFINE v_partida   DYNAMIC ARRAY OF RECORD
    cnumcta   LIKE ctb_dtransac.numcta,
    ctotdeb   LIKE ctb_dtransac.totdeb,
    ctothab   LIKE ctb_dtransac.tothab,
    cconcpt   LIKE ctb_dtransac.concep
END RECORD

DEFINE porcenisv      DECIMAL(5,2)
DEFINE username       VARCHAR(15) 
DEFINE ArchivoErrores STRING 
DEFINE ArchivoPcError STRING 
DEFINE nrows,ncols    INT  
DEFINE totcta         INT  

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarCargarEstadoBancos")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("cargaestadobancos")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST-- Numero documento (excel) 

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo usuario
 LET username = FGL_GETENV("LOGNAME") 

 -- Menu de opciones 
 CALL bcopro001_Menu()
END MAIN 

-- Subutina para el menu de la carga 

FUNCTION bcopro001_Menu()
 DEFINE w       ui.Window,
        f       ui.Form,
        existe  SMALLINT, 
        wpais   STRING
 
 -- Abriendo la ventana para la carga 
 OPEN WINDOW wpro001a AT 5,2 WITH FORM "bcopro001a" 
  ATTRIBUTE(BORDER) 

  -- Desplegando datos del encabezado
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1)

  -- Inicializando datos
  CALL bcopro001_inival(1)

  -- Cargando combobox de empresas
  LET datos.codemp = librut003_DCbxEmpresaEnUsoXUsuario()
  IF datos.codemp IS NULL THEN
     CALL fgl_winmessage(
     "Atencion","Usuario sin empresa asignada, VERIFICA.","stop")
     CLOSE WINDOW wpro001a
     RETURN
  END IF
  DISPLAY BY NAME datos.codemp 

  -- Cargando cuentas
  LET datos.numcta = librut003_DCbxCuentasBanco(datos.codemp) 
  IF datos.numcta IS NULL THEN 
     CALL fgl_winmessage(
     "Atencion","Empresa sin cuentas de banco asignadas, VERIFICA.","stop") 
     CLOSE WINDOW wpro001a 
     RETURN 
  END IF  

  -- Cargando combobox de centros de costo 
  CALL librut003_CbxCentrosCosto("codcos") 

  -- Obteniendo porcentaje de impuesto sobre venta
  CALL librut003_parametros(8,1)
  RETURNING existe,porcenisv
  IF NOT existe THEN LET porcenisv = 0 END IF

  -- Menu de opciones
  MENU "Opciones" 
   BEFORE MENU
    -- Verificando accesos a opciones
    -- Seleccionar 
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "seleccionar"
    END IF

   ON ACTION seleccionar 
    CALL bcopro001_SeleccionarPlantilla()
   ON ACTION salir  
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU  
  END MENU
 CLOSE WINDOW wpro001a
END FUNCTION

-- Subrutina para seleccionar y cargar la plantilla en pantalla 

FUNCTION bcopro001_SeleccionarPlantilla()
 DEFINE rrow            fgl_excel.RowType 
 DEFINE rsheet          fgl_excel.sheetType
 DEFINE rworkbook       fgl_excel.workbookType 
 DEFINE celda           fgl_excel.cell
 DEFINE colchar         CHAR(50) 
 DEFINE textName        STRING
 DEFINE colname         STRING  
 DEFINE filename        STRING 
 DEFINE NombreArchivo   STRING 
 DEFINE ArchivoDestino  STRING 
 DEFINE ArchivoOrigen   STRING 
 DEFINE cellValue       STRING 
 DEFINE strtable        STRING
 DEFINE strregistros    STRING
 DEFINE idx,r,c,i,lg,j  INT 
 DEFINE numero          BIGINT 
 DEFINE deciml          DEC(12,2) 
 DEFINE valorcelda      CHAR(50) 
 DEFINE ppg             VARCHAR(10)
 DEFINE pp              DEC(14,6)
 DEFINE tg,r4,pb,conteo INT 
 DEFINE carga,nr        INT 

 -- Cargando plantilla
 TRY 
  -- Seleccionando el archivo en excel 
  LET filename = winopenfile(DirectorioBase,
                            "Archivos",
                            ExtensionFiles,
                            "Seleccion del Plantilla de Excel a Cargar")

  IF (LENGTH(filename)>0) THEN 
    -- Abriendo archivo 
    LET ArchivoOrigen  = librut001_replace(filename,"\/","\\",40) 

    -- Creando nombre archivo de errores a copiar a pc cliente 
    LET ArchivoPcError = filename 
    LET ArchivoPcError = librut001_replace(ArchivoPcError,"xlsx","csv",1)  
    LET ArchivoPcError = librut001_replace(ArchivoPcError,"xls","csv",1)  
    LET ArchivoPcError = librut001_replace(ArchivoPcError," ","",40)  

    -- Creando nombre archivo de errores a generar en server
    LET ArchivoErrores = os.Path.basename(filename) 
    LET ArchivoErrores = librut001_replace(ArchivoErrores,"xlsx","csv",1)  
    LET ArchivoErrores = librut001_replace(ArchivoErrores,"xls","csv",1)  
    LET ArchivoErrores = librut001_replace(ArchivoErrores," ","",40)  
    LET ArchivoErrores = FGL_GETENV("SPOOLDIR") CLIPPED,"/"||ArchivoErrores

    -- Creando nombre archivo a copiar en server
    LET NombreArchivo  = os.Path.basename(filename) 
    LET ArchivoDestino = os.Path.basename(filename) 
    LET ArchivoDestino = librut001_replace(ArchivoDestino," ","",40) 
    LET ArchivoDestino = FGL_GETENV("SPOOLDIR") CLIPPED,"/"||ArchivoDestino 

    -- Copiando el archivo del front end al back end 
    CALL librut001_getfile(ArchivoOrigen,ArchivoDestino)
  ELSE
    CALL fgl_winmessage(
    "Atencion","Carga de plantilla cancelada.","stop")
    RETURN 
  END IF 

  -- Abriendo libro de excel
  LET rworkbook = fgl_excel.workbook_open(ArchivoDestino.trim())
 
  -- Onteniendo hoja uno del libro de excel
  LET rsheet = rworkbook.getSheetAt(0) 

  -- Obteniendo numero de filas del libro de excel 
  LET nrows = rsheet.getPhysicalNumberOfRows()

  -- Asignando nombre de la ventana con numero de registros a cargar 
  LET textName = "Plantilla [ "||NombreArchivo||" ] Registros ["||(nrows-1)||"]"

  -- Obteniendo columnas de la primera fila 
  LET rrow  = rsheet.getRow(0)
  LET ncols = rrow.getLastCellNum() 

  -- Creando tabla temporal que contendra los datos de la carga
  CREATE TEMP TABLE tmp_excel 
  (
   fecemi DATE,
   codofi CHAR(10),  
   desmov VARCHAR(50),
   numdoc VARCHAR(20),
   codref VARCHAR(20),
   totcre DEC(10,2), 
   totdeb DEC(10,2),
   totsal DEC(10,2) 
   )  

  -- Cargando informacion del libro de excel y poblando tabla temporal
  -- Recorriendo filas 
  LET tg = 0 
  LET nr = 0 
  FOR r = 1 TO nrows 
   LET rrow = rsheet.getRow(r)
   IF rrow IS NULL THEN 
      CONTINUE FOR   
   END IF

   -- Desplegando progreso de la carga 
   LET tg = tg+1
   IF (tg=1) THEN 
    OPEN WINDOW wcarga AT 5,2 WITH FORM "progressb" 
     ATTRIBUTE(BORDER,TEXT=textName) 
   END IF 
   DISPLAY "Registro #"||tg||")" TO mensaje 

   -- Llenando progress bar 
   LET pp  = (tg*100)/(nrows-1) 
   LET r4  = pp
   LET ppg = r4
   LET ppg = ppg CLIPPED,"%"
   DISPLAY BY NAME r4,ppg
   CALL ui.Interface.refresh()

   -- Recorriendo columnas 
   LET strregistros = "INSERT INTO tmp_excel VALUES (" 
   LET ncols = rrow.getLastCellNum() 
   FOR c = 0 TO (ncols-1)
    LET Celda = rrow.getCell(c)
    LET cellValue = Celda 

    -- Verificando tipo de campo 
    LET carga = TRUE 
    CASE (c)
     WHEN  1 LET numero = cellValue LET cellValue = numero 
     WHEN  2 -- Verificando si registro aplica a carga 
             LET valorcelda = cellValue CLIPPED 
             SELECT COUNT(*)
              INTO  conteo
              FROM  bco_tipomovs a
              WHERE a.cargas  = 1
                AND a.refcar  = valorcelda
              IF (conteo>0) THEN
                 LET carga = TRUE 
              ELSE 
                 LET carga = FALSE 
                 EXIT FOR 
              END IF 
     WHEN  3 LET numero = cellValue LET cellValue = numero 
     WHEN  4 LET numero = cellValue LET cellValue = numero 
     WHEN  5 LET deciml = cellValue LET cellValue = deciml 
     WHEN  6 LET deciml = cellValue LET cellValue = deciml 
     WHEN  7 LET deciml = cellValue LET cellValue = deciml 
    END CASE 

    IF cellValue IS NULL THEN 
       LET cellValue = "" 
    END IF

    -- Creando registros 
    IF (c<(ncols-1)) THEN 
       LET strregistros = strregistros.trim(),"'",cellValue,"'," 
    ELSE
       LET strregistros = strregistros.trim(),"'",cellValue,"')" 
    END IF 
   END FOR    
   IF (ncols>0) THEN 
     -- Insertando registros 
     IF carga THEN
      PREPARE s2 FROM strregistros 
      EXECUTE s2
      FREE s2 

      -- Numero de registros insertados 
      LET nr = nr+1 
     END IF 
   END IF 
  END FOR    

  -- Cerrando ventana de progreso
  IF (tg>1) THEN
     CLOSE WINDOW wcarga 
  END IF 

  -- Desplegando y navegando la plantilla cargada de excel
  LET nrows = nr 
  LET textName = "Plantilla [ "||NombreArchivo||" ] Registros ["||nrows||"]"
  CALL bcopro001_NavegarPlantilla("tmp_excel",textName,nrows)

  -- Eliminando tabla temporal
  DROP TABLE tmp_excel 
 
 CATCH 
  CALL fgl_winmessage("Atencion",err_get(status),"stop") 
  
 END TRY 
END FUNCTION 

-- Desplegando y navengando la plantilla cargada en pantalla 

FUNCTION bcopro001_NavegarPlantilla(tabname,textName,totrows)
 DEFINE tabname  STRING
 DEFINE textName STRING
 DEFINE sql      STRING
 DEFINE nr       INT 
 DEFINE errores  INT 
 DEFINE totrows  INT 
 DEFINE totreg   INT 
 DEFINE haycarga BOOLEAN  

 -- Inicializando datos
 CALL bcopro001_inival(2)

 -- Llenando vector de datos 
 LET sql = "SELECT a.*,'' FROM "||tabName||" a"
 PREPARE p1 FROM sql
 DECLARE c1 CURSOR FOR p1
 LET totreg = 1
 FOREACH c1 INTO v_registros[totreg].*
  LET totreg = totreg+1 
 END FOREACH  
 CLOSE c1
 FREE  c1 
 LET totreg = totreg-1

 -- Desplegando registros
 CALL bcopro001_Registros(2,textName)  

 -- Inicializando datos
 LET datos.numcta = NULL
 LET datos.codcos = NULL 

 -- Desplegando datos 
 DIALOG ATTRIBUTES(UNBUFFERED,FIELD ORDER FORM)

  -- Ingresando datos
  INPUT BY NAME datos.numcta,datos.codcos
   ATTRIBUTE (WITHOUT DEFAULTS=TRUE) 

   BEFORE INPUT
    CALL Dialog.SetActionHidden("close",1)
  END INPUT 
 
  -- Desplegando datos 
  DISPLAY ARRAY v_registros TO s_registros.*
   ATTRIBUTE (COUNT=totreg,KEEP CURRENT ROW)
  END DISPLAY 

  ON ACTION cancel
   -- Salir sin cargar 
   EXIT DIALOG 

  ON ACTION cargar 
   -- Verificando ingreso de datos
   -- Numero de cuenta bancaria 
   IF datos.numcta IS NULL THEN
      CALL fgl_winmessage(
      "Atencion",
      "Debe seleccionarse la cuenta bancaria, VERIFICA.",
      "stop")
      CONTINUE DIALOG 
   END IF 

   -- Cuenta contable bancaria 
   INITIALIZE datos.numctb TO NULL 
   SELECT a.numctb INTO datos.numctb FROM bco_mcuentas a
    WHERE a.numcta = datos.numcta
   IF (LENGTH(datos.numctb)<=0) THEN
      CALL fgl_winmessage(
      "Atencion",
      "Cuenta bancaria sin cuenta contable definida, VERIFICA.",
      "stop")
      CONTINUE DIALOG 
   END IF 
   
   -- Centro de costo               
   IF datos.codcos IS NULL THEN
      CALL fgl_winmessage(
      "Atencion",
      "Debe seleccionarse el centro de costo, VERIFICA.",
      "stop")
      CONTINUE DIALOG 
   END IF 
 
   -- Cargando informacion a libro de bancos 
   IF librut001_YesOrNot(
      "Confirmacion",
      "Desea cargar plantilla? \n"||"Registros a cargar ["||totrows||"]",
      "Si","No","question") THEN

      -- Cargando Datos 
      CALL bcopro001_CargarDatosPlantilla(tabName,totrows)
      RETURNING haycarga,nr,errores 
      IF errores=0 THEN 
       IF (nr>0) THEN 
        CALL fgl_winmessage(
        "Atencion","Plantilla cargada. ["||nr||"] registros.","information") 
        CONTINUE DIALOG 
       ELSE 
        IF haycarga THEN
         CALL fgl_winmessage(
         "Atencion","Plantilla con todos sus datos ya registrados.","stop")
         CONTINUE DIALOG 
        END IF 
       END IF
      ELSE 
       CALL fgl_winmessage(
       "Atencion",
       "Carga de plantilla con observaciones en los datos."||
       "\nRegistros cargados  ["||nr||"]"||
       "\nRegistros no cargados ["||errores||"]"||
       "\nVer archivo ["||ArchivoPcError||"]", 
       "stop")
       CONTINUE DIALOG 
      END IF 
   END IF 
 END DIALOG 

 -- Inicializando datos
 CALL bcopro001_inival(2)
END FUNCTION

-- Subrutina para cargar los datos de la plantilla al libro de bancos
-- Crea movimiento en bancos y partida contable 

FUNCTION bcopro001_CargarDatosPlantilla(tabName,totrows)
 DEFINE nr              RECORD LIKE bco_mtransac.* 
 DEFINE ct              RECORD LIKE ctb_mtransac.* 
 DEFINE xtipnom         LIKE glb_empresas.tipnom
 DEFINE xtotval         LIKE ctb_mtransac.totdoc
 DEFINE totdeb,tothab   LIKE ctb_mtransac.totdoc
 DEFINE xnumcta         LIKE ctb_mcuentas.numcta
 DEFINE xtipcol         LIKE glb_ctasxrub.tipcol 
 DEFINE xtotdoc         LIKE bco_mtransac.totdoc 
 DEFINE wtotdoc         LIKE bco_mtransac.totdoc 
 DEFINE xtotisv         LIKE bco_mtransac.totdoc 
 DEFINE ppg             VARCHAR(10)
 DEFINE pp              DEC(14,6)
 DEFINE sql             STRING 
 DEFINE tabName         STRING
 DEFINE xtipope         CHAR(1) 
 DEFINE n,conteo,correl INT 
 DEFINE errores,rg,tg   INT
 DEFINE r4,pb,totrows,i INT 

 -- Obteniendo tipo de nomenclatura de la empresa 
 INITIALIZE xtipnom TO NULL
 SELECT a.tipnom INTO xtipnom FROM glb_empresas a
  WHERE a.codemp = datos.codemp     

 -- Limpiando barra de progreso 
 CALL librut002_InicioProgreso()

  -- Seleccionando datos 
  LET n       = 0
  LET rg      = 0 
  LET tg      = 0 
  LET errores = 0 

  -- Creando archivo de errores
  START REPORT bcopro001_ArchivoconErrores TO ArchivoErrores 

  -- Llenando tabla (bco_mtransac) 
  FOR tg = 1 TO totrows 
   -- Desplegando progreso de la carga 
   IF (tg=1) THEN 
    OPEN WINDOW wcarga AT 5,2 WITH FORM "progressb" 
     ATTRIBUTE(BORDER,TEXT="Progreso de Carga [ "||totrows||" Registros ]")
   END IF 
   DISPLAY "Registro #"||tg||
           " Codigo Referencia ( "||v_registros[tg].codref CLIPPED||")"
   TO      mensaje 

   -- Llenando progress bar 
   LET pp  = (tg*100)/totrows 
   LET r4  = pp
   LET ppg = r4
   LET ppg = ppg CLIPPED,"%"
   DISPLAY BY NAME r4,ppg
   CALL ui.Interface.refresh()
   
   -- Asignando campos llave para verificar que no se cargue un registro mas de una vez 
   LET nr.numcta = datos.numcta 
   LET nr.numdoc = v_registros[tg].numdoc -- Numero documento (excel) 
   LET nr.codref = v_registros[tg].codref -- Codigo referencia (excel) 

   -- Chequeando que no exista ya el registro por medio del codigo referencia 
   SELECT COUNT(*)
    INTO  conteo 
    FROM  bco_mtransac a 
    WHERE a.tipori = 1 -- Tipo de carga archivo de excel 
      AND a.codref = nr.codref 
    IF conteo>0 THEN
       -- Numero de registros con error 
       LET errores = errores+1 

       -- Agregando a archivo de errores csv 
       OUTPUT TO REPORT bcopro001_ArchivoConErrores(v_registros[tg].*, 
       "YA FUE CARGADO AL SISTEMA - CHEQUEAR COLUMNA (referencia)") 
       CONTINUE FOR 
    END IF 

   -- Obteniendo datos del tipo de movimiento 
   SELECT a.tipmov,a.nommov,a.tipope,a.codrub
    INTO  nr.tipmov,nr.nomsoc,nr.tipope,nr.codrub
    FROM  bco_tipomovs a
    WHERE a.cargas = 1
      AND a.refcar = v_registros[tg].desmov  
      AND a.codrub IS NOT NULL
    IF (status=NOTFOUND) THEN
       -- Numero de registros con error 
       LET errores = errores+1 

       -- Agregando a archivo de errores csv 
       OUTPUT TO REPORT bcopro001_ArchivoConErrores(v_registros[tg].*,
       "NO EXISTE PARAMETRIZACION EN EL SISTEMA PARA EL MOVIMIENTO") 
       CONTINUE FOR 
    END IF 

   -- Chequeando que no exista ya el registro por medio de la llave (tipmov,numcta,numdoc)
   SELECT COUNT(*)
    INTO  conteo 
    FROM  bco_mtransac a 
    WHERE a.lnkbco  IS NOT NULL 
      AND a.tipmov  = nr.tipmov
      AND a.numcta  = nr.numcta
      AND a.numdoc  = nr.numdoc
    IF conteo>0 THEN
       -- Numero de registros con error 
       LET errores = errores+1 

       -- Agregando a archivo de errores csv 
       OUTPUT TO REPORT bcopro001_ArchivoConErrores(v_registros[tg].*, 
       "YA FUE CARGADO AL SISTEMA - CHEQUEAR NUMERO DE DOCUMENTO") 
       CONTINUE FOR 
    END IF 

   -- Numero de registros procesados 
   LET n=n+1 

   -- Asignando datos 
   LET nr.lnkbco = 0
   LET nr.codemp = datos.codemp
   LET nr.codcos = datos.codcos
   LET nr.descrp = "CARGA DE EXCEL CODIGO DE REFERENCIA ("||
                   v_registros[tg].codref CLIPPED||")" 

   -- Verificando operacion del tipo de movimiento
   CASE (nr.tipope)
    WHEN 1 -- Cargo 
     LET nr.totdoc = v_registros[tg].totcre -- Creditos (excel)
     LET nr.totsal = nr.totdoc
    WHEN 0 -- Abono
     LET nr.totdoc = v_registros[tg].totdeb -- Debitos  (excel)
     LET nr.totsal = nr.totdoc*(-1) 
   END CASE 

   LET nr.codsoc = 0 
   LET nr.fecemi = v_registros[tg].fecemi -- Fecha del movimiento (excel) 
   LET nr.nomchq = NULL
   LET nr.tipsol = 0 
   LET nr.codaut = 0
   LET nr.totpag = 0
   LET nr.monext = 0
   LET nr.tascam = 0
   LET nr.estado = 1
   LET nr.nofase = 1
   LET nr.tipori = 1 -- Marcando el registro como carga de excel 
   LET nr.lnkori = 0 
   LET nr.userid = username 
   LET nr.fecsis = CURRENT 
   LET nr.horsis = CURRENT HOUR TO SECOND 
   LET nr.motanl = NULL
   LET nr.usranl = NULL
   LET nr.fecanl = NULL
   LET nr.horanl = NULL 
   LET nr.feccob = nr.fecemi 
   LET nr.ctaref = NULL
   LET nr.reisra = 0 
   LET nr.reishs = 0
   LET nr.otrdes = 0 
   LET nr.lnkcaj = 0 
   LET nr.numesc = NULL
   LET nr.fecesc = NULL
   LET nr.numser = NULL
   LET nr.numfac = NULL 

   -- Insertanto registro 
   TRY 
    -- 1.  Grabando en libro de bancos
    INSERT INTO bco_mtransac 
    VALUES (nr.*) 
    LET nr.lnkbco = SQLCA.SQLERRD[2]  

    -- 2.  Grabando poliza contable 
    -- 2.1 Encabezado de la poliza  

    -- Asignando datos
    LET ct.lnktra = 0
    LET ct.codemp = nr.codemp 

    -- Verificando tipo de poliza 
    CASE (nr.tipope)
     WHEN 1 LET ct.tiptrn = TIPOTRNPOLIZAINGRESO
     WHEN 0 LET ct.tiptrn = TIPOTRNPOLIZAEGRESO  
    END CASE  

    LET ct.fecemi = nr.feccob 
    LET ct.numdoc = librut003_NumeroPolizaAutomatica(ct.codemp,
                                                     ct.tiptrn,
                                                     ct.fecemi)
    LET ct.totdoc = nr.totdoc 
    LET ct.concep = nr.descrp 
    LET ct.codapl = "BCO" 
    LET ct.lnkapl = nr.lnkbco 
    LET ct.coddiv = 1 
    LET ct.estado = 1 
    LET ct.infadi = nr.nomsoc CLIPPED," # ("||nr.numdoc CLIPPED||
                    ") DE FECHA ("||ct.fecemi||") CUENTA BANCO ("||
                    nr.numcta CLIPPED||") ORIGEN (EXCEL) CODIGO REFERENCIA ("||
                    nr.codref CLIPPED||")" 
    LET ct.userid = username
    LET ct.fecsis = CURRENT 
    LET ct.horsis = CURRENT HOUR TO SECOND 

    INSERT INTO ctb_mtransac
    VALUES (ct.*) 
    LET ct.lnktra = SQLCA.SQLERRD[2] 

    -- 2.2 Detalle de la poliza  
    -- Creando vector de cuentas del detalle de la poliza  
    LET totcta = 0 

    -- Verificando operacion del tipo de movimiento
    CASE (nr.tipope)
     WHEN 1 -- Cargo 
      LET totdeb = nr.totdoc 
      LET tothab = 0 
     WHEN 0 -- Abono
      LET totdeb = 0 
      LET tothab = nr.totdoc 
    END CASE 

    -- Cuenta contable del banco 
    CALL bcopro001_AgregaCuentaDetallePoliza(datos.numctb,totdeb,tothab,"") 

    -- Cuentas contables por tipo de movimiento, centro de costo, rubro de gasto y empresa
    DECLARE cct CURSOR FOR
    SELECT a.numcta,a.tipcol,a.numcor 
     FROM  glb_ctasxrub a
     WHERE a.tipmov = nr.tipmov
       AND a.codcos = nr.codcos 
       AND a.codrub = nr.codrub
       AND a.codemp = nr.codemp 
     ORDER BY 3

    LET wtotdoc = nr.totdoc 
    FOREACH cct INTO xnumcta,xtipcol

     -- Parametrizar
     {IF (nr.tipmov=5) THEN
        IF xnumcta="1120202" THEN
           LET xtotdoc   = nr.totdoc/((porcenisv/100)+1)
           LET wtotdoc = xtotdoc 
        END IF 
        IF xnumcta="2120101" THEN
           LET xtotisv = nr.totdoc 
           LET xtotdoc = nr.totdoc/((porcenisv/100)+1)
           LET wtotdoc = xtotisv-xtotdoc 
        END IF 
     END IF} 

     -- Verificando tipo de columna de la cuenta
     CASE (xtipcol)
      WHEN "D" -- Debe
       LET totdeb = wtotdoc 
       LET tothab = 0 
      WHEN "H" -- Haber
       LET totdeb = 0 
       LET tothab = wtotdoc 
     END CASE 

     -- Cuenta contable complemento 
     CALL bcopro001_AgregaCuentaDetallePoliza(xnumcta,totdeb,tothab,"") 
    END FOREACH
    CLOSE cct
    FREE  cct 

    -- Creando detalle 
    LET correl = 0
    FOR i = 1 TO totcta 
     IF v_partida[i].cnumcta IS NULL THEN
        CONTINUE FOR
     END IF 
     LET correl = correl+1

     -- Verificando debe y haber
     IF (v_partida[i].ctotdeb>0) THEN
        LET xtipope = "D"
        LET xtotval = v_partida[i].ctotdeb 
     ELSE
        LET xtipope = "H"
        LET xtotval = v_partida[i].ctothab  
     END IF

     -- Grabando detalle de la poliza en bancos 
     INSERT INTO bco_dtpoliza 
     VALUES (nr.lnkbco           , -- link del encabezado bancos
             correl              , -- correlativo
             v_partida[i].cnumcta, -- numero de cuenta
             v_partida[i].ctotdeb, -- total debe
             v_partida[i].ctothab, -- total haber
             v_partida[i].cconcpt, -- concepto de la linea
             xtipope)              -- tipo de operacion 

     -- Grabando detalle de cuentas de poliza contable 
     INSERT INTO ctb_dtransac
     VALUES (ct.lnktra           , -- link del encabezado
             correl              , -- correlativo
             v_partida[i].cnumcta, -- numero de cuenta
             v_partida[i].cconcpt, -- concepto de la linea
             v_partida[i].ctotdeb, -- total debe
             v_partida[i].ctothab, -- total haber
             xtipope)

     -- Creando cuentas padre
     CALL librut003_CuentasPadre(
             ct.lnktra,
             correl,
             ct.codemp,
             ct.fecemi,
             v_partida[i].cnumcta,
             xtipnom,                        
             xtipope,
             xtotval,
             v_partida[i].ctotdeb,
             v_partida[i].ctothab,
             "N",
             "N")
    END FOR 

    -- Numero de registros cargados correctamente 
    LET rg = rg+1

   CATCH 
    -- Numero de registros con error 
    LET errores = errores+1 

    -- Agregando a archivo de errores csv 
    OUTPUT TO REPORT bcopro001_ArchivoConErrores(v_registros[tg].*, 
    "ERROR EN EL FORMATO DE LA LINEA DE EXCEL") 
   END TRY  
  END FOR

  IF (errores>0) THEN 
   -- Finalizando reporte
   FINISH REPORT bcopro001_ArchivoconErrores 

   -- Copiando el archivo del back end al fron end  
   CALL librut001_putfile(ArchivoErrores,ArchivoPcError)

   -- Cerrando ventana
   IF (tg>0) THEN 
      CLOSE WINDOW wcarga 
   END IF 

   RETURN FALSE,rg,errores 
  ELSE 
   -- Terminando reporte
   TERMINATE REPORT bcopro001_ArchivoconErrores 

   -- Cerrando ventana
   IF (tg>0) THEN 
      CLOSE WINDOW wcarga 
   END IF 

   RETURN TRUE,rg,0 
  END IF 
END FUNCTION

-- Subrutina para crear archivo csv con errores

REPORT bcopro001_ArchivoConErrores(imp)
 DEFINE imp        RECORD 
         fecemi    DATE,
         codofi    CHAR(10),  
         desmov    VARCHAR(50),
         numdoc    VARCHAR(20),
         codref    VARCHAR(20),
         totcre    DEC(10,2), 
         totdeb    DEC(10,2),
         rlleno    CHAR(1),
         observ    STRING 
        END RECORD 

  OUTPUT LEFT   MARGIN  0
         PAGE   LENGTH  1
         TOP    MARGIN  0
         BOTTOM MARGIN  0

 FORMAT
  ON EVERY ROW                     
   -- Imprimiendo datos
   PRINT imp.fecemi                ,",", 
         imp.codofi         CLIPPED,",",
         imp.desmov  	    CLIPPED,",",
         imp.numdoc         CLIPPED,",",
         imp.codref         CLIPPED,",",
         imp.totcre                ,",",
         imp.totdeb                ,",",
         imp.observ.trim() 
END REPORT 

-- Subrutina para desplegar mensaje de registros

FUNCTION bcopro001_Registros(operacion,msg) 
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 1 CALL librut001_dpelement(
         "group1","Datos de la Carga")
  WHEN 2 CALL librut001_dpelement(
         "group1","Datos de la Carga "||msg CLIPPED)
 END CASE
END FUNCTION

-- Subrutina para agregar cuentas contables al vector de detalle de la poliza 

FUNCTION bcopro001_AgregaCuentaDetallePoliza(dp) 
 DEFINE dp        RECORD
         cnumcta  LIKE ctb_dtransac.numcta,
         ctotdeb  LIKE ctb_dtransac.totdeb,
         ctothab  LIKE ctb_dtransac.tothab,
         cconcpt  LIKE ctb_dtransac.concep
        END RECORD

 -- Asignando datos
 LET totcta                    = totcta+1 
 LET v_partida[totcta].cnumcta = dp.cnumcta
 LET v_partida[totcta].ctotdeb = dp.ctotdeb
 LET v_partida[totcta].ctothab = dp.ctothab
 LET v_partida[totcta].cconcpt = dp.cconcpt
END FUNCTION 

-- Subrutina para inicializar datos 

FUNCTION bcopro001_inival(i)
 DEFINE i SMALLINT 

 -- Inicializando datos  
 CASE (i)
  WHEN 1
   INITIALIZE datos.* TO NULL
  WHEN 2
   INITIALIZE datos.numcta,datos.codcos TO NULL  
 END CASE 
 DISPLAY BY NAME datos.numcta,datos.codcos

 -- Inicializando vectores
 LET totcta = 0 
 CALL v_registros.clear() 
 CALL v_partida.clear() 
 DISPLAY ARRAY v_registros TO s_registros.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 

 -- Desplegando registros
 CALL bcopro001_Registros(1,"")  
END FUNCTION

