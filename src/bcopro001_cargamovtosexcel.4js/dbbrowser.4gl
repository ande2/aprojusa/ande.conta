-- a table description
DEFINE fields DYNAMIC ARRAY OF RECORD
    name STRING, -- a column name
    type STRING  -- a column type
END RECORD

DEFINE filter DYNAMIC ARRAY OF RECORD
    name STRING, -- a column name
    value STRING
END RECORD

-- Here the real demo starts: Create a DISPLAY ARRAY be a given table-name
-- browseTable: ui.Dialog.createDisplayArrayTo {{{

FUNCTION browseTable(tabname)
 DEFINE tabname STRING
 DEFINE d       ui.Dialog
 DEFINE done    BOOLEAN
 DEFINE sql     STRING

 -- Creando campos del arreglo
 CALL CamposArray(tabName)
 CALL filter.clear()

 -- Abriendo ventana 
 OPEN WINDOW w WITH 1 ROWS, 1 COLUMNS

  -- Creando arreglo 
  CALL createDisplayArrayForm(tabName)
  LET sql = "select * from ", tabName
  LET done = FALSE
  WHILE NOT done
   LET d = ui.Dialog.createDisplayArrayTo(fields, tabName)
   CALL d.addTrigger("ON ACTION Done")
   CALL d.addTrigger("ON ACTION Cargar")
   CALL d.addTrigger("ON ACTION Filter")

   -- Llenando arreglo 
   CALL LlenarArreglo(d, sql, tabName)

   WHILE TRUE -- Evaluando acciones 
    CASE d.nextEvent()
     WHEN "ON ACTION Done"
          LET done = TRUE
          EXIT WHILE
     WHEN "ON ACTION Cargar"
          -- CALL inputRow(tabName, d, "Update")
     WHEN "ON ACTION Filter"
          LET sql = getFilter(tabName)
          EXIT WHILE
     WHEN "AFTER DISPLAY"
          EXIT WHILE
    END CASE
   END WHILE
   CALL d.close()
  END WHILE
 CLOSE WINDOW w
END FUNCTION

-- Creando campos del arreglo en base a los campos de la tabla temporal 

FUNCTION CamposArray(tabName)
 DEFINE tabName STRING
 DEFINE h       base.SqlHandle
 DEFINE i       INT

 -- Creando campos 
 LET h = base.SqlHandle.create()
 CALL h.prepare("select * from " || tabName)
 CALL h.open()
 CALL fields.clear()
 FOR i = 1 TO h.getResultCount()
  LET fields[i].name = h.getResultName(i)
  LET fields[i].type = h.getResultType(i)
 END FOR
 CALL h.close()
END FUNCTION

-- Subrutina para llenar el arreglo con los datos de una tabla 

FUNCTION LlenarArreglo(d, sql, tabName)
 DEFINE d       ui.Dialog
 DEFINE tabName STRING
 DEFINE sql     STRING
 DEFINE h       base.SqlHandle
 DEFINE i,j     INT

 -- Llenando arreglo 
 LET h = base.SqlHandle.create()
 CALL h.prepare(sql)
 CALL h.open()
 CALL h.fetch()
 LET j = 0

 WHILE status == 0
  LET j = j + 1
  CALL d.setCurrentRow(tabName, j) 
  FOR i = 1 TO h.getResultCount()
   CALL d.setFieldValue(h.getResultName(i), h.getResultValue(i))
  END FOR
  CALL h.fetch()
  END WHILE
 CALL d.setCurrentRow(tabName, 1)
 CALL h.close()
END FUNCTION

-- }}}
-- filter: ui.Dialog.createConstructByName {{{

FUNCTION getFilter(tabName)
    DEFINE tabName STRING
    DEFINE d ui.Dialog
    DEFINE i, j INT
    DEFINE s, q, sql STRING

    OPEN WINDOW modify WITH 1 ROWS, 1 COLUMNS
    CALL createDetailForm(tabName, "Filter", TRUE)

    LET d = ui.Dialog.createConstructByName(fields)
    -- restore the filet from the previous run,
    -- this implements a "CONSTRUCT WITHOUT DEFAULTS"
    FOR i = 1 TO filter.getLength()
        CALL d.setFieldValue(filter[i].name, filter[i].value)
    END FOR
    --
    CALL d.addTrigger("ON ACTION accept")
    WHILE TRUE
        CASE d.nextEvent()
        WHEN "ON ACTION accept"
            CALL d.accept()
        WHEN "AFTER CONSTRUCT"
            EXIT WHILE
        END CASE
    END WHILE
    -- creates the query
    CALL filter.clear()
    FOR i = 1 TO fields.getLength()
        LET s = d.getQueryFromField(fields[i].name)
        -- getQueryFromField(name) returns a query expession from this field:
        -- example: returns 'fname matches "Antony*"' if name=="fname" and value=="Antony*"
        IF s IS NOT NULL THEN
            LET j = j + 1
            -- save the filter
            LET filter[j].name = fields[i].name
            LET filter[j].value = d.getFieldValue(fields[i].name)
            --
            IF q IS NOT NULL THEN
                LET q = q, " AND "
            END IF
            LET q = q, s CLIPPED
        END IF
    END FOR
    --DISPLAY q
    CALL d.close()
    CLOSE WINDOW modify

    LET sql = "select * from ", tabName
    IF q IS NOT NULL THEN
        LET sql = sql, " where ", q
    END IF
    RETURN (sql) -- The compiler reads RETURN sql as RETURN; SQL ..
END FUNCTION


--}}}
-- createDetailForm {{{
FUNCTION createDetailForm(tabName, title, forConstruct)
    DEFINE tabName STRING
    DEFINE title STRING
    DEFINE forConstruct BOOLEAN
    DEFINE i, width INT
    DEFINE colName, colType STRING
    DEFINE f ui.Form
    DEFINE w ui.Window

    DEFINE window, form, grid, label, formfield, edit  om.DomNode
    DEFINE screenRecord, link om.DomNode

    LET w = ui.Window.getCurrent()
    LET f = w.createForm("modify")
    LET form = f.getNode()


    LET window = form.getParent()
    CALL window.setAttribute("text", title || " " || tabName)

    LET grid = form.createChild("Grid")
    CALL grid.setAttribute("width", 2)
    CALL grid.setAttribute("height", fields.getLength())
    --
    LET screenRecord = form.createChild("RecordView")
    CALL screenRecord.setAttribute("tabName", tabName)
    --
    FOR i = 1 TO fields.getLength()
        LET colName = fields[i].name
        LET colType = fields[i].type
        --
        LET label = grid.createChild("Label")
        CALL label.setAttribute("posX", 0)
        CALL label.setAttribute("posY", i - 1)
        CALL label.setAttribute("gridWidth", 1)
        CALL label.setAttribute("text", colName)
        --
        LET formfield = grid.createChild("FormField")
        CALL formfield.setAttribute("colName", colName)
        CALL formfield.setAttribute("name", tabName || "." || colName)
        CALL formfield.setAttribute("sqlType", colType)
        CALL formfield.setAttribute("fieldId", i)
        CALL formfield.setAttribute("tabIndex", i + 1)
        --
        LET edit = formfield.createChild(IIF(colType=="DATE", "DateEdit", "Edit")) -- FIXME: make it better
        CALL edit.setAttribute("posX", 1)
        CALL edit.setAttribute("posY", i - 1)
        LET width = IIF(forConstruct, 20, bestWidth(colType))
        CALL edit.setAttribute("gridWidth", width)
        CALL edit.setAttribute("width", width)
        --
        LET link = screenRecord.createChild("Link")
        CALL link.setAttribute("colName", colName)
        CALL link.setAttribute("fieldIdRef", i)
    END FOR

    CALL form.writeXml("test.42f")
END FUNCTION

--}}}
-- createDisplayArrayForm {{{
FUNCTION createDisplayArrayForm(tabName)
    DEFINE tabName STRING
    DEFINE i INT
    DEFINE colName, colType STRING
    DEFINE f ui.Form
    DEFINE w ui.Window

    DEFINE window, form, grid, table, formfield, edit  om.DomNode

    LET w = ui.Window.getCurrent()
    LET f = w.createForm("test")
    LET form = f.getNode()

    --
    LET window = form.getParent()
    CALL window.setAttribute("text", tabName)
    --
    LET grid = form.createChild("Grid")
    CALL grid.setAttribute("width", 1)
    CALL grid.setAttribute("height", 1)
    LET table = grid.createChild("Table")
    CALL table.setAttribute("doubleClick", "update")
    CALL table.setAttribute("tabName", tabName)
    CALL table.setAttribute("pageSize", 10)
    CALL table.setAttribute("gridWidth", 1)
    CALL table.setAttribute("gridHeight", 1)
    FOR i = 1 TO fields.getLength()
        LET formfield = table.createChild("TableColumn")
        LET colName = fields[i].name
        LET colType = fields[i].type
        CALL formfield.setAttribute("text", colName)
        CALL formfield.setAttribute("colName", colName)
        CALL formfield.setAttribute("name", tabName || "." || colName)
        CALL formfield.setAttribute("sqlType", colType)
        --CALL formfield.setAttribute("fieldId", i)
        CALL formfield.setAttribute("tabIndex", i + 1)
        LET edit = formfield.createChild("Edit")
        CALL edit.setAttribute("width", bestWidth(colType))
    END FOR
    --CALL form.writeXml("test.42f")
END FUNCTION

FUNCTION bestWidth(t)
    DEFINE t STRING
    DEFINE i, j, len INT
    IF (i := t.getIndexOf('(', 1)) > 0 THEN
        IF (j := t.getIndexOf(',', i + 1)) == 0 THEN
            LET j = t.getIndexOf(')', i + 1)
        END IF
        LET len = t.subString(i + 1, j - 1)
        LET t = t.subString(1, i - 1)
    END IF
    CASE t
    WHEN "BOOLEAN"  RETURN 1
    WHEN "TINYINT"  RETURN 4
    WHEN "SMALLINT" RETURN 6
    WHEN "INTEGER"  RETURN 11
    WHEN "BIGINT"   RETURN 20
    WHEN "SMALLFLOAT" RETURN 14
    WHEN "FLOAT"   RETURN 14
    WHEN "STRING"  RETURN 20
    WHEN "DECIMAL" RETURN IIF(len IS NULL, 16, LEN + 2)
    WHEN "MONEY"   RETURN IIF(len IS NULL, 16, LEN + 2)
    WHEN "CHAR"    RETURN IIF(len IS NULL, 1, IIF (len > 20, 20, len))
    WHEN "VARCHAR" RETURN IIF(len IS NULL, 1, IIF (len > 20, 20, len))
    WHEN "DATE"    RETURN 10
    OTHERWISE
        RETURN 20
    END CASE
END FUNCTION
--}}}
