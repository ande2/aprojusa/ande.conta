

drop   table "sistemas".glb_ctasxrub; 
create table "sistemas".glb_ctasxrub
  (
    tipmov smallint,
    codcos smallint not null ,
    codrub smallint not null ,
    numcta char(20) not null ,
    codemp smallint not null ,
    tipcol char(1) not null ,
    numcor smallint not null ,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null
  ) extent size 16 next size 16 lock mode row;

alter table "sistemas".glb_ctasxrub add constraint (foreign key
    (codrub) references "sistemas".fac_rubgasto  on delete cascade
    constraint "sistemas".fkfacrubgasto1);
alter table "sistemas".glb_ctasxrub add constraint (foreign key
    (numcta,codemp) references "sistemas".ctb_mcuentas  constraint
    "sistemas".fkctbmcuentas1);
