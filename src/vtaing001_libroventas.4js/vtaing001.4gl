{
Programo : Mynor Ramirez
Objetivo : Mantenimiento de movimiento de ventas 
}

-- Definicion de variables globales

GLOBALS "vtaglb001.4gl"
DEFINE v_consulta DYNAMIC ARRAY OF RECORD 
        tlnkvta   INT, 
        ttipdoc   SMALLINT, 
        tcodsoc   CHAR(25), 
        tnommov   CHAR(25), 
        tnomsoc   CHAR(40),
        tfecemi   DATE,
        tnserie   CHAR(15),
        tnumdoc   CHAR(15),
        ttotdoc   DEC(14,2), 
        tnumesc   CHAR(20),   
        tfecesc   DATE,      
        tcodcos   SMALLINT,
        trellen   CHAR(1) 
       END RECORD 
DEFINE wpais            VARCHAR(255), 
       regreso          SMALLINT,
       existe           SMALLINT,
       msg              STRING, 
       totrec           INT 

-- Subrutina principal

MAIN
 -- Atrapando interrupts 
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarMovimientosVentas")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("movimientosventas")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ 

 -- Obteniendo datos del pais 
 CALL librut003_parametros(1,0)
 RETURNING existe,wpais

 -- Obteniendo usuario del sistema
 LET username = FGL_GETENV("LOGNAME")
 LET codigoapl= "VTA" 

 -- Menu de opciones
 CALL vtaing001_menu()
END MAIN

-- Subutina para el menu de movimientos de ventas  

FUNCTION vtaing001_menu()
 -- Abriendo la ventana del mantenimiento
 OPEN WINDOW wing004a AT 5,2  
  WITH FORM "vtaing001a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL librut001_header("vtaing001",wpais,1)

  -- Escondiendo tabla de consultas 
  CALL f.SetElementHidden("titulolista",1) 
  CALL f.SetElementHidden("tablaconsulta",1) 

  -- Cargando combobox de empresas
  LET gcodemp = librut003_DCbxEmpresaEnUsoXUsuario()
  IF gcodemp IS NULL THEN 
     CALL fgl_winmessage(
     "Atencion","Usuario sin empresa asignada, VERIFICA.","stop") 
     CLOSE WINDOW wing004a
     RETURN 
  END IF 

  -- Cargando tipos de documento de venta 
  LET gtipdoc = librut003_DCbxTiposDocumentoVentas()
  -- Cargando combobox de centros de costo pantala
  LET gcodcos = librut003_DCbxCentrosCosto("codcos")
  -- Cargando combobox de establecimientos sat
  CALL librut003_CbxEstablecimientosSAT() 
  -- Cargando combobox de centros de costo vector             
  CALL librut003_CbxCentrosCosto("tcodcos")

  -- Inicializando datos 
  CALL vtaing001_inival(1)

  -- Menu de opciones
  MENU "Opciones" 
   BEFORE MENU
    -- Verificando accesos a opciones
    -- Consultar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Consultar"
    END IF

   ON ACTION consultar  
    CALL vtaing001_Ventas(1)
   ON ACTION salir  
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU  
  END MENU
 CLOSE WINDOW wing004a
END FUNCTION

-- Subrutina para consultar/modificar ventas 

FUNCTION vtaing001_Ventas(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     STRING, 
        loop,existe         SMALLINT,
        operacion           SMALLINT,
        qryres,msg          CHAR(80),
        wherestado          STRING,
        cmdstr              STRING, 
        opc,totreg,res,arr  INTEGER 

 -- Verificando operacion
 CASE (operacion)
  WHEN 1 LET wherestado = NULL
 END CASE 

 -- Definiendo nivel de aislamiento 
 SET ISOLATION TO DIRTY READ 

 -- Buscando datos 
 LET loop = TRUE 
 WHILE loop 
  -- Inicializando las variables 
  CALL vtaing001_EstadoMenu(operacion," ")
  INITIALIZE qrytext,qrypart TO NULL
  LET int_flag = 0 
  CLEAR FORM
  DISPLAY gcodemp TO codemp 
                                                     
  -- Construyendo busqueda 
  CONSTRUCT BY NAME qrytext 
                 ON a.tipdoc,
                    a.fecemi,
                    a.numest,
                    a.codcos,
                    a.emisor,
                    a.nomemi,
                    a.recpto,
                    a.nomrec,
                    a.nserie,
                    a.nfolio, 
                    a.totnet,
                    a.totisv,
                    a.totdoc,
                    a.estado, 
                    a.lnkvta,
                    a.idocto,   
                    a.userid,
                    a.fecsis, 
                    a.horsis,
                    a.numesc,
                    a.fecesc 
   ON ACTION cancel
    -- Salida
    LET loop = FALSE
    EXIT CONSTRUCT
  END CONSTRUCT
  IF NOT loop THEN
     EXIT WHILE
  END IF 

  -- Preparando la busqueda 
  ERROR " Seleccionando datos ... por favor espere ..." ATTRIBUTE(CYAN) 

  -- Creando la busqueda 
  LET qrypart = "SELECT a.lnkvta,a.tipdoc,a.recpto,t.nomdoc,",
                       "a.nomrec,a.fecemi,a.nserie,a.nfolio,a.totdoc,",
                       "a.numesc,a.fecesc,a.codcos,'' ",
                 "FROM  vta_ldocutec a,glb_empresas e,vta_tipodocs t ",
                 "WHERE a.codemp = ",gcodemp," AND ",
                 qrytext CLIPPED,wherestado CLIPPED,
                  " AND e.codemp = a.codemp ",
                  " AND t.tipdoc = a.tipdoc ",
                  "ORDER BY a.lnkvta DESC" 

  -- Contando registros
  LET totreg = librut003_NumeroRegistros(qrypart)

  -- Declarando el cursor 
  PREPARE estqbx001 FROM qrypart
  DECLARE c_movimientos SCROLL CURSOR WITH HOLD FOR estqbx001  
     
  LET totrec = 1 
  CALL v_consulta.clear() 
  FOREACH c_movimientos INTO v_consulta[totrec].* 
   LET totrec = totrec+1
  END FOREACH
  CLOSE c_movimientos
  FREE  c_movimientos 
  LET totrec = totrec-1 

  -- Verificando si hay movimientos 
  IF (totrec=0) THEN 
     ERROR ""
     CALL fgl_winmessage(
     "Atencion",
     "No existen movimientos de ventas con el criterio seleccionado.",
     "stop") 
  ELSE
     ERROR "" 

     -- Escondiendo tabla de consultas 
     CALL f.SetElementHidden("titulolista",0) 
     CALL f.SetElementHidden("tablaconsulta",0) 

     -- Desplegando datos 
     DIALOG ATTRIBUTES(UNBUFFERED,FIELD ORDER FORM)
 
      -- Desplegando movimientos
      DISPLAY ARRAY v_consulta TO s_consulta.* 
       ATTRIBUTE (COUNT=totrec,KEEP CURRENT ROW) 

       BEFORE DISPLAY 
        CALL Dialog.SetActionHidden("close",1)

        -- Verificando tipo de operacion 
        CASE (operacion)
         WHEN 1 -- Desabilitando acciones              
          -- Verificando acceso a opcion de modificacion 
          IF NOT seclib001_accesos(progname,2,username) THEN
             CALL Dialog.SetActionActive("modificar",0)
          END IF
        END CASE

        -- Desplegando datos 
        LET msg = " - Registros [ ",totreg||" ]"
        CALL vtaing001_EstadoMenu(operacion,msg CLIPPED)

       BEFORE ROW 
        LET arr = DIALOG.getcurrentrow("s_consulta") 
        IF (arr>totrec) THEN
            CALL FGL_SET_ARR_CURR(totrec)
        END IF

        -- Desplegando datos 
        CALL vtaing001_datos(v_consulta[arr].tlnkvta) 

       AFTER ROW 
        LET arr = DIALOG.getcurrentrow("s_consulta") 

        -- Desplegando datos 
        CALL vtaing001_datos(v_consulta[arr].tlnkvta) 

       ON ACTION modificar 
        LET arr = DIALOG.getcurrentrow("s_consulta") 

        -- Verificando si documento de venta esta anulado           
        IF w_mae_trc.estado=0 THEN
           CALL fgl_winmessage(
           "Atencion", 
           "Movimiento de ventas esta anulado, VERIFICA.",
           "stop") 
           CONTINUE DIALOG  
        END IF 

        -- Regresando estado del menu
        CALL vtaing001_EstadoMenu(2,msg CLIPPED)

        -- Modificando movimientos de venta 
        CALL vtaing001_ModificarVentas(arr) 

        -- Desplegando datos 
        CALL vtaing001_datos(v_consulta[arr].tlnkvta) 

        -- Regresando estado del menu
        CALL vtaing001_EstadoMenu(operacion,msg CLIPPED)

       ON ACTION partida          
        -- Ejecutando consulta de la partida contable 
        IF ui.Interface.getChildInstances("transaccionesctb")==0 THEN
           LET cmdstr = "fglrun transaccionesctb.42r 1 "||
                        w_mae_trc.lnkvta||" "||codigoapl CLIPPED 
           RUN cmdstr WITHOUT WAITING
        ELSE
           CALL fgl_winmessage(
           "Atencion","Consulta de registro contable ya en ejecucion.","stop")
        END IF 
      END DISPLAY 

      ON ACTION prevrow   
       -- Siguiente fila 

      ON ACTION nextrow   
       -- Fila Anterior  

      ON ACTION consultar  
       -- Volver a consultar 
       EXIT DIALOG  

      ON ACTION cancel
       -- Salida 
       LET loop = FALSE
       EXIT DIALOG 

      ON KEY(F4,CONTROL-E)
       -- Salida 
       LET loop = FALSE
       EXIT DIALOG 
     END DIALOG 
 
     -- Escondiendo tabla de consultas 
     CALL f.SetElementHidden("titulolista",1) 
     CALL f.SetElementHidden("tablaconsulta",1) 
  END IF     
 END WHILE

 -- Desplegando estado del menu 
 CALL vtaing001_estadoMenu(0,"")

 -- Inicializando datos 
 CALL vtaing001_inival(1)
END FUNCTION 

-- Subrutina para desplegar los datos del movimiento de venta 

FUNCTION vtaing001_datos(xlnkvta)
 DEFINE xlnkvta LIKE vta_ldocutec.lnkvta,
        existe  SMALLINT

 -- Obteniendo datos del movimiento de venta 
 INITIALIZE w_mae_trc.* TO NULL  
 CALL librut003_BMovimientoVentas(xlnkvta) 
 RETURNING w_mae_trc.*,existe

 -- Desplegando datos del movimiento de venta 
 DISPLAY BY NAME w_mae_trc.codemp, 
                 w_mae_trc.tipdoc,
                 w_mae_trc.numest, 
                 w_mae_trc.codcos,
                 w_mae_trc.fecemi,
                 w_mae_trc.emisor,
                 w_mae_trc.nomemi, 
                 w_mae_trc.recpto,
                 w_mae_trc.nomrec, 
                 w_mae_trc.nserie,
                 w_mae_trc.nfolio, 
                 w_mae_trc.totnet,
                 w_mae_trc.totisv,
                 w_mae_trc.totdoc,
                 w_mae_trc.estado, 
                 w_mae_trc.lnkvta,
                 w_mae_trc.idocto, 
                 w_mae_trc.userid, 
                 w_mae_trc.fecsis, 
                 w_mae_trc.horsis,
                 w_mae_trc.numesc,
                 w_mae_trc.fecesc 

 -- Desplegando detalle de la poliza contable
 CALL vtaing001_PolizaContable()
END FUNCTION 

-- Subrutina para desplegar el detalle de la poliza contable 

FUNCTION vtaing001_PolizaContable() 
 -- Selecionando datos de la poliza 
 CALL v_partida.clear() 
 DECLARE cpoliza CURSOR FOR 
 SELECT y.numcta, 
        z.nomcta,
        y.totdeb,
        y.tothab,
        y.concep,
        "", 
        y.correl
  FROM  ctb_mtransac x,ctb_dtransac y,ctb_mcuentas z 
  WHERE x.lnktra = y.lnktra 
    AND x.lnkapl = w_mae_trc.lnkvta
    AND x.codapl = codigoapl 
    AND z.codemp = x.codemp 
    AND z.numcta = y.numcta
  ORDER BY y.correl 

 LET totlin = 1 
 FOREACH cpoliza INTO v_partida[totlin].*
  LET totlin=totlin+1 
 END FOREACH
 CLOSE cpoliza
 FREE cpoliza
 LET totlin=totlin-1 

 -- Desplegando poliza 
 DISPLAY ARRAY v_partida TO s_partida.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION 

-- Subutina para la modificacion de los datos del movimiento de ventas   

FUNCTION vtaing001_ModificarVentas(linea) 
 DEFINE loop,scr   SMALLINT,
        opc,arr    SMALLINT, 
        linea      SMALLINT, 
        retroceso  SMALLINT 

 -- Iniciando el loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop

  -- Modificando datos 
  INPUT ARRAY v_consulta WITHOUT DEFAULTS FROM s_consulta.*
   ATTRIBUTE(COUNT=totrec,INSERT ROW=FALSE,ACCEPT=FALSE,CANCEL=FALSE,
             DELETE ROW=FALSE,UNBUFFERED=TRUE,FIELD ORDER FORM)

   ON ACTION cancel 
    -- Cancelar
    LET retroceso = TRUE
    LET loop      = FALSE
    EXIT INPUT

   BEFORE INPUT 
    CALL DIALOG.setActionHidden("append",TRUE)
    CALL FGL_SET_ARR_CURR(linea)

   BEFORE ROW
    LET totrec = ARR_COUNT()

   ON CHANGE tnumesc
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Grabando cambio
    UPDATE vta_ldocutec 
    SET    vta_ldocutec.numesc = v_consulta[arr].tnumesc
    WHERE  vta_ldocutec.lnkvta = v_consulta[arr].tlnkvta 

   ON CHANGE tfecesc
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Grabando cambio
    UPDATE vta_ldocutec 
    SET    vta_ldocutec.fecesc = v_consulta[arr].tfecesc
    WHERE  vta_ldocutec.lnkvta = v_consulta[arr].tlnkvta 

   ON CHANGE tcodcos
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Grabando cambio
    UPDATE vta_ldocutec 
    SET    vta_ldocutec.codcos = v_consulta[arr].tcodcos
    WHERE  vta_ldocutec.lnkvta = v_consulta[arr].tlnkvta 

   AFTER INPUT 
    LET totrec = ARR_COUNT()
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  LET loop = FALSE 
 END WHILE
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION vtaing001_inival(i)
 DEFINE i       SMALLINT,
        wfecha  VARCHAR(80) 
 
 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1  
   INITIALIZE w_mae_trc.* TO NULL 
   CALL vtaing001_inivec()
   CLEAR FORM 
   LET w_mae_trc.codemp = gcodemp  
   LET w_mae_trc.tipdoc = gtipdoc 

   -- Obteniendo datos de la empresa 
   INITIALIZE w_mae_emp.* TO NULL 
   CALL librut003_bempresa(w_mae_trc.codemp)
   RETURNING w_mae_emp.*,existe 
 END CASE 

 DISPLAY BY NAME w_mae_trc.codemp

 -- Desplegando fecha 
 LET wfecha = librut001_formatofecha(TODAY,1,wpais) CLIPPED
 CALL librut001_dpelement("labelz",wfecha)
END FUNCTION

-- Subrutina para inicializar el vector de trabajo

FUNCTION vtaing001_inivec()
 -- Inicializando poliza contable 
 CALL v_partida.clear() 
 LET totlin = 0 
 DISPLAY ARRAY v_partida TO s_partida.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION  

-- Subrutina para desplegar los estados del menu 

FUNCTION vtaing001_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement(
         "group1","Datos del Movimiento de Venta - MENU")
  WHEN 1 CALL librut001_dpelement(
         "group1","Datos del Movimiento de Venta - CONSULTAR"||msg CLIPPED)
  WHEN 2 CALL librut001_dpelement(
         "group1","Datos del Movimiento de Venta - MODIFICAR") 
 END CASE
END FUNCTION
