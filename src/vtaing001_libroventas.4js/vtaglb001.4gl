{ 
Programo : Mynor Ramirez 
Objetivo : Programa de variables globales 
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "vtaing001"
DEFINE w_mae_emp   RECORD LIKE glb_empresas.*,
       w_mae_trc   RECORD LIKE vta_ldocutec.*, 
       username    LIKE glb_permxusr.userid,
       b           ui.ComboBox,
       cba         ui.ComboBox,
       w           ui.Window,
       f           ui.Form, 
       v_partida   DYNAMIC ARRAY OF RECORD
        cnumcta    LIKE ctb_dtransac.numcta,
        cnomcta    LIKE ctb_mcuentas.nomcta,
        ctotdeb    LIKE ctb_dtransac.totdeb,
        ctothab    LIKE ctb_dtransac.tothab,
        cconcpt    LIKE ctb_dtransac.concep,
        crellen    CHAR(1)
       END RECORD,
       gcodemp     LIKE vta_ldocutec.codemp,
       gtipdoc     LIKE vta_ldocutec.tipdoc,
       gcodcos     LIKE vta_ldocutec.codcos,
       totlin      INTEGER, 
       codigoapl   CHAR(3)
END GLOBALS
