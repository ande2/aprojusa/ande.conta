{
ctbglo001.4gl
MRS
Mantenimiento de nomenclaturas
}

DATABASE erpjuridico

{ Definicion de variables globales }

GLOBALS
CONSTANT progname = "ctbmae001"
DEFINE w_mae_pro   RECORD LIKE ctb_tiposnom.*,
       w_mae_dig   RECORD
        digt01     SMALLINT, 
        digt02     SMALLINT, 
        digt03     SMALLINT, 
        digt04     SMALLINT, 
        digt05     SMALLINT, 
        digt06     SMALLINT, 
        digt07     SMALLINT, 
        digt08     SMALLINT, 
        digt09     SMALLINT, 
        digt10     SMALLINT 
       END RECORD, 
       v_nomenclaturas DYNAMIC ARRAY OF RECORD
        tipnom     LIKE ctb_tiposnom.tipnom,
        desnom     LIKE ctb_tiposnom.desnom,
        numniv     LIKE ctb_tiposnom.numniv,
        format     LIKE ctb_tiposnom.format 
       END RECORD,
       v_nivel     ARRAY[10] OF RECORD
        digits     SMALLINT
       END RECORD,
       username    VARCHAR(15),
       tot_niv     SMALLINT,
       blancos     CHAR(1)
END GLOBALS
