{
ctbmae001.4gl 
Mantenimiento de nomenclaturas 
FL                 
noviembre 2014 
}

-- Definicion de variables globales 

GLOBALS "ctbglo001.4gl"
DEFINE v_nivel      ARRAY[10] OF RECORD
        digito      SMALLINT 
       END RECORD,
       tn           SMALLINT 

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("nomenclaturas")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo usuario del sistema 
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de principal 
 CALL ctbmae001_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION ctbmae001_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255), 
        savedata SMALLINT, 
        existe   SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "ctbmae001a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0) 
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Nomenclaturas"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,4,username) THEN
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Modificar"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de nomenclaturas."
    CALL ctbqbe001_nomenclaturas(1) 
   COMMAND "Nuevo"
    " Ingreso de una nueva nomenclatura."
    LET savedata = ctbmae001_nomenclaturas(1) 
   COMMAND "Modificar"
    " Modificacion de una nomenclatura existente."
    CALL ctbqbe001_nomenclaturas(2) 
   COMMAND "Borrar"
    " Eliminacion de una nomenclatura existente."
    CALL ctbqbe001_nomenclaturas(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION ctbmae001_nomenclaturas(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL ctbqbe001_EstadoMenu(4,"")
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL ctbmae001_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.desnom,
                w_mae_pro.fecape,
                w_mae_pro.numniv,
                w_mae_pro.nivsep,
                w_mae_pro.sepdor,
                w_mae_dig.digt01,
                w_mae_dig.digt02, 
                w_mae_dig.digt03, 
                w_mae_dig.digt04, 
                w_mae_dig.digt05, 
                w_mae_dig.digt06, 
                w_mae_dig.digt07, 
                w_mae_dig.digt08, 
                w_mae_dig.digt09, 
                w_mae_dig.digt10 
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   AFTER FIELD desnom  
    --Verificando nombre de la nomenclatura
    IF (LENGTH(w_mae_pro.desnom)=0) THEN
       ERROR "Error: nombre de la nomenclatura invalido, VERIFICA"
       LET w_mae_pro.desnom = NULL
       NEXT FIELD desnom  
    END IF

    -- Verificando que no exista otra nomenclatura con el mismo nombre
    SELECT UNIQUE (a.tipnom)
     FROM  ctb_tiposnom a
     WHERE (a.tipnom != w_mae_pro.tipnom) 
       AND (a.desnom  = w_mae_pro.desnom) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion",
        " Existe otra nomenclatura con el mismo nombre, VERIFICA.",
        "information")
        NEXT FIELD desnom
     END IF 

   AFTER FIELD fecape
    --Verificando fecha de apertura 
    IF w_mae_pro.fecape IS NULL OR 
       w_mae_pro.fecape >TODAY THEN
       LET w_mae_pro.fecape = TODAY
       DISPLAY BY NAME w_mae_pro.fecape 
    END IF 

   AFTER FIELD numniv  
    --Verificando tipo de saldo 
    IF (w_mae_pro.numniv IS NULL) OR
       (w_mae_pro.numniv<=0) THEN 
       ERROR "Error: numero de niveles invalido, VERIFICA."
       LET w_mae_pro.numniv = NULL
       NEXT FIELD numniv  
    END IF

   AFTER FIELD nivsep  
    --Verificando nivel despues del separador 
    IF (w_mae_pro.nivsep IS NULL) OR
       (w_mae_pro.nivsep =0) THEN 
       LET w_mae_pro.nivsep = w_mae_pro.numniv 
       LET w_mae_pro.sepdor = NULL
       DISPLAY BY NAME w_mae_pro.nivsep,w_mae_pro.sepdor 
    END IF

    -- Verificado que nivel del separador no sea mayor al numero de niveles 
    IF (w_mae_pro.nivsep>w_mae_pro.numniv) THEN 
       ERROR "Error: nivel del separador invalido, VERIFICA." 
       NEXT FIELD nivsep 
    END IF 

    -- Verificando si hay separador o no 
    IF (w_mae_pro.nivsep=w_mae_pro.numniv) THEN
       LET w_mae_pro.sepdor = NULL
       DISPLAY BY NAME w_mae_pro.sepdor 
       CALL Dialog.SetFieldActive("sepdor",FALSE) 
    ELSE
       CALL Dialog.SetFieldActive("sepdor",TRUE) 
    END IF 

   AFTER FIELD sepdor  
    --Verificando separador 
    IF w_mae_pro.sepdor IS NULL THEN 
       ERROR "Error: separador invalido, VERIFICA." 
       NEXT FIELD sepdor 
    END IF

   AFTER INPUT 
    -- Formateando la cuenta segun niveles
    CALL ctbmae001_FormatoCuenta() 
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL ctbmae001_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando nomenclatura
    CALL ctbmae001_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL ctbqbe001_EstadoMenu(0,"")
    CALL ctbmae001_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar una nomenclatura

FUNCTION ctbmae001_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando nomenclatura ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

  -- Asignando datos
  LET w_mae_pro.dig001 = w_mae_dig.digt01
  LET w_mae_pro.dig002 = w_mae_dig.digt02
  LET w_mae_pro.dig003 = w_mae_dig.digt03
  LET w_mae_pro.dig004 = w_mae_dig.digt04
  LET w_mae_pro.dig005 = w_mae_dig.digt05
  LET w_mae_pro.dig006 = w_mae_dig.digt06
  LET w_mae_pro.dig007 = w_mae_dig.digt07
  LET w_mae_pro.dig008 = w_mae_dig.digt08
  LET w_mae_pro.dig009 = w_mae_dig.digt09
  LET w_mae_pro.dig010 = w_mae_dig.digt10

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.tipnom),0)
    INTO  w_mae_pro.tipnom 
    FROM  ctb_tiposnom a
    IF (w_mae_pro.tipnom IS NULL) THEN
       LET w_mae_pro.tipnom = 1
    ELSE
       LET w_mae_pro.tipnom = w_mae_pro.tipnom+1
    END IF

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO ctb_tiposnom   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.tipnom 

   --Asignando el mensaje 
   LET msg = " Nomenclatura (",w_mae_pro.tipnom USING "<<<<<<",") registrada."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE ctb_tiposnom
   SET    ctb_tiposnom.*      = w_mae_pro.*
   WHERE  ctb_tiposnom.tipnom = w_mae_pro.tipnom 

   --Asignando el mensaje 
   LET msg = " Nomenclatura (",w_mae_pro.tipnom USING "<<<<<<",") actualizada."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando nomenclaturas
   DELETE FROM ctb_tiposnom 
   WHERE (ctb_tiposnom.tipnom = w_mae_pro.tipnom)

   --Asignando el mensaje 
   LET msg = " Nomenclatura (",w_mae_pro.tipnom USING "<<<<<<",") borrada."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL ctbmae001_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION ctbmae001_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.*,w_mae_dig.* TO NULL
   LET w_mae_pro.tipnom = 0
   LET w_mae_pro.nivsep = 0
   LET w_mae_pro.sepdor = NULL 
   LET w_mae_pro.userid = username  
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.tipnom,w_mae_pro.desnom THRU w_mae_pro.format 
 DISPLAY BY NAME w_mae_pro.tipnom,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
END FUNCTION

-- Subrutina para crear el formato de la cuenta contable  

FUNCTION ctbmae001_FormatoCuenta() 
 DEFINE i,j,ini,pos SMALLINT,
        x_cuenta    CHAR(40)

 # Lenando vector de digitos
 LET tn = 0 
 IF (w_mae_dig.digt01>0) THEN LET v_nivel[1].digito  = w_mae_dig.digt01 LET tn=tn+1 END IF 
 IF (w_mae_dig.digt02>0) THEN LET v_nivel[2].digito  = w_mae_dig.digt02 LET tn=tn+1 END IF 
 IF (w_mae_dig.digt03>0) THEN LET v_nivel[3].digito  = w_mae_dig.digt03 LET tn=tn+1 END IF 
 IF (w_mae_dig.digt04>0) THEN LET v_nivel[4].digito  = w_mae_dig.digt04 LET tn=tn+1 END IF 
 IF (w_mae_dig.digt05>0) THEN LET v_nivel[5].digito  = w_mae_dig.digt05 LET tn=tn+1 END IF 
 IF (w_mae_dig.digt06>0) THEN LET v_nivel[6].digito  = w_mae_dig.digt06 LET tn=tn+1 END IF 
 IF (w_mae_dig.digt07>0) THEN LET v_nivel[7].digito  = w_mae_dig.digt07 LET tn=tn+1 END IF 
 IF (w_mae_dig.digt08>0) THEN LET v_nivel[8].digito  = w_mae_dig.digt08 LET tn=tn+1 END IF 
 IF (w_mae_dig.digt09>0) THEN LET v_nivel[9].digito  = w_mae_dig.digt09 LET tn=tn+1 END IF 
 IF (w_mae_dig.digt10>0) THEN LET v_nivel[10].digito = w_mae_dig.digt10 LET tn=tn+1 END IF 
 
 LET ini      = 1
 LET pos      = 0
 LET x_cuenta = NULL
 FOR i = 1 TO tn      
  IF v_nivel[i].digito IS NULL THEN
     CONTINUE FOR
  END IF
  LET pos = (pos+v_nivel[i].digito)

  FOR j = ini TO pos
   LET x_cuenta = x_cuenta CLIPPED,"#"
  END FOR

  IF (i<w_mae_pro.numniv) THEN
     IF (w_mae_pro.nivsep>0) THEN 
      IF (i>=w_mae_pro.nivsep) THEN
         LET x_cuenta = x_cuenta CLIPPED,w_mae_pro.sepdor 
      END IF
     END IF 
  END IF

  LET ini = (pos+1)
  LET pos = (ini-1)
 END FOR

 LET w_mae_pro.format = x_cuenta
 DISPLAY BY NAME w_mae_pro.format
END FUNCTION
