{
ctbqbe001.4gl 
Mynor Ramirez
Mantenimiento de nomenclaturas.
}

{ Definicion de variables globales }

GLOBALS "ctbglo001.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION ctbqbe001_nomenclaturas(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,
        msg                 STRING

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox de paises
  CALL librut003_cbxpaises() 

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL ctbqbe001_EstadoMenu(operacion," ")
   INITIALIZE qrytext,qrypart TO NULL
   CALL ctbmae001_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.tipnom,a.desnom,a.numniv
    ATTRIBUTE(CANCEL=FALSE)
    ON ACTION cancel
     -- Salida
     CALL ctbmae001_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT
   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.tipnom,a.desnom,a.numniv,a.format ",
                 " FROM ctb_tiposnom a ",
                 " WHERE ",qrytext CLIPPED,
                 " ORDER BY 2"

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_nomenclaturas SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_nomenclaturas.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_nomenclaturas INTO v_nomenclaturas[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_nomenclaturas
   FREE  c_nomenclaturas
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL ctbqbe001_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_nomenclaturas TO s_nomenclaturas.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel
      -- Salida
      CALL ctbmae001_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscar 
      CALL ctbmae001_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Verificando integridad 
      LET res = ctbqbe001_integridad()
      IF (res>0) THEN  
       LET msg = "Esta nomenclatura ya tiene registros.\nNomenclatura no puede modificarse."
       CALL fgl_winmessage("Atencion",msg,"stop")
       CONTINUE DISPLAY 
      END IF 

      -- Modificando 
      IF ctbmae001_nomenclaturas(2) THEN
         EXIT DISPLAY 
      ELSE
         -- Desplegando datos
         CALL ctbqbe001_datos(v_nomenclaturas[ARR_CURR()].tipnom)
      END IF 

     ON KEY (CONTROL-M) 
      -- Verificando integridad 
      LET res = ctbqbe001_integridad()
      IF (res>0) THEN  
       LET msg = "Esta nomenclatura ya tiene registros.\nNomenclatura no puede modificarse."
       CALL fgl_winmessage("Atencion",msg,"stop")
       CONTINUE DISPLAY 
      END IF 

      -- Modificando 
      IF (operacion=2) THEN 
       IF ctbmae001_nomenclaturas(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL ctbqbe001_datos(v_nomenclaturas[ARR_CURR()].tipnom)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      LET res = ctbqbe001_integridad()
      IF (res>0) THEN  
         LET msg = "Esta nomenclatura ya tiene registros.\nNomenclatura no puede borrarse."
         CALL fgl_winmessage("Atencion",msg,"stop")
         CONTINUE DISPLAY 
      END IF 

      -- Confirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de borrar esta nomenclatura ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL ctbmae001_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Nomenclatura"
      LET arrcols[2] = "Nombre"
      LET arrcols[3] = "Numero Niveles"
      LET arrcols[4] = "Formato"
      LET arrcols[5] = "Usuario Registro"
      LET arrcols[6] = "Fecha Registro"
      LET arrcols[7] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry = "SELECT a.tipnom,a.desnom,a.numniv,a.format,a.userid,a.fecsis,a.horsis",
                " FROM ctb_tiposnom a ",
                " WHERE ",qrytext CLIPPED,
                " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res = librut002_excelreport("Tipos de Nomenclaturas",qry,13,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE 
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
         CALL ctbqbe001_datos(v_nomenclaturas[ARR_CURR()].tipnom)
      END IF 

    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen nomenclaturas con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Desplegando estado del menu
  CALL ctbqbe001_EstadoMenu(0,"")
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION ctbqbe001_datos(wtipnom)
 DEFINE wtipnom LIKE ctb_tiposnom.tipnom,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  ctb_tiposnom a "||
              "WHERE a.tipnom = "||wtipnom||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_nomenclaturast SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_nomenclaturast INTO w_mae_pro.*
 END FOREACH
 CLOSE c_nomenclaturast
 FREE  c_nomenclaturast

 -- Asignando datos
 LET w_mae_dig.digt01 = w_mae_pro.dig001
 LET w_mae_dig.digt02 = w_mae_pro.dig002
 LET w_mae_dig.digt03 = w_mae_pro.dig003
 LET w_mae_dig.digt04 = w_mae_pro.dig004
 LET w_mae_dig.digt05 = w_mae_pro.dig005
 LET w_mae_dig.digt06 = w_mae_pro.dig006
 LET w_mae_dig.digt07 = w_mae_pro.dig007
 LET w_mae_dig.digt08 = w_mae_pro.dig008
 LET w_mae_dig.digt09 = w_mae_pro.dig009
 LET w_mae_dig.digt10 = w_mae_pro.dig010

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.desnom THRU w_mae_pro.format 
 DISPLAY BY NAME w_mae_dig.digt01 THRU w_mae_dig.digt10 
 DISPLAY BY NAME w_mae_pro.tipnom,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION 

-- Subrutina para verificar si la nomenclatura ya tiene registros 

FUNCTION ctbqbe001_integridad()
 DEFINE conteo INTEGER 

 --Verificando cuentas  
 SELECT COUNT(*)
  INTO  conteo
  FROM  ctb_mcuentas a 
  WHERE (a.tipnom = w_mae_pro.tipnom)
  IF (conteo>0) THEN
      RETURN TRUE
  ELSE
      RETURN FALSE 
  END IF
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION ctbqbe001_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Nomenclaturas - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Nomenclaturas - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Nomenclaturas - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Nomenclaturas - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Nomenclaturas - NUEVO")
 END CASE
END FUNCTION
