{
glbmae005.4gl 
MRS 
Mantenimiento de paises 
}

-- Definicion de variables globales 

GLOBALS "glbglo005.4gl"

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("paises")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo usuario del sistema
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de principal 
 CALL glbmae005_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION glbmae005_mainmenu()
 DEFINE titulo   STRING, 
        wpais    VARCHAR(255),
        existe   SMALLINT,
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "glbmae005a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header(progname,wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Paiss"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,4,username) THEN
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Modificar"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de paises."
    CALL glbqbe005_paises(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo pais."
    LET savedata = glbmae005_paises(1) 
   COMMAND "Modificar"
    " Modificacion de un pais existente."
    CALL glbqbe005_paises(2) 
   COMMAND "Borrar"
    " Eliminacion de un pais existente."
    CALL glbqbe005_paises(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION glbmae005_paises(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL glbqbe005_EstadoMenu(4,"") 
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL glbmae005_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nompai
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   AFTER FIELD nompai  
    --Verificando nombre de la pais
    IF (LENGTH(w_mae_pro.nompai)=0) THEN
       ERROR "Error: nombre del  pais invalida, VERIFICA"
       LET w_mae_pro.nompai = NULL
       NEXT FIELD nompai  
    END IF

    -- Verificando que no exista otro pais con el mismo nombre
    SELECT UNIQUE (a.codpai)
     FROM  glb_mtpaises a
     WHERE (a.codpai != w_mae_pro.codpai) 
       AND (a.nompai  = w_mae_pro.nompai) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otro pais con el mismo nombre, VERIFICA ...",
        "information")
        NEXT FIELD nompai
     END IF 
   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.nompai IS NULL THEN 
       NEXT FIELD nompai
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL glbmae005_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando pais
    CALL glbmae005_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL glbqbe005_EstadoMenu(0,"") 
    CALL glbmae005_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un pais

FUNCTION glbmae005_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando pais ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.codpai),0)
    INTO  w_mae_pro.codpai
    FROM  glb_mtpaises a
    IF (w_mae_pro.codpai IS NULL) THEN
       LET w_mae_pro.codpai = 1
    ELSE 
       LET w_mae_pro.codpai = (w_mae_pro.codpai+1)
    END IF 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO glb_mtpaises   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.codpai 

   --Asignando el mensaje 
   LET msg = "Pais (",w_mae_pro.codpai USING "<<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE glb_mtpaises
   SET    glb_mtpaises.*        = w_mae_pro.*
   WHERE  glb_mtpaises.codpai = w_mae_pro.codpai 

   --Asignando el mensaje 
   LET msg = "Pais (",w_mae_pro.codpai USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando paises
   DELETE FROM glb_mtpaises 
   WHERE (glb_mtpaises.codpai = w_mae_pro.codpai)

   --Asignando el mensaje 
   LET msg = "Pais (",w_mae_pro.codpai USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL glbmae005_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION glbmae005_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.codpai = 0 
   LET w_mae_pro.userid = username 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codpai THRU w_mae_pro.nompai 
 DISPLAY BY NAME w_mae_pro.codpai,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION
