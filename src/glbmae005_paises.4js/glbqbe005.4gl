{ 
glbqbe005.4gl 
MRS 
Mantenimiento de paises 
}

{ Definicion de variables globales }

GLOBALS "glbglo005.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION glbqbe005_paises(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,
        msg                 STRING,
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL glbqbe005_EstadoMenu(operacion," ")
   INITIALIZE qrytext,qrypart TO NULL
   CALL glbmae005_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codpai,a.nompai,a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel       
     -- Salida
     CALL glbmae005_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codpai,a.nompai ",
                 " FROM glb_mtpaises a ",
                 " WHERE ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_paispro SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_paises.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_paispro INTO v_paises[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_paispro
   FREE  c_paispro
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL glbqbe005_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_paises TO s_paispro.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel
      -- Salida
      CALL glbmae005_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscando
      CALL glbmae005_inival(1)
      EXIT DISPLAY

     ON ACTION modificar
      -- Modificando 
      IF glbmae005_paises(2) THEN
         EXIT DISPLAY 
      END IF 

     ON KEY (CONTROL-M)
      -- Modificando 
      IF (operacion=2) THEN 
       IF glbmae005_paises(2) THEN
          EXIT DISPLAY 
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF glbqbe005_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Este pais ya tiene registros. \n Pais no puede borrarse.", 
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de Borrar este pais ?"
      LET opc = librut001_menuopcs("Confirmacion",
                                  msg,
                                  "Si",
                                  "No",
                                  NULL,
                                  NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL glbmae005_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Pais"
      LET arrcols[2] = "Nombre"
      LET arrcols[3] = "Usuario Registro"
      LET arrcols[4] = "Fecha Registro"
      LET arrcols[5] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry        = "SELECT a.* ",
                       " FROM glb_mtpaises a ",
                       " WHERE ",qrytext CLIPPED,
                       " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res        = librut002_excelreport("Paises",qry,5,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
	 CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
	 CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
         CALL glbqbe005_datos(v_paises[ARR_CURR()].tcodpai)
      END IF 

    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen paises con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Desplegando estado del menu
  CALL glbqbe005_EstadoMenu(0,"")
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION glbqbe005_datos(wcodpai)
 DEFINE wcodpai LIKE glb_mtpaises.codpai,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.codpai,a.nompai,a.userid,a.fecsis,a.horsis "||
              "FROM  glb_mtpaises a "||
              "WHERE a.codpai = "||wcodpai||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_paisprot SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_paisprot INTO w_mae_pro.*
 END FOREACH
 CLOSE c_paisprot
 FREE  c_paisprot

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nompai
 DISPLAY BY NAME w_mae_pro.codpai,w_mae_pro.userid THRU w_mae_pro.horsis
END FUNCTION 

-- Subrutina para verificar si el pais ya tiene registros

FUNCTION glbqbe005_integridad()
 DEFINE conteo INTEGER 

 -- Verificando empresas
 SELECT COUNT(*)
  INTO  conteo
  FROM  glb_empresas a 
  WHERE (a.codpai = w_mae_pro.codpai) 
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE 
     -- Verificando proveedores 
     SELECT COUNT(*)
      INTO  conteo
      FROM  glb_sociosng a 
      WHERE (a.codpai = w_mae_pro.codpai) 
      IF (conteo>0) THEN
         RETURN TRUE
      ELSE
         RETURN FALSE
      END IF 
  END IF
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION glbqbe005_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Paises - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Paises - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Paises - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Paises - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Paises - NUEVO")
 END CASE
END FUNCTION
