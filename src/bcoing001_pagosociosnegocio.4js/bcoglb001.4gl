{ 
Programo : Mynor Ramirez  
Objetivo : Programa de variables globales pagos socios de negocios 
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "bcoing001"
DEFINE w_mae_emp   RECORD LIKE glb_empresas.*,
       w_mae_tra   RECORD LIKE bco_mtransac.*,
       w_mae_caj   RECORD LIKE fac_cajchica.*,
       w_mae_pro   RECORD LIKE glb_sociosng.*,
       w_mae_par   RECORD LIKE bco_paramtrs.*, 
       username    LIKE glb_permxusr.userid,
       v_pagos     DYNAMIC ARRAY OF RECORD  
        pfecven    LIKE cmp_mtransac.fecemi,
        pdiaven    SMALLINT,  
        pfecemi    LIKE cmp_mtransac.fecemi,
        pnomcen    CHAR(15), 
        pnomrub    CHAR(15), 
        pnumdoc    CHAR(20),
        ptotdoc    LIKE cmp_mtransac.totdoc,
        ptotsal    LIKE cmp_mtransac.totsal,
        ptotpag    LIKE cmp_mtransac.totdoc, 
        plnkcmp    LIKE cmp_mtransac.lnkcmp, 
        prellen    CHAR(1) 
       END RECORD,
       v_partida   DYNAMIC ARRAY OF RECORD
        cnumcta    LIKE ctb_dtransac.numcta,
        cnomcta    LIKE ctb_mcuentas.nomcta,
        ctotdeb    LIKE ctb_dtransac.totdeb,
        ctothab    LIKE ctb_dtransac.tothab,
        cconcpt    LIKE ctb_dtransac.concep,
        crellen    CHAR(1)
       END RECORD,
       gcodemp     LIKE cmp_mtransac.codemp,
       totaldeb    DEC(14,2),
       totalhab    DEC(14,2),
       totsaldo    DEC(14,2), 
       b           ui.ComboBox,
       cba         ui.ComboBox,
       w           ui.Window,
       f           ui.Form,
       codigoapl   CHAR(3),
       totlin      INTEGER,  
       totpag      INTEGER 
END GLOBALS
