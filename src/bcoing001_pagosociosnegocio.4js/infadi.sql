-- SELECT a.tiptrn,count(*)
update ctb_mtransac
set infadi = (SELECT "CHEQUES DOCUMENTO # ("||TRIM(x.numdoc)||
              ") DE FECHA ("||x.fecemi||
              ") CUENTA BANCO ("||TRIM(x.numcta)||
              ") PROVEEDOR ("||TRIM(x.nomsoc)||")"
              from bco_mtransac x
              where x.lnkbco = ctb_mtransac.lnkapl
                and x.tipmov = 2
                and x.tipsol > 0)
where ctb_mtransac.tiptrn = 102
  and ctb_mtransac.codapl = "BCO"
  and exists (SELECT y.lnkbco from bco_mtransac y
               WHERE y.lnkbco = ctb_mtransac.lnkapl
                 and y.tipmov = 2
                 and y.tipsol > 0)
