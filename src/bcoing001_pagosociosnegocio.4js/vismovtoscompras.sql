drop view vis_movtoscompra;

create view "sistemas".vis_movtoscompra (lnkcmp,codemp,nomemp,tipmov,nommov,numdoc,codsoc,nomsoc,fecemi,fecven,diacre,descrp,codaut,nomaut,codcos,nomcen,codrub,nomrub,totdoc,totsal,totabo,totval,fulabo,tipope,nomope,estado,frmpag,nomest,userid,fecsis,horsis,motanl,usranl,fecanl,horanl,lnkinv) as
  select x0.lnkcmp ,x0.codemp ,x1.nomemp ,x0.tipmov ,x2.nommov
    ,x0.numdoc ,x0.codsoc ,x3.nomsoc ,x0.fecemi ,x0.fecven ,x0.diacre
    ,x0.descrp ,x0.codaut ,x4.nomaut ,x0.codcos ,x6.nomcen ,x0.codrub
    ,x5.nomrub ,x0.totdoc ,x0.totsal ,x0.totabo ,x0.totval ,x0.fulabo
    ,x0.tipope ,CASE WHEN (x0.tipope = 1 )  THEN 'CARGO'  WHEN
    (x0.tipope = 0 )  THEN 'ABONO'  END ,x0.estado ,x0.frmpag
    ,CASE WHEN (x0.estado = 1 )  THEN 'VIGENTE'  WHEN (x0.estado
    = 0 )  THEN 'ANULADA'  END ,x0.userid ,x0.fecsis ,x0.horsis
    ,x0.motanl ,x0.usranl ,x0.fecanl ,x0.horanl ,x0.lnkinv from
    "sistemas".cmp_mtransac x0 ,"sistemas".glb_empresas x1 ,"sistemas"
    .cmp_tipomovs x2 ,"sistemas".glb_sociosng x3 ,"sistemas".glb_userauth
    x4 ,"sistemas".fac_rubgasto x5 ,"sistemas".glb_cencosto x6
    where x1.codemp = x0.codemp AND 
          x2.tipmov = x0.tipmov AND 
          x2.afesal = 1 AND 
          x3.codsoc = x0.codsoc AND 
          x4.codaut = x0.codaut AND 
          x5.codrub = x0.codrub AND 
          x6.codcos = x0.codcos ;

grant select on vis_movtoscompra to public;
