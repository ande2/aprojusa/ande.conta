{
Programo : Mynor Ramirez
Objetivo : Mantenimiento de pago a socios de negocios 
}

-- Definicion de variables globales

GLOBALS "bcoglb001.4gl"
CONSTANT TIPOTRNPOLIZAEGRESO  = 102
DEFINE w_mae_cbo   RECORD LIKE bco_mcuentas.*,
       v_rubros    DYNAMIC ARRAY OF RECORD
        codrub     LIKE fac_dcachica.codrub,
        nomrub     CHAR(40),
        tipdoc     LIKE fac_dcachica.tipdoc, 
        totgra     LIKE fac_dcachica.totgra, 
        totexe     LIKE fac_dcachica.totgra, 
        totisv     LIKE fac_dcachica.totgra, 
        totedp     LIKE fac_dcachica.totedp, 
        totigt     LIKE fac_dcachica.totigt,
        tosiva     LIKE fac_dcachica.valgto,
        tociva     LIKE fac_dcachica.valgto,
        rlleno     CHAR(1) 
       END RECORD,
       gtipmov     LIKE bco_mtransac.tipmov, 
       wpais       VARCHAR(255), 
       regreso     SMALLINT,
       existe      SMALLINT,
       totrub      SMALLINT, 
       msg         STRING 

-- Subrutina principal

MAIN
 -- Atrapando interrupts 
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarPagoSocios")
 

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("pagosprov")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo usuario del sistema
 LET username = FGL_GETENV("LOGNAME")
 LET codigoapl= "BCO" 

 -- Menu de opciones
 CALL bcoing001_menu()
END MAIN

-- Subutina para el menu de pago de socios de negocios 

FUNCTION bcoing001_menu()
 -- Abriendo la ventana del mantenimiento
 OPEN WINDOW wing001a AT 5,2  
  WITH FORM "bcoing001a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ 

  -- Desplegando datos del encabezado
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("bcoing001",wpais,1)

  -- Escondiendo tabla de consultas 
  CALL f.SetElementHidden("titulolista",1) 
  CALL f.SetElementHidden("tablaconsulta",1) 

  -- Cargando combobox de empresas
  LET gcodemp = librut003_DCbxEmpresaEnUsoXUsuario()
  IF gcodemp IS NULL THEN
     CALL fgl_winmessage(
     "Atencion","Usuario sin empresa asignada, VERIFICA.","stop")
     CLOSE WINDOW wing001a
     RETURN
  END IF

  -- Inicializando datos 
  CALL bcoing001_inival(1)

  -- Cargando tipos de movimiento de pago
  LET gtipmov = librut003_DCbxTiposMovimientoPagoBco()
  -- Cargando combobox de centros de costo 
  CALL librut003_CbxCentrosCosto("codcos") 
  -- Cargando combobox de tipos de concepto
  CALL librut003_CbxRubrosGasto() 

  -- Menu de opciones
  MENU "Opciones" 
   BEFORE MENU
    -- Verificando accesos a opciones
    -- Consultar
    IF NOT seclib001_accesos(progname,4,username) THEN
       HIDE OPTION "Consultar"
    END IF
    -- Emitir  
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Emitir"
    END IF
    -- Anular
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Anular"
    END IF
    -- Eliminar  
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "delete"
    END IF
    -- Parametros 
    IF NOT seclib001_accesos(progname,5,username) THEN
       HIDE OPTION "parametros"
    END IF

    -- Obteniendo parametros de pagos de socios de negocios 
    INITIALIZE w_mae_par.* TO NULL
    SELECT a.* INTO w_mae_par.* FROM bco_paramtrs a

   ON ACTION consultar  
    CALL bcoqbx001_PagosSocios(1)
   ON ACTION emitir   
    CALL bcoing001_PagosSocios(1) 
   ON ACTION anular  
    CALL bcoqbx001_PagosSocios(2) 
   ON ACTION delete
    CALL bcoqbx001_PagosSocios(3) 
   ON ACTION parametros
    CALL bcoing001_ParametrosPagosSocios() 
   ON ACTION salir  
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU  
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso de los datos del pago 

FUNCTION bcoing001_PagosSocios(operacion)
 DEFINE gcodaut           LIKE bco_mtransac.codaut, 
        retroceso         SMALLINT,
        operacion,opc     SMALLINT,
        loop,existe,i     SMALLINT,
        anular            SMALLINT,
        conteo            INTEGER 

 -- Desplegando estado del menu
 CALL bcoing001_EstadoMenu(4,"")

 -- Desactivando salida del input
 OPTIONS INPUT WRAP 

 -- Inicio del loop
 LET loop      = TRUE
 LET retroceso = FALSE
 WHILE loop   
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Verificando si operacion es ingreso
     IF (operacion=1) THEN 
        -- Inicializando datos 
        CALL bcoing001_inival(1) 
     END IF 
  END IF
  LET anular = FALSE 

  -- Ingresando datos
  INPUT BY NAME w_mae_tra.tipmov, 
                w_mae_tra.codsoc, 
                w_mae_tra.tipsol,
                w_mae_tra.lnkcaj, 
                w_mae_tra.codcos,
                w_mae_tra.codaut,
                w_mae_tra.codrub,
                w_mae_tra.numcta, 
                w_mae_tra.numdoc,
                w_mae_tra.totpag,
                w_mae_tra.reisra,
                w_mae_tra.reishs, 
                w_mae_tra.otrdes, 
                w_mae_tra.totdoc,
                w_mae_tra.nomchq, 
                w_mae_tra.descrp 
                WITHOUT DEFAULTS
    ATTRIBUTE(UNBUFFERED,CANCEL=FALSE)

   ON ACTION cancel 
    -- Salida 
    IF (operacion=1) THEN  -- Ingreso 
     IF INFIELD(tipmov) THEN
        LET loop = FALSE
        EXIT INPUT
     ELSE
        -- Inicializando 
        CALL bcoing001_inival(1)
        LET retroceso = FALSE 
        NEXT FIELD tipmov 
     END IF 
    ELSE                   -- Modifidacion 
     LET loop = FALSE 
     EXIT INPUT 
    END IF 

   ON KEY (CONTROL-F,F3)
    -- Cambiando fecha de emision
    IF INFIELD(tipmov) THEN
       CALL bcoing001_FechaEmision()
    ELSE
       CALL fgl_winmessage(
       "Atencion",
       "Para cambiar fecha posicionarse en dato Tipo de Movimiento.",
       "information")  
    END IF 

   ON ACTION anular
    -- Registrando documento pago como anulado
    IF NOT INFIELD(tipmov) THEN
     CALL fgl_winmessage(
     "Atencion",
     "Para registrar documento como anulado posicionarse en dato Tipo Movimiento.",
     "information")  
     NEXT FIELD tipmov 
    END IF 

    -- Asignando datos del documento de pago anulado 
    LET anular           = TRUE 
    LET w_mae_tra.totdoc = 0
    LET w_mae_tra.totpag = 0 
    LET w_mae_tra.descrp = "DOCUMENTO GRABADO COMO ANULADO" 
    LET w_mae_tra.nomchq = "" 
    LET w_mae_tra.codsoc = 0 
    LET w_mae_tra.tipmov = gtipmov 
    LET w_mae_tra.tipsol = 2
    LET w_mae_tra.estado = 0 
    LET w_mae_tra.motanl = "DOCUMENTO GRABADO COMO ANULADO"
    LET w_mae_tra.nomsoc = "DOCUMENTO GRABADO COMO ANULADO"
    LET w_mae_tra.usranl = username
    LET w_mae_tra.horanl = CURRENT HOUR TO SECOND
    LET w_mae_tra.fecanl = w_mae_tra.fecemi  
    LET w_mae_tra.feccob = w_mae_tra.fecemi 

    DISPLAY BY NAME w_mae_tra.codcos,w_mae_tra.codsoc,
                    w_mae_tra.codaut,w_mae_tra.codrub,
                    w_mae_tra.totdoc,w_mae_tra.nomchq,
                    w_mae_tra.descrp,w_mae_tra.estado  

    -- Desactivando campos
    CALL Dialog.SetFieldActive("lnkcaj",0) 
    CALL Dialog.SetFieldActive("codcos",0) 
    CALL Dialog.SetFieldActive("codsoc",0) 
    CALL Dialog.SetFieldActive("codaut",0) 
    CALL Dialog.SetFieldActive("codrub",0) 
    CALL Dialog.SetFieldActive("reisra",0) 
    CALL Dialog.SetFieldActive("reishs",0) 
    CALL Dialog.SetFieldActive("otrdes",0) 
    CALL Dialog.SetFieldActive("totdoc",0) 
    CALL Dialog.SetFieldActive("nomchq",0) 
    CALL Dialog.SetFieldActive("descrp",0) 
    NEXT FIELD numcta  

   ON ACTION calculator 
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   BEFORE FIELD tipmov 
    -- Verificando operacion
    IF retroceso THEN
       CALL Dialog.SetFieldActive("tipmov",0) 
       CALL Dialog.SetFieldActive("codsoc",0) 
       CALL Dialog.SetFieldActive("tipsol",0) 
       CALL Dialog.SetFieldActive("lnkcaj",0) 

       -- Verificando tipo de solicitud
       IF (w_mae_tra.tipsol=1) THEN  -- Con facturas 
          CALL Dialog.SetFieldActive("codcos",0) 
          CALL Dialog.SetFieldActive("codaut",0) 
          CALL Dialog.SetFieldActive("codrub",0) 
          CALL Dialog.SetFieldActive("totpag",0) 
          CALL Dialog.SetFieldActive("totdoc",0) 
          NEXT FIELD numcta 
       END IF
       IF (w_mae_tra.tipsol=2) THEN -- Sin facturas
          CALL Dialog.SetFieldActive("codcos",1) 
          CALL Dialog.SetFieldActive("codaut",1) 
          CALL Dialog.SetFieldActive("codrub",1) 
          CALL Dialog.SetFieldActive("totpag",1) 
          CALL Dialog.SetFieldActive("totdoc",0)
          NEXT FIELD codcos 
       END IF 
       IF (w_mae_tra.tipsol=3) THEN -- Cajas/Liquidaciones 
          CALL Dialog.SetFieldActive("lnkcaj",0) 
          CALL Dialog.SetFieldActive("codcos",0) 
          CALL Dialog.SetFieldActive("codaut",1) 
          CALL Dialog.SetFieldActive("codrub",1) 
          CALL Dialog.SetFieldActive("totpag",1) 
          CALL Dialog.SetFieldActive("totdoc",0)
          NEXT FIELD codaut 
       END IF 
    ELSE 
       CALL Dialog.SetFieldActive("tipmov",1) 
       CALL Dialog.SetFieldActive("codsoc",1) 
       CALL Dialog.SetFieldActive("tipsol",1) 
       CALL Dialog.SetFieldActive("lnkcaj",0) 
       CALL Dialog.SetFieldActive("codcos",0) 
       CALL Dialog.SetFieldActive("codaut",0) 
       CALL Dialog.SetFieldActive("codrub",0) 
       CALL Dialog.SetFieldActive("totpag",0) 
       CALL Dialog.SetFieldActive("totdoc",0) 
    END IF 

    -- Desabilitando accion de accept
    CALL Dialog.SetActionActive("accept",0) 
    CALL Dialog.SetActionActive("anular",1) 

   AFTER FIELD tipmov 
    -- Verificando tipmov 
    IF w_mae_tra.tipmov IS NULL THEN
       ERROR "Error: tipo de movimiento invalido, VERIFICA."
       NEXT FIELD tipmov 
    END IF 

   BEFORE FIELD codsoc 
    -- Cargando combobox de socios de negocios 
    CALL librut003_CbxProveedores()   

   ON CHANGE codsoc
    -- Obteniendo datos del socio 
    INITIALIZE w_mae_pro.* TO NULL 
    CALL librut003_BSociosNeg(w_mae_tra.codsoc)
    RETURNING w_mae_pro.*,existe 

    -- Asignando nombre default del documento 
    LET w_mae_tra.nomchq = w_mae_pro.nomsoc 
    DISPLAY BY NAME w_mae_tra.nomchq 

   AFTER FIELD codsoc 
    -- Verificando codsoc
    IF w_mae_tra.codsoc IS NULL THEN 
       ERROR "Error: socio invalido, VERIFICA."
       NEXT FIELD codsoc 
    END IF  

    -- Obteniendo datos del socio 
    INITIALIZE w_mae_pro.* TO NULL 
    CALL librut003_BSociosNeg(w_mae_tra.codsoc)
    RETURNING w_mae_pro.*,existe 

   AFTER FIELD tipsol
    -- Verificando tipsol
    IF w_mae_tra.tipsol IS NULL THEN
       ERROR "Error: tipo de solicitud invalida, VERIFICA."
       NEXT FIELD tipsol
    END IF 

    -- Verificando si tipo de solicitud es con facturas 
    IF (w_mae_tra.tipsol=1) THEN
       -- Cargando pagos pendientes del socio 
       IF bcoing001_PagosPendientesSocios() THEN
          NEXT FIELD tipsol 
       END IF 

       -- Verificando pagos 
       IF (totpag=0) THEN 
        CALL fgl_winmessage(
        "Atencion","Socio sin pagos pendientes.","stop")
        NEXT FIELD codsoc 
       END IF 

       -- Desactivando campos 
       CALL Dialog.SetFieldActive("lnkcaj",0) 
       CALL Dialog.SetFieldActive("codcos",0) 
       CALL Dialog.SetFieldActive("codaut",0) 
       CALL Dialog.SetFieldActive("codrub",0) 
       CALL Dialog.SetFieldActive("totdoc",0) 
       CALL Dialog.SetFieldActive("totpag",0) 
       LET retroceso = TRUE
    END IF 
    -- Verificando si tipo de solicitud es sin facturas 
    IF (w_mae_tra.tipsol=2) THEN
       -- Inicializando datos
       CALL bcoing001_inivec()

       -- Activando campos     
       CALL Dialog.SetFieldActive("lnkcaj",0) 
       CALL Dialog.SetFieldActive("codcos",1) 
       CALL Dialog.SetFieldActive("codaut",1) 
       CALL Dialog.SetFieldActive("codrub",1) 
       CALL Dialog.SetFieldActive("totdoc",0) 
       CALL Dialog.SetFieldActive("totpag",1) 
       LET retroceso = TRUE
    END IF 
    -- Verificando si tipo de solicitud es caja/liquidaciones
    IF (w_mae_tra.tipsol=3) THEN
       -- Inicializando datos
       CALL bcoing001_inivec()

       -- Activando campos     
       CALL Dialog.SetFieldActive("lnkcaj",1) 
       CALL Dialog.SetFieldActive("codcos",0) 
       CALL Dialog.SetFieldActive("codaut",1) 
       CALL Dialog.SetFieldActive("codrub",1) 
       CALL Dialog.SetFieldActive("totdoc",0) 
       CALL Dialog.SetFieldActive("totpag",1) 
       LET retroceso = TRUE
    END IF 

   BEFORE FIELD lnkcaj 
    -- Desactivando campos 
    CALL Dialog.SetFieldActive("tipmov",0) 
    CALL Dialog.SetFieldActive("codsoc",0) 
    CALL Dialog.SetFieldActive("tipsol",0) 

    -- Cargando ids de las cajas/liquidaciones 
    CALL librut003_CbxIdsCajas("AND a.lnkbco=0") 

   ON CHANGE lnkcaj 
    -- Obteniendo datos de la caja o liquidacion 
    INITIALIZE w_mae_caj.* TO NULL
    CALL librut003_BCajas(w_mae_tra.lnkcaj)
    RETURNING w_mae_caj.*,existe

   AFTER FIELD lnkcaj 
    -- Verificando lnkcaj 
    IF w_mae_tra.lnkcaj IS NULL THEN
       ERROR "Error: id de la caja o liquidacion invalido, VERIFICA."
       NEXT FIELD lnkcaj 
    END IF 

    -- Obteniendo datos de la caja o liquidacion 
    INITIALIZE w_mae_caj.* TO NULL
    CALL librut003_BCajas(w_mae_tra.lnkcaj)
    RETURNING w_mae_caj.*,existe
    IF NOT existe THEN
       CALL fgl_winmessage(
       "Atencion","ID de la caja o liquidacion no registrado, VERIFICA.","stop") 
       NEXT FIELD lnkcaj 
    END IF

    -- Desplegando datos
    LET w_mae_tra.codcos = w_mae_caj.cencos 
    LET w_mae_tra.totpag = w_mae_caj.totgto 
    LET w_mae_tra.totdoc = w_mae_tra.totpag 

    IF (w_mae_caj.tipcaj=3) THEN 
     LET w_mae_tra.descrp = "LIQUIDACION # ",w_mae_caj.numdoc USING "<<<<<<<<<<",
                            " DE FECHA ",w_mae_caj.fecemi,"." 
    ELSE 
     LET w_mae_tra.descrp = "CAJA # ",w_mae_caj.numdoc USING "<<<<<<<<<<",
                            " DE FECHA ",w_mae_caj.fecemi,"." 
    END IF 

    DISPLAY BY NAME w_mae_tra.descrp,w_mae_tra.totpag,
                    w_mae_tra.totdoc,w_mae_tra.codcos 
    CALL Dialog.SetFieldActive("lnkcaj",0) 

   BEFORE FIELD codcos 
    -- Desactivando campos 
    CALL Dialog.SetFieldActive("tipmov",0) 
    CALL Dialog.SetFieldActive("codsoc",0) 
    CALL Dialog.SetFieldActive("tipsol",0) 
    CALL Dialog.SetFieldActive("lnkcaj",0) 

   AFTER FIELD codcos 
    -- Verificando codcos 
    IF w_mae_tra.codcos IS NULL THEN
       ERROR "Error: centro de costo invalido, VERIFICA."
       NEXT FIELD codcos 
    END IF 

   BEFORE FIELD codaut
    -- Cargando combobox de autorizantes
    LET gcodaut = librut003_DCbxAutorizantes()
    IF w_mae_tra.codaut IS NULL THEN
       LET w_mae_tra.codaut = gcodaut 
    END IF 

   AFTER FIELD codaut 
    -- Verificando codaut
    IF w_mae_tra.codaut IS NULL THEN 
       ERROR "Error: autorizante invalido, VERIFICA."
       NEXT FIELD codaut 
    END IF 

   AFTER FIELD codrub 
    -- Verificando codrub 
    IF w_mae_tra.codrub IS NULL THEN 
       ERROR "Error: tipo de concepto invalido, VERIFICA."
       NEXT FIELD codrub
    END IF 

   BEFORE FIELD numcta 
    -- Subrutina que carga el combobox de las cuentas bancarias
    CALL librut003_CbxCuentasBancoVouchers(w_mae_tra.codemp)

    -- Obteniendo datos de la cuenta contable 
    INITIALIZE w_mae_cbo.* TO NULL 
    CALL librut003_BCuentaBanco(w_mae_tra.numcta) 
    RETURNING w_mae_cbo.*,existe 

   AFTER FIELD numcta 
    -- Verificando numcta
    IF w_mae_tra.numcta IS NULL THEN 
       ERROR "Error: cuenta bancaria invalida, VERIFICA."
       NEXT FIELD numcta 
    END IF 

    -- Verificando si el pago existe
    SELECT COUNT(*)
     INTO  conteo 
     FROM  bco_mtransac a
     WHERE a.numcta = w_mae_tra.numcta
       AND a.tipmov = w_mae_tra.tipmov 
       AND a.numdoc = w_mae_tra.numdoc
     IF (conteo>0) THEN
        CALL fgl_winmessage(
        "Atencion",
        "Numero de documento "||w_mae_tra.numdoc CLIPPED||" ya existe registrado.",
        "stop")
        NEXT FIELD numdoc
     END IF 

    -- Obteniendo datos de la cuenta contable 
    INITIALIZE w_mae_cbo.* TO NULL 
    CALL librut003_BCuentaBanco(w_mae_tra.numcta) 
    RETURNING w_mae_cbo.*,existe 

    -- Verificando existencia de cuenta contable del banco
    IF LENGTH(w_mae_cbo.numctb)=0 THEN
       CALL fgl_winmessage(
       "Atencion",
       "Numero de documento "||w_mae_tra.numdoc CLIPPED||" ya existe registrado."||
       "\nVerifica cuenta bancaria y tipo de movimiento.",
       "information")
       NEXT FIELD numcta 
    END IF 

   AFTER FIELD numdoc
    -- Verificando numdoc
    IF w_mae_tra.numdoc IS NULL OR 
       w_mae_tra.numdoc <=0 THEN 
       ERROR "Error: numero de documento invalido, VERIFICA."
       NEXT FIELD numdoc 
    END IF 

    -- Verificando si el documento existe
    SELECT COUNT(*)
     INTO  conteo 
     FROM  bco_mtransac a
     WHERE a.numcta = w_mae_tra.numcta
       AND a.tipmov = w_mae_tra.tipmov 
       AND a.numdoc = w_mae_tra.numdoc
     IF (conteo>0) THEN
      CALL fgl_winmessage(
      "Atencion",
      "Numero de documento "||w_mae_tra.numdoc CLIPPED||" ya existe registrado."||
      "\nVerifica cuenta bancaria y tipo de movimiento.",
      "information")
      NEXT FIELD numdoc
     END IF

    -- Si es documento de pago anulado se sale del input y graba  
    IF anular THEN
       EXIT INPUT
    END IF 

   ON CHANGE totpag 
    -- Calculando total a pagar
    CALL bcoing001_TotalApagar()

   AFTER FIELD totpag 
    -- Verificando totpag
    IF w_mae_tra.totpag IS NULL OR 
       w_mae_tra.totpag <=0 OR 
       w_mae_tra.totpag >w_mae_par.totval THEN 
       CALL fgl_winmessage(
       "Atencion", 
       "Valor total del pago mayor a total permitido, VERIFICA."||
       "\nTotal permitido ("||w_mae_par.totval||") ", 
       "stop") 
       NEXT FIELD totpag 
    END IF 
  
    -- Calculando total a pagar
    CALL bcoing001_TotalApagar()

   ON CHANGE reisra 
    -- Calculando total a pagar
    CALL bcoing001_TotalApagar()

   AFTER FIELD reisra 
    -- Verificando reisra
    IF w_mae_tra.reisra IS NULL OR 
       w_mae_tra.reisra <0 OR 
       w_mae_tra.reisra >=w_mae_tra.totpag THEN 
       ERROR "Error: valor retencion invalido, VERIFICA."
       NEXT FIELD reisra 
    END IF 
  
    -- Calculando total a pagar
    CALL bcoing001_TotalApagar()

   ON CHANGE reishs
    -- Calculando total a pagar
    CALL bcoing001_TotalApagar()

   AFTER FIELD reishs 
    -- Verificando reisra
    IF w_mae_tra.reishs IS NULL OR 
       w_mae_tra.reishs <0 OR 
       w_mae_tra.reishs >=w_mae_tra.totpag THEN 
       ERROR "Error: valor retencion invalido, VERIFICA."
       NEXT FIELD reishs 
    END IF 

    -- Calculando total a pagar
    CALL bcoing001_TotalApagar()

   ON CHANGE otrdes 
    -- Calculando total a pagar
    CALL bcoing001_TotalApagar()

   AFTER FIELD otrdes 
    -- Verificando otrdes 
    IF w_mae_tra.otrdes IS NULL OR 
       w_mae_tra.otrdes <0 OR 
       w_mae_tra.otrdes >=w_mae_tra.totpag THEN 
       ERROR "Error: otros descuentos invalido, VERIFICA."
       NEXT FIELD otrdes 
    END IF 

    -- Calculando total a pagar
    CALL bcoing001_TotalApagar()

   AFTER FIELD totdoc
    -- Verificando totdoc
    IF w_mae_tra.totdoc IS NULL OR 
       w_mae_tra.totdoc <=0 OR 
       w_mae_tra.totdoc >w_mae_par.totval THEN 
       CALL fgl_winmessage(
       "Atencion", 
       "Total a pagar mayor a total permitido, VERIFICA."||
       "\nTotal permitido ("||w_mae_par.totval||") ", 
       "stop") 
       NEXT FIELD totdoc 
    END IF 

   AFTER FIELD nomchq 
    -- Verificando nomchq
    IF LENGTH(w_mae_tra.nomchq)=0 THEN
       LET w_mae_tra.nomchq = w_mae_pro.nomsoc 
       DISPLAY BY NAME w_mae_tra.nomchq 
    END IF 

   BEFORE FIELD descrp 
    -- Habilitando accion de accept
    CALL Dialog.SetActionActive("accept",1)

   AFTER FIELD descrp
    -- Verificando descrp 
    IF LENGTH(w_mae_tra.descrp)=0 THEN
       ERROR "Error: descripcion del pago invalida, VERIFICA."
       NEXT FIELD descrp 
    END IF

  END INPUT 
  IF NOT loop THEN
     EXIT WHILE 
  END IF 

  -- Verificando que no sea documento de pago anulado 
  IF NOT anular THEN 

   -- Verificando que total a pagar no se negativo 
   IF (w_mae_tra.totdoc<=0) THEN 
      CALL fgl_winmessage(
      "Atencion",
      "Valor total a pagar debe ser mayor a cero."||
      "\nVerifica retenciones y otros descuentos.", 
      "stop")
      LET retroceso = TRUE 
      CONTINUE WHILE 
   END IF 

   -- Ingresando detalle de la poliza
   IF bcoing001_DetallePoliza() THEN
      LET retroceso = TRUE 
      CONTINUE WHILE 
   END IF 

   -- Verificando total del pago y total de la poliza contable 
   IF (w_mae_tra.totpag!=totaldeb) THEN
      CALL fgl_winmessage(
      "Atencion",
      "Valor total del pago diferente al total de la poliza contable."||
      "\nVerificar totales de la poliza contable.", 
      "stop")
      LET retroceso = TRUE 
      CONTINUE WHILE 
   END IF 
  
   -- Menu de opciones 
   LET opc = librut001_menugraba("Confirmacion",
                                 "Que desea hacer?",
                                 "Guardar",
                                 "Modificar",
                                 "Cancelar",
                                 "")
   -- Verificando opcion 
   CASE (opc)
    WHEN 0 -- Cancelando
     LET retroceso = FALSE
    WHEN 1 -- Grabando
     LET retroceso = FALSE 

     -- Grabando 
     CALL bcoing001_grabar(operacion,FALSE)
    WHEN 2 -- Modificando
     LET retroceso = TRUE 
   END CASE
  ELSE
   -- Grabando documento de anulado 
   -- Confirmacion de la accion a ejecutar
   LET msg = "Esta SEGURO de grabar documento como anulado."   
   LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

   -- Verificando opcion 
   CASE (opc)
    WHEN 0 -- Cancelando
     LET retroceso = FALSE
    WHEN 1 -- Grabando
     LET retroceso = FALSE 

     -- Grabando 
     CALL bcoing001_grabar(operacion,TRUE)
   END CASE
  END IF 
 END WHILE

 -- Activando salida del input
 OPTIONS INPUT NO WRAP 

 -- Inicializando datos 
 IF (operacion=1) THEN 
    CALL bcoing001_inival(1) 
 END IF 

 -- Desplegando estado del menu
 CALL bcoing001_EstadoMenu(0,"")
END FUNCTION

-- Subrutina para grabar el pago 

FUNCTION bcoing001_grabar(operacion,anular)
 DEFINE w_mae_ctb                 RECORD LIKE ctb_mtransac.*,
        xtotval                   LIKE ctb_mtransac.totdoc,
        xtipnom                   LIKE glb_empresas.tipnom,
        xnommov                   LIKE bco_tipomovs.nommov, 
        i,correl,operacion,anular SMALLINT,
        xtipope                   CHAR(1),
        msg                       STRING

 -- Grabando transaccion
 CASE (operacion)
  WHEN 1 ERROR " Registrando Pago ... " ATTRIBUTE(CYAN)
  WHEN 3 ERROR " Eliminando Pago ... " ATTRIBUTE(CYAN)
 END CASE 

 -- Iniciando la transaccion
 BEGIN WORK
   CASE (operacion)
    WHEN 1 -- Grabando 

     -- Asignando datos
     IF NOT anular THEN 
        LET w_mae_tra.nomsoc = w_mae_pro.nomsoc
     END IF 

     LET w_mae_tra.tascam = 0 
     LET w_mae_tra.monext = (w_mae_tra.totdoc*w_mae_tra.tascam) 
     LET w_mae_tra.feccob = w_mae_tra.fecemi 

     -- Verificando tipo de saldo 
     LET w_mae_tra.tipope = 0  
     LET w_mae_tra.totsal = w_mae_tra.totdoc
     IF w_mae_tra.tipope=0 THEN
        LET w_mae_tra.totsal = (w_mae_tra.totsal*(-1)) 
     END IF 

     -- Grabando maestro de bancos
     SET LOCK MODE TO WAIT 
     INSERT INTO bco_mtransac  
     VALUES (w_mae_tra.*)
     LET w_mae_tra.lnkbco = SQLCA.SQLERRD[2]  

     -- Grabando detalle de facturas pagadas si tipo de solicitud 
     -- de pago es con facturas
     IF (w_mae_tra.tipsol=1) THEN 
        FOR i = 1 TO totpag 
         IF v_pagos[i].pfecven IS NULL OR
            v_pagos[i].ptotpag IS NULL OR 
            v_pagos[i].ptotpag = 0 THEN      
            CONTINUE FOR
         END IF 

         SET LOCK MODE TO WAIT
         INSERT INTO bco_dtransac
         VALUES (w_mae_tra.lnkbco,
                 0, 
                 v_pagos[i].plnkcmp,
                 w_mae_tra.fecemi, 
                 v_pagos[i].pdiaven,
                 v_pagos[i].ptotsal,
                 v_pagos[i].ptotpag,
                 w_mae_tra.estado)
        END FOR 
     END IF 

     -- Actualizando caja o liquidacion de pagada si tipo de solicitud 
     -- es caja o liquidacion
     IF (w_mae_tra.tipsol=3) THEN 
        IF w_mae_tra.lnkcaj>0 THEN
           -- Actualizando caja o liquidacion 
           SET LOCK MODE TO WAIT 
           UPDATE fac_cajchica 
           SET    fac_cajchica.lnkbco = w_mae_tra.lnkbco 
           WHERE  fac_cajchica.lnkcaj = w_mae_tra.lnkcaj 
        END IF 
     END IF 

     -- Grabando maestro de poliza contable
     IF NOT anular THEN -- Si documento es grabado como normal 
      -- Asignando datos
      LET w_mae_ctb.lnktra = 0
      LET w_mae_ctb.codemp = w_mae_tra.codemp
      LET w_mae_ctb.tiptrn = TIPOTRNPOLIZAEGRESO  

      -- Obteniendo tipo de nomenclatura de la empresa 
      INITIALIZE xtipnom TO NULL
      SELECT a.tipnom INTO xtipnom FROM glb_empresas a
       WHERE a.codemp = w_mae_ctb.codemp 

      -- Obteniendo nombre del tipo de movimiento            
      INITIALIZE xnommov TO NULL
      SELECT a.nommov INTO xnommov FROM bco_tipomovs a
       WHERE a.tipmov = w_mae_tra.tipmov 

      LET w_mae_ctb.fecemi = w_mae_tra.feccob 
      LET w_mae_ctb.numdoc = librut003_NumeroPolizaAutomatica(w_mae_ctb.codemp,
                                                              w_mae_ctb.tiptrn,
                                                              w_mae_ctb.fecemi)
      LET w_mae_ctb.totdoc = w_mae_tra.totpag 
      LET w_mae_ctb.concep = w_mae_tra.descrp 
      LET w_mae_ctb.codapl = codigoapl 
      LET w_mae_ctb.lnkapl = w_mae_tra.lnkbco 
      LET w_mae_ctb.coddiv = 1 
      LET w_mae_ctb.estado = 1 
      LET w_mae_ctb.infadi = xnommov CLIPPED," DOCUMENTO # ("||
                             w_mae_tra.numdoc CLIPPED||
                             ") DE FECHA ("||w_mae_tra.fecemi||
                             ") CUENTA BANCO ("||w_mae_tra.numcta CLIPPED||
                             ") PROVEEDOR ("||w_mae_tra.nomsoc CLIPPED||")"
      LET w_mae_ctb.userid = username
      LET w_mae_ctb.fecsis = CURRENT 
      LET w_mae_ctb.horsis = CURRENT HOUR TO SECOND 

      SET LOCK MODE TO WAIT
      INSERT INTO ctb_mtransac
      VALUES (w_mae_ctb.*) 
      LET w_mae_ctb.lnktra = SQLCA.SQLERRD[2] 

      -- Grabando detalle de cuentas de la poliza contable
      LET correl = 0
      FOR i = 1 TO totlin
       IF v_partida[i].cnumcta IS NULL THEN
          CONTINUE FOR
       END IF 
       LET correl = correl+1

       -- Verificando debe y haber
       IF (v_partida[i].ctotdeb>0) THEN
          LET xtipope = "D"
          LET xtotval = v_partida[i].ctotdeb
       ELSE
          LET xtipope = "H"
          LET xtotval = v_partida[i].ctothab
       END IF

       -- Grabando detalle de cuentas
       SET LOCK MODE TO WAIT
       INSERT INTO bco_dtpoliza 
       VALUES (w_mae_tra.lnkbco    , -- link del encabezado
               correl              , -- correlativo
               v_partida[i].cnumcta, -- numero de cuenta
               v_partida[i].ctotdeb, -- total debe
               v_partida[i].ctothab, -- total haber
               v_partida[i].cconcpt, -- concepto de la linea
               xtipope)              -- tipo de operacion 

       -- Grabando detalle de cuentas de poliza contable
       SET LOCK MODE TO WAIT
       INSERT INTO ctb_dtransac
       VALUES (w_mae_ctb.lnktra    , -- link del encabezado
               correl              , -- correlativo
               v_partida[i].cnumcta, -- numero de cuenta
               v_partida[i].cconcpt, -- concepto de la linea
               v_partida[i].ctotdeb, -- total debe
               v_partida[i].ctothab, -- total haber
               xtipope)

       -- Creando cuentas padre
       CALL librut003_CuentasPadre(
              w_mae_ctb.lnktra,
              correl,
              w_mae_ctb.codemp,
              w_mae_ctb.fecemi,
              v_partida[i].cnumcta,
              xtipnom,
              xtipope,
              xtotval,
              v_partida[i].ctotdeb,
              v_partida[i].ctothab,
              "N",
              "N")
      END FOR 
     END IF 
  
     WHEN 3 -- Eliminando
      -- Eliminando pago
      SET LOCK MODE TO WAIT
      DELETE FROM bco_mtransac
      WHERE (bco_mtransac.lnkbco = w_mae_tra.lnkbco)

      -- Eliminando poliza de la contabilidad
      SET LOCK MODE TO WAIT
      DELETE FROM ctb_mtransac
      WHERE ctb_mtransac.codapl = codigoapl
        AND ctb_mtransac.lnkapl = w_mae_tra.lnkbco

      -- Anulando marca si pago es de caja o liquidacion
      IF w_mae_tra.tipsol=3 THEN
         IF w_mae_tra.lnkcaj>0 THEN
            -- Actualizando caja o liquidacion
            SET LOCK MODE TO WAIT
            UPDATE fac_cajchica
            SET    fac_cajchica.lnkbco = 0
            WHERE  fac_cajchica.lnkcaj = w_mae_tra.lnkcaj
         END IF
      END IF
    END CASE 

 -- Finalizando la transaccion
 COMMIT WORK

 -- Confirmando grabacion 
 CASE (operacion)
  WHEN 1 -- Grabando 
   IF NOT anular THEN 
    LET msg = "Pago con Numero de Movimiento ("||w_mae_tra.lnkbco||") registrado."
    CALL fgl_winmessage("Atencion",msg,"information")

    -- Imprimiendo voucher  
    ERROR " Generando Voucher ... por favor esperar ..." ATTRIBUTE(CYAN)
    CALL bcorpt001_GenerarVoucher(0)               
   ELSE 
    LET msg = "Pago Numero de Movimiento ("||w_mae_tra.lnkbco||") registrado."||
              "\n No se generara voucher." 
    CALL fgl_winmessage("Atencion",msg,"information")
   END IF 
  WHEN 3 -- Eliminando 
   -- Asignando el mensaje
   LET msg = "Pago Numero de Movimiento ("||w_mae_tra.lnkbco||") eliminado."
   CALL fgl_winmessage("Atencion",msg,"information")
 END CASE 

 ERROR ""
END FUNCTION  

-- Subrutina para ingresar los parametros de los pagos de socios 

FUNCTION bcoing001_ParametrosPagosSocios() 
 DEFINE grabar SMALLINT 

 -- Abriendo ventana
 OPEN WINDOW wing001d AT 5,2
  WITH FORM "bcoing001c" ATTRIBUTE(BORDER)

  -- Obteniendo datos
  INITIALIZE w_mae_par.* TO NULL
  SELECT a.* INTO w_mae_par.* FROM bco_paramtrs a
  DISPLAY BY NAME w_mae_par.* 

  -- Ingresando datos
  OPTIONS INPUT WRAP 
  INPUT BY NAME w_mae_par.totval,
                w_mae_par.diaing,
                w_mae_par.diapos, 
                w_mae_par.diaanu, 
                w_mae_par.diaeli WITHOUT DEFAULTS 
   ATTRIBUTE(UNBUFFERED,FIELD ORDER FORM)

   ON ACTION accept
    -- Aceptar
    LET grabar = TRUE 
    EXIT INPUT

   ON ACTION cancel 
    -- Cancelar
    LET grabar = FALSE 
    EXIT INPUT

   AFTER FIELD totval 
    -- Verificando total
    {IF w_mae_par.totval IS NULL OR 
       w_mae_par.totval<0 OR 
       w_mae_par.totval>9999999 THEN
       LET w_mae_par.totval = 999999.99 
    END IF} 
    DISPLAY BY NAME w_mae_par.totval 

   AFTER FIELD diaing 
    -- Verificando dias
    IF w_mae_par.diaing IS NULL OR 
       w_mae_par.diaing<0 OR 
       w_mae_par.diaing>360 THEN
       LET w_mae_par.diaing = 30 
    END IF 
    DISPLAY BY NAME w_mae_par.diaing 

   AFTER FIELD diapos 
    -- Verificando dias
    IF w_mae_par.diapos IS NULL OR 
       w_mae_par.diapos<0 OR 
       w_mae_par.diapos>360 THEN
       LET w_mae_par.diapos = 30 
    END IF 
    DISPLAY BY NAME w_mae_par.diapos 

   AFTER FIELD diaanu 
    -- Verificando dias
    IF w_mae_par.diaanu IS NULL OR 
       w_mae_par.diaanu<0 OR 
       w_mae_par.diaanu>360 THEN
       LET w_mae_par.diaanu = 30 
    END IF 
    DISPLAY BY NAME w_mae_par.diaanu 

   AFTER FIELD diaeli 
    -- Verificando dias
    IF w_mae_par.diaeli IS NULL OR 
       w_mae_par.diaeli<0 OR 
       w_mae_par.diaeli>360 THEN
       LET w_mae_par.diaeli = 30 
    END IF 
    DISPLAY BY NAME w_mae_par.diaeli 

  END INPUT 

  -- Verificando grabacion 
  IF grabar THEN 
     LET w_mae_par.userid = username 
     LET w_mae_par.fecsis = CURRENT 
     LET w_mae_par.horsis = CURRENT HOUR TO SECOND 

     SELECT a.* FROM bco_paramtrs a 
     IF (status=NOTFOUND) THEN
      -- Grabando 
      INSERT INTO bco_paramtrs 
      VALUES (w_mae_par.*) 
     ELSE
      -- Grabando 
      UPDATE bco_paramtrs 
      SET    bco_paramtrs.* = w_mae_par.* 
     END IF 
  END IF 
  OPTIONS INPUT NO WRAP 
 CLOSE WINDOW wing001d
END FUNCTION 

-- Subrutina para buscar los pagos pendientes del socio 

FUNCTION bcoing001_PagosPendientesSocios() 
 DEFINE arr,scr,regreso,loop SMALLINT,
        qrytxt               STRING 
                                                             
 -- Inicializando datos
 CALL bcoing001_inivec()
 LET totpag = 1 

 -- Selecionando datos
 LET qrytxt = "SELECT a.fecven,",
                      "0,", 
                      "a.fecemi,",
                      "a.nomcen,", 
                      "a.nomrub,", 
                      "a.numdoc,",
                      "a.totdoc,",
                      "a.totsal,",
                      "0,", 
                      "a.lnkcmp,",
                      "'' ",
                "FROM  vis_movtoscompra a ",
                "WHERE a.fecven <= TODAY+30 ", 
                  "AND a.codsoc = ",w_mae_tra.codsoc,  
                  "AND a.totsal > 0 ",
                  "AND a.estado = 1 ",
                  "AND a.frmpag = 2 ",
                "ORDER BY a.fecven,a.numdoc" 

 PREPARE c_prepare FROM qrytxt
 DECLARE cpagos CURSOR FOR c_prepare 
 FOREACH cpagos INTO v_pagos[totpag].*
  -- Calculando dias vencidos despues de la fecha de vencimiento del pago 
  LET v_pagos[totpag].pdiaven = (TODAY-v_pagos[totpag].pfecven) 
  IF (v_pagos[totpag].pdiaven<0) THEN
     LET v_pagos[totpag].pdiaven = 0 
  END IF 
  LET totpag=totpag+1 
 END FOREACH
 CLOSE cpagos 
 FREE cpagos  
 LET totpag =totpag-1 
 IF (totpag=0) THEN
    RETURN totpag
 END IF 

 -- Totalizando pagos
 CALL bcoing001_TotalPagos()

 -- Seleccionando pagos 
 CALL librut001_dpelement("labeltp","Detalle de Facturas [ "||totpag||" ]")

 LET loop = TRUE 
 WHILE loop
  LET regreso = FALSE 

  INPUT ARRAY v_pagos WITHOUT DEFAULTS FROM s_pagos.*
   ATTRIBUTE(MAXCOUNT=totpag,INSERT ROW=FALSE,APPEND ROW=FALSE,
             ACCEPT=FALSE,CANCEL=FALSE,
             DELETE ROW=FALSE,UNBUFFERED=TRUE,FIELD ORDER FORM)

   ON ACTION cancel 
    -- Regreso
    LET loop    = FALSE 
    LET regreso = TRUE 
    EXIT INPUT

   ON ACTION accept
    -- Salida
    EXIT INPUT 

   BEFORE FIELD ptotpag 
    -- Totalizando pagos
    CALL bcoing001_TotalPagos()

   AFTER FIELD ptotpag 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE() 

    -- Verificando total a pagar
    IF v_pagos[arr].ptotpag IS NULL OR
       v_pagos[arr].ptotpag>v_pagos[arr].ptotsal THEN
       LET v_pagos[arr].ptotpag = v_pagos[arr].ptotsal 
    END IF 

    -- Totalizando pagos
    CALL bcoing001_TotalPagos()
  END INPUT   
  IF NOT loop THEN
     EXIT WHILE
  END IF 

  -- Totalizando pagos
  CALL bcoing001_TotalPagos()

  -- Verificando total de pagos 
  IF (w_mae_tra.totpag<=0) THEN
   CALL fgl_winmessage(
   "Atencion",
   "Valor total del pago no puede ser cero.\n"|| 
   "Deben seleccionarse facturas a pagar.",
   "stop")
   LET loop = TRUE 
   CONTINUE WHILE 
  END IF

  -- Verificando que total de pagos no sea mayor a saldo pendiente 
  IF (w_mae_tra.totpag>totsaldo) THEN
   CALL fgl_winmessage(
   "Atencion",
   "Valor total del pago mayor a saldo pendiente.\n"|| 
   "Saldo Pendiente ("||totsaldo||") "||
   "Valor Pago ("||w_mae_tra.totpag||") ", 
   "stop")
   LET loop = TRUE 
   CONTINUE WHILE 
  END IF

  LET loop = FALSE 
 END WHILE 

 RETURN regreso 
END FUNCTION 

-- Subrutina para totalizar el detalle de pagos  

FUNCTION bcoing001_TotalPagos()
 DEFINE i SMALLINT

 -- Totalizando pagos 
 LET w_mae_tra.totpag = 0
 LET totsaldo = 0 
 FOR i = 1 TO totpag 
  IF v_pagos[i].pfecven IS NULL THEN
     CONTINUE FOR
  END IF
  
  LET w_mae_tra.totpag = w_mae_tra.totpag+v_pagos[i].ptotpag 
  LET totsaldo         = totsaldo+v_pagos[i].ptotsal 
 END FOR 
 DISPLAY BY NAME w_mae_tra.totpag 

 -- Calculando total a pagar
 CALL bcoing001_TotalApagar()
END FUNCTION 

-- Subrutina para calcular el total a pagar

FUNCTION bcoing001_TotalApagar()
 -- Calculando total a pagar
 LET w_mae_tra.totdoc=(w_mae_tra.totpag-w_mae_tra.reisra-
                       w_mae_tra.reishs-w_mae_tra.otrdes)

 DISPLAY BY NAME w_mae_tra.totdoc 
END FUNCTION 

-- Subrutina para ingresar la fecha de emision

FUNCTION bcoing001_FechaEmision()
 -- Habilitando salida del input 
 OPTIONS INPUT NO WRAP 

 -- Ingresando fecha
 INPUT BY NAME w_mae_tra.fecemi WITHOUT DEFAULTS
  ATTRIBUTE(UNBUFFERED,CANCEL=FALSE)

  ON ACTION cancel
   EXIT INPUT  

  AFTER FIELD fecemi
   -- Verificando fecha
   IF w_mae_tra.fecemi > (TODAY+w_mae_par.diapos) OR 
      w_mae_tra.fecemi < (TODAY-w_mae_par.diaing) THEN
      CALL fgl_winmessage(
      "Atencion",
      "Fecha de emision del pago INVALIDA.",
      "stop") 
      NEXT FIELD fecemi 
   END IF 
 END INPUT   

 -- Deshabilitando salida del input 
 OPTIONS INPUT WRAP 
END FUNCTION 

-- Subrutina para inicializar las variables de trabajo 

FUNCTION bcoing001_inival(i)
 DEFINE i       SMALLINT,
        wfecha  VARCHAR(80) 
 
 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1  
   INITIALIZE w_mae_tra.* TO NULL 
   CALL bcoing001_inivec()
   CLEAR FORM 
 END CASE 

 -- Inicializando datos
 LET w_mae_tra.lnkbco = 0                    
 LET w_mae_tra.codemp = gcodemp 
 LET w_mae_tra.fecemi = CURRENT  
 LET w_mae_tra.feccob = CURRENT 
 LET w_mae_tra.tipmov = gtipmov 
 LET w_mae_tra.totpag = 0
 LET w_mae_tra.totdoc = 0
 LET w_mae_tra.reisra = 0 
 LET w_mae_tra.reishs = 0 
 LET w_mae_tra.otrdes = 0 
 LET w_mae_tra.tipsol = 1 
 LET w_mae_tra.estado = 1
 LET w_mae_tra.tipori = 0
 LET w_mae_tra.lnkori = 0 
 LET w_mae_tra.lnkcaj = 0 
 LET w_mae_tra.userid = username 
 LET w_mae_tra.fecsis = CURRENT 
 LET w_mae_tra.horsis = CURRENT HOUR TO SECOND
 LET totaldeb         = 0
 LET totalhab         = 0

 LET w_mae_tra.nofase = 1
 IF w_mae_tra.tipmov=1 THEN 
    LET w_mae_tra.nofase = 0 
 END IF 

 -- Obteniendo datos de la empresa 
 INITIALIZE w_mae_emp.* TO NULL 
 CALL librut003_bempresa(w_mae_tra.codemp)
 RETURNING w_mae_emp.*,existe 

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_tra.fecemi,
                 w_mae_tra.userid,
                 w_mae_tra.fecsis,
                 w_mae_tra.feccob,
                 w_mae_tra.horsis,
                 w_mae_tra.estado,
                 w_mae_tra.nofase,
                 w_mae_tra.codemp,
                 w_mae_tra.tipmov, 
                 totalhab,
                 totaldeb 

 -- Desplegando fecha 
 LET wfecha = librut001_formatofecha(TODAY,1,wpais) CLIPPED
 CALL librut001_dpelement("labelz",wfecha)
END FUNCTION

-- Subrutina para inicializar el vector de trabajo

FUNCTION bcoing001_inivec()
 -- Inicializando pagos facturas 
 CALL v_pagos.clear() 
 LET totpag = 0 
 DISPLAY ARRAY v_pagos TO s_pagos.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 

 -- Inicializando poliza contable 
 CALL v_partida.clear() 
 LET totlin = 0 
 DISPLAY ARRAY v_partida TO s_partida.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION  

-- Subrutina para desplegar los estados del menu 

FUNCTION bcoing001_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement(
         "group1","Datos del Pago - MENU")
  WHEN 1 CALL librut001_dpelement(
         "group1","Datos del Pago - CONSULTAR"||msg CLIPPED)
  WHEN 2 CALL librut001_dpelement(
         "group1","Datos del Pago - ANULAR"||msg CLIPPED)
  WHEN 3 CALL librut001_dpelement(
         "group1","Datos del Pago - ELIMINAR"||msg CLIPPED)
  WHEN 4 CALL librut001_dpelement(
         "group1","Datos del Pago - EMITIR")
 END CASE
END FUNCTION

-- Subutina para el ingreso del detalle de cuentas de la poliza contable

FUNCTION bcoing001_DetallePoliza()
 DEFINE w_mae_cta  RECORD LIKE ctb_mcuentas.*,
        loop,scr   SMALLINT,
        opc,arr,i  SMALLINT, 
        linea      SMALLINT, 
        retroceso  SMALLINT, 
        lastkey    INTEGER  

 -- Inicializando la poliza contable 
 IF (totlin=0) THEN
    -- Linea del debe 
    -- CALL bcoing001_PolizaDefault(CuentaDebe,w_mae_tra.totdoc,0,1) 
    -- Linea del haber 
    -- CALL bcoing001_PolizaDefault(w_mae_cbo.numctb,0,w_mae_tra.totdoc,2) 
 END IF 

 -- Iniciando el loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop

  -- Ingresando partida 
  INPUT ARRAY v_partida WITHOUT DEFAULTS FROM s_partida.*
   ATTRIBUTE(COUNT=totlin,INSERT ROW=FALSE,ACCEPT=FALSE,CANCEL=FALSE,
             UNBUFFERED=TRUE,FIELD ORDER FORM)

   ON ACTION accept 
    -- Aceptar
    EXIT INPUT

   ON ACTION cancel 
    -- Cancelar
    LET retroceso = TRUE
    LET loop      = FALSE
    EXIT INPUT

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   ON ACTION listacuenta  
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Seleccionado lista de cuentas contables
    CALL librut002_ListaBusqueda("Consulta de Cuentas Contables ",
                            "# Cuenta",
                            "Nombre de la Cuenta",
                            "",
                            "numcta",
                            "nomcta",
                            "",
                            "ctb_mcuentas",
                            "codemp = "||w_mae_tra.codemp||" AND tipcta='D'",
                            1)
    RETURNING v_partida[arr].cnumcta,v_partida[arr].cnomcta,regreso
    IF regreso THEN
       NEXT FIELD cnumcta
    ELSE 
       -- Asignando descripcion
       DISPLAY v_partida[arr].cnumcta TO s_partida[scr].cnumcta 
       DISPLAY v_partida[arr].cnomcta TO s_partida[scr].cnomcta 
    END IF  

   ON ACTION rubros 
    -- Mostrando lista resumen de rubros incluidos  
    CALL bcoing001_ResumenRubrosCaja(w_mae_tra.lnkcaj) 
   
   BEFORE INPUT 
    CALL DIALOG.setActionHidden("append",TRUE)

    -- Totalizando cuentas
    CALL bcoing001_TotalCuentasPoliza() 

   BEFORE ROW
    LET totlin = ARR_COUNT()

   BEFORE FIELD cnumcta 
    -- Habilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",TRUE)

   AFTER FIELD cnumcta 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) THEN 
       NEXT FIELD cnumcta 
    END IF
 
   BEFORE FIELD ctotdeb 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    IF v_partida[arr].cnumcta IS NULL THEN 
     -- Seleccionado lista de cuentas contables
     CALL librut002_ListaBusqueda("Consulta de Cuentas Contables ",
                            "# Cuenta",
                            "Nombre de la Cuenta",
                            "",
                            "numcta",
                            "nomcta",
                            "",
                            "ctb_mcuentas",
                            "codemp = "||w_mae_tra.codemp||" AND tipcta='D'",
                            1)
     RETURNING v_partida[arr].cnumcta,v_partida[arr].cnomcta,regreso 
     IF regreso THEN
        NEXT FIELD cnumcta
     ELSE 
        -- Asignando descripcion
        DISPLAY v_partida[arr].cnumcta TO s_partida[scr].cnumcta 
        DISPLAY v_partida[arr].cnomcta TO s_partida[scr].cnomcta 
     END IF  
    END IF  

    -- Verificando numero de cuenta 
    IF (LENGTH(v_partida[arr].cnumcta)<=0) THEN 
       CALL fgl_winmessage(
       "Atencion",
       "Numero de cuenta invalida, VERIFICA.",
       "stop")
       INITIALIZE v_partida[arr].cnumcta THRU v_partida[arr].cconcpt TO NULL
       CLEAR s_partida[scr].cnumcta,s_partida[scr].cnomcta, 
             s_partida[scr].ctotdeb,s_partida[scr].ctothab, 
             s_partida[scr].cconcpt 
       NEXT FIELD cnumcta 
    END IF 

    -- Verificando si la cuenta existe   
    INITIALIZE w_mae_cta.* TO NULL
    CALL librut003_BCuentaContable(w_mae_tra.codemp,v_partida[arr].cnumcta)
    RETURNING w_mae_cta.*,existe
    IF NOT existe THEN
       CALL fgl_winmessage(
       "Atencion",
       "Numero de cuenta no registrada, VERIFICA.",
       "top")
       INITIALIZE v_partida[arr].cnumcta THRU v_partida[arr].cconcpt TO NULL
       CLEAR s_partida[scr].cnumcta,s_partida[scr].cnomcta, 
             s_partida[scr].ctotdeb,s_partida[scr].ctothab, 
             s_partida[scr].cconcpt
       NEXT FIELD cnumcta 
    END IF 

    -- Asignando descripcion
    LET v_partida[arr].cnomcta = w_mae_cta.nomcta 
    DISPLAY v_partida[arr].cnomcta TO s_partida[scr].cnomcta 

    -- Verificando que cuenta se de detalle 
    IF w_mae_cta.tipcta="M" THEN 
       CALL fgl_winmessage(
       "Atencion",
       "Numero de cuenta no es de detalle, VERIFICA.",
       "stop")
       INITIALIZE v_partida[arr].cnumcta THRU v_partida[arr].cconcpt TO NULL
       CLEAR s_partida[scr].cnumcta,s_partida[scr].cnomcta, 
             s_partida[scr].ctotdeb,s_partida[scr].ctothab, 
             s_partida[scr].cconcpt
       NEXT FIELD cnumcta
    END IF 

    -- Deshabilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",FALSE)

    -- Totalizando cuentas
    CALL bcoing001_TotalCuentasPoliza() 

   ON CHANGE ctotdeb 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Totalizando cuentas
    CALL bcoing001_TotalCuentasPoliza() 

   AFTER FIELD ctotdeb
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD ctotdeb 
    END IF

    -- Verificando total del debe
    IF v_partida[arr].ctotdeb IS NULL OR
       (v_partida[arr].ctotdeb <0) OR
       (v_partida[arr].ctotdeb >w_mae_par.totval) THEN 
       LET v_partida[arr].ctotdeb = 0 
    END IF 
    
    -- Totalizando cuentas
    CALL bcoing001_TotalCuentasPoliza() 

   ON CHANGE ctothab 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Totalizando cuentas
    CALL bcoing001_TotalCuentasPoliza() 

   AFTER FIELD ctothab 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD ctothab 
    END IF

    -- Verificando total del haber 
    IF v_partida[arr].ctothab IS NULL OR
       (v_partida[arr].ctothab <0) OR
       (v_partida[arr].ctothab >w_mae_par.totval) THEN 
       LET v_partida[arr].ctothab = 0 
    END IF 

    -- Verificando totales de debe y haber = 0
    IF (v_partida[arr].ctotdeb=0) AND (v_partida[arr].ctothab=0) THEN
       LET msg = "Tota del debe y haber no pueden ser ambos ceros, VERIFICA."
       CALL fgl_winmessage("Atencion",msg,"stop")
       NEXT FIELD ctotdeb 
    END IF

    -- Verificando totales de debe y haber >0 
    IF (v_partida[arr].ctotdeb>0) AND (v_partida[arr].ctothab>0) THEN
     LET msg = "Totales debe y haber no pueden ser ambos mayores a cero, VERIFICA."
     CALL fgl_winmessage("Atencion",msg,"stop")
     NEXT FIELD ctotdeb 
    END IF

    -- Totalizando cuentas
    CALL bcoing001_TotalCuentasPoliza() 

   AFTER FIELD cconcpt 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD cconcpt 
    END IF

   AFTER ROW,INSERT  
    LET totlin = ARR_COUNT()

    -- Totalizando cuentas
    CALL bcoing001_TotalCuentasPoliza() 

   ON ACTION delete
    -- Eliminando linea seleccionada
    LET arr = ARR_CURR() 
    CALL v_partida.deleteElement(arr)

    -- Totalizando cuentas
    CALL bcoing001_TotalCuentasPoliza() 
    NEXT FIELD cnumcta 

   ON ROW CHANGE 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Totalizando cuentas
    CALL bcoing001_TotalCuentasPoliza() 

   AFTER INPUT 
    LET totlin = ARR_COUNT()

    -- Totalizando cuentas
    CALL bcoing001_TotalCuentasPoliza() 
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Desplegando datos 
  DISPLAY ARRAY v_partida TO s_partida.*
   BEFORE DISPLAY 
    EXIT DISPLAY 
  END DISPLAY

  -- Totalizando cuentas
  CALL bcoing001_TotalCuentasPoliza() 

  -- Verificando lineas incompletas 
  LET linea = bcoing001_CuentasPolizaIncompletas()
  IF (linea>0) THEN
     LET msg = " Linea ("||linea||") incompleta. \n VERIFICA."
     CALL fgl_winmessage(
     "Atencion",msg,"stop")
     CONTINUE WHILE
  END IF 

  -- Verificando cuadre de debe y haber 
  IF (totaldeb!=totalhab) OR
     ((totaldeb+totalhab)=0) THEN
     CALL fgl_winmessage(
     "Atencion",
     "Total del debe y haber invalidos."||
     "\nVerificar que ambos totales sean iguales, sin ser ceros.",
     "stop")
     CONTINUE WHILE
  END IF

  LET loop = FALSE 
 END WHILE

 RETURN retroceso
END FUNCTION

-- Subrutina para totalizar el detalle de cuentas de la poliza contable 

FUNCTION bcoing001_TotalCuentasPoliza()
 DEFINE i SMALLINT

 -- Totalizando
 LET totaldeb = 0
 LET totalhab = 0
 FOR i = 1 TO totlin  
  IF v_partida[i].ctotdeb IS NOT NULL THEN
     LET totaldeb = (totaldeb+v_partida[i].ctotdeb)
  END IF
  IF (v_partida[i].ctothab IS NOT NULL) THEN
     LET totalhab = (totalhab+v_partida[i].ctothab)
  END IF
 END FOR
 DISPLAY BY NAME totaldeb,totalhab
END FUNCTION

-- Subrutina para verificar si hay cuentas incompletas

FUNCTION bcoing001_CuentasPolizaIncompletas()
 DEFINE i,linea SMALLINT 

 LET linea = 0 
 FOR i = 1 TO totlin 
  IF v_partida[i].cnumcta IS NULL THEN
     CONTINUE FOR
  END IF

  -- Verificando lineas
  IF v_partida[i].cnomcta IS NULL OR
     v_partida[i].ctotdeb IS NULL OR
     v_partida[i].ctothab IS NULL THEN 
     LET linea = i 
     EXIT FOR 
  END IF 
 END FOR

 RETURN linea 
END FUNCTION

-- Subrutina para asignar datos a poliza default 

FUNCTION bcoing001_PolizaDefault(wnumcta,wtotdeb,wtothab,idx) 
 DEFINE w_mae_cta RECORD LIKE ctb_mcuentas.*,
        wnumcta   LIKE ctb_mcuentas.numcta,
        wtotdeb   DEC(14,2),
        wtothab   DEC(14,2),
        idx       SMALLINT 

 -- Asignando datos
 LET v_partida[idx].cnumcta = wnumcta 

 -- Obteniendo nombre de la cuenta
 INITIALIZE w_mae_cta.* TO NULL
 CALL librut003_BCuentaContable(w_mae_tra.codemp,v_partida[idx].cnumcta) 
 RETURNING w_mae_cta.*,existe 
 LEt v_partida[idx].cnomcta = w_mae_cta.nomcta 

 LEt v_partida[idx].ctotdeb = wtotdeb
 LEt v_partida[idx].ctothab = wtothab 
 LEt v_partida[idx].cconcpt = NULL 
END FUNCTION 

-- Subrutina para mostrar la lista de los rubros resumen si el pago es caja

FUNCTION bcoing001_ResumenRubrosCaja(wlnkcaj) 
 DEFINE wlnkcaj     LIKE bco_mtransac.lnkcaj,
        xtext       STRING

 -- Seleccionando datos
 CALL v_rubros.clear() 
 DECLARE crubs CURSOR FOR
 SELECT x.codrub,
        y.nomrub,
        x.tipdoc, 
        NVL(SUM(x.totgra),0),
        NVL(SUM(x.totexe),0),
        NVL(SUM(x.totisv),0),
        NVL(SUM(x.totedp),0),
        NVL(SUM(x.totigt),0), 
        NVL(SUM(x.valgto)-SUM(x.totisv),0),
        NVL(SUM(x.valgto),0),
        "" 
 FROM   fac_dcachica x,fac_rubgasto y 
 WHERE  x.lnkcaj = wlnkcaj
   AND  y.codrub = x.codrub 
 GROUP BY 1,2,3
 ORDER BY 2,3 
 LET totrub = 1 
 FOREACH crubs INTO v_rubros[totrub].*
  LET totrub = totrub+1
 END FOREACH
 LET totrub = totrub-1
 CLOSE crubs
 FREE  crubs

 -- Desplegando resumen 
 IF (totrub>0) THEN
  -- Verificando si tipo de solicitud es de caja o liquidacion
  IF (w_mae_tra.tipsol=3) THEN
   IF w_mae_caj.tipcaj=3 THEN
    LET xtext = "Resumen de Rubros Liquidacion # ",
                w_mae_caj.numdoc USING "<<<<<<<<<<"," de Fecha ",w_mae_caj.fecemi 
   ELSE
    LET xtext = "Resumen de Rubros Caja # ",
                w_mae_caj.numdoc USING "<<<<<<<<<<"," de Fecha ",w_mae_caj.fecemi 
   END IF 
  END IF 

  OPEN WINDOW wing001r AT 5,2  
   WITH FORM "bcoing001b" 
   ATTRIBUTE(BORDER,TEXT=xtext) 
   
   -- Desplegando rubros 
   DISPLAY ARRAY v_rubros TO s_rubros.*
    ATTRIBUTE (COUNT=totrub) 
    ON ACTION cancel
     EXIT DISPLAY 
    ON ACTION imprimir  
     -- Imprimiendo resumen de rubros 
     CALL bcoing001_GenerarResumenRubros() 
   END DISPLAY 
  CLOSE WINDOW wing001r 
 ELSE
  CALL fgl_winmessage(
  "Atencion","No existen rubros registrados, VERIFICA."||
  "\nVerifica","stop") 
 END IF 
END FUNCTION 

-- Subrutina para generar el resumen de rubros de gasto 

FUNCTION bcoing001_GenerarResumenRubros()
DEFINE filename       STRING,   
       pipeline,xtext STRING   

 -- Definiendo archivo de impresion
 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/ResumenRubros.spl"

 -- Asignando valores
 LET pipeline = "pdf2"   

 -- Verificando tipo de caja  
 IF w_mae_caj.tipcaj=3 THEN
    LET xtext = "Resumen-Rubros-Liquidacion" 
 ELSE
    LET xtext = "Resumen-Rubros-Caja"
 END IF 

 -- Iniciando reporte
 START REPORT bcoing001_ImprimirResumenRubros TO filename 
  
  -- Llenando reporte
  OUTPUT TO REPORT bcoing001_ImprimirResumenRubros(w_mae_tra.*)

 -- Finalizando reporte 
 FINISH REPORT bcoing001_ImprimirResumenRubros 

 -- Enviando reporte al destino seleccionado
 CALL librut001_sendreport
 (filename,pipeline,"Resumen Rubros", 
 "--noline-numbers "||
 "--nofooter "||
 "--font-size 8 "||
 "--page-width 842 --page-height 595 "||
 "--left-margin 35 --right-margin 15 "||
 "--top-margin 35 --bottom-margin 45 "||
 "--title '"||xtext CLIPPED||"'")
END FUNCTION 

-- Subrutinar para imprimir el resumen de rubros

REPORT bcoing001_ImprimirResumenRubros(imp1) 
 DEFINE imp1        RECORD LIKE bco_mtransac.*,
        totales     RECORD
         totgra     LIKE fac_dcachica.totgra, 
         totexe     LIKE fac_dcachica.totgra, 
         totisv     LIKE fac_dcachica.totgra, 
         totedp     LIKE fac_dcachica.totedp, 
         totigt     LIKE fac_dcachica.totigt,
         tosiva     LIKE fac_dcachica.valgto,
         tociva     LIKE fac_dcachica.valgto
        END RECORD,
        exis,i,col  SMALLINT, 
        reimpresion SMALLINT,
        xnomemp     CHAR(50), 
        xnomcen     CHAR(50), 
        xnomest     CHAR(12), 
        xnumest     CHAR(40), 
        xtext       CHAR(100), 
        linea       CHAR(138) 

  OUTPUT LEFT   MARGIN  0 
         PAGE   LENGTH 50 
         TOP    MARGIN  0 
         BOTTOM MARGIN  0 

 FORMAT 
  BEFORE GROUP OF imp1.lnkbco 
   -- Definiendo linea
   LET linea = "__________________________________________________", 
               "__________________________________________________",
               "______________________________________"

   -- Obteniendo datos de la empresa
   INITIALIZE xnomemp TO NULL 
   SELECT a.nomemp INTO xnomemp FROM glb_empresas a
    WHERE a.codemp = w_mae_caj.codemp 

   -- Obteniendo datos del centro de costo 
   INITIALIZE xnomcen TO NULL 
   SELECT a.nomcen INTO xnomcen FROM glb_cencosto a
    WHERE a.codcos = w_mae_caj.cencos 

   -- Obteniendo datos del establecimiento sat 
   INITIALIZE xnumest TO NULL 
   SELECT a.nomest INTO xnumest FROM glb_estabsat a
    WHERE a.numest = w_mae_caj.numest 

   -- Verificando estado
   LET xnomest = "ABIERTA" 
   IF (w_mae_caj.estado=1) THEN LET xnomest = "LIQUIDADA" END IF

   -- Imprimiendo datos
   -- Verificando tipo de caja  
   IF w_mae_caj.tipcaj=3 THEN
    PRINT "NUMERO ID LIQUIDACION (",w_mae_caj.lnkcaj USING "<<<<<<<<<<",")"
    LET xtext = "Numero de Liquidacion     : ",w_mae_caj.numdoc
   ELSE
    PRINT "NUMERO ID CAJA (",w_mae_caj.lnkcaj USING "<<<<<<<<<<",")"
    LET xtext = "Numero de Caja            : ",w_mae_caj.numdoc
   END IF 

   SKIP 1 LINES 
   PRINT COLUMN   1,"Empresa                   : ",xnomemp CLIPPED
   PRINT COLUMN   1,"Centro de Costo           : ",xnomcen CLIPPED
   PRINT COLUMN   1,xtext CLIPPED, 
         COLUMN 105,"Estado            : ",xnomest CLIPPED 
   SKIP 1 LINES 
   PRINT COLUMN   1,"Observaciones             : ",w_mae_caj.observ[1,100]
    IF LENGTH(w_mae_caj.observ)>100 THEN
       PRINT COLUMN 29,w_mae_caj.observ[101,200]  
    END IF 
    SKIP 1 LINES 
   PRINT COLUMN   1,"Establecimiento SAT.      : ",xnumest CLIPPED, 
         COLUMN 105,"Usuario Registro  : ",w_mae_caj.userid CLIPPED 
   PRINT COLUMN   1,"+ Total Reintegro Q.      : ",
                    w_mae_caj.totrei USING "----,--&.&&",
         COLUMN 105,"Fecha de Registro : ",w_mae_caj.fecsis
   PRINT COLUMN   1,"+ Total Saldo Anterior Q. : ",
                    w_mae_caj.salant USING "----,--&.&&",
         COLUMN 105,"Hora de Registro  : ",w_mae_caj.horsis  
   PRINT COLUMN   1,"= Total Caja Q.           : ",
                    w_mae_caj.totval USING "----,--&.&&",
         COLUMN 105,"Usuario Liquido   : ",w_mae_caj.usrliq CLIPPED 
   PRINT COLUMN   1,"- Total Gastos Q.         : ",
                    w_mae_caj.totgto USING "----,--&.&&",
         COLUMN 105,"Fecha Liquidacion : ",w_mae_caj.fecliq 
   PRINT COLUMN   1,"= Saldo Actual Q.         : ",
                    w_mae_caj.totsal USING "----,--&.&&",
         COLUMN 105,"Hora Liquidacion  : ",w_mae_caj.horliq
 
   SKIP 1 LINES 
   PRINT COLUMN   1,"DETALLE DE RUBROS DE GASTO"  
   PRINT COLUMN   1,"Rubro de Gasto                          ",
                    "  Tipo  Total Gravado  Total Exento   Total IDP  ",
                    "Total Inguat  Total Iva  Total S/Iva  Total C/Iva"  
   PRINT linea

   -- Imprimiendo detalle de pagos          
   LET totales.totgra = 0
   LET totales.totexe = 0
   LET totales.totedp = 0
   LET totales.totigt = 0
   LET totales.totisv = 0
   LET totales.tosiva = 0
   LET totales.tociva = 0

   FOR i = 1 TO totrub 
    IF v_rubros[i].codrub IS NULL THEN 
       CONTINUE FOR
    END IF

    PRINT COLUMN   1,v_rubros[i].nomrub                    ,2  SPACES,
                     v_rubros[i].tipdoc                    ,4  SPACES, 
                     v_rubros[i].totgra  USING "###,##&.&&",4  SPACES,
                     v_rubros[i].totexe  USING "###,##&.&&",2  SPACES,
                     v_rubros[i].totedp  USING "###,##&.&&",2  SPACES,
                     v_rubros[i].totigt  USING "###,##&.&&",3  SPACES,
                     v_rubros[i].totisv  USING "###,##&.&&",3  SPACES,
                     v_rubros[i].tosiva  USING "###,##&.&&",3  SPACES,
                     v_rubros[i].tociva  USING "###,##&.&&"

    -- Totalizando 
    LET totales.totgra = totales.totgra+v_rubros[i].totgra
    LET totales.totexe = totales.totexe+v_rubros[i].totexe
    LET totales.totedp = totales.totedp+v_rubros[i].totedp
    LET totales.totigt = totales.totigt+v_rubros[i].totigt
    LET totales.totisv = totales.totisv+v_rubros[i].totisv
    LET totales.tosiva = totales.tosiva+v_rubros[i].tosiva
    LET totales.tociva = totales.tociva+v_rubros[i].tociva
   END FOR 
   PRINT linea 

   -- Imprimiendo totales 
   PRINT COLUMN  52,totales.totgra  USING "###,##&.&&",4  SPACES,
                    totales.totexe  USING "###,##&.&&",2  SPACES,
                    totales.totedp  USING "###,##&.&&",2  SPACES,
                    totales.totigt  USING "###,##&.&&",3  SPACES,
                    totales.totisv  USING "###,##&.&&",3  SPACES,
                    totales.tosiva  USING "###,##&.&&",3  SPACES,
                    totales.tociva  USING "###,##&.&&"


   PRINT linea 
   PRINT "Impresion: ",username CLIPPED," ",today," ",time 
END REPORT 
