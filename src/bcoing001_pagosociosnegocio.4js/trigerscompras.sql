
drop trigger trginsertpagoscompras;
create trigger trginsertpagoscompras insert on bco_dtransac
    referencing new as pos
    for each row
    (
     execute procedure sptsaldofacturascompra(pos.lnkcmp)
    );

drop trigger trgdeletepagoscompras;
create trigger trgdeletepagoscompras delete on bco_dtransac
    referencing old as pre
    for each row
    (
     execute procedure sptsaldofacturascompra(pre.lnkcmp)
    );

drop trigger trganulapagoscompras;
create trigger trganulapagoscompras update of estado on bco_dtransac
    referencing old as pre
    for each row
    (
     execute procedure sptsaldofacturascompra(pre.lnkcmp)
    );
