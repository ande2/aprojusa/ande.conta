{ 
Programo : Mynor Ramirez
Objetivo : Programa de criterios de seleccion para:
           Consultar/anular/eliminar movimiento de pagos socios de negocios 
} 

-- Definicion de variables globales 
GLOBALS "bcoglb001.4gl"
DEFINE v_consulta DYNAMIC ARRAY OF RECORD 
        tlnkbco   INT, 
        tnomsol   CHAR(25), 
        tnomsoc   CHAR(40),
        tfecemi   DATE,
        tnumcta   CHAR(20),
        tnumdoc   CHAR(20),
        ttotdoc   DEC(14,2), 
        trellen   CHAR(1) 
       END RECORD 
DEFINE totrec INT 

-- Subrutina para consultar/anular pagos socios 

FUNCTION bcoqbx001_PagosSocios(operacion)
 DEFINE qrytext,qrypart,qry STRING, 
        loop,existe         SMALLINT,
        operacion,arr       SMALLINT,
        qryres,msg          CHAR(80),
        cmdstr              STRING, 
        wherestado          STRING,
        opc,totreg,res      INTEGER 

 -- Verificando operacion
 CASE (operacion)
  WHEN 1 LET wherestado = NULL
  WHEN 2 LET wherestado = " AND a.estado = 1" 
  WHEN 3 LET wherestado = " AND a.estado = 1" 
 END CASE 

 -- Definiendo nivel de aislamiento 
 SET ISOLATION TO DIRTY READ 

 -- Cargando combobox de socios 
 CALL librut003_CbxProveedores() 
 -- Cargando combobox de autorizantes
 CALL librut003_CbxAutorizantes() 
 -- Cargando ids de las cajas o liquidaciones ya liquidadas
 CALL librut003_CbxIdsCajas("")

 -- Buscando datos 
 LET loop = TRUE 
 WHILE loop 
  -- Inicializando las variables 
  CALL bcoing001_EstadoMenu(operacion," ")
  INITIALIZE qrytext,qrypart TO NULL
  LET int_flag = 0 
  CLEAR FORM
  DISPLAY gcodemp TO codemp 
                                                     
  -- Construyendo busqueda 
  CONSTRUCT BY NAME qrytext 
                 ON a.fecemi,
                    a.tipmov, 
                    a.codsoc,
                    a.tipsol,
                    a.lnkcaj, 
                    a.feccob,
                    a.codcos, 
                    a.codaut,
                    a.codrub,
                    a.numcta,
                    a.numdoc, 
                    a.totpag, 
                    a.reisra,
                    a.reishs,
                    a.otrdes, 
                    a.totdoc,
                    a.nomchq, 
                    a.descrp,
                    a.lnkbco, 
                    a.estado,
                    a.nofase, 
                    a.userid,
                    a.fecsis, 
                    a.horsis,
                    a.usranl,
                    a.fecanl,
                    a.horanl,
                    a.motanl

   ON ACTION cancel
    -- Salida
    LET loop = FALSE
    EXIT CONSTRUCT
  END CONSTRUCT
  IF NOT loop THEN
     EXIT WHILE
  END IF 

  -- Preparando la busqueda 
  ERROR " Seleccionando datos ... por favor espere ..." ATTRIBUTE(CYAN) 

  -- Creando la busqueda 
  LET qrypart = "SELECT a.lnkbco,",
                       "CASE(a.tipsol) ",
                       "WHEN 1 THEN 'CON FACTURA' ",
                       "WHEN 2 THEN 'SIN FACTURA' ",
                       "WHEN 3 THEN 'CAJA/LIQ' END,",
                       "a.nomsoc,a.fecemi,a.numcta,a.numdoc,a.totdoc,'' ",
                 "FROM  bco_mtransac a ",
                 "WHERE a.codemp = ",gcodemp,
                  " AND a.tipmov IN (SELECT x.tipmov FROM bco_tipomovs x ",
                                     "WHERE x.tipmov = a.tipmov AND x.pagpro = 1) ",
                  " AND ",qrytext CLIPPED,wherestado CLIPPED,
                 " ORDER BY a.lnkbco DESC" 

  -- Contando registros
  LET totreg = librut003_NumeroRegistros(qrypart)

  -- Declarando el cursor 
  PREPARE estqbx001 FROM qrypart
  DECLARE c_pagpro SCROLL CURSOR WITH HOLD FOR estqbx001  

  LET totrec = 1
  CALL v_consulta.clear()
  FOREACH c_pagpro INTO v_consulta[totrec].*
   LET totrec = totrec+1
  END FOREACH
  CLOSE c_pagpro        
  FREE  c_pagpro
  LET totrec = totrec-1

  -- Verificando si hay movimientos
  IF (totrec=0) THEN
     ERROR ""
     CALL fgl_winmessage(
     "Atencion","No existen pagos con el criterio seleccionado.","stop")
  ELSE
     ERROR "" 

     -- Escondiendo tabla de consultas 
     CALL f.SetElementHidden("titulolista",0) 
     CALL f.SetElementHidden("tablaconsulta",0) 

     -- Desplegando datos 
     DIALOG ATTRIBUTES(UNBUFFERED,FIELD ORDER FORM)
 
      -- Desplegando movimientos
      DISPLAY ARRAY v_consulta TO s_consulta.* 
       ATTRIBUTE (COUNT=totrec,KEEP CURRENT ROW) 

       BEFORE DISPLAY 
        CALL Dialog.SetActionHidden("close",1)

        -- Verificando tipo de operacion 
        CASE (operacion)
         WHEN 1 -- Desabilitando acciones              
          CALL Dialog.SetActionActive("anular",0)
          CALL Dialog.SetActionActive("delete",0)
         WHEN 2 -- Desabilitando acciones              
          CALL Dialog.SetActionActive("delete",0)
          CALL Dialog.SetActionActive("imprimir",0)
          CALL Dialog.SetActionActive("cheque",0)
         WHEN 3 -- Desabilitando acciones              
          CALL Dialog.SetActionActive("anular",0)
          CALL Dialog.SetActionActive("imprimir",0)
          CALL Dialog.SetActionActive("cheque",0)
        END CASE

        -- Desplegando datos 
        LET msg = " - Registros [ ",totreg||" ]"
        CALL bcoing001_EstadoMenu(operacion,msg CLIPPED)

       BEFORE ROW
        LET arr = DIALOG.getcurrentrow("s_consulta") 
        IF (arr>totrec) THEN
           CALL FGL_SET_ARR_CURR(totrec)
        ELSE   
           -- Desplegando datos 
           CALL bcoqbx001_datos(v_consulta[arr].tlnkbco) 

           -- Verificando si tipo de pago es liquidacion o caja 
           CALL Dialog.SetActionActive("rubros",0)
           IF (w_mae_tra.tipsol=3) THEN 
              CALL Dialog.SetActionActive("rubros",1)
           END IF 
        END IF 

       AFTER ROW 
        LET arr = DIALOG.getcurrentrow("s_consulta") 
        IF (arr<=totrec) THEN
           -- Desplegando datos 
           CALL bcoqbx001_datos(v_consulta[arr].tlnkbco) 
        END IF 

       ON ACTION anular 
        -- Verificando si pago ya fue anulado
        IF w_mae_tra.estado=0 THEN
           CALL fgl_winmessage(
           "Atencion","Pago ya fue anulado, VERIFICA.","stop") 
           CONTINUE DIALOG  
        END IF 

        -- Verificando fecha de pago 
        IF (w_mae_tra.fecemi<(TODAY-w_mae_par.diaanu)) THEN
           CALL fgl_winmessage(
           "Atencion",
           "Pago no puede anularse.\n"||
           "Fecha del pago excede limite de dias. ("||w_mae_par.diaanu||")",
           "stop")
           CONTINUE DIALOG
        END IF

        -- Anulando pago 
        CALL bcoqbx001_AnularPago()      

        -- Desplegando datos
        CALL bcoqbx001_datos(v_consulta[arr].tlnkbco) 

       ON ACTION delete 
        -- Eliminando pago 

         -- Verificando fecha del pago
        IF (w_mae_tra.fecemi<(TODAY-w_mae_par.diaeli)) THEN
           CALL fgl_winmessage(
           "Atencion",
           "Pago no puede eliminarse.\n"||
           "Fecha del pago excede limite de dias. ("||w_mae_par.diaeli||")",
           "stop")
           CONTINUE DIALOG
        END IF

        -- Confirmacion de la accion a ejecutar 
        LET msg = "Esta SEGURO de Eliminar este Pago ? "
        IF librut001_yesornot("Confirmacion",msg,"Si","No","question") THEN 
           -- Eliminando
           CALL bcoing001_grabar(operacion,0)
           EXIT DIALOG   
        END IF  

       ON ACTION imprimir 
        -- Verificando si pago ya fue anulado
        IF w_mae_tra.estado=0 THEN
           CALL fgl_winmessage(
           "Atencion","Pago ya fue anulado, no puede imprimir voucher.","stop") 
           CONTINUE DIALOG  
        END IF 

        -- Reimpresion de vocuher pago a socios de negocios 
        CALL bcorpt001_GenerarVoucher(1)

       ON ACTION cheque   
        -- Verificando si pago ya fue anulado
        IF w_mae_tra.estado=0 THEN
           CALL fgl_winmessage(
           "Atencion","Pago ya fue anulado, no puede imprimir cheque.","stop") 
           CONTINUE DIALOG  
        END IF 

        -- Confirmacion de la accion a ejecutar 
        LET msg = "Esta SEGURO de Imprimir el Cheque ?\n"||
                  "Colocar correctamente el cheque." 
        IF librut001_yesornot("Confirmacion",msg,"Si","No","question") THEN 
           -- Impresion de cheque pago a socios de negocios 
           CALL bcorpt001_GenerarCheque(1)
        END IF  

       ON ACTION rubros
        -- Mostrando lista resumen de rubros incluidos
        CALL bcoing001_ResumenRubrosCaja(w_mae_tra.lnkcaj) 

       ON ACTION partida
        -- Ejecutando consulta de la partida contable
        IF ui.Interface.getChildInstances("transaccionesctb")==0 THEN
           LET cmdstr = "fglrun transaccionesctb.42r 1 "||w_mae_tra.lnkbco||" BCO"
           RUN cmdstr WITHOUT WAITING
        ELSE
           CALL fgl_winmessage(
           "Atencion","Consulta de registro contable ya en ejecucion.","stop")
        END IF
      END DISPLAY 

      -- Desplegando facturas
      DISPLAY ARRAY v_pagos TO s_pagos.*
       ATTRIBUTE (COUNT=totpag,KEEP CURRENT ROW) 
       BEFORE ROW
        -- Verificando control de lineas
        LET arr = DIALOG.getcurrentrow("s_pagos") 
        IF (arr>totpag) THEN
           CALL FGL_SET_ARR_CURR(totpag)
        END IF
      END DISPLAY 

      -- Desplegando poliza 
      DISPLAY ARRAY v_partida TO s_partida.*
       ATTRIBUTE (COUNT=totlin,KEEP CURRENT ROW) 
       BEFORE ROW
        -- Verificando control de lineas
        LET arr = DIALOG.getcurrentrow("s_partida") 
        IF (arr>totlin) THEN
           CALL FGL_SET_ARR_CURR(totlin)
        END IF
      END DISPLAY 

      ON ACTION prevrow   
       -- Siguiente fila 

      ON ACTION nextrow   
       -- Fila Anterior  

      ON ACTION consultar  
       -- Volver a consultar 
       EXIT DIALOG  

      ON ACTION cancel
       -- Salida 
       LET loop = FALSE
       EXIT DIALOG 

      ON KEY(F4,CONTROL-E)
       -- Salida 
       LET loop = FALSE
       EXIT DIALOG 
     END DIALOG 
 
     -- Escondiendo tabla de consultas 
     CALL f.SetElementHidden("titulolista",1) 
     CALL f.SetElementHidden("tablaconsulta",1) 
  END IF     
 END WHILE

 -- Desplegando estado del menu 
 CALL bcoing001_estadoMenu(0,"")

 -- Inicializando datos 
 CALL bcoing001_inival(1)
END FUNCTION 

-- Subrutina para desplegar los datos del pago 

FUNCTION bcoqbx001_datos(xlnkbco)
 DEFINE xlnkbco LIKE bco_mtransac.lnkbco,
        existe  SMALLINT

 -- Obteniendo datos del pago 
 INITIALIZE w_mae_tra.* TO NULL 
 CALL librut003_BTransaccionBanco(xlnkbco) 
 RETURNING w_mae_tra.*,existe

 -- Cargando el combobox de las cuentas bancarias
 DISPLAY "EMPRESA ",w_mae_tra.codemp 
 CALL librut003_CbxCuentasBancoVouchers(w_mae_tra.codemp)

 -- Desplegando datos del documento 
 DISPLAY BY NAME w_mae_tra.codemp, w_mae_tra.codcos,
                 w_mae_tra.fecemi, w_mae_tra.numdoc,
                 w_mae_tra.codaut, w_mae_tra.codrub,
                 w_mae_tra.numcta, w_mae_tra.totdoc, 
                 w_mae_tra.descrp, w_mae_tra.userid, 
                 w_mae_tra.fecsis, w_mae_tra.horsis,
                 w_mae_tra.nomchq, w_mae_tra.estado,
                 w_mae_tra.nofase, w_mae_tra.tipmov,
                 w_mae_tra.motanl, w_mae_tra.usranl,
                 w_mae_tra.fecanl, w_mae_tra.horanl,
                 w_mae_tra.codsoc, w_mae_tra.feccob,
                 w_mae_tra.lnkbco, w_mae_tra.totpag,
                 w_mae_tra.reisra, w_mae_tra.reishs,
                 w_mae_tra.otrdes, w_mae_tra.tipsol,
                 w_mae_tra.lnkcaj 

 -- Obteniendo datos de la caja o liquidacion si pago es caja o liquidacion 
 INITIALIZE w_mae_caj.* TO NULL 
 IF w_mae_tra.tipsol=3 THEN
    -- Obteniendo datos de la caja o liquidacion
    INITIALIZE w_mae_caj.* TO NULL 
    CALL librut003_BCajas(w_mae_tra.lnkcaj)
    RETURNING w_mae_caj.*,existe
 END IF 

 -- Desplegando las facturas pagadas 
 -- Inicializando datos 
 CALL bcoing001_inivec() 

 -- Verificando si tipo de solicitud de pago es con facturas
 IF w_mae_tra.tipsol=1 THEN
    -- Desplegando facturas pagadas
    CALL bcoqbx001_PagosFacturas() 
 END IF 

 -- Desplegando detalle de la poliza contable 
 CALL bcoqbx001_PolizaContable() 
END FUNCTION 

-- Subrutina para desplegar las facturas pagadas 

FUNCTION bcoqbx001_PagosFacturas() 
 DEFINE qrytxt STRING 
                                                             
 -- Selecionando datos
 LET qrytxt = "SELECT a.fecven,",
                     "b.diaven,", 
                     "a.fecemi,",
                     "a.nomcen,", 
                     "a.nomrub,", 
                     "a.numdoc,",
                     "a.totdoc,",
                     "b.totsal,",
                     "b.totpag,", 
                     "a.lnkcmp,",
                     "'' ",
               "FROM  vis_movtoscompra a,bco_dtransac b ",
               "WHERE b.lnkbco = ",w_mae_tra.lnkbco, 
                " AND a.lnkcmp = b.lnkcmp ", 
               "ORDER BY a.fecven,a.numdoc" 

 PREPARE c_prepare1 FROM qrytxt
 DECLARE cfacturas CURSOR FOR c_prepare1
 LET totpag = 1 
 FOREACH cfacturas INTO v_pagos[totpag].*
  LET totpag=totpag+1 
 END FOREACH
 CLOSE cfacturas
 FREE cfacturas 
 LET totpag =totpag-1 

 -- Desplegando facturas
 DISPLAY ARRAY v_pagos TO s_pagos.*
  ATTRIBUTE (COUNT=totpag) 
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION 

-- Subrutina para desplegar el detalle de la poliza contable 

FUNCTION bcoqbx001_PolizaContable() 
 -- Selecionando datos de la poliza 
 DECLARE cpoliza CURSOR FOR 
 SELECT x.numcta, 
        y.nomcta,
        x.totdeb,
        x.tothab,
        x.concep,
        "", 
        x.correl
  FROM  bco_dtpoliza x,ctb_mcuentas y 
  WHERE x.lnkbco = w_mae_tra.lnkbco
    AND y.codemp = w_mae_tra.codemp 
    AND y.numcta = x.numcta
  ORDER BY x.correl 

 LET totlin = 1 
 FOREACH cpoliza INTO v_partida[totlin].*
  LET totlin=totlin+1 
 END FOREACH
 CLOSE cpoliza
 FREE cpoliza
 LET totlin=totlin-1 

 -- Desplegando poliza 
 DISPLAY ARRAY v_partida TO s_partida.*
  ATTRIBUTE (COUNT=totlin) 
  BEFORE DISPLAY   
   EXIT DISPLAY
 END DISPLAY 

 -- Desplegando totales 
 CALL bcoing001_TotalCuentasPoliza()
END FUNCTION 

-- Subrutina para anular el pago 

FUNCTION bcoqbx001_AnularPago()      
 DEFINE anula SMALLINT 

 -- Ingresando motivo de la anulacion
 LET anula = TRUE 
 INPUT BY NAME w_mae_tra.motanl WITHOUT DEFAULTS 
  ATTRIBUTE(UNBUFFERED) 
  ON ACTION accept 
   EXIT INPUT 
  ON ACTION cancel
   LET anula = FALSE 
   EXIT INPUT 
  AFTER FIELD motanl
   -- Verificando motivo de la anulacion 
   IF w_mae_tra.motanl IS NULL THEN
      CALL fgl_winmessage
      ("Atencion","Motivo de la anulacion invalido, VERIFICA.","stop") 
      NEXT FIELD motanl
   END IF 
 END INPUT 
 IF NOT anula THEN
    RETURN
 END IF 

 -- Confirmando anulacion 
 IF NOT librut001_yesornot("Confirmacion",
                           "Desea Anular el Pago ?",
                           "Si",
                           "No",
                           "question") THEN
    RETURN 
 END IF

 -- Iniciando Transaccion
 BEGIN WORK

   -- 1. Anulando movimiento de bancos 
   SET LOCK MODE TO WAIT
   UPDATE bco_mtransac
   SET    bco_mtransac.estado = 0,
          bco_mtransac.motanl = w_mae_tra.motanl, 
          bco_mtransac.fecanl = CURRENT,
          bco_mtransac.horanl = CURRENT HOUR TO SECOND, 
          bco_mtransac.usranl = USER 
   WHERE  bco_mtransac.lnkbco = w_mae_tra.lnkbco 

   -- 2. Anulando pagos de facturas
   IF w_mae_tra.tipsol=1 THEN
      SET LOCK MODE TO WAIT
      UPDATE bco_dtransac
      SET    bco_dtransac.estado = 0
      WHERE  bco_dtransac.lnkbco = w_mae_tra.lnkbco 
   END IF 

   -- 3. Anulando marca si pago es de caja o liquidacion 
   IF w_mae_tra.tipsol=3 THEN
      IF w_mae_tra.lnkcaj>0 THEN
         -- Actualizando caja o liquidacion 
         SET LOCK MODE TO WAIT
         UPDATE fac_cajchica
         SET    fac_cajchica.lnkbco = 0 
         WHERE  fac_cajchica.lnkcaj = w_mae_tra.lnkcaj
      END IF
   END IF 

   -- 3. Reversando poliza contable 
   
  { -- Obteniendo datos de la transaccion original 

   -- Grabando encabezado de la transaccion 
   -- Asignando datos
   LET w_mae_tra.tiptrn = xtiptrn 
   LET w_mae_tra.totdoc = totaldeb
   LET w_mae_tra.codapl = "CTB" 
   LET w_mae_tra.fecemi = TODAY 
   LET w_mae_tra.userid = username 
   LET w_mae_tra.fecsis = CURRENT 
   LET w_mae_tra.horsis = CURRENT HOUR TO SECOND
   LET w_mae_tra.lnkapl = w_mae_tra.lnktra 
   LET w_mae_tra.coddiv = 1  
   LET w_mae_tra.lnktra = 0 
   LET w_mae_tra.estado = 0 

   -- Obteniendo correlativo de transaccion 
   CALL ctbing001_NumeroDocumento() 

   -- Actualizando transaccion original
   SET LOCK MODE TO WAIT 
   UPDATE ctb_mtransac
   SET    ctb_mtransac.estado = 0                     
   WHERE  ctb_mtransac.lnktra = w_mae_tra.lnkapl 

   -- Grabando
   SET LOCK MODE TO WAIT
   INSERT INTO ctb_mtransac
   VALUES (w_mae_tra.*)
   LET w_mae_tra.lnktra = SQLCA.SQLERRD[2] 

   -- 2. Grabando detalle de cuentas de la transaccion 
   LET correl = 0
   FOR i = 1 TO totlin 
     IF v_partida[i].numcta IS NULL OR 
        v_partida[i].totdeb IS NULL OR  
        v_partida[i].tothab IS NULL THEN 
        CONTINUE FOR 
     END IF 
     LET correl = (correl+1) 

     -- Verificando debe y haber
     LET xtmptot = v_partida[i].totdeb
     LET v_partida[i].totdeb = v_partida[i].tothab
     LET v_partida[i].tothab = xtmptot

     -- Verificando debe y haber
     IF (v_partida[i].totdeb>0) THEN
        LET xtipope = "D"
        LET xtotval = v_partida[i].totdeb 
     ELSE
        LET xtipope = "H"
        LET xtotval = v_partida[i].tothab 
     END IF 

     -- Grabando detalle de cuentas 
     SET LOCK MODE TO WAIT
     INSERT INTO ctb_dtransac 
     VALUES (w_mae_tra.lnktra    , -- link del encabezado
             correl              , -- correlativo 
             v_partida[i].numcta , -- numero de cuenta 
             v_partida[i].concpt , -- concepto de la linea 
             v_partida[i].totdeb , -- total debe 
             v_partida[i].tothab , -- total haber 
             xtipope)            

     -- Creando cuentas padre
     CALL ctbing001_CuentasPadre(
             w_mae_tra.lnktra, 
             i,
             w_mae_tra.codemp,
             w_mae_tra.fecemi,      
             v_partida[i].numcta, 
             w_mae_emp.tipnom, 
             xtipope,             
             xtotval, 
             v_partida[i].totdeb,
             v_partida[i].tothab, 
             "N",
             "N")
   END FOR 


} 

   
 -- Finalizando Transaccion
 COMMIT WORK

 CALL fgl_winmessage(
 "Atencion",
 "Pago con Numero de Movimiento "||w_mae_tra.lnkbco CLIPPED||" anulado.",
 "information")
END FUNCTION 
