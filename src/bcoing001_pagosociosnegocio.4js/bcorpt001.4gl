{
Programo : Mynor Ramirez        
Objetivo : Impresion de vouchers cheques de pago.  
}

GLOBALS "bcoglb001.4gl" 
CONSTANT lineasup = 5  
CONSTANT colfec   = 10 
CONSTANT colnom   = 10 
CONSTANT coltot   = 53 
CONSTANT collet   = 10 
CONSTANT lugar    = "GUATEMALA" 
DEFINE w_mae_usr     RECORD LIKE glb_usuarios.*,
       existe        SMALLINT, 
       filename      STRING,   
       pipeline      STRING,   
       i             INT

-- Subrutina para generar el voucher de cheque de socios de negocios 

FUNCTION bcorpt001_GenerarVoucher(reimpresion)
 DEFINE reimpresion SMALLINT 

 -- Verificando si cuenta imprime voucher
 SELECT a.impvou FROM bco_mcuentas a
  WHERE a.numcta = w_mae_tra.numcta
    AND a.impvou = 1 
  IF (status=NOTFOUND) THEN
     RETURN
  END IF 

 -- Definiendo archivo de impresion
 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/VoucherPagoSociosNegocios.spl"

 -- Asignando valores
 LET pipeline = "pdf2"   

 -- Iniciando reporte
 START REPORT bcorpt001_ImprimirVoucher TO filename 
  
  -- Llenando reporte
  OUTPUT TO REPORT bcorpt001_ImprimirVoucher(w_mae_tra.*,reimpresion)

 -- Finalizando reporte 
 FINISH REPORT bcorpt001_ImprimirVoucher 

 -- Enviando reporte al destino seleccionado
 CALL librut001_sendreport
 (filename,pipeline,"Cheque", 
 "--noline-numbers "||
 "--nofooter "||
 "--font-size 8 "||
 "--page-width 595 --page-height 842 "||
 "--left-margin 35 --right-margin 15 "||
 "--top-margin 35 --bottom-margin 45 "||
 "--title Pago-Socios-de-Negocios-Cheque-"||w_mae_tra.numdoc)
END FUNCTION 

-- Subrutinar para imprimir el voucher cheque de socios de negocios 

REPORT bcorpt001_ImprimirVoucher(imp1,reimpresion) 
 DEFINE imp1        RECORD LIKE bco_mtransac.*,
        exis,i,col  SMALLINT, 
        reimpresion SMALLINT,
        xnomemp     CHAR(50), 
        xnombco     CHAR(50), 
        xnomcta     CHAR(50), 
        xnomsoc     CHAR(50), 
        xnomusr     CHAR(30), 
        xnomaut     CHAR(30), 
        linea       CHAR(104), 
        wcanletras  STRING,
        tl          INT 

  OUTPUT LEFT   MARGIN  0 
         PAGE   LENGTH 50 
         TOP    MARGIN  0 
         BOTTOM MARGIN  0 

 FORMAT 
  BEFORE GROUP OF imp1.lnkbco 
   -- Imprimiendo encabezado
   LET wcanletras = librut001_numtolet(imp1.totdoc) 

   -- Definiendo linea
   LET linea = "__________________________________________________", 
               "__________________________________________________",
               "____" 

   -- Obteniendo datos de la empresa
   INITIALIZE xnomemp TO NULL 
   SELECT a.nomemp INTO xnomemp FROM glb_empresas a
    WHERE a.codemp = w_mae_tra.codemp 

   -- Obteniendo datos del socio de negocios 
   INITIALIZE xnomsoc TO NULL 
   SELECT a.nomsoc INTO xnomsoc FROM glb_sociosng a
    WHERE a.codsoc = w_mae_tra.codsoc 

   -- Obteniendo datos del banco
   INITIALIZE xnombco TO NULL 
   SELECT x.nombco INTO xnombco FROM glb_mtbancos x,bco_mcuentas y 
    WHERE y.numcta = w_mae_tra.numcta
      AND x.codbco = y.codbco 

   -- Obteniendo datos de la cuenta de bancos 
   INITIALIZE xnomcta TO NULL 
   SELECT x.nomcta INTO xnomcta FROM bco_mcuentas x 
    WHERE x.numcta = w_mae_tra.numcta

   -- Imprimiendo datos
   PRINT "CONSTANCIA DE EGRESOS NUMERO (",
         w_mae_tra.lnkbco USING "<<<<<<<<<<",")"
   SKIP 1 LINES 
   PRINT COLUMN   1,"Empresa                 : ",xnomemp CLIPPED
   PRINT COLUMN   1,"Socio de Negocios       : ",xnomsoc CLIPPED
   PRINT COLUMN   1,"Nombre en Documento     : ",w_mae_tra.nomchq[1,50]
   PRINT COLUMN   1,"Banco                   : ",xnombco CLIPPED
   PRINT COLUMN   1,"Cuenta Bancaria         : ",w_mae_tra.numcta CLIPPED," ",xnomcta CLIPPED 
   PRINT COLUMN   1,"Numero de Documento     : ",w_mae_tra.numdoc CLIPPED,
         COLUMN  75,"Fecha de Emision  : ",w_mae_tra.fecemi
   SKIP 1 LINES 
   PRINT COLUMN   1,"Descripcion Documento   : ",w_mae_tra.descrp[1,75]
    IF LENGTH(w_mae_tra.descrp)>75 THEN
       PRINT COLUMN 27,w_mae_tra.descrp[76,150]  
    END IF 
    IF LENGTH(w_mae_tra.descrp)>150 THEN
       PRINT COLUMN 27,w_mae_tra.descrp[151,225]  
    END IF 
    IF LENGTH(w_mae_tra.descrp)>225 THEN
       PRINT COLUMN 27,w_mae_tra.descrp[226,255]  
    END IF 
    SKIP 1 LINES 
   PRINT COLUMN   1,"Valor Total del Pago Q. : ",
                    w_mae_tra.totpag USING "###,##&.&&",
         COLUMN  75,"Usuario Registro  : ",w_mae_tra.userid CLIPPED 
   PRINT COLUMN   1,"- Retencion Ant/ISR  Q. : ",
                    w_mae_tra.reisra USING "###,##&.&&",
         COLUMN  75,"Fecha de Registro : ",w_mae_tra.fecsis
   PRINT COLUMN   1,"- Retencion Hon/Serv Q. : ",
                    w_mae_tra.reishs USING "###,##&.&&",
         COLUMN  75,"Hora de Registro  : ",w_mae_tra.horsis  
   PRINT COLUMN   1,"- Otros Descuentos   Q. : ",
                    w_mae_tra.otrdes USING "###,##&.&&"
   PRINT COLUMN   1,"Total a Pagar        Q. : ",
                    w_mae_tra.totdoc USING "###,##&.&&" 
   PRINT COLUMN   1,"Total a Pagar en Letras : ",wcanletras CLIPPED 
 
   -- Verificando tipo de solicitud es con facturas 
   IF (w_mae_tra.tipsol=1) THEN 
    SKIP 1 LINES 
    PRINT COLUMN   1,"DETALLE DE PAGOS REALIZADOS" 
    PRINT COLUMN   1,"Numero Factura           Fecha Emision      ",
                     "Total Factura     Total Pago  Autorizo" 
    PRINT linea

    -- Imprimiendo detalle de pagos          
    LET tl = 30 
    FOR i = 1 TO totpag
     IF v_pagos[i].pfecven IS NULL OR
        v_pagos[i].ptotpag IS NULL OR 
        v_pagos[i].ptotpag = 0 THEN
        CONTINUE FOR
     END IF

     -- Obteniendo datos del autorizante 
     LET xnomaut = NULL 
     SELECT a.nomaut INTO xnomaut FROM glb_userauth a,cmp_mtransac y
      WHERE y.lnkcmp = v_pagos[i].plnkcmp 
        AND a.codaut = y.codaut 

     PRINT COLUMN   3,v_pagos[i].pnumdoc                       ,3  SPACES,
                      v_pagos[i].pfecemi                       ,9  SPACES, 
                      v_pagos[i].ptotdoc  USING "##,###,##&.&&",2  SPACES,
                      v_pagos[i].ptotpag  USING "##,###,##&.&&",2  SPACES,
                      xnomaut[1,20]  

     LET tl = tl-1
    END FOR 
    PRINT linea 
   ELSE 
    SKIP 1 LINES 

    -- Imprimiendo referencia de la caja si es tipo de solicitud caja 
    IF (w_mae_tra.tipsol=3) THEN 
     IF w_mae_caj.tipcaj=3 THEN 
      PRINT "Numero de Liquidacion   : ",
            w_mae_caj.numdoc USING "<<<<<<<<<<"," - ",w_mae_caj.fecemi 
     ELSE 
      PRINT "Numero de Caja          : ",
            w_mae_caj.numdoc USING "<<<<<<<<<<"," - ",w_mae_caj.fecemi 
     END IF 
    END IF 

    -- Obteniendo datos del autorizante 
    LET xnomaut = NULL 
    SELECT a.nomaut INTO xnomaut FROM glb_userauth a
     WHERE a.codaut = w_mae_tra.codaut 

    PRINT "Autorizado Por          : ",xnomaut CLIPPED 
   END IF 
   
   -- Imprimiendo partida contable
   SKIP 1 LINES 
   PRINT COLUMN   1,"DETALLE POLIZA CONTABLE" 
   PRINT COLUMN   1,"Cuenta Numero           Nombre de la Cuenta",
         " Contable                           Total DEBE    Total HABER" 
   PRINT linea
   FOR i = 1 TO totlin 
    IF v_partida[i].cnumcta IS NULL OR
       v_partida[i].cnomcta IS NULL OR 
       v_partida[i].ctotdeb IS NULL OR
       v_partida[i].ctothab IS NULL THEN 
       CONTINUE FOR
    END IF

    LET xnomcta = v_partida[i].cnomcta 
    PRINT COLUMN   3,v_partida[i].cnumcta                       ,2  SPACES,
                     xnomcta                                    ,2  SPACES, 
                     v_partida[i].ctotdeb  USING "##,###,##&.&&",2  SPACES,
                     v_partida[i].ctothab  USING "##,###,##&.&&"

    IF LENGTH(v_partida[i].cconcpt)>0 THEN
       PRINT COLUMN 26,v_partida[i].cconcpt[1,60] 
    END IF 

    LET tl = tl-1
   END FOR 
   PRINT linea  
   PRINT COLUMN   1,"Totales -->",   
         COLUMN  77,totaldeb              USING "##,###,##&.&&",2  SPACES,
                    totalhab              USING "##,###,##&.&&"

   SKIP 6 LINES 
   -- Imprimiendo realizado y recibido por
   -- Obteniendo datos del usuario que realizo
   LET xnomusr = NULL
   SELECT a.nomusr INTO xnomusr FROM glb_usuarios a
    WHERE a.userid = w_mae_tra.userid 

   PRINT COLUMN  1,"_______________________________", 
         COLUMN  50,"______________________________"
   PRINT COLUMN   1,"REALIZADO POR:",
         COLUMN  50,"FIRMA RECIBIDO POR:"           
   PRINT COLUMN   1,xnomusr CLIPPED 

   -- Si es reimpresion
   IF reimpresion THEN
      SKIP 1 LINES 
      PRINT "Re-impresion ",TODAY," ",TIME," ",username CLIPPED
   END IF 
END REPORT 

-- Subrutina para generar el cheque de socios de negocios 

FUNCTION bcorpt001_GenerarCheque(reimpresion)
 DEFINE reimpresion SMALLINT 

 -- Verificando si cuenta imprime voucher
 SELECT a.impvou FROM bco_mcuentas a
  WHERE a.numcta = w_mae_tra.numcta
    AND a.impvou = 1 
  IF (status=NOTFOUND) THEN
     RETURN
  END IF 

 -- Definiendo archivo de impresion
 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/ChequePagoSociosNegocios.spl"

 -- Asignando valores
 LET pipeline = "local"   

 -- Iniciando reporte
 START REPORT bcorpt001_ImprimirCheque TO filename 
  
  -- Llenando reporte
  OUTPUT TO REPORT bcorpt001_ImprimirCheque(w_mae_tra.*,reimpresion)

 -- Finalizando reporte 
 FINISH REPORT bcorpt001_ImprimirCheque

 -- Enviando reporte al destino seleccionado
 CALL librut001_sendreport(filename,pipeline,"Cheque","") 
END FUNCTION 

-- Subrutinar para imprimir el voucher cheque de socios de negocios 

REPORT bcorpt001_ImprimirCheque(imp1,reimpresion) 
 DEFINE imp1        RECORD LIKE bco_mtransac.*,
        exis,i,col  SMALLINT, 
        reimpresion SMALLINT,
        wcanletras  STRING

  OUTPUT LEFT   MARGIN  0 
         PAGE   LENGTH 11
         TOP    MARGIN  0 
         BOTTOM MARGIN  0 

 FORMAT 
  BEFORE GROUP OF imp1.lnkbco 
   -- Obteniendo cantidad en letras 
   LET wcanletras = librut001_numtolet(imp1.totdoc) 

   -- Imprimiendo datos del cheque 
   SKIP lineasup LINES 
   PRINT COLUMN colfec,lugar CLIPPED,", ",
                DAY(w_mae_tra.fecemi) USING "<<"," ",
                UPSHIFT(librut001_nombremeses(MONTH(w_mae_tra.fecemi),1)) CLIPPED,
                " DE ",YEAR(w_mae_tra.fecemi) USING "####",
         COLUMN coltot,w_mae_tra.totdoc USING "***,**&.&&" 
   SKIP 1 LINES
   PRINT COLUMN colnom,w_mae_tra.nomchq CLIPPED
   SKIP 1 LINES 
   PRINT COLUMN collet,wcanletras CLIPPED  
   SKIP 5 LINES 
END REPORT 
