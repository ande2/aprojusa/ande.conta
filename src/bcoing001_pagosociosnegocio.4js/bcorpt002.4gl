{
Programo : Mynor Ramirez        
Objetivo : Impresion de resumen de rubros de gasto de una caja chica o liquidacion
}

GLOBALS "bcoglb001.4gl" 
DEFINE existe        SMALLINT, 
       filename      STRING,   
       pipeline      STRING   

-- Subrutina para generar el resumen de rubros  

FUNCTION bcorpt002_GenerarResumenRubros()
 -- Definiendo archivo de impresion
 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/ResumenRubros.spl"

 -- Asignando valores
 LET pipeline = "pdf2"   

 -- Iniciando reporte
 START REPORT bcorpt002_ImprimirResumenRubros TO filename 
  
  -- Llenando reporte
  OUTPUT TO REPORT bcorpt002_ImprimirResumenRubros(w_mae_tra.*)

 -- Finalizando reporte 
 FINISH REPORT bcorpt002_ImprimirResumenRubros 

 -- Enviando reporte al destino seleccionado
 CALL librut001_sendreport
 (filename,pipeline,"Resumen Rubros", 
 "--noline-numbers "||
 "--nofooter "||
 "--font-size 8 "||
 "--page-width 595 --page-height 842 "||
 "--left-margin 35 --right-margin 15 "||
 "--top-margin 35 --bottom-margin 45 "||
 "--title Resumen-Rubros")
END FUNCTION 

-- Subrutinar para imprimir el resumen de rubros

REPORT bcorpt002_ImprimirResumenRubros(imp1) 
 DEFINE imp1        RECORD LIKE bco_mtransac.*,
        exis,i,col  SMALLINT, 
        reimpresion SMALLINT,
        xnomemp     CHAR(50), 
        xnombco     CHAR(50), 
        xnomcta     CHAR(50), 
        xnomsoc     CHAR(50), 
        xnomusr     CHAR(30), 
        xnomaut     CHAR(30), 
        linea       CHAR(104), 
        wcanletras  STRING,
        tl          INT 

  OUTPUT LEFT   MARGIN  0 
         PAGE   LENGTH 50 
         TOP    MARGIN  0 
         BOTTOM MARGIN  0 

 FORMAT 
  BEFORE GROUP OF imp1.lnkbco 
   -- Definiendo linea
   LET linea = "__________________________________________________", 
               "__________________________________________________",
               "____" 

   -- Obteniendo datos de la empresa
   INITIALIZE xnomemp TO NULL 
   SELECT a.nomemp INTO xnomemp FROM glb_empresas a
    WHERE a.codemp = w_mae_tra.codemp 

   -- Imprimiendo datos
   PRINT "CONSTANCIA DE EGRESOS NUMERO (",
         w_mae_tra.lnkbco USING "<<<<<<<<<<",")"
   SKIP 1 LINES 
   PRINT COLUMN   1,"Empresa                 : ",xnomemp CLIPPED
   PRINT COLUMN   1,"Socio de Negocios       : ",xnomsoc CLIPPED
   PRINT COLUMN   1,"Nombre en Documento     : ",w_mae_tra.nomchq CLIPPED
   PRINT COLUMN   1,"Numero de Documento     : ",w_mae_tra.numdoc CLIPPED, 
         COLUMN  75,"Fecha de Emision  : ",w_mae_tra.fecemi
   SKIP 1 LINES 
   PRINT COLUMN   1,"Descripcion Documento   : ",w_mae_tra.descrp[1,75]
    IF LENGTH(w_mae_tra.descrp)>75 THEN
       PRINT COLUMN 27,w_mae_tra.descrp[76,150]  
    END IF 
    IF LENGTH(w_mae_tra.descrp)>150 THEN
       PRINT COLUMN 27,w_mae_tra.descrp[151,225]  
    END IF 
    IF LENGTH(w_mae_tra.descrp)>225 THEN
       PRINT COLUMN 27,w_mae_tra.descrp[226,255]  
    END IF 
    SKIP 1 LINES 
   PRINT COLUMN   1,"Valor Total del Pago Q. : ",
                    w_mae_tra.totpag USING "###,##&.&&",
         COLUMN  75,"Usuario Registro  : ",w_mae_tra.userid CLIPPED 
   PRINT COLUMN   1,"- Retencion Ant/ISR  Q. : ",
                    w_mae_tra.reisra USING "###,##&.&&",
         COLUMN  75,"Fecha de Registro : ",w_mae_tra.fecsis
   PRINT COLUMN   1,"- Retencion Hon/Serv Q. : ",
                    w_mae_tra.reishs USING "###,##&.&&",
         COLUMN  75,"Hora de Registro  : ",w_mae_tra.horsis  
   PRINT COLUMN   1,"- Otros Descuentos   Q. : ",
                    w_mae_tra.otrdes USING "###,##&.&&"
   PRINT COLUMN   1,"Total a Pagar        Q. : ",
                    w_mae_tra.totdoc USING "###,##&.&&" 
   PRINT COLUMN   1,"Total a Pagar en Letras : ",wcanletras CLIPPED 
 
   -- Verificando tipo de solicitud es con facturas 
   IF (w_mae_tra.tipsol=1) THEN 
    SKIP 1 LINES 
    PRINT COLUMN   1,"DETALLE DE PAGOS REALIZADOS" 
    PRINT COLUMN   1,"Numero Factura           Fecha Emision      ",
                     "Total Factura     Total Pago  Autorizo" 
    PRINT linea

    -- Imprimiendo detalle de pagos          
    LET tl = 30 
    FOR i = 1 TO totpag
     IF v_pagos[i].pfecven IS NULL OR
        v_pagos[i].ptotpag IS NULL OR 
        v_pagos[i].ptotpag = 0 THEN
        CONTINUE FOR
     END IF

     -- Obteniendo datos del autorizante 
     LET xnomaut = NULL 
     SELECT a.nomaut INTO xnomaut FROM glb_userauth a,cmp_mtransac y
      WHERE y.lnkcmp = v_pagos[i].plnkcmp 
        AND a.codaut = y.codaut 

     PRINT COLUMN   3,v_pagos[i].pnumdoc                       ,3  SPACES,
                      v_pagos[i].pfecemi                       ,9  SPACES, 
                      v_pagos[i].ptotdoc  USING "##,###,##&.&&",2  SPACES,
                      v_pagos[i].ptotpag  USING "##,###,##&.&&",2  SPACES,
                      xnomaut[1,20]  

     LET tl = tl-1
    END FOR 
    PRINT linea 
   ELSE 
    SKIP 1 LINES 

    -- Imprimiendo referencia de la caja si es tipo de solicitud caja 
    IF (w_mae_tra.tipsol=3) THEN 
     IF w_mae_caj.tipcaj=3 THEN 
      PRINT "Numero de Liquidacion   : ",
            w_mae_caj.numdoc USING "<<<<<<<<<<"," - ",w_mae_caj.fecemi 
     ELSE 
      PRINT "Numero de Caja          : ",
            w_mae_caj.numdoc USING "<<<<<<<<<<"," - ",w_mae_caj.fecemi 
     END IF 
    END IF 

    -- Obteniendo datos del autorizante 
    LET xnomaut = NULL 
    SELECT a.nomaut INTO xnomaut FROM glb_userauth a
     WHERE a.codaut = w_mae_tra.codaut 

    PRINT "Autorizado Por          : ",xnomaut CLIPPED 
   END IF 
END REPORT 
