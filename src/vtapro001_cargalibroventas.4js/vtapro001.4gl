{
Carga de plantilla excel de libro de ventas   
Julio 2018 
}

IMPORT FGL fgl_excel
IMPORT util
IMPORT os 

DATABASE erpjuridico 

-- Definicion de variables globales 

CONSTANT DirectorioBase       = "c:\\\\"
CONSTANT ExtensionFiles       = "*.xlsx *.xls" 
CONSTANT progname             = "vtapro001"
CONSTANT TIPOTRNPOLIZAVENTAS  = 104 

-- Tipo de datos de los registros cargados de la plantilla 
TYPE DatosLibroVentas RECORD
   numest INTEGER,             
   idocto INTEGER,
   idtipo INTEGER,
   nomdoc VARCHAR(50),
   emisor INTEGER,
   nserie VARCHAR(50),
   nfolio VARCHAR(50),
   recpto VARCHAR(50),
   nomemi VARCHAR(50),
   nomrec VARCHAR(50),
   fecemi VARCHAR(50), 
   moneda VARCHAR(10),
   totnet DEC(10,2),
   totisv DEC(10,2),
   totdoc DEC(10,2),
   iddivi VARCHAR(50),
   nomdiv VARCHAR(50),
   automa INTEGER,
   numint VARCHAR(50),
   numtra VARCHAR(50),
   nompro VARCHAR(50),
   fecrep DATETIME YEAR TO MINUTE,
   estado VARCHAR(50),
   impres VARCHAR(50),
   descar VARCHAR(50),
   fecdoc DATE,
   rlleno CHAR(1) 
END RECORD 

-- Registro datos de la carga
DEFINE datos  RECORD
    codemp    LIKE ctb_mtransac.codemp 
END RECORD

-- Registros cargados de la plantilla
DEFINE v_registros DYNAMIC ARRAY OF DatosLibroVentas

-- Registro Detalle de cuentas poliza contable
DEFINE v_partida   DYNAMIC ARRAY OF RECORD
    cnumcta   LIKE ctb_dtransac.numcta,
    ctotdeb   LIKE ctb_dtransac.totdeb,
    ctothab   LIKE ctb_dtransac.tothab,
    cconcpt   LIKE ctb_dtransac.concep
END RECORD

DEFINE porcenisv      DECIMAL(5,2)
DEFINE username       VARCHAR(15) 
DEFINE ArchivoErrores STRING 
DEFINE ArchivoPcError STRING 
DEFINE nrows,ncols    INT  
DEFINE totcta         INT  

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarCargarLibroVentas")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("cargalibroventas")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo usuario
 LET username = FGL_GETENV("LOGNAME") 

 -- Menu de opciones 
 CALL vtapro001_Menu()
END MAIN 

-- Subutina para el menu de la carga 

FUNCTION vtapro001_Menu()
 DEFINE w       ui.Window,
        f       ui.Form,
        existe  SMALLINT, 
        wpais   STRING
 
 -- Abriendo la ventana para la carga 
 OPEN WINDOW wpro001a AT 5,2 WITH FORM "vtapro001a" 
  ATTRIBUTE(BORDER) 

  -- Desplegando datos del encabezado
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1)

  -- Inicializando datos
  CALL vtapro001_inival(1)

  -- Cargando combobox de empresas
  LET datos.codemp = librut003_DCbxEmpresaEnUsoXUsuario()
  IF datos.codemp IS NULL THEN
     CALL fgl_winmessage(
     "Atencion","Usuario sin empresa asignada, VERIFICA.","stop")
     CLOSE WINDOW wpro001a
     RETURN
  END IF
  DISPLAY BY NAME datos.codemp 

  -- Obteniendo porcentaje de impuesto sobre venta
  CALL librut003_parametros(8,1)
  RETURNING existe,porcenisv

  IF NOT existe THEN LET porcenisv = 0 END IF
  -- Menu de opciones
  MENU "Opciones" 
   BEFORE MENU
    -- Verificando accesos a opciones
    -- Seleccionar 
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "seleccionar"
    END IF

   ON ACTION seleccionar 
    CALL vtapro001_SeleccionarPlantilla()
   ON ACTION salir  
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU  
  END MENU
 CLOSE WINDOW wpro001a
END FUNCTION

-- Subrutina para seleccionar y cargar la plantilla en pantalla 

FUNCTION vtapro001_SeleccionarPlantilla()
 DEFINE rrow            fgl_excel.RowType 
 DEFINE rsheet          fgl_excel.sheetType
 DEFINE rworkbook       fgl_excel.workbookType 
 DEFINE celda           fgl_excel.cell
 DEFINE colchar         CHAR(50) 
 DEFINE textName        STRING
 DEFINE colname         STRING  
 DEFINE filename        STRING 
 DEFINE NombreArchivo   STRING 
 DEFINE ArchivoDestino  STRING 
 DEFINE ArchivoOrigen   STRING 
 DEFINE cellValue       STRING 
 DEFINE strtable        STRING
 DEFINE strregistros    STRING
 DEFINE idx,r,c,i,lg,j  INT 
 DEFINE numero          BIGINT 
 DEFINE deciml          DEC(12,2) 
 DEFINE ppg             VARCHAR(10)
 DEFINE pp              DEC(14,6)
 DEFINE tg,r4,pb,conteo INT 
 DEFINE carga,nr        INT 

 -- Cargando plantilla
 TRY 
  -- Seleccionando el archivo en excel 
  LET filename = winopenfile(DirectorioBase,
                            "Archivos",
                            ExtensionFiles,
                            "Seleccion del Plantilla de Excel a Cargar")

  IF (LENGTH(filename)>0) THEN 
    -- Abriendo archivo 
    LET ArchivoOrigen  = librut001_replace(filename,"\/","\\",40) 

    -- Creando nombre archivo de errores a copiar a pc cliente 
    LET ArchivoPcError = filename CLIPPED
    LET ArchivoPcError = librut001_replace(ArchivoPcError,"xlsx","csv",1)  
    LET ArchivoPcError = librut001_replace(ArchivoPcError,"xls","csv",1)  
    LET ArchivoPcError = librut001_replace(ArchivoPcError," ","",40)  

    -- Creando nombre archivo de errores a generar en server
    LET ArchivoErrores = os.Path.basename(filename) 
    LET ArchivoErrores = librut001_replace(ArchivoErrores,"xlsx","csv",1)  
    LET ArchivoErrores = librut001_replace(ArchivoErrores,"xls","csv",1)  
    LET ArchivoErrores = librut001_replace(ArchivoErrores," ","",40)  
    LET ArchivoErrores = FGL_GETENV("SPOOLDIR") CLIPPED,"/"||ArchivoErrores

    -- Creando nombre archivo a copiar en server
    LET NombreArchivo  = os.Path.basename(filename) 
    LET ArchivoDestino = os.Path.basename(filename) 
    LET ArchivoDestino = librut001_replace(ArchivoDestino," ","",40) 
    LET ArchivoDestino = FGL_GETENV("SPOOLDIR") CLIPPED,"/"||ArchivoDestino 

    -- Copiando el archivo del front end al back end 
    CALL librut001_getfile(ArchivoOrigen,ArchivoDestino)
  ELSE
    CALL fgl_winmessage(
    "Atencion","Carga de plantilla cancelada.","stop")
    RETURN 
  END IF 

  -- Abriendo libro de excel
  LET rworkbook = fgl_excel.workbook_open(ArchivoDestino.trim())
 
  -- Onteniendo hoja uno del libro de excel
  LET rsheet = rworkbook.getSheetAt(0) 

  -- Obteniendo numero de filas del libro de excel 
  LET nrows = rsheet.getPhysicalNumberOfRows()

  -- Asignando nombre de la ventana con numero de registros a cargar 
  LET textName = "Plantilla [ "||NombreArchivo||" ] Registros ["||(nrows-1)||"]"

  -- Obteniendo columnas de la primera fila 
  LET rrow  = rsheet.getRow(0)
  LET ncols = rrow.getLastCellNum() 

  -- Creando tabla temporal que contendra los datos de la carga
  CREATE TEMP TABLE tmp_excel 
  (
   numest VARCHAR(50),
   idocto INTEGER,
   idtipo INTEGER,
   nomdoc VARCHAR(50),
   emisor INTEGER,
   nserie VARCHAR(50),
   nfolio VARCHAR(50),
   recpto VARCHAR(50),
   nomemi VARCHAR(50),
   nomrec VARCHAR(50),
   fecemi VARCHAR(50), 
   moneda VARCHAR(10),
   totnet DEC(10,2),
   totisv DEC(10,2),
   totdoc DEC(10,2),
   iddivi VARCHAR(50),
   nomdiv VARCHAR(50),
   automa INTEGER,
   numint VARCHAR(50),
   numtra VARCHAR(50),
   nompro VARCHAR(50),
   fecrep DATETIME YEAR TO MINUTE,
   estado VARCHAR(50),
   impres VARCHAR(50),
   descar VARCHAR(50),
   fecdoc DATE
  )

  -- Cargando informacion del libro de excel y poblando tabla temporal
  -- Recorriendo filas 
  LET tg = 0 
  LET nr = 0 
  FOR r = 1 TO nrows 
   LET rrow = rsheet.getRow(r)
   IF rrow IS NULL THEN 
      CONTINUE FOR   
   END IF

   -- Desplegando progreso de la carga 
   LET tg = tg+1
   IF (tg=1) THEN 
    OPEN WINDOW wcarga AT 5,2 WITH FORM "progressb" 
     ATTRIBUTE(BORDER,TEXT=textName) 
   END IF 
   DISPLAY "Registro #"||tg||")" TO mensaje 

   -- Llenando progress bar 
   LET pp  = (tg*100)/(nrows-1) 
   LET r4  = pp
   LET ppg = r4
   LET ppg = ppg CLIPPED,"%"
   DISPLAY BY NAME r4,ppg
   CALL ui.Interface.refresh()

   -- Recorriendo columnas 
   LET strregistros = "INSERT INTO tmp_excel VALUES (" 
   LET ncols = rrow.getLastCellNum() 
   FOR c = 0 TO (ncols-1)
    LET Celda = rrow.getCell(c)
    LET cellValue = Celda 

    display c," ",cellValue 

    -- Verificando tipo de campo 
    LET carga = TRUE 
    CASE (c)
     WHEN  1 LET numero = cellValue LET cellValue = numero 
     WHEN  2 LET numero = cellValue LET cellValue = numero 
     WHEN  4 LET numero = cellValue LET cellValue = numero 
     WHEN  7 -- NIT
             IF cellValue MATCHES "*.*" THEN
                LET numero = cellValue
                LET cellValue = numero
             END IF 
     WHEN 12 LET deciml = cellValue LET cellValue = deciml 
     WHEN 13 LET deciml = cellValue LET cellValue = deciml 
     WHEN 14 LET deciml = cellValue LET cellValue = deciml 
     WHEN 17 LET numero = cellValue LET cellValue = numero 
     WHEN 21 LET cellValue = "" 
    END CASE 

    IF cellValue IS NULL THEN 
       LET cellValue = "" 
    END IF

    -- Creando registros 
    IF (c<(ncols-1)) THEN 
       LET strregistros = strregistros.trim(),"'",cellValue,"'," 
    ELSE
       LET strregistros = strregistros.trim(),"'",cellValue,"')" 
    END IF 
   END FOR    
   IF (ncols>0) THEN 
     -- Insertando registros 
     IF carga THEN
      PREPARE s2 FROM strregistros 
      EXECUTE s2
      FREE s2 

      -- Numero de registros insertados 
      LET nr = nr+1 
     END IF 
   END IF 
  END FOR    

  -- Cerrando ventana de progreso
  IF (tg>1) THEN
     CLOSE WINDOW wcarga 
  END IF 

  -- Desplegando y navegando la plantilla cargada de excel
  LET nrows = nr 
  LET textName = "Plantilla [ "||NombreArchivo||" ] Registros ["||nrows||"]"
  CALL vtapro001_NavegarPlantilla("tmp_excel",textName,nrows)

  -- Eliminando tabla temporal
  DROP TABLE tmp_excel 
 
 CATCH 
  CALL fgl_winmessage("Atencion",err_get(status),"stop") 
 END TRY 
END FUNCTION 

-- Desplegando y navengando la plantilla cargada en pantalla 

FUNCTION vtapro001_NavegarPlantilla(tabname,textName,totrows)
 DEFINE tabname  STRING
 DEFINE textName STRING
 DEFINE sql      STRING
 DEFINE nr       INT 
 DEFINE conteo   INT 
 DEFINE errores  INT 
 DEFINE totrows  INT 
 DEFINE totreg   INT 
 DEFINE haycarga BOOLEAN  

 -- Inicializando datos
 CALL vtapro001_inival(2)

 -- Llenando vector de datos 
 LET sql = "SELECT a.*,'' FROM "||tabName||" a"
 PREPARE p1 FROM sql
 DECLARE c1 CURSOR FOR p1
 LET totreg = 1
 FOREACH c1 INTO v_registros[totreg].*
  LET totreg = totreg+1 
 END FOREACH  
 CLOSE c1
 FREE  c1 
 LET totreg = totreg-1

 -- Desplegando registros
 CALL vtapro001_Registros(2,textName)  

 -- Desplegando datos 
 DIALOG ATTRIBUTES(UNBUFFERED,FIELD ORDER FORM)
 
  -- Desplegando datos 
  DISPLAY ARRAY v_registros TO s_registros.*
   ATTRIBUTE (COUNT=totreg,KEEP CURRENT ROW)
   BEFORE DISPLAY 
    CALL Dialog.SetActionHidden("close",1)
  END DISPLAY 

  ON ACTION cancel
   -- Salir sin cargar 
   EXIT DIALOG 

  ON ACTION cargar 
   -- Cargando informacion al libro de ventas 
   IF librut001_YesOrNot(
      "Confirmacion",
      "Desea cargar plantilla? \n"||"Registros a cargar ["||totrows||"]",
      "Si","No","question") THEN

      -- Cargando Datos 
      CALL vtapro001_CargarDatosPlantilla(tabName,totrows)
      RETURNING haycarga,nr,errores 
      IF errores=0 THEN 
       IF (nr>0) THEN 
        CALL fgl_winmessage(
        "Atencion","Plantilla cargada. ["||nr||"] registros.","information") 
        CONTINUE DIALOG 
       ELSE 
        IF haycarga THEN
         CALL fgl_winmessage(
         "Atencion","Plantilla con todos sus datos ya registrados.","stop")
         CONTINUE DIALOG 
        END IF 
       END IF
      ELSE 
       CALL fgl_winmessage(
       "Atencion",
       "Carga de plantilla con observaciones en los datos."||
       "\nRegistros cargados  ["||nr||"]"||
       "\nRegistros con error ["||errores||"]"||
       "\nVer archivo ["||ArchivoPcError||"]", 
       "stop")
       CONTINUE DIALOG 
      END IF 
   END IF 
 END DIALOG 

 -- Inicializando datos
 CALL vtapro001_inival(2)
END FUNCTION

-- Subrutina para cargar los datos de la plantilla al libro de ventas 
-- Crea registro en ventas y partida contable 

FUNCTION vtapro001_CargarDatosPlantilla(tabName,totrows)
 DEFINE nr              RECORD LIKE vta_ldocutec.* 
 DEFINE ct              RECORD LIKE ctb_mtransac.* 
 DEFINE xtipnom         LIKE glb_empresas.tipnom
 DEFINE xtotval         LIKE ctb_mtransac.totdoc
 DEFINE totdeb,tothab   LIKE ctb_mtransac.totdoc
 DEFINE xtotdoc         LIKE ctb_mtransac.totdoc 
 DEFINE wtotdoc         LIKE ctb_mtransac.totdoc 
 DEFINE xtotisv         LIKE ctb_mtransac.totdoc 
 DEFINE xtipcol         LIKE vta_ctasxtmv.tipcol 
 DEFINE xtipcal         LIKE vta_ctasxtmv.tipcal 
 DEFINE xnumcta         LIKE ctb_dtransac.numcta
 DEFINE ppg             VARCHAR(10)
 DEFINE xfecha          CHAR(10) 
 DEFINE pp              DEC(14,6)
 DEFINE sql             STRING 
 DEFINE tabName         STRING
 DEFINE xtipope         CHAR(1) 
 DEFINE n,conteo,correl INT 
 DEFINE errores,rg,tg   INT
 DEFINE r4,pb,totrows,i INT 

 -- Obteniendo tipo de nomenclatura de la empresa 
 INITIALIZE xtipnom TO NULL
 SELECT a.tipnom INTO xtipnom FROM glb_empresas a
  WHERE a.codemp = datos.codemp     

 -- Limpiando barra de progreso 
 CALL librut002_InicioProgreso()

  -- Seleccionando datos 
  LET n       = 0
  LET rg      = 0 
  LET tg      = 0 
  LET errores = 0 

  -- Creando archivo de errores
  START REPORT vtapro001_ArchivoconErrores TO ArchivoErrores 

  -- Llenando tabla (vta_ldocutec) 
  FOR tg = 1 TO totrows 
   -- Desplegando progreso de la carga 
   IF (tg=1) THEN 
    OPEN WINDOW wcarga AT 5,2 WITH FORM "progressb" 
     ATTRIBUTE(BORDER,TEXT="Progreso de Carga [ "||totrows||" Registros ]")
   END IF 
   DISPLAY "Registro #"||tg||
           " Documento Numero ( "||v_registros[tg].idocto||")"
   TO      mensaje 

   -- Llenando progress bar 
   LET pp  = (tg*100)/totrows 
   LET r4  = pp
   LET ppg = r4
   LET ppg = ppg CLIPPED,"%"
   DISPLAY BY NAME r4,ppg
   CALL ui.Interface.refresh()

   -- Chequeando que no exista ya el registro por medio de la llave (idocto) 
   SELECT COUNT(*)
    INTO  conteo 
    FROM  vta_ldocutec a 
    WHERE a.idocto = v_registros[tg].idocto 
    IF conteo>0 THEN
       -- Numero de registros con error 
       LET errores = errores+1 

       -- Agregando a archivo de errores csv 
       OUTPUT TO REPORT vtapro001_ArchivoConErrores(v_registros[tg].*, 
       "YA FUE CARGADO AL SISTEMA - CHEQUEAR COLUMNA (idocto)") 
       CONTINUE FOR 
    END IF 

   -- Obteniendo datos del tipo de documento de ventas
   SELECT a.tipdoc,a.tipope 
    INTO  nr.tipdoc,nr.tipope
    FROM  vta_tipodocs a 
    WHERE a.cargas = 1
      AND a.refcar = v_registros[tg].idtipo 
    IF (status=NOTFOUND) THEN
       -- Numero de registros con error 
       LET errores = errores+1 

       -- Agregando a archivo de errores csv 
       OUTPUT TO REPORT vtapro001_ArchivoConErrores(v_registros[tg].*,
       "NO EXISTE PARAMETRIZACION EN EL SISTEMA PARA EL TIPO ("||
       v_registros[tg].nomdoc CLIPPED||")") 
       CONTINUE FOR 
    END IF 

   -- Numero de registros procesados 
   LET n=n+1 

   -- Asignando datos 
   LET nr.lnkvta = 0
   LET nr.numcar = 1 
   LET nr.codemp = datos.codemp 
   LET nr.numest = v_registros[tg].numest 
   LET nr.idocto = v_registros[tg].idocto
   LET nr.idtipo = v_registros[tg].idtipo
   LET nr.emisor = v_registros[tg].emisor
   LET nr.nserie = v_registros[tg].nserie

   -- Obteniendo centro costo por medio de la serie del documento
   LET nr.codcos = 0
   SELECT NVL(a.codcos,0)
    INTO  nr.codcos
    FROM  vta_serfacel a
    WHERE a.nserie = nr.nserie 
      AND a.numest = nr.numest 

   LET nr.nfolio = v_registros[tg].nfolio
   LET nr.recpto = v_registros[tg].recpto CLIPPED 
   LET nr.nomdoc = v_registros[tg].nomdoc

   -- Verificando NIT recepctor 
   IF nr.recpto = "21965218" THEN 
      LET nr.nomrec = "BANCO DE DESARROLLO RURAL, S.A." 
   END IF 

   LET nr.nomemi = v_registros[tg].nomemi
   LET nr.moneda = v_registros[tg].moneda
   LET nr.fecemi = v_registros[tg].fecdoc
   ---LET xfecha    = nr.fecemi 
   --LET nr.fecemi = xfecha[1,10] 

   -- Verificando tipo de operacion del tipo de documento
   IF (nr.tipope=0) THEN 
      LET nr.totnet = v_registros[tg].totnet*(-1)
      LET nr.totisv = v_registros[tg].totisv*(-1)
      LET nr.totdoc = v_registros[tg].totdoc*(-1)
   ELSE 
      LET nr.totnet = v_registros[tg].totnet
      LET nr.totisv = v_registros[tg].totisv
      LET nr.totdoc = v_registros[tg].totdoc
   END IF 

   LET nr.iddivi = v_registros[tg].iddivi
   LET nr.nomdiv = v_registros[tg].nomdiv
   LET nr.automa = v_registros[tg].automa
   LET nr.numint = v_registros[tg].numint
   LET nr.numtra = v_registros[tg].numtra
   LET nr.nompro = v_registros[tg].nompro
   LET nr.fecrep = v_registros[tg].fecrep

   -- Verificando estado (0=Anulado 1=Vigente)
   LET nr.estado = 1 
   IF nr.totdoc=0 THEN
      LET nr.estado = 0
   END IF 

   LET nr.impres = v_registros[tg].impres
   LET nr.descar = v_registros[tg].descar
   LET nr.fecdoc = v_registros[tg].fecdoc
   LET nr.userid = username 
   LET nr.fecsis = CURRENT
   LET nr.horsis = CURRENT HOUR TO SECOND 
   LET nr.numesc = NULL
   LET nr.fecesc = NULL
   LET nr.lnkctb = 0

   -- Insertanto registro 
   TRY 
    -- 1.  Grabando en libro de ventas 
    INSERT INTO vta_ldocutec 
    VALUES (nr.*) 
    LET nr.lnkvta = SQLCA.SQLERRD[2] 

    -- 2.  Grabando poliza contable 
    -- 2.1 Encabezado de la poliza  

    -- Verificando que total del documento sea mayor que cero
    IF (nr.totdoc>0) THEN 

     -- Asignando datos
     LET ct.lnktra = 0
     LET ct.codemp = nr.codemp 
     LET ct.tiptrn = TIPOTRNPOLIZAVENTAS    
     LET ct.fecemi = nr.fecemi 
     LET ct.numdoc = librut003_NumeroPolizaAutomatica(ct.codemp,
                                                      ct.tiptrn,
                                                      ct.fecemi)
     LET ct.totdoc = nr.totdoc 

     -- Verificando tipo de operacion del tipo de documento
     IF (nr.tipope=0) THEN 
       LET ct.concep = "POR ANULACION DE FACTURA MES DE "||
                       UPSHIFT(librut001_nombremeses(MONTH(ct.fecemi),1)) CLIPPED||
                       " ",YEAR(ct.fecemi) USING "<<<<"
     ELSE
       LET ct.concep = "R/SERVICIOS PROFESIONALES PRESTADOS MES DE "||
                       UPSHIFT(librut001_nombremeses(MONTH(ct.fecemi),1)) CLIPPED||
                       " ",YEAR(ct.fecemi) USING "<<<<"
     END IF 

     LET ct.codapl = "VTA"
     LET ct.lnkapl = nr.lnkvta 
     LET ct.coddiv = 1 
     LET ct.estado = 1 
     LET ct.infadi = "DOCUMENTO # ("||nr.nfolio CLIPPED||
                     ") SERIE ("||nr.nserie CLIPPED,
                     ") DE FECHA ("||ct.fecemi||
                     ") ORIGEN (EXCEL) ID DOCUMENTO ("||nr.idocto||")" 
     LET ct.userid = username
     LET ct.fecsis = CURRENT 
     LET ct.horsis = CURRENT HOUR TO SECOND 

     INSERT INTO ctb_mtransac
     VALUES (ct.*) 
     LET ct.lnktra = SQLCA.SQLERRD[2] 

     -- 2.2 Detalle de la poliza  
     -- Creando vector de cuentas del detalle de la poliza  
     LET totcta = 0 

     -- Verificando operacion del tipo de documento 
     CASE (nr.tipope)
      WHEN 1 -- Cargo 
       LET totdeb = nr.totdoc 
       LET tothab = 0 
      WHEN 0 -- Abono
       LET totdeb = 0 
       LET tothab = nr.totdoc 
     END CASE 

     -- Cuentas contables por tipo de documento, centro de costo y empresa
     DECLARE cct CURSOR FOR
     SELECT a.numcta,a.tipcol,a.tipcal,a.numcor 
      FROM  vta_ctasxtmv a
      WHERE a.tipdoc = nr.tipdoc 
        AND a.codcos = nr.codcos 
        AND a.codemp = nr.codemp 
      ORDER BY 4

     LET wtotdoc = nr.totdoc 
     FOREACH cct INTO xnumcta,xtipcol,xtipcal 

      -- Verificando tipo de calculo
      CASE (xtipcal)
       WHEN 1 -- Total
        LET wtotdoc = nr.totdoc
       WHEN 2 -- Total-Impuesto 
        LET xtotdoc = nr.totdoc/((porcenisv/100)+1)
        LET wtotdoc = xtotdoc 
       WHEN 3 -- Impuesto
        LET xtotisv = nr.totdoc 
        LET xtotdoc = nr.totdoc/((porcenisv/100)+1)
        LET wtotdoc = xtotisv-xtotdoc 
      END CASE 

      -- Verificando tipo de columna de la cuenta
      CASE (xtipcol)
       WHEN "D" -- Debe
        LET totdeb = wtotdoc 
        LET tothab = 0 
       WHEN "H" -- Haber
        LET totdeb = 0 
        LET tothab = wtotdoc 
      END CASE 

      -- Cuenta contable complemento 
      CALL vtapro001_AgregaCuentaDetallePoliza(xnumcta,totdeb,tothab,"") 
     END FOREACH
     CLOSE cct
     FREE  cct 

     -- Creando detalle 
     LET correl = 0
     FOR i = 1 TO totcta 
      IF v_partida[i].cnumcta IS NULL THEN
         CONTINUE FOR
      END IF 
      LET correl = correl+1

      -- Verificando debe y haber
      IF (v_partida[i].ctotdeb>0) THEN
         LET xtipope = "D"
         LET xtotval = v_partida[i].ctotdeb 
      ELSE
         LET xtipope = "H"
         LET xtotval = v_partida[i].ctothab  
      END IF

      -- Grabando detalle de cuentas de poliza contable 
      INSERT INTO ctb_dtransac
      VALUES (ct.lnktra           , -- link del encabezado
              correl              , -- correlativo
              v_partida[i].cnumcta, -- numero de cuenta
              v_partida[i].cconcpt, -- concepto de la linea
              v_partida[i].ctotdeb, -- total debe
              v_partida[i].ctothab, -- total haber
              xtipope)

      -- Creando cuentas padre
      CALL librut003_CuentasPadre(
              ct.lnktra,
              correl,
              Ct.codemp,
              ct.fecemi,
              v_partida[i].cnumcta,
              xtipnom,                        
              xtipope,
              xtotval,
              v_partida[i].ctotdeb,
              v_partida[i].ctothab,
              "N",
              "N")
     END FOR 
    END IF 

    -- Numero de registros cargados correctamente 
    LET rg = rg+1

   CATCH 
    -- Numero de registros con error 
    LET errores = errores+1 

    -- Agregando a archivo de errores csv 
    OUTPUT TO REPORT vtapro001_ArchivoConErrores(v_registros[tg].*, 
    "ERROR EN EL FORMATO DE LA LINEA DE EXCEL") 
    CALL fgl_winmessage("Atencion",err_get(status),"stop") 
   END TRY 
  END FOR  

  IF (errores>0) THEN 
   -- Finalizando reporte
   FINISH REPORT vtapro001_ArchivoconErrores 

   -- Copiando el archivo del back end al fron end  
   CALL librut001_putfile(ArchivoErrores,ArchivoPcError)

   -- Cerrando ventana
   IF (tg>0) THEN 
      CLOSE WINDOW wcarga 
   END IF 

   RETURN FALSE,rg,errores 
  ELSE 
   -- Terminando reporte
   TERMINATE REPORT vtapro001_ArchivoconErrores 

   -- Cerrando ventana
   IF (tg>0) THEN 
      CLOSE WINDOW wcarga 
   END IF 

   RETURN TRUE,rg,0 
  END IF 
END FUNCTION

-- Subrutina para crear archivo csv con errores

REPORT vtapro001_ArchivoConErrores(imp,xobserv)
 DEFINE imp     DatosLibroVentas,
        xnumdoc STRING,
        xnumnit STRING,
        xobserv STRING

  OUTPUT LEFT   MARGIN  0
         PAGE   LENGTH  1
         TOP    MARGIN  0
         BOTTOM MARGIN  0

 FORMAT
  ON EVERY ROW                     
   -- Imprimiendo datos
   LET xnumdoc = "'",imp.nfolio CLIPPED
   LET xnumnit = "'",imp.recpto CLIPPED
   PRINT imp.numest                ,",", 
         imp.idocto                ,",",
         imp.idtipo                ,",",
         imp.nomdoc                ,",",
         imp.emisor                ,",",
         imp.nserie                ,",",
         xnumdoc                   ,",",
         xnumnit                   ,",",
         librut001_Replace(imp.nomemi,",","",50)   ,",",
         librut001_Replace(imp.nomrec,",","",50)   ,",",
         imp.fecemi                ,",",
         imp.moneda                ,",",
         imp.totnet                ,",",
         imp.totisv                ,",",
         imp.totdoc                ,",",
         imp.iddivi                ,",",
         imp.nomdiv                ,",",
         imp.automa                ,",",
         imp.numint                ,",",
         imp.numtra                ,",",
         imp.nompro                ,",",
         imp.fecrep                ,",",
         imp.estado                ,",",
         imp.impres                ,",",
         imp.descar                ,",",
         imp.fecdoc                ,",",
         xobserv.trim()    
END REPORT 

-- Subrutina para desplegar mensaje de registros

FUNCTION vtapro001_Registros(operacion,msg) 
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 1 CALL librut001_dpelement(
         "group1","Datos de la Carga")
  WHEN 2 CALL librut001_dpelement(
         "group1","Datos de la Carga "||msg CLIPPED)
 END CASE
END FUNCTION

-- Subrutina para agregar cuentas contables al vector de detalle de la poliza 

FUNCTION vtapro001_AgregaCuentaDetallePoliza(dp) 
 DEFINE dp        RECORD
         cnumcta  LIKE ctb_dtransac.numcta,
         ctotdeb  LIKE ctb_dtransac.totdeb,
         ctothab  LIKE ctb_dtransac.tothab,
         cconcpt  LIKE ctb_dtransac.concep
        END RECORD

 -- Asignando datos
 LET totcta                    = totcta+1 
 LET v_partida[totcta].cnumcta = dp.cnumcta
 LET v_partida[totcta].ctotdeb = dp.ctotdeb
 LET v_partida[totcta].ctothab = dp.ctothab
 LET v_partida[totcta].cconcpt = dp.cconcpt
END FUNCTION 

-- Subrutina para inicializar datos 

FUNCTION vtapro001_inival(i)
 DEFINE i SMALLINT 

 -- Inicializando datos  
 CASE (i)
  WHEN 1
   INITIALIZE datos.* TO NULL
 END CASE 

 -- Inicializando vectores
 LET totcta = 0 
 CALL v_registros.clear() 
 CALL v_partida.clear() 
 DISPLAY ARRAY v_registros TO s_registros.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 

 -- Desplegando registros
 CALL vtapro001_Registros(1,"")  
END FUNCTION
