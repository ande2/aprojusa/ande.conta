
DBSCHEMA Schema Utility       INFORMIX-SQL Version 12.10.FC6WE




create table "sistemas".vta_ldocutec 
  (
    lnkvta serial not null ,
    numcar integer not null ,
    codemp smallint not null ,
    codcos smallint not null ,
    tipdoc smallint not null ,
    numest smallint not null ,
    idocto integer not null ,
    idtipo integer not null ,
    nomdoc varchar(50) not null ,
    emisor integer,
    nserie varchar(50),
    nfolio varchar(50),
    recpto varchar(50),
    nomemi varchar(50),
    nomrec varchar(50),
    fecemi date,
    moneda varchar(10),
    totnet decimal(10,2),
    totisv decimal(10,2),
    totdoc decimal(10,2),
    iddivi varchar(50),
    nomdiv varchar(50),
    automa integer,
    numint varchar(50),
    numtra varchar(50),
    nompro varchar(50),
    fecrep datetime year to minute,
    estado smallint,
    impres varchar(50),
    descar varchar(50),
    fecdoc date,
    tipope smallint not null ,
    numesc char(20),
    fecesc date,
    lnkctb integer,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime year to second not null,
    primary key (lnkvta) constraint pkvtaldocutec
  );
