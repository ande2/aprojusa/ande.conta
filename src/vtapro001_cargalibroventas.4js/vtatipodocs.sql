create table "sistemas".vta_ctasxtmv
  (
    tipdoc smallint not null,
    codcos smallint not null ,
    numcta char(20) not null ,
    codemp smallint not null ,
    tipcol char(1) not null ,
    tipcal smallint not null,
    numcor smallint not null ,
    userid varchar(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null,
    foreign key(tipdoc) references vta_tipodocs constraint fkvtatipodocs1
  );

alter table "sistemas".vta_ctasxtmv add constraint (foreign key
    (numcta,codemp) references "sistemas".ctb_mcuentas  constraint
    "sistemas".fkctbmcuentas2);
