{ 
glbglo011.4gl
Mynor Ramirez
Mantenimiento de usuarios
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "glbmae011"
DEFINE w_mae_pro   RECORD LIKE glb_usuarios.*,
       v_usuarios  DYNAMIC ARRAY OF RECORD
        tuserid    LIKE glb_usuarios.userid,
        tnomusr    LIKE glb_usuarios.nomusr, 
        temail1    LIKE glb_usuarios.email1 
       END RECORD, 
       username    VARCHAR(15),
       v_costos    DYNAMIC ARRAY OF RECORD
         codcos    LIKE glb_cencosto.codcos,
         nomcen    LIKE glb_cencosto.nomcen,
         ch_habilitada  LIKE glb_usuarioch.ch_habilitada
       END RECORD,
       Opcion      CHAR(1)
END GLOBALS
