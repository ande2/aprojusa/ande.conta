{
glbqbe011.4gl 
Mynor Ramirez
Mantenimiento de usuarios 
}

{ Definicion de variables globales }

GLOBALS "glbglo011.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION glbqbe011_usuarios(operacion)

 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,
        msg                 STRING,
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL glbqbe011_EstadoMenu(operacion," ")
   INITIALIZE qrytext,qrypart TO NULL
   CALL glbmae011_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.userid,a.nomusr,a.email1,a.roleid,
                                a.usuaid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel 
     -- Salida
     CALL glbmae011_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.userid,a.nomusr,a.email1 ",
                 " FROM glb_usuarios a ",
                 " WHERE ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_usuarios SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_usuarios.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_usuarios INTO v_usuarios[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_usuarios
   FREE  c_usuarios
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL glbqbe011_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_usuarios TO s_usuarios.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL glbmae011_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscar
      CALL glbmae011_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF glbmae011_usuarios(2) THEN
         EXIT DISPLAY 
      ELSE
         -- Desplegando datos
         CALL glbqbe011_datos(v_usuarios[ARR_CURR()].tuserid)
      END IF 

     ON KEY (CONTROL-M)   
      -- Modificando 
      IF (operacion=2) THEN 
       IF glbmae011_usuarios(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL glbqbe011_datos(v_usuarios[ARR_CURR()].tuserid)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      LET res = 0 -- librut004_IntegridadUsuarios(w_mae_pro.userid) 
      IF (res>0) THEN
         LET msg = "Este usuario ya tiene registros. \nUsuario no puede borrarse."
         CALL fgl_winmessage(" Atencion",msg,"stop")
         CONTINUE DISPLAY 
      END IF 

      -- Confirmacion de la accion a ejecutar 
      LET msg = "Este SEGURO de Borrar este usuario ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             -- Eliminando
             CALL glbmae011_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Usuario"
      LET arrcols[2] = "Nombre"
      LET arrcols[3] = "Direccion de Correo"
      LET arrcols[4] = "Perfil"
      LET arrcols[5] = "Usuario Registro"
      LET arrcols[6] = "Fecha Registro"
      LET arrcols[7] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry = "SELECT a.userid,a.nomusr,a.email1,b.nmrole,a.usuaid,a.fecsis,a.horsis ",
                " FROM glb_usuarios a,glb_rolesusr b ",
                " WHERE b.roleid = a.roleid AND ",qrytext CLIPPED,
                " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res = librut002_excelreport("Usuarios",qry,10,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
         CALL DIALOG.setActionActive("modificar",FALSE)
         CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

       WHEN 2 -- Modificar
         CALL DIALOG.setActionActive("modificar",TRUE)
         CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
         CALL DIALOG.setActionActive("modificar",FALSE)
         CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
         CALL glbqbe011_datos(v_usuarios[ARR_CURR()].tuserid)
      END IF 

    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    "Atencion",
    "No existen usuarios con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Asignando estado del menu 
  CALL glbqbe011_EstadoMenu(0,"")
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION glbqbe011_datos(wuserid)
 DEFINE wuserid   LIKE glb_usuarios.userid,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  glb_usuarios a "||
              "WHERE a.userid = '"||wuserid||"'"||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_usuariost SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_usuariost INTO w_mae_pro.*
 END FOREACH
 CLOSE c_usuariost
 FREE  c_usuariost

 -- Desplegando datos 
  DISPLAY BY NAME w_mae_pro.nomusr THRU w_mae_pro.roleid
  DISPLAY w_mae_pro.passwd TO xpasswd  
  DISPLAY BY NAME w_mae_pro.userid,w_mae_pro.usuaid THRU w_mae_pro.horsis 
  CALL Init_Costos()
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION glbqbe011_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Usuarios - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Usuarios - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Usuarios - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Usuarios - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Usuarios - NUEVO")
 END CASE
END FUNCTION
