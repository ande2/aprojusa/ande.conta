{ 
glbglo020.4gl
Mynor Ramirez
Mantenimiento de programas
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "glbmae020"
DEFINE w_mae_pro   RECORD LIKE glb_programs.*,
       v_programas DYNAMIC ARRAY OF RECORD
        tordpro    LIKE glb_programs.ordpro, 
        tcodpro    LIKE glb_programs.codpro,
        tnompro    LIKE glb_programs.nompro 
       END RECORD,
       username    VARCHAR(15)
END GLOBALS
