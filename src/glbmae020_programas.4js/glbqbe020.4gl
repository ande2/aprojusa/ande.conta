{
glbqbe020.4gl 
Mynor Ramirez
Mantenimiento de programas 
}

{ Definicion de variables globales }

GLOBALS "glbglo020.4gl" 
DEFINE v_opciones     DYNAMIC ARRAY OF RECORD
        lnkpro        INT,
        nomopc        VARCHAR(50),
        passwd        SMALLINT 
       END RECORD,
       loop,i,totlin  INT,
       totopc         INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION glbqbe020_programas(operacion)

 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion,arr       SMALLINT,
        qry                 STRING,
        msg                 STRING,
        rotulo              CHAR(12),
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Verificando operacion
  -- Desplegando estado del menu
  CASE (operacion)
   WHEN 1 CALL librut001_dpelement("labelx","Lista de Programas - BUSCAR")
   WHEN 2 CALL librut001_dpelement("labelx","Lista de Programas - MODIFICAR")
   WHEN 3 CALL librut001_dpelement("labelx","Lista de Programas - BORRAR")
          LET rotulo = "Borrar"
  END CASE

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   INITIALIZE qrytext,qrypart TO NULL
   CALL glbmae020_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codpro,a.nompro,a.dcrpro,a.codmod,
                                a.ordpro,a.actpro,a.userid,a.fecsis,
                                a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel 
     -- Salida
     CALL glbmae020_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = "SELECT UNIQUE a.codpro,a.nompro,a.ordpro ",
                 " FROM glb_programs a ",
                 " WHERE ",qrytext CLIPPED,
                 " ORDER BY 3 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_programas SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_programas.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_programas INTO v_programas[totlin].tcodpro,
                            v_programas[totlin].tnompro, 
                            v_programas[totlin].tordpro  

    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_programas
   FREE  c_programas
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_programas TO s_programas.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL glbmae020_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscar
      CALL glbmae020_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF glbmae020_programas(2) THEN
         EXIT DISPLAY 
      ELSE
         -- Desplegando datos
         CALL glbqbe020_datos(v_programas[ARR_CURR()].tcodpro)
      END IF 

     ON KEY (CONTROL-M)   
      -- Modificando 
      IF (operacion=2) THEN 
       IF glbmae020_programas(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL glbqbe020_datos(v_programas[ARR_CURR()].tcodpro)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      LET res =  glbqbe020_integridad()
      IF (res>0) THEN
       LET msg = "Programa ya tiene movimientos registrados. \nPrograma no puede borrarse."
       CALL fgl_winmessage(" Atencion",msg,"stop")
       CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Este SEGURO de "||rotulo CLIPPED||" este programa ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL glbmae020_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION opciones 
      LET arr = ARR_CURR() 

      -- Ingresando opciones 
      CALL glbqbe020_opciones(v_programas[arr].tcodpro) 
      CALL glbqbe020_CargaOpciones(v_programas[arr].tcodpro) 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Programa"
      LET arrcols[2] = "Nombre"
      LET arrcols[3] = "Descripcion"
      LET arrcols[4] = "Orden"
      LET arrcols[5] = "Accion"
      LET arrcols[6] = "Usuario Registro"
      LET arrcols[7] = "Fecha Registro"
      LET arrcols[8] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry        = "SELECT a.* ",
                       " FROM glb_programs a ",
                       " WHERE ",qrytext CLIPPED,
                       " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res        = librut002_excelreport("Programas",qry,8,0,arrcols)

     BEFORE DISPLAY
      CALL FGL_SET_ARR_CURR(1)  

      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

     BEFORE ROW 
      LET arr = ARR_CURR() 

      -- Verificando control del total de lineas 
      IF (arr>totlin) THEN
         CALL FGL_SET_ARR_CURR(1)
         CALL glbqbe020_datos(v_programas[1].tcodpro)
      ELSE
         CALL glbqbe020_datos(v_programas[arr].tcodpro)
      END IF 

    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen programas con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Asignando estado del menu 
  CALL librut001_dpelement("labelx","Lista de Programas - MENU")
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION glbqbe020_datos(wcodpro)
 DEFINE wcodpro   LIKE glb_programs.codpro,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  glb_programs a "||
              "WHERE a.codpro = '"||wcodpro||"'"||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_programast SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_programast INTO w_mae_pro.*
 END FOREACH
 CLOSE c_programast
 FREE  c_programast

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nompro THRU w_mae_pro.actpro 
 DISPLAY BY NAME w_mae_pro.codpro,w_mae_pro.userid THRU w_mae_pro.horsis 

 -- Desplegando opciones 
 CALL glbqbe020_CargaOpciones(wcodpro) 
END FUNCTION 

-- Subrutina para ingresar las opciones del programa

FUNCTION glbqbe020_opciones(wcodpro)
 DEFINE wcodpro  LIKE glb_dprogram.codpro, 
        wcodopc  LIKE glb_dprogram.codopc,
        conteo   SMALLINT,
        lastkey  INT

 -- Cargando opciones
 CALL glbqbe020_CargaOpciones(wcodpro)

 -- Inicio del loop
 LET loop = TRUE 
 WHILE loop
  -- Ingresando 
   INPUT ARRAY v_opciones WITHOUT DEFAULTS FROM s_opciones.*
    ATTRIBUTE(COUNT=totopc,INSERT ROW=FALSE,ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED=TRUE,
              FIELD ORDER FORM)
  
   ON ACTION salida
    -- Salir
    LET loop = FALSE
    EXIT INPUT 

   ON ACTION grabar 
    -- Verificando si hay opciones ingresadas 
    IF (totopc<=0) THEN
       CALL fgl_winmessage(
       " Atencion:"," No existe ninguna opcion ingresada. \n VERIFICA.","stop")
       CONTINUE INPUT 
    END IF 

    -- Aceptar
    LET loop = TRUE 
    EXIT INPUT 

   ON ACTION DELETE
    -- Eliminando linea seleccionada
    CALL v_opciones.deleteElement(ARR_CURR())

   BEFORE INPUT
    CALL DIALOG.setActionHidden("append",TRUE)

   BEFORE ROW
    -- Total de filas 
    LET totopc = ARR_COUNT()

   AFTER FIELD nomopc
    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) AND 
       (v_opciones[ARR_CURR()].nomopc IS NULL) THEN
       NEXT FIELD nomopc 
    END IF

   BEFORE FIELD passwd 
    -- Verificando opcion 
    IF LENGTH(v_opciones[ARR_CURR()].nomopc)<=0 THEN
       ERROR "Error: nombre de la opcion invalida, VERIFICA."
       NEXT FIELD nomopc 
    END IF 

   AFTER FIELD passwd
    -- Verificando passwd
    IF v_opciones[ARR_CURR()].passwd IS NULL THEN
       LET v_opciones[ARR_CURR()].passwd = 0
       DISPLAY v_opciones[ARR_CURR()].passwd TO s_opciones[SCR_LINE()].passwd 
    END IF 

   AFTER ROW
    -- Total de filas 
    LET totopc = ARR_COUNT()

   AFTER INSERT,DELETE 
    -- Total de filas 
    LET totopc = ARR_COUNT()
  END INPUT 
  IF NOT loop THEN
     EXIT WHILE
  END IF 

  -- Grabando opciones
  ERROR "Grabando opciones ..." ATTRIBUTE(YELLOW) 

  -- Iniciando transaccion
  BEGIN WORK

   -- Insertando opciones 
   FOR i=1 TO totopc
    IF v_opciones[i].nomopc IS NULL OR
       v_opciones[i].passwd IS NULL THEN
       CONTINUE FOR
    END IF 

    -- Verificando si opcion ya existe
    SELECT COUNT(*)
     INTO  conteo
     FROM  glb_dprogram
     WHERE glb_dprogram.lnkpro = v_opciones[i].lnkpro
     IF (conteo>0) THEN
       -- Actualizando opcion
       UPDATE glb_dprogram
       SET    glb_dprogram.nomopc = v_opciones[i].nomopc, 
              glb_dprogram.passwd = v_opciones[i].passwd 
       WHERE  glb_dprogram.lnkpro = v_opciones[i].lnkpro 

     ELSE
        -- Creando codigo de la opcion 
        LET wcodopc = wcodpro CLIPPED,"-",i USING "<<"

        -- Grabando
        INSERT INTO glb_dprogram
        VALUES (0,                                -- id unico de programa
                wcodpro,                          -- codigo del programa
                i,                                -- id del programa
                wcodopc,                          -- codigo de la opcion
                v_opciones[i].nomopc,             -- nombre de la opcion
                i,                                -- orden de la opcion
                v_opciones[i].passwd,             -- opcion requiere password
                USER,                             -- usuario registro
                CURRENT,                          -- fecha registro
                CURRENT HOUR TO SECOND)           -- hora registro 
     END IF 
   END FOR 

  -- Finaliando transaccion
  COMMIT WORK 
  ERROR "" 
  LET loop = FALSE 
 END WHILE
END FUNCTION 

-- Subrutina para cargar las opciones del programa

FUNCTION glbqbe020_CargaOpciones(wcodpro)
 DEFINE wcodpro LIKE glb_dprogram.codpro

 -- Desplegando titulo
 CALL librut001_dpelement("nombreopcion","Opciones ["||w_mae_pro.nompro CLIPPED||"]") 

 -- Inicializando
 CALL v_opciones.clear() 

 -- Cargando opciones
 DECLARE copc CURSOR FOR
 SELECT a.lnkpro,
        UPPER(a.nomopc),
        a.passwd 
  FROM  glb_dprogram a
  WHERE a.codpro = wcodpro 
    AND a.progid >0 
  ORDER BY a.progid 

  LET totopc=1 
  FOREACH copc INTO v_opciones[totopc].*
   LET totopc = totopc+1 
  END FOREACH
  CLOSE copc
  FREE  copc
  LET totopc = totopc-1 

 -- Desplegando opciones
 DISPLAY ARRAY v_opciones TO s_opciones.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION 

-- Subrutina para verificar si el programa ya tiene movimientos  

FUNCTION glbqbe020_integridad()
 DEFINE conteo SMALLINT

 -- Verificando movimientos 
 -- Pefiles de usuario
 SELECT COUNT(*)
  INTO  conteo
  FROM  glb_permxrol a
  WHERE (a.codpro = w_mae_pro.codpro) 
  IF (conteo=0) THEN 
     -- Permisos x usuario
     SELECT COUNT(*)
      INTO  conteo
      FROM  glb_permxusr a
      WHERE (a.codpro = w_mae_pro.codpro) 
      IF (conteo=0) THEN
         RETURN 0
      ELSE
         RETURN 1 
      END IF 
  ELSE
     RETURN 1 
  END IF
END FUNCTION 
