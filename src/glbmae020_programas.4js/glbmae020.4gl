{
glbmae020.4gl 
Mynor Ramirez
Mantenimiento de programas 
}

-- Definicion de variables globales 

GLOBALS "glbglo020.4gl"

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar17")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("programas")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Obteniendo nombre del programa 
 LET username = FGL_GETENV("LOGNAME")
 LET username = "sistemas"

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL glbmae020_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION glbmae020_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255),
        savedata SMALLINT, 
        existe   SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "glbmae020a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header("glbmae020",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox de modulos
  CALL librut003_CbxModulos() 

  -- Menu de opciones
  MENU " Programas"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,4,username) THEN
       HIDE OPTION "Buscar"
    END IF
    --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Modificar"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de programas."
    CALL glbqbe020_programas(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo programa."
    LET savedata = glbmae020_programas(1) 
   COMMAND "Modificar"
    " Modificacion de un programa existente."
    CALL glbqbe020_programas(2) 
   COMMAND "Borrar"
    " Eliminacion de un programa existente."
    CALL glbqbe020_programas(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION glbmae020_programas(operacion)
 DEFINE w_mae_usr         RECORD LIKE glb_programs.*,
        loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL librut001_dpelement("labelx","Lista de Programas - NUEVO") 
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL glbmae020_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.codpro,
                w_mae_pro.nompro,
                w_mae_pro.dcrpro,
                w_mae_pro.codmod,
                w_mae_pro.ordpro,
                w_mae_pro.actpro 
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   BEFORE FIELD codpro 
    -- Verificando si no es ingreso
    IF (operacion=2) THEN
       NEXT FIELD nompro
    END IF 

   AFTER FIELD codpro
    --Verificando programa
    IF (LENGTH(w_mae_pro.codpro)=0) THEN
       ERROR "Error: codigo del programa invalido, VERIFICA."
       LET w_mae_pro.codpro = NULL
       CLEAR codpro
       NEXT FIELD codpro
    END IF

    -- Verificando si el programa existe registrado
    INITIALIZE w_mae_usr.* TO NULL
    CALL librut003_bprograma(w_mae_pro.codpro)
    RETURNING w_mae_usr.*,existe
    IF existe THEN
       ERROR "Error: programa ya existe registrado, VERIFICA."
       NEXT FIELD codpro
    END IF 

   AFTER FIELD nompro  
    --Verificando nombre del programa
    IF (LENGTH(w_mae_pro.nompro)=0) THEN
       ERROR "Error: nombre del programa invalido, VERIFICA."
       LET w_mae_pro.nompro = NULL
       NEXT FIELD nompro  
    END IF

    -- Verificando que no exista otro programa con el mismo nombre
    SELECT UNIQUE (a.codpro)
     FROM  glb_programs a
     WHERE (a.codpro != w_mae_pro.codpro) 
       AND (a.nompro  = w_mae_pro.nompro) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otro programa con el mismo nombre, VERIFICA.",
        "information")
        NEXT FIELD nompro
     END IF 

   AFTER FIELD dcrpro  
    --Verificando descripcion del programa
    IF (LENGTH(w_mae_pro.dcrpro)=0) THEN
       ERROR "Error: descripcion del programa invalido, VERIFICA."
       LET w_mae_pro.dcrpro = NULL
       NEXT FIELD dcrpro  
    END IF

   AFTER FIELD codmod  
    --Verificando modulo 
    IF w_mae_pro.codmod IS NULL THEN 
       ERROR "Error: modulo del programa invalido, VERIFICA."
       LET w_mae_pro.codmod = NULL
       NEXT FIELD codmod  
    END IF

   AFTER FIELD ordpro  
    --Verificando orden del programa
    IF w_mae_pro.ordpro IS NULL OR
       w_mae_pro.ordpro <0 THEN
       ERROR "Error: orden del programa invalido, VERIFICA."
       LET w_mae_pro.ordpro = NULL
       NEXT FIELD ordpro  
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL glbmae020_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando programa
    CALL glbmae020_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL librut001_dpelement("labelx","Lista de Programas - MENU") 
    CALL glbmae020_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un programa

FUNCTION glbmae020_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando programa ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO glb_programs   
   VALUES (w_mae_pro.*)

   --Asignando el mensaje 
   LET msg = "Programa (",w_mae_pro.codpro CLIPPED,") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE glb_programs
   SET    glb_programs.*      = w_mae_pro.*
   WHERE  glb_programs.codpro = w_mae_pro.codpro 

   --Asignando el mensaje 
   LET msg = "Programa (",w_mae_pro.codpro CLIPPED,") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando programas
   DELETE FROM glb_programs 
   WHERE (glb_programs.codpro = w_mae_pro.codpro)

   --Asignando el mensaje 
   LET msg = "Programa (",w_mae_pro.codpro CLIPPED,") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL glbmae020_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION glbmae020_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.userid = username 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codpro,w_mae_pro.nompro THRU w_mae_pro.actpro
 DISPLAY BY NAME w_mae_pro.codpro,w_mae_pro.userid THRU w_mae_pro.horsis
END FUNCTION
