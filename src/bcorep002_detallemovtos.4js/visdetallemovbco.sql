
drop view vis_detallemovbco;

create view "sistemas".vis_detallemovbco
 (lnkbco,codemp,nomemp,numcta,nomcta,nombco,feccob,tipmov,nommov,
  numdoc,nomchq,descrp,totdoc,codrub,nomrub,tipope,estado,nofase,
  userid,fecsis,horsis) as

  select x0.lnkbco,x0.codemp,x4.nomemp,x0.numcta,x1.nomcta,
         x2.nombco,x0.feccob,x0.tipmov,x3.nommov,x0.numdoc,
         x0.nomchq,x0.descrp,x0.totdoc,x0.codrub,x5.nomrub,
         x0.tipope,x0.estado,x0.nofase,x0.userid,x0.fecsis,
         x0.horsis
  from   bco_mtransac x0,bco_mcuentas x1,glb_mtbancos x2,
         bco_tipomovs x3,glb_empresas x4,outer fac_rubgasto x5
  where x4.codemp = x0.codemp
    AND x1.numcta = x0.numcta
    AND x1.codbco = x2.codbco
    AND x3.tipmov = x0.tipmov
    AND x5.codrub = x0.codrub;

grant select on vis_detallemovbco to public;
