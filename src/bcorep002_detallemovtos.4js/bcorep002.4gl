{ 
Programa : bcorep002.4gl                   
Programo : Mynor Ramirez 
Objetivo : Reporte de detalle de movimientos bancarios 
}

DATABASE erpjuridico 

-- Definicion de variables globales 

CONSTANT progname = "bcorep002" 
TYPE    datosreporte        RECORD 
         lnkbco             LIKE bco_mtransac.lnkbco,
         codemp             LIKE bco_mtransac.codemp, 
         nomemp             CHAR(50), 
         numcta             CHAR(15), 
         nomcta             CHAR(20),                         
         nombco             CHAR(30),
         feccob             LIKE bco_mtransac.feccob,
         tipmov             LIKE bco_mtransac.tipmov,
         nommov             CHAR(40), 
         numdoc             CHAR(15),                       
         nomchq             CHAR(30), 
         descrp             CHAR(300),
         totdoc             LIKE bco_mtransac.totdoc,
         codrub             LIKE bco_mtransac.codrub, 
         nomrub             CHAR(20),  
         tipope             LIKE bco_mtransac.tipope,
         estado             SMALLINT, 
         nofase             SMALLINT, 
         userid             CHAR(10), 
         fecsis             LIKE bco_mtransac.fecsis, 
         horsis             LIKE bco_mtransac.horsis
        END RECORD 
DEFINE  datos               RECORD 
         codemp             LIKE bco_mtransac.codemp, 
         numcta             LIKE bco_mtransac.numcta,
         tipmov             LIKE bco_mtransac.tipmov,
         fecini             DATE,
         fecfin             DATE,
         tipope             LIKE bco_mtransac.tipope 
        END RECORD
DEFINE  p                   RECORD
         length             SMALLINT,
         topmg              SMALLINT,
         botmg              SMALLINT,
         lefmg              SMALLINT,
         rigmg              SMALLINT
        END RECORD
DEFINE  gcodemp             LIKE bco_mtransac.codemp 
DEFINE  gnumcta             LIKE bco_mtransac.numcta 
DEFINE  existe              SMALLINT
DEFINE  tituloreporte       STRING 
DEFINE  filename            STRING
DEFINE  pipeline            STRING
DEFINE  s,d,nc,apostrofe    CHAR(1) 
DEFINE  np                  CHAR(3) 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarReportes1")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rdetallemovbco")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL bcorep002_MovimientosBancos()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION bcorep002_MovimientosBancos()
 DEFINE imp1      datosreporte,
        wlnkcmp   INTEGER, 
        wpais     VARCHAR(255),
        qrytxt    STRING,
        qrypart   STRING,
        loop      SMALLINT,
        w         ui.Window, 
        f         ui.Form   

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep002a AT 5,2
  WITH FORM "bcorep002a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/MovimientosBanco.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox de empresas
  LET gcodemp = librut003_DCbxEmpresaEnUsoXUsuario()
  IF gcodemp IS NULL THEN
     CALL fgl_winmessage(
     "Atencion","Usuario sin empresa asignada, VERIFICA.","stop")
     CLOSE WINDOW wrep002a 
     RETURN
  END IF

  -- Cargando cuentas
  LET gnumcta = librut003_DCbxCuentasBanco(gcodemp)
  IF gnumcta IS NULL THEN
     CALL fgl_winmessage(
     "Atencion","Empresa sin cuentas de banco asignadas, VERIFICA.","stop")
     CLOSE WINDOW wrep002a 
     RETURN
  END IF

  -- Llenando combobox de tipos de movimiento
  CALL librut003_CbxTiposMovimientoBco(1)
  -- Llenando combobox de coneptos de movimiento 
  CALL librut003_CbxRubrosGasto()

  -- Inicio del loop
  OPTIONS INPUT WRAP 
  LET apostrofe = "'" 
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE pipeline TO NULL
   LET datos.codemp = gcodemp 
   LET s = 1 SPACES 
   CLEAR FORM
   DISPLAY BY NAME datos.codemp
   
   -- Ingresando datos 
   DIALOG ATTRIBUTES(UNBUFFERED,FIELD ORDER FORM)

     -- Construyendo busqueda
     INPUT BY NAME datos.tipmov,
                  datos.numcta,
                  datos.tipope,
                  datos.fecini,
                  datos.fecfin
     END INPUT 

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT DIALOG

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 
     LET p.length = 57  
     LET p.topmg  = 3
     LET p.botmg  = 2
     LET p.lefmg  = 2
     LET p.rigmg  = 2

     -- Verificando ingreso de filtros
     IF NOT bcorep002_FiltrosCompletos() THEN 
        NEXT FIELD tipmov 
     END IF 
     EXIT DIALOG 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 
     LET p.length = 57  
     LET p.topmg  = 0
     LET p.botmg  = 0
     LET p.lefmg  = 0
     LET p.rigmg  = 0

     -- Verificando ingreso de filtros
     IF NOT bcorep002_FiltrosCompletos() THEN 
        NEXT FIELD tipmov 
     END IF 
     EXIT DIALOG 

    ON ACTION excel 
     -- Asignando dispositivo 
     LET pipeline = "excel" 
     LET s = ASCII(9) 
     LET p.length = 57  
     LET p.topmg  = 0
     LET p.botmg  = 0
     LET p.lefmg  = 0
     LET p.rigmg  = 0

     -- Verificando ingreso de filtros
     IF NOT bcorep002_FiltrosCompletos() THEN 
        NEXT FIELD tipmov 
     END IF 
     EXIT DIALOG

   END DIALOG 
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Construyendo seleccion 
   LET qrytxt = "SELECT a.* ",
                 "FROM  vis_detallemovbco a ",
                 "WHERE a.codemp = ",datos.codemp,
                 "  AND a.numcta ='"||datos.numcta||"'",
                 "  AND a.tipmov = "||datos.tipmov,
                 "  AND a.feccob >= '",datos.fecini,"'", 
                 "  AND a.feccob <= '",datos.fecfin,"'", 
                 "  AND a.tipope = "||datos.tipope,
                 " ORDER BY a.codemp,a.tipmov,a.feccob,a.numcta" 

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep002 FROM qrytxt 
   DECLARE c_crep002 CURSOR FOR c_rep002
   LET existe = FALSE
   FOREACH c_crep002 INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       LET existe = TRUE
       START REPORT bcorep002_ImprimirMovimientoBco TO filename 
    END IF 

    -- Verificando si documento esta anulado 
    IF (imp1.estado=0) THEN 
       LET imp1.totdoc = 0 
    END IF 

    -- Verificando si tipo de movimiento es abono
    IF imp1.tipope=0 THEN
       -- Verificando si concepto esta en blanco 
       IF (imp1.codrub IS NULL) THEN
          LET imp1.nomrub = NULL 
          SELECT NVL(MIN(a.lnkcmp),0) 
           INTO  wlnkcmp 
           FROM  bco_dtransac a
           WHERE a.lnkbco = imp1.lnkbco
           IF (wlnkcmp>0) THEN
              SELECT y.nomrub
               INTO  imp1.nomrub
               FROM  cmp_mtransac x,fac_rubgasto y
               WHERE x.lnkcmp = wlnkcmp
                 AND y.codrub = x.codrub 
           END IF 
       END IF 
    END IF 

    -- Llenando el reporte
    OUTPUT TO REPORT bcorep002_ImprimirMovimientoBco(imp1.*)
   END FOREACH
   CLOSE c_crep002 
   FREE  c_crep002 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT bcorep002_ImprimirMovimientoBco 

      -- Transfiriendo reporte a excel
      IF pipeline = "excel" THEN
         CALL bcorep002_excel(filename)
      ELSE
       -- Enviando reporte al destino seleccionado
       CALL librut001_sendreport
       (filename,pipeline,tituloreporte, 
        "--noline-numbers "||
        "--nofooter "||
        "--font-size 7 "||
        "--page-width 842 --page-height 595  "||
        "--left-margin 35 --right-margin 15 "||
        "--top-margin 30 --bottom-margin 35 "||
        "--title Bancos")
      END IF

      ERROR "" 
      CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(
      " Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 
  END WHILE

  OPTIONS INPUT NO WRAP 

 CLOSE WINDOW wrep002a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION bcorep002_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE

 -- Verificando datos 
 IF datos.fecini IS NULL OR 
    datos.fecfin IS NULL THEN 
    CALL fgl_winmessage(
    " Atencion",
    " Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte 

REPORT bcorep002_ImprimirMovimientoBco(imp1)
 DEFINE imp1              datosreporte,
        linea             CHAR(170),
        col,i,nmovs,lg    INTEGER,
        xnommov           CHAR(40),
        xnomrub           CHAR(40), 
        espacios          CHAR(4), 
        marca             CHAR(1), 
        fechareporte      STRING

  OUTPUT LEFT MARGIN   p.lefmg
         TOP MARGIN    p.topmg
         BOTTOM MARGIN p.botmg
         PAGE LENGTH   p.length

  FORMAT 
   PAGE HEADER

    -- Llenando linea
    LET lg = 170 
    LET linea = NULL
    FOR i = 1 TO lg
     LET linea = linea CLIPPED,"-"
    END FOR  

    -- Imprimiendo Encabezado
    LET col = librut001_centrado(tituloreporte,lg) 
    PRINT COLUMN   1,"Bancos",
          COLUMN col,UPSHIFT(tituloreporte) CLIPPED,  
	  COLUMN (lg-20),PAGENO USING "Pagina: <<<<"

    LET fechareporte = "PERIODO DE FECHAS ",
                       "DEL [ ",datos.fecini," ] AL [ ",datos.fecfin," ]" 
    LET col = librut001_centrado(fechareporte,lg) 
    PRINT COLUMN   1,"Bcorep002",
          COLUMN col,fechareporte CLIPPED,  
          COLUMN (lg-20),"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    LET col = librut001_centrado(imp1.nomemp,lg) 
    PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
          COLUMN col,imp1.nomemp CLIPPED,
          COLUMN (lg-20),"Hora  : ",TIME 

    PRINT linea CLIPPED 
    PRINT "Fecha       Numero Cuenta    # Documento     Beneficiario                    ",
          "Tipo de Concepto              Total  Descripcion del Movimiento      ",
          "Usuario    Fecha "        
    PRINT "Emision     Bancaria         Bancario                                        ",
          "                         Movimiento                                  ",
          "Registro   Registro"
    PRINT linea

  BEFORE GROUP OF imp1.codemp 
   -- Verificando si reporte es a excel 
   IF (pipeline="excel") THEN 
      -- Imprimiendo datos de la cuenta 
      PRINT tituloreporte CLIPPED,s 
      PRINT imp1.nomemp CLIPPED,s
      PRINT "Periodo Del ",datos.fecini," Al ",datos.fecfin,s
      PRINT linea
      PRINT s 
      PRINT "Fecha Emision",s,
            "Numero Cuenta",s, 
            "Numero Documento",s,  
            "Beneficiario",s,
            "Tipo Concepto",s,
            "Total Movimiento",s,
            "Descripcion del Movimiento",s,
            "Fecha Registro",s,
            "Usuario Registro",s 
      PRINT s 
   END IF 

  BEFORE GROUP OF imp1.tipmov 
   PRINT "Tipo de Movimiento :",s,imp1.nommov CLIPPED 
   LET nmovs = 0 

  ON EVERY ROW 
   -- Imprimiendo movimientos 
   LET espacios = s,s,s,s 
   LET nmovs = (nmovs+1)

   -- Marcando de anulado el cheque
   LET marca = "" 
   IF imp1.estado=0 THEN
      LET marca = "A"
   END IF 

   -- Verificando si reportes es a excel 
   IF (pipeline="excel") THEN 
    PRINT imp1.feccob                                     ,s,apostrofe, 
          imp1.numcta                                     ,s,apostrofe,
          imp1.numdoc                                     ,s,
          imp1.nomchq                                     ,s,
          imp1.nomrub                                     ,s, 
          imp1.totdoc               USING "###,###,##&.&&",s,apostrofe, 
          imp1.descrp               CLIPPED               ,s,
          imp1.fecsis                                     ,s,
          imp1.userid                                     ,s,
          marca                                           ,s 
   ELSE
    PRINT imp1.feccob                                     ,s,1 SPACES,
          imp1.numcta                                     ,s,1 SPACES,
          imp1.numdoc                                     ,s,
          imp1.nomchq                                     ,s,1 SPACES,
          imp1.nomrub                                     ,s,
          imp1.totdoc               USING "###,###,##&.&&",s,1 SPACES,
          imp1.descrp[1,30]                               ,s,1 SPACES, 
          imp1.fecsis                                     ,s,
          imp1.userid                                     ,s,
          marca                                           ,s 

    -- Verificando si tiene descripcion
    IF (LENGTH(imp1.descrp)>30) THEN
       PRINT COLUMN 115,espacios CLIPPED,imp1.descrp[31,60] 
    END IF
    -- Verifica si tiene descripcion
    IF (LENGTH(imp1.descrp)>60) THEN
       PRINT COLUMN 115,espacios CLIPPED,imp1.descrp[61,90] 
    END IF
    -- Verifica si tiene descripcion
    IF (LENGTH(imp1.descrp)>90) THEN
       PRINT COLUMN 115,espacios CLIPPED,imp1.descrp[91,120] 
    END IF
    -- Verifica si tiene descripcion
    IF (LENGTH(imp1.descrp)>120) THEN
       PRINT COLUMN 115,espacios CLIPPED,imp1.descrp[121,150] 
    END IF
    -- Verifica si tiene descripcion
    IF (LENGTH(imp1.descrp)>150) THEN
       PRINT COLUMN 115,espacios CLIPPED,imp1.descrp[151,180] 
    END IF
    -- Verifica si tiene descripcion
    IF (LENGTH(imp1.descrp)>180) THEN
       PRINT COLUMN 115,espacios CLIPPED,imp1.descrp[181,210] 
    END IF
    -- Verifica si tiene descripcion
    IF (LENGTH(imp1.descrp)>210) THEN
       PRINT COLUMN 115,espacios CLIPPED,imp1.descrp[211,240] 
    END IF
    -- Verifica si tiene descripcion
    IF (LENGTH(imp1.descrp)>240) THEN
       PRINT COLUMN 115,espacios CLIPPED,imp1.descrp[241,270] 
    END IF
    -- Verifica si tiene descripcion
    IF (LENGTH(imp1.descrp)>271) THEN
       PRINT COLUMN 115,espacios CLIPPED,imp1.descrp[271,300] 
    END IF
   END IF 

  AFTER GROUP OF imp1.tipmov 
   -- Totalizando
   PRINT s
   PRINT COLUMN   1,nmovs USING "<<<<<<"," Movimiento[s] ",s,s,s,s, 
         COLUMN  81,"Totales -->",s, 
         COLUMN  99,GROUP SUM(imp1.totdoc) USING "###,###,##&.&&",s
   PRINT s

  ON LAST ROW
   -- Imprimiendo filtros
   PRINT "** Filtros **",s
   IF datos.tipmov IS NOT NULL THEN
      SELECT a.nommov INTO xnommov FROM bco_tipomovs a WHERE a.tipmov = datos.tipmov 
      PRINT "Tipo de Movimiento :",s,xnommov,s
   END IF
   IF datos.numcta IS NOT NULL THEN
      PRINT "Numero de Cuenta   :",s,datos.numcta,s
   END IF
   IF datos.tipope IS NOT NULL THEN
      CASE (datos.tipope)
       WHEN 1 PRINT "Tipo de Operacion  :",s,"Cargos",s
       WHEN 0 PRINT "Tipo de Operacion  :",s,"Abonos",s
      END CASE
   END IF
END REPORT 

-- Subrutina para exportar el reporte a excel usando WinCOM 

FUNCTION bcorep002_excel(filename) 
 DEFINE sb          base.StringBuffer
 DEFINE ch          base.channel
 DEFINE lista       base.StringTokenizer
 DEFINE result      INTEGER
 DEFINE i           INTEGER
 DEFINE xlapp       INTEGER
 DEFINE xlwb        INTEGER
 DEFINE linea       STRING 
 DEFINE lineadep    STRING 
 DEFINE filename    STRING 
 DEFINE columna     STRING 
 DEFINE separador   CHAR(1) 
 DEFINE nuevalinea  CHAR(1) 

 -- Inicializando variables                            
 LET xlapp = -1
 LET xlwb = -1

 -- Creando la instancia de Excel
 CALL ui.Interface.frontCall("WinCOM", "CreateInstance", ['Excel.Application'], [xlapp])
 CALL CheckError(xlapp, xlwb, xlapp)
   
 -- Agregando un libro al documento de excel               
 CALL ui.interface.frontCall("WinCOM", "CallMethod", [xlapp, 'WorkBooks.Add'], [xlwb])
 CALL CheckError(xlapp, xlwb, xlwb)
   
 -- Haciendo visible el libro                 
 CALL excel_set_property(xlapp, xlapp, "Visible","True")
  
 -- Llenando celdas desde el FrontCall 
 -- Construyendo celdas delimitando columnas por TAB (ASCII(9)), 
 -- y filas por CR (ASCII(10)
 -- Copia al clipboard via el Frontcall y luego instruye a excel para pegar el contenido
 -- de la celdas dentro del libro 
 LET sb = base.StringBuffer.create()

 -- Leyendo el archivo que contiene el reporte y copiandolo al clipboard 
 LET ch = base.channel.create()
 call ch.openfile(filename,"r")

 let i = 1 
 let separador  = ascii(9) 
 let nuevalinea = ascii(10)
 while true
  let linea = ch.readLine() 
  if ch.isEof() then exit while end if
  if linea.getindexof(separador,1)>0 then
     CALL sb.append(linea)
     CALL sb.append(nuevalinea)
  end if 
  let i = i+1
 end while
 call ch.close() 

 -- Pegando el contenido de las celdas en el clipboard al libro de excel
 CALL ui.Interface.FrontCall("standard","cbset",sb.toString(),result)
 CALL excel_call_method(xlapp, xlwb, 'ActiveSheet.Paste')
 CALL excel_call_method(xlapp, xlwb, 'Columns("A:Z").EntireColumn.AutoFit')
 --CALL excel_set_property(xlapp, xlwb, excel_column(nc,"Z","NumberFormat"), "0.00")

 -- Liberando informacion del clipboard    
 CALL freeMemory(xlapp, xlwb)
END FUNCTION
