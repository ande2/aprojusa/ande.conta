{
parqbe001.4gl 
Mynor Ramirez
Mantenimiento de parametros generales
}

-- Definicion de variables globales 

GLOBALS "parglo001.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION parqbe001_parametros(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),    
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,   
        msg                 STRING,
        rotulo              CHAR(12), 
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL parqbe001_EstadoMenu(operacion," ") 
   INITIALIZE qrytext,qrypart TO NULL
   CALL parmae001_inival(1)

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = "SELECT a.numpar,a.tippar,a.nompar,a.valchr ",
                 " FROM glb_paramtrs a ",
                 " ORDER BY 1,2 "

   -- Declarando el cursor
   PREPARE cparams FROM qrypart
   DECLARE c_paramtrs SCROLL CURSOR WITH HOLD FOR cparams

   -- Inicializando vector de seleccion
   CALL v_paramtrs.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_paramtrs INTO v_paramtrs[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_paramtrs
   FREE  c_paramtrs
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL parqbe001_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_paramtrs TO s_paramtrs.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL parmae001_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscando
      CALL parmae001_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF parmae001_parametros() THEN
         EXIT DISPLAY 
      ELSE 
         -- Desplegando datos
         CALL parqbe001_datos(v_paramtrs[ARR_CURR()].tnumpar,
                              v_paramtrs[ARR_CURR()].ttippar)
      END IF 

     ON KEY (CONTROL-M) 
      -- Modificando 
      IF (operacion=2) THEN 
       -- Modificando 
       IF parmae001_parametros() THEN
          EXIT DISPLAY 
       ELSE 
          -- Desplegando datos
          CALL parqbe001_datos(v_paramtrs[ARR_CURR()].tnumpar,
                               v_paramtrs[ARR_CURR()].ttippar)
       END IF 
      END IF 

     ON ACTION reporte 
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Nombre del Parametro" 
      LET arrcols[2] = "Valor del Parametro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte 
      LET qry        = "SELECT a.nompar,a.valchr,a.numpar,a.tippar ",
                       " FROM glb_paramtrs a ",
                       " ORDER BY 3,4"

      -- Ejecutando el reporte
      LET res    = librut002_excelreport("Parametros",qry,2,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,3,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
       CALL parqbe001_datos(v_paramtrs[ARR_CURR()].tnumpar,v_paramtrs[ARR_CURR()].ttippar)
      END IF 
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen parametros registrados.",
    "stop")
   END IF 
  END WHILE
   
  -- Desplegando estado del menu 
  CALL parqbe001_EstadoMenu(0," ") 
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION parqbe001_datos(wnumpar,wtippar)
 DEFINE wnumpar LIKE glb_paramtrs.numpar, 
        wtippar LIKE glb_paramtrs.tippar,
        existe  SMALLINT,
        qryres  STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  glb_paramtrs a "||
              "WHERE a.numpar = "||wnumpar||
              "  AND a.tippar = "||wtippar||
              " ORDER BY 1,2 "

 -- Declarando el cursor
 PREPARE cparamst FROM qryres
 DECLARE c_paramtrst SCROLL CURSOR WITH HOLD FOR cparamst

 -- Llenando vector de seleccion
 FOREACH c_paramtrst INTO w_mae_pro.*
  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.nompar THRU w_mae_pro.horsis
 END FOREACH
 CLOSE c_paramtrst
 FREE  c_paramtrst

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nompar THRU w_mae_pro.horsis
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION parqbe001_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Empresas - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Empresas - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Empresas - MODIFICAR"||msg)
 END CASE
END FUNCTION
                                  
