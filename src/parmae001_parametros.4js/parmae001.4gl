{
parmae001.4gl 
Mynor Ramirez
Mantenimiento de parametros generales
}

-- Definicion de variables globales 

GLOBALS "parglo001.4gl"

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
  IF isGDC() THEN CALL ui.Interface.loadStyles("styles") END IF 
 CALL ui.Interface.loadToolbar("toolbar18")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("parametros")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo usuario del sistema
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de principal 
 CALL parmae001_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION parmae001_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255), 
        existe   SMALLINT,
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "parmae001a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Parametros"
   BEFORE MENU
    -- Verificando accesos
    -- Consulta
    IF NOT seclib001_accesos(progname,2,username) THEN
       CALL DIALOG.setActionActive("buscar",FALSE)
    END IF  
    -- Modificar 
    IF NOT seclib001_accesos(progname,1,username) THEN
       CALL DIALOG.setActionActive("modificar",FALSE)
    END IF  

    -- Consultar 
   COMMAND "Buscar"
    " Busqueda de parametros."
    CALL parqbe001_parametros(1) 
   COMMAND "Modificar"
    " Modificacion de parametros existentes."
    CALL parqbe001_parametros(2) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para la modificacion de los datos del mantenimiento 

FUNCTION parmae001_parametros()
 DEFINE loop,existe,opc   SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT

 -- Inicio del loop
 LET loop      = TRUE
 LET retroceso = TRUE
 WHILE loop
  -- Ingresando datos
  INPUT BY NAME w_mae_pro.valchr 
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   AFTER FIELD valchr  
    --Verificando valor del parametro 
    IF (LENGTH(w_mae_pro.valchr)=0) THEN
       ERROR "Error: valor del parametro invalido, VERIFICA"
       LET w_mae_pro.valchr = NULL
       NEXT FIELD valchr  
    END IF
   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.valchr IS NULL THEN 
       NEXT FIELD valchr
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")
  CASE (opc)
   WHEN 0 -- Cancelando
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando parametro
    CALL parmae001_grabar()
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para modificar un parametro

FUNCTION parmae001_grabar()
 -- Grabando transaccion
 ERROR " Actualizando parametro ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

  -- Actualizando
  SET LOCK MODE TO WAIT

  --Actualizando 
  UPDATE glb_paramtrs
  SET    glb_paramtrs.valchr = w_mae_pro.valchr
  WHERE  glb_paramtrs.numpar = w_mae_pro.numpar 
    AND  glb_paramtrs.tippar = w_mae_pro.tippar 

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion","Parametro actualizado.","information")
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION parmae001_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.nompar THRU w_mae_pro.horsis 
END FUNCTION
