{ 
parglo001.4gl
Mynor Ramirez
Mantenimiento de parametros generales 
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "parmae001"
DEFINE w_mae_pro   RECORD LIKE glb_paramtrs.*,
       v_paramtrs  DYNAMIC ARRAY OF RECORD
        tnumpar    LIKE glb_paramtrs.numpar,
        ttippar    LIKE glb_paramtrs.tippar,
        tnompar    LIKE glb_paramtrs.nompar,
        tvalchr    LIKE glb_paramtrs.valchr
       END RECORD,
       username    VARCHAR(15) 
END GLOBALS
