{ 
glbglo042.4gl
Mantenimiento de bancos 
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "glbmae042"
DEFINE w_mae_pro   RECORD LIKE glb_mtbancos.*,
       v_bancos    DYNAMIC ARRAY OF RECORD
        tcodbco    LIKE glb_mtbancos.codbco,
        tnombco    LIKE glb_mtbancos.nombco 
       END RECORD,
       username    VARCHAR(15)
END GLOBALS
