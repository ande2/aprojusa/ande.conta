SCHEMA db0000

DEFINE grDep RECORD
  id_commdep  LIKE commdep.id_commdep,
  descripcion LIKE commdep.descripcion,
  hijo_de     LIKE commdep.hijo_de
END RECORD,
grPue RECORD
  id_commpue    LIKE commpue.id_commpue,
  descripcion   LIKE commpue.descripcion,
  id_commdep    LIKE commpue.id_commdep
END RECORD  

{MAIN

  MENU "Reporte"
    COMMAND "Generar"
      CALL genera()
    COMMAND "Salir"
      EXIT MENU 
  END MENU 

END MAIN }

FUNCTION repPuesto()
  DEFINE pos SMALLINT
  DEFINE ant LIKE commdep.hijo_de
  DEFINE pLine VARCHAR(100)
  DEFINE sql_stmt STRING 
  DEFINE posPue SMALLINT 
  
  START REPORT rRep
  DECLARE curDep CURSOR FOR 
      SELECT id_commdep, descripcion, hijo_de 
        FROM commdep 
       WHERE estado = 1 
         AND id_commdep > 0 
    ORDER BY hijo_de

    LET sql_stmt = "SELECT * FROM commpue WHERE id_commdep = ? AND estado = 1 ORDER BY id_commpue "
    PREPARE ex_stmt FROM sql_stmt 
    DECLARE curPue CURSOR FOR ex_stmt 
    
    LET pos = 12
    LET ant = 0  
    FOREACH curDep INTO grDep.*
      
      LET pLine = grDep.id_commdep USING "######"
      
      IF grDep.hijo_de <> ant THEN
         LET ant = grDep.hijo_de
         LET pos = pos + 3
         LET pLine[pos,100] = grDep.descripcion
      ELSE 
         LET pLine[pos,100] = grDep.descripcion
      END IF 
      
      OUTPUT TO REPORT rRep(pLine)
      -- Para puestos
      OPEN curPue USING grDep.id_commdep
      WHILE TRUE
        FETCH NEXT curPue INTO grPue.*
        IF sqlca.sqlcode = 100 THEN
           EXIT WHILE 
        ELSE 
           --LET pLine = ""
           LET pLine = grPue.id_commpue USING "######"
           LET posPue = pos + 3
           LET pLine[posPue,100] = "-", grPue.descripcion 
           OUTPUT TO REPORT rRep(pLine)
        END IF 
      END WHILE    
    END FOREACH

    FINISH REPORT rRep
    
END FUNCTION 


REPORT rRep(lLine)
 
DEFINE lLine VARCHAR(100)

DEFINE vEmpresa LIKE commemp.nombre
DEFINE vUsuario VARCHAR(50)

FORMAT 

  PAGE HEADER 
    --Para empresa
    LET vEmpresa = "SELECT nombre FROM commemp WHERE id_commemp = 1"
    PREPARE ex_vEmpresa FROM vEmpresa
    EXECUTE ex_vEmpresa INTO vEmpresa
    PRINT COLUMN 001, vEmpresa,
          COLUMN 075, "FECHA   : ", TODAY USING "dd/mm/yyyy"

    PRINT COLUMN 025, "REPORTE DE ORGANIZACIÓN - PUESTOS"

    --Para usuario
    LET vUsuario = "SELECT UNIQUE USER FROM systables WHERE tabid = 99"
    PREPARE ex_vUsuario FROM vUsuario 
    EXECUTE ex_vUsuario INTO vUsuario 
    PRINT COLUMN 001, "USUARIO: ", vUsuario,
          COLUMN 075, "HOJA No.: ", PAGENO USING "&&"
    PRINT COLUMN 000, "=============================================================================================="
    SKIP 2 LINES 

    PRINT COLUMN 001, " CODIGO", 
          COLUMN 015, "DESCRIPCIÓN"
    PRINT COLUMN 001, "--------",
          COLUMN 015, "--------------------------"

  ON EVERY ROW 
    PRINT COLUMN 001, lLine

END REPORT 