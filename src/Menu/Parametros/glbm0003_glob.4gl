################################################################################
# Funcion     : %M%
# id_commdep : Catalogo de Empresas
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        id_commdep de la modificacion
#
################################################################################
SCHEMA db0000

GLOBALS 
TYPE 
   tDet RECORD 
      numpar    LIKE glb_paramtrs.numpar,
      tippar    LIKE glb_paramtrs.tippar,
      nompar    LIKE glb_paramtrs.nompar,
      valchr    LIKE glb_paramtrs.valchr,
      userid    LIKE glb_paramtrs.userid,
      fecsis    LIKE glb_paramtrs.fecsis,
      horsis    LIKE glb_paramtrs.horsis
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "glbm0003"
END GLOBALS