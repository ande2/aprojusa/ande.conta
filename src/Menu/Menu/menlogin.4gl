DATABASE erpjuridico

MAIN
   --DEFINE ok   SMALLINT
   DEFINE ok,c,s  SMALLINT
   DEFINE rec
      RECORD
         usuLogin LIKE glb_usuarios.userid,
         usuPwd   LIKE glb_usuarios.passwd
      END RECORD
   DEFINE qry     STRING
   DEFINE runcmd  STRING 

   CLOSE WINDOW SCREEN 
   
   CALL ui.Interface.loadActionDefaults("actiondefaultsLogin")
   CALL ui.Interface.loadStyles("style")
 
{   LET ok = login()

   IF (ok) THEN
   END IF 
END FUNCTION }

--FUNCTION login()
  
   LET ok = FALSE
   LET s = TRUE
   OPEN WINDOW menLogin AT 1,1 WITH FORM "menLogin" ATTRIBUTES (TEXT = "Inicio de sesion...")
   --CALL lib_cleanTinyScreen()
   CALL ui.Interface.setText("Inicio de sesion...")
  
   WHILE s
      LET int_flag = FALSE
      INPUT rec.usuLogin, rec.usuPwd FROM userid, passwd 
      
         AFTER INPUT
            IF (NOT int_flag) THEN
               LET c = 0
               {WHENEVER ERROR CONTINUE 
                  CONNECT TO "segovia" USER rec.usuLogin USING rec.usuPwd
               WHENEVER ERROR STOP 
               ERROR "Resultado ", sqlca.sqlcode}
               
               LET qry = "SELECT COUNT(*) FROM glb_usuarios WHERE userid = ? AND passwd = ?"
               PREPARE prpL1 FROM qry
               EXECUTE prpL1 USING rec.* INTO c
               IF sqlca.sqlcode = 0 AND c = 1 THEN
                  CALL fgl_setenv("LOGNAME",rec.usuLogin) 
                  --CALL fgl_setenv("LOGNAME","sistemas") --Para pruebaa 
                  
                  {IF c = 1 THEN
                     LET qry = "SELECT * FROM usuario WHERE usuLogin = ? " --AND usuPwd = ?"
                     PREPARE prpL2 FROM qry
                     EXECUTE prpL2 USING rec.usuLogin INTO gUsuario.*
                     IF STATUS = 0 THEN}
                        LET s = FALSE
                        LET ok = TRUE
                     --END IF
               ELSE
                  CALL msg("Usuario/Password incorrecto "||sqlca.sqlcode)
                  NEXT FIELD userid
              -- END IF
               
               END IF
            ELSE
               LET int_flag = TRUE
               LET s = FALSE
            END IF
      END INPUT
      
   END WHILE
   CLOSE WINDOW menLogin
   --RETURN ok, rec.usuLogin
   IF ok THEN 
      LET runcmd = "fglrun mainmenu.42r ", rec.usuLogin 
      RUN runcmd
   END IF 
END MAIN

FUNCTION msg(msg)
   DEFINE msg  STRING
   CALL box_valdato(msg)
END FUNCTION

FUNCTION lib_cleanTinyScreen()
define screenToBeCleaned   om.DomNode
define doc                 om.DomDocument
define l                   om.NodeList
define att                 STRING
constant nodeName = "Window"
constant nodeAtt  = "screen"

   let doc = ui.Interface.getDocument()
   let screenToBeCleaned = doc.getDocumentElement()
   let l = screenToBeCleaned.selectByTagName(nodeName)
   let screenToBeCleaned = l.item(1)
   let att = screenToBeCleaned.getAttribute("name")
   if att = nodeAtt then
      call doc.removeElement(screenToBeCleaned)
   end if
END FUNCTION

FUNCTION box_valdato(mensaje)
DEFINE
   mensaje VARCHAR(255),
   flag_return SMALLINT

   MENU "Advertencia"
      ATTRIBUTE(STYLE="dialog",COMMENT=mensaje CLIPPED,IMAGE="exclamation")
      COMMAND "Aceptar"
         LET flag_return = FALSE
         EXIT MENU
   END MENU
END FUNCTION