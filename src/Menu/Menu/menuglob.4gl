
SCHEMA "ande"
#########################################################################
## Function  : GLOBALS
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Declaracion de funciones globales
#########################################################################
GLOBALS
DEFINE gUsuario            RECORD LIKE adm_usu.*
DEFINE gDbname, dbname   	CHAR(40)
DEFINE dbempr_nomct        LIKE mempr.empr_nomct
DEFINE dbempr_nom          CHAR(100)
DEFINE dbmon_pais	         CHAR(50)
DEFINE cnt1			         SMALLINT
DEFINE i				         INTEGER
DEFINE gpais_id	         SMALLINT
DEFINE g_host		         STRING
DEFINE nodo                SMALLINT
DEFINE accion              SMALLINT 
DEFINE arre    DYNAMIC ARRAY OF
      RECORD
         menTipo   CHAR(1), 
         node       om.DomNode
      END RECORD
CONSTANT cRaiz = -1
CONSTANT cErr = "Error. La operación no fue procesada."
CONSTANT cAddOK = "Registro agregado exitosamente"
CONSTANT cDelOK = "Registro eliminado"
CONSTANT cUpdOK = "Registro actualizado"
CONSTANT cPerOK = "Permisos actualizados"
END GLOBALS