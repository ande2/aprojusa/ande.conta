GLOBALS "menuGlobals.4gl"


DEFINE gUsuario   RECORD LIKE usuario.*
CONSTANT cRaiz = -1
CONSTANT cErr = "Error. La operaci?n no fue procesada."
CONSTANT cAddOK = "Registro agregado exitosamente"
CONSTANT cDelOK = "Registro eliminado"
CONSTANT cUpdOK = "Registro actualizado"
CONSTANT cPerOK = "Permisos actualizados"




#########################################################################
## Function  : MAIN
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Funcion principal del programa
#########################################################################
MAIN
   DEFER INTERRUPT
   OPTIONS
      INPUT WRAP,
      ON CLOSE APPLICATION STOP
   WHENEVER ERROR CONTINUE
   CALL catalogos()
END MAIN

#########################################################################
## Function  : catalogos()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Opciones del Menu Catalogos 
#########################################################################
FUNCTION catalogos()
   DEFINE w ui.Window
   OPEN WINDOW menCatalogos AT 1,1 WITH FORM "menCatalogos" 
      CALL lib_cleanTinyScreen()
      CALL ui.Interface.setText("Catalogos")
      CALL ui.Interface.loadActionDefaults("actiondefaults")
      CALL ui.Interface.loadStyles("style")
      CALL ui.Interface.loadToolbar("toolbar12")
      MENU ""
         ON ACTION grupos
            CALL catGrupo()
         ON ACTION usuarios
            CALL catUsuario()
         ON ACTION menus
            CALL catMenu()
         ON ACTION permisos
            CALL catPermiso()
         ON ACTION cancel
            EXIT MENU
      END MENU
   CLOSE WINDOW menCatalogos
END FUNCTION

#########################################################################
## Function  : catMenu()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Opciones de Programa Menu de Catalogos
#########################################################################
FUNCTION catMenu()
   DEFINE rec  RECORD LIKE menu.*
   DEFINE qry  STRING
   DEFINE cmb  STRING
   DEFINE resp SMALLINT

   LET cmb = "SELECT menId, menId||'-'||menNombre FROM menu WHERE menTipo = 0"
   OPEN WINDOW menCatMenu AT 1,1 WITH FORM "menCatMenu" ATTRIBUTES (TEXT = "Catalogo de Menus")
      MENU ""
			BEFORE MENU
				HIDE OPTION ALL
				SHOW OPTION "buscar","agregar"

         ON ACTION buscar
				SHOW OPTION ALL
      		CALL combo_din2("menpadre", cmb)
      		LET qry = "SELECT * FROM menu"
      		PREPARE prpCM1 FROM qry
      		DECLARE curCM1 SCROLL CURSOR FOR prpCM1
     			OPEN curCM1
     			FETCH FIRST curCM1 INTO rec.*
     			IF STATUS = NOTFOUND THEN
        			CALL msg("No existen datos en la tabla")
					HIDE OPTION ALL
					SHOW OPTION "buscar","agregar"
      		END IF
      		DISPLAY BY NAME rec.*

         ON ACTION agregar
            CALL combo_din2("menpadre", cmb)
            LET int_flag = FALSE
            INPUT BY NAME rec.*

               ON CHANGE menpadre 
                  ERROR "Menpadre ", rec.menpadre
                  
               AFTER INPUT
                  IF (NOT int_flag) THEN
                     LET rec.menId = 0
                     BEGIN WORK
                     INSERT INTO menu VALUES (rec.*)
                     IF STATUS = 0 THEN
                        COMMIT WORK
                        CALL msg(cAddOK)
                        CLOSE curCM1
                        OPEN curCM1
                        FETCH LAST curCM1 INTO rec.*
                        DISPLAY BY NAME rec.*
                     ELSE
                        ROLLBACK WORK
                        CALL msg(cErr)
                     END IF
                  ELSE
                     LET int_flag = TRUE
                  END IF
            END INPUT
				CLEAR FORM

         ON ACTION actualizar
            CALL combo_din2("menpadre", cmb)
            IF rec.menId <> cRaiz THEN
               LET int_flag = FALSE
               INPUT BY NAME rec.* WITHOUT DEFAULTS
                  AFTER INPUT
                     IF (NOT int_flag) THEN
                        BEGIN WORK
                        LET qry = "UPDATE menu SET menu.menNombre = ?, menu.menCmd = ?, menu.menTipo = ?, menu.menPadre = ? WHERE menu.menId = ?"
                        PREPARE prpCMU FROM qry
                        EXECUTE prpCMU USING rec.menNombre, rec.menCmd, rec.menTipo, rec.menPadre, rec.menId
                        IF STATUS = 0 THEN
                           COMMIT WORK
                           CALL msg(cUpdOK)
                           CLOSE curCM1
                           OPEN curCM1
                           FETCH FIRST curCM1 INTO rec.*
                           DISPLAY BY NAME rec.* 
                        ELSE
                           ROLLBACK WORK
                           CALL msg(cErr)
                        END IF
                     ELSE
                        LET int_flag = TRUE
                     END IF
               END INPUT
					CLEAR FORM
					HIDE OPTION ALL
					SHOW OPTION "buscar","agregar"
            END IF

         ON ACTION eliminar
            IF rec.menId <> cRaiz THEN
               LET resp = FALSE
               LET resp = confirma("Esta seguro que desea eliminar el registro?")
               IF resp THEN
                  BEGIN WORK
                  LET qry = "DELETE FROM menu WHERE menu.menId = ", rec.menId USING"<<<<"
                  PREPARE prpCMD FROM qry
                  EXECUTE prpCMD
                  IF STATUS = 0 THEN
                     COMMIT WORK
                     CALL msg(cDelOk)
                     CLOSE curCM1
                     OPEN curCM1
                     FETCH FIRST curCM1 INTO rec.*
                     DISPLAY BY NAME rec.*               
                  ELSE
                     CALL msg(cErr)
                     ROLLBACK WORK
                  END IF
               END IF
					CLEAR FORM
					HIDE OPTION ALL
					SHOW OPTION "buscar","agregar"
            END IF

         ON ACTION primero
            FETCH FIRST curCM1 INTO rec.*
            DISPLAY BY NAME rec.*

         ON ACTION siguiente
            FETCH NEXT curCM1 INTO rec.*
            DISPLAY BY NAME rec.*

         ON ACTION anterior
            FETCH PREVIOUS curCM1 INTO rec.*
            DISPLAY BY NAME rec.*

         ON ACTION ultimo
            FETCH LAST curCM1 INTO rec.*
            DISPLAY BY NAME rec.*

         COMMAND KEY(INTERRUPT)
            EXIT MENU
      END MENU
   CLOSE WINDOW menCatMenu
END FUNCTION

#########################################################################
## Function  : catGrupo()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Opciones de Programa Grupo de Catalogos
#########################################################################
FUNCTION catGrupo()
   DEFINE rec  RECORD LIKE grupo.*
   DEFINE qry  STRING
   DEFINE resp SMALLINT

   OPEN WINDOW menCatGrupo AT 1,1 WITH FORM "menCatGrupo" ATTRIBUTES (TEXT = "Catalogo de Grupos")
      MENU ""
			BEFORE MENU
				HIDE OPTION ALL
				SHOW OPTION "buscar","agregar"

         ON ACTION buscar
				SHOW OPTION ALL
      		LET qry = "SELECT * FROM grupo"
      		PREPARE prpCG1 FROM qry
      		DECLARE curCG1 SCROLL CURSOR FOR prpCG1
      		OPEN curCG1
      		FETCH FIRST curCG1 INTO rec.*
      		IF STATUS = NOTFOUND THEN
         		CALL msg("No existen datos en la tabla")
					HIDE OPTION ALL
					SHOW OPTION "buscar","agregar"
      		END IF
      		DISPLAY BY NAME rec.*

         ON ACTION agregar
            LET int_flag = FALSE
            INPUT BY NAME rec.*
               AFTER INPUT
                  IF (NOT int_flag) THEN
                     LET rec.grpId = 0
                     BEGIN WORK
                     INSERT INTO grupo VALUES (rec.*)
                     IF STATUS = 0 THEN
                        COMMIT WORK
                        CALL msg(cAddOK)
                        CLOSE curCG1
                        OPEN curCG1
                        FETCH LAST curCG1 INTO rec.*
                        DISPLAY BY NAME rec.*
                     ELSE
                        ROLLBACK WORK
                        CALL msg(cErr)
                     END IF
                  ELSE
                     LET int_flag = TRUE
                  END IF
            END INPUT
				CLEAR FORM

         ON ACTION actualizar
            IF rec.grpId <> cRaiz THEN
               LET int_flag = FALSE
               INPUT BY NAME rec.* WITHOUT DEFAULTS
                  AFTER INPUT
                     IF (NOT int_flag) THEN
                        BEGIN WORK
                        LET qry = "UPDATE grupo SET grupo.grpNombre = ? WHERE grupo.grpId = ?"
                        PREPARE prpCGU FROM qry
                        EXECUTE prpCGU USING rec.grpNombre, rec.grpId
                        IF STATUS = 0 THEN
                           COMMIT WORK
                           CALL msg(cUpdOK)
                           CLOSE curCG1
                           OPEN curCG1
                           FETCH FIRST curCG1 INTO rec.*
                           DISPLAY BY NAME rec.* 
                        ELSE
                           ROLLBACK WORK
                           CALL msg(cErr)
                        END IF
                     ELSE
                        LET int_flag = TRUE
                     END IF
               END INPUT
					CLEAR FORM
					HIDE OPTION ALL
					SHOW OPTION "buscar","agregar"
            END IF

         ON ACTION eliminar
            IF rec.grpId <> cRaiz THEN
               LET resp = FALSE
               LET resp = confirma("Esta seguro que desea eliminar el registro?")
               IF resp THEN
                  BEGIN WORK
                  LET qry = "DELETE FROM grupo WHERE grupo.grpId = ", rec.grpId
                  PREPARE prpCGD FROM qry
                  EXECUTE prpCGD
                  IF STATUS = 0 THEN
                     COMMIT WORK
                     CALL msg(cDelOk)
                     CLOSE curCG1
                     OPEN curCG1
                     FETCH FIRST curCG1 INTO rec.*
                     DISPLAY BY NAME rec.*               
                  ELSE
                     CALL msg(cErr)
                     ROLLBACK WORK
                  END IF
               END IF
					CLEAR FORM
					HIDE OPTION ALL
					SHOW OPTION "buscar","agregar"
            END IF

         ON ACTION primero
            FETCH FIRST curCG1 INTO rec.*
            DISPLAY BY NAME rec.*

         ON ACTION siguiente
            FETCH NEXT curCG1 INTO rec.*
            DISPLAY BY NAME rec.*

         ON ACTION anterior
            FETCH PREVIOUS curCG1 INTO rec.*
            DISPLAY BY NAME rec.*

         ON ACTION ultimo
            FETCH LAST curCG1 INTO rec.*
            DISPLAY BY NAME rec.*

         COMMAND KEY(INTERRUPT)
            EXIT MENU
      END MENU
   CLOSE WINDOW menCatGrupo
END FUNCTION

#########################################################################
## Function  : catUsuario()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Opciones de Programa Usuarios de Catalogos
#########################################################################
FUNCTION catUsuario()
   DEFINE rec  RECORD LIKE usuario.*
   DEFINE qry  STRING
   DEFINE cmb  STRING
   DEFINE resp SMALLINT

   LET cmb = "SELECT grpId, grpNombre FROM grupo"
   OPEN WINDOW menCatUsuario AT 1,1 WITH FORM "menCatUsuario" ATTRIBUTES (TEXT = "Catalogo de Usuarios")
      MENU ""
			BEFORE MENU
				HIDE OPTION ALL
				SHOW OPTION "buscar","agregar"

         ON ACTION buscar
				SHOW OPTION ALL
      		CALL combo_din2("usugrpid", cmb)
      		LET qry = "SELECT * FROM usuario"
      		PREPARE prpCU1 FROM qry
      		DECLARE curCU1 SCROLL CURSOR FOR prpCU1
      		OPEN curCU1
      		FETCH FIRST curCU1 INTO rec.*
      		IF STATUS = NOTFOUND THEN
      	   	CALL msg("No existen datos en la tabla")
					HIDE OPTION ALL
					SHOW OPTION "buscar","agregar"
      		END IF
      		DISPLAY BY NAME rec.*

         ON ACTION agregar
            CALL combo_din2("usugrpid", cmb)
            LET int_flag = FALSE
            INPUT BY NAME rec.*
               AFTER INPUT
                  IF (NOT int_flag) THEN
                     LET rec.usuId = 0
                     BEGIN WORK
                     INSERT INTO usuario VALUES (rec.*)
                     IF STATUS = 0 THEN
                        COMMIT WORK
                        CALL msg(cAddOK)
                        CLOSE curCU1
                        OPEN curCU1
                        FETCH LAST curCU1 INTO rec.*
                        DISPLAY BY NAME rec.*
                     ELSE
                        ROLLBACK WORK
                        CALL msg(cErr)
                     END IF
                  ELSE
                     LET int_flag = TRUE
                  END IF
            END INPUT
				CLEAR FORM

         ON ACTION actualizar
            CALL combo_din2("usugrpid", cmb)
            IF rec.usuId <> cRaiz THEN
               LET int_flag = FALSE
               INPUT BY NAME rec.* WITHOUT DEFAULTS
                  AFTER INPUT
                     IF (NOT int_flag) THEN
                        BEGIN WORK
                        LET qry = "UPDATE usuario SET usuario.usuLogin = ?, usuario.usuPwd = ?, usuario.usuNombre = ?, usuario.usuGrpId = ? WHERE usuario.usuId = ?"
                        PREPARE prpCUU FROM qry
                        EXECUTE prpCUU USING rec.usuLogin, rec.usuPwd, rec.usuNombre, rec.usuGrpId, rec.usuId
                        IF STATUS = 0 THEN
                           COMMIT WORK
                           CALL msg(cUpdOK)
                           CLOSE curCU1
                           OPEN curCU1
                           FETCH FIRST curCU1 INTO rec.*
                           DISPLAY BY NAME rec.* 
                        ELSE
                           ROLLBACK WORK
                           CALL msg(cErr)
                        END IF
                     ELSE
                        LET int_flag = TRUE
                     END IF
               END INPUT
					CLEAR FORM
					HIDE OPTION ALL
					SHOW OPTION "buscar","agregar"
            END IF

         ON ACTION eliminar
            IF rec.usuId <> cRaiz THEN
               LET resp = FALSE
               LET resp = confirma("Esta seguro que desea eliminar el registro?")
               IF resp THEN
                  BEGIN WORK
                  LET qry = "DELETE FROM usuario WHERE usuario.usuId = ", rec.usuId
                  PREPARE prpCUD FROM qry
                  EXECUTE prpCUD
                  IF STATUS = 0 THEN
                     COMMIT WORK
                     CALL msg(cDelOk)
                     CLOSE curCU1
                     OPEN curCU1
                     FETCH FIRST curCU1 INTO rec.*
                     DISPLAY BY NAME rec.*               
                  ELSE
                     CALL msg(cErr)
                     ROLLBACK WORK
                  END IF
               END IF
					CLEAR FORM
					HIDE OPTION ALL
					SHOW OPTION "buscar","agregar"
            END IF

         ON ACTION primero
            FETCH FIRST curCU1 INTO rec.*
            DISPLAY BY NAME rec.*

         ON ACTION siguiente
            FETCH NEXT curCU1 INTO rec.*
            DISPLAY BY NAME rec.*

         ON ACTION anterior
            FETCH PREVIOUS curCU1 INTO rec.*
            DISPLAY BY NAME rec.*

         ON ACTION ultimo
            FETCH LAST curCU1 INTO rec.*
            DISPLAY BY NAME rec.*

         COMMAND KEY(INTERRUPT)
            EXIT MENU
      END MENU
   CLOSE WINDOW menCatUsuario
END FUNCTION


#########################################################################
## Function  : catPermiso()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Opciondes de Programa Permisos de Catalogos
#########################################################################
FUNCTION catPermiso()
   DEFINE qry, cmb   STRING
   DEFINE id         LIKE grupo.grpId
   DEFINE sigue      SMALLINT

   LET sigue = TRUE
   OPEN WINDOW menCatPermiso AT 1,1 WITH FORM "menCatPermiso" ATTRIBUTES (TEXT = "Catalogo de Permisos")
   WHILE sigue
      LET cmb = "SELECT grpId, grpNombre FROM grupo"
         CALL combo_din2("id", cmb)
         LET int_flag = FALSE
         INPUT BY NAME id
            AFTER INPUT
               IF (NOT int_flag) THEN
                  CALL tablaPermisos(id)
               ELSE
                  LET int_flag = TRUE
                  LET sigue = FALSE
               END IF
         END INPUT
   END WHILE
   CLOSE WINDOW menCatPermiso
END FUNCTION

#########################################################################
## Function  : tablaPermisos
##
## Parameters: id
##
## Returnings: int_flag
##
## Comments  : Verifica los permisos del usuario ingresado para ejecutar 
##             opciones 
#########################################################################
FUNCTION tablaPermisos(id)
   DEFINE id         LIKE grupo.grpId
   DEFINE arr        DYNAMIC ARRAY OF
      RECORD
         permiso     SMALLINT,
         menNombre   LIKE menu.menNombre,
         menId       LIKE menu.menId
      END RECORD
   DEFINE qry        STRING
   DEFINE i,cnt,pos  SMALLINT
   DEFINE err, x     SMALLINT
   LET qry = "SELECT 0, menNombre, menId FROM menu"
   PREPARE prpTP1 FROM qry
   DECLARE curTP1 CURSOR FOR prpTP1
   LET i = 1
   FOREACH curTP1 INTO arr[i].*
      LET qry = "SELECT COUNT(*) FROM permiso WHERE permiso.perGrpId = ? AND permiso.perMenId = ?"
      PREPARE prpTP2 FROM qry
      EXECUTE prpTP2 USING id, arr[i].menId INTO arr[i].permiso
      LET i = i + 1
   END FOREACH
   CALL SET_COUNT(i-1)
   LET int_flag = FALSE
   INPUT ARRAY arr WITHOUT DEFAULTS FROM scr.*
      AFTER INPUT
         IF (NOT int_flag) THEN
            LET err = FALSE
            BEGIN WORK
            FOR pos = 1 TO i
               LET qry = "SELECT COUNT(*) FROM permiso WHERE permiso.perGrpId = ? AND permiso.perMenId = ?"
               PREPARE prpTP3 FROM qry
               EXECUTE prpTP3 USING id, arr[pos].menId INTO cnt
               IF cnt <> arr[pos].permiso THEN
                  CASE arr[pos].permiso
                     WHEN 0
                        LET qry = "DELETE FROM permiso WHERE perGrpId = ? AND perMenId = ?"
                        PREPARE prpTP4 FROM qry
                        EXECUTE prpTP4 USING id, arr[pos].menId
                        IF STATUS <> 0 THEN
                           LET err = TRUE
                           EXIT FOR
                        END IF
                     OTHERWISE
                        LET qry = "INSERT INTO permiso VALUES (?,?)"
                        PREPARE prpTP5 FROM qry
                        EXECUTE prpTP5 USING id, arr[pos].menId
                        IF STATUS <> 0 THEN
                           LET err = TRUE
                           EXIT FOR
                        END IF
                  END CASE
               END IF
            END FOR
            IF (err) THEN
               ROLLBACK WORK
               CALL msg(cErr)
            ELSE
               COMMIT WORK
               CALL msg(cPerOK)
            END IF
         ELSE
            LET int_flag = TRUE
         END IF
   END INPUT
END FUNCTION
