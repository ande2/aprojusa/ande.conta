
SELECT y.lnktra,y.totdeb,y.tothab,x.fecemi,x.fecsis
       FROM  ctb_mtransac x,ctb_dtransac y,ctb_tipostrn t
       WHERE (x.lnktra =  y.lnktra)
         AND (x.codemp =  1)
         AND (x.fecemi >= "010118")
         AND (x.fecemi <= "310118")
         AND (y.numcta = "1110101")
         AND (t.tiptrn =  x.tiptrn)
       ORDER BY x.fecemi,x.lnktra
