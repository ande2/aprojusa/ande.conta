{ 
Credicorp
Programa : ctbre129.4gl
Autor    : Edwin Rolando Alvarado
Fecha    : Agosto 1996. 
Objetivo : Reporte de diario mayor general autorizado.
}

{ Definicion de variables globales }

GLOBALS "../cta/ctagb001.4gl"
 DEFINE w_mae_emp       RECORD LIKE empresas.*,
        w_emp_tmp       RECORD LIKE empresas.*,
        w_ult_cie       RECORD LIKE gb_mcierres.*,
        w_ult_cta       RECORD LIKE cb_saldomes.*, 
        w_total         RECORD
         cargos         DECIMAL(14,2),
         abonos         DECIMAL(14,2),
         cargos_a       DECIMAL(14,2),
         abonos_a       DECIMAL(14,2),
         saldo_ant      DECIMAL(14,2),
         saldo_act      DECIMAL(14,2),
         saldo_eje      DECIMAL(14,2)
        END RECORD,
        w_femision      LIKE cb_mtransac.f_emision,
        w_iniper        LIKE cb_mcuentas.f_apertura,
        w_periodo1      CHAR(50),
        w_periodo2      CHAR(50),
        w_preimpreso    SMALLINT,
        nohoja          SMALLINT,
        w_detallado     SMALLINT 

{ Subrutina para seleccionar las cuentas y movimientos del diario }

FUNCTION re129_diamge() 
 DEFINE w_mae_ctb       RECORD LIKE cb_mcuentas.*,
        w_inimes        LIKE cb_mcuentas.f_apertura,
        loop,exis,wf    SMALLINT,
        w_query,w_texto CHAR(500),
        msg,w_titulo    CHAR(80),
        hilera          CHAR(1)



 # Abriendo la forma para el reporte
 OPEN WINDOW wre129 AT 6,3
  WITH FORM "ctbfre129a" ATTRIBUTE(BORDER,FORM LINE 1,MESSAGE LINE LAST)
  CALL messages(1,0,3,78,"Emision de Diario Mayor General Autorizado")
    CALL ini_menus() 
    # Llenando el menu de opciones
    CALL load_menu(1,"Formato Preimpreso"," Hojas Autorizadas.")
    CALL load_menu(2,"Hoja Simple "," Se imprimira encabezado")
    CALL load_menu(3,"Regreso","") 

    # Ejecutando el menu de opciones
    LET wf = menus(4,8,05,0,"Tipo Hoja",0,0,1)
    # Verificando seleccion
    CASE (wf)
         WHEN 1 LET w_titulo = "FORMATO PRE-IMPRESO"
                DISPLAY "                                     " AT 8,2
                LET w_preimpreso = TRUE 
         WHEN 2 LET w_titulo = "HOJA SIMPLE"
                LET w_preimpreso = FALSE 
         WHEN 3 LET loop = FALSE
    END CASE
  CALL messages(2,0,3,70,w_titulo)

  # Definiendo archivo de impresion
  LET filename = ARG_VAL(3) CLIPPED,"/ctbre129.spl"

  LET msg = "F4 Regreso  Ctrl-I Reimpresion  Ctrl-G Libro Inventario"
  CALL msgup(1,msg,3)
  CURRENT WINDOW is wre129

  # Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   # Inicializando datos
   INITIALIZE w_mae_ctb.*,w_femision,w_query,w_texto TO NULL
   INITIALIZE w_mae_emp.* TO NULL
   LET w_emp_tmp.* = w_mae_emp.*
   LET w_femision = TODAY
   LET nohoja     = 1 
   CLEAR FORM
   DISPLAY BY NAME w_femision
   LET w_detallado = TRUE 

   # Construyendo la seleccion
   CONSTRUCT BY NAME w_query ON c.empresa,       
                                c.cuenta,    
                              # c.nombre,    
                                c.nivel    
    ON KEY(F4)
     # Salida
     LET loop = FALSE
     EXIT CONSTRUCT 
    ON KEY(F1)
     # Lista de empresas
     IF INFIELD(empresa) THEN
        LET hilera = view_empresas()
     END IF 
    ON KEY(CONTROL-I)
     # Recuperando ultimo reporte
     IF acceso("CB","reprint") THEN 
        CALL reprint(filename)
     END IF 
    ON KEY(CONTROL-G)
     LET w_detallado = NOT w_detallado
     IF (w_detallado=TRUE) THEN
        CALL msgerr("Reporte SI Detallado") 
     ELSE
        CALL msgerr("Reporte NO Detallado") 
     END IF
    ON KEY(INTERRUPT)
     ERROR "Tecla invalida."
    AFTER CONSTRUCT
     IF NOT FIELD_TOUCHED(c.empresa) THEN
        CALL msgerr(" Atencion: debes ingresar la empresa a listar.")
        # Inicializando datos
        INITIALIZE w_mae_ctb.*,w_femision,w_query,w_texto TO NULL
        LET w_femision = TODAY
        CLEAR FORM
        DISPLAY BY NAME w_femision
        NEXT FIELD empresa 
     END IF 
     
     LET wc_empresa       = GET_FLDBUF(c.empresa)
     CALL busca_empresa(wc_empresa) RETURNING w_mae_emp.*,exis
     DISPLAY BY NAME w_mae_emp.nombre ATTRIBUTE(REVERSE)
     # Buscando datos de la empresa
     IF wc_empresa = "01" THEN # (SISA = CREDICORP)
       INPUT BY NAME w_mae_emp.nombre WITHOUT DEFAULTS
         ON KEY(F4)
            EXIT INPUT 
       AFTER FIELD nombre
          IF w_mae_emp.nombre IS NULL THEN
             NEXT FIELD nombre
          END IF
       END INPUT
       # para que no cambie el nombre
       LET w_emp_tmp.* = w_mae_emp.*
     END IF

     # Ingresando fecha a la cual se desea el reporte
     IF re129_ingfecha() THEN
        CONTINUE CONSTRUCT
     END IF 
     # Verificar si la impresion se hara en papel sencillo  
     IF  w_preimpreso= FALSE THEN 
       INPUT BY NAME nohoja WITHOUT DEFAULTS
         ON KEY(F4)
            EXIT INPUT 
       AFTER FIELD nohoja
          IF nohoja IS NULL OR nohoja <=0 THEN
             NEXT FIELD nohoja
          END IF
       END INPUT
     END IF
     # ======= Criterios de busqueda =================
     LET wc_cuenta        = GET_FLDBUF(c.cuenta)
  -- LET wc_nombre        = GET_FLDBUF(c.nombre)
     LET wc_nivel         = GET_FLDBUF(c.nivel)
     LET wc_periodo       = "AL ",w_femision USING "dd/mmm/yyyy" 
   END CONSTRUCT
   LET w_periodo1 = "AL ",w_femision USING "dd/mmm/yyyy" 

   # Seleccionando dispositivo
   IF loop THEN
      LET impress = device(6,60)
      IF impress IS NULL THEN
         CONTINUE WHILE 
      END IF
   ELSE
      EXIT WHILE 
   END IF 

   # Construyendo seleccion 
   LET w_texto = "SELECT c.* ",
                  "FROM  cb_mcuentas c ",
                  "WHERE ",w_query CLIPPED,
                  " ORDER BY 2,1"

   # Preparando seleccion
   PREPARE c_re129 FROM w_texto
   DECLARE c_cre129 CURSOR FOR c_re129
   LET existe = FALSE
   FOREACH c_cre129 INTO w_mae_ctb.*

    # Iniciando reporte
    IF NOT existe THEN
       # Creando tabla temporal para sumas
       CREATE TEMP TABLE cb_sumasrep
       (empresa   CHAR(2),
        cuenta    CHAR(16),
        cargos    DECIMAL(14,2),
        abonos    DECIMAL(14,2),
        cargos_a  DECIMAL(14,2),
        abonos_a  DECIMAL(14,2),
        saldo_ant DECIMAL(14,2),
        saldo_act DECIMAL(14,2),
        saldo_eje DECIMAL(14,2),
        nivel     SMALLINT) WITH NO LOG

       # Seleccionando fonts
       CALL fonts(impress) RETURNING fnt.* 

       LET existe = TRUE
       START REPORT re129_idiariom TO filename
       ERROR "Atencion: seleccionando datos ... por favor espere ..."
    END IF 

    # Buscando ultimo cierre de la empresa de la cuenta
    INITIALIZE w_ult_cie.* TO NULL
    CALL dlast_cierre("CB",1,w_mae_ctb.empresa,w_femision)
    RETURNING w_ult_cie.*,exis

    # Verificando si existe cierre
    IF NOT exis THEN
       LET w_mae_ctb.saldo_ant = w_mae_ctb.saldo_ini
       LET w_mae_ctb.cargos_a  = 0
       LET w_mae_ctb.abonos_a  = 0
       LET w_inimes = 
       MDY(MONTH(w_mae_ctb.f_apertura),1,YEAR(w_mae_ctb.f_apertura))
       LET w_iniper = NULL 
    ELSE
       IF (w_ult_cie.default = "S") THEN 
          LET w_mae_ctb.saldo_ant = w_mae_ctb.saldo_ini
          LET w_mae_ctb.cargos_a  = 0
          LET w_mae_ctb.abonos_a  = 0
          LET w_inimes = 
          MDY(MONTH(w_mae_ctb.f_apertura),1,YEAR(w_mae_ctb.f_apertura))
          LET w_iniper = NULL 
       ELSE
          LET w_inimes=(w_ult_cie.f_cierre+1)
          LET w_iniper=w_inimes 

          # Buscando saldos del ultimo cierre
          INITIALIZE w_ult_cta.* TO NULL
          CALL last_cierrecta(w_mae_ctb.empresa,w_mae_ctb.cuenta,
                              w_ult_cie.f_cierre,"M")
          RETURNING w_ult_cta.*,exis

          LET w_mae_ctb.saldo_ant  = w_ult_cta.saldo_mes 
          LET w_mae_ctb.saldo_eje  = w_ult_cta.saldo_eje
       END IF 
    END IF 
    # Verificando que no sean nulos
    IF w_mae_ctb.saldo_ant IS NULL THEN
       LET w_mae_ctb.saldo_ant = 0
    END IF 
    IF w_mae_ctb.saldo_eje IS NULL THEN
       LET w_mae_ctb.saldo_eje = 0
    END IF 
    IF w_mae_ctb.cargos_a IS NULL THEN
       LET w_mae_ctb.cargos_a = 0
    END IF 
    IF w_mae_ctb.abonos_a IS NULL THEN
       LET w_mae_ctb.abonos_a = 0
    END IF 

    # Encontrando saldo al dia
    CALL saldo_cuenta(w_mae_ctb.empresa,w_mae_ctb.cuenta,w_mae_ctb.tipo_saldo,
                      w_mae_ctb.saldo_ant,w_inimes,w_femision)
    RETURNING w_mae_ctb.saldo_act,w_mae_ctb.cargos,w_mae_ctb.abonos 

    # Verificando que no sean nulos
    IF w_mae_ctb.saldo_act IS NULL THEN
       LET w_mae_ctb.saldo_act = 0
    END IF 
    IF w_mae_ctb.saldo_eje IS NULL THEN
       LET w_mae_ctb.saldo_eje = 0
    END IF 
    IF w_mae_ctb.saldo_ant IS NULL THEN
       LET w_mae_ctb.saldo_ant = 0
    END IF 
    IF w_mae_ctb.cargos IS NULL THEN
       LET w_mae_ctb.cargos = 0
    END IF 
    IF w_mae_ctb.abonos IS NULL THEN
       LET w_mae_ctb.abonos = 0
    END IF 

    # Creando cargos y abonos acumulados
    IF w_ult_cta.cargos_a IS NULL THEN
       LET w_ult_cta.cargos_a = 0
    END IF 
    IF w_ult_cta.abonos_a IS NULL THEN
       LET w_ult_cta.abonos_a = 0
    END IF 
    LET w_mae_ctb.cargos_a = (w_ult_cta.cargos_a+w_mae_ctb.cargos)
    LET w_mae_ctb.abonos_a = (w_ult_cta.abonos_a+w_mae_ctb.abonos)

    IF w_mae_ctb.cargos_a IS NULL THEN
       LET w_mae_ctb.cargos_a = 0
    END IF 
   
    IF w_mae_ctb.abonos_a IS NULL THEN
       LET w_mae_ctb.abonos_a = 0
    END IF 

    # Eliminando cuentas sin saldo y sin movimiento
    IF (w_mae_ctb.cargos=0) AND 
       (w_mae_ctb.abonos=0) AND 
       (w_mae_ctb.cargos_a=0) AND
       (w_mae_ctb.abonos_a=0) AND
       (w_mae_ctb.saldo_eje=0) AND
       (w_mae_ctb.saldo_ant=0) AND
       (w_mae_ctb.saldo_act=0) THEN
       CONTINUE FOREACH
    END IF 

    # Llenando tabla temporal
    INSERT INTO cb_sumasrep
    VALUES (w_mae_ctb.empresa,w_mae_ctb.cuenta,w_mae_ctb.cargos,
            w_mae_ctb.abonos,w_mae_ctb.cargos_a,w_mae_ctb.abonos_a,
            w_mae_ctb.saldo_ant,w_mae_ctb.saldo_act,w_mae_ctb.saldo_eje,
            w_mae_ctb.nivel)

    # Llenando el reporte
    OUTPUT TO REPORT re129_idiariom(w_mae_ctb.*,w_inimes)
   END FOREACH
   IF existe THEN
      # Finalizando el reporte
      FINISH REPORT re129_idiariom 

      # Borrando la tabla temporal
      DROP TABLE cb_sumasrep

      # Imprimiendo el reporte
      CALL to_printer(filename,impress,"132") 
   ELSE
      # Notificar que no existen datos
      CALL msgbusqueda()
   END IF 
  END WHILE
 CLOSE WINDOW wre129
 CLOSE WINDOW wmsgup3 
END FUNCTION 

{ Subrutina para ingresar la fecha de emision del reporte }    

FUNCTION re129_ingfecha()
 DEFINE regreso,lastkey SMALLINT

 # Ingresando fecha
 LET regreso    = FALSE
 LET w_femision = TODAY 
 DISPLAY BY NAME w_femision 
 INPUT BY NAME w_femision WITHOUT DEFAULTS
  ON KEY(F4)
   # Salida 
   LET regreso = TRUE
   EXIT INPUT 
  ON KEY(INTERRUPT)
   # Atrapandp errores
   ERROR "Tecla Invalida."
  AFTER FIELD w_femision
   LET lastkey = FGL_LASTKEY()
   IF (lastkey = FGL_KEYVAL("up")) OR (lastkey = FGL_KEYVAL("left")) THEN
      LET regreso = TRUE 
      EXIT INPUT
   END IF

   IF w_femision IS NULL THEN
      LET w_femision = TODAY
   END IF 
   DISPLAY BY NAME w_femision 
 END INPUT 
 RETURN regreso
END FUNCTION 

{ Subrutina para imprimir el reporte }

REPORT re129_idiariom(imp1,w_inimes)
 DEFINE imp1      RECORD LIKE cb_mcuentas.*,
        wm        RECORD LIKE cb_mtransac.*,
        wd        RECORD LIKE cb_dtransac.*,
        wt        RECORD LIKE cb_tipotrns.*,
        w_saldo   LIKE cb_mcuentas.saldo_act,
        w_cargom  LIKE cb_mcuentas.saldo_act,
        w_abonom  LIKE cb_mcuentas.saldo_act,
        w_inimes  LIKE cb_mcuentas.f_apertura,
        exis,col  SMALLINT,
        existr    SMALLINT,
        contador  SMALLINT,
        linea     CHAR(154),
        w_desc1   CHAR(33)

  OUTPUT LEFT   MARGIN 0 
         PAGE   LENGTH 88 --68 
         TOP    MARGIN 0 
         BOTTOM MARGIN 8 

  FORMAT 
  PAGE HEADER
   # Definiendo una linea
   LET linea = "-------------------------------------------------------------",
               "-------------------------------------------------------------",
               "--------------------------------"

   PRINT fnt.ini CLIPPED,fnt.t88 CLIPPED,fnt.c12 CLIPPED
   --PRINT fnt.ini CLIPPED,fnt.c10 CLIPPED
   LET col = centrado(142,w_periodo1)
   IF w_preimpreso = TRUE THEN
      PRINT COLUMN 001, ""
      PRINT COLUMN 001, ""
      PRINT COLUMN 001, ""
      SKIP 2 LINE
   ELSE
      PRINT COLUMN 001,fnt.twd CLIPPED, w_mae_emp.nombre, fnt.fwd CLIPPED
      PRINT COLUMN   1,"Guatemala, C.A.",
            --COLUMN  43,fnt.twd CLIPPED,"DIARIO MAYOR GENERAL",fnt.fwd CLIPPED
            COLUMN  43,fnt.twd CLIPPED,"LIBRO DE INVENTARIOS",fnt.fwd CLIPPED
      PRINT COLUMN 001,"Nit. : ",w_mae_emp.nit CLIPPED,
            COLUMN 132,"Hoja No.", 
                       fnt.twd CLIPPED,nohoja USING "<<<<<",fnt.fwd CLIPPED
      SKIP 2 LINE
      LET nohoja = nohoja + 1
   END IF
   # Imprimiendo encabezado
   PRINT COLUMN col,fnt.tbl CLIPPED,
            w_periodo1 CLIPPED,
            fnt.fbl CLIPPED
   IF (w_detallado=TRUE) THEN
   PRINT "Cuenta               Nombre cuenta                             ",
         "                    Saldo inicial           Cargos        ",
         "   Abonos           Saldo"
   ELSE
   PRINT "Cuenta               Nombre cuenta                             ",
         "                                                          ",
         "                    Saldo"
   END IF
   PRINT "                                                               ",
         "                    Del Ejercicio                         ",
         "                    Actual"
   PRINT "                         F/Emision    Abr # Documento  ",
         "C O N C E P T O"

  BEFORE GROUP OF imp1.empresa
   SKIP TO TOP OF PAGE

   # Definiendo periodo de transacciones
   LET w_periodo2 = "PERIODO DEL ",w_iniper   USING "dd/mmm/yyyy",
                    " AL ",w_femision USING "dd/mmm/yyyy"
   
   # Buscando datos de la empresa
   IF w_mae_emp.empresa = "01" AND imp1.empresa = "01" THEN # (SISA = CREDICORP)
   ELSE
      INITIALIZE w_mae_emp.* TO NULL
      CALL busca_empresa(imp1.empresa) RETURNING w_mae_emp.*,exis
   END IF

   {* IF w_mae_emp.consolida MATCHES "[Ss]" THEN
      PRINT fnt.twd CLIPPED,
            imp1.empresa CLIPPED," ",w_mae_emp.nombre," C O N S O L I D A D O",
            fnt.fwd CLIPPED
   ELSE
      PRINT fnt.twd CLIPPED,
            imp1.empresa CLIPPED," ",w_mae_emp.nombre,
            fnt.fwd CLIPPED
   END IF }

   SKIP 2 LINE
{
   IF (w_iniper IS NOT NULL) THEN
      PRINT fnt.tbl CLIPPED,
            w_periodo2 CLIPPED,
            fnt.fbl CLIPPED
   END IF 
}
   SKIP 1 LINES

  BEFORE GROUP OF imp1.cuenta
   # Colocando en cascada los nombres de las cuentas
   LET contador = (16+imp1.nivel)

   IF (w_detallado=TRUE) THEN
   PRINT COLUMN        1,imp1.cuenta,
         COLUMN contador,imp1.nombre,
         COLUMN       83,imp1.saldo_eje  USING "---,---,--&.&&",1 SPACES,
                         imp1.cargos_a   USING "-,---,---,--&.&&",1 SPACES,
                         imp1.abonos_a   USING "-,---,---,--&.&&",3 SPACES,
                         imp1.saldo_act  USING "---,---,--&.&&"
   ELSE
   PRINT COLUMN        1,imp1.cuenta,
         COLUMN contador,imp1.nombre,
         COLUMN       131, imp1.saldo_act  USING "---,---,--&.&&"
   END IF

  ON EVERY ROW
   # Buscando transacciones de la cuentas de detalle
   IF (imp1.tipo_cta="D") AND (w_detallado=true) THEN
      LET w_saldo  = imp1.saldo_ant
      # Verificando datos
      IF w_saldo IS NULL THEN
         LET w_saldo = 0
      END IF
      LET w_cargom = 0
      LET w_abonom = 0
      DECLARE ctrans CURSOR FOR
      SELECT m.*,d.*
       FROM  cb_mtransac m,cb_dtransac d, cb_tipotrns t
       WHERE (m.link      =  d.link) AND
             (m.empresa   =  imp1.empresa) AND
             (m.tipo_trns =  t.tipo_trns) AND
             (t.funcion  !=  "R") AND 
             (t.funcion  !=  "C") AND 
             (m.f_emision >= w_inimes) AND
             (m.f_emision <= w_femision) AND
             (d.cuenta    =  imp1.cuenta)  
       ORDER BY m.f_emision,m.documento

       LET existr = FALSE 
       FOREACH ctrans INTO wm.*,wd.*
        IF NOT existr THEN
           LET existr = TRUE 

           SKIP 1 LINES
           PRINT COLUMN  66,"Saldo Anterior : ",
                            imp1.saldo_ant USING "---,---,--&.&&"
           SKIP 1 LINES
        END IF 

        # Obteniendo datos del tipo de transaccion
        INITIALIZE wt.* TO NULL
        CALL busca_tiptrncb(wm.tipo_trns)
        RETURNING wt.*,exis

        # Calculando saldo 
        CASE (wd.operacion)
         WHEN "D" 
           # Verificando tipo de saldo de la cuenta 
           CASE (imp1.tipo_saldo) 
            WHEN "D" # Deudor
             LET w_saldo  = (w_saldo +wd.debe)
             LET w_cargom = (w_cargom+wd.debe)
            WHEN "A"
             LET w_saldo  = (w_saldo -wd.debe)
             LET w_cargom = (w_cargom+wd.debe)
           END CASE
         WHEN "H" 
           # Verificando tipo de saldo de la cuenta 
           CASE (imp1.tipo_saldo) 
            WHEN "D" # Acreedor 
             LET w_saldo  = (w_saldo -wd.haber)
             LET w_abonom = (w_abonom+wd.haber)
            WHEN "A"
             LET w_saldo  = (w_saldo +wd.haber)
             LET w_abonom = (w_abonom+wd.haber)
           END CASE
        END CASE
        LET w_desc1     = NULL
        IF (wd.concepto IS NULL) OR (wd.concepto = " ") THEN
           LET wd.concepto = wm.concepto1 CLIPPED
        END IF
        LET w_desc1     = wd.concepto

        # Imprimiendo datos
        PRINT COLUMN  21,wm.f_emision USING "dd/mmm/yyyy"   ,2  SPACES,
                         wt.abreviado                       ,1  SPACES,
                         wm.documento                       ,2  SPACES,
                         w_desc1                            ,16 SPACES, 
                         wd.debe      USING "---,---,--&.&&",3  SPACES,
                         wd.haber     USING "---,---,--&.&&",3  SPACES,
                         w_saldo      USING "---,---,--&.&&"

       END FOREACH
       IF existr THEN
          SKIP 1 LINES
           PRINT COLUMN  66,"Sumas del Mes  : ",
                 COLUMN 100,w_cargom       USING "---,---,--&.&&",
                 COLUMN 117,w_abonom       USING "---,---,--&.&&"
          SKIP 1 LINES
       END IF 
   END IF

  AFTER GROUP OF imp1.empresa  
   # Hallando maximo nivel de cuentas por empresa
   LET w_total.cargos    = 0
   LET w_total.abonos    = 0
   LET w_total.saldo_act = 0
   LET w_total.saldo_ant = 0
   LET w_total.saldo_eje = 0
   LET w_total.cargos_a  = 0
   LET w_total.abonos_a  = 0
   SELECT SUM(c.cargos)    ,SUM(c.abonos)    ,SUM(c.saldo_act),
          SUM(c.saldo_ant) ,SUM(c.saldo_eje) ,SUM(c.cargos_a)  ,
          SUM(c.abonos_a)
    INTO  w_total.cargos   ,w_total.abonos   ,w_total.saldo_act,
          w_total.saldo_ant,w_total.saldo_eje,w_total.cargos_a,
          w_total.abonos_a
    FROM  cb_sumasrep c
    WHERE (c.empresa = imp1.empresa) AND
          (c.nivel   = (SELECT MAX(d.nivel)
                         FROM  cb_sumasrep d
                         WHERE (d.empresa = imp1.empresa) AND
                               (d.nivel   IS NOT NULL)))
   # Totalizando empresa
   SKIP 1 LINES

   PRINT COLUMN   1,"Total transacciones periodo  -->",
         COLUMN  60,"Saldo Anterior : ",
         COLUMN  81,w_total.saldo_ant         USING "-,---,---,--&.&&",1 SPACES,
                    w_total.cargos            USING "-,---,---,--&.&&",1 SPACES,
                    w_total.abonos            USING "-,---,---,--&.&&",1 SPACES,
                    w_total.saldo_act         USING "-,---,---,--&.&&"

   PRINT COLUMN   1,"Total empresa ",w_mae_emp.abreviado," -->",
         COLUMN  81,w_total.saldo_eje         USING "-,---,---,--&.&&",1 SPACES,
                    w_total.cargos_a          USING "-,---,---,--&.&&",1 SPACES,
                    w_total.abonos_a          USING "-,---,---,--&.&&",1 SPACES,
                    w_total.saldo_act         USING "-,---,---,--&.&&"

{
  ON LAST ROW
   # ======== Imprimiendo los criterios de seleccion =======================
   LET w_crit = 0 
   SKIP 5 LINES
   LET col = centrado(154,"CRITERIOS DE BUSQUEDA DEL REPORTE")
   PRINT COLUMN col, "CRITERIOS DE BUSQUEDA DEL REPORTE"
   PRINT COLUMN col, "---------------------------------"
   SKIP 1 LINE
   PRINT COLUMN 005, "Los criterios ingresados son:"
   SKIP 1 LINE

   WHILE TRUE
        LET w_crit = w_crit + 1 
        CASE
           WHEN w_crit = 1 AND wc_empresa  IS NOT NULL AND wc_empresa  != " " 
                PRINT COLUMN 005, "Numero Empresa  : ",
                      COLUMN 021, wc_empresa   CLIPPED
           WHEN w_crit = 2 AND wc_cuenta   IS NOT NULL AND wc_cuenta   != " " 
                PRINT COLUMN 005, "Numero Cuenta   : ",
                      COLUMN 021, wc_cuenta    CLIPPED
           WHEN w_crit = 3 AND wc_nombre   IS NOT NULL  AND wc_nombre    != " " 
                PRINT COLUMN 005, "Nombre          : ",
                      COLUMN 021, wc_nombre    CLIPPED
           WHEN w_crit = 4 AND wc_nivel    IS NOT NULL  AND wc_nivel     != " " 
                PRINT COLUMN 005, "Nivel           : ",
                      COLUMN 021, wc_nivel     CLIPPED
           WHEN w_crit = 5 AND wc_periodo IS NOT NULL  AND wc_periodo != " " 
                PRINT COLUMN 005, "Periodo         : ",
                      COLUMN 021, wc_periodo     CLIPPED
           WHEN w_crit = 6 EXIT WHILE
        END CASE
   END WHILE

   SKIP 3 LINES
   PRINT COLUMN 005, fnt.tbl CLIPPED, "Simbologia : ",fnt.fbl CLIPPED,
         COLUMN 029, "*  Todos",
         COLUMN 049, "!= Diferente a",
         COLUMN 069, "<= Menor igual a",
         COLUMN 089, ">= Mayor igual a "
   PRINT COLUMN 025, "|  Ambos",
         COLUMN 045, ":  Rango",
         COLUMN 065, "=  Igual a",
         COLUMN 085, "?  Equivalente"
}
END REPORT
