{ 
ctbglo008.4gl
Mynor Ramirez
Mantenimiento de centros de costo 
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "ctbmae008"
DEFINE w_mae_pro      RECORD LIKE glb_grucenct.*,
       v_centros      DYNAMIC ARRAY OF RECORD
        tcodgru       LIKE glb_grucenct.codgru,
        tnomgru       LIKE glb_grucenct.nomgru,
        tendrec       CHAR(1) 
       END RECORD,
       username       VARCHAR(15)
END GLOBALS
