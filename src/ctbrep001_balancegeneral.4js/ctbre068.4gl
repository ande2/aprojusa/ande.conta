{ 
Credicorp
Programa : ctbre068.4gl
Autor    : Mynor Ramirez    
Fecha    : Marzo 1995. 
Objetivo : Reporte de Balance General.
}

{ Definicion de variables globales }

GLOBALS "../cta/ctagb001.4gl"
 DEFINE w_mae_emp       RECORD LIKE empresas.*,
        w_ult_cie       RECORD LIKE gb_mcierres.*,
        w_ult_cta       RECORD LIKE cb_saldomes.*, 
        w_femision      LIKE cb_mtransac.f_emision,
        w_iniper        LIKE cb_mcuentas.f_apertura,
        w_periodo1      CHAR(50)

{ Subrutina para seleccionar las cuentas del balance general }

FUNCTION re068_balgen() 
 DEFINE w_mae_ctb       RECORD LIKE cb_mcuentas.*,
        w_inimes        LIKE cb_mcuentas.f_apertura,
        loop,exis       SMALLINT,
        w_query,w_texto CHAR(500),
        msg             CHAR(80),
        hilera          CHAR(1),
        w_separa        SMALLINT,
        w_ctb_mae       RECORD LIKE cb_mcuentas.*,
        w_mae_ctr       RECORD LIKE cb_ctransac.*,
        w_mae_tra       RECORD LIKE cb_mtransac.*,
        w_mae_trn       RECORD LIKE cb_tipotrns.*,
        w_mae_agr       RECORD LIKE cb_agructas.*,
        w_salcta        RECORD
           empresa         LIKE cb_mcuentas.empresa,
           cuenta          LIKE cb_mcuentas.cuenta,
           debe            DECIMAL(14,2),
           haber           DECIMAL(14,2),
           operacion       LIKE cb_ctransac.operacion
                        END RECORD


   # Construyendo seleccion 
   LET w_texto = "SELECT c.* ",
                  "FROM  cb_mcuentas c,cb_creareps r ",
                  "WHERE c.empresa     = c.empcta",
                  #" AND  c.f_apertura <= \"",w_femision,"\"",
                  " AND  c.tipo_cta    = \"M\" ",
                  " AND  c.cuenta_p1   = r.cuenta_p1 ",
                  " AND  r.reporte     = 1 ",
                  " AND  ",w_query CLIPPED,
                  " ORDER BY 2,1"

   # Preparando seleccion
   PREPARE c_re068 FROM w_texto
   DECLARE c_cre068 CURSOR FOR c_re068
   LET existe = FALSE
   LET w_separa = TRUE
   FOREACH c_cre068 INTO w_mae_ctb.*

    # Iniciando reporte
    IF NOT existe THEN
       # Creando tabla temporal para separar transacciones del periodo
       CREATE TEMP TABLE cb_saldocta
        (empresa     CHAR(2),
         cuenta      CHAR(16),
         debe        DECIMAL(14,2),
         haber       DECIMAL(14,2),
         operacion   CHAR(01)) WITH NO LOG
        CREATE INDEX ix_001ss ON cb_saldocta(empresa,cuenta)

       # Creando tabla temporal para sumas
       CREATE TEMP TABLE cb_sumasrep
       (empresa   CHAR(2),
        cuenta    CHAR(16),
        saldo_ant DECIMAL(11,2),
        saldo_act DECIMAL(13,2)) WITH NO LOG
        CREATE INDEX ix_001s1 ON cb_saldocta(empresa) 

       # Seleccionando fonts
       CALL fonts(impress) RETURNING fnt.* 

       LET existe = TRUE
       START REPORT re068_ibalgen TO filename
       ERROR "Atencion: seleccionando datos ... por favor espere ..."
    END IF 

    # Buscando ultimo cierre de la empresa de la cuenta
    INITIALIZE w_ult_cie.* TO NULL
    CALL dlast_cierre("CB",1,w_mae_ctb.empresa,w_femision)
    RETURNING w_ult_cie.*,exis

    # Verificando si existe cierre
    IF NOT exis THEN
       LET w_mae_ctb.saldo_ant = w_mae_ctb.saldo_ini
       LET w_mae_ctb.cargos_a  = 0
       LET w_mae_ctb.abonos_a  = 0
       LET w_inimes = 
       MDY(MONTH(w_mae_ctb.f_apertura),1,YEAR(w_mae_ctb.f_apertura))
       LET w_iniper = NULL 
    ELSE
       IF (w_ult_cie.default = "S") THEN 
          LET w_mae_ctb.saldo_ant = w_mae_ctb.saldo_ini
          LET w_mae_ctb.cargos_a  = 0
          LET w_mae_ctb.abonos_a  = 0
          LET w_inimes = 
          MDY(MONTH(w_mae_ctb.f_apertura),1,YEAR(w_mae_ctb.f_apertura))
          LET w_iniper = NULL 
       ELSE
          LET w_inimes=(w_ult_cie.f_cierre+1)
          LET w_iniper=w_inimes 

          # Buscando saldos del ultimo cierre
          INITIALIZE w_ult_cta.* TO NULL
          CALL last_cierrecta(w_mae_ctb.empresa,w_mae_ctb.cuenta,
                              w_ult_cie.f_cierre,"M")
          RETURNING w_ult_cta.*,exis

          LET w_mae_ctb.saldo_ant  = w_ult_cta.saldo_mes 
          LET w_mae_ctb.saldo_eje  = w_ult_cta.saldo_eje
       END IF 
    END IF 

    # Separando las transacciones del periodo
    IF w_separa THEN

       # Preparando seleccion

       INITIALIZE w_ctb_mae.* TO NULL
       PREPARE c_txt FROM w_texto
       DECLARE c_ctas CURSOR FOR c_txt
       FOREACH c_ctas INTO w_ctb_mae.*

        # Busca transacciones desglodas
        DECLARE c_ctransper CURSOR FOR
         SELECT d.*
          FROM  cb_ctransac d
          WHERE (d.empresa  = w_ctb_mae.empresa)
            AND (d.cuenta   = w_ctb_mae.cuenta)
            AND (d.f_emision>=w_inimes)
            AND (d.f_emision<=w_femision)
          FOREACH c_ctransper INTO w_mae_ctr.*
     
           SELECT a.*
            INTO  w_mae_tra.*
            FROM  cb_mtransac a
            WHERE (a.link = w_mae_ctr.link)
            IF (STATUS=NOTFOUND) THEN
               CONTINUE FOREACH
            END IF
      
            # Busca tipo de transaccion
            CALL busca_tiptrncb(w_mae_tra.tipo_trns) 
            RETURNING w_mae_trn.*,existe
            IF (w_mae_trn.funcion MATCHES "[RC]") THEN 
               CONTINUE FOREACH 
            END IF 
    
            MESSAGE "Procesando cuenta ",w_mae_ctr.cuenta
    
            INITIALIZE w_salcta.* TO NULL
            SELECT c.*
             INTO  w_salcta.*
             FROM  cb_saldocta c
             WHERE (c.empresa   = w_mae_ctr.empresa)
               AND (c.cuenta    = w_mae_ctr.cuenta)
               AND (c.operacion = w_mae_ctr.operacion)
    
             IF (STATUS=NOTFOUND) THEN
                 INSERT INTO cb_saldocta
                  VALUES(w_mae_ctr.empresa,w_mae_ctr.cuenta,w_mae_ctr.debe,
                         w_mae_ctr.haber,w_mae_ctr.operacion)
             ELSE 
                 LET w_salcta.debe = (w_salcta.debe +w_mae_ctr.debe)
                 LET w_salcta.haber= (w_salcta.haber+w_mae_ctr.haber)
                 IF (w_mae_ctr.operacion="D") THEN
                     UPDATE cb_saldocta
                      SET   cb_saldocta.debe = w_salcta.debe
                     WHERE (cb_saldocta.empresa   = w_mae_ctr.empresa)
                       AND (cb_saldocta.cuenta    = w_mae_ctr.cuenta)
                       AND (cb_saldocta.operacion = w_mae_ctr.operacion)
                 ELSE 
                     UPDATE cb_saldocta
                      SET   cb_saldocta.haber = w_salcta.haber
                     WHERE (cb_saldocta.empresa   = w_mae_ctr.empresa)
                       AND (cb_saldocta.cuenta    = w_mae_ctr.cuenta)
                       AND (cb_saldocta.operacion = w_mae_ctr.operacion)
                 END IF 
             END IF 
          END FOREACH 
          CLOSE c_ctransper 
          FREE  c_ctransper 

          # Busca empresa
          INITIALIZE w_mae_ctr.*,w_mae_trn.*,w_salcta.* TO NULL
   
       END FOREACH 
       CLOSE c_ctas 
       FREE  c_ctas 
       LET w_separa = FALSE
    END IF 


    # Verificando datos
    IF w_mae_ctb.saldo_ant IS NULL
       THEN LET w_mae_ctb.saldo_ant = 0
    END IF 
    IF w_mae_ctb.saldo_eje IS NULL
       THEN LET w_mae_ctb.saldo_eje = 0
    END IF 

    # Encontrando saldo al dia
    CALL saldo_ctasemp(w_mae_ctb.empresa,w_mae_ctb.cuenta,w_mae_ctb.tipo_saldo,
                       w_mae_ctb.saldo_ant,w_inimes,w_femision)
    RETURNING w_mae_ctb.saldo_act,w_mae_ctb.cargos,w_mae_ctb.abonos 

    # Verificando datos
    IF w_mae_ctb.saldo_act IS NULL
       THEN LET w_mae_ctb.saldo_act = 0
    END IF 
    IF w_mae_ctb.cargos IS NULL
       THEN LET w_mae_ctb.cargos = 0
    END IF 
    IF w_mae_ctb.abonos IS NULL
       THEN LET w_mae_ctb.abonos = 0
    END IF 

    # Cheque que no aparezcan los que tiene saldo cero
    IF (w_mae_ctb.saldo_ant=0) AND
       (w_mae_ctb.saldo_act=0) THEN
       CONTINUE FOREACH
    END IF 
       
    # Llenando tabla temporal
    IF (w_mae_ctb.nivel=1) THEN
       INSERT INTO cb_sumasrep
       VALUES (w_mae_ctb.empresa,w_mae_ctb.cuenta,
               w_mae_ctb.saldo_ant,w_mae_ctb.saldo_act)
    END IF 

    # Llenando el reporte
    OUTPUT TO REPORT re068_ibalgen(w_mae_ctb.*)
   END FOREACH
   IF existe THEN
      # Finalizando el reporte
      FINISH REPORT re068_ibalgen 

      # Borrando la tabla temporal
      DROP TABLE cb_saldocta
      DROP TABLE cb_sumasrep

      # Imprimiendo el reporte
      CALL to_printer(filename,impress,"132") 
   ELSE
      # Notificar que no existen datos
      CALL msgbusqueda()
   END IF 
  END WHILE
 CLOSE WINDOW wre068
 CLOSE WINDOW wmsgup3 
END FUNCTION 

{ Subrutina para ingresar la fecha de emision del reporte }    

FUNCTION re068_ingfecha()
 DEFINE regreso,lastkey SMALLINT

 # Ingresando fecha
 LET regreso    = FALSE
 LET w_femision = TODAY 
 DISPLAY BY NAME w_femision 
 INPUT BY NAME w_femision WITHOUT DEFAULTS
  ON KEY(F4)
   # Salida 
   LET regreso = TRUE
   EXIT INPUT 
  ON KEY(INTERRUPT)
   # Atrapandp errores
   ERROR "Tecla Invalida."
  AFTER FIELD w_femision
   LET lastkey = FGL_LASTKEY()
   IF (lastkey = FGL_KEYVAL("up")) OR (lastkey = FGL_KEYVAL("left")) THEN
      LET regreso = TRUE 
      EXIT INPUT
   END IF

   IF w_femision IS NULL THEN
      LET w_femision = TODAY
   END IF 
   DISPLAY BY NAME w_femision 
 END INPUT 
 RETURN regreso
END FUNCTION 

{ Subrutina para imprimir el reporte }

REPORT re068_ibalgen(imp1)
 DEFINE imp1           RECORD LIKE cb_mcuentas.*,
        w_sant,wstant  LIKE cb_mcuentas.saldo_act,
        w_sact,wstact  LIKE cb_mcuentas.saldo_act,
        w_ctas         LIKE cb_mcuentas.cuenta,   
        exis,col1,col  SMALLINT,
        col2,col3,cont SMALLINT,
        linea          CHAR(152)

  OUTPUT LEFT   MARGIN 0 
         PAGE   LENGTH 88
         TOP    MARGIN 0 
         BOTTOM MARGIN 8 

  FORMAT 
  PAGE HEADER
   # Definiendo una linea
   LET linea = "-------------------------------------------------------------",
               "-------------------------------------------------------------",
               "------------------------------"

   PRINT fnt.ini CLIPPED,fnt.t88 CLIPPED,fnt.cmp CLIPPED,fnt.c12 CLIPPED
   PRINT COLUMN   1,"Contabilidad",
         COLUMN  55,fnt.twd CLIPPED,"BALANCE DE SALDOS",fnt.fwd CLIPPED,
         COLUMN 119,PAGENO USING "Pagina: <<"

   LET col3 = centrado(142,w_periodo1)
   PRINT COLUMN    1,"Ctbre068",
         COLUMN col1,w_periodo1 CLIPPED,
         COLUMN  134,"Fecha : ",TODAY USING "dd/mmm/yyyy" 
   PRINT COLUMN  134,"Hora  : ",TIME 

   # Imprimiendo encabezado
   PRINT linea 
   PRINT COLUMN  1,"Cuenta               Nombre cuenta",
         COLUMN 84,"Saldo Mes Anterior"
   PRINT linea 

  BEFORE GROUP OF imp1.empresa
   SKIP TO TOP OF PAGE
   
   # Buscando datos de la empresa
   INITIALIZE w_mae_emp.* TO NULL
   CALL busca_empresa(imp1.empresa)
   RETURNING w_mae_emp.*,exis
   
   # Verifica si es de consolidado
   IF w_mae_emp.consolida MATCHES "[Ss]" THEN
      PRINT fnt.twd CLIPPED,
            imp1.empresa CLIPPED," ",w_mae_emp.nombre,"C O N S O L I D A D O",
            fnt.fwd CLIPPED
   ELSE
      PRINT fnt.twd CLIPPED,
            imp1.empresa CLIPPED," ",w_mae_emp.nombre,
            fnt.fwd CLIPPED
   END IF
   SKIP 1 LINES

  ON EVERY ROW
   # Colocando en casacada los nombres de las cuentas
   LET col1 = (20+imp1.nivel)
   LET col2 = (86+(10*imp1.nivel))

   PRINT COLUMN    1,imp1.cuenta,
         COLUMN col1,imp1.nombre[1,50],
         COLUMN   86,imp1.saldo_ant USING "-,---,---,--&.&&",
         COLUMN col2,imp1.saldo_act USING "-,---,---,--&.&&"

  AFTER GROUP OF imp1.empresa  
   # Hallando maximo nivel de cuentas por empresa y sumando cuentas
   DECLARE c_sumas CURSOR FOR
   SELECT c.cuenta,SUM(c.saldo_ant),SUM(c.saldo_act)
    FROM  cb_sumasrep c
    WHERE (c.empresa = imp1.empresa) 
    GROUP BY 1 
    ORDER BY 1

    LET wstant = 0
    LET wstact = 0
    LET cont   = 0
    FOREACH c_sumas INTO w_ctas,w_sant,w_sact
     IF w_sant IS NULL THEN 
        LET w_sant = 0
     END IF
     IF w_sact IS NULL THEN
        LET w_sact = 0
     END IF

     IF (cont=0) THEN
        LET wstant = w_sant
        LET wstact = w_sact
     ELSE
        LET wstant = (wstant-w_sant)
        LET wstact = (wstact-w_sact)
     END IF 
     LET cont = (cont+1)
    END FOREACH
 
   # Totalizando empresa
   SKIP 1 LINES

   PRINT COLUMN   1,fnt.tbl CLIPPED,"UTILIDAD -->", 
         COLUMN  88,(wstant) USING "-,---,---,--&.&&",
         COLUMN 100,(wstact) USING "-,---,---,--&.&&",
                    fnt.fbl CLIPPED 

   ON LAST ROW
   # ======== Imprimiendo los criterios de seleccion =======================
   LET w_crit = 0 
   SKIP 5 LINES
   LET col = centrado(152,"CRITERIOS DE BUSQUEDA DEL REPORTE")
   PRINT COLUMN col, "CRITERIOS DE BUSQUEDA DEL REPORTE"
   PRINT COLUMN col, "---------------------------------"
   SKIP 1 LINE
   PRINT COLUMN 005, "Los criterios ingresados son:"
   SKIP 1 LINE

   WHILE TRUE
        LET w_crit = w_crit + 1 
        CASE
           WHEN w_crit = 1 AND wc_empresa  IS NOT NULL AND wc_empresa  != " " 
                PRINT COLUMN 005, "Numero Empresa  : ",
                      COLUMN 021, wc_empresa   CLIPPED
           WHEN w_crit = 2 AND wc_cuenta   IS NOT NULL AND wc_cuenta   != " " 
                PRINT COLUMN 005, "Numero Cuenta   : ",
                      COLUMN 021, wc_cuenta    CLIPPED
           WHEN w_crit = 3 AND wc_nombre   IS NOT NULL  AND wc_nombre    != " " 
                PRINT COLUMN 005, "Nombre          : ",
                      COLUMN 021, wc_nombre    CLIPPED
           WHEN w_crit = 4 AND wc_nivel    IS NOT NULL  AND wc_nivel     != " " 
                PRINT COLUMN 005, "Nivel           : ",
                      COLUMN 021, wc_nivel     CLIPPED
           WHEN w_crit = 5 AND wc_periodo IS NOT NULL  AND wc_periodo != " " 
                PRINT COLUMN 005, "Periodo         : ",
                      COLUMN 021, wc_periodo     CLIPPED
           WHEN w_crit = 6 EXIT WHILE
        END CASE
   END WHILE

   SKIP 3 LINES
   PRINT COLUMN 005, fnt.tbl CLIPPED, "Simbologia : ",fnt.fbl CLIPPED,
         COLUMN 029, "*  Todos",
         COLUMN 049, "!= Diferente a",
         COLUMN 069, "<= Menor igual a",
         COLUMN 089, ">= Mayor igual a "
   PRINT COLUMN 025, "|  Ambos",
         COLUMN 045, ":  Rango",
         COLUMN 065, "=  Igual a",
         COLUMN 085, "?  Equivalente"

END REPORT
