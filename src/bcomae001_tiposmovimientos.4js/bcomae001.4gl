{
Mantenimiento de tipos de movimiento
bcomae001.4gl 
}

-- Definicion de variables globales 

GLOBALS "bcoglo001.4gl"

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 IF isGDC() THEN 
   CALL ui.Interface.loadStyles("styles")
END IF 
 CALL ui.Interface.loadToolbar("toolbar3")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("tiposmovimiento1")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo usuario del sistema
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de principal 
 CALL bcomae001_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION bcomae001_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255),
        existe   SMALLINT, 
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "bcomae001a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header(progname,wpais,1) 
  CALL librut001_dpelement("labela","Accesos a Usuarios") 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Tipos de Movimiento"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,4,username) THEN
       HIDE OPTION "Buscar"
    END IF
    --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Modificar"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de tipos de movimiento existentes."
    CALL bcoqbe001_tipomovs(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo tipo de movimiento."
    LET savedata = bcomae001_tipomovs(1) 
   COMMAND "Modificar"
    " Modificacion de un tipo de movimiento existente."
    CALL bcoqbe001_tipomovs(2) 
   COMMAND "Borrar"
    " Eliminacion de un tipo de movimiento existente."
    CALL bcoqbe001_tipomovs(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION bcomae001_tipomovs(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL bcoqbe001_EstadoMenu(4,"") 
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL bcomae001_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nommov, 
                w_mae_pro.nomabr,
                w_mae_pro.tipope,
                w_mae_pro.pagpro,
                w_mae_pro.polctb, 
                w_mae_pro.impdoc,
                w_mae_pro.ctaref
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   
BEFORE INPUT
    -- Verificando integridad
    -- Si tipo de movimiento ya tiene movimientos no pueden modificarse los 
    -- campos de nombre, tipo de operacion
    IF (operacion=2) THEN 
      -- Verificando integridad
      IF bcoqbe001_integridad() THEN
         -- Deshabilitando campos
         CALL Dialog.SetFieldActive("tipope",FALSE) 
      ELSE 
         -- Habilitando campos
         CALL Dialog.SetFieldActive("tipope",TRUE) 
      END IF
    END IF 

   AFTER FIELD nommov  
    --Verificando nombre del tipo de movimiento 
    IF (LENGTH(w_mae_pro.nommov)=0) THEN
       ERROR "Error: nombre del tipo de movimiento invalido, VERIFICA."
       LET w_mae_pro.nommov = NULL
       NEXT FIELD nommov  
    END IF

    -- Verificando que no exista otro tipo de movimiento con el mismo nombre
    SELECT UNIQUE (a.tipmov)
     FROM  bco_tipomovs a
     WHERE (a.tipmov != w_mae_pro.tipmov) 
       AND (a.nommov  = w_mae_pro.nommov) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion",
        " Existe otro tipo de movimiento con el mismo nombre, VERIFICA.",
        "information")
        NEXT FIELD nommov
     END IF 

   AFTER FIELD nomabr  
    --Verificando nombre abreviado
    IF (LENGTH(w_mae_pro.nomabr)=0) THEN
       ERROR "Error: nombre abreviado del tipo de movimiento invalido, VERIFICA."
       LET w_mae_pro.nomabr = NULL
       NEXT FIELD nomabr  
    END IF

   AFTER FIELD tipope  
    --Verificando tipo de operacion
    IF (w_mae_pro.tipope IS NULL) THEN
       ERROR "Error: tipo de operacion invalida, VERIFICA." 
       NEXT FIELD tipope  
    END IF

   AFTER FIELD pagpro 
    --Verificando pago a proveedores 
    IF (w_mae_pro.pagpro IS NULL) THEN
       ERROR "Error: pago a proveedores invalido, VERIFICA." 
       NEXT FIELD pagpro  
    END IF

   AFTER FIELD polctb 
    --Verificando poliza contable
    IF (w_mae_pro.polctb IS NULL) THEN
       ERROR "Error: poliza contable invalida, VERIFICA." 
       NEXT FIELD polctb  
    END IF

   AFTER FIELD impdoc
    --Verificando responsables 
    IF (w_mae_pro.impdoc IS NULL) THEN
       ERROR "Error: imprime responsables invalido, VERIFICA." 
       NEXT FIELD impdoc  
    END IF

   AFTER FIELD ctaref
    --Verificando responsables 
    IF (w_mae_pro.ctaref IS NULL) THEN
       ERROR "Error: cuenta de referencia INVALIDA, VERIFICA." 
       NEXT FIELD ctaref  
    END IF
   
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL bcomae001_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando tipo de movimiento
    CALL bcomae001_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL bcoqbe001_EstadoMenu(0,"") 
    CALL bcomae001_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un tipo de movimiento

FUNCTION bcomae001_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando tipo de movimiento ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.tipmov),0)
    INTO  w_mae_pro.tipmov
    FROM  bco_tipomovs a
        IF (w_mae_pro.tipmov IS NULL) THEN
           LET w_mae_pro.tipmov = 1
       	ELSE
            LET w_mae_pro.tipmov = (w_mae_pro.tipmov+1)
    END IF 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO bco_tipomovs   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.tipmov 

   --Asignando el mensaje 
   LET msg = "Tipo de Movimiento (",w_mae_pro.tipmov USING "<<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE bco_tipomovs
   SET    bco_tipomovs.*      = w_mae_pro.*
   WHERE  bco_tipomovs.tipmov = w_mae_pro.tipmov 

   --Asignando el mensaje 
   LET msg = "Tipo de Movimiento (",w_mae_pro.tipmov USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando
   DELETE FROM bco_tipomovs 
   WHERE (bco_tipomovs.tipmov = w_mae_pro.tipmov)

   --Asignando el mensaje 
   LET msg = "Tipo de Movimiento (",w_mae_pro.tipmov USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL bcomae001_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION bcomae001_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL 
   LET w_mae_pro.tipmov = 0  
   LET w_mae_pro.pagpro = 0  
   LET w_mae_pro.impdoc = 0  
   LET w_mae_pro.polctb = 1  
   LET w_mae_pro.ctaref = 0  
   LET w_mae_pro.userid = username 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.tipmov THRU w_mae_pro.ctaref 
 DISPLAY BY NAME w_mae_pro.tipmov,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION
