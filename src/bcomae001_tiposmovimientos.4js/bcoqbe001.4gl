{
bcoqbe001.4gl 
Mantenimiento de tipos de movimiento
}

{ Definicion de variables globales }

GLOBALS "bcoglo001.4gl" 
DEFINE totlin,totusr INT,
       w             ui.Window,
       f             ui.Form

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION bcoqbe001_tipomovs(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,
        msg                 STRING

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL bcoqbe001_EstadoMenu(operacion," ") 
   INITIALIZE qrytext,qrypart TO NULL
   CALL bcomae001_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.tipmov,a.nommov,a.nomabr,a.tipope,
                                a.pagpro,a.polctb,a.impdoc,a.ctaref,
                                a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel       
     -- Salida
     CALL bcomae001_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = 
     " SELECT UNIQUE a.tipmov,a.nommov ",
     "  FROM  bco_tipomovs a ",
     "  WHERE ", qrytext CLIPPED,
     "  ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_tipomovs SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_tipomovs.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_tipomovs INTO v_tipomovs[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_tipomovs
   FREE  c_tipomovs
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL bcoqbe001_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_tipomovs TO s_tipomovs.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel  
      -- Salida
      CALL bcomae001_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar 
      -- Buscar 
      CALL bcomae001_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      LET res = bcomae001_tipomovs(2) 
      -- Desplegando datos
      CALL bcoqbe001_datos(v_tipomovs[ARR_CURR()].ttipmov)
      CALL bcoqbe001_permxusr(v_tipomovs[ARR_CURR()].ttipmov,1)

     ON KEY (CONTROL-M)
      -- Modificando 
      IF (operacion=2) THEN 
         -- Modificando 
         LET res = bcomae001_tipomovs(2) 
         -- Desplegando datos
         CALL bcoqbe001_datos(v_tipomovs[ARR_CURR()].ttipmov)
         CALL bcoqbe001_permxusr(v_tipomovs[ARR_CURR()].ttipmov,1)
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF bcoqbe001_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Este tipo de movimiento ya tiene registros. "||
         "\nTipo de movimiento no puede borrarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de Borrar este tipo de movimiento ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL bcomae001_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION accesos
      IF (totusr=0) THEN 
         ERROR "Atencion: no existen usuarios con accesos autorizados." 
      END IF 

      -- Desplegando accesos x usuario
      CALL bcoqbe001_detpermxusr(v_tipomovs[ARR_CURR()].ttipmov)

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      
      LET arrcols[1] = "Tipo Movimiento"
      LET arrcols[2] = "Nombre"
      LET arrcols[3] = "Nombre Abreviado"
      LET arrcols[4] = "Tipo Operacion"
      LET arrcols[5] = "Pago Proveedores"
      LET arrcols[6] = "Poliza Contable"
      LET arrcols[7] = "Imprime Documentos"
      LET arrcols[8] = "Cuenta Referencia"
      LET arrcols[9] = "Usuario Registro"
      LET arrcols[10]= "Fecha Registro"
      LET arrcols[11]= "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry = 
        "SELECT a.tipmov,a.nommov,a.nomabr,", 
               "CASE (a.tipope) WHEN 1 THEN 'CARGO' WHEN 0 THEN 'ABONO' END,",
               "CASE (a.pagpro) WHEN 1 THEN 'SI' WHEN 0 THEN 'NO' END,", 
               "CASE (a.polctb) WHEN 1 THEN 'SI' WHEN 0 THEN 'NO' END,", 
               "CASE (a.impdoc) WHEN 1 THEN 'SI' WHEN 0 THEN 'NO' END,",
               "CASE (a.ctaref) WHEN 1 THEN 'SI' WHEN 0 THEN 'NO' END,",
              "a.userid,a.fecsis,a.horsis",  
        " FROM bco_tipomovs a",
        " WHERE ",qrytext CLIPPED,
        " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res=librut002_excelreport("Tipos de Movimiento Bancario",qry,11,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a opcion de accesos 
         IF NOT seclib001_accesos(progname,6,username) THEN
            CALL DIALOG.setActionActive("accesos",FALSE)
         ELSE
            CALL DIALOG.setActionActive("accesos",TRUE)
         END IF

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
	 CALL DIALOG.setActionActive("accesos",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
	 CALL DIALOG.setActionActive("accesos",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

      -- Escondiendo campo de seleccion de accesos de usuario
      CALL f.setFieldHidden("tcheckb",1)

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
         CALL bcoqbe001_datos(v_tipomovs[ARR_CURR()].ttipmov)
         CALL bcoqbe001_permxusr(v_tipomovs[ARR_CURR()].ttipmov,1)
      END IF 

    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen tipos de movimiento con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Desplegando estado del menu
  CALL bcoqbe001_EstadoMenu(0,"") 
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION bcoqbe001_datos(wtipmov)
 DEFINE wtipmov LIKE bco_tipomovs.tipmov,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  bco_tipomovs a "||
              "WHERE a.tipmov = "||wtipmov||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_tipomovst SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_tipomovst INTO w_mae_pro.*
 END FOREACH
 CLOSE c_tipomovst
 FREE  c_tipomovst

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nommov THRU w_mae_pro.ctaref 
 DISPLAY BY NAME w_mae_pro.tipmov,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION 

-- Subrutina para buscar los accesos a usuarios

FUNCTION bcoqbe001_permxusr(wtipmov,opcion)
 DEFINE wtipmov   LIKE bco_tipomovs.tipmov,
        i,opcion  SMALLINT,
        qrytext   STRING 

 -- Inicializando vector de datos
 CALL v_permxusr.clear()

 -- Seleccionando datos
 CASE (opcion)
  WHEN 1 -- Consulta de usuarios con acceso
   LET qrytext = "SELECT 1,a.userid,b.nomusr,a.usuaid,a.fecsis,a.horsis "||
                  "FROM  bco_permxtmv a, glb_usuarios b " ||
                  "WHERE a.tipmov = "||wtipmov||
                   " AND b.userid = a.userid "||
                   " ORDER BY a.fecsis DESC " 

  WHEN 2 -- Cargando usuarios para seleccionar acceso
   LET qrytext = "SELECT 0,a.userid,a.nomusr,USER,CURRENT,CURRENT HOUR TO SECOND "||
                  "FROM  glb_usuarios a " ||
                  "WHERE NOT exists (SELECT b.userid FROM bco_permxtmv b "||
                                     "WHERE b.tipmov = "||wtipmov||
                                      " AND b.userid = a.userid) "||
                   "ORDER BY 2" 
 END CASE 
  
 -- Seleccionando datos
 PREPARE cp1 FROM qrytext 
 DECLARE cperm CURSOR FOR cp1 
 LET totusr = 1
 FOREACH cperm INTO v_permxusr[totusr].* 
  LET totusr = (totusr+1) 
 END FOREACH
 CLOSE cperm
 FREE  cperm
 LET totusr = (totusr-1) 

 -- Desplegando datos
 DISPLAY ARRAY v_permxusr TO s_permxusr.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY
END FUNCTION 

-- Subrutina para visualizar el detalle de accesos a usuarios

FUNCTION bcoqbe001_detpermxusr(wtipmov)
 DEFINE wtipmov    LIKE bco_tipomovs.tipmov,
        loop,opc,i SMALLINT

 -- Desplegando usuarios
 LET loop = TRUE
 WHILE loop
  DISPLAY ARRAY v_permxusr TO s_permxusr.*
   ATTRIBUTE(COUNT=totusr,ACCEPT=FALSE,CANCEL=FALSE)
   ON ACTION cancel  
    -- Salida
    LET loop = FALSE
    EXIT DISPLAY 

   ON ACTION agregarusers
    -- Agregando usuarios 
    CALL bcoqbe001_permxusr(wtipmov,2) 
    IF (totusr=0) THEN
        CALL fgl_winmessage(
        " Atencion",
        " No existen mas usuarios para agregar."||
        "\n Todos los usuarios ya tienen acceso.", 
        "stop")

       -- Cargando usuarios con accesos
       CALL bcoqbe001_permxusr(wtipmov,1) 
       EXIT DISPLAY 
    ELSE  
      -- Mostrando campo de seleccion de accesos de usuario
      CALL f.setFieldHidden("tcheckb",0)

      -- Seleccionando usuarios
      INPUT ARRAY v_permxusr WITHOUT DEFAULTS FROM s_permxusr.*
       ATTRIBUTE(MAXCOUNT=totusr,INSERT ROW=FALSE,
                 APPEND ROW=FALSE,DELETE ROW=FALSE,
                 ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
       ON ACTION cancel
        -- Salida
        EXIT INPUT 

       ON ACTION accept
        -- Verificando si existen usuarios que agregar
        IF NOT bcoqbe001_hayusuarios(1) THEN
           ERROR "Atencion: no existen usuarios marcados que agregar."
           CONTINUE INPUT
        END IF

        -- Grabando usuarios seleccionados
        LET opc = librut001_menugraba("Confirmacion de Accesos",
                                      "Que desea hacer ?",
                                      "Guardar",
                                      "Modificar",
                                      "Cancelar",
                                      NULL)

        CASE (opc)
         WHEN 0 -- Cancelando
          EXIT INPUT 
         WHEN 1 -- Grabando
          -- Iniciando la transaccion
          BEGIN WORK 

          -- Guardando accesos
          FOR i = 1 TO totusr
           IF (v_permxusr[i].tcheckb=0) OR
              (v_permxusr[i].tcheckb IS NULL) THEN 
              CONTINUE FOR
           END IF 

           -- Grabando  
           SET LOCK MODE TO WAIT
           INSERT INTO bco_permxtmv 
           VALUES (wtipmov,
                   v_permxusr[i].tuserid, 
                   v_permxusr[i].tusuaid, 
                   v_permxusr[i].tfecsis, 
                   v_permxusr[i].thorsis)
          END FOR

          -- Terminando la transaccion
          COMMIT WORK 

          EXIT INPUT 
         WHEN 2 -- Modificando
          CONTINUE INPUT 
        END CASE

       BEFORE ROW
        -- Verificando control del total de lineas 
        IF (ARR_CURR()>totusr) THEN
           CALL FGL_SET_ARR_CURR(1)
        END IF 
      END INPUT 

      -- Escondiendo campo de seleccion de accesos de usuario
      CALL f.setFieldHidden("tcheckb",1)

      -- Cargando usuarios con accesos
      CALL bcoqbe001_permxusr(wtipmov,1) 
      EXIT DISPLAY 
    END IF 

   ON ACTION eliminarusers
    IF (totusr=0) THEN
       ERROR "Atencion: no existen usuarios con accesos autorizados que borrar."
       EXIT DISPLAY 
    END IF  

    -- Eliminando usuarios 
    -- Mostrando campo de seleccion de accesos de usuario
    CALL f.setFieldHidden("tcheckb",0)

    -- Seleccionando usuarios
    INPUT ARRAY v_permxusr WITHOUT DEFAULTS FROM s_permxusr.*
     ATTRIBUTE(MAXCOUNT=totusr,INSERT ROW=FALSE,
               APPEND ROW=FALSE,DELETE ROW=FALSE,
               ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
     ON ACTION cancel
      -- Salida
      EXIT INPUT 

     ON ACTION accept   
      -- Verificando si existen usuarios que borrar
      IF NOT bcoqbe001_hayusuarios(0) THEN
         ERROR "Atencion: no existen usuarios desmarcados que borrar."
         CONTINUE INPUT
      END IF

      -- Grabando usuarios seleccionados
      LET opc = librut001_menugraba("Confirmacion de Accesos",
                                    "Que desea hacer ?",
                                    "Borrar",
                                    "Modificar",
                                    "Cancelar",
                                    NULL)

      CASE (opc)
       WHEN 0 -- Cancelando
        EXIT INPUT 
       WHEN 1 -- Grabando
        -- Iniciando la transaccion
        BEGIN WORK 

        -- Guardando accesos
        FOR i = 1 TO totusr
         IF (v_permxusr[i].tcheckb=1) OR 
            (v_permxusr[i].tcheckb IS NULL) THEN 
            CONTINUE FOR
         END IF 

         -- Eliminando  
         SET LOCK MODE TO WAIT
         DELETE FROM bco_permxtmv 
         WHERE bco_permxtmv.tipmov = wtipmov
           AND bco_permxtmv.userid = v_permxusr[i].tuserid 
        END FOR

        -- Terminando la transaccion
        COMMIT WORK 

        EXIT INPUT 
       WHEN 2 -- Modificando
        CONTINUE INPUT 
      END CASE

     BEFORE ROW
      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totusr) THEN
         CALL FGL_SET_ARR_CURR(1)
      END IF 
    END INPUT 

    -- Escondiendo campo de seleccion de accesos de usuario
    CALL f.setFieldHidden("tcheckb",1)

    -- Cargando usuarios con accesos
    CALL bcoqbe001_permxusr(wtipmov,1) 
    EXIT DISPLAY 

   BEFORE ROW 
    -- Verificando control del total de lineas 
    IF (ARR_CURR()>totusr) THEN
       CALL FGL_SET_ARR_CURR(1)
    END IF 
  END DISPLAY 
 END WHILE 
END FUNCTION 

-- Subrutina para verificar si existen usuarios seleccionados que agregar o que borrar

FUNCTION bcoqbe001_hayusuarios(estado)
 DEFINE i,estado,hayusr SMALLINT

 -- Chequeando estado
 LET hayusr = 0
 FOR i = 1 TO totusr
  -- Verificando estado
  IF (v_permxusr[i].tcheckb=estado) THEN
     LET hayusr = 1
     EXIT FOR
  END IF
 END FOR

 RETURN hayusr
END FUNCTION

-- Subrutina para verificar si el tipo de movimiento ya tiene registros

FUNCTION bcoqbe001_integridad()
 DEFINE conteo INTEGER   

 -- Verificando transacciones 
 SELECT COUNT(*)
  INTO  conteo
  FROM  bco_mtransac a 
  WHERE (a.tipmov = w_mae_pro.tipmov) 
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION bcoqbe001_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Tipos de Movimiento - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Tipos de Movimiento - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Tipos de Movimiento - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Tipos de Movimiento - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Tipos de Movimiento - NUEVO")
 END CASE
END FUNCTION
