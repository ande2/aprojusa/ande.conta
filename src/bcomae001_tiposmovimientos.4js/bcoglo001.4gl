{ 
bcoglo001.4gl
Mantenimiento de tipos de movimiento
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "bcomae001" 
DEFINE w_mae_pro   RECORD LIKE bco_tipomovs.*,
       v_tipomovs  DYNAMIC ARRAY OF RECORD
        ttipmov    LIKE bco_tipomovs.tipmov,
        tnommov    LIKE bco_tipomovs.nommov 
       END RECORD, 
       v_permxusr  DYNAMIC ARRAY OF RECORD
        tcheckb    SMALLINT,
        tuserid    LIKE bco_permxtmv.userid,
        tnomusr    VARCHAR(50), 
        tusuaid    LIKE bco_permxtmv.usuaid,
        tfecsis    LIKE bco_permxtmv.fecsis,
        thorsis    LIKE bco_permxtmv.horsis,
        tendrec    CHAR(1)
       END RECORD,
       username    VARCHAR(15),
       haymov      SMALLINT,
       corinimov   SMALLINT 
END GLOBALS
