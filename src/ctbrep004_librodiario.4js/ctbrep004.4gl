{ 
Programa : ctbrep004.4gl                   
Programo : Mynor Ramirez 
Objetivo : Reporte de diario mayor 
}

DATABASE erpjuridico 

-- Definicion de variables globales 
GLOBALS
TYPE    datosreporte        RECORD 
         lnktra             LIKE ctb_mtransac.lnktra, 
         codemp             LIKE ctb_mtransac.codemp,
         tiptrn             LIKE ctb_mtransac.tiptrn,
         fecemi             LIKE ctb_mtransac.fecemi,
         numdoc             LIKE ctb_mtransac.numdoc,
         totdoc             LIKE ctb_mtransac.totdoc,
         concep             LIKE ctb_mtransac.concep,
         codapl             LIKE ctb_mtransac.codapl,
         lnkapl             LIKE ctb_mtransac.lnkapl,
         estado             LIKE ctb_mtransac.estado,
         coddiv             LIKE ctb_mtransac.coddiv,
         infadi             LIKE ctb_mtransac.infadi,
         userid             LIKE ctb_mtransac.userid,
         fecsis             LIKE ctb_mtransac.fecsis,
         horsis             LIKE ctb_mtransac.horsis
        END RECORD 
CONSTANT progname = "ctbrep004" 
DEFINE  datos               RECORD 
         codemp             LIKE ctb_mtransac.codemp, 
         tiptrn             LIKE ctb_mtransac.tiptrn, 
         fecini             DATE, 
         fecfin             DATE
        END RECORD
DEFINE  p                   RECORD
         length             SMALLINT,
         topmg              SMALLINT,
         botmg              SMALLINT,
         lefmg              SMALLINT,
         rigmg              SMALLINT
        END RECORD
DEFINE  gcodemp             LIKE glb_empresas.codemp 
DEFINE  existe              SMALLINT
DEFINE  haydata             SMALLINT
DEFINE  wnomemp             LIKE glb_empresas.nomemp 
DEFINE  tituloreporte       STRING 
DEFINE  filename            STRING
DEFINE  pipeline            STRING
DEFINE  s,d                 CHAR(1) 
END GLOBALS
-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarReportes1")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rdiariomayor")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL ctbrep004_DiarioMayor()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION ctbrep004_DiarioMayor()
 DEFINE impl     datosreporte,
        wpais    VARCHAR(255),
        qrytxt   STRING,
        qryemp   STRING,
        qrypart  STRING,
        loop     SMALLINT,
        w        ui.Window, 
        f        ui.FORM   

  DEFINE myHandler om.SaxDocumentHandler
        
 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep004a AT 5,2
  WITH FORM "ctbrep004a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/librodiario.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox de empresas
  LET gcodemp = librut003_DCbxEmpresaEnUsoXUsuario()
  IF gcodemp IS NULL THEN
     CALL fgl_winmessage(
     "Atencion","Usuario sin empresa asignada, VERIFICA.","stop")
     CLOSE WINDOW wrep004a 
     RETURN
  END IF

  -- Llenando combo de tipos de transacciones
  CALL librut003_CbxTiposTransaccionesContables()

  -- Inicio del loop
  OPTIONS INPUT WRAP 
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE pipeline,datos.* TO NULL
   LET datos.codemp = gcodemp 
   LET s = 1 SPACE
   CLEAR FORM
   DISPLAY BY NAME datos.codemp

   -- Ingresando datos 
   INPUT BY NAME datos.tiptrn, 
                 datos.fecini, 
                 datos.fecfin
                 WITHOUT DEFAULTS 
    ATTRIBUTE(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT    

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 
     LET p.length = 48  
     LET p.topmg  = 3
     LET p.botmg  = 2
     LET p.lefmg  = 2
     LET p.rigmg  = 2

     -- Verificando ingreso de filtros
     IF NOT ctbrep004_FiltrosCompletos() THEN 
        CONTINUE INPUT 
     END IF 
     EXIT INPUT    

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 
     LET p.length = 48  
     LET p.topmg  = 0
     LET p.botmg  = 0
     LET p.lefmg  = 0
     LET p.rigmg  = 0

     -- Verificando ingreso de filtros
     IF NOT ctbrep004_FiltrosCompletos() THEN 
        CONTINUE INPUT  
     END IF 
     EXIT INPUT    

    ON ACTION excel 
     -- Asignando dispositivo 
     LET pipeline = "excel" 
     LET s = ASCII(9) 
     LET p.length = 48  
     LET p.topmg  = 0
     LET p.botmg  = 0
     LET p.lefmg  = 0
     LET p.rigmg  = 0

     -- Verificando ingreso de filtros
     IF NOT ctbrep004_FiltrosCompletos() THEN 
        CONTINUE INPUT 
     END IF 
     EXIT INPUT   

   END INPUT 
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Obteniendo datos de la empresa
   INITIALIZE wnomemp TO NULL
   SELECT a.nomemp INTO wnomemp FROM glb_empresas a
    WHERE a.codemp = datos.codemp 

   -- Construyendo seleccion 
   LET qrytxt = 
    "SELECT a.lnktra,a.codemp,a.tiptrn,a.fecemi,a.numdoc,a.totdoc,trim(a.concep),",
           "a.codapl,a.lnkapl,a.estado,a.coddiv,a.infadi,a.userid,a.fecsis,",
           "a.horsis ", 
     "FROM  ctb_mtransac a ",
     "WHERE a.codemp = ",datos.codemp,
     "  AND a.tiptrn = "||datos.tiptrn,
     "  AND a.fecemi >= '",datos.fecini,"'",
     "  AND a.fecemi <= '",datos.fecfin,"'",
     " ORDER BY a.codemp,a.fecemi,a.numdoc" 

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep004 FROM qrytxt 
   DECLARE c_crep004 CURSOR FOR c_rep004
   LET haydata = FALSE
   FOREACH c_crep004 INTO impl.* 
    -- Iniciando reporte
    IF NOT haydata THEN
       LET haydata = TRUE

       LET myHandler = gral_reporte("carta","horizontal","PDF",160,"ANDE_Rep_238")
       DISPLAY "Despues del myHandler"
       -- Iniciando reporte 
       START REPORT ctbrep004_ImprimirDatos TO SCREEN -- filename 
    END IF 

    -- Llenando el reporte
    OUTPUT TO REPORT ctbrep004_ImprimirDatos(impl.*)
   END FOREACH
   CLOSE c_crep004 
   FREE  c_crep004 

   IF haydata THEN
      -- Finalizando el reporte
      FINISH REPORT ctbrep004_ImprimirDatos 

      -- Transfiriendo reporte a excel
      IF pipeline = "excel" THEN
         CALL ctbrep004_excel(filename)
      {ELSE
       CALL librut001_sendreport
       (filename,pipeline,tituloreporte, 
        "--noline-numbers "||
        "--nofooter "||
        "--font-size 7 "||
        "--page-width 842 --page-height 595  "||
        "--left-margin 35 --right-margin 15 "||
        "--top-margin 30 --bottom-margin 30 "||
        "--title Contabilidad")
        }
      END IF

      ERROR "" 
      CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(
      " Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF  
  END WHILE

  OPTIONS INPUT NO WRAP 

 CLOSE WINDOW wrep004a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION ctbrep004_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE

 -- Verificando datos 
 IF datos.codemp IS NULL OR
    datos.fecini IS NULL OR 
    datos.fecfin IS NULL THEN 
    CALL fgl_winmessage(
    " Atencion",
    " Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte 

REPORT ctbrep004_ImprimirDatos(imp1)
DEFINE 
   wd                RECORD 
      numcta           LIKE ctb_dtransac.numcta,
      nomcta           LIKE ctb_mcuentas.nomcta,
      concep           CHAR(50), 
      totdeb           LIKE ctb_dtransac.totdeb, 
      tothab           LIKE ctb_dtransac.tothab,
      tipope           LIKE ctb_dtransac.tipope 
   END RECORD, 
   wabonom           DECIMAL(14,2),
   wcargom           DECIMAL(14,2),
   imp1               datosreporte,
   linea             CHAR(150),
   lineat            CHAR(031),
   wtiptrn           CHAR(30), 
   lg                SMALLINT,
   fin,
   ini               SMALLINT, 
   col               INTEGER,
   fechareporte      STRING,
   buf               CHAR(200)

   OUTPUT 
      LEFT   MARGIN 0
      PAGE   LENGTH 96
      TOP    MARGIN 0 
      RIGHT  MARGIN 0 
      BOTTOM MARGIN 0 

   FORMAT 
      PAGE HEADER
         -- Llenando linea
         LET lg = 150 
         LET linea = NULL
         LET linea = llenarLinea ( lg, "-")
         LET lineat = llenarLinea ( 31, "-")

         -- Imprimiendo Encabezado
         LET col = librut001_centrado(wnomemp,lg) 
         PRINT COLUMN   1, "Contabilidad",
               COLUMN col,wnomemp CLIPPED,  
               COLUMN (lg-22),PAGENO USING "Pagina : <<<<."

         LET col = librut001_centrado(tituloreporte,lg) 
         PRINT COLUMN   1, "Ctbrep004",
               COLUMN col, UPSHIFT(tituloreporte) CLIPPED,  
               COLUMN (lg-22),"Fecha  : ",TODAY USING "dd/mmm/yyyy" 

         LET fechareporte = "DEL [",datos.fecini,"] AL [ ",datos.fecfin," ]" 
         LET col = librut001_centrado(fechareporte,lg) 

         PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
               COLUMN col,fechareporte CLIPPED,
               COLUMN (lg-22),"Hora   : ", TIME
          
         SKIP 2 LINES
    
   BEFORE GROUP OF imp1.lnktra  
      NEED 8 LINES
      -- Obteniendo tipo de transaccion
      INITIALIZE wtiptrn TO NULL 
      SELECT   a.nomtrn 
         INTO  wtiptrn 
         FROM  ctb_tipostrn a
         WHERE a.tiptrn = imp1.tiptrn 

         PRINT COLUMN   1,"Poliza #: ",imp1.numdoc,s,
               COLUMN  40,"Fecha Emision: ",imp1.fecemi,s,
               COLUMN  80,"Tipo Transaccion: ",wtiptrn CLIPPED

               
 
         LET ini = 1
         LET fin = 100
         LET buf = imp1.concep CLIPPED
         WHILE ini <= LENGTH(buf)
            IF ini = 1 THEN
               PRINT COLUMN   1,"Concepto: ",
                     COLUMN  11, imp1.concep[ini, fin] CLIPPED 
            ELSE
               PRINT COLUMN  11, imp1.concep[ini, fin] CLIPPED
            END IF
            LET ini = ini + 100
            LET fin = ini + 100
            IF fin > 200 THEN LET fin = 200 END IF
         END WHILE

         PRINT linea
         
         PRINT COLUMN   8, "Cuenta",
               COLUMN  33, "Nombre de la Cuenta",
               COLUMN  88, "Comentario",
               COLUMN 123, "D E B E",
               COLUMN 138, "H A B E R"
   
         PRINT linea

        
      ON EVERY ROW 
         -- Imprimiendo detalle de cuentas 
         LET wcargom = 0
         LET wabonom = 0
         DECLARE ctrans CURSOR FOR
            SELECT   y.numcta,c.nomcta,y.concep,y.totdeb,y.tothab,y.tipope
               FROM  ctb_dtransac y,ctb_mcuentas c 
               WHERE (y.lnktra = imp1.lnktra) 
               AND   (c.codemp = imp1.codemp)
               AND   (c.numcta = y.numcta)  
               ORDER BY y.correl
               
         FOREACH ctrans INTO wd.*
            -- Totalizando debe y haber
            CASE (wd.tipope)
               WHEN "D" -- Debe
                  LET wcargom = (wcargom+wd.totdeb)
               WHEN "H" -- Haber 
                  LET wabonom = (wabonom+wd.tothab)
            END CASE

            -- Imprimiendo movimiento de la cuenta 
            PRINT COLUMN   1,wd.numcta ,
                  COLUMN  24, wd.nomcta[1,50],
                  COLUMN  77, wd.concep[1,40], 
                  COLUMN 120, wd.totdeb USING "-,---,--&.&&",
                  COLUMN 136, wd.tothab USING "-,---,--&.&&"
         END FOREACH
         
         CLOSE ctrans
         FREE  ctrans  

   AFTER GROUP OF imp1.lnktra
            PRINT COLUMN 120, lineat
            
            PRINT COLUMN 104, "T O T A L E S",
                  COLUMN 120, wcargom USING "-,---,--&.&&",
                  COLUMN 136, wabonom USING "-,---,--&.&&"
                  
      SKIP 2 LINES
      
END REPORT 

-- Subrutina para exportar el reporte a excel usando WinCOM 

FUNCTION ctbrep004_excel(filename) 
 DEFINE sb          base.StringBuffer
 DEFINE ch          base.channel
 DEFINE lista       base.StringTokenizer
 DEFINE result      INTEGER
 DEFINE i           INTEGER
 DEFINE xlapp       INTEGER
 DEFINE xlwb        INTEGER
 DEFINE linea       STRING 
 DEFINE lineadep    STRING 
 DEFINE filename    STRING 
 DEFINE columna     STRING 
 DEFINE separador   CHAR(1) 
 DEFINE nuevalinea  CHAR(1) 

 -- Inicializando variables                            
 LET xlapp = -1
 LET xlwb = -1

 -- Creando la instancia de Excel
 CALL ui.Interface.frontCall("WinCOM", "CreateInstance", ['Excel.Application'], [xlapp])
 CALL CheckError(xlapp, xlwb, xlapp)
   
 -- Agregando un libro al documento de excel               
 CALL ui.interface.frontCall("WinCOM", "CallMethod", [xlapp, 'WorkBooks.Add'], [xlwb])
 CALL CheckError(xlapp, xlwb, xlwb)
   
 -- Haciendo visible el libro                 
 CALL excel_set_property(xlapp, xlapp, "Visible","True")
  
 -- Llenando celdas desde el FrontCall 
 -- Construyendo celdas delimitando columnas por TAB (ASCII(9)), 
 -- y filas por CR (ASCII(10)
 -- Copia al clipboard via el Frontcall y luego instruye a excel para pegar el contenido
 -- de la celdas dentro del libro 
 LET sb = base.StringBuffer.create()

 -- Leyendo el archivo que contiene el reporte y copiandolo al clipboard 
 LET ch = base.channel.create()
 call ch.openfile(filename,"r")

 let i = 1 
 let separador  = ascii(9) 
 let nuevalinea = ascii(10)
 while true
  let linea = ch.readLine() 
  if ch.isEof() then exit while end if
  if linea.getindexof(separador,1)>0 then
     CALL sb.append(linea)
     CALL sb.append(nuevalinea)
  end if 
  let i = i+1
 end while
 call ch.close() 

 -- Pegando el contenido de las celdas en el clipboard al libro de excel
 CALL ui.Interface.FrontCall("standard","cbset",sb.toString(),result)
 CALL excel_call_method(xlapp, xlwb, 'ActiveSheet.Paste')
 CALL excel_call_method(xlapp, xlwb, 'Columns("A:Z").EntireColumn.AutoFit')
 --CALL excel_set_property(xlapp, xlwb, excel_column(nc,"Z","NumberFormat"), "0.00")

 -- Liberando informacion del clipboard    
 CALL freeMemory(xlapp, xlwb)
END FUNCTION
