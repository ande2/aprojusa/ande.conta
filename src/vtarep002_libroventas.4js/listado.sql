   SELECT 1,a.codemp,c.nomemp,a.numest,a.feclbc,a.fecemi,a.nserie,
          a.numdoc,a.tipdoc,a.codsoc,a.numnit,trim(a.nomsoc),
          case (a.tipdoc)
           when 'FPC' then 0
           else nvl(a.totdoc,0)
          end,
          case (a.tipcom)
           when 1 then nvl(a.totgra,0)
           else 0
          end,
          case (a.tipcom)
           when 2 then nvl(a.totgra,0)
           else 0
          end,
          case (a.tipcom)
           when 3 then nvl(a.totgra,0)
           else 0
          end,
          (a.totedp+a.totigt),
          case (a.tipdoc)
           when 'FPC' then nvl(a.totdoc,0)
           else 0
          end,
          b.nomcen,
          a.totisv,
          a.lnkcmp 
   FROM cmp_mtransac a,glb_empresas c,glb_cencosto b
   WHERE a.tipmov = 1
     AND a.estado = 1
     AND c.codemp = a.codemp
     and b.codcos = a.codcos

 UNION ALL

   SELECT 2,x.codemp,c.nomemp,x.numest,y.feclbc,y.fecgto,y.nserie,
          y.ndocto,y.tipdoc,y.codsoc,z.numnit,trim(z.nomsoc), 
          case (y.tipdoc)
           when 'FPC' then 0
           else nvl(y.valgto,0)
          end,
          case (y.tipcom)
           when 1 then nvl(y.totgra,0)
           else 0
          end,
          case (y.tipcom)
           when 2 then nvl(y.totgra,0)
           else 0
          end,
          case (y.tipcom)
           when 3 then nvl(y.totgra,0)
           else 0
          end,
          (y.totedp+y.totigt),
          case (y.tipdoc)
           when 'FPC' then nvl(y.valgto,0)
           else 0
          end,
          b.nomcen,
          y.totisv,
          0 
   FROM fac_cajchica x,fac_dcachica y,glb_empresas c,
        glb_cencosto b,glb_sociosng z
   WHERE x.lnkcaj = y.lnkcaj
     and x.estado = 1
     AND c.codemp = x.codemp
     and b.codcos = x.cencos
     and z.codsoc = y.codsoc;

