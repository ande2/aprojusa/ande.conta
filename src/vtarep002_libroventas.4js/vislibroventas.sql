drop view vis_libroventas;

create view vis_libroventas
  (origen,codemp,nomemp,numest,nomest,fecemi,nserie,numdoc,tipdoc,nomdoc,
   estado,codsoc,numnit,nomsoc,bgralo,sgralo,bexelo,sexelo,bgraex,
   sgraex,bexeex,sexeex,totisv,totdoc,tipope)
   as
   SELECT 1,a.codemp,c.nomemp,a.numest,
          trim(d.nomest)||' ('||a.numest||')',a.fecemi,a.nserie,
          a.nfolio,a.tipdoc,"FCE","E",0,a.recpto,trim(a.nomrec),0,
          a.totnet, 0,0,0,0,0,0,a.totisv,a.totdoc,a.tipope
   FROM vta_ldocutec a,glb_empresas c,glb_cencosto b,glb_estabsat d
   WHERE c.codemp = a.codemp
     AND b.codcos = a.codcos
     AND d.numest = a.numest;

grant select on vis_libroventas to public;

