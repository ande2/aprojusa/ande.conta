drop view vis_librocompras;

create view vis_librocompras
  (codemp,nomemp,fecemi,nserie,numdoc,tipdoc,codsoc,numnit,nomsoc,totdoc,totcmp,
   totser,totedp,totigt,totfpc,nomcen,totisv) ad
   SELECT a.codemp,c.nomemp,a.fecemi,a.nserie,a.numdoc,a.tipdoc,a.numnit,a.nomsoc,
          case (a.tipdoc)
           when 'FPC' then 0
           else nvl(a.totdoc,0)
          end,
          case (a.tipcom)
           when 1 then nvl(a.totgra,0)
           else 0
          end,
          case (a.tipcom)
           when 2 then nvl(a.totgra,0)
           else 0
          end,
          a.totedp,a.totigt,
          case (a.tipdoc)
           when 'FPC' then nvl(a.totdoc,0)
           else 0
          end,
          b.nomcen,
          a.totisv
   FROM cmp_mtransac a,glb_empresas c,glb_cencosto b
   WHERE a.tipmov = 1
     AND a.estado = 1
     AND c.codemp = a.codemp
     and b.codcos = a.codcos;

grant select on vis_librocompras to public;
