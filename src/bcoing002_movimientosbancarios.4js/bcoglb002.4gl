{ 
glbglo011.4gl
Mantenimiento de movimientos bancarios 
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "bcoing002"
DEFINE w_mae_pro      RECORD LIKE bco_mtransac.*,
       w_mae_par      RECORD LIKE bco_parammov.*,
       v_movimientos  DYNAMIC ARRAY OF RECORD
        tlnkbco       LIKE bco_mtransac.lnkbco,
        tnumcta       LIKE bco_mtransac.numcta,
        tnommov       CHAR(40),
        tfecemi       LIKE bco_mtransac.fecemi,
        tfeccob       LIKE bco_mtransac.feccob,
        tnumdoc       LIKE bco_mtransac.numdoc,
        ttotdoc       LIKE bco_mtransac.totdoc,
        tendrec       CHAR(1) 
       END RECORD, 
       v_partida      DYNAMIC ARRAY OF RECORD
        cnumcta       LIKE ctb_dtransac.numcta,
        cnomcta       LIKE ctb_mcuentas.nomcta,
        ctotdeb       LIKE ctb_dtransac.totdeb,
        ctothab       LIKE ctb_dtransac.tothab,
        cconcpt       LIKE ctb_dtransac.concep,
        crellen       CHAR(1)
       END RECORD,
       gcodemp        LIKE bco_mtransac.codemp, 
       gcodcos        LIKE bco_mtransac.codcos, 
       username       VARCHAR(15),
       totaldeb       DEC(14,2),
       totalhab       DEC(14,2),
       totlin         INTEGER, 
       totcta         INTEGER
END GLOBALS
