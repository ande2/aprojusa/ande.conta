{
bcoing002.4gl 
Mantenimiento de movimientos bancarios 
}

-- Definicion de variables globales 

GLOBALS "bcoglb002.4gl"
CONSTANT TIPOTRNPOLIZAINGRESO = 101 
CONSTANT TIPOTRNPOLIZAEGRESO  = 102
DEFINE w_mae_cta   RECORD LIKE bco_mcuentas.*,
       w_tip_mov   RECORD LIKE bco_tipomovs.*,
       codigoapl   CHAR(3)

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarMovimientosBancos.4tb") 

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("movimientosbco")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Obteniendo nombre del movimiento 
 LET username = FGL_GETENV("LOGNAME")
 LET codigoapl= "BCO" 

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL bcoing002_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION bcoing002_mainmenu()
 DEFINE gnumcta  LIKE bco_mcuentas.numcta,
        titulo   STRING,
        wpais    VARCHAR(255),
        existe   SMALLINT, 
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing002a AT 5,2
  WITH FORM "bcoing002a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header("bcoing002",wpais,1) 

  -- Cargando combobox de empresas
  LET gcodemp = librut003_DCbxEmpresaEnUsoXUsuario()
  IF gcodemp IS NULL THEN 
     CALL fgl_winmessage(
     "Atencion","Usuario sin empresa asignada, VERIFICA.","stop") 
     CLOSE WINDOW wing002a 
     RETURN 
  END IF 

  -- Cargando cuentas
  LET gnumcta = librut003_DCbxCuentasBanco(gcodemp) 
  IF gnumcta IS NULL THEN 
     CALL fgl_winmessage(
     "Atencion","Empresa sin cuentas de banco asignadas, VERIFICA.","stop") 
     CLOSE WINDOW wing002a 
     RETURN 
  END IF  

  -- Cargando combobox de centros de costo
  LET gcodcos = librut003_DCbxCentrosCosto("codcos")

  -- Menu de opciones
  MENU "Movimientos"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,4,username) THEN
       HIDE OPTION "consultar"
    END IF
    --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "ingresar" 
    END IF
    -- Modificar
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "modificar"
    END IF
    -- Eliminar 
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "delete"
    END IF
    -- Parametros 
    IF NOT seclib001_accesos(progname,5,username) THEN
       HIDE OPTION "parametros"
    END IF

    -- Obteniendo parametros de movimientos de bancos 
    INITIALIZE w_mae_par.* TO NULL
    SELECT a.* INTO w_mae_par.* FROM bco_parammov a
   ON ACTION consultar   
    CALL bcoqbx002_movimientos(1) 
   ON ACTION ingresar  
    LET savedata = bcoing002_movimientos(1) 
   ON ACTION modificar 
    CALL bcoqbx002_movimientos(2) 
   ON ACTION delete   
    CALL bcoqbx002_movimientos(3) 
   ON ACTION parametros
    CALL bcoing002_ParametrosMovimientos() 
   ON ACTION salir  
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing002a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION bcoing002_movimientos(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        conteo            INTEGER, 
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL bcoqbx002_EstadoMenu(4,"") 
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL bcoing002_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.numcta,
                w_mae_pro.tipmov,
                w_mae_pro.ctaref, 
                w_mae_pro.codrub,
                w_mae_pro.codcos,
                w_mae_pro.fecemi,
                w_mae_pro.feccob, 
                w_mae_pro.numdoc,
                w_mae_pro.descrp, 
                w_mae_pro.totdoc,
                w_mae_pro.numesc,
                w_mae_pro.fecesc,
                w_mae_pro.numser,
                w_mae_pro.numfac 
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   BEFORE INPUT
    -- Verificando si operacion es modificacion
    IF (operacion=2) THEN
       -- Obteniendo datos del tipo de movimiento
       INITIALIZE w_tip_mov.* TO NULL
       CALL librut003_BTiposMovimientoBco(w_mae_pro.tipmov)
       RETURNING w_tip_mov.*,existe

       -- Desabilitando campos
       CALL Dialog.SetFieldActive("numcta",0)
       CALL Dialog.SetFieldActive("tipmov",0)
       CALL Dialog.SetFieldActive("feccob",0)

       -- Si tipo de movimiento es pago de proveedores 
       IF (w_tip_mov.pagpro=1) THEN 
          CALL Dialog.SetFieldActive("feccob",1)
          CALL Dialog.SetFieldActive("ctaref",0) 
          CALL Dialog.SetFieldActive("codrub",0) 
          CALL Dialog.SetFieldActive("codcos",0) 
          CALL Dialog.SetFieldActive("fecemi",0) 
          CALL Dialog.SetFieldActive("numdoc",0) 
          CALL Dialog.SetFieldActive("descrp",0) 
          CALL Dialog.SetFieldActive("totdoc",0) 
          CALL Dialog.SetFieldActive("numesc",0) 
          CALL Dialog.SetFieldActive("fecesc",0) 
          CALL Dialog.SetFieldActive("numser",0) 
          CALL Dialog.SetFieldActive("numfac",0) 
       END IF 

       -- Si origen del movimiento es excel 
       IF w_mae_pro.tipori=1 THEN
          CALL Dialog.SetFieldActive("feccob",0)
          CALL Dialog.SetFieldActive("ctaref",0) 
          CALL Dialog.SetFieldActive("codrub",1) 
          CALL Dialog.SetFieldActive("codcos",1) 
          CALL Dialog.SetFieldActive("fecemi",0) 
          CALL Dialog.SetFieldActive("numdoc",0) 
          CALL Dialog.SetFieldActive("descrp",1) 
          CALL Dialog.SetFieldActive("totdoc",0) 
          CALL Dialog.SetFieldActive("numesc",1) 
          CALL Dialog.SetFieldActive("fecesc",1) 
          CALL Dialog.SetFieldActive("numser",1) 
          CALL Dialog.SetFieldActive("numfac",1) 
       END IF 
    ELSE 
       -- Habilitando campos
       CALL Dialog.SetFieldActive("numcta",1)
       CALL Dialog.SetFieldActive("tipmov",1)
       CALL Dialog.SetFieldActive("feccob",0)
    END IF 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   ON CHANGE numcta 
    -- Obteniendo datos de la cuenta
    INITIALIZE w_mae_cta.* TO NULL
    CALL librut003_BCuentaBanco(w_mae_pro.numcta)
    RETURNING w_mae_cta.*,existe
    LET w_mae_pro.codemp = w_mae_cta.codemp 

   AFTER FIELD numcta
    --Verificando cuenta
    IF w_mae_pro.numcta IS NULL THEN
       ERROR "Error: numero de cuenta invalido, VERIFICA."
       NEXT FIELD numcta
    END IF

    -- Obteniendo datos de la cuenta
    INITIALIZE w_mae_cta.* TO NULL
    CALL librut003_BCuentaBanco(w_mae_pro.numcta)
    RETURNING w_mae_cta.*,existe
    LET w_mae_pro.codemp = w_mae_cta.codemp 

   ON CHANGE tipmov 
    -- Obteniendo datos del tipo de movimiento
    INITIALIZE w_tip_mov.* TO NULL
    CALL librut003_BTiposMovimientoBco(w_mae_pro.tipmov)
    RETURNING w_tip_mov.*,existe

   AFTER FIELD tipmov 
    -- Verificando tipo de movimiento
    IF w_mae_pro.tipmov IS NULL THEN
       ERROR "Error: tipo de movimiento invalido, VERIFICA."
       NEXT FIELD tipmov
    END IF 

    -- Obteniendo datos del tipo de movimiento
    INITIALIZE w_tip_mov.* TO NULL
    CALL librut003_BTiposMovimientoBco(w_mae_pro.tipmov)
    RETURNING w_tip_mov.*,existe

   BEFORE FIELD ctaref
    -- Verificando si el tipo de movimiento tiene cuenta de referencia
    IF w_tip_mov.ctaref=1 THEN
       CALL Dialog.SetFieldActive("ctaref",1)
    ELSE 
       CALL Dialog.SetFieldActive("ctaref",0)
       NEXT FIELD codrub 
    END IF

   AFTER FIELD ctaref
    -- Verificando cuenta de referencia 
    IF w_mae_pro.ctaref IS NULL THEN
       ERROR "Error: cuenta de referencia invalida, VERIFICA."
       NEXT FIELD ctaref 
    END IF 

    -- Verificando que cuenta no sea igual a la cuenta principal
    IF (w_mae_pro.ctaref=w_mae_cta.numcta) THEN 
     CALL fgl_winmessage(
     "Atencion",
     "Numero de cuenta y cuenta referencia iguales, VERIFICA."||
     "\nCuentas deben ser diferentes.",
     "stop")
     NEXT FIELD ctaref 
    END IF 

   AFTER FIELD codrub
    -- Verificando tipo de concepto
    IF w_mae_pro.codrub IS NULL THEN
       ERROR "Error: tipo de concepto invalido, VERIFICA."
       NEXT FIELD codrub 
    END IF 

   AFTER FIELD codcos
    -- Verificando centro de costo
    IF w_mae_pro.codcos IS NULL THEN
       ERROR "Error: centro de costo invalido, VERIFICA."
       NEXT FIELD codcos 
    END IF 

   AFTER FIELD fecemi
    -- Verificando fecha de emision
    IF w_mae_pro.fecemi IS NULL OR
       w_mae_pro.fecemi <(TODAY-w_mae_par.diaing) OR 
       w_mae_pro.fecemi >TODAY THEN
       CALL fgl_winmessage(
       "Atencion",
       "Fecha del movimiento invalida, VERIFICA."||
       "\nNo puede ser blancos, mayor que el dia o fuera de dias permitidos ("||
       w_mae_par.diaing||")", 
       "stop")
       LET w_mae_pro.fecemi = NULL 
       CLEAR fecemi
       NEXT FIELD fecemi 
    END IF 

   AFTER FIELD feccob 
    -- Verificando fecha de cobro 
    IF w_mae_pro.feccob IS NULL OR
       w_mae_pro.feccob <w_mae_pro.fecemi THEN
       LET w_mae_pro.feccob = w_mae_pro.fecemi  
       DISPLAY BY NAME w_mae_pro.feccob 
    END IF

   AFTER FIELD numdoc
    -- Verificando numero de documento
    IF LENGTH(w_mae_pro.numdoc)<=0 THEN 
       ERROR "Error: numero de documento invalido, VERIFICA."
       NEXT FIELD numdoc 
    END IF 

   AFTER FIELD descrp
    -- Verificando descrp
    IF LENGTH(w_mae_pro.descrp)<=0 THEN 
       ERROR "Error: descripcion del movimiento invalido, VERIFICA."
       NEXT FIELD descrp
    END IF 

   AFTER FIELD totdoc
    -- Verificando valor
    IF w_mae_pro.totdoc IS NULL OR
       w_mae_pro.totdoc <=0 OR
       w_mae_pro.totdoc >w_mae_par.totval THEN
       CALL fgl_winmessage(
       "Atencion",
       "Valor del movimiento invalido, VERIFICA."||
       "\nTotal debe ser (>0 y <="||w_mae_par.totval||")",
       "stop")
       LET w_mae_pro.totdoc = NULL 
       CLEAR totdoc
       NEXT FIELD totdoc 
    END IF 
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Verificando que movimiento de banco no exista ya registrado 
  SELECT COUNT(*)
   INTO  conteo 
   FROM  bco_mtransac a
   WHERE a.lnkbco != w_mae_pro.lnkbco
     AND a.numcta  = w_mae_pro.numcta
     AND a.tipmov  = w_mae_pro.tipmov 
     AND a.numdoc  = w_mae_pro.numdoc
   IF (conteo>0) THEN
      CALL fgl_winmessage(
      "Atencion",
      "Numero de documento "||w_mae_pro.numdoc CLIPPED||" ya existe registrado."||
      "\nVerifica cuenta bancaria y tipo de movimiento.",
      "information")
      LET retroceso = TRUE
      CONTINUE WHILE   
   END IF 

  -- Si tipo de movimiento graba poliza contable     
  IF (w_tip_mov.polctb=1) THEN 
   -- Ingresando detalle de la poliza
   IF bcoing002_DetallePoliza() THEN
      LET retroceso = TRUE
      CONTINUE WHILE
   END IF

   -- Verificando total del pago y total de la poliza contable
   IF (w_mae_pro.totdoc!=totaldeb) THEN
      CALL fgl_winmessage(
      "Atencion",
      "Valor total a pagar diferente al total de la poliza contable."||
      "\nVerificar totales de la poliza contable.",
      "stop")
      LET retroceso = TRUE
      CONTINUE WHILE
   END IF 
  END IF 

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")
  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL bcoing002_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando 
    CALL bcoing002_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL bcoqbx002_EstadoMenu(0,"") 
    CALL bcoing002_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar 

FUNCTION bcoing002_grabar(operacion)
 DEFINE w_mae_ctb          RECORD LIKE ctb_mtransac.*,
        xtotval            LIKE ctb_mtransac.totdoc,
        xtipnom            LIKE glb_empresas.tipnom,
        operacion,correl,i SMALLINT,
        msg                CHAR(80),
        xnomori            STRING, 
        xtipope            CHAR(1) 

 -- Grabando transaccion
 ERROR "Registrando movimiento ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   LET w_mae_pro.lnkbco = 0 
   LET w_mae_pro.codemp = w_mae_cta.codemp 
   LET w_mae_pro.nomsoc = w_tip_mov.nommov 
   LET w_mae_pro.nomchq = NULL 
   LET w_mae_pro.feccob = w_mae_pro.fecemi 
   LET w_mae_pro.tipope = w_tip_mov.tipope 
   LET w_mae_pro.monext = 0 
   LET w_mae_pro.tascam = 0  
   LET w_mae_pro.lnkcaj = 0 

   -- Verificando tipo de operacion
   CASE (w_mae_pro.tipope)
    WHEN 1 LET w_mae_pro.totsal = w_mae_pro.totdoc 
    WHEN 0 LET w_mae_pro.totsal = (w_mae_pro.totdoc*(-1)) 
   END CASE 

   -- Grabando 
   INSERT INTO bco_mtransac   
   VALUES (w_mae_pro.*)
   LET w_mae_pro.lnkbco = SQLCA.SQLERRD[2] 

   --Asignando el mensaje 
   LET msg = "Movimiento ("||w_mae_pro.lnkbco||") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   UPDATE bco_mtransac
   SET    bco_mtransac.*      = w_mae_pro.*
   WHERE  bco_mtransac.lnkbco = w_mae_pro.lnkbco 

   --Asignando el mensaje 
   LET msg = "Movimiento ("||w_mae_pro.lnkbco||") actualizado."
  WHEN 3 -- Eliminando 
   -- Eliminando       
   DELETE FROM bco_mtransac 
   WHERE (bco_mtransac.lnkbco = w_mae_pro.lnkbco)

   -- Eliminando poliza de la contabilidad 
   DELETE FROM ctb_mtransac
   WHERE ctb_mtransac.codapl = codigoapl 
     AND ctb_mtransac.lnkapl = w_mae_pro.lnkbco 

   -- Asignando el mensaje 
   LET msg = "Movimiento ("||w_mae_pro.lnkbco||") eliminado."
 END CASE

 -- Si operacion es ingreso o modificacion
 IF (operacion=1) OR (operacion=2) THEN 
  -- Si pago no es pago a proveedores
  IF (w_tip_mov.polctb=1) THEN 
    -- Grabando encabezado de la poliza  
    -- Eliminando poliza de la contabilidad antes de grabar
    DELETE FROM ctb_mtransac
    WHERE ctb_mtransac.codapl = codigoapl 
      AND ctb_mtransac.lnkapl = w_mae_pro.lnkbco 

    -- Grabando 
    -- Asignando datos
    LET w_mae_ctb.lnktra = 0
    LET w_mae_ctb.codemp = w_mae_pro.codemp

    -- Obteniendo tipo de nomenclatura de la empresa 
    INITIALIZE xtipnom TO NULL
    SELECT a.tipnom INTO xtipnom FROM glb_empresas a
     WHERE a.codemp = w_mae_ctb.codemp 

    -- Verificando tipo de poliza 
    CASE (w_mae_pro.tipope)
     WHEN 1 LET w_mae_ctb.tiptrn = TIPOTRNPOLIZAINGRESO
     WHEN 0 LET w_mae_ctb.tiptrn = TIPOTRNPOLIZAEGRESO  
    END CASE  

    LET w_mae_ctb.fecemi = w_mae_pro.feccob 
    LET w_mae_ctb.numdoc = librut003_NumeroPolizaAutomatica(w_mae_ctb.codemp,
                                                            w_mae_ctb.tiptrn,
                                                            w_mae_ctb.fecemi)
    LET w_mae_ctb.totdoc = w_mae_pro.totdoc 
    LET w_mae_ctb.concep = w_mae_pro.descrp 
    LET w_mae_ctb.codapl = codigoapl 
    LET w_mae_ctb.lnkapl = w_mae_pro.lnkbco 
    LET w_mae_ctb.coddiv = 1 
    LET w_mae_ctb.estado = 1 

    -- Verificando origen del movimiento
    IF (w_mae_pro.tipori=0) THEN  -- Ingresado
       LET xnomori = "ORIGEN (INGRESADO)" 
    ELSE                          -- Excel 
       LET xnomori = "ORIGEN (EXCEL) CODIGO REFERENCIA ("||w_mae_pro.codref CLIPPED||")" 
    END IF 

    LET w_mae_ctb.infadi = w_tip_mov.nommov CLIPPED," # ("||w_mae_pro.numdoc CLIPPED||
                           ") DE FECHA ("||w_mae_ctb.fecemi||") CUENTA BANCO ("||
                           w_mae_pro.numcta CLIPPED||") "||xnomori CLIPPED

    LET w_mae_ctb.userid = username
    LET w_mae_ctb.fecsis = CURRENT 
    LET w_mae_ctb.horsis = CURRENT HOUR TO SECOND 

    INSERT INTO ctb_mtransac
    VALUES (w_mae_ctb.*) 
    LET w_mae_ctb.lnktra = SQLCA.SQLERRD[2] 

    -- Grabando detalle de cuentas de la poliza contable 

    -- Eliminando poliza de bancos antes de grabar
    DELETE FROM bco_dtpoliza 
    WHERE bco_dtpoliza.lnkbco = w_mae_pro.lnkbco 

    -- Grabando poliza 
    LET correl = 0
    FOR i = 1 TO totcta 
     IF v_partida[i].cnumcta IS NULL THEN
        CONTINUE FOR
     END IF 
     LET correl = correl+1

     -- Verificando debe y haber
     IF (v_partida[i].ctotdeb>0) THEN
        LET xtipope = "D"
        LET xtotval = v_partida[i].ctotdeb 
     ELSE
        LET xtipope = "H"
        LET xtotval = v_partida[i].ctothab  
     END IF

     -- Grabando detalle de la poliza de bancos 
     INSERT INTO bco_dtpoliza 
     VALUES (w_mae_pro.lnkbco    , -- link del encabezado
             correl              , -- correlativo
             v_partida[i].cnumcta, -- numero de cuenta
             v_partida[i].ctotdeb, -- total debe
             v_partida[i].ctothab, -- total haber
             v_partida[i].cconcpt, -- concepto de la linea
             xtipope)              -- tipo de operacion 

     -- Grabando detalle de cuentas de poliza contable 
     INSERT INTO ctb_dtransac
     VALUES (w_mae_ctb.lnktra    , -- link del encabezado
             correl              , -- correlativo
             v_partida[i].cnumcta, -- numero de cuenta
             v_partida[i].cconcpt, -- concepto de la linea
             v_partida[i].ctotdeb, -- total debe
             v_partida[i].ctothab, -- total haber
             xtipope)

     -- Creando cuentas padre
     CALL librut003_CuentasPadre(
             w_mae_ctb.lnktra,
             correl,
             w_mae_ctb.codemp,
             w_mae_ctb.fecemi,
             v_partida[i].cnumcta,
             xtipnom,                        
             xtipope,
             xtotval,
             v_partida[i].ctotdeb,
             v_partida[i].ctothab,
             "N",
             "N")
    END FOR 
  END IF
 END IF 

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage("Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL bcoing002_inival(1)
 END IF 
END FUNCTION

-- Subutina para el ingreso del detalle de cuentas de la poliza contable

FUNCTION bcoing002_DetallePoliza()
 DEFINE w_mae_ctb  RECORD LIKE ctb_mcuentas.*,
        loop,scr   SMALLINT,
        opc,arr,i  SMALLINT, 
        linea      SMALLINT, 
        retroceso  SMALLINT, 
        existe     SMALLINT, 
        regreso    SMALLINT, 
        lastkey    INTEGER,
        msg        STRING 

 -- Iniciando el loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop

  -- Ingresando productos
  INPUT ARRAY v_partida WITHOUT DEFAULTS FROM s_partida.*
   ATTRIBUTE(COUNT=totcta,INSERT ROW=FALSE,ACCEPT=FALSE,CANCEL=FALSE,
             UNBUFFERED=TRUE,FIELD ORDER FORM)

   ON ACTION accept 
    -- Aceptar
    EXIT INPUT

   ON ACTION cancel 
    -- Cancelar
    LET retroceso = TRUE
    LET loop      = FALSE
    EXIT INPUT

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   ON ACTION listacuenta  
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Seleccionado lista de cuentas contables
    CALL librut002_ListaBusqueda("Consulta de Cuentas Contables ",
                            "# Cuenta",
                            "Nombre de la Cuenta",
                            "",
                            "numcta",
                            "nomcta",
                            "numniv", 
                            "ctb_mcuentas",
                            "codemp = "||w_mae_pro.codemp||" AND tipcta='D'",
                            1)
    RETURNING v_partida[arr].cnumcta,v_partida[arr].cnomcta,regreso 
    IF regreso THEN
       NEXT FIELD cnumcta
    ELSE 
       -- Asignando descripcion
       DISPLAY v_partida[arr].cnumcta TO s_partida[scr].cnumcta 
       DISPLAY v_partida[arr].cnomcta TO s_partida[scr].cnomcta 
    END IF  
   
   BEFORE INPUT 
    CALL DIALOG.setActionHidden("append",TRUE)

    -- Totalizando cuentas
    CALL bcoing002_TotalCuentasPoliza() 

   BEFORE ROW
    LET totcta = ARR_COUNT()

   BEFORE FIELD cnumcta 
    -- Habilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",TRUE)

   AFTER FIELD cnumcta 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) THEN 
       NEXT FIELD cnumcta 
    END IF
 
   BEFORE FIELD ctotdeb 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    IF v_partida[arr].cnumcta IS NULL THEN 
     -- Seleccionado lista de cuentas contables
     CALL librut002_ListaBusqueda("Consulta de Cuentas Contables ",
                             "# Cuenta",
                             "Nombre de la Cuenta",
                             "",
                             "numcta",
                             "nomcta",
                             "numniv", 
                             "ctb_mcuentas",
                             "codemp = "||w_mae_pro.codemp||" AND tipcta='D'",
                             1)
     RETURNING v_partida[arr].cnumcta,v_partida[arr].cnomcta,regreso 
     IF regreso THEN
        NEXT FIELD cnumcta
     ELSE 
        -- Asignando descripcion
        DISPLAY v_partida[arr].cnumcta TO s_partida[scr].cnumcta 
        DISPLAY v_partida[arr].cnomcta TO s_partida[scr].cnomcta 
     END IF  
    END IF  

    -- Verificando numero de cuenta 
    IF (LENGTH(v_partida[arr].cnumcta)<=0) THEN 
       CALL fgl_winmessage(
       "Atencion",
       "Numero de cuenta invalida, VERIFICA.",
       "stop")
       INITIALIZE v_partida[arr].cnumcta THRU v_partida[arr].cconcpt TO NULL
       CLEAR s_partida[scr].cnumcta,s_partida[scr].cnomcta, 
             s_partida[scr].ctotdeb,s_partida[scr].ctothab, 
             s_partida[scr].cconcpt 
       NEXT FIELD cnumcta 
    END IF 

    -- Verificando si la cuenta existe   
    INITIALIZE w_mae_ctb.* TO NULL
    CALL librut003_BCuentaContable(w_mae_pro.codemp,v_partida[arr].cnumcta)
    RETURNING w_mae_ctb.*,existe
    IF NOT existe THEN
       CALL fgl_winmessage(
       "Atencion",
       "Numero de cuenta no registrada, VERIFICA.",
       "top")
       INITIALIZE v_partida[arr].cnumcta THRU v_partida[arr].cconcpt TO NULL
       CLEAR s_partida[scr].cnumcta,s_partida[scr].cnomcta, 
             s_partida[scr].ctotdeb,s_partida[scr].ctothab, 
             s_partida[scr].cconcpt
       NEXT FIELD cnumcta 
    END IF 

    -- Asignando descripcion
    LET v_partida[arr].cnomcta = w_mae_ctb.nomcta 
    DISPLAY v_partida[arr].cnomcta TO s_partida[scr].cnomcta 

    -- Verificando que cuenta se de detalle 
    IF w_mae_ctb.tipcta="M" THEN 
       CALL fgl_winmessage(
       "Atencion",
       "Numero de cuenta no es de detalle, VERIFICA.",
       "stop")
       INITIALIZE v_partida[arr].cnumcta THRU v_partida[arr].cconcpt TO NULL
       CLEAR s_partida[scr].cnumcta,s_partida[scr].cnomcta, 
             s_partida[scr].ctotdeb,s_partida[scr].ctothab, 
             s_partida[scr].cconcpt
       NEXT FIELD cnumcta
    END IF 

    -- Deshabilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",FALSE)

    -- Totalizando cuentas
    CALL bcoing002_TotalCuentasPoliza() 

   ON CHANGE ctotdeb 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Totalizando cuentas
    CALL bcoing002_TotalCuentasPoliza() 

   AFTER FIELD ctotdeb
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD ctotdeb 
    END IF

    -- Verificando total del debe
    IF v_partida[arr].ctotdeb IS NULL OR
       (v_partida[arr].ctotdeb <0) OR
       (v_partida[arr].ctotdeb >w_mae_par.totval) THEN 
       LET v_partida[arr].ctotdeb = 0 
    END IF 
    
    -- Totalizando cuentas
    CALL bcoing002_TotalCuentasPoliza() 

   ON CHANGE ctothab 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Totalizando cuentas
    CALL bcoing002_TotalCuentasPoliza() 

   AFTER FIELD ctothab 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD ctothab 
    END IF

    -- Verificando total del haber 
    IF v_partida[arr].ctothab IS NULL OR
       (v_partida[arr].ctothab <0) OR
       (v_partida[arr].ctothab >w_mae_par.totval) THEN 
       LET v_partida[arr].ctothab = 0 
    END IF 

    -- Verificando totales de debe y haber = 0
    IF (v_partida[arr].ctotdeb=0) AND (v_partida[arr].ctothab=0) THEN
       LET msg = "Tota del debe y haber no pueden ser ambos ceros, VERIFICA."
       CALL fgl_winmessage("Atencion",msg,"stop")
       NEXT FIELD ctotdeb 
    END IF

    -- Verificando totales de debe y haber >0 
    IF (v_partida[arr].ctotdeb>0) AND (v_partida[arr].ctothab>0) THEN
       LET msg = "Total del debe y haber no pueden ser ambos mayores a cero, VERIFICA."
       CALL fgl_winmessage("Atencion",msg,"stop")
       NEXT FIELD ctotdeb 
    END IF

    -- Totalizando cuentas
    CALL bcoing002_TotalCuentasPoliza() 

   AFTER FIELD cconcpt 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD cconcpt 
    END IF

   AFTER ROW,INSERT  
    LET totcta = ARR_COUNT()

    -- Totalizando cuentas
    CALL bcoing002_TotalCuentasPoliza() 

   ON ACTION delete
    -- Eliminando linea seleccionada
    LET arr = ARR_CURR() 
    CALL v_partida.deleteElement(arr)

    -- Totalizando cuentas
    CALL bcoing002_TotalCuentasPoliza() 
    NEXT FIELD cnumcta 

   ON ROW CHANGE 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Totalizando cuentas
    CALL bcoing002_TotalCuentasPoliza() 

   AFTER INPUT 
    LET totcta = ARR_COUNT()

    -- Totalizando cuentas
    CALL bcoing002_TotalCuentasPoliza() 
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Desplegando datos 
  DISPLAY ARRAY v_partida TO s_partida.*
   BEFORE DISPLAY 
    EXIT DISPLAY 
  END DISPLAY

  -- Totalizando cuentas
  CALL bcoing002_TotalCuentasPoliza() 

  -- Verificando lineas incompletas 
  LET linea = bcoing002_CuentasPolizaIncompletas()
  IF (linea>0) THEN
     LET msg = "Linea ("||linea||") incompleta. \n VERIFICA."
     CALL fgl_winmessage("Atencion",msg,"stop")
     CONTINUE WHILE
  END IF 

  -- Verificando cuadre de debe y haber 
  IF (totaldeb!=totalhab) OR
     ((totaldeb+totalhab)=0) THEN
     CALL fgl_winmessage(
     "Atencion",
     "Total del debe y haber invalidos."||
     "\nVerificar que ambos totales sean iguales, sin ser ceros.",
     "stop")
     CONTINUE WHILE
  END IF

  LET loop = FALSE 
 END WHILE

 RETURN retroceso
END FUNCTION

-- Subrutina para totalizar el detalle de cuentas de la poliza contable 

FUNCTION bcoing002_TotalCuentasPoliza()
 DEFINE i SMALLINT

 -- Totalizando
 LET totaldeb = 0
 LET totalhab = 0
 FOR i = 1 TO totcta  
  IF v_partida[i].ctotdeb IS NOT NULL THEN
     LET totaldeb = (totaldeb+v_partida[i].ctotdeb)
  END IF
  IF (v_partida[i].ctothab IS NOT NULL) THEN
     LET totalhab = (totalhab+v_partida[i].ctothab)
  END IF
 END FOR
 -- DISPLAY BY NAME totaldeb,totalhab
END FUNCTION

-- Subrutina para verificar si hay cuentas incompletas

FUNCTION bcoing002_CuentasPolizaIncompletas()
 DEFINE i,linea SMALLINT 

 LET linea = 0 
 FOR i = 1 TO totcta 
  IF v_partida[i].cnumcta IS NULL THEN
     CONTINUE FOR
  END IF

  -- Verificando lineas
  IF v_partida[i].cnomcta IS NULL OR
     v_partida[i].ctotdeb IS NULL OR
     v_partida[i].ctothab IS NULL THEN 
     LET linea = i 
     EXIT FOR 
  END IF 
 END FOR

 RETURN linea 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION bcoing002_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.lnkbco = 0 
   LET w_mae_pro.codemp = gcodemp 
   LET w_mae_pro.fecemi = TODAY 
   LET w_mae_pro.feccob = TODAY 
   LET w_mae_pro.codsoc = 0 
   LET w_mae_pro.tipsol = 0 
   LET w_mae_pro.codcos = gcodcos 
   LET w_mae_pro.codaut = 0
   LET w_mae_pro.totdoc = 0 
   LET w_mae_pro.totsal = 0 
   LET w_mae_pro.totpag = 0 
   LET w_mae_pro.estado = 1
   LET w_mae_pro.nofase = 1 
   LET w_mae_pro.reisra = 0
   LET w_mae_pro.reishs = 0 
   LET w_mae_pro.otrdes = 0 
   LET w_mae_pro.lnkori = 0 
   LET w_mae_pro.tipori = 0 
   LET w_mae_pro.userid = username 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CALL bcoing002_inivec()
   CLEAR FORM
 END CASE

 -- Cargando otras cuentas de banco de referencia 
 CALL librut003_CbxOtrasCuentasBanco() 

 -- Cargando tipos de movimiento 
 CALL librut003_CbxTipoMovBcoxUsuario(1,w_mae_pro.userid)

 -- Cargando tipos de concepto 
 CALL librut003_CbxRubrosGasto() 

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codemp,w_mae_pro.fecemi,w_mae_pro.nofase,
                 w_mae_pro.feccob,w_mae_pro.codcos,w_mae_pro.estado,
                 w_mae_pro.tipori
 DISPLAY BY NAME w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION

-- Subrutina para inicializar el vector de trabajo

FUNCTION bcoing002_inivec()
 -- Inicializando poliza contable 
 CALL v_partida.clear() 
 LET totcta = 0 
 DISPLAY ARRAY v_partida TO s_partida.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION  

-- Subrutina para ingresar los parametros de los movimientos bancarios 

FUNCTION bcoing002_ParametrosMovimientos() 
 DEFINE grabar SMALLINT 

 -- Abriendo ventana
 OPEN WINDOW wing001d AT 5,2
  WITH FORM "bcoing002b" ATTRIBUTE(BORDER)

  -- Obteniendo datos
  INITIALIZE w_mae_par.* TO NULL
  SELECT a.* INTO w_mae_par.* FROM bco_parammov a
  DISPLAY BY NAME w_mae_par.* 

  -- Ingresando datos
  OPTIONS INPUT WRAP 
  INPUT BY NAME w_mae_par.totval,
                w_mae_par.diaing,
                w_mae_par.diamod, 
                w_mae_par.diaeli WITHOUT DEFAULTS 
   ATTRIBUTE(UNBUFFERED,FIELD ORDER FORM)

   ON ACTION accept
    -- Aceptar
    LET grabar = TRUE 
    EXIT INPUT

   ON ACTION cancel 
    -- Cancelar
    LET grabar = FALSE 
    EXIT INPUT

   AFTER FIELD totval 
    -- Verificando total
    {IF w_mae_par.totval IS NULL OR 
       w_mae_par.totval<0 OR 
       w_mae_par.totval>9999999 THEN
       LET w_mae_par.totval = 9999999
    END IF} 
    DISPLAY BY NAME w_mae_par.totval 

   AFTER FIELD diaing 
    -- Verificando dias
    IF w_mae_par.diaing IS NULL OR 
       w_mae_par.diaing<0 OR 
       w_mae_par.diaing>360 THEN
       LET w_mae_par.diaing = 30 
    END IF 
    DISPLAY BY NAME w_mae_par.diaing 

   AFTER FIELD diamod 
    -- Verificando dias
    IF w_mae_par.diamod IS NULL OR 
       w_mae_par.diamod<0 OR 
       w_mae_par.diamod>360 THEN
       LET w_mae_par.diamod = 30 
    END IF 
    DISPLAY BY NAME w_mae_par.diamod 

   AFTER FIELD diaeli 
    -- Verificando dias
    IF w_mae_par.diaeli IS NULL OR 
       w_mae_par.diaeli<0 OR 
       w_mae_par.diaeli>360 THEN
       LET w_mae_par.diaeli = 30 
    END IF 
    DISPLAY BY NAME w_mae_par.diaeli 

  END INPUT 

  -- Verificando grabacion 
  IF grabar THEN 
     LET w_mae_par.userid = username 
     LET w_mae_par.fecsis = CURRENT 
     LET w_mae_par.horsis = CURRENT HOUR TO SECOND 

     SELECT a.* FROM bco_parammov a 
     IF (status=NOTFOUND) THEN
      -- Grabando 
      INSERT INTO bco_parammov 
      VALUES (w_mae_par.*) 
     ELSE
      -- Grabando 
      UPDATE bco_parammov 
      SET    bco_parammov.* = w_mae_par.* 
     END IF 
  END IF 
  OPTIONS INPUT NO WRAP 
 CLOSE WINDOW wing001d
END FUNCTION 
