{
bcoqbx002.4gl 
Mantenimiento de movimientos bancarias 
}

{ Definicion de variables globales }

GLOBALS "bcoglb002.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION bcoqbx002_movimientos(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion,arr       SMALLINT,
        qry,cmdstr          STRING,
        msg,strpagpro       STRING,
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Si operacion es eliminacion excluye a los movimientos que pagan proveedores 
  LET strpagpro = NULL
  IF (operacion=3) THEN
     LET strpagpro = " AND y.pagpro = 0" 
  END IF 

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL bcoqbx002_EstadoMenu(operacion," ")
   INITIALIZE qrytext,qrypart TO NULL
   CALL bcoing002_inival(1)
   LET int_flag = 0

   -- Cargando tipos de movimiento
   CALL librut003_CbxTiposMovimientoBco(1)

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.numcta,a.tipmov,a.codrub,a.ctaref,
                                a.codcos,a.fecemi,a.feccob,a.numdoc,
                                a.descrp,a.totdoc,a.numesc,a.fecesc,
                                a.numser,a.numfac,a.codref,a.tipori,
                                a.lnkbco,a.userid,a.fecsis,a.horsis,
                                a.estado,a.nofase
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel 
     -- Salida
     CALL bcoing002_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = "SELECT UNIQUE a.lnkbco,a.numcta,y.nommov,a.fecemi,a.feccob,",
                               "a.numdoc,a.totdoc,'' ",
                 " FROM bco_mtransac a,bco_tipomovs y ",
                 " WHERE a.codemp = ",gcodemp," AND ",qrytext CLIPPED,
                 "   AND y.tipmov = a.tipmov ",
                 strpagpro CLIPPED,
                 " ORDER BY a.lnkbco DESC"

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_movimientos SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_movimientos.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_movimientos INTO v_movimientos[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_movimientos
   FREE  c_movimientos
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL bcoqbx002_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_movimientos TO s_movimientos.*
      ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE,FOCUSONFIELD)

      ON ACTION cancel 
       -- Salida
       CALL bcoing002_inival(1)
       LET loop = FALSE
       EXIT DISPLAY 

      ON ACTION consultar 
       -- Consultar 
       CALL bcoing002_inival(1)
       EXIT DISPLAY 
 
      ON ACTION modificar
       -- Verificando fecha del movimiento 
       IF (w_mae_pro.fecemi<(TODAY-w_mae_par.diamod)) THEN
          CALL fgl_winmessage(
          "Atencion",
          "Movimiento no puede modificarse.\n"||
          "Fecha del movimiento excede limite de dias permitidos ("||w_mae_par.diamod||")",
          "stop")
          CONTINUE DISPLAY 
       END IF

       -- Modificando 
       LET res = bcoing002_movimientos(2) 

       -- Desplegando datos
       CALL bcoqbx002_datos(w_mae_pro.lnkbco)

      ON KEY (CONTROL-M)   
       -- Modificando 
       IF (operacion=2) THEN 
        -- Verificando fecha del movimiento 
        IF (w_mae_pro.fecemi<(TODAY-w_mae_par.diamod)) THEN
           CALL fgl_winmessage(
           "Atencion",
           "Movimiento no puede modificarse.\n"||
           "Fecha del movimiento excede limite de dias permitidos ("||w_mae_par.diamod||")",
           "stop")
           CONTINUE DISPLAY 
        END IF

        -- Modificando 
        LET res = bcoing002_movimientos(2) 
 
        -- Desplegando datos
        CALL bcoqbx002_datos(w_mae_pro.lnkbco)
       END IF 

      ON ACTION delete 
       -- Eliminando 
       -- Verificando si movimiento no es originando por bancos 
       IF (w_mae_pro.tipori>0) THEN
          LET msg = "Este movimiento no puede eliminarse."||
                    "\nMovimiento creado desde otro origen."
          CALL fgl_winmessage("Atencion",msg,"stop")
          CONTINUE DISPLAY 
       END IF 

       -- Verificando fecha del movimiento 
       IF (w_mae_pro.fecemi<(TODAY-w_mae_par.diaeli)) THEN
          CALL fgl_winmessage(
          "Atencion",
          "Movimiento no puede eliminarse.\n"||
          "Fecha del movimiento excede limite de dias permitidos ("||w_mae_par.diaeli||")",
          "stop")
          CONTINUE DISPLAY 
       END IF

       -- Comfirmacion de la accion a ejecutar 
       LET msg = "Esta SEGURO de Eliminar este movimiento ? "
       LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

       -- Verificando operacion
       CASE (opc) 
        WHEN 1
          IF (operacion=3) THEN
              -- Eliminando
              CALL bcoing002_grabar(3)
              EXIT DISPLAY
          END IF 
        WHEN 2
          CONTINUE DISPLAY
       END CASE 

      ON ACTION partida
       -- Ejecutando consulta de la partida contable
       IF ui.Interface.getChildInstances("transaccionesctb")==0 THEN
          LET cmdstr = "fglrun transaccionesctb.42r 1 "||w_mae_pro.lnkbco||" BCO"
          RUN cmdstr WITHOUT WAITING
       ELSE
          CALL fgl_winmessage(
          "Atencion","Consulta de registro contable ya en ejecucion.","stop")
       END IF
 
      BEFORE DISPLAY
       -- Desabilitando y habilitando opciones segun operacion
       CASE (operacion) 
        WHEN 1 -- Consultar 
 	 CALL DIALOG.setActionActive("modificar",FALSE)
 	 CALL DIALOG.setActionActive("delete",FALSE)
        WHEN 2 -- Modificar
 	 CALL DIALOG.setActionActive("modificar",TRUE)
 	 CALL DIALOG.setActionActive("delete",FALSE)
        WHEN 3 -- Eliminar        
 	 CALL DIALOG.setActionActive("modificar",FALSE)
 	 CALL DIALOG.setActionActive("delete",TRUE)
       END CASE
 
      BEFORE ROW 
       -- Verificando control del total de lineas 
       IF (ARR_CURR()<=totlin) THEN
          CALL bcoqbx002_datos(v_movimientos[ARR_CURR()].tlnkbco)
       END IF 
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    "Atencion",
    "No existen movimientos con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Asignando estado del menu 
  CALL bcoqbx002_EstadoMenu(0,"")
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION bcoqbx002_datos(wlnkbco)
 DEFINE wlnkbco   LIKE bco_mtransac.lnkbco,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  bco_mtransac a "||
              "WHERE a.lnkbco = "||wlnkbco

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_movimientost SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_movimientost INTO w_mae_pro.*
 END FOREACH
 CLOSE c_movimientost
 FREE  c_movimientost

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.numcta,w_mae_pro.tipmov,w_mae_pro.codrub,
                 w_mae_pro.codcos,w_mae_pro.fecemi,w_mae_pro.numdoc,
                 w_mae_pro.descrp,w_mae_pro.ctaref,w_mae_pro.totdoc,
                 w_mae_pro.nofase,w_mae_pro.feccob,w_mae_pro.estado,
                 w_mae_pro.lnkbco,w_mae_pro.userid,w_mae_pro.fecsis,
                 w_mae_pro.horsis,w_mae_pro.numesc,w_mae_pro.fecesc,
                 w_mae_pro.numser,w_mae_pro.numfac,w_mae_pro.codref,
                 w_mae_pro.tipori

 -- Desplegando detalle de la poliza contable 
 CALL bcoqbx002_PolizaContable(wlnkbco,w_mae_pro.codemp)
END FUNCTION 

-- Subrutina para desplegar el detalle de la poliza contable 

FUNCTION bcoqbx002_PolizaContable(wlnkbco,wcodemp) 
 DEFINE wlnkbco LIKE bco_mtransac.lnkbco,
        wcodemp LIKE bco_mtransac.codemp 

 -- Inicializando vector 
 CALL bcoing002_inivec() 

 -- Selecionando datos de la poliza 
 DECLARE cpoliza CURSOR FOR 
 SELECT x.numcta, 
        y.nomcta,
        x.totdeb,
        x.tothab,
        x.concep,
        "", 
        x.correl
  FROM  bco_dtpoliza x,ctb_mcuentas y 
  WHERE x.lnkbco = wlnkbco 
    AND y.codemp = wcodemp 
    AND y.numcta = x.numcta
  ORDER BY x.correl 

 LET totcta = 1 
 FOREACH cpoliza INTO v_partida[totcta].*
  LET totcta=totcta+1 
 END FOREACH
 CLOSE cpoliza
 FREE cpoliza
 LET totcta=totcta-1 

 -- Desplegando poliza 
 IF (totcta>0) THEN 
    DISPLAY ARRAY v_partida TO s_partida.*
     BEFORE DISPLAY
      EXIT DISPLAY
    END DISPLAY 
 END IF 

 -- Desplegando totales 
 CALL bcoing002_TotalCuentasPoliza()
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION bcoqbx002_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement(
  "labelx","Lista de Movimientos Bancarios - MENU")
  WHEN 1 CALL librut001_dpelement(
  "labelx","Lista de Movimientos Bancarios - CONSULTAR"||msg)
  WHEN 2 CALL librut001_dpelement(
  "labelx","Lista de Movimientos Bancarios - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement(
  "labelx","Lista de Movimientos Bancarios - ELIMINAR"||msg)
  WHEN 4 CALL librut001_dpelement(
  "labelx","Lista de Movimientos Bancarios - NUEVO")
 END CASE
END FUNCTION
