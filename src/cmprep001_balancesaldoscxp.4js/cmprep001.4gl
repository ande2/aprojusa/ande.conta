{ 
Programa : cmprep001.4gl                   
Objetivo : Reporte de balance de saldos por socio de negocios.
}

DATABASE erpjuridico 

-- Definicion de variables globales 

CONSTANT progname = "cmprep001" 
TYPE    datosreporte        RECORD 
         codsoc             LIKE glb_sociosng.codsoc,
         nomsoc             CHAR(50),                    
         salant             DEC(14,2),
         cargos             DEC(14,2),
         abocmp             DEC(14,2), 
         abonot             DEC(14,2), 
         aboefe             DEC(14,2), 
         abonos             DEC(14,2),
         salact             DEC(14,2),
         tipope             SMALLINT 
        END RECORD 
DEFINE  p                   RECORD
         length             SMALLINT,
         topmg              SMALLINT,
         botmg              SMALLINT,
         lefmg              SMALLINT,
         rigmg              SMALLINT
        END RECORD
DEFINE  datos               RECORD 
         codsoc             LIKE glb_sociosng.codsoc, 
         fecini             DATE,
         fecfin             DATE
        END RECORD
DEFINE  w_mae_emp           RECORD LIKE glb_empresas.*
DEFINE  gcodemp             LIKE cmp_mtransac.codemp  
DEFINE  existe              SMALLINT
DEFINE  primeravez          SMALLINT 
DEFINE  tituloreporte       STRING 
DEFINE  filename            STRING
DEFINE  pipeline            STRING
DEFINE  s,d                 CHAR(1) 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarReportes1")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rbalancesaldossocios") 
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL cmprep001_BalanceSaldosSociosNegocios() 
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION cmprep001_BalanceSaldosSociosNegocios() 
 DEFINE imp1      datosreporte,
        wpais     VARCHAR(255),
        w         ui.Window, 
        loop      SMALLINT,
        f         ui.Form,
        qrytxt    STRING

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep001a AT 5,2
  WITH FORM "cmprep001a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/BalanceSaldosSociosNegocios.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox de empresas
  LET gcodemp = librut003_DCbxEmpresaEnUsoXUsuario()
  IF gcodemp IS NULL THEN
     CALL fgl_winmessage(
     "Atencion","Usuario sin empresa asignada, VERIFICA.","stop")
     CLOSE WINDOW wrep001a 
     RETURN
  END IF

  -- Obteniendo nombre de la empresa
  INITIALIZE w_mae_emp.* TO NULL 
  CALL librut003_bempresa(gcodemp)
  RETURNING w_mae_emp.*,existe 

  -- Llenando combobox de socios de negocios 
  CALL librut003_CbxSociosCompras() 

  -- Inicio del loop
  OPTIONS INPUT WRAP 
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE datos.*,pipeline TO NULL
   LET s = 1 SPACE
   LET primeravez = TRUE
   CLEAR FORM
   DISPLAY gcodemp TO codemp 

   -- Ingresando datos 
   DIALOG ATTRIBUTES(UNBUFFERED,FIELD ORDER FORM)

    -- Construyendo busqueda
    INPUT BY NAME datos.codsoc,
                  datos.fecfin 
    END INPUT 

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT DIALOG

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "array" 
     LET p.length = 57
     LET p.topmg  = 3
     LET p.botmg  = 2
     LET p.lefmg  = 2
     LET p.rigmg  = 2

     -- Verificando ingreso de filtros
     IF NOT cmprep001_FiltrosCompletos() THEN 
        NEXT FIELD codsoc 
     END IF 
     EXIT DIALOG 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 
     LET p.length = 57
     LET p.topmg  = 0
     LET p.botmg  = 0
     LET p.lefmg  = 0
     LET p.rigmg  = 0

     -- Verificando ingreso de filtros
     IF NOT cmprep001_FiltrosCompletos() THEN 
        NEXT FIELD codsoc 
     END IF 
     EXIT DIALOG 

    ON ACTION excel 
     -- Asignando dispositivo 
     LET pipeline = "excel" 
     LET s        = ASCII(9) 
     LET p.length = 57
     LET p.topmg  = 0
     LET p.botmg  = 0
     LET p.lefmg  = 0
     LET p.rigmg  = 0

     -- Verificando ingreso de filtros
     IF NOT cmprep001_FiltrosCompletos() THEN 
        NEXT FIELD codsoc 
     END IF 
     EXIT DIALOG

   END DIALOG 
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Definiendo fecha inicio saldos
   LET datos.fecini = MDY(MONTH(datos.fecfin),1,YEAR(datos.fecfin)) 

   -- Construyendo seleccion 
   LET qrytxt = "SELECT UNIQUE a.codsoc,a.nomsoc,0 ",
                 "FROM  glb_sociosng a ",
                 "WHERE EXISTS (SELECT p.codsoc FROM cmp_mtransac p ", 
                                "WHERE p.codemp = ",gcodemp," ", 
                                  "AND p.codsoc = a.codsoc ",
                                  "AND p.estado = 1) ",
                   "AND a.codsoc = "||datos.codsoc, 
                 " ORDER BY a.nomsoc" 

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep001 FROM qrytxt 
   DECLARE c_balpro CURSOR FOR c_rep001
   LET existe = FALSE
   FOREACH c_balpro INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       LET existe = TRUE
       START REPORT cmprep001_GenerarReporte TO filename 
    END IF 

    -- Calculando saldo anterior 
    LET imp1.salant = 
     librut003_SaldoAnteriorCXP(gcodemp,imp1.codsoc,datos.fecini) 

    -- Totalizando cargos compras 
    LET imp1.cargos = 0 
    SELECT NVL(SUM(a.totval),0)
     INTO  imp1.cargos 
     FROM  cmp_mtransac a
     WHERE (a.codemp  = gcodemp) 
       AND (a.codsoc  = imp1.codsoc)
       AND (a.fecemi >= datos.fecini)
       AND (a.fecemi <= datos.fecfin) 
       AND (a.frmpag  IN (1,2)) 
       AND (a.tipope  = 1) 
       AND (a.estado  = 1)

    -- Totalizando abonos compras
    LET imp1.abocmp = 0 
    SELECT NVL(SUM(a.totval),0)
     INTO  imp1.abocmp 
     FROM  cmp_mtransac a
     WHERE (a.codemp  = gcodemp)
       AND (a.codsoc  = imp1.codsoc)
       AND (a.fecemi >= datos.fecini)
       AND (a.fecemi <= datos.fecfin) 
       AND (a.frmpag  IS NOT NULL) 
       AND (a.tipope  = 0) 
       AND (a.estado  = 1)

    -- Totalizando abonos bancos 
    LET imp1.abonos = 0 
    SELECT NVL(SUM(y.totpag),0)
     INTO  imp1.abonos 
     FROM  bco_mtransac x,bco_dtransac y 
     WHERE (x.lnkbco  = y.lnkbco)
       AND (x.codemp  = gcodemp) 
       AND (x.codsoc  = imp1.codsoc)
       AND (x.feccob >= datos.fecini) 
       AND (x.feccob <= datos.fecfin)
       AND (x.estado  = 1)

    -- Totalizando abonos notas de debito
    LET imp1.abonot = 0 
    SELECT NVL(SUM(y.totpag),0)
     INTO  imp1.abonot 
     FROM  cmp_notasdeb x,bco_dtransac y 
     WHERE (x.lnknot  = y.lnknot)
       AND (x.codemp  = gcodemp) 
       AND (x.codsoc  = imp1.codsoc)
       AND (x.fecemi >= datos.fecini) 
       AND (x.fecemi <= datos.fecfin)
       AND (x.estado  = 1)

    -- Totalizando abonos efectivo
    LET imp1.aboefe = 0
    SELECT NVL(SUM(a.totval),0)
     INTO  imp1.aboefe
     FROM  cmp_mtransac a
     WHERE (a.codemp  = gcodemp)
       AND (a.codsoc  = imp1.codsoc)
       AND (a.fecemi >= datos.fecini)
       AND (a.fecemi <= datos.fecfin)
       AND (a.frmpag  IN (1))
       AND (a.tipope  = 1)
       AND (a.estado  = 1)

    -- Calculando Saldo Actual 
    LET imp1.abonos = (imp1.abonos+imp1.abocmp+imp1.abonot+imp1.aboefe)
    LET imp1.salact = (imp1.salant+imp1.cargos-imp1.abonos)

    -- Verificando si hay saldos
    IF (imp1.salant=0) AND 
       (imp1.salact=0) AND
       (imp1.abonos=0) AND 
       (imp1.cargos=0) THEN
       CONTINUE FOREACH
    END IF 

    -- Llenando el reporte
    OUTPUT TO REPORT cmprep001_GenerarReporte(imp1.*)
   END FOREACH
   CLOSE c_balpro 
   FREE  c_balpro 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT cmprep001_GenerarReporte 

      -- Transfiriendo reporte a excel
      IF pipeline = "excel" THEN
         CALL cmprep001_excel(filename)
      ELSE
         -- Enviando reporte al destino seleccionado
         CALL librut001_sendreport
         (filename,pipeline,tituloreporte,
         "--noline-numbers "||
         "--nofooter "||
         "--font-size 7 "||
         "--page-width 842 --page-height 595 "||
         "--left-margin 35 --right-margin 15 "||
         "--top-margin 30 --bottom-margin 35 "||
         "--title Cuentas-Por-Pagar")
      END IF

      ERROR "" 
      CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(
      " Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 
  END WHILE
  OPTIONS INPUT NO WRAP 

 CLOSE WINDOW wrep001a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION cmprep001_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE

 -- Verificando datos 
 IF datos.fecfin IS NULL THEN 
    CALL fgl_winmessage(
    " Atencion",
    " Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte 

REPORT cmprep001_GenerarReporte(imp1)
 DEFINE imp1              datosreporte,
        w_cargou          DEC(14,2),
        w_cartou,w_abonou DEC(12,2),
        w_abotou          DEC(14,2),
        linea             CHAR(150),
        col,i,lg          INTEGER,
        espacios          CHAR(4), 
        periodo           STRING

  OUTPUT LEFT MARGIN   p.lefmg
         TOP MARGIN    p.topmg
         BOTTOM MARGIN p.botmg
         PAGE LENGTH   p.length

  FORMAT 
   PAGE HEADER

    -- Llenando linea
    LET linea = NULL
    LET lg    = 150 
    FOR i = 1 TO lg
     LET linea = linea CLIPPED,"-"
    END FOR  

    -- Imprimiendo Encabezado
    LET col = librut001_centrado(w_mae_emp.nomemp,lg) 
    PRINT COLUMN   1,"Cuentas X Pagar",
          COLUMN col,w_mae_emp.nomemp CLIPPED,  
	  COLUMN (lg-20),PAGENO USING "Pagina: <<<<"

    LET col = librut001_centrado(tituloreporte,lg) 
    PRINT COLUMN   1,"Cmprep001",
          COLUMN col,UPSHIFT(tituloreporte) CLIPPED,  
          COLUMN (lg-20),"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    LET periodo = "PERIODO DE FECHAS DEL [ ",
                  datos.fecini," ] AL [ ",datos.fecfin," ]" 
    LET col = librut001_centrado(periodo,lg) 

    PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
          COLUMN col,periodo CLIPPED,
          COLUMN (lg-20),"Hora  : ",TIME 
    PRINT linea 
    PRINT "Socio de Negocios                                      Saldo Anterior   ", 
          "     A b o n o s        C a r g o s       Saldo Actual" 
    PRINT linea 

  ON EVERY ROW 
   -- Imprimiendo encabezado a excel
   IF (pipeline="excel") THEN
      IF primeravez THEN 
         PRINT UPSHIFT(tituloreporte) CLIPPED,s
         PRINT periodo CLIPPED,s
         PRINT s 
         PRINT "Socio de Negocios",s,
               "Saldo Anterior",s,
               "Abonos",s,
               "Cargos",s,
               "Saldo Actual" 
         PRINT s 
         LET primeravez = FALSE 
      END IF 
   END IF 

   -- Imprimiendo movimientos 
   LET espacios = s,s,s,s 

   PRINT imp1.nomsoc                                      ,s,3 SPACES,
         imp1.salant               USING "----,---,--&.&&",s,3 SPACES,           
         imp1.abonos               USING "----,---,--&.&&",s,3 SPACES,
         imp1.cargos               USING "----,---,--&.&&",s,3 SPACES,           
         imp1.salact               USING "----,---,--&.&&",s

  ON LAST ROW
   -- Totalizando reporte 
   PRINT s 
   PRINT "Total Balance -->",33 SPACES                    ,s,3 SPACES,
         SUM(imp1.salant)          USING "----,---,--&.&&",s,3 SPACES,           
         SUM(imp1.abonos)          USING "----,---,--&.&&",s,3 SPACES,
         SUM(imp1.cargos)          USING "----,---,--&.&&",s,3 SPACES,           
         SUM(imp1.salact)          USING "----,---,--&.&&",s
END REPORT 

-- Subrutina para exportar el reporte a excel usando WinCOM 

FUNCTION cmprep001_excel(filename) 
 DEFINE sb          base.StringBuffer
 DEFINE ch          base.channel
 DEFINE lista       base.StringTokenizer
 DEFINE result      INTEGER
 DEFINE i           INTEGER
 DEFINE xlapp       INTEGER
 DEFINE xlwb        INTEGER
 DEFINE linea       STRING 
 DEFINE lineadep    STRING 
 DEFINE filename    STRING 
 DEFINE columna     STRING 
 DEFINE separador   CHAR(1) 
 DEFINE nuevalinea  CHAR(1) 

 -- Inicializando variables                            
 LET xlapp = -1
 LET xlwb = -1

 -- Creando la instancia de Excel
 CALL ui.Interface.frontCall("WinCOM", "CreateInstance", ['Excel.Application'], [xlapp])
 CALL CheckError(xlapp, xlwb, xlapp)
   
 -- Agregando un libro al documento de excel               
 CALL ui.interface.frontCall("WinCOM", "CallMethod", [xlapp, 'WorkBooks.Add'], [xlwb])
 CALL CheckError(xlapp, xlwb, xlwb)
   
 -- Haciendo visible el libro                 
 CALL excel_set_property(xlapp, xlapp, "Visible","True")
  
 -- Llenando celdas desde el FrontCall 
 -- Construyendo celdas delimitando columnas por TAB (ASCII(9)), 
 -- y filas por CR (ASCII(10)
 -- Copia al clipboard via el Frontcall y luego instruye a excel para pegar el contenido
 -- de la celdas dentro del libro 
 LET sb = base.StringBuffer.create()

 -- Leyendo el archivo que contiene el reporte y copiandolo al clipboard 
 LET ch = base.channel.create()
 call ch.openfile(filename,"r")

 let i = 1 
 let separador  = ascii(9) 
 let nuevalinea = ascii(10)
 while true
  let linea = ch.readLine() 
  if ch.isEof() then exit while end if
  if linea.getindexof(separador,1)>0 then
     CALL sb.append(linea)
     CALL sb.append(nuevalinea)
  end if 
  let i = i+1
 end while
 call ch.close() 

 -- Pegando el contenido de las celdas en el clipboard al libro de excel
 CALL ui.Interface.FrontCall("standard","cbset",sb.toString(),result)
 CALL excel_call_method(xlapp, xlwb, 'ActiveSheet.Paste')
 CALL excel_call_method(xlapp, xlwb, 'Columns("A:Z").EntireColumn.AutoFit')

 -- Liberando informacion del clipboard    
 CALL freeMemory(xlapp, xlwb)
END FUNCTION
