{ 
Programa : ctbrep005.4gl                   
Programo : Mynor Ramirez 
Objetivo : Reporte de balance de comprobacion de saldos 
}

DATABASE erpjuridico 

-- Definicion de variables globales 

CONSTANT progname = "ctbrep005" 
TYPE    datosreporte        RECORD 
         codemp             LIKE ctb_mcuentas.codemp, 
         numcta             LIKE ctb_mcuentas.numcta,
         nomcta             LIKE ctb_mcuentas.nomcta,
         numniv             LIKE ctb_mcuentas.numniv,
         tipcta             LIKE ctb_mcuentas.tipcta, 
         salant             LIKE ctb_mcuentas.salant,
         salact             LIKE ctb_mcuentas.salact, 
         cargos             LIKE ctb_mcuentas.cargos, 
         caracu             LIKE ctb_mcuentas.caracu, 
         abonos             LIKE ctb_mcuentas.abonos, 
         aboacu             LIKE ctb_mcuentas.aboacu  
        END RECORD 
DEFINE  datos               RECORD 
         codemp             LIKE ctb_mcuentas.codemp, 
         numcta             LIKE ctb_mcuentas.numcta, 
         fecini             DATE, 
         fecfin             DATE
        END RECORD
DEFINE  p                   RECORD
         length             SMALLINT,
         topmg              SMALLINT,
         botmg              SMALLINT,
         lefmg              SMALLINT,
         rigmg              SMALLINT
        END RECORD
DEFINE  w_mae_emp           RECORD LIKE glb_empresas.*
DEFINE  gcodemp             LIKE glb_empresas.codemp 
DEFINE  existe              SMALLINT
DEFINE  haydata             SMALLINT
DEFINE  tituloreporte       STRING 
DEFINE  filename            STRING
DEFINE  pipeline            STRING
DEFINE  s,d                 CHAR(1) 
DEFINE  np                  CHAR(3) 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarReportes1")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rbalcomprobacion")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL ctbrep005_BalanceComprobacion()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION ctbrep005_BalanceComprobacion()
 DEFINE imp1     datosreporte,
        wpais    VARCHAR(255),
        qrytxt   STRING,
        qryemp   STRING,
        qrypart  STRING,
        loop     SMALLINT,
        w        ui.Window, 
        f        ui.Form   

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep002a AT 5,2
  WITH FORM "ctbrep005a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/balancecomprobacion.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox de empresas
  LET gcodemp = librut003_DCbxEmpresaEnUsoXUsuario()
  IF gcodemp IS NULL THEN
     CALL fgl_winmessage(
     "Atencion","Usuario sin empresa asignada, VERIFICA.","stop")
     CLOSE WINDOW wrep002a 
     RETURN
  END IF

  -- Inicio del loop
  OPTIONS INPUT WRAP 
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE pipeline,datos.* TO NULL
   LET datos.codemp = gcodemp 
   LET s = 1 SPACE
   CLEAR FORM
   DISPLAY BY NAME datos.codemp

   -- Ingresando datos 
   INPUT BY NAME datos.numcta, 
                 datos.fecini, 
                 datos.fecfin WITHOUT DEFAULTS 
    ATTRIBUTE(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT    

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 
     LET p.length = 55  
     LET p.topmg  = 3
     LET p.botmg  = 2
     LET p.lefmg  = 2
     LET p.rigmg  = 2

     -- Verificando ingreso de filtros
     IF NOT ctbrep005_FiltrosCompletos() THEN 
        CONTINUE INPUT 
     END IF 
     EXIT INPUT    

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 
     LET p.length = 55  
     LET p.topmg  = 0
     LET p.botmg  = 0
     LET p.lefmg  = 0
     LET p.rigmg  = 0

     -- Verificando ingreso de filtros
     IF NOT ctbrep005_FiltrosCompletos() THEN 
        CONTINUE INPUT  
     END IF 
     EXIT INPUT    

    ON ACTION excel 
     -- Asignando dispositivo 
     LET pipeline = "excel" 
     LET s = ASCII(9) 
     LET p.length = 55  
     LET p.topmg  = 0
     LET p.botmg  = 0
     LET p.lefmg  = 0
     LET p.rigmg  = 0

     -- Verificando ingreso de filtros
     IF NOT ctbrep005_FiltrosCompletos() THEN 
        CONTINUE INPUT 
     END IF 
     EXIT INPUT   

   END INPUT 
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Obteniendo datos de la empresa
   INITIALIZE w_mae_emp.* TO NULL
   CALL librut003_BEmpresa(datos.codemp) 
   RETURNING w_mae_emp.*,existe 

   -- Construyendo seleccion 
   LET qrytxt = 
    "SELECT a.codemp,a.numcta,a.nomcta,a.numniv,a.tipcta,0,0,0,0,0,0 ",
     "FROM  ctb_mcuentas a ",
     "WHERE a.codemp = ",datos.codemp,
     "  AND a.numcta MATCHES '"||datos.numcta CLIPPED||"'",
     "  AND a.tipcta = 'D' ", 
     " ORDER BY a.codemp,a.numcta" 

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep002 FROM qrytxt 
   DECLARE c_crep002 CURSOR FOR c_rep002
   LET haydata = FALSE
   FOREACH c_crep002 INTO imp1.* 
    -- Iniciando reporte
    IF NOT haydata THEN
       LET haydata = TRUE

       -- Iniciando reporte 
       START REPORT ctbrep005_ImprimirDatos TO filename 
    END IF 

    -- Obteniendo saldos de la cuenta
    CALL ctbrep005_SaldoCuentasContables(imp1.codemp,
                                         imp1.numcta,
                                         datos.fecini,
                                         datos.fecfin)
    RETURNING imp1.salant,imp1.salact,imp1.cargos,imp1.abonos

    -- Excluyendo cuentas sin saldo y sin movimiento
    IF (imp1.salant=0) AND
       (imp1.cargos=0) AND
       (imp1.abonos=0) THEN 
       CONTINUE FOREACH
    END IF  

    -- Calculando saldo
    LET imp1.salact = imp1.cargos-imp1.abonos 

    -- Llenando el reporte
    OUTPUT TO REPORT ctbrep005_ImprimirDatos(imp1.*)
   END FOREACH
   CLOSE c_crep002 
   FREE  c_crep002 

   IF haydata THEN
      -- Finalizando el reporte
      FINISH REPORT ctbrep005_ImprimirDatos 

      -- Transfiriendo reporte a excel
      IF pipeline = "excel" THEN
         CALL ctbrep005_excel(filename)
      ELSE
       CALL librut001_sendreport
       (filename,pipeline,tituloreporte, 
        "--noline-numbers "||
        "--nofooter "||
        "--font-size 7 "||
        "--page-width 842 --page-height 595  "||
        "--left-margin 35 --right-margin 15 "||
        "--top-margin 30 --bottom-margin 35 "||
        "--title Contabilidad")
      END IF

      ERROR "" 
      CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(
      " Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF  
  END WHILE

  OPTIONS INPUT NO WRAP 

 CLOSE WINDOW wrep002a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION ctbrep005_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE

 -- Verificando datos 
 IF datos.codemp IS NULL OR
    datos.fecini IS NULL OR 
    datos.fecfin IS NULL THEN 
    CALL fgl_winmessage(
    " Atencion",
    " Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte 

REPORT ctbrep005_ImprimirDatos(imp1)
 DEFINE wt                RECORD 
         salact           DECIMAL(14,2),
         cargos           DECIMAL(14,2),
         abonos           DECIMAL(14,2),
         salant           DECIMAL(14,2) 
        END RECORD, 
        wm                RECORD LIKE ctb_mtransac.*, 
        wd                RECORD LIKE ctb_dtransac.*, 
        wtiptrn           RECORD LIKE ctb_tipostrn.*, 
        wabonom           DECIMAL(14,2),
        wcargom           DECIMAL(14,2),
        wconcpt           CHAR(60), 
        imp1              datosreporte,
        linea             CHAR(306),
        cont,i,existr     INTEGER,       
        exist,lg          SMALLINT, 
        col,col1,col2     INTEGER,
        col3,col4,col5    INTEGER,
        espacios          CHAR(4), 
        fechareporte      STRING,
        f                 CHAR(1) 

  OUTPUT LEFT MARGIN   p.lefmg
         TOP MARGIN    p.topmg
         BOTTOM MARGIN p.botmg
         PAGE LENGTH   p.length

  FORMAT 
   PAGE HEADER

    -- Llenando linea
    LET lg = 170 
    LET linea = NULL
    FOR i = 1 TO lg
     LET linea = linea CLIPPED,"-"
    END FOR  

    -- Imprimiendo Encabezado
    LET col = librut001_centrado(tituloreporte,lg) 
    PRINT COLUMN   1,"Contabilidad",
          COLUMN col,UPSHIFT(tituloreporte) CLIPPED,  
	  COLUMN (lg-20),PAGENO USING "Pagina: <<<<"

    LET col = librut001_centrado("Expresado en Quetzales",lg) 
    PRINT COLUMN   1,"Ctbrep005",
          COLUMN col,"Expresado en Quetzales",
          COLUMN (lg-20),"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    LET fechareporte = "DEL [",datos.fecini,"] AL [ ",datos.fecfin," ]" 
    LET col = librut001_centrado(fechareporte,lg) 

    PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
          COLUMN col,fechareporte CLIPPED,
          COLUMN (lg-20),"Hora  : ",TIME 
    PRINT linea 

    -- Verificando si no es detallado 
    PRINT "Cuenta              Nombre de la Cuenta                               ",
          "                      Saldo Inicial         D E B E       ",
          "H A B E R           Saldo"
    PRINT linea
    PRINT "Empresa: ",w_mae_emp.nomemp CLIPPED
    SKIP 1 LINES 

  BEFORE GROUP OF imp1.codemp 
   -- Verificando si reporte es a excel
   IF (PAGENO=1) THEN 
    IF (pipeline="excel") THEN
      -- Imprimiendo datos 
      PRINT tituloreporte CLIPPED,s
      PRINT "Empresa: ",w_mae_emp.nomemp CLIPPED,s
      PRINT "Expresado en Quetzales",s
      PRINT "Periodo Del [",datos.fecini,"] AL [ ",datos.fecfin," ]",s
      PRINT s
      PRINT "Cuenta",s,
            "Nombre de la Cuenta",s,
            "Saldo Inicial",s,
            "D E B E",s,
            "H A B E R",s,  
            "Saldo",s
    END IF 
   END IF

  BEFORE GROUP OF imp1.numcta 
   -- Verificando si reporte es a excel 
   IF (pipeline="excel") THEN  
      LET col1 = 22
      LET col2 = 82  
   ELSE
      LET col1 = (18+(imp1.numniv+2))
      LET col2 = 140 
      LET col3 = 91  
   END IF 

   -- Verificando nivel 1
   IF imp1.numniv = 1 THEN
      PRINT s
   END IF

   -- Imprimiendo cuentas y valores 
   IF (pipeline!="excel") THEN 
    PRINT COLUMN    1,imp1.numcta,s,
          COLUMN col1,imp1.nomcta,s,
          COLUMN col3,imp1.salant USING "----,---,--&.&&",s, 
                      imp1.cargos USING "----,---,--&.&&",s,
                      imp1.abonos USING "----,---,--&.&&",s,
                      imp1.salact USING "----,---,--&.&&",s
   ELSE 
    PRINT "'",imp1.numcta,s,
          imp1.nomcta,s,
          imp1.salant USING "----,---,--&.&&",s, 
          imp1.cargos USING "----,---,--&.&&",s,
          imp1.abonos USING "----,---,--&.&&",s,
          imp1.salact USING "----,---,--&.&&",s
   END IF 

  AFTER GROUP OF imp1.codemp 
   -- Imprimiendo totales    
   PRINT s 
   IF (pipeline!="excel") THEN 
    PRINT COLUMN   1,"Total del Periodo -->",s, 
          COLUMN col3,SUM(imp1.salant) USING "----,---,--&.&&",s,
                      SUM(imp1.cargos) USING "----,---,--&.&&",s,
                      SUM(imp1.abonos) USING "----,---,--&.&&",s,
                      SUM(imp1.salact) USING "----,---,--&.&&",s
   ELSE
    PRINT "Total del Periodo -->",s,s,
          SUM(imp1.salant) USING "----,---,--&.&&",s,
          SUM(imp1.cargos) USING "----,---,--&.&&",s,
          SUM(imp1.abonos) USING "----,---,--&.&&",s,
          SUM(imp1.salact) USING "----,---,--&.&&",s
   END IF 
END REPORT 

-- Subrutina para exportar el reporte a excel usando WinCOM 

FUNCTION ctbrep005_excel(filename) 
 DEFINE sb          base.StringBuffer
 DEFINE ch          base.channel
 DEFINE lista       base.StringTokenizer
 DEFINE result      INTEGER
 DEFINE i           INTEGER
 DEFINE xlapp       INTEGER
 DEFINE xlwb        INTEGER
 DEFINE linea       STRING 
 DEFINE lineadep    STRING 
 DEFINE filename    STRING 
 DEFINE columna     STRING 
 DEFINE separador   CHAR(1) 
 DEFINE nuevalinea  CHAR(1) 

 -- Inicializando variables                            
 LET xlapp = -1
 LET xlwb = -1

 -- Creando la instancia de Excel
 CALL ui.Interface.frontCall("WinCOM", "CreateInstance", ['Excel.Application'], [xlapp])
 CALL CheckError(xlapp, xlwb, xlapp)
   
 -- Agregando un libro al documento de excel               
 CALL ui.interface.frontCall("WinCOM", "CallMethod", [xlapp, 'WorkBooks.Add'], [xlwb])
 CALL CheckError(xlapp, xlwb, xlwb)
   
 -- Haciendo visible el libro                 
 CALL excel_set_property(xlapp, xlapp, "Visible","True")
  
 -- Llenando celdas desde el FrontCall 
 -- Construyendo celdas delimitando columnas por TAB (ASCII(9)), 
 -- y filas por CR (ASCII(10)
 -- Copia al clipboard via el Frontcall y luego instruye a excel para pegar el contenido
 -- de la celdas dentro del libro 
 LET sb = base.StringBuffer.create()

 -- Leyendo el archivo que contiene el reporte y copiandolo al clipboard 
 LET ch = base.channel.create()
 call ch.openfile(filename,"r")

 let i = 1 
 let separador  = ascii(9) 
 let nuevalinea = ascii(10)
 while true
  let linea = ch.readLine() 
  if ch.isEof() then exit while end if
  if linea.getindexof(separador,1)>0 then
     CALL sb.append(linea)
     CALL sb.append(nuevalinea)
  end if 
  let i = i+1
 end while
 call ch.close() 

 -- Pegando el contenido de las celdas en el clipboard al libro de excel
 CALL ui.Interface.FrontCall("standard","cbset",sb.toString(),result)
 CALL excel_call_method(xlapp, xlwb, 'ActiveSheet.Paste')
 CALL excel_call_method(xlapp, xlwb, 'Columns("A:Z").EntireColumn.AutoFit')
 CALL excel_set_property(xlapp, xlwb, excel_column("C","F","NumberFormat"), "0.00")

 -- Liberando informacion del clipboard    
 CALL freeMemory(xlapp, xlwb)
END FUNCTION

-- Subrutina para encontrar los saldos de las cuentas contables en base a un un periodo }

FUNCTION ctbrep005_SaldoCuentasContables(w_codemp,w_numcta,w_fecini,w_fecfin)
 DEFINE w_codemp  LIKE ctb_mcuentas.codemp,
        w_numcta  LIKE ctb_mcuentas.numcta,
        w_salant  LIKE ctb_mcuentas.salant,
        w_saldos  LIKE ctb_mcuentas.salact, 
        w_salcom  LIKE ctb_mcuentas.salact, 
        w_cargos  LIKE ctb_mcuentas.cargos,   
        w_abonos  LIKE ctb_mcuentas.abonos,   
        w_fecini  LIKE ctb_mtransac.fecemi,
        w_fecfin  LIKE ctb_mtransac.fecemi,
        existe    SMALLINT

 -- Inicializando datos
 LET w_saldos = 0 
 LET w_cargos = 0
 LET w_abonos = 0

 -- Calculando saldo anterior 
 SELECT NVL(SUM(x.salact),0) 
  INTO  w_salant 
  FROM  ctb_ctransac x,ctb_mtransac a,ctb_tipostrn t 
  WHERE (a.lnktra = x.lnktra) AND
        (x.codemp = w_codemp) AND
        (x.numcta = w_numcta) AND
        (x.fecemi < w_fecini) AND
        (t.tiptrn = a.tiptrn) AND 
        (t.tipope = 1) -- Movimiento 

 -- Sumando cargos, abonos y saldo actual 
 SELECT NVL(SUM(x.cargos),0),
        NVL(SUM(x.abonos),0), 
        NVL(SUM(x.salact),0) 
  INTO  w_cargos,
        w_abonos,
        w_salcom 
  FROM  ctb_ctransac x,ctb_mtransac a,ctb_tipostrn t 
  WHERE (a.lnktra =  x.lnktra) AND
        (x.codemp =  w_codemp) AND
        (x.numcta =  w_numcta) AND
        (x.fecemi >= w_fecini) AND
        (x.fecemi <= w_fecfin) AND
        (t.tiptrn =  a.tiptrn) AND 
        (t.tipope = 1) -- Movimiento 

  LET w_saldos = w_salcom+w_salant 

 RETURN w_salant,w_saldos,w_cargos,w_abonos
END FUNCTION
