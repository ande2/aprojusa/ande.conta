{ 
cmprep004.4gl 
Reporte de libro de compras
}

DATABASE erpjuridico 

--  Definicion de variables globales 

CONSTANT progname     = "cmprep004" 
TYPE     datosreporte    RECORD 
          origen         SMALLINT, 
          codemp         LIKE glb_empresas.codemp,
          nomemp         CHAR(50), 
          numest         LIKE cmp_mtransac.numest, 
          feclbc         DATE, 
          fecemi         DATE, 
          nserie         CHAR(20),
          numdoc         CHAR(20),
          tipdoc         CHAR(5),
          codsoc         LIKE cmp_mtransac.codsoc,  
          numnit         CHAR(20), 
          nomsoc         VARCHAR(70), 
          totdoc         DEC(14,2), 
          totcmp         DEC(14,2), 
          totser         DEC(14,2),
          totedp         DEC(14,2),
          totipt         DEC(14,2),
          totded         DEC(14,2),
          totfpc         DEC(14,2),
          nomcen         CHAR(30), 
          totisv         DEC(14,2),
          lnkori         INT, 
          nomest         CHAR(30),
          nomcom         CHAR(15) 
         END RECORD 
DEFINE   w_datos         RECORD
          codemp         LIKE cmp_mtransac.codemp,
          numest         LIKE cmp_mtransac.numest, 
          fecini         DATE,
          fecfin         DATE
         END RECORD
DEFINE   v_tipdocs       DYNAMIC ARRAY OF RECORD
          tipdoc         CHAR(5),
          cheque         SMALLINT,
          rellen         CHAR(1)
         END RECORD 
DEFINE   p               RECORD
          length         SMALLINT,
          topmg          SMALLINT,
          botmg          SMALLINT,
          lefmg          SMALLINT,
          rigmg          SMALLINT
         END RECORD
DEFINE   gcodemp         LIKE cmp_mtransac.codemp  
DEFINE   xnumnit         CHAR(30)  
DEFINE   xnomest         CHAR(40)  
DEFINE   existe,tottdc   SMALLINT
DEFINE   tituloreporte   STRING
DEFINE   filename        STRING
DEFINE   pipeline        STRING
DEFINE   s,d             CHAR(1)

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarReportes1") 

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rlibrocompras")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL cmprep004_LibroCompras()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION cmprep004_LibroCompras()
 DEFINE myHandler om.SaxDocumentHandler, 
        imp1      datosreporte, 
        wpais     VARCHAR(255),
        xdoctos   CHAR(10),
        xnumdoc   CHAR(10), 
        qrytxt    STRING,
        loop,i    SMALLINT,
        w         ui.Window

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep004a AT 5,2
  WITH FORM "cmprep004a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/LibroCompras.spl"

  -- Cargando combobox de empresas
  LET gcodemp = librut003_DCbxEmpresaEnUsoXUsuario()
  IF gcodemp IS NULL THEN
     CALL fgl_winmessage(
     "Atencion","Usuario sin empresa asignada, VERIFICA.","stop")
     CLOSE WINDOW wrep004a  
     RETURN
  END IF

  -- Cargando combobox de establecimientos sat
  CALL librut003_CbxEstablecimientosSAT()

  -- Creando table temporal de tipos de documento
  CREATE TEMP TABLE tmp_tipodocs
  ( tipdoc CHAR(5) ) 

  -- Inicio del loop
  OPTIONS INPUT WRAP 
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
   LET s = 1 SPACES
   LET d = "~" 
   LET w_datos.codemp = gcodemp 
   CLEAR FORM
   DISPLAY BY NAME w_datos.codemp
   CALL cmprep004_TiposDocumentoRegimenFiscal() 

   DIALOG ATTRIBUTES(UNBUFFERED) 
    -- Construyendo busqueda
    INPUT BY NAME w_datos.numest,
                  w_datos.fecini, 
                  w_datos.fecfin
     ATTRIBUTE (WITHOUT DEFAULTS) 
    END INPUT 

    -- Seleccionando tipos de documento
    INPUT ARRAY v_tipdocs FROM s_tipdocs.* 
     ATTRIBUTE (WITHOUT DEFAULTS,MAXCOUNT=tottdc,INSERT ROW=FALSE,APPEND ROW=FALSE,
                DELETE ROW=FALSE) 
    END INPUT 

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT DIALOG  

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 
     LET p.length = 57  
     LET p.topmg  = 3
     LET p.botmg  = 2
     LET p.lefmg  = 2
     LET p.rigmg  = 2

     -- Verificando si filtros estan completos
     IF NOT cmprep004_FiltrosCompletos() THEN
        NEXT FIELD numest 
     END IF
     EXIT DIALOG  

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 
     LET p.length = 57  
     LET p.topmg  = 0
     LET p.botmg  = 0
     LET p.lefmg  = 0
     LET p.rigmg  = 0

     -- Verificando si filtros estan completos
     IF NOT cmprep004_FiltrosCompletos() THEN
        NEXT FIELD numest 
     END IF
     EXIT DIALOG  

    ON ACTION excel 
     -- Asignando dispositivo 
     LET pipeline = "excel" 
     LET s        = ASCII(9) 
     LET d        = ASCII(9) 
     LET p.length = 57  
     LET p.topmg  = 0
     LET p.botmg  = 0
     LET p.lefmg  = 0
     LET p.rigmg  = 0

     -- Verificando si filtros estan completos
     IF NOT cmprep004_FiltrosCompletos() THEN
        NEXT FIELD numest 
     END IF
     EXIT DIALOG  
   END DIALOG 

   IF NOT loop THEN
      EXIT WHILE
   END IF    

   -- Obteniendo NIT de la empresa
   LET xnumnit = NULL
   SELECT a.numnit INTO xnumnit FROM glb_empresas a
    WHERE a.codemp = w_datos.codemp 

   -- Obteniendo nombre el establecimiento 
   LET xnomest = NULL
   IF w_datos.numest IS NOT NULL THEN 
    SELECT a.nomest INTO xnomest FROM glb_estabsat a
     WHERE a.numest = w_datos.numest 
   ELSE
    IF (pipeline!="excel") THEN 
     LET xnomest = "ESTABLECIMIENTO SAT: TODOS" 
    ELSE
     LET xnomest = "TODOS" 
    END IF 
   END IF 

   -- Llenando tabla temporal de tipos de documento seleccionados 
   DELETE FROM tmp_tipodocs 
   FOR i = 1 TO tottdc 
    IF v_tipdocs[i].cheque!=1 THEN
       CONTINUE FOR
    END IF

    -- Grabando tipo de documento 
    INSERT INTO tmp_tipodocs 
    VALUES (v_tipdocs[i].tipdoc) 
   END FOR 

   -- Construyendo seleccion 
   LET qrytxt = 
    "SELECT a.* ", 
    " FROM  vis_librocompras a ",
    " WHERE a.codemp  =  ",w_datos.codemp, 
    "   AND a.numest  =  "||w_datos.numest," ",
    "   AND a.feclbc >= '",w_datos.fecini,"' ",
    "   AND a.feclbc <= '",w_datos.fecfin,"' ", 
    "   AND EXISTS (SELECT x.tipdoc FROM tmp_tipodocs x WHERE x.tipdoc=a.tipdoc)", 
    " ORDER BY a.fecemi" 

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep004 FROM qrytxt 
   DECLARE c_crep004 SCROLL CURSOR WITH HOLD FOR c_rep004
   LET existe = FALSE
   FOREACH c_crep004 INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       LET existe = TRUE
       START REPORT cmprep004_GeneraReporte TO filename
    END IF 

    -- Obteniendo cheques con que se pago compra
    LET xdoctos = NULL
    IF (imp1.lnkori>0) THEN
       -- Buscando cheque en bancos
       DECLARE cchqs CURSOR FOR
       SELECT x.numdoc,x.fecemi 
        FROM  bco_mtransac x, bco_dtransac y 
        WHERE x.lnkbco = y.lnkbco
          AND y.lnkcmp = imp1.lnkori 
        ORDER BY 2 

        LET xdoctos = NULL 
        FOREACH cchqs INTO xnumdoc 
         LET xdoctos = xdoctos CLIPPED,xnumdoc USING "<<<<<<<<<<","," 
        END FOREACH
        LET xdoctos = librut001_replace(xdoctos,",","",10) 
    END IF 

    -- Llenando el reporte
    OUTPUT TO REPORT cmprep004_GeneraReporte(imp1.*,xdoctos) 
   END FOREACH
   CLOSE c_crep004 
   FREE  c_crep004 

   IF existe THEN
    -- Finalizando el reporte
    FINISH REPORT cmprep004_GeneraReporte   

    -- Transfiriendo reporte a excel 
    IF pipeline = "excel" THEN 
      CALL librut005_excel(filename) 
    ELSE 
      -- Enviando reporte al destino seleccionado
      CALL librut001_sendreport
      (filename,pipeline,tituloreporte,
      "--noline-numbers "||
      "--nofooter "||
      "--font-size 7 "||
      "--page-width 1450 --page-height 595  "||
      "--left-margin 35 --right-margin 15 "||
      "--top-margin 30 --bottom-margin 35 "||
      "--title Cuentas-Por-Pagar")
    END IF 


    ERROR "" 
    CALL fgl_winmessage(
    " Atencion","Reporte Emitido.","information") 
   ELSE
    ERROR "" 
    CALL fgl_winmessage(" Atencion",
    "No existen datos con el filtro seleccionado.","stop")
   END IF 
  END WHILE
  OPTIONS INPUT NO WRAP 

  -- Dropeando table temporal
  DROP TABLE tmp_tipodocs 

 CLOSE WINDOW wrep004a   
END FUNCTION 

-- Subrutina para cargas los tipos de documento del regimen fiscal

FUNCTION cmprep004_TiposDocumentoRegimenFiscal() 
 -- Seleccionando datos
 CALL v_tipdocs.clear()
 DECLARE ctip CURSOR FOR
 SELECT UNIQUE x.tipdoc,1
  FROM  glb_dregmfis x
  ORDER BY 1 
  LET tottdc = 1
  FOREACH ctip INTO v_tipdocs[tottdc].*
   LET tottdc = tottdc+1
  END FOREACH
  CLOSE ctip
  FREE  ctip
  LET tottdc = tottdc-1

 -- Desplegando datos
 DISPLAY ARRAY v_tipdocs TO s_tipdocs.* 
  BEFORE DISPLAY
   EXIT DISPLAY 
 END DISPLAY 
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION cmprep004_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE
 IF w_datos.codemp IS NULL OR
    w_datos.fecini IS NULL OR
    w_datos.fecfin IS NULL THEN 
    CALL fgl_winmessage(
    " Atencion",
    " Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte 

REPORT cmprep004_GeneraReporte(imp1,xdoctos)
 DEFINE imp1      datosreporte,
        linea     VARCHAR(331),
        col,i,lg  SMALLINT,
        lfn       SMALLINT,
        vnitsoc   STRING,           
        vnumdoc   STRING,      
        vnserie   STRING,      
        vtipdoc   STRING, 
        xnitemp   CHAR(40),
        xnitsoc   CHAR(20),
        xnumdoc   CHAR(21), 
        xnserie   CHAR(21), 
        xnomsoc   CHAR(40), 
        xdoctos   CHAR(10),
        periodo   STRING 

  OUTPUT LEFT MARGIN   p.lefmg
         TOP MARGIN    p.topmg
         BOTTOM MARGIN p.botmg
         PAGE LENGTH   p.length

  FORMAT 
   PAGE HEADER
    -- Definiendo linea 
    LET linea = "__________________________________________________",
                "__________________________________________________",
                "__________________________________________________",
                "__________________________________________________", 
                "__________________________________________________",
                "__________________________________________________",
                "_______________________________" 


    -- Ajustando largo del reporte 
    LET lg    = 331 
    LET lfn   = lg-20  

    -- Periodo de fechas
    LET periodo = "PERIODO DEL ",w_datos.fecini," AL ",w_datos.fecfin

    -- Imprimiendo Encabezado
    LET col = librut001_centrado(tituloreporte,lg) 
    PRINT COLUMN   1,"COMPRAS",
          COLUMN col,UPSHIFT(tituloreporte) CLIPPED,  
	  COLUMN lfn,PAGENO USING "Pagina: <<<<"

    LET col = librut001_centrado(periodo,lg) 
    PRINT COLUMN   1,"cmprep004",
          COLUMN col,periodo CLIPPED, 
          COLUMN lfn,"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    LET col = librut001_centrado(imp1.nomemp,lg) 
    PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
          COLUMN col,imp1.nomemp CLIPPED, 
          COLUMN lfn,"Hora  : ",TIME 

    LET xnitemp = "NIT: ",xnumnit 
    LET col = librut001_centrado(xnitemp,lg) 
    PRINT COLUMN   1,xnomest CLIPPED, 
          COLUMN col,xnitemp CLIPPED 

    PRINT linea 
    PRINT "FECHA      SERIE                 NUMERO                FPC   NIT",
          "                  PROVEEDOR                                 ",
          "TOTAL COMPRAS      MONTO        MONTO        MONTO               MONTO",
          "       MONTO            FACTURAS  NUMERO     ",
          "ESTABLECIMIENTO SAT            ",
          "SEDE                           TIPO DE              IVA     "
    PRINT "                                                                ",
          "                                                            ",
          " O SERVICIOS       COMPRAS Q.   SERVICIOS Q. COMBUSTIBLE Q. IDP/INGUAT",
          " Q.    OTRAS DED Q.      PEQUENO  ",
          "CHEQUE                                    ",
          "                               COMPRA               CREDITO "
    PRINT "                                                                ",
          "                                                            ",
          "      Q.           SIN/IVA      SIN/IVA      SIN/IVA                  ",
          "                       CONTRIB Q.                             ",
          "                                                            ",
          "      FISCAL Q."
    PRINT linea

   BEFORE GROUP OF imp1.codemp 
    -- Verificando si tipo de reporte es a EXCEL
    IF (pipeline="excel") THEN
       PRINT "LIBRO DE COMPRAS",s 
       PRINT "PERIODO: ",s,"PERIODO DEL ",w_datos.fecini," AL ",w_datos.fecfin,s
       PRINT "EMPRESA:",s,imp1.nomemp CLIPPED,s
       PRINT "NIT:",s,xnumnit CLIPPED,s
       PRINT "ESTABLECIMIENTO SAT: ",s,xnomest CLIPPED,s 
       PRINT s 
       PRINT "FECHA",s,"SERIE",s,"NUMERO",s,"FPC",s,"NIT",s,"PROVEEDOR",s,
             "TOTAL COMPRAS FACTURAS O SERVCIOS Q.",s,"MONTO COMPRAS Q.",s,
             "MONTO SERVICIOS Q.",s,"MONTO COMBUSTIBLE Q.",s,
             "MONTO IDP/INGUAT Q.",s,"TOTAL OTR/DED Q.",s,
             "FACTURAS PEQUENO CONTRIBUYENTE",s,
             "NUMERO CHEQUE",s,"ESTABLECIMIENTO SAT",s,"SEDE",s,
             "TIPO DE COMPRA",s,"IVA CREDITO FISCAL Q."
       PRINT s 
    END IF 

   ON EVERY ROW
    -- Imprimiendo detalle libro compras
    
    -- Verificando si envio es a excel
    LET xnumdoc = imp1.numdoc 
    LET xnserie = imp1.nserie 
    LET xnitsoc = imp1.numnit 
    IF pipeline="excel" THEN
     LET vnumdoc = "'",imp1.numdoc 
     LET vnserie = "'",imp1.nserie 
     LET vnitsoc = "'",imp1.numnit 
     LET vtipdoc =  imp1.tipdoc     

     PRINT imp1.fecemi                       ,s,
           vnserie.trim()                    ,s,
           vnumdoc.trim()                    ,s, 
           vtipdoc.trim()                    ,s,
           vnitsoc.trim()                    ,s,
           imp1.nomsoc                       ,s, 
           imp1.totdoc USING "###,###,##&.&&",s,
           imp1.totcmp USING "###,###,##&.&&",s,
           imp1.totser USING "###,###,##&.&&",s,
           imp1.totedp USING "###,###,##&.&&",s,
           imp1.totipt USING "###,###,##&.&&",s,
           imp1.totded USING "###,###,##&.&&",s,
           imp1.totfpc USING "###,###,##&.&&",s,
           xdoctos                           ,s,
           imp1.nomest                       ,s, 
           imp1.nomcen                       ,s, 
           imp1.nomcom                       ,s, 
           imp1.totisv USING "###,###,##&.&&"
    ELSE
     LET xnomsoc = imp1.nomsoc[1,40] 
     PRINT imp1.fecemi                       ,s,
           xnserie                           ,s,
           xnumdoc                           ,s, 
           imp1.tipdoc                       ,s,
           xnitsoc                           ,s,
           xnomsoc                           ,s, 
           imp1.totdoc USING "###,###,##&.&&",s,
           imp1.totcmp USING "###,###,##&.&&",s,
           imp1.totser USING "###,###,##&.&&",s,
           imp1.totedp USING "###,###,##&.&&",s,
           imp1.totipt USING "###,###,##&.&&",s,
           imp1.totded USING "###,###,##&.&&",s,
           imp1.totfpc USING "###,###,##&.&&",s,
           xdoctos                           ,s,
           imp1.nomest                       ,s, 
           imp1.nomcen                       ,s, 
           imp1.nomcom                       ,s, 
           imp1.totisv USING "###,###,##&.&&"
    END IF 

   ON LAST ROW
    -- Totalizando reporte 
    -- Verificando si tipo de reporte no es a EXCEL
    IF (pipeline!="excel") THEN
     PRINT linea 
     PRINT 110 SPACES,"TOTALES -->",2 SPACES, 
           SUM(imp1.totdoc) USING "###,###,##&.&&",s,
           SUM(imp1.totcmp) USING "###,###,##&.&&",s,
           SUM(imp1.totser) USING "###,###,##&.&&",s,
           SUM(imp1.totedp) USING "###,###,##&.&&",s,
           SUM(imp1.totipt) USING "###,###,##&.&&",s,
           SUM(imp1.totded) USING "###,###,##&.&&",s,
           SUM(imp1.totfpc) USING "###,###,##&.&&",90 SPACES, 
           SUM(imp1.totisv) USING "###,###,##&.&&"
    ELSE
     PRINT s 
     PRINT s,s,s,s,s,"TOTALES",s, 
           SUM(imp1.totdoc) USING "###,###,##&.&&",s,
           SUM(imp1.totcmp) USING "###,###,##&.&&",s,
           SUM(imp1.totser) USING "###,###,##&.&&",s,
           SUM(imp1.totedp) USING "###,###,##&.&&",s,
           SUM(imp1.totipt) USING "###,###,##&.&&",s,
           SUM(imp1.totded) USING "###,###,##&.&&",s,
           SUM(imp1.totfpc) USING "###,###,##&.&&",s,s,s,s,s,
           SUM(imp1.totisv) USING "###,###,##&.&&"
    END IF 
END REPORT 
