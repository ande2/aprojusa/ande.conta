{
Programo : Mynor Ramirez 
Objetivo : Impresion de caja completa o por selecciona de gastos 
}

GLOBALS "facglb004.4gl" 
DEFINE w_mae_usr     RECORD LIKE glb_usuarios.*,
       existe        SMALLINT, 
       tipoimpresion SMALLINT,
       tituloreporte STRING,  
       filename      STRING,   
       pipeline      STRING,   
       i             INT
 
-- Subrutina para imprimir un movimiento de producto

FUNCTION facrpt004_CajaAdmon() 
 DEFINE imprimereporte SMALLINT
 DEFINE myHandler om.SaxDocumentHandler 

 -- Confirmando tipo de impresion 
 LET tipoimpresion = librut001_yesornot("Confirmacion",
                                        "Que desea imprimir ?",
                                        "Caja Completa",
                                        "Seleccion de Gastos", 
                                        "question")

 -- Definiendo archivo de impresion
 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/CajaAdmon.spl"

 -- Asignando valores
 LET pipeline = "pdf2"   

 -- Obteniendo datos de la empresa
 INITIALIZE w_mae_emp.* TO NULL 
 CALL librut003_BEmpresa(w_mae_tra.codemp)
 RETURNING w_mae_emp.*,existe 

 LET myHandler = gral_reporte("carta","vertical","PDF",80,"ANDE_Rep_238")
 
 -- Verificando tipo de impresion
 CASE (tipoimpresion)
  WHEN 1 -- Caja completa
   -- Iniciando reporte
   START REPORT facrpt004_PrintCajaAdmon --TO filename 
    OUTPUT TO REPORT facrpt004_PrintCajaAdmon()
   FINISH REPORT facrpt004_PrintCajaAdmon 
   LET imprimereporte = TRUE 
  WHEN 0 -- Seleccion de gastos 
   -- Seleccionando detalle de gastos por seleccion  
   LET imprimereporte = facrpt004_SeleccionGastos() 
   IF imprimereporte THEN
    -- Iniciando reporte
    START REPORT facrpt004_PrintDetalleGastos --TO filename 
     OUTPUT TO REPORT facrpt004_PrintDetalleGastos()
    FINISH REPORT facrpt004_PrintDetalleGastos   
   END IF 
 END CASE 

 -- Imprimiendo el reporte
 {IF imprimereporte THEN 
    
    CALL librut001_sendreport
         (filename,pipeline,tituloreporte CLIPPED,
         "--noline-numbers "||
         "--nofooter "||
         "--font-size 8 "||
         "--page-width 842 --page-height 595 "||
         "--left-margin 55 --right-margin 25 "||
         "--top-margin 35 --bottom-margin 45 "||
         "--title Gastos")
 END IF} 
END FUNCTION 

-- Subrutinar para generar la impresion de la caja  

REPORT facrpt004_PrintCajaAdmon()
 DEFINE linea     CHAR(158),
        wcencos   CHAR(40),
        xcencos   CHAR(19),
        xnomsoc   CHAR(45),
        xnomrub   CHAR(40), -- CHAR(19),
        xnomres   CHAR(19),
        xnomaut   CHAR(18),
        xnplaca   CHAR(10),
        xdescrp   CHAR(100)

  OUTPUT LEFT   MARGIN 2 
         RIGHT  MARGIN 2 
         PAGE   LENGTH 50 
         TOP    MARGIN 0 
         BOTTOM MARGIN 0 

 FORMAT 
  PAGE HEADER

   -- Definiendo linea de impresion 
   LET linea = "____________________________________________________________",
               "__________",
               "____________________________________________________________"
               --"__________________________________________________________"

   -- Imprimiendo Encabezado
   PRINT COLUMN   1,"Empresa         : ",
                    w_mae_emp.nomemp CLIPPED,
         COLUMN 110,PAGENO USING  "Pagina: <<"

   -- Obteniendo nombre del centro de costo de la caja 
   SELECT a.nomcen INTO wcencos FROM glb_cencosto a 
    WHERE a.codcos = w_mae_tra.cencos

   PRINT COLUMN   1,"Centro de Costo : ",
                    wcencos CLIPPED,
         COLUMN 110,"Fecha : ",TODAY USING "dd/mmm/yyyy"
   PRINT COLUMN   1,"facrpt004",
         COLUMN  52,"CAJA NUMERO ",w_mae_tra.numdoc USING "<<<<<<<<",
         COLUMN 110,"Hora  : ",TIME
   PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED
   PRINT linea
   PRINT COLUMN   1,"Total Caja        : ",w_mae_tra.totval USING "--,---,--&.&&",
         COLUMN  50,"Fecha de Apertura : ",w_mae_tra.fecsis USING "dd/mm/yyyy",
         COLUMN 097,"Fecha Liquidacion : ",w_mae_tra.fecliq USING "dd/mm/yyyy"
   PRINT COLUMN   1,"Total Gastos      : ",w_mae_tra.totgto USING "--,---,--&.&&",
         COLUMN  50,"Hora de Apertura  : ",w_mae_tra.horsis,
         COLUMN 097,"Hora Liquidacion  : ",w_mae_tra.horliq 
   PRINT COLUMN   1,"Saldo a Liquidar  : ",w_mae_tra.totsal USING "--,---,--&.&&",
         COLUMN  50,"Usuario Aperturo  : ",w_mae_tra.userid,
         COLUMN 097,"Usuario Liquid�   : ",w_mae_tra.usrliq 
   PRINT COLUMN   1,"Observaciones     : ",w_mae_tra.observ[1,100]
   PRINT COLUMN  22,w_mae_tra.observ[101,200]
   PRINT 1 SPACES

   -- Imprimiendo rotulo del detalle de productos
   PRINT "DETALLE DE GASTOS"
   PRINT linea
   --PRINT "Fecha Gasto Centro Costo   Rubro Gasto         Actividad     ",
   --      "Proveedor      Responsable       Autorizante     ",
   --      "Vehiculo   Valor Gasto"
   PRINT COLUMN 001, "Fecha Gasto",
         COLUMN 015, "Num Doc",
         COLUMN 027, "Proveedor",
         --COLUMN 017, "Centro Costo",
         COLUMN 070, "Rubro Gasto",
       --  COLUMN 038, "Actividad",
         --COLUMN 048, "Proveedor",
         --COLUMN 058, "Responsable",
         --COLUMN 070, "Autorizante",
         --COLUMN 082, "Vehiculo",
         COLUMN 115, "Valor Gasto"
   PRINT COLUMN 001, "Descripcion"
   PRINT linea

  ON EVERY ROW
   -- Imprimiendo detalle de gastos
   FOR i = 1 TO totlin
    -- Inicializando datos
    INITIALIZE xnomsoc,xnomrub,xnomres,xnplaca TO NULL 

    -- Obteniendo datos de la caja 
    SELECT a.nomrub INTO xnomrub FROM fac_rubgasto a 
     WHERE a.codrub = v_gastos[i].codrub 
    SELECT a.nomsoc INTO xnomsoc FROM glb_sociosng a 
     WHERE a.codsoc = v_gastos[i].codsoc

    -- Imprimiendo gastos 
    LET xdescrp = v_gastos[i].descrp 
    PRINT COLUMN 001, v_gastos[i].fecgto, --2 SPACES,
          COLUMN 015, v_gastos[i].ndocto CLIPPED, --2 SPACES, 
          COLUMN 027, xnomsoc CLIPPED, --2 SPACES, 
          COLUMN 070, xnomrub CLIPPED, --2 SPACES, 
          COLUMN 115, v_gastos[i].valgto USING "###,##&.&&" 
    PRINT COLUMN   1,xdescrp
   END FOR

  ON LAST ROW
   -- Imprimiendo totales y pie de pagina 
   PRINT linea
   PRINT COLUMN   1,"TOTAL GASTOS [ ",totlin USING "<<<<"," ] --->",
         COLUMN 149,w_mae_tra.totgto         USING "###,##&.&&" 
   SKIP 4 LINES
   PRINT "_________________________________________                     ",
         "____________________________________________________"
   PRINT "   Firma Responsable Registro de Gastos                       ",
         "   Firma Autorizado COMPRAS/CONTABILIDAD/GERENCIA"
END REPORT 

-- Subutina para seleccionar los gastos a imprimir

FUNCTION facrpt004_SeleccionGastos()           
 DEFINE loop,scr   SMALLINT,
        arr,i      SMALLINT, 
        avanza     SMALLINT,
        marcados   SMALLINT 

 -- Desplegando mensaje
 CALL librut001_dpelement("mensaje","[ Marcar los gastos a imprimir ]") 

 -- Iniciando el loop
 LET loop = TRUE
 WHILE loop

  -- Ingresando gastos 
  LET avanza = TRUE 
  INPUT ARRAY v_gastos WITHOUT DEFAULTS FROM s_gastos.*
   ATTRIBUTE(MAXCOUNT=totlin,INSERT ROW=FALSE,ACCEPT=FALSE,CANCEL=FALSE,
             UNBUFFERED=TRUE,FIELD ORDER FORM)

   ON ACTION imprimir 
    -- Aceptar
    EXIT INPUT

   ON ACTION cancel 
    -- Cancelar
    LET avanza = FALSE
    LET loop   = FALSE
    EXIT INPUT

   BEFORE INPUT 
    -- Desabilitando teclas
    CALL DIALOG.setActionHidden("append",TRUE)
    CALL DIALOG.setActionActive("delete",FALSE)
    {CALL DIALOG.setFieldActive("fecgto",FALSE)
    CALL DIALOG.setFieldActive("ndocto",FALSE)
    CALL DIALOG.setFieldActive("codrub",FALSE)
    CALL DIALOG.setFieldActive("codsoc",FALSE)
    CALL DIALOG.setFieldActive("codres",FALSE)
    CALL DIALOG.setFieldActive("codaut",FALSE)
    CALL DIALOG.setFieldActive("codveh",FALSE)
    CALL DIALOG.setFieldActive("descrp",FALSE)
    CALL DIALOG.setFieldActive("valgto",FALSE)}
    CALL DIALOG.setFieldActive("cheque",TRUE)
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Verificando si existe algun gasto marcado
  LET marcados = 0
  FOR i = 1 TO totlin
   IF v_gastos[i].cheque THEN
      LET marcados = (marcados+1)
   END IF 
  END FOR 
  IF marcados=0 THEN
     CALL fgl_winmessage(
     " Atencion:",
     " Debe marcarse al menos un gasto de la caja.",
     "stop")
     CONTINUE WHILE 
  END IF 

  LET loop = FALSE 
 END WHILE

 -- Desplegando mensaje
 CALL librut001_dpelement("mensaje","") 

 RETURN avanza 
END FUNCTION

-- Subrutinar para generar la impresion de detalle de gastos por seleccion  
-- de una caja 

REPORT facrpt004_PrintDetalleGastos() 
 DEFINE linea     CHAR(160),
        xdescrp   CHAR(100),
        xnomsoc   CHAR(30),
        xnomrub   CHAR(20),
        xnomres   CHAR(20),
        xnplaca   CHAR(10)

  OUTPUT LEFT   MARGIN 0 
         PAGE   LENGTH 50 
         TOP    MARGIN 0
         BOTTOM MARGIN 0

 FORMAT 
  PAGE HEADER

   -- Definiendo linea de impresion 
   LET linea = "____________________________________________________________",
               "____________________________________________________________",
               "____________________________________________________________"

   -- Imprimiendo Encabezado
   PRINT COLUMN   1,"Empresa : ",w_mae_tra.codemp USING "<<"," ",
                    w_mae_emp.nomemp CLIPPED,
         COLUMN 140,PAGENO USING  "Pagina: <<"
   PRINT COLUMN   1,"facrpt004",
         COLUMN  70,"CAJA NUMERO ",w_mae_tra.numdoc USING "<<<<<<<<",
         COLUMN 140,"Fecha : ",TODAY USING "dd/mmm/yyyy"
   PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
         COLUMN  71,"DETALLE DE GASTOS",
         COLUMN 140,"Hora  : ",TIME
   PRINT linea

   -- Imprimiendo rotulo del detalle de productos
   PRINT "DETALLE DE GASTOS"
   PRINT "Fecha Gasto Actividad             Destino del Gasto     Proveedor             ",
         "          Responsable           Vehiculo   Valor Gasto  Firma Responsable"      
   PRINT "Descripcion"              
   PRINT linea
   SKIP 1 LINES 

  ON EVERY ROW
   -- Imprimiendo detalle de gastos
   FOR i = 1 TO totlin
    -- Verificando si linea esta marcada para impresion 
    IF NOT v_gastos[i].cheque THEN
       CONTINUE FOR 
    END IF 
   
    -- Inicializando datos
    INITIALIZE xnomsoc,xnomrub,xnomres,xnplaca TO NULL 

    -- Obteniendo datos de la caja  
    SELECT a.nomsoc INTO xnomsoc FROM glb_sociosng a 
     WHERE a.codsoc = v_gastos[i].codsoc
    SELECT a.nomrub INTO xnomrub FROM fac_rubgasto a 
     WHERE a.codrub = v_gastos[i].codrub 

    -- Imprimiendo gastos 
    LET xdescrp = v_gastos[i].descrp 
    PRINT COLUMN   1,v_gastos[i].fecgto                              , 2 SPACES,
                     v_gastos[i].ndocto                              , 2 SPACES, 
                     xnomsoc                                         , 2 SPACES, 
                     xnomrub                                         , 2 SPACES, 
                     v_gastos[i].valgto       USING "###,##&.&&"     , 2 SPACES,
                     "_________________________" 
    PRINT COLUMN    1,xdescrp       

    SKIP 1 LINES 
   END FOR

  ON LAST ROW
   PRINT linea
   SKIP 4 LINES
   PRINT "_________________________________________                     ",
         "____________________________________________________"
   PRINT "   Firma Responsable Registro de Gastos                       ",
         "   Firma Autorizado COMPRAS/CONTABILIDAD/GERENCIA"
END REPORT 
