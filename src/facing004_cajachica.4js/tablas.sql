

drop   table "sistemas".fac_cajchica ; 
ereate table "sistemas".fac_cajchica 
  (
    lnkcaj serial not null ,
    codemp smallint not null ,
    cencos smallint not null ,
    tipcaj smallint not null ,
    numdoc integer not null ,
    fecemi date not null ,
    totrei decimal(14,2) not null ,
    salant decimal(14,2) not null ,
    totval decimal(14,2) not null ,
    totgto decimal(14,2) not null ,
    totsal decimal(14,2) not null ,
    numest smallint not null ,
    observ varchar(200,1),
    estado smallint not null ,
    usrliq varchar(15),
    fecliq date,
    horliq datetime hour to second,
    userid varchar(15,1) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (lnkcaj)  constraint "sistemas".pkfaccajchica
  );

drop   table "sistemas".fac_dcachica ; 
create table "sistemas".fac_dcachica 
  (
    lnkcaj integer not null ,
    correl smallint not null ,
    fecgto date not null ,
    tipdoc char(5) not null ,
    nserie char(20) not null ,
    ndocto char(20) not null ,
    codsoc smallint not null ,
    codrub smallint not null ,
    descrp char(100) not null ,
    tipdlg smallint,
    tipcom smallint not null ,
    valgto decimal(12,2) not null ,
    totedp decimal(11,2) not null ,
    totigt decimal(11,2) not null ,
    totgra decimal(14,2),
    totexe decimal(14,2),
    totisv decimal(14,2),
    porisv decimal(5,2)
  );

alter table "sistemas".fac_dcachica add constraint (foreign key 
    (lnkcaj) references "sistemas".fac_cajchica  on delete cascade 
    constraint "sistemas".fkfaccajchica1 index disabled);


create table "sistemas".glb_tiposcaj 
  (
    tipcaj smallint,
    nomtip char(30),
    userid varchar(15),
    fecsis date,
    horsis datetime hour to second,
    primary key (tipcaj)  constraint "sistemas".pkglbtiposcaj
  );


create table "sistemas".glb_tiposdlg 
  (
    tipdlg smallint,
    nomdlg char(50),
    userid varchar(15),
    fecsis date,
    horsis datetime hour to second,
    primary key (tipdlg)  constraint "sistemas".pkglbtiposdlg
  );
