{ 
Programo : Mynor Ramirez
Objetivo : Programa de criterios de seleccion para:
           Consultar/Modificar/Liquidar/Abrir/Imprimir una caja administracion 
} 

-- Definicion de variables globales 
GLOBALS "facglb004.4gl"

-- Subrutina para consultar/

FUNCTION facqbx004_CajaAdmon(operacion)
 DEFINE qrytext,qrypart     CHAR(500),
        loop,existe         SMALLINT,
        operacion,res       SMALLINT,
        qryres,msg,titqry   CHAR(80),
        wherestado          STRING,
        opc,totreg          INTEGER 

 -- Verificando operacion
 CASE (operacion)
  WHEN 1 LET wherestado  = NULL
  WHEN 2 LET wherestado  = " AND a.estado = 0" 
  WHEN 3 LET wherestado  = " AND a.estado = 0" 
  WHEN 6 LET wherestado  = " AND a.estado = 1" 
  WHEN 7 LET wherestado  = " AND a.estado = 0" 
 END CASE 

 -- Definiendo nivel de aislamiento 
 SET ISOLATION TO DIRTY READ 

 -- Cargando combobox de centros de costo de la caja 
 CALL librut003_CbxCentrosCosto("cencos")
 -- Cargando combobox de tipos de caja
 CALL librut003_CbxTiposCaja(username)
 -- Cargando combobox de establecimientos sat
 CALL librut003_CbxEstablecimientosSAT() 
 -- Cargando combobox de tipos de diligencia
 CALL librut003_CbxTiposDiligencias()
 -- Llenando comboox de tipos de documento del regimen fiscal
 CALL librut003_CbxTiposDocRegimenFisTodos()

 -- Buscando datos 
 LET loop = TRUE 
 WHILE loop 
  -- Inicializando las variables 
  CALL facqbx004_EstadoMenu(operacion," ")
  INITIALIZE qrytext,qrypart TO NULL
  LET int_flag = 0 
  CLEAR FORM

  -- Construyendo busqueda 
  CONSTRUCT BY NAME qrytext 
                 ON a.codemp, 
                    a.cencos, 
                    a.tipcaj, 
                    a.numdoc,
                    a.numest, 
                    a.observ,
                    a.totrei,
                    a.salant, 
                    a.totval,
                    a.totgto,
                    a.totsal,
                    a.userid,
                    a.fecsis,
                    a.horsis,
                    a.usrliq,
                    a.fecliq,
                    a.horliq,
                    a.lnkcaj 

   ON ACTION cancel
    -- Salida
    LET loop = FALSE
    EXIT CONSTRUCT
  END CONSTRUCT
  IF NOT loop THEN
     EXIT WHILE
  END IF 

  -- Preparando la busqueda 
  ERROR " Seleccionando datos ... por favor espere ..." ATTRIBUTE(CYAN) 

  -- Creando la busqueda 
  LET qrypart = "SELECT a.lnkcaj ",
                 "FROM  fac_cajchica a ",
                 "WHERE ",qrytext CLIPPED,wherestado CLIPPED, 
                 " ORDER BY a.lnkcaj DESC" 

  -- Contando registros
  LET totreg = librut003_NumeroRegistros(qrypart)

  -- Declarando el cursor 
  PREPARE estqbx004 FROM qrypart
  DECLARE c_caja SCROLL CURSOR WITH HOLD FOR estqbx004  
  OPEN c_caja 
  FETCH FIRST c_caja INTO w_mae_tra.lnkcaj
  IF (status = NOTFOUND) THEN
     INITIALIZE w_mae_tra.* TO NULL
     ERROR ""
     CALL fgl_winmessage(
     "Atencion",
     "No existen cajas con el criterio seleccionado.",
     "stop") 
  ELSE
     ERROR "" 

     -- Desplegando datos 
     LET msg = " - Registros [ ",totreg||" ]"
     CALL facqbx004_EstadoMenu(operacion,msg CLIPPED)
     CALL facqbx004_datos()

     -- Fetchando cajas 
     MENU "Cajas" 
      BEFORE MENU 
       -- Verificando tipo de operacion 
       CASE (operacion)
        WHEN 1 HIDE OPTION "Modificar" 
               HIDE OPTION "Liquidar"
               HIDE OPTION "Abrir"     
               HIDE OPTION "delete"   
        WHEN 2 HIDE OPTION "Liquidar"
               HIDE OPTION "Abrir" 
               HIDE OPTION "delete"   
        WHEN 3 HIDE OPTION "Modificar" 
               HIDE OPTION "Abrir"    
               HIDE OPTION "delete"   
        WHEN 6 HIDE OPTION "Modificar" 
               HIDE OPTION "Liquidar" 
               HIDE OPTION "delete"   
        WHEN 7 HIDE OPTION "Modificar" 
               HIDE OPTION "Liquidar" 
               HIDE OPTION "Abrir" 
               HIDE OPTION "imprimir" 
               HIDE OPTION "Gastos" 
       END CASE 

      COMMAND "Consultar" 
       EXIT MENU

      ON ACTION modificar  
       -- Modificando caja  
       CALL facing004_CajaAdmon(2) 

       -- Desplegando datos
       CALL facqbx004_datos()

      ON ACTION delete 
       -- Eliminando caja  
       IF facqbx004_EliminarCaja() THEN 
          EXIT MENU 
       END IF 

      ON ACTION liquidar  
       -- Verificando si la caja ya fue liquidada
       IF w_mae_tra.estado=1 THEN
          CALL fgl_winmessage(
          "Atencion","Caja ya fue liquidada.\nVERIFICA.","stop")
          CONTINUE MENU 
       END IF 
      
       -- Liquidando caja   
       CALL facqbx004_LiquidarAbrirCaja(1,"Liquidar")         

       -- Desplegando datos
       CALL facqbx004_datos()

      ON ACTION abrir 
       -- Verificando si la caja ya fue abierta 
       IF w_mae_tra.estado=0 THEN
          CALL fgl_winmessage(
          "Atencion","Caja ya fue abierta.\nVERIFICA.","stop")
          CONTINUE MENU 
       END IF 

       -- Verificando si la caja ya tiene pago  
       IF w_mae_tra.lnkbco>0 THEN
          CALL fgl_winmessage(
          "Atencion","Caja ya tiene pago registrado.\nCaja no puede abrirse.","stop")
          CONTINUE MENU 
       END IF 
      
       -- Abriendo caja     
       CALL facqbx004_LiquidarAbrirCaja(2,"Abrir")         

       -- Desplegando datos
       CALL facqbx004_datos()

      COMMAND "Gastos"
       CALL facqbx004_VerDetalleGastos()

      ON ACTION imprimir
       -- Impresion de caja 
       CALL facrpt004_CajaAdmon()

       -- Desplegando datos
       CALL facqbx004_datos()

      ON ACTION anterior   
       FETCH NEXT c_caja INTO w_mae_tra.lnkcaj 
        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas cajas anteriores en lista.", 
           "information")
           FETCH LAST c_caja INTO w_mae_tra.lnkcaj
        END IF 

       -- Desplegando datos 
       CALL facqbx004_datos()

      ON ACTION siguiente  
       FETCH PREVIOUS c_caja INTO w_mae_tra.lnkcaj
        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas cajas siguientes en lista.", 
           "information")
           FETCH FIRST c_caja INTO w_mae_tra.lnkcaj
        END IF

       -- Desplegando datos 
       CALL facqbx004_datos()

      ON ACTION primero  
       FETCH FIRST c_caja INTO w_mae_tra.lnkcaj
        -- Desplegando datos 
        CALL facqbx004_datos()

      ON ACTION ultimo  
       FETCH LAST c_caja INTO w_mae_tra.lnkcaj
        -- Desplegando datos
        CALL facqbx004_datos()

      ON ACTION cancel
       LET loop = FALSE
       EXIT MENU

      COMMAND KEY(F4,CONTROL-E)
       EXIT MENU
     END MENU
   END IF     
  CLOSE c_caja
 END WHILE

 -- Desplegando estado del menu 
 CALL facqbx004_EstadoMenu(0,"")

 -- Inicializando datos 
 CALL facing004_inival(1)
END FUNCTION 

-- Subrutina para desplegar los datos de la caja  

FUNCTION facqbx004_datos()
 DEFINE existe SMALLINT

 -- Desplegando datos 
 CLEAR FORM

 -- Obteniendo datos de la caja 
 CALL librut003_BCajaAdmon(w_mae_tra.lnkcaj)
 RETURNING w_mae_tra.*,existe

 -- Obteniendo datos del tipo de caja
 LET IngresaSaldoCaja=NULL 
 SELECT a.valsal
  INTO  IngresaSaldoCaja 
  FROM  glb_tiposcaj a
  WHERE a.tipcaj = w_mae_tra.tipcaj 

 -- Desplegando datos del documento 
 CLEAR FORM 
 DISPLAY BY NAME w_mae_tra.codemp,w_mae_tra.cencos,w_mae_tra.numdoc,
                 w_mae_tra.totval,w_mae_tra.totgto,w_mae_tra.totsal,
                 w_mae_tra.observ,w_mae_tra.userid,w_mae_tra.fecsis,
                 w_mae_tra.horsis,w_mae_tra.usrliq,w_mae_tra.fecliq,
                 w_mae_tra.horliq,w_mae_tra.numest,w_mae_tra.totrei,
                 w_mae_tra.salant,w_mae_tra.tipcaj,w_mae_tra.lnkcaj 

 -- Seleccionando detalle de gastos
 CALL facqbx004_detallegastos(w_mae_tra.lnkcaj)           
END FUNCTION 

-- Subrutina para seleccionar los datos del detalle de gastos de la caja  

FUNCTION facqbx004_DetalleGastos(wlnkcaj)
 DEFINE existe     SMALLINT,
        wlnkcaj    INT 

 -- Cargando combobox de socios de negocio  
 CALL librut003_CbxProveedores()
 -- Cargando combobox de rubros de gasto
 CALL librut003_CbxRubrosGasto()

 -- Inicializando vector de gastos
 CALL facing004_inivec()

 -- Seleccionando detalle de gastos
 DECLARE cdet CURSOR FOR
 SELECT x.fecgto,
        x.feclbc, 
        x.nserie,
        x.ndocto,
        x.codsoc,
        x.codrub, 
        x.tipdoc,
        x.tipcom, 
        x.valgto,
        x.totedp,
        x.totigt,
        x.totded, 
        x.totgra,
        x.totexe,
        x.totisv,
        x.descrp,
        x.tipdlg,
        x.porisv, 
        0,  
        x.correl
  FROM  fac_dcachica x 
  WHERE (x.lnkcaj = wlnkcaj)
  ORDER BY x.correl 

  LET totlin = 1 
  FOREACH cdet INTO v_gastos[totlin].*
   -- Acumulando contador
   LET totlin = (totlin+1) 
  END FOREACH
  CLOSE cdet
  FREE  cdet
  LET totlin = (totlin-1)  

  -- Despelgando datos del detalle
  DISPLAY ARRAY v_gastos TO s_gastos.*
   ATTRIBUTE(COUNT=totlin) 
   BEFORE DISPLAY
    EXIT DISPLAY
  END DISPLAY 
END FUNCTION 

-- Subrutina para ver el detalle de gastos de la caja 

FUNCTION facqbx004_VerDetalleGastos()
 -- Desplegando gastos
 DISPLAY ARRAY v_gastos TO s_gastos.*
  ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE)

  ON ACTION cancel 
   -- Salida
   EXIT DISPLAY 

  BEFORE ROW 
   -- Verificando control del total de lineas
   IF (ARR_CURR()>totlin) THEN
      CALL FGL_SET_ARR_CURR(1)
   END IF
 END DISPLAY 
END FUNCTION 

-- Subrutina para liquidar o abrir una caja 

FUNCTION facqbx004_LiquidarAbrirCaja(operacion,xestd)      
 DEFINE xtext,xestd,msg STRING, 
        operacion       SMALLINT

 -- Verificando tipo de caja
 IF (w_mae_tra.tipcaj<=2) THEN 
    LET xtext = "Caja" 
 ELSE
    LET xtext = "Liquidacion" 
 END IF 

 -- Confirmando liquidacion 
 IF NOT librut001_yesornot("Confirmacion",
                           "Desea "||xestd||" "||xtext||" ?",
                           "Si",
                           "No",
                           "question") THEN
    RETURN 
 END IF

 -- Iniciando Transaccion
 BEGIN WORK

  -- Verificando operacion 
  CASE (operacion) 
   WHEN 1 -- Marcando caja de liquidada 
    UPDATE fac_cajchica
    SET    fac_cajchica.estado = 1, 
           fac_cajchica.usrliq = USER,
           fac_cajchica.fecliq = CURRENT,
           fac_cajchica.horliq = CURRENT
    WHERE (fac_cajchica.lnkcaj = w_mae_tra.lnkcaj)

    LET msg = xtext||" # "||w_mae_tra.numdoc||" liquidada." 
   WHEN 2 -- Marcando caja de abierta 
    UPDATE fac_cajchica
    SET    fac_cajchica.estado = 0, 
           fac_cajchica.usrliq = NULL, 
           fac_cajchica.fecliq = NULL, 
           fac_cajchica.horliq = NULL 
    WHERE (fac_cajchica.lnkcaj = w_mae_tra.lnkcaj)

    LET msg = xtext||" # "||w_mae_tra.numdoc||" abierta."
  END CASE 

 -- Finalizando Transaccion
 COMMIT WORK

 -- Desplegando mensaje 
 CALL fgl_winmessage("Atencion",msg,"information")
END FUNCTION 

-- Subrutina para eliminar una caja 

FUNCTION facqbx004_EliminarCaja()      
 -- Confirmando eliminacion 
 IF NOT librut001_yesornot("Confirmacion",
                           "Desea Eliminar Caja ?",
                           "Si",
                           "No",
                           "question") THEN

    RETURN FALSE 
 END IF

 -- Iniciando Transaccion
 BEGIN WORK

  -- Eliminando caja
  DELETE FROM fac_cajchica
  WHERE (fac_cajchica.lnkcaj = w_mae_tra.lnkcaj)

 -- Finalizando Transaccion
 COMMIT WORK

 CALL fgl_winmessage(
 " Atencion",
 " Caja # "||w_mae_tra.numdoc||" eliminada.",
 "information")

 RETURN TRUE 
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION facqbx004_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement(
         "group1","Datos de la Caja - MENU")
  WHEN 1 CALL librut001_dpelement(
         "group1","Datos de la Caja - CONSULTAR"||msg CLIPPED) 
  WHEN 2 CALL librut001_dpelement(
         "group1","Datos de la Caja - MODIFICAR"||msg CLIPPED)
  WHEN 3 CALL librut001_dpelement(
         "group1","Datos de la Caja - LIQUIDAR"||msg CLIPPED)
  WHEN 4 CALL librut001_dpelement(
         "group1","Datos de la Caja - INGRESAR")
  WHEN 6 CALL librut001_dpelement(
         "group1","Datos de la Caja - ABRIR"||msg CLIPPED)
  WHEN 7 CALL librut001_dpelement(
         "group1","Datos de la Caja - ELIMINAR"||msg CLIPPED)
 END CASE
END FUNCTION
