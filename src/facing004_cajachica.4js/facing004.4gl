{
Programo : Mynor Ramirez
Objetivo : Mantenimiento de cajas admistracion 
}

-- Definicion de variables globales

GLOBALS "facglb004.4gl"
CONSTANT programa = "facing004" 
CONSTANT TipoCompraDefault   = 1 

DEFINE wpais            VARCHAR(255), 
       porcenisv        DEC(5,2), 
       conteo           INTEGER,  
       regreso          SMALLINT,
       existe           SMALLINT,
       msg              STRING 

-- Subrutina principal

MAIN
 -- Atrapando interrupts 
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 IF isGDC() THEN 
   CALL ui.Interface.loadStyles("styles")
 END IF
 
 CALL ui.Interface.loadToolbar("ToolBarCajaChica")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("cajaadmon")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ 

 -- Obteniendo datos del pais 
 CALL librut003_parametros(1,0)
 RETURNING existe,wpais

 -- Obteniendo usuario del sistema
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de opciones
 CALL facing004_menu()
END MAIN

-- Subutina para el menu de cajas 

FUNCTION facing004_menu()
 -- Abriendo la ventana del mantenimiento
 OPEN WINDOW wing004a AT 5,2  
  WITH FORM "facing004a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL librut001_header("facing004",wpais,1)

  -- Obteniendo porcentaje de impuesto sobre venta
  CALL librut003_parametros(8,1)
  RETURNING existe,porcenisv
  IF NOT existe THEN
     LET porcenisv = 0
  END IF

  -- Inicializando datos 
  CALL facing004_inival(1)

  -- Menu de opciones
  MENU "Opciones" 
   BEFORE MENU
    -- Verificando accesos a opciones
    -- Consultar
    IF NOT seclib001_accesos(progname,4,username) THEN
       HIDE OPTION "consultar"
    END IF
    -- Ingresar 
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "ingresar" 
    END IF
    -- Modificar 
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "modificar"
    END IF
    -- Liquidar 
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "liquidar" 
    END IF
    -- Abrir     
    IF NOT seclib001_accesos(progname,6,username) THEN
       HIDE OPTION "abrir"
    END IF
    -- Parametros 
    IF NOT seclib001_accesos(progname,7,username) THEN
       HIDE OPTION "parametros"
    END IF
    -- Eliminar
    IF NOT seclib001_accesos(progname,8,username) THEN
       HIDE OPTION "delete"
    END IF 

    -- Obteniendo parametros de pagos de socios de negocios 
    INITIALIZE w_mae_par.* TO NULL
    SELECT a.* INTO w_mae_par.* FROM fac_paramtrs a

   ON ACTION consultar  
    CALL facqbx004_CajaAdmon(1)
   ON ACTION ingresar 
    CALL facing004_CajaAdmon(1) 
   ON ACTION modificar  
    CALL facqbx004_CajaAdmon(2)
   ON ACTION liquidar  
    CALL facqbx004_CajaAdmon(3)
   ON ACTION abrir     
    CALL facqbx004_CajaAdmon(6)
   ON ACTION delete 
    CALL facqbx004_CajaAdmon(7) 
   ON ACTION parametros 
    CALL facing004_ParametrosCaja() 
   ON ACTION salir  
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU  
  END MENU
 CLOSE WINDOW wing004a
END FUNCTION

-- Subrutina para el ingreso de los datos del encabezado de la caja 

FUNCTION facing004_CajaAdmon(operacion)
 DEFINE w_mae_caj         RECORD LIKE fac_cajchica.*,
        retroceso         SMALLINT,
        operacion         SMALLINT,
        loop,existe,i     SMALLINT,
        conteo            INT 

 -- Desplegando estado del menu
 CALL facqbx004_EstadoMenu(4,"")

 -- Inicio del loop
 LET loop  = TRUE
 WHILE loop   
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Verificando si operacion es ingreso
     IF (operacion=1) THEN 
        -- Inicializando datos 
        CALL facing004_inival(1) 
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_tra.codemp,
                w_mae_tra.cencos,
                w_mae_tra.tipcaj, 
                w_mae_tra.totrei,
                w_mae_tra.salant, 
                w_mae_tra.numest, 
                w_mae_tra.observ 
                WITHOUT DEFAULTS
    ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=FALSE)

   ON ACTION cancel 
    -- Salida 
    IF (operacion=1) THEN  -- Ingreso 
     IF INFIELD(codemp) THEN
        LET loop = FALSE
        EXIT INPUT
     ELSE
        -- Inicializando 
        CALL facing004_inival(1)
        NEXT FIELD codemp 
     END IF 
    ELSE                 -- Modifidacion 
     LET loop = FALSE 
     EXIT INPUT 
    END IF 

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   ON CHANGE codemp 
    -- Obteniendo datos de la empresa 
    INITIALIZE w_mae_emp.* TO NULL 
    CALL librut003_bempresa(w_mae_tra.codemp)
    RETURNING w_mae_emp.*,existe 

   BEFORE INPUT 
    -- Verificando operacion
    IF (operacion=2) THEN
       CALL Dialog.SetFieldActive("codemp",0) 
       CALL Dialog.SetFieldActive("cencos",0) 
       CALL Dialog.SetFieldActive("tipcaj",0) 
       CALL Dialog.SetFieldActive("totrei",0) 
       CALL Dialog.SetFieldActive("salant",0) 
       NEXT FIELD numest 
    END IF 

   BEFORE FIELD codemp 
    -- Verificando retroceso 
    IF (operacion=1) THEN 
       IF retroceso THEN
          NEXT FIELD totrei 
       END IF 
    END IF 

   AFTER FIELD codemp 
    -- Verificando codemp
    IF w_mae_tra.codemp IS NULL THEN
       ERROR "Error: empresa invalida, VERIFICA."
       NEXT FIELD codemp 
    END IF 

   AFTER FIELD cencos 
    -- Verificando cencos 
    IF w_mae_tra.cencos IS NULL THEN
       ERROR "Error: centro de costo invalido, VERIFICA."
       NEXT FIELD cencos 
    END IF 

   AFTER FIELD tipcaj 
    -- Verificando tipo de caja 
    IF w_mae_tra.tipcaj IS NULL THEN
       ERROR "Error: tipo de caja invalido, VERIFICA."
       NEXT FIELD tipcaj 
    END IF 

    -- Verificando si existe caja abierta
    SELECT COUNT(*)
     INTO  conteo 
     FROM  fac_cajchica a
     WHERE a.codemp = w_mae_tra.codemp 
       AND a.cencos = w_mae_tra.cencos 
       AND a.tipcaj = w_mae_tra.tipcaj 
       AND a.estado = 0 
     IF (conteo=1) THEN 
        -- Buscando datos de la caja abierta
        INITIALIZE w_mae_caj.* TO NULL 
        SELECT a.*
         INTO  w_mae_caj.*
         FROM  fac_cajchica a 
         WHERE a.codemp = w_mae_tra.codemp
           AND a.cencos = w_mae_tra.cencos 
           AND a.tipcaj = w_mae_tra.tipcaj 
           AND a.estado = 0 

        -- Desplegando datos 
        LET w_mae_tra.* = w_mae_caj.*
        DISPLAY BY NAME w_mae_tra.numdoc, w_mae_tra.totval,
                        w_mae_tra.totgto, w_mae_tra.totsal,
                        w_mae_tra.observ, w_mae_tra.userid,
                        w_mae_tra.fecsis, w_mae_tra.horsis,
                        w_mae_tra.usrliq, w_mae_tra.fecliq,
                        w_mae_tra.horliq, w_mae_tra.numest,
                        w_mae_tra.totrei, w_mae_tra.salant 

        -- Desplegando detalle de gastos
        CALL facqbx004_DetalleGastos(w_mae_tra.lnkcaj) 
        CALL facing004_totdet()

        CALL fgl_winmessage(
        "Atencion:",
        "Ya existe la Caja Numero "||w_mae_tra.numdoc||" abierta. \n"||
        "No puede abrirse una nueva sin antes liquidar la actual.",
        "stop")
        LET loop = FALSE 
        EXIT INPUT 
     ELSE
        -- Asignando correlativo de la caja nueva 
        SELECT (NVL(MAX(a.numdoc),0)+1)
         INTO  w_mae_tra.numdoc 
         FROM  fac_cajchica a
         WHERE a.codemp = w_mae_tra.codemp 
           AND a.cencos = w_mae_tra.cencos 
           AND a.tipcaj = w_mae_tra.tipcaj 

        -- Obteniendo total reintegro y saldo anterior de la caja anterior
        SELECT NVL(a.totgto,0),NVL(a.totsal,0) 
         INTO  w_mae_tra.totrei,w_mae_tra.salant 
         FROM  fac_cajchica a
         WHERE a.lnkcaj = (SELECT MAX(x.lnkcaj) FROM fac_cajchica x
                            WHERE x.lnkcaj IS NOT NULL
                              AND x.codemp = a.codemp
                              AND x.cencos = a.cencos
                              AND x.tipcaj = a.tipcaj 
                              AND x.estado = 1) 
           AND a.codemp = w_mae_tra.codemp 
           AND a.cencos = w_mae_tra.cencos 
           AND a.tipcaj = w_mae_tra.tipcaj 
           AND a.estado = 1 

        -- Verificando si saldo anterior es negativo, lo pone igual a cero
        IF (w_mae_tra.salant<0) THEN LET w_mae_tra.salant = 0 END IF 

        -- Obteniendo si el tipo de caja valida saldos
        LET IngresaSaldoCaja=NULL 
        SELECT a.valsal
         INTO  IngresaSaldoCaja 
         FROM  glb_tiposcaj a
         WHERE a.tipcaj = w_mae_tra.tipcaj 
         IF (IngresaSaldoCaja=1) THEN
            LET w_mae_tra.totval = (w_mae_tra.totrei+w_mae_tra.salant) 
            CALL Dialog.SetFieldActive("totrei",1) 
            CALL Dialog.SetFieldActive("salant",1) 
         ELSE 
            LET w_mae_tra.totval = 0 
            LET w_mae_tra.totrei = 0
            LET w_mae_tra.salant = 0 
            CALL Dialog.SetFieldActive("totrei",0) 
            CALL Dialog.SetFieldActive("salant",0) 
         END IF 

        -- Desplegando totales      
        CALL facing004_totdet()
        DISPLAY BY NAME w_mae_tra.numdoc,w_mae_tra.totrei,
                        w_mae_tra.salant,w_mae_tra.totval 
     END IF 

   BEFORE FIELD totrei 
    LET retroceso = FALSE 

   ON CHANGE totrei 
    -- Calculando total de la caja 
    LET w_mae_tra.totval = (w_mae_tra.totrei+w_mae_tra.salant) 
    CALL facing004_totdet()
    DISPLAY BY NAME w_mae_tra.totval 

   AFTER FIELD totrei 
    -- Verificando total reintegro 
    IF w_mae_tra.totrei IS NULL OR
       w_mae_tra.totrei <0 THEN
       ERROR "Error: total reintegro invalido, VERIFICA."
       NEXT FIELD totrei 
    END IF 

    -- Calculando total de la caja 
    LET w_mae_tra.totval = (w_mae_tra.totrei+w_mae_tra.salant) 
    CALL facing004_totdet()
    DISPLAY BY NAME w_mae_tra.totval 

   ON CHANGE salant 
    -- Calculando total de la caja 
    LET w_mae_tra.totval = (w_mae_tra.totrei+w_mae_tra.salant) 
    CALL facing004_totdet()
    DISPLAY BY NAME w_mae_tra.totval 

   AFTER FIELD salant 
    -- Verificando saldo anterior  
    IF w_mae_tra.salant IS NULL OR
       w_mae_tra.salant <0 THEN
       ERROR "Error: saldo anterior invalido, VERIFICA."
       NEXT FIELD salant 
    END IF 

    -- Calculando total de la caja 
    LET w_mae_tra.totval = (w_mae_tra.totrei+w_mae_tra.salant) 
    CALL facing004_totdet()
    DISPLAY BY NAME w_mae_tra.totval 

   AFTER FIELD numest
    -- Verificando numero establecimiento
    IF w_mae_tra.numest IS NULL THEN
       ERROR "Error: numero de establecimiento invalido, VERIFICA." 
       NEXT FIELD numest 
    END IF 
  END INPUT 
  IF NOT loop THEN
     EXIT WHILE 
  END IF 

  -- Ingresando detalle del movimiento 
  LET retroceso = facing004_detallegastos(operacion) 

  -- Si operacion es modificar 
  IF (operacion=2) THEN
     LET loop = FALSE
  END IF 
 END WHILE

 -- Inicializando datos 
 IF (operacion=1) THEN 
    CALL facing004_inival(1) 
 END IF 

 -- Desplegando estado del menu
 CALL facqbx004_EstadoMenu(0,"")
END FUNCTION

-- Subutina para el ingreso del detalle de gastos de la caja

FUNCTION facing004_detallegastos(operacion)
 DEFINE loop,scr   SMALLINT,
        xcodgru    SMALLINT, 
        opc,arr,i  SMALLINT, 
        linea      SMALLINT, 
        retroceso  SMALLINT, 
        operacion  SMALLINT, 
        lastkey    INTEGER  

 -- Iniciando el loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop

  -- Ingresando gastos 
  INPUT ARRAY v_gastos WITHOUT DEFAULTS FROM s_gastos.*
   ATTRIBUTE(COUNT=totlin,INSERT ROW=FALSE,ACCEPT=FALSE,CANCEL=FALSE,
             UNBUFFERED=TRUE,FIELD ORDER FORM)

   ON ACTION accept 
    -- Aceptar
    EXIT INPUT

   ON ACTION cancel 
    -- Cancelar
    LET retroceso = TRUE
    LET loop      = FALSE
    EXIT INPUT

   ON ACTION DELETE
    -- Eliminando linea seleccionada
    LET arr = ARR_CURR()
    CALL v_gastos.deleteElement(arr)

    -- Totalizando
    CALL facing004_totdet()
    NEXT FIELD fecgto

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   BEFORE INPUT 
    -- Desabilitando tecla de append 
    CALL DIALOG.setActionHidden("append",TRUE)
    CALL DIALOG.setFieldActive("cheque",FALSE)

    -- Cargando combobox de socios de negocios 
    CALL librut003_CbxProveedores() 
    -- Cargando combobox de rubros de gasto 
    CALL librut003_CbxRubrosGasto()
    -- Cargando combobox de tipos de diligencia 
    CALL librut003_CbxTiposDiligencias() 
    -- Llenando comboox de tipos de documento del regimen fiscal
    CALL librut003_CbxTiposDocRegimenFisTodos()

   BEFORE FIELD fecgto 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Habilitando tecla de accept 
    CALL DIALOG.SetActionActive("accept",TRUE)
    LET v_gastos[arr].cheque = 0 

   AFTER FIELD fecgto
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) THEN
     IF v_gastos[arr].fecgto IS NULL THEN 
       NEXT FIELD fecgto 
     END IF 
    END IF

   BEFORE FIELD feclbc 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()
    
    -- Verificando fecha 
    IF v_gastos[arr].fecgto IS NULL OR 
       v_gastos[arr].fecgto <=(TODAY-w_mae_par.diaing) OR 
       v_gastos[arr].fecgto >TODAY THEN 
       CALL fgl_winmessage
       ("Atencion",
        "Fecha del gasto invalida."||
        "\nVerifica fecha del gasto y dias permitidos.",
        "stop")
       NEXT FIELD fecgto 
    END IF 

    -- Asgnando fecha del libro de compras
    IF v_gastos[arr].feclbc IS NULL THEN
     LET v_gastos[arr].feclbc = v_gastos[arr].fecgto 
    END IF 

    -- Des-Habilitando tecla de accept 
    CALL DIALOG.SetActionActive("accept",FALSE)

   AFTER FIELD feclbc 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando fecha 
    IF v_gastos[arr].feclbc IS NULL OR
       v_gastos[arr].feclbc < v_gastos[arr].fecgto OR     
       v_gastos[arr].feclbc > (TODAY+w_mae_par.diapos) THEN 
       CALL fgl_winmessage
       ("Atencion",
        "Fecha del libro de compras invalida."||
        "\nVerifica fecha del gasto y dias permitidos.",
        "stop")
       NEXT FIELD feclbc 
    END IF 

   AFTER FIELD nserie
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando serie documento
    IF LENGTH(v_gastos[arr].nserie)=0 THEN          
       ERROR "Error: numero serie invalida, VERIFICA."
       LET v_gastos[arr].nserie = NULL 
       CLEAR s_gastos[scr].nserie 
       NEXT FIELD nserie 
    END IF 

   AFTER FIELD ndocto 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando numero documento
    IF LENGTH(v_gastos[arr].ndocto)=0 THEN          
       ERROR "Error: numero documento invalido, VERIFICA."
       LET v_gastos[arr].ndocto = NULL 
       CLEAR s_gastos[scr].ndocto 
       NEXT FIELD ndocto 
    END IF 

   AFTER FIELD codsoc 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()
    
    -- Verificando proveedor 
    IF v_gastos[arr].codsoc IS NULL THEN 
       ERROR "Error: proveedor invalido, VERIFICA."
       NEXT FIELD codsoc 
    END IF 

   AFTER FIELD codrub 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()
    
    -- Verificando rubro 
    IF v_gastos[arr].codrub IS NULL THEN 
       ERROR "Error: rubro invalido, VERIFICA."
       NEXT FIELD codrub 
    END IF 

   BEFORE FIELD tipdoc 
    -- Obteniendo regimen fiscal del socio de negocios
    LET xcodgru = NULL 
    SELECT a.codgru INTO  xcodgru FROM  glb_sociosng a
     WHERE a.codsoc = v_gastos[arr].codsoc 

   AFTER FIELD tipdoc 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()
    
    -- Verificando tipo documento 
    IF v_gastos[arr].tipdoc IS NULL THEN 
       ERROR "Error: tipo documento invalido, VERIFICA."
       NEXT FIELD tipdoc 
    END IF 

    -- Verificando si tipo de documento aplica
    SELECT COUNT(*) 
     INTO  conteo
     FROM  glb_dregmfis a
     WHERE a.codgru = xcodgru 
       AND a.tipdoc = v_gastos[arr].tipdoc
     IF (conteo=0) THEN
        CALL fgl_winmessage
        ("Atencion",
         "Tipo documento no aplica para este socio de negocios."||
         "\nVerifica el regimen fiscal el socio de negocios.",
         "stop")
        NEXT FIELD tipdoc 
     END IF 

   BEFORE FIELD tipcom 
    -- Verificando si tipo de compra es blanco asigna COMPRAS 
    IF v_gastos[arr].tipcom IS NULL THEN 
       LET v_gastos[arr].tipcom = TipoCompraDefault 
    END IF 

   AFTER FIELD tipcom 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()
    
    -- Verificando tipo compra                
    IF v_gastos[arr].tipcom IS NULL THEN 
       ERROR "Error: tipo de compra invalida, VERIFICA."
       NEXT FIELD tipcom 
    END IF 

   ON CHANGE valgto 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Calculando impuesto
    CALL facing004_CalculoISV(arr,scr)

    -- Totalizando 
    CALL facing004_totdet()

   AFTER FIELD valgto 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()
    
    -- Verificando valor 
    IF v_gastos[arr].valgto IS NULL OR 
       v_gastos[arr].valgto <=0 THEN --OR
       NEXT FIELD NEXT 
    ELSE 
       --v_gastos[arr].valgto >w_mae_par.totval THEN
       IF w_mae_tra.tipcaj != 3 THEN --liquidacion  
         IF v_gastos[arr].valgto >w_mae_par.totval THEN          
            ERROR "Error: total invalido, VERIFICA."
            LET v_gastos[arr].valgto = NULL 
            CLEAR s_gastos[scr].valgto 
            NEXT FIELD valgto
         END IF 
      END IF 
    END IF 

    -- Calculando impuesto
    CALL facing004_CalculoISV(arr,scr)

    -- Totalizando 
    CALL facing004_totdet()

   ON CHANGE totedp 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Calculando impuesto
    CALL facing004_CalculoISV(arr,scr)

    -- Totalizando 
    CALL facing004_totdet()

   BEFORE FIELD totedp 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    IF v_gastos[arr].totedp IS NULL THEN
       LET v_gastos[arr].totedp = 0
    END IF 

   AFTER FIELD totedp 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()
    
    -- Verificando valor 
    IF v_gastos[arr].totedp IS NULL OR 
       v_gastos[arr].totedp <=0 OR
       v_gastos[arr].totedp >w_mae_par.totval THEN          
       LET v_gastos[arr].totedp = 0
    END IF 

    -- Calculando impuesto
    CALL facing004_CalculoISV(arr,scr)

    -- Totalizando 
    CALL facing004_totdet()

   BEFORE FIELD totigt 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    IF v_gastos[arr].totigt IS NULL THEN
       LET v_gastos[arr].totigt = 0
    END IF 

   ON CHANGE totigt 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Calculando impuesto
    CALL facing004_CalculoISV(arr,scr)

    -- Totalizando 
    CALL facing004_totdet()

   AFTER FIELD totigt 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()
    
    -- Verificando valor 
    IF v_gastos[arr].totigt IS NULL OR 
       v_gastos[arr].totigt <=0 OR
       v_gastos[arr].totigt >w_mae_par.totval THEN          
       LET v_gastos[arr].totigt = 0 
    END IF 

    -- Calculando impuesto
    CALL facing004_CalculoISV(arr,scr)

    -- Totalizando 
    CALL facing004_totdet()

   BEFORE FIELD totded 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    IF v_gastos[arr].totded IS NULL THEN
       LET v_gastos[arr].totded = 0
    END IF 

   ON CHANGE totded 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Calculando impuesto
    CALL facing004_CalculoISV(arr,scr)

    -- Totalizando 
    CALL facing004_totdet()

   AFTER FIELD totded 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()
    
    -- Verificando valor 
    IF v_gastos[arr].totded IS NULL OR 
       v_gastos[arr].totded <=0 OR
       v_gastos[arr].totded >w_mae_par.totval THEN          
       LET v_gastos[arr].totded = 0 
    END IF 

    -- Calculando impuesto
    CALL facing004_CalculoISV(arr,scr)

    -- Totalizando 
    CALL facing004_totdet()

   AFTER FIELD descrp
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()
    
    -- Verificando descrp
    IF v_gastos[arr].descrp IS NULL THEN 
       ERROR "Error: descripcion invalida, VERIFICA."
       NEXT FIELD descrp 
    END IF 

   AFTER FIELD tipdlg 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()
    
    -- Verificando tipo compra                
    IF v_gastos[arr].tipdlg IS NULL THEN 
       ERROR "Error: tipo de diligencia invalida, VERIFICA."
       NEXT FIELD tipdlg 
    END IF 

   AFTER ROW,INSERT
    -- Obteneniendo total de gastos 
    LET totlin = v_gastos.getlength() 

    -- Totalizando 
    CALL facing004_totdet()

  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Verificando lineas incompletas
  LET linea = facing004_incompletos()
  IF (linea>0) THEN
     LET msg = " Linea ("||linea||") incompleta. \n VERIFICA."
     CALL fgl_winmessage(
     "Atencion",msg,"stop")
     CONTINUE WHILE
  END IF

  -- Verificando que se ingrese al menos un gasto    
  IF (w_mae_tra.totgto=0) THEN
     CALL fgl_winmessage(
     "Atencion",
     "Debe ingresarse al menos un gasto. \nVERIFICA.",
     "stop")
     CONTINUE WHILE
  END IF

  -- Verificando sobregiros 
  IF IngresaSaldoCaja THEN 
   IF w_mae_par.sobgro=0 THEN 
    IF (w_mae_tra.totsal<0) THEN
     CALL fgl_winmessage(
     "Atencion",
     "Total gastos mayor al total de la caja.\nSobregiro no permitido.",
     "stop")
     CONTINUE WHILE
    END IF 
   END IF 
  END IF

  -- Menu de opciones
  LET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  -- Verificando opcion 
  CASE (opc)
   WHEN 0 -- Cancelando
    LET loop      = FALSE
    LET retroceso = FALSE
   WHEN 1 -- Grabando
    LET loop      = FALSE
    LET retroceso = FALSE

    -- Grabando 
    CALL facing004_grabar(operacion)
   WHEN 2 -- Modificando
    LET loop = TRUE 
  END CASE
 END WHILE

 RETURN retroceso
END FUNCTION

-- Subrutina para totalizar el detalle de gastos

FUNCTION facing004_totdet()
 DEFINE i SMALLINT

 -- Totalizando
 LET w_mae_tra.totgto = 0
 LET w_mae_tra.totsal = 0
 FOR i = 1 TO totlin
  IF v_gastos[i].fecgto IS NULL OR 
     v_gastos[i].valgto IS NULL OR 
     v_gastos[i].valgto <0 THEN        
     CONTINUE FOR
  END IF

  -- Totalizando
  LET w_mae_tra.totgto = (w_mae_tra.totgto+v_gastos[i].valgto)
 END FOR

 -- Calculando saldo de la caja
 IF IngresaSaldoCaja THEN 
  LET w_mae_tra.totsal = (w_mae_tra.totval-w_mae_tra.totgto) 
 END IF 

 -- Desplegando totales
 DISPLAY BY NAME w_mae_tra.totgto,w_mae_tra.totsal
END FUNCTION 

-- Subrutina para calcular totales del movimiento de compra 

FUNCTION facing004_CalculoISV(idx,scr)
 DEFINE xpagisv LIKE fac_rubgasto.pagisv,
        xafecmp LIKE glb_gruprovs.afecmp,
        idx,scr SMALLINT 
 
 -- Calculando totales 
 IF (v_gastos[idx].valgto>0) THEN
    -- Verificando si el rubro de gasto paga impuesto
    LET xpagisv = NULL 
    SELECT a.pagisv
     INTO  xpagisv 
     FROM  fac_rubgasto a
     WHERE a.codrub = v_gastos[idx].codrub

    -- Verificando si proveedor esta afecto al impuesto
    LET xafecmp = NULL
    SELECT y.afecmp
     INTO  xafecmp
     FROM  glb_sociosng x,glb_gruprovs y
     WHERE x.codsoc = v_gastos[idx].codsoc 
       AND y.codgru = x.codgru

     -- Si paga impuesto lo calcula
     IF xpagisv=1 AND xafecmp=1 THEN 
      -- Verificando si hay inguat o idp 
      LET v_gastos[idx].porisv = porcenisv 
      LET v_gastos[idx].totgra = ((v_gastos[idx].valgto-
                                   v_gastos[idx].totedp-
                                   v_gastos[idx].totigt-
                                   v_gastos[idx].totded)/
                                  (1+(v_gastos[idx].porisv/100))) 
      IF v_gastos[idx].totgra<0 THEN
         LET v_gastos[idx].totgra = 0
      END IF 
      LET v_gastos[idx].totisv = ((v_gastos[idx].valgto-
                                   v_gastos[idx].totedp-
                                   v_gastos[idx].totigt- 
                                   v_gastos[idx].totded)-
                                   v_gastos[idx].totgra) 

      IF v_gastos[idx].totisv<0 THEN
         LET v_gastos[idx].totisv = 0
      END IF 
      LET v_gastos[idx].totexe = 0 
     ELSE
      LET v_gastos[idx].porisv = 0 
      LET v_gastos[idx].totgra = 0
      LET v_gastos[idx].totisv = 0
      LET v_gastos[idx].totexe = (v_gastos[idx].valgto-
                                  v_gastos[idx].totedp-
                                  v_gastos[idx].totigt- 
                                  v_gastos[idx].totded) 

      IF (v_gastos[idx].totexe<0) THEN
         LET v_gastos[idx].totexe = 0
      END IF 
     END IF 
 ELSE
  LET v_gastos[idx].porisv = 0
  LET v_gastos[idx].totisv = 0
  LET v_gastos[idx].totgra = 0
  LET v_gastos[idx].totexe = 0
 END IF 

 -- Desplegando datos 
 DISPLAY v_gastos[idx].totgra TO s_gastos[scr].totgra 
 DISPLAY v_gastos[idx].totexe TO s_gastos[scr].totexe 
 DISPLAY v_gastos[idx].totisv TO s_gastos[scr].totisv 
END FUNCTION 

-- Subrutina para verificar si el detalle de gastos tiene lineas incompletas

FUNCTION facing004_incompletos()
 DEFINE i,linea SMALLINT

 LET linea = 0
 FOR i = 1 TO totlin 
  IF v_gastos[i].fecgto IS NULL THEN
     CONTINUE FOR
  END IF

  -- Verificando lineas
  IF v_gastos[i].feclbc IS NULL OR
     v_gastos[i].tipdoc IS NULL OR
     v_gastos[i].nserie IS NULL OR
     v_gastos[i].ndocto IS NULL OR
     v_gastos[i].codsoc IS NULL OR
     v_gastos[i].codrub IS NULL OR
     v_gastos[i].descrp IS NULL OR
     v_gastos[i].valgto IS NULL OR
     v_gastos[i].totedp IS NULL OR
     v_gastos[i].totigt IS NULL OR 
     v_gastos[i].totded IS NULL OR 
     v_gastos[i].tipdlg IS NULL THEN
     LET linea = i
     EXIT FOR
  END IF
 END FOR

 RETURN linea
END FUNCTION

-- Subrutina para grabar la caja 

FUNCTION facing004_grabar(operacion)
 DEFINE i,correl,operacion SMALLINT,
        msg                STRING 

 -- Grabando transaccion
 CASE (operacion)
  WHEN 1 ERROR " Registrando Caja ... " ATTRIBUTE(CYAN)
  WHEN 2 ERROR " Actualizando Caja ..." ATTRIBUTE(CYAN)
  WHEN 3 ERROR " Eliminando Caja ..."   ATTRIBUTE(CYAN)
 END CASE 

 -- Iniciando la transaccion
 BEGIN WORK
   -- 1. Encabezado
   CASE (operacion)
    WHEN 1 -- Grabando 
     INSERT INTO fac_cajchica 
     VALUES (w_mae_tra.*)
     LET w_mae_tra.lnkcaj = SQLCA.SQLERRD[2]  
    WHEN 2 -- Actualizando
     UPDATE fac_cajchica 
     SET    fac_cajchica.numest = w_mae_tra.numest, 
            fac_cajchica.totgto = w_mae_tra.totgto,
            fac_cajchica.totsal = w_mae_tra.totsal,
            fac_cajchica.observ = w_mae_tra.observ 
     WHERE  fac_cajchica.lnkcaj = w_mae_tra.lnkcaj 
   END CASE 

   -- 2. Grabando detalle
   -- Borrando detalle antes de grabar
   DELETE FROM fac_dcachica 
   WHERE fac_dcachica.lnkcaj = w_mae_tra.lnkcaj 
   
   LET correl = 0
   FOR i = 1 TO totlin
     IF v_gastos[i].fecgto IS NULL OR
        v_gastos[i].valgto IS NULL THEN
        CONTINUE FOR
     END IF
     LET correl = (correl+1)

     -- Grabando
     SET LOCK MODE TO WAIT
     INSERT INTO fac_dcachica 
     VALUES (w_mae_tra.lnkcaj  ,     -- link del encabezado
             correl            ,     -- correlativo de ingreso
             v_gastos[i].fecgto,     -- fecha de gasto
             v_gastos[i].feclbc,     -- mes del libro de compras 
             v_gastos[i].tipdoc,     -- tipo de contribuyente
             v_gastos[i].nserie,     -- serie documento 
             v_gastos[i].ndocto,     -- numero documento  
             v_gastos[i].codsoc,     -- socio de negocios
             v_gastos[i].codrub,     -- rubro del gasto 
             v_gastos[i].descrp,     -- destino del gasto 
             v_gastos[i].tipdlg,     -- tipo de diligencia 
             v_gastos[i].tipcom,     -- tipo de compra    
             v_gastos[i].valgto,     -- total del gasto
             v_gastos[i].totedp,     -- total edp
             v_gastos[i].totigt,     -- total inguat 
             v_gastos[i].totded,     -- total deducciones 
             v_gastos[i].totgra,     -- total gravado con impuesto 
             v_gastos[i].totexe,     -- total exento de impuesto
             v_gastos[i].totisv,     -- total impuesto isv
             v_gastos[i].porisv)     -- porcentale iva 
   END FOR

 -- Finalizando la transaccion
 COMMIT WORK

 -- Confirmando grabacion 
 CASE (operacion)
  WHEN 1 LET msg = " Caja Numero "||w_mae_tra.numdoc||" registrada."
  WHEN 2 LET msg = " Caja NUmero "||w_mae_tra.numdoc||" actualizada."
 END CASE
 CALL fgl_winmessage("Atencion",msg,"information")

 ERROR ""
END FUNCTION 

-- Subrutina para inicializar las variables de trabajo 

FUNCTION facing004_inival(i)
 DEFINE i       SMALLINT,
        wfecha  VARCHAR(80) 
 
 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1  
   INITIALIZE w_mae_tra.* TO NULL 
   CLEAR FORM 
 END CASE 

 -- Inicializando datos
 LET w_mae_tra.lnkcaj = 0                    
 LET w_mae_tra.fecemi = CURRENT  
 LET w_mae_tra.numest = 1 
 LET w_mae_tra.totrei = 0
 LET w_mae_tra.salant = 0
 LET w_mae_tra.totval = 0
 LET w_mae_tra.totgto = 0 
 LET w_mae_tra.totsal = 0 
 LET w_mae_tra.estado = 0
 LET w_mae_tra.lnkbco = 0 
 LET w_mae_tra.userid = username 
 LET w_mae_tra.fecsis = CURRENT 
 LET w_mae_tra.horsis = CURRENT HOUR TO SECOND

 -- Llenando combo de empresas
 CALL librut003_CbxEmpresas()
 -- Cargando combobox de centros de costo caja 
 CALL librut003_CbxCentrosCosto2("cencos", username) 
 -- Cargando combobox de tipos de caja                   
 CALL librut003_CbxTiposCaja(username) 
 -- Cargando combobox de establecimientos sat
 CALL librut003_CbxEstablecimientosSAT() 

 -- Inicializando vectores de datos
 CALL facing004_inivec() 

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_tra.totval,w_mae_tra.totgto,w_mae_tra.totsal,  
                 w_mae_tra.userid,w_mae_tra.fecsis,w_mae_tra.horsis,
                 w_mae_tra.usrliq,w_mae_tra.fecliq,w_mae_tra.horliq, 
                 w_mae_tra.totrei,w_mae_tra.salant,w_mae_tra.numest 

 -- Desplegando fecha 
 LET wfecha = librut001_formatofecha(TODAY,1,wpais) CLIPPED
 CALL librut001_dpelement("labelz",wfecha)
END FUNCTION

-- Subrutina para inicializar y limpiar los vectores de trabajo 

FUNCTION facing004_inivec()
 DEFINE i SMALLINT

 -- Inicializando vectores
 CALL v_gastos.clear()
 LET totlin = 0 
 DISPLAY ARRAY v_gastos TO s_gastos.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION 

-- Subrutina para ingresar los parametros de la caja 

FUNCTION facing004_ParametrosCaja() 
 DEFINE grabar SMALLINT 

 -- Abriendo ventana
 OPEN WINDOW wing004d AT 5,2
  WITH FORM "facing004b" ATTRIBUTE(BORDER)

  -- Obteniendo datos
  INITIALIZE w_mae_par.* TO NULL
  SELECT a.* INTO w_mae_par.* FROM fac_paramtrs a
  DISPLAY BY NAME w_mae_par.* 

  -- Ingresando datos
  OPTIONS INPUT WRAP 
  INPUT BY NAME w_mae_par.totval,
                w_mae_par.diaing,
                w_mae_par.diapos, 
                w_mae_par.sobgro WITHOUT DEFAULTS 
   ATTRIBUTE(UNBUFFERED,FIELD ORDER FORM)

   ON ACTION accept
    -- Aceptar
    LET grabar = TRUE 
    EXIT INPUT

   ON ACTION cancel 
    -- Cancelar
    LET grabar = FALSE 
    EXIT INPUT

   AFTER FIELD totval 
    -- Verificando total
    {IF w_mae_par.totval IS NULL OR 
       w_mae_par.totval<0 OR 
       w_mae_par.totval>99999 THEN
       LET w_mae_par.totval = 99999
    END IF} 
    DISPLAY BY NAME w_mae_par.totval 

   AFTER FIELD diaing 
    -- Verificando dias
    IF w_mae_par.diaing IS NULL OR 
       w_mae_par.diaing<0 OR 
       w_mae_par.diaing>360 THEN
       LET w_mae_par.diaing = 30 
    END IF 
    DISPLAY BY NAME w_mae_par.diaing 

   AFTER FIELD diapos 
    -- Verificando dias
    IF w_mae_par.diapos IS NULL OR 
       w_mae_par.diapos<0 OR 
       w_mae_par.diapos>360 THEN
       LET w_mae_par.diapos = 30 
    END IF 
    DISPLAY BY NAME w_mae_par.diapos 

   AFTER FIELD sobgro 
    -- Verificando sobregiro 
    IF w_mae_par.sobgro IS NULL THEN 
       LET w_mae_par.sobgro = 0  
    END IF 
    DISPLAY BY NAME w_mae_par.sobgro 
  END INPUT 

  -- Verificando grabacion 
  IF grabar THEN 
     LET w_mae_par.userid = username 
     LET w_mae_par.fecsis = CURRENT 
     LET w_mae_par.horsis = CURRENT HOUR TO SECOND 

     SELECT a.* FROM fac_paramtrs a 
     IF (status=NOTFOUND) THEN
      -- Grabando 
      INSERT INTO fac_paramtrs 
      VALUES (w_mae_par.*) 
     ELSE
      -- Grabando 
      UPDATE fac_paramtrs 
      SET    fac_paramtrs.* = w_mae_par.* 
     END IF 
  END IF 
  OPTIONS INPUT NO WRAP 
 CLOSE WINDOW wing004d
END FUNCTION 
