{ 
Programo : Mynor Ramirez 
Objetivo : Programa de variables globales cajas 
}

DATABASE erpjuridico 

{ Definicion de variables globales }

GLOBALS
CONSTANT progname = "facing004"
DEFINE w_mae_pro   RECORD LIKE fac_cajchica.*, 
       w_mae_emp   RECORD LIKE glb_empresas.*,
       w_mae_tra   RECORD LIKE fac_cajchica.*,
       w_mae_cli   RECORD LIKE glb_sociosng.*,
       w_mae_par   RECORD LIKE fac_paramtrs.*, 
       v_gastos    DYNAMIC ARRAY OF RECORD
        fecgto     LIKE fac_dcachica.fecgto,
        feclbc     LIKE fac_dcachica.feclbc, 
        nserie     LIKE fac_dcachica.nserie, 
        ndocto     LIKE fac_dcachica.ndocto, 
        codsoc     LIKE fac_dcachica.codsoc, 
        codrub     LIKE fac_dcachica.codrub, 
        tipdoc     LIKE fac_dcachica.tipdoc, 
        tipcom     LIKE fac_dcachica.tipdoc, 
        valgto     LIKE fac_dcachica.valgto,
        totedp     LIKE fac_dcachica.totedp,
        totigt     LIKE fac_dcachica.totigt,
        totded     LIKE fac_dcachica.totded, 
        totgra     LIKE fac_dcachica.totgra,
        totexe     LIKE fac_dcachica.totexe,
        totisv     LIKE fac_dcachica.totisv,
        descrp     LIKE fac_dcachica.descrp, 
        tipdlg     LIKE fac_dcachica.tipdlg, 
        porisv     LIKE fac_dcachica.porisv, 
        cheque     SMALLINT 
       END RECORD, 
       IngresaSaldoCaja  SMALLINT, 
       totlin            SMALLINT,
       b                 ui.ComboBox,
       cba               ui.ComboBox,
       w                 ui.Window,
       f                 ui.FORM
   DEFINE username         LIKE glb_permxusr.userid
END GLOBALS
