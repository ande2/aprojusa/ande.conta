{ 
glbglo006.4gl
Mynor Ramirez
Mantenimiento de grupos de proveedores 
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "glbmae006"
DEFINE w_mae_pro      RECORD LIKE glb_gruprovs.*,
       v_proveedores  DYNAMIC ARRAY OF RECORD
        tcodgru       LIKE glb_gruprovs.codgru,
        tnomgru       LIKE glb_gruprovs.nomgru,
        tendrec       CHAR(1) 
       END RECORD,
       username       VARCHAR(15)
END GLOBALS
