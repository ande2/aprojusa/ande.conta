{
glbqbe006.4gl 
Fernando Lontero
Mantenimiento de grupos de proveedores 
}

{ Definicion de variables globales }

GLOBALS "glbglo006.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION glbqbe006_proveedores(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),    
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,   
        msg                 STRING,
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox de grupos de proveedores 
     CALL librut003_CbxGruposProv()

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL glbqbe006_EstadoMenu(operacion," ") 
   INITIALIZE qrytext,qrypart TO NULL
   CALL glbmae006_inival(1)
   LET int_flag = 0


   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codgru,a.nomgru,a.afecmp,a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel
     -- Salida
     CALL glbmae006_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codgru,a.nomgru",
                 " FROM glb_gruprovs a",
                 " WHERE ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_proveedores SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_proveedores.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_proveedores INTO v_proveedores[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_proveedores
   FREE  c_proveedores
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL glbqbe006_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_proveedores TO s_proveedores.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL glbmae006_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscando
      CALL glbmae006_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      LET res = glbmae006_proveedores(2) 
      -- Desplegando datos
      CALL glbqbe006_datos(v_proveedores[ARR_CURR()].tcodgru)

     ON KEY (CONTROL-M) 
      -- Modificando 
      IF (operacion=2) THEN 
         -- Modificando 
         LET res = glbmae006_proveedores(2) 
         -- Desplegando datos
         CALL glbqbe006_datos(v_proveedores[ARR_CURR()].tcodgru)
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF glbqbe006_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Este grupo ya tiene registros.\n Grupo no puede borrarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de Borrar este Grupo ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL glbmae006_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte 
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Grupo" 
      LET arrcols[2] = "Nombre del Grupo" 
      LET arrcols[3] = "Afecto a Compras" 
      LET arrcols[4] = "Usuario Registro"
      LET arrcols[5] = "Fecha Registro"
      LET arrcols[6] = "Hora Registro" 

      -- Se aplica el mismo query de la seleccion para enviar al reporte 
      LET qry        = "SELECT a.codgru,a.nomgru,",
                              "CASE (a.afecmp) WHEN 1 THEN 'SI' WHEN 0 THEN 'NO' END,",
                              "a.userid,a.fecsis,a.horsis ",
                       " FROM glb_gruprovs a ",
                       " WHERE ",qrytext CLIPPED,
                       " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res    = librut002_excelreport("Grupos Proveedores",qry,6,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
         CALL glbqbe006_datos(v_proveedores[ARR_CURR()].tcodgru)
      END IF 

    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen grupos de proveedores con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Asignando estado del menu 
  CALL glbqbe006_EstadoMenu(0,"") 
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION glbqbe006_datos(wcodgru)
 DEFINE wcodgru LIKE glb_gruprovs.codgru,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  glb_gruprovs a "||
              "WHERE a.codgru = "||wcodgru||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_proveedorest SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_proveedorest INTO w_mae_pro.*
 END FOREACH
 CLOSE c_proveedorest
 FREE  c_proveedorest

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.codgru,w_mae_pro.nomgru,w_mae_pro.afecmp
 DISPLAY BY NAME w_mae_pro.codgru,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION 
 
-- Subrutina para verificar si el grupo ya tiene registros 

FUNCTION glbqbe006_integridad()
 DEFINE conteo INTEGER 

 -- Verificando proveedores 
 SELECT COUNT(*)
  INTO  conteo
  FROM  glb_sociosng a
  WHERE (a.codgru = w_mae_pro.codgru)
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE 
  END IF
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION glbqbe006_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Grupos de Proveedores - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Grupos de Proveedores - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Grupos de Proveedores - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Grupos de Proveedores - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Grupos de Proveedores - NUEVO")
 END CASE
END FUNCTION
