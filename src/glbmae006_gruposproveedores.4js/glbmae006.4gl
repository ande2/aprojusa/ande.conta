{
Mantenimiento de grupos de proveedores
glbmae006.4gl 
Fernando Lontero
}

-- Definicion de variables globales 

GLOBALS "glbglo006.4gl"

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("grupoproveedores")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo nombre del usuario 
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de principal 
 CALL glbmae006_mainmenu()
END MAIN


-- Subrutina para el menu principal del mantenimiento

FUNCTION glbmae006_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255), 
        savedata SMALLINT,
        existe   SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "glbmae006a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais

  CALL librut001_header("glbmae006",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Grupos de Proveedores" 
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(progname,4,username) THEN 
       HIDE OPTION "Buscar"
    END IF
    --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(progname,2,username) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(progname,3,username) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de grupo de proveedores."
    CALL glbqbe006_proveedores(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo grupo de proveedores." 
    LET savedata = glbmae006_proveedores(1) 
   COMMAND "Modificar"
    " Modificacion de un grupo de proveedores existente."
    CALL glbqbe006_proveedores(2) 
   COMMAND "Borrar"
    " Eliminacion de un grupo de proveedores existente."
    CALL glbqbe006_proveedores(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION glbmae006_proveedores(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL glbqbe006_EstadoMenu(4,"")
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL glbmae006_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nomgru,
                w_mae_pro.afecmp
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   AFTER FIELD nomgru  
    --Verificando nombre del grupo 
    IF (LENGTH(w_mae_pro.nomgru)=0) THEN
       ERROR "Error: nombre del grupo invalido, VERIFICA."
       LET w_mae_pro.nomgru = NULL
       NEXT FIELD nomgru  
    END IF

    -- Verificando que no exista otro grupo con el mismo nombre
    SELECT UNIQUE (a.codgru)
     FROM  glb_gruprovs a
     WHERE (a.codgru != w_mae_pro.codgru) 
       AND (a.nomgru  = w_mae_pro.nomgru) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otro grupo con el mismo nombre, VERIFICA.",
        "information")
        NEXT FIELD nomgru
     END IF 

   AFTER FIELD afecmp
    -- Verificando si grupo esta afecto a compras
    IF w_mae_pro.afecmp IS NULL THEN
       ERROR "Error: afecto a compras invalido, VERIFICA."  
       NEXT FIELD afecmp 
    END IF 
   
   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.codgru IS NULL THEN 
       NEXT FIELD codgru
    END IF
    IF w_mae_pro.nomgru IS NULL THEN
       NEXT FIELD nomgru
    END IF
    IF w_mae_pro.afecmp IS NULL THEN
       NEXT FIELD afecmp
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL glbmae006_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando 
    CALL glbmae006_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL glbqbe006_EstadoMenu(0,"")
    CALL glbmae006_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un grupo de proveedores 

FUNCTION glbmae006_grabar(operacion)
 DEFINE operacion SMALLINT,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando Grupo ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando
   -- Asignando datos
   SELECT NVL(MAX(a.codgru),0)
    INTO  w_mae_pro.codgru
    FROM  glb_gruprovs a
    IF (w_mae_pro.codgru IS NULL) THEN
       LET w_mae_pro.codgru = 1
    ELSE
       LET w_mae_pro.codgru = (w_mae_pro.codgru+1)
    END IF

   -- Grabando
   SET LOCK MODE TO WAIT
   INSERT INTO glb_gruprovs
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.codgru

   --Asignando el mensaje 
   LET msg = "Grupo Proveedor (",w_mae_pro.codgru USING "<<<",") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE glb_gruprovs
   SET    glb_gruprovs.*      = w_mae_pro.*
   WHERE  glb_gruprovs.codgru = w_mae_pro.codgru 

   --Asignando el mensaje 
   LET msg = "Grupo Proveedor (",w_mae_pro.codgru USING "<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando proveedores
   DELETE FROM glb_gruprovs 
   WHERE (glb_gruprovs.codgru = w_mae_pro.codgru)

   --Asignando el mensaje 
   LET msg = "Grupo Proveedor (",w_mae_pro.codgru USING "<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL glbmae006_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION glbmae006_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.codgru = 0 
   LET w_mae_pro.userid = username 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codgru,w_mae_pro.nomgru,w_mae_pro.afecmp 
 DISPLAY BY NAME w_mae_pro.codgru,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
END FUNCTION
