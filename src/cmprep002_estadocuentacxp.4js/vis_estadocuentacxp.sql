
drop view vis_estadocuentacxp;

create view vis_estadocuentacxp
(origen,codsoc,nomsoc,tipmov,nommov,fecemi,fecven,numdoc,totdoc,descrp,tipope,lnkcmp,numfac)

as

select 1,a.codsoc,p.nomsoc,a.tipmov,c.nommov,a.fecemi,a.fecven,a.numdoc,a.totdoc,a.descrp,
       a.tipope,a.lnkcmp,"FACTURAS"
from cmp_mtransac a,cmp_tipomovs c,glb_sociosng p
where a.frmpag in (2)
  and a.estado = 1
  and c.tipmov = a.tipmov
  and p.codsoc = a.codsoc

union all

select 2,a.codsoc,p.nomsoc,a.tipmov,c.nommov,a.feccob,a.feccob,a.numdoc,y.totpag,a.descrp,
       a.tipope,m.lnkcmp,m.numdoc
from bco_mtransac a,bco_dtransac y,bco_tipomovs c,glb_sociosng p,cmp_mtransac m
where a.lnkbco = y.lnkbco
  and a.estado = 1
  and y.lnkcmp = m.lnkcmp
  and c.tipmov = a.tipmov
  and p.codsoc = a.codsoc

union all

select 3,a.codsoc,p.nomsoc,1,"NOTA DE DEBITO",a.fecemi,a.fecemi,a.numdoc,y.totpag,a.descrp,
       0,m.lnkcmp,m.numdoc
from cmp_notasdeb a,bco_dtransac y,glb_sociosng p,cmp_mtransac m
where a.lnknot = y.lnknot
  and a.estado = 1
  and y.lnkcmp = m.lnkcmp
  and p.codsoc = a.codsoc

union all

select 4,a.codsoc,p.nomsoc,a.tipmov,c.nommov,a.fecemi,a.fecven,a.numdoc,a.totdoc,a.descrp,
       a.tipope,a.lnkcmp,"EFECTIVO"
from cmp_mtransac a,cmp_tipomovs c,glb_sociosng p
where a.frmpag in (1)
  and a.estado = 1
  and c.tipmov = a.tipmov
  and p.codsoc = a.codsoc;

grant select on vis_estadocuentacxp to public;
