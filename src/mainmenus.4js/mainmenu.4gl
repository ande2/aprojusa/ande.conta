{
mainmenu.4gl 
Programo: Mynor Ramirez
Objetivo: Menu Principal SISTEMA ERP 
} 

DATABASE erpjuridico 

CONSTANT SUPERVISOR = "sistemas"  
DEFINE v_programas DYNAMIC ARRAY OF RECORD
        codpro     LIKE glb_programs.codpro, 
        actpro     LIKE glb_programs.actpro, 
        ordpro     LIKE glb_programs.ordpro  
       END RECORD,
       totpro      INT 

-- Subrutina principal 

MAIN
 DEFINE w_datos    RECORD LIKE glb_empenuso.*
 DEFINE aui        om.DomNode 
 DEFINE wpais      VARCHAR(255) 
 DEFINE existe,i   SMALLINT 
 DEFINE salida     SMALLINT 
 DEFINE conteo     INTEGER  
 DEFINE cmdstr     STRING 
 DEFINE username   STRING 
 DEFINE servername STRING 
 DEFINE sistema    STRING
 DEFINE empresa    STRING
 DEFINE titulo     STRING 
 DEFINE version    STRING 
 DEFINE usuario    STRING 
 DEFINE ok         SMALLINT 

 OPTIONS ON CLOSE APPLICATION STOP

 -- Pidiendo usuario y contraseņa
   --CALL ingreso() RETURNING ok, username
 --IF NOT ok THEN RETURN END IF 
 IF isGDC() THEN 
   LET username = FGL_GETENV("LOGNAME")
 ELSE 
   LET username = arg_val(1)
 END IF 
 -- 

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ 

 -- Obteniendo nombre del sistema
 CALL librut003_parametros(0,1)
 RETURNING existe,sistema

 -- Obteniendo version del sistema
 CALL librut003_parametros(0,2)
 RETURNING existe,version

 -- Obteniendo nombre de la empresa
 CALL librut003_parametros(1,2)
 RETURNING existe,empresa

 -- Obteniendo nombre del usuario
   

 -- Obteniendo nombre del server
 LET servername = FGL_GETENV("HOSTNAME") 

 -- Definiendo titulo del sistema
 LET titulo = "[ "||servername CLIPPED||" ] - "||sistema CLIPPED||" - Version "||
              version CLIPPED||" - "||empresa CLIPPED

 -- Cargando estilos del menu
 CALL ui.Interface.setName("mainmenu")
 CALL ui.Interface.setType("container")
 CALL ui.Interface.setText(titulo)
 CALL ui.Interface.setImage("") 
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")

 -- Abriendo forma del menu
 OPEN FORM wmenu FROM "mainmenu" 
 DISPLAY FORM wmenu 

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header("mainmenu",wpais,1)

  -- Verificando uso de usuario supervisor
  IF username!=SUPERVISOR THEN 

  -- Combobox de empresas x usuario
  LET w_datos.codemp = librut003_DCbxEmpresasXUsuario() 

  -- Borrando empresas en uso anteriores
  DELETE FROM glb_empenuso
  WHERE glb_empenuso.userid = username  

  -- Verificando numero de empresas asignadas al usuario 
  LET usuario = FGL_GETENV("LOGNAME")
  SELECT COUNT(*)
   INTO  conteo
   FROM  glb_permxemp a
   WHERE a.userid = username -- usuario 
   --CS WHERE a.userid = USER 
   IF (conteo=0) THEN
      CALL fgl_winmessage(
      "Atencion","Usuario sin ninguna empresa asignada."||
      "\nConsultar con el administrador del sistema.","stop") 
   END IF 
DISPLAY "conteo -> ", conteo 
  -- Ingresando empresa
  IF (conteo>=1) THEN 
      IF (conteo>1) THEN 
         DISPLAY "entre"
         LET salida = FALSE 
         INPUT BY NAME w_datos.codemp WITHOUT DEFAULTS
            ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=FALSE) 
            ON ACTION salir   
               LET salida = TRUE 
               EXIT INPUT 
         END INPUT
         IF salida THEN
            CLOSE FORM wmenu 
            EXIT PROGRAM 
         END IF 
      ELSE
         DISPLAY BY NAME w_datos.codemp 
   END IF
  DISPLAY "Empresa ", w_datos.codemp 

   -- Registrando empresa seleccionada
   -- Asignando datos
   LET w_datos.userid = username 
   LET w_datos.sesion = 1
   LET w_datos.ttynid = "tty1" 
   LET w_datos.fecsis = CURRENT
   LET w_datos.horsis = CURRENT HOUR TO SECOND  

   -- Iniciando transaccion
   BEGIN WORK

    -- Registrando empresa
    INSERT INTO glb_empenuso
    VALUES (w_datos.*)

   -- Finalizando transaccion 
   COMMIT WORK 
  END IF 
  END IF 

  -- Menu 
  MENU "Menu Principal"
   BEFORE MENU  
    -- Cargando programas del menu 
    CALL  mainmenu_programas()

    -- Verificando opciones
    FOR i = 1 TO totpro
      -- Deshabilitando opcion
     CALL Dialog.SetActionActive(v_programas[i].actpro CLIPPED,0) 

     -- Verificando si opcion tiene acceso
     IF seclib001_AccesosMenu(v_programas[i].codpro,username) THEN 
        -- Habilitando opcion 
        CALL Dialog.SetActionActive(v_programas[i].actpro CLIPPED,1) 
     END IF  
    END FOR 

   ON ACTION perfiles 
    LET cmdstr = "fglrun perfiles.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION usuarios           
    LET cmdstr = "fglrun usuarios.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION parametros         
    LET cmdstr = "fglrun parametros.42r 1"
    RUN cmdstr WITHOUT WAITING 

   -- Generales

   ON ACTION paises   
    LET cmdstr = "fglrun paises.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION departamentos 
    LET cmdstr = "fglrun departamentos.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION empresas              
    LET cmdstr = "fglrun empresas.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION responsables
    LET cmdstr = "fglrun responsables.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION bancos      
    LET cmdstr = "fglrun bancos.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION sociosnegocio 
    LET cmdstr = "fglrun sociosnegocio.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION autorizantes
    LET cmdstr = "fglrun autorizantes.42r 1"
    RUN cmdstr WITHOUT WAITING

   -- Contabilidad
   ON ACTION tiposnomenclactb 
    LET cmdstr = "fglrun tiposnomenclactb.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION cuentascontables 
    LET cmdstr = "fglrun cuentascontables.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION tipostransacctb 
    LET cmdstr = "fglrun tipostransacctb.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION transaccionesctb
    LET cmdstr = "fglrun transaccionesctb.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION rlibrodiario  
    LET cmdstr = "fglrun rlibrodiario.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION rbalancegeneral  
    LET cmdstr = "fglrun rbalancegeneral.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION rbalcomprobacion 
    LET cmdstr = "fglrun rbalcomprobacion.42r 1"
    RUN cmdstr WITHOUT WAITING 
    
   ON ACTION rdiariomayor  
    LET cmdstr = "fglrun rdiariomayor.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION restadoresultados
    LET cmdstr = "fglrun restadoresultados.42r 1"
    RUN cmdstr WITHOUT WAITING 
   
   -- Bancos 
   ON ACTION cuentasbanco
    LET cmdstr = "fglrun cuentasbanco.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION tiposmovtobco    
    LET cmdstr = "fglrun tiposmovtobco.42r 1"
    RUN cmdstr WITHOUT WAITING 
   
   ON ACTION movimientosbancos
    LET cmdstr = "fglrun movimientosbancos.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION cargarestadoctabco
    -- Verificando que solo una vez pueda ejecutarse la opcion
    IF ui.Interface.getChildInstances("cargaestadobancos")==0 THEN
     LET cmdstr = "fglrun cargarestadoctabco.42r 1"
     RUN cmdstr WITHOUT WAITING
    ELSE
     CALL fgl_winmessage(
     "Atencion:","Opcion Cargar Plantilla Estado de Cuenta ya en ejecucion.","stop")
    END IF

   ON ACTION rdetallemovbco 
    LET cmdstr = "fglrun rdetallemovbco.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION restadobancario  
    LET cmdstr = "fglrun restadobancario.42r 1"
    RUN cmdstr WITHOUT WAITING 

   ON ACTION rconcilbancaria  
    LET cmdstr = "fglrun rconcilbancaria.42r 1"
    RUN cmdstr WITHOUT WAITING 

   -- Cuentas x pagar
   ON ACTION movimientoscompras 
    LET cmdstr = "fglrun movimientoscompras.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION programacionpagos
    LET cmdstr = "fglrun programacionpagos.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION pagossocios     
    LET cmdstr = "fglrun pagossocios.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION notasdebitocxp  
    LET cmdstr = "fglrun notasdebitocxp.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION rlibrocompras
    LET cmdstr = "fglrun rlibrocompras.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION rbalancesaldoscxp
    LET cmdstr = "fglrun rbalancesaldoscxp.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION restadocuentacxp
    LET cmdstr = "fglrun restadocuentacxp.42r 1"
    RUN cmdstr WITHOUT WAITING

   -- Gastos 
   ON ACTION centroscosto 
    LET cmdstr = "fglrun centroscosto.42r 1"
    RUN cmdstr WITHOUT WAITING
   
   ON ACTION rubrosgasto  
    LET cmdstr = "fglrun rubrosgasto.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION cajachica   
    LET cmdstr = "fglrun cajachica.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION rintegragasxcen
    LET cmdstr = "fglrun rintegragasxcen.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION rgastosxtipodil
    LET cmdstr = "fglrun rgastosxtipodil.42r 1"
    RUN cmdstr WITHOUT WAITING

   -- Ventas 
   ON ACTION cargalibroventas   
    -- Verificando que solo una vez pueda ejecutarse la opcion
    IF ui.Interface.getChildInstances("cargalibroventas")==0 THEN
     LET cmdstr = "fglrun cargalibroventas.42r 1"
     RUN cmdstr WITHOUT WAITING
    ELSE
     CALL fgl_winmessage(
     "Atencion:","Opcion Cargar Plantilla Libro de Ventas ya en ejecucion.","stop")
    END IF

   ON ACTION movimientosventas   
    LET cmdstr = "fglrun movimientosventas.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION rlibroventas   
    LET cmdstr = "fglrun rlibroventas.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION about  
    LET cmdstr = "fglrun about.42r 1"
    RUN cmdstr WITHOUT WAITING

   ON ACTION salir             
    -- Verificando si existe algun programa abierto 
    IF ui.Interface.getChildCount()>0 THEN
       CALL fgl_winmessage(
       "Atencion:",
       "Deben cerrarse todas las ventanas antes de salir del sistema.",
       "exclamation")
    ELSE
       EXIT PROGRAM
    END IF

   COMMAND KEY(CONTROL-P) 
    LET cmdstr = "fglrun programas.42r 1"
    RUN cmdstr 

   COMMAND KEY(INTERRUPT)
    -- Verificando si existe algun programa abierto 
    IF ui.Interface.getChildCount()>0 THEN
       CALL fgl_winmessage(
       "Atencion:",
       "Deben cerrarse todas las ventanas antes de salir del sistema.",
       "exclamation")
    ELSE
       EXIT PROGRAM
    END IF
  END MENU 
 CLOSE FORM wmenu 
END MAIN

-- Subrutina para cargar los programas del menu principal

FUNCTION mainmenu_programas()
 -- Inicializando vector
 CALL v_programas.clear()
 
 -- Cargando programas
 DECLARE cprog CURSOR FOR
 SELECT a.codpro,
        a.actpro,
        a.ordpro 
  FROM  glb_programs a,glb_mmodulos y
  WHERE LENGTH(a.actpro)>0 
    AND y.codmod = a.codmod
    AND y.estado = 1 
  ORDER BY 3

  LET totpro = 1  
  FOREACH cprog INTO v_programas[totpro].*
   LET totpro = totpro+1  
  END FOREACH
  CLOSE cprog
  FREE  cprog
  LET totpro = totpro-1  
END FUNCTION 

-- Subrutina par crear el menu 

FUNCTION createTopMenuGroup(p,t,i)
 DEFINE p om.DomNode
 DEFINE t,i STRING
 DEFINE s om.DomNode
 
 LET s = p.createChild("TopMenuGroup")
 CALL s.setAttribute("text",t) 
 CALL s.setAttribute("image",i) 
 RETURN s
END FUNCTION 

-- Subrutina para crear las opciones del menu 

FUNCTION createTopMenuCommand(p,t,i)
 DEFINE p om.DomNode
 DEFINE t,i STRING
 DEFINE s om.DomNode
 
 LET s = p.createChild("TopMenuCommand")
 CALL s.setAttribute("text",t) 
 CALL s.setAttribute("image",i) 
 RETURN s
END FUNCTION 

