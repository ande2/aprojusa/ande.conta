{
Mantenimiento de cuentas contables 
ctbmae005.4gl 
MRS
Enero 2011 
}

-- Definicion de variables globales 

GLOBALS "ctbglo005.4gl"
DEFINE w_mae_gru  RECORD LIKE ctb_grupocta.* 
DEFINE formato    LIKE ctb_tiposnom.format, 
       existe,tn  SMALLINT, 
       v_nivel    ARRAY[10] OF RECORD
        digits    LIKE ctb_tiposnom.dig001 
       END RECORD

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarCuentas")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("cuentascontables")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo usuario del sistema 
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de principal 
 CALL ctbmae005_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION ctbmae005_mainmenu()
 DEFINE titulo   STRING,
        savedata SMALLINT, 
        wpais    VARCHAR(255)

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "ctbmae005a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header("ctbmae005",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox de empresas
  LET gcodemp = librut003_DCbxEmpresaEnUsoXUsuario()
  IF gcodemp IS NULL THEN
     CALL fgl_winmessage(
     "Atencion","Usuario sin empresa asignada, VERIFICA.","stop")
     CLOSE WINDOW wing001a 
     RETURN
  END IF

  -- Buscando datos de la empresa 
  INITIALIZE w_mae_emp.* TO NULL
  CALL librut003_BEmpresa(w_mae_pro.codemp)  
  RETURNING w_mae_emp.*,existe

  -- Cargando combobox de grupos de cuentas contables 
  CALL librut003_CbxGruposCuentasContables() 

  -- Menu de opciones
  MENU "Cuentas"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,4,username) THEN
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Nueva"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Modificar"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    "Busqueda de cuentas contables."
    CALL ctbqbe005_cuentas(1) 
   COMMAND "Nueva"
    "Ingreso de un nueva cuenta contable."
    LET savedata = ctbmae005_cuentas(1)
   COMMAND "Modificar"
    "Modificacion de un cuenta contable existente."
    CALL ctbqbe005_cuentas(2) 
   COMMAND "Borrar"
    "Eliminacion de un cuenta contable existente."
    CALL ctbqbe005_cuentas(3) 
   COMMAND "Salir"
    "Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION ctbmae005_cuentas(operacion)
 DEFINE w_mae_cta         RECORD LIKE ctb_mcuentas.*, 
        lgtfmt            SMALLINT,  
        loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL ctbmae005_EstadoMenu(4,"")
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL ctbmae005_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.numcta,
                w_mae_pro.nomcta,
                w_mae_pro.nomabr,
                w_mae_pro.tipsal
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    IF INFIELD(numcta) THEN 
       LET loop = FALSE
       EXIT INPUT
    ELSE
       -- Verificando operacion
       IF (operacion=1) THEN
          CALL DIALOG.SetFieldActive("numcta",TRUE)
          CALL ctbmae005_inival(1)
          LET retroceso = FALSE
          NEXT FIELD numcta 
       ELSE
          LET loop = FALSE
          EXIT INPUT 
       END IF 
    END IF 

   BEFORE INPUT  
    -- Desactivando tipo de saldo de la cuenta
    CALL DIALOG.SetFieldActive("tipsal",FALSE)

    -- Verificando retroceso
    IF retroceso THEN
       -- Desactivando campos 
       CALL DIALOG.SetFieldActive("numcta",FALSE)
       NEXT FIELD nomcta 
    ELSE
       -- Activando campos 
       CALL DIALOG.SetFieldActive("numcta",TRUE)
    END IF 

   BEFORE FIELD numcta 
    -- Buscando datos de la empresa 
    INITIALIZE w_mae_emp.* TO NULL
    CALL librut003_BEmpresa(w_mae_pro.codemp)  
    RETURNING w_mae_emp.*,existe

    -- Verificando si existe definido nomenclatura para la empresa seleccionada 
    CALL librut003_BTiposNomenclaturaCtb(w_mae_emp.tipnom)  
    RETURNING w_tip_nom.*,existe  
    IF NOT existe THEN 
      CALL fgl_winmessage( 
      " Atencion:",
      " Empresa no tiene definido tipo de nomenclatura contable."||
      "\n Definir para poder crear la cuenta contable de la empresa.",
      "stop")

      -- Inicializando datos
      CALL ctbmae005_inival(1) 
      NEXT FIELD numcta 
    END IF
    DISPLAY BY NAME w_tip_nom.format

    -- Cargando digitos por nivel del tipo de nomenclatura 
    CALL ctbmae005_Niveles()

   AFTER FIELD numcta
    -- Eliminando caracteres y letras del numero de cuenta
    LET w_mae_pro.numcta = librut001_SoloNumeros(w_mae_pro.numcta) 

    --Verificando numero de la cuenta
    IF (LENGTH(w_mae_pro.numcta)=0) THEN
       ERROR "Error: numero de cuenta invalido, VERIFICA."
       LET w_mae_pro.numcta = NULL
       NEXT FIELD numcta  
    END IF

    -- Formateando nnumero de cuenta 
    LET formato          = librut001_QuitarSeparadores(w_tip_nom.format,
                                                       w_tip_nom.sepdor) 
    display formato 
    LET lgtfmt           = LENGTH(w_tip_nom.format)
    display lgtfmt  
    LET w_mae_pro.numcta = librut001_AjustaCuentaContable(
                                  w_mae_pro.numcta,
                                  (lgtfmt-(w_tip_nom.numniv-w_tip_nom.nivsep)))
    display w_mae_pro.numcta 

    LET w_mae_pro.numcta = librut001_FormatoCuentaContable(
                                  w_mae_pro.numcta,
                                  formato,
                                  (lgtfmt-(w_tip_nom.numniv-w_tip_nom.nivsep)),
                                  "0")
    display w_mae_pro.numcta 

    -- Poniendo separador a la cuenta
    LET w_mae_pro.numcta = librut001_PonerSeparadores(w_mae_pro.numcta,
                                                      w_tip_nom.sepdor)
    DISPLAY BY NAME w_mae_pro.numcta 
    display w_mae_pro.numcta 

    -- Obteniendo datos del grupo de cuenta 
    LET w_mae_pro.codgru = w_mae_pro.numcta[1,w_tip_nom.dig001] 
    INITIALIZE w_mae_gru.* TO NULL
    CALL librut003_BGrupoCuentaContable(w_mae_pro.codgru)
    RETURNING w_mae_gru.*,existe
    IF NOT existe THEN
       CALL fgl_winmessage( 
       " Atencion:",
       " Grupo de la cuenta contable no registrado."||
       "\n Debe crearse el grupo antes de crear nueva cuenta.",
       "stop")

       -- Inicializando datos
       CALL ctbmae005_inival(2) 
       NEXT FIELD numcta 
    END IF

    --Asignando datos del grupo de cuenta 
    LET w_mae_pro.tipcat = w_mae_gru.tipcat  
    DISPLAY BY NAME w_mae_pro.codgru,w_mae_pro.tipcat 

    -- Obteniendo cuentas padre de la cuenta 
    CALL ctbmae005_CuentasPadre(w_mae_pro.numcta)

    -- Verificando si la cuenta padre existe 
    -- Buscando datos de la cuenta padre 
    IF (w_mae_pro.numniv>1) THEN  
       INITIALIZE w_mae_cpa.* TO NULL
       CALL librut003_BCuentaContable(w_mae_pro.codemp,w_mae_pro.cta_pa)
       RETURNING w_mae_cpa.*,existe
       IF NOT existe THEN
          CALL fgl_winmessage( 
          " Atencion:",
          " No existe cuenta de nivel anterior [ "||w_mae_pro.cta_pa CLIPPED||" ]"||
          "\n Debe crearse cuenta de nivel anterior antes de crear nueva cuenta.",
          "stop")

          -- Inicializando datos
          CALL ctbmae005_inival(2) 
          NEXT FIELD numcta 
       END IF

       -- Asignando tipo de saldo de la cuenta padre 
       LET w_mae_pro.tipsal = w_mae_cpa.tipsal 
       DISPLAY BY NAME w_mae_pro.tipsal 
    ELSE 
       -- Activando tipo de saldo de la cuenta
       CALL DIALOG.SetFieldActive("tipsal",TRUE)
    END IF 

    -- Verificando si la cuenta existe  
    INITIALIZE w_mae_cta.* TO NULL
    CALL librut003_BCuentaContable(w_mae_pro.codemp,w_mae_pro.numcta)
    RETURNING w_mae_cta.*,existe
    IF existe THEN
       -- Desplegando datos
       DISPLAY BY NAME w_mae_cta.nomcta,w_mae_cta.nomabr,
                       w_mae_cta.tipsal,w_mae_cta.tipcta,
                       w_mae_cta.numniv,w_mae_cta.fecape,
                       w_mae_cta.userid,w_mae_cta.fecsis,
                       w_mae_cta.horsis 
 
       CALL fgl_winmessage( 
       " Atencion:",
       " Numero de cuenta [ "||w_mae_pro.numcta CLIPPED||
       " ] ya existe registrada, VERIFICA."||
       "\n [ "||w_mae_cta.nomcta CLIPPED||" ]", 
       "stop")

       -- Inicializando datos
       CALL ctbmae005_inival(2) 
       NEXT FIELD numcta 
    END IF

   BEFORE FIELD nomcta
    LET retroceso = TRUE 

   AFTER FIELD nomcta  
    --Verificando nombre de la cuenta
    IF (LENGTH(w_mae_pro.nomcta)=0) THEN
       ERROR "Error: nombre de la cuenta invalida, VERIFICA."
       LET w_mae_pro.nomcta = NULL
       NEXT FIELD nomcta  
    END IF

    -- Asignando nombre a nombre abreviado
    IF LENGTH(w_mae_pro.nomabr)<=0 THEN 
       LET w_mae_pro.nomabr = w_mae_pro.nomcta
       DISPLAY BY NAME w_mae_pro.nomabr
    END IF 

   AFTER FIELD nomabr 
    -- Verificando anombre abreviado 
    IF LENGTH(w_mae_pro.nomabr)<=0 THEN 
       LET w_mae_pro.nomabr = w_mae_pro.nomcta
       DISPLAY BY NAME w_mae_pro.nomabr
    END IF 

   AFTER FIELD tipsal 
    --Verificando tipo de saldo              
    IF w_mae_pro.tipsal IS NULL THEN
       ERROR "Error: tipo de saldo invalido, VERIFICA."
       NEXT FIELD tipsal 
    END IF

   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.nomcta IS NULL THEN 
       NEXT FIELD nomcta
    END IF
    IF w_mae_pro.tipsal IS NULL THEN 
       NEXT FIELD tipsal 
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  LET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    LET retroceso = FALSE 
   WHEN 1 -- Grabando
    LET savedata  = TRUE 
    LET retroceso = FALSE 

    -- Grabando cuenta
    CALL ctbmae005_grabar(operacion)
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 

  -- Si operacion es modificar 
  IF (operacion=2) THEN 
     LET loop = FALSE 
  END IF 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL ctbmae005_EstadoMenu(0,"")
    CALL ctbmae005_inival(1)
 END IF

 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar una cuenta

FUNCTION ctbmae005_grabar(operacion)
 DEFINE operacion SMALLINT,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando cuenta ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   LET w_mae_pro.tipnom = w_tip_nom.tipnom 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO ctb_mcuentas   
   VALUES (w_mae_pro.*)

   --Asignando el mensaje 
   LET msg = "Cuenta (",w_mae_pro.numcta CLIPPED,") registrada."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE ctb_mcuentas
   SET    ctb_mcuentas.*      = w_mae_pro.*
   WHERE  ctb_mcuentas.codemp = w_mae_pro.codemp 
     AND  ctb_mcuentas.numcta = w_mae_pro.numcta 

   --Asignando el mensaje 
   LET msg = "Cuenta (",w_mae_pro.numcta CLIPPED,") actualizada."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando cuenta 
   DELETE FROM ctb_mcuentas 
   WHERE ctb_mcuentas.codemp = w_mae_pro.codemp 
     AND ctb_mcuentas.numcta = w_mae_pro.numcta

   --Asignando el mensaje 
   LET msg = "Cuenta (",w_mae_pro.numcta CLIPPED,") borrada."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL ctbmae005_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION ctbmae005_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.codemp = gcodemp 
   CLEAR FORM
  WHEN 2
   INITIALIZE w_mae_pro.numcta,w_mae_pro.nomcta THRU w_mae_pro.horsis TO NULL 
   CLEAR numcta,nomcta,codgru,nomabr,fecape,numniv,tipsal,tipcta,tipcat
 END CASE

 -- Asignando datos  
 LET w_mae_pro.salini = 0
 LET w_mae_pro.salact = 0
 LET w_mae_pro.salant = 0
 LET w_mae_pro.saleje = 0
 LET w_mae_pro.cargos = 0
 LET w_mae_pro.abonos = 0
 LET w_mae_pro.caracu = 0
 LET w_mae_pro.aboacu = 0
 LET w_mae_pro.tipsal = 0 
 LET w_mae_pro.empcta = 0 
 LET w_mae_pro.aplisv = 0 
 LET w_mae_pro.apltcb = 0 
 LET w_mae_pro.fecape = CURRENT 
 LET w_mae_pro.userid = username 
 LET w_mae_pro.fecsis = CURRENT
 LET w_mae_pro.horsis = CURRENT HOUR TO SECOND

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codemp,w_mae_pro.numcta,w_mae_pro.nomcta THRU 
                 w_mae_pro.nomcta,w_mae_pro.fecape 
 DISPLAY BY NAME w_mae_pro.numcta,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION ctbmae005_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Cuentas Contables - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Cuentas Contables - BUSCAR"||msg CLIPPED)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Cuentas Contables - MODIFICAR"||
         msg CLIPPED)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Cuentas Contables - BORRAR"||msg CLIPPED)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Cuentas Contables - NUEVO")
 END CASE
END FUNCTION

-- Subrutina para validar solo numeros en una cadena

FUNCTION librut001_SoloNumeros(wcadena)
 DEFINE wcadena,wresultado CHAR(20), 
        i                  SMALLINT

 LET wresultado = NULL 
 FOR i = 1 TO LENGTH(wcadena)
  IF (wcadena[i,i] MATCHES "[0-9]") THEN
     LET wresultado = wresultado CLIPPED,wcadena[i,i] CLIPPED
  END IF
 END FOR

 RETURN wresultado CLIPPED 
END FUNCTION

-- Subrutina para buscar los niveles del tipo de nomenclatura 

FUNCTION ctbmae005_Niveles()
 DEFINE w_nivel LIKE ctb_tiposnom.dig001 ,i smallint 
 
 -- Seleccionando datos
 LET tn = 0 
 IF w_tip_nom.dig001 >0 THEN LET v_nivel[1].digits  = w_tip_nom.dig001 LET tn = tn+1 END IF 
 IF w_tip_nom.dig002 >0 THEN LET v_nivel[2].digits  = w_tip_nom.dig002 LET tn = tn+1 END IF 
 IF w_tip_nom.dig003 >0 THEN LET v_nivel[3].digits  = w_tip_nom.dig003 LET tn = tn+1 END IF 
 IF w_tip_nom.dig004 >0 THEN LET v_nivel[4].digits  = w_tip_nom.dig004 LET tn = tn+1 END IF 
 IF w_tip_nom.dig005 >0 THEN LET v_nivel[5].digits  = w_tip_nom.dig005 LET tn = tn+1 END IF 
 IF w_tip_nom.dig006 >0 THEN LET v_nivel[6].digits  = w_tip_nom.dig006 LET tn = tn+1 END IF 
 IF w_tip_nom.dig007 >0 THEN LET v_nivel[7].digits  = w_tip_nom.dig007 LET tn = tn+1 END IF 
 IF w_tip_nom.dig008 >0 THEN LET v_nivel[8].digits  = w_tip_nom.dig008 LET tn = tn+1 END IF 
 IF w_tip_nom.dig009 >0 THEN LET v_nivel[9].digits  = w_tip_nom.dig009 LET tn = tn+1 END IF 
 IF w_tip_nom.dig010 >0 THEN LET v_nivel[10].digits = w_tip_nom.dig010 LET tn = tn+1 END IF 
END FUNCTION 

-- Subrutina para obtener las cuentas padre de una cuenta 

FUNCTION ctbmae005_CuentasPadre(w_cuenta)
 DEFINE t_nivel         ARRAY[10] OF RECORD   
         suma           SMALLINT,
         digitos        CHAR(20)
        END RECORD,
        w_cuenta        LIKE ctb_mcuentas.numcta,
        w_ncuenta       LIKE ctb_mcuentas.numcta,
        hil             LIKE ctb_mcuentas.numcta,
        pnivel          LIKE ctb_mcuentas.numniv, 
        i,j,pos,ini,acu SMALLINT,
        dig             SMALLINT

 -- Inicializando vector temporal
 FOR i = 1 TO 10
  INITIALIZE t_nivel[i].* TO NULL
 END FOR 

 -- Eliminando separadores de la cuenta
 LET w_ncuenta = NULL
 LET pnivel    = 0
 FOR i = 1 TO LENGTH(w_cuenta)
  IF (w_cuenta[i,i]=w_tip_nom.sepdor) THEN
     CONTINUE FOR
  END IF
  LET w_ncuenta = w_ncuenta CLIPPED,w_cuenta[i,i]
 END FOR
 LET w_cuenta = w_ncuenta CLIPPED

 -- Recorriendo niveles para encontrar el anterior 
 LET ini = 1
 LET pos = 0
 FOR i = 1 TO tn
  IF v_nivel[i].digits IS NULL THEN
     CONTINUE FOR
  END IF 

  LET pos = (pos+v_nivel[i].digits)
  LET hil = w_cuenta[ini,pos]

  LET acu = 0
  FOR j = 1 TO LENGTH(hil)
   LET dig = hil[j,j]
   LET acu = (acu+dig)
  END FOR 
  LET t_nivel[i].digitos = w_cuenta[ini,pos]

  IF (acu>0) THEN
     LET t_nivel[i].suma = 1
  ELSE
     LET t_nivel[i].suma = 0
  END IF 

  LET ini = (pos+1)
  LET pos = (ini-1)
 END FOR

 -- Obteniendo nivel anterior 
 FOR i =  tn TO 1 STEP -1
  IF (t_nivel[i].suma=1) THEN
     EXIT FOR
  END IF  
 END FOR
 LET pnivel = i

 -- Chequeando niveles 
 IF (pnivel<=1) THEN
    LET j = pnivel
 ELSE
    LET j = (pnivel-1)
 END IF

 -- Creando cuenta padre anterior 
 LET w_mae_pro.cta_pa = NULL
 FOR i = 1 TO j
  LET w_mae_pro.cta_pa = w_mae_pro.cta_pa CLIPPED,t_nivel[i].digitos CLIPPED
 END FOR

 -- Formateando la cuenta padre al formato del tipo de nomenclatura
 LET w_mae_pro.cta_pa = librut001_FormatoCuentaContable(
                        w_mae_pro.cta_pa,
                        formato,
                        (LENGTH(w_tip_nom.format)-
                        (w_tip_nom.numniv-w_tip_nom.nivsep)),
                        "0")
 LET w_mae_pro.cta_pa = librut001_PonerSeparadores(w_mae_pro.cta_pa,
                                                   w_tip_nom.sepdor)

 -- Formateando la cuenta padre de nivel 1 al formato del tipo de nomenclatura
 LET w_mae_pro.cta_p1 = librut001_FormatoCuentaContable(
                        w_mae_pro.cta_pa[1,1],
                        formato,
                        (LENGTH(w_tip_nom.format)-
                        (w_tip_nom.numniv-w_tip_nom.nivsep)),
                        "0")
 LET w_mae_pro.cta_p1 = librut001_PonerSeparadores(w_mae_pro.cta_p1,
                        w_tip_nom.sepdor)

 -- Asignando tipo de cuenta   
 LET w_mae_pro.numniv = pnivel  
 IF (pnivel=w_tip_nom.numniv) THEN
    LET w_mae_pro.tipcta= "D"
 ELSE
    LET w_mae_pro.tipcta= "M"
 END IF 
 DISPLAY BY NAME w_mae_pro.tipcta 
END FUNCTION
