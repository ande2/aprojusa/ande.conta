{
ctbqbe005.4gl 
Mantenimiento de cuentas contables / Consultar/Modificar/Eiminar de cuentas 
}

{ Definicion de variables globales }

GLOBALS "ctbglo005.4gl" 
DEFINE totlin INTEGER 

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION ctbqbe005_cuentas(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,
        msg                 STRING,
        w                   ui.Window,
        f                   ui.Form,
        qrytext,qrypart     STRING

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL ctbmae005_EstadoMenu(operacion," ") 
   INITIALIZE qrytext,qrypart TO NULL
   CALL ctbmae005_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.numcta,a.codgru,
                                a.tipcat,a.tipcta,a.numniv,
                                a.fecape,a.nomcta,a.nomabr, 
                                a.tipsal,a.userid,a.fecsis,
                                a.horsis 
    ATTRIBUTE(CANCEL=FALSE)
    ON ACTION cancel 
     -- Salida
     CALL ctbmae005_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codemp,a.numcta,a.nomcta,''",
                 " FROM ctb_mcuentas a ",
                 " WHERE a.codemp = "||gcodemp||" AND "||qrytext CLIPPED,
                 " ORDER BY a.codemp,a.numcta "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_cuentas SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_cuentas.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_cuentas INTO v_cuentas[totlin].tcodemp,v_cuentas[totlin].tnumcta,
                          v_cuentas[totlin].tnomcta

    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_cuentas
   FREE  c_cuentas
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL ctbmae005_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_cuentas TO s_cuentas.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL ctbmae005_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscar
      CALL ctbmae005_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF ctbmae005_cuentas(2) THEN
         EXIT DISPLAY 
      END IF 

     ON KEY (CONTROL-M)   
      -- Modificando 
      IF (operacion=2) THEN 
       IF ctbmae005_cuentas(2) THEN
          EXIT DISPLAY 
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF ctbqbe005_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Cuenta contable ya tiene movimientos o existen cuentas de nivel mayor."||
         "\n Cuenta contable no puede borrarse.", 
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Este SEGURO de Borrar esta cuenta contable ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL ctbmae005_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1]  = "Empresa"
      LET arrcols[2]  = "Numero de Cuenta"
      LET arrcols[3]  = "Nombre de la Cuenta" 
      LET arrcols[4]  = "Fecha Apertura" 
      LET arrcols[5]  = "Nivel"
      LET arrcols[6]  = "Tipo de Cuenta"
      LET arrcols[7]  = "Tipo de Saldo"  
      LET arrcols[8]  = "Categoria"  
      LET arrcols[9]  = "Usuario Registro"
      LET arrcols[10] = "Fecha Registro"
      LET arrcols[11] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry  = 
      "SELECT a.codemp,a.numcta,a.nomcta,a.fecape,a.numniv,",  
       "CASE (a.tipcta) WHEN 'D' THEN 'DETALLE' WHEN 'M' THEN 'MAESTRA' END CASE,",
       "CASE (a.tipsal) WHEN 'D' THEN 'DEUDOR' WHEN 'A' THEN 'ACREEDOR' END CASE,",
       "CASE (a.tipcat) WHEN 1 THEN 'BALANCE' WHEN 2 THEN 'RESULTADOS' END CASE,",
       "a.userid,a.fecsis,a.horsis ",
      " FROM ctb_mcuentas a ",
      " WHERE ",qrytext CLIPPED,
      " ORDER BY 1,2 "

      -- Ejecutando el reporte
      LET res  = librut002_excelreport("Cuentas Contables",qry,11,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
         CALL ctbqbe005_datos(v_cuentas[ARR_CURR()].tcodemp,v_cuentas[ARR_CURR()].tnumcta)
      END IF 
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen cuentas contables con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Asignando estado del menu
  CALL ctbmae005_EstadoMenu(0,"") 
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION ctbqbe005_datos(wcodemp,wnumcta)
 DEFINE wcodemp LIKE ctb_mcuentas.codemp, 
        wnumcta LIKE ctb_mcuentas.numcta,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  ctb_mcuentas a "||
              "WHERE a.codemp = "||wcodemp||
              "  AND a.numcta = '"||wnumcta||"' "|| 
              " ORDER BY a.codemp,a.numcta "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_cuentast SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_cuentast INTO w_mae_pro.*
 END FOREACH
 CLOSE c_cuentast
 FREE  c_cuentast

 -- Obeniendo datos del tipo de nomenclatura 
 INITIALIZE w_tip_nom.* TO NULL 
 CALL librut003_BTiposNomenclaturaCtb(w_mae_pro.tipnom)
 RETURNING w_tip_nom.*,existe

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.codemp,w_mae_pro.numcta,w_mae_pro.nomcta,
                 w_mae_pro.nomabr,w_mae_pro.fecape,w_mae_pro.numniv, 
                 w_mae_pro.tipcta,w_mae_pro.tipsal,w_mae_pro.codgru, 
                 w_mae_pro.tipcat,w_tip_nom.format 

 DISPLAY BY NAME w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION 

-- Subrutina para verificar si el cuenta ya tiene movimientos  

FUNCTION ctbqbe005_integridad()
 DEFINE conteo INTEGER 

 -- Verificando niveles
 IF (w_mae_pro.numniv=w_tip_nom.numniv) THEN 
  SELECT COUNT(*)
   INTO  conteo
   FROM  ctb_mtransac x,ctb_dtransac y
   WHERE x.lnktra = y.lnktra
     AND x.codemp = w_mae_pro.codemp 
     AND y.numcta = w_mae_pro.numcta 
   IF (conteo>0) THEN
      RETURN TRUE 
   ELSE 
      RETURN FALSE 
   END IF
 ELSE
   SELECT COUNT(*)
    INTO  conteo
    FROM  ctb_mcuentas x
    WHERE x.codemp = w_mae_pro.codemp
      AND x.cta_p1 = w_mae_pro.cta_p1
      AND x.numniv > w_mae_pro.numniv
    IF (conteo>0) THEN
       RETURN TRUE
    ELSE
       RETURN FALSE
    END IF 
 END IF 
END FUNCTION 
