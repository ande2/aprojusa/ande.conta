{ 
Programa : bcorep004.4gl                   
Programo : Mynor Ramirez 
Objetivo : Reporte de egresos por centro de costo 
}

DATABASE erpjuridico 

-- Definicion de variables globales 

CONSTANT progname = "bcorep004" 
TYPE    datosreporte        RECORD 
         lnkbco             LIKE bco_mtransac.lnkbco,
         codemp             LIKE bco_mtransac.codemp, 
         nomemp             CHAR(40), 
         numcta             LIKE bco_mtransac.numcta,
         nomcta             LIKE bco_mcuentas.nomcta,
         nombco             CHAR(30),
         fecemi             LIKE bco_mtransac.fecemi,
         tipmov             LIKE bco_mtransac.tipmov,
         nommov             CHAR(6), 
         codrub             LIKE bco_mtransac.codrub,
         nomrub             CHAR(50), 
         numdoc             CHAR(15),                       
         nomsoc             CHAR(35), 
         descrp             CHAR(150),
         totdoc             LIKE bco_mtransac.totdoc,
         tipope             LIKE bco_mtransac.tipope,
         estado             SMALLINT, 
         nofase             SMALLINT,
         codcos             LIKE bco_mtransac.codcos,
         nomcen             CHAR(50) 
        END RECORD 
DEFINE  datos               RECORD 
         codemp             LIKE bco_mtransac.codemp, 
         fecini             DATE,
         fecfin             DATE
        END RECORD
DEFINE  w_mae_emp           RECORD LIKE glb_empresas.*
DEFINE  existe,nlines,lg,tc SMALLINT
DEFINE  haymov              SMALLINT
DEFINE  tituloreporte       STRING 
DEFINE  filename            STRING
DEFINE  pipeline            STRING
DEFINE  s,d                 CHAR(1) 
DEFINE  np                  CHAR(3) 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/ToolBarReportes1")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("regresoscentros")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL bcorep004_EgresosCentros()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION bcorep004_EgresosCentros()
 DEFINE imp1     datosreporte,
        wpais    VARCHAR(255),
        qrytxt   STRING,
        qryemp   STRING,
        qrypart  STRING,
        loop     SMALLINT,
        w        ui.Window, 
        f        ui.Form   

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep004a AT 5,2
  WITH FORM "bcorep004a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/EgresosCentros.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Llenando combobox de empresas         
  CALL librut003_CbxEmpresas() 

  -- Inicio del loop
  OPTIONS INPUT WRAP 
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE pipeline TO NULL
   LET s = 1 SPACE
   LET haymov = 1 
   CLEAR FORM

   -- Ingresando datos 
   DIALOG ATTRIBUTES(UNBUFFERED,FIELD ORDER FORM)

    -- Construyendo busqueda
    INPUT BY NAME datos.codemp,
                  datos.fecini,
                  datos.fecfin
    END INPUT 

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT DIALOG

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 

     -- Verificando ingreso de filtros
     IF NOT bcorep004_FiltrosCompletos() THEN 
        NEXT FIELD codemp
     END IF 
     EXIT DIALOG 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf" 

     -- Verificando ingreso de filtros
     IF NOT bcorep004_FiltrosCompletos() THEN 
        NEXT FIELD codemp
     END IF 
     EXIT DIALOG 

    ON ACTION excel 
     -- Asignando dispositivo 
     LET pipeline = "excel" 
     LET s = ASCII(9) 

     -- Verificando ingreso de filtros
     IF NOT bcorep004_FiltrosCompletos() THEN 
        NEXT FIELD codemp
     END IF 
     EXIT DIALOG

   END DIALOG 
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Obteniendo datos de la empresa
   INITIALIZE w_mae_emp.* TO NULL
   CALL librut003_BEmpresa(datos.codemp) 
   RETURNING w_mae_emp.*,existe 

   -- Calculando numero de largo del reporte
   LET lg    = 170 
   LET np    = 7.5
   LET tc    = 85   

   -- Seleccion de empresa
   IF datos.codemp IS NOT NULL THEN  
      LET qryemp = "WHERE a.codemp = ",datos.codemp," AND "
   ELSE
      LET qryemp = "WHERE " 
   END IF 

   -- Construyendo seleccion 
   LET qrytxt = "SELECT a.* ",
                 "FROM  vis_egresosdiarios a ",
                 qryemp CLIPPED, 
                 "      a.fecemi >= '",datos.fecini,"'", 
                 "  AND a.fecemi <= '",datos.fecfin,"'", 
                 " ORDER BY a.codcos,a.codrub" 

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep004 FROM qrytxt 
   DECLARE c_crep004 CURSOR FOR c_rep004
   LET existe = FALSE
   LET haymov = 1 
   FOREACH c_crep004 INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       CASE pipeline
        WHEN "pdf" LET nlines = 76 
        OTHERWISE  LET nlines = 66
       END CASE 

       LET existe = TRUE
       START REPORT bcorep004_ImprimirEgresosCentros TO filename 
    END IF 

    -- Llenando el reporte
    OUTPUT TO REPORT bcorep004_ImprimirEgresosCentros(imp1.*)
   END FOREACH
   CLOSE c_crep004 
   FREE  c_crep004 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT bcorep004_ImprimirEgresosCentros 

      -- Transfiriendo reporte a excel
      IF pipeline = "excel" THEN
         CALL bcorep004_excel(filename)
      ELSE
         -- Enviando reporte al destino seleccionado
         CALL librut001_sendreport(filename,pipeline,tituloreporte,"-l -p "||np)
      END IF

      ERROR "" 
      CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(
      " Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 
  END WHILE

  OPTIONS INPUT NO WRAP 

 CLOSE WINDOW wrep004a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION bcorep004_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE

 -- Verificando datos 
 IF datos.fecini IS NULL OR 
    datos.fecfin IS NULL THEN 
    CALL fgl_winmessage(
    " Atencion",
    " Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte 

REPORT bcorep004_ImprimirEgresosCentros(imp1)
 DEFINE imp1              datosreporte,
        linea             CHAR(306),
        col,i             INTEGER,
        espacios          CHAR(4), 
        fechareporte      STRING,
        f                 CHAR(1) 

  OUTPUT LEFT MARGIN 3
         TOP MARGIN 3 
         BOTTOM MARGIN 3 
         PAGE LENGTH nlines

  FORMAT 
   PAGE HEADER

    -- Llenando linea
    LET linea = NULL
    FOR i = 1 TO lg
     LET linea = linea CLIPPED,"-"
    END FOR  

    -- Imprimiendo Encabezado
    LET col = librut001_centrado(tituloreporte,lg) 
    PRINT COLUMN   1,"Bancos",
	  COLUMN (lg-20),PAGENO USING "Pagina: <<<<"

    PRINT COLUMN   1,"Bcorep004",
          COLUMN col,UPSHIFT(tituloreporte) CLIPPED,  
          COLUMN (lg-20),"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    LET fechareporte = "PERIODO DE FECHAS ",
                       "DEL [ ",datos.fecini," ] AL [ ",datos.fecfin," ]" 
    LET col = librut001_centrado(fechareporte,lg) 

    PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
          COLUMN col,fechareporte CLIPPED,
          COLUMN (lg-20),"Hora  : ",TIME 
    PRINT linea CLIPPED 
    PRINT "Tipo de Concepto                                             Total"
    PRINT "                                                          Concepto"
    PRINT linea
    IF datos.codemp IS NOT NULL THEN 
       PRINT "Empresa: ",w_mae_emp.nomemp CLIPPED
    ELSE
       PRINT "Empresa: TODAS"
    END IF 
    SKIP 1 LINES 

  BEFORE GROUP OF imp1.codcos 
   -- Verificando si reporte es a excel
   IF (PAGENO=1) THEN 
    IF (pipeline="excel") THEN
      -- Imprimiendo datos 
      PRINT tituloreporte CLIPPED,s
      IF datos.codemp IS NOT NULL THEN 
         PRINT "Empresa: ",w_mae_emp.nomemp CLIPPED,s
      ELSE
         PRINT "Empresa: TODAS",s
      END IF 
      PRINT s
      PRINT "Tipo Concepto",s,
            "Total Concepto",s
      PRINT s
    END IF 
   END IF

   IF (PAGENO>1) THEN 
      PRINT s 
   END IF 

   -- Imprimiendo centros de costo 
   PRINT "CENTRO COSTO:",s,imp1.nomcen CLIPPED 
   PRINT s 

  AFTER GROUP OF imp1.codrub  
   -- Imprimiendo tipos de concepto 
   PRINT imp1.nomrub                                     ,s,
         GROUP SUM(imp1.totdoc)    USING "###,###,##&.&&",s

  AFTER GROUP OF imp1.codcos 
   -- Totalizando x centro de costo 
   PRINT s
   PRINT "TOTAL CENTRO",38 SPACES                        ,s,
         GROUP SUM(imp1.totdoc)    USING "###,###,##&.&&",s
   PRINT s  

  ON LAST ROW 
   -- Totalizando reporte 
   PRINT "TOTAL REPORTE",37 SPACES                       ,s,
         SUM(imp1.totdoc)          USING "###,###,##&.&&",s
   PRINT s  
END REPORT 

-- Subrutina para exportar el reporte a excel usando WinCOM 

FUNCTION bcorep004_excel(filename) 
 DEFINE sb          base.StringBuffer
 DEFINE ch          base.channel
 DEFINE lista       base.StringTokenizer
 DEFINE result      INTEGER
 DEFINE i           INTEGER
 DEFINE xlapp       INTEGER
 DEFINE xlwb        INTEGER
 DEFINE linea       STRING 
 DEFINE lineadep    STRING 
 DEFINE filename    STRING 
 DEFINE columna     STRING 
 DEFINE separador   CHAR(1) 
 DEFINE nuevalinea  CHAR(1) 

 -- Inicializando variables                            
 LET xlapp = -1
 LET xlwb = -1

 -- Creando la instancia de Excel
 CALL ui.Interface.frontCall("WinCOM", "CreateInstance", ['Excel.Application'], [xlapp])
 CALL CheckError(xlapp, xlwb, xlapp)
   
 -- Agregando un libro al documento de excel               
 CALL ui.interface.frontCall("WinCOM", "CallMethod", [xlapp, 'WorkBooks.Add'], [xlwb])
 CALL CheckError(xlapp, xlwb, xlwb)
   
 -- Haciendo visible el libro                 
 CALL excel_set_property(xlapp, xlapp, "Visible","True")
  
 -- Llenando celdas desde el FrontCall 
 -- Construyendo celdas delimitando columnas por TAB (ASCII(9)), 
 -- y filas por CR (ASCII(10)
 -- Copia al clipboard via el Frontcall y luego instruye a excel para pegar el contenido
 -- de la celdas dentro del libro 
 LET sb = base.StringBuffer.create()

 -- Leyendo el archivo que contiene el reporte y copiandolo al clipboard 
 LET ch = base.channel.create()
 call ch.openfile(filename,"r")

 let i = 1 
 let separador  = ascii(9) 
 let nuevalinea = ascii(10)
 while true
  let linea = ch.readLine() 
  if ch.isEof() then exit while end if
  if linea.getindexof(separador,1)>0 then
     CALL sb.append(linea)
     CALL sb.append(nuevalinea)
  end if 
  let i = i+1
 end while
 call ch.close() 

 -- Pegando el contenido de las celdas en el clipboard al libro de excel
 CALL ui.Interface.FrontCall("standard","cbset",sb.toString(),result)
 CALL excel_call_method(xlapp, xlwb, 'ActiveSheet.Paste')
 CALL excel_call_method(xlapp, xlwb, 'Columns("A:Z").EntireColumn.AutoFit')
 --CALL excel_set_property(xlapp, xlwb, excel_column(nc,"Z","NumberFormat"), "0.00")

 -- Liberando informacion del clipboard    
 CALL freeMemory(xlapp, xlwb)
END FUNCTION
