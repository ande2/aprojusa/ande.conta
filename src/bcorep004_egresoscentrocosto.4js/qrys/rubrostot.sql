
--//==//==drop view vis_egresosdiarios;

--create view vis_egresosdiarios
--(codemp, nomemp, fecemi, codrub, nomrub, totdoc)

--as

select extend(fecemi,year to month),sum(a.totdoc)
from   bco_mtransac a,glb_empresas e,fac_rubgasto f,outer glb_cencosto x
where  a.estado = 1
  and  a.tipope = 0
  and  e.codemp = a.codemp
  and  f.codrub = a.codrub
  and  a.fecemi between "010114" and "311214"
  and  a.tipmov not in (5,7,25)
  and  a.tipope = 0
  and  a.codcos = x.codcos
group by 1
order by 1
