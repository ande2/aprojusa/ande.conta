select a.fecemi,a.tipmov,a.codcos,a.nomsoc[1,15],a.totdoc,f.nomrub[1,15]
from   bco_mtransac a,glb_empresas e,fac_rubgasto f
where  a.estado = 1
  and  a.tipope = 0
  and  e.codemp = a.codemp
  and  f.codrub = a.codrub
  and  a.fecemi between "010115" and "310115"
  and  a.tipmov not in (5,7,25)
  and  a.codrub = 4
