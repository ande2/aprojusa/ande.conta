unload to compras.csv delimiter ","
select extend(fecemi,year to month),fecemi,nomsoc,sum(a.totdoc)
from   bco_mtransac a,glb_empresas e,fac_rubgasto f,outer glb_cencosto x
where  a.estado = 1
  and  a.tipope = 0
  and  e.codemp = a.codemp
  and  f.codrub = a.codrub
  and  a.fecemi between "010114" and "280215"
  and  a.tipmov in (1,24)
  and  a.tipope = 0
  and  a.codcos = x.codcos
  and  a.codemp = 1
group by 1,2,3
order by 1,2
