
--//==//==drop view vis_egresosdiarios;

--create view vis_egresosdiarios
--(codemp, nomemp, fecemi, codrub, nomrub, totdoc)

--as

select
           a.codrub,
         f.nomrub[1,25],
         x.nomcen[1,25],
       sum(a.totdoc)
from   bco_mtransac a,glb_empresas e,fac_rubgasto f,glb_cencosto x
where  a.estado = 1
  and  a.tipope = 0
  and  e.codemp = a.codemp
  and  f.codrub = a.codrub
  and  a.fecemi between "011214" and "311214"
  and  a.tipmov in (1,24)
  and  a.codcos = x.codcos

  group by 1,2,3
   order by 1,2,3
