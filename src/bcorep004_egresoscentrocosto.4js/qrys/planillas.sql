select a.fecemi,a.codcos,a.nomsoc[1,20],a.totdoc,
       a.numdoc



from   bco_mtransac a,glb_empresas e,fac_rubgasto f
where  a.estado = 1
  and  a.tipope = 0
  and  e.codemp = a.codemp
  and  f.codrub = a.codrub
  and  a.fecemi between "011214" and "311214"
  and  a.tipmov in (7) and a.codrub = 27
order by 1 desc
