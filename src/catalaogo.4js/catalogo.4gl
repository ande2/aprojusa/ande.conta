{
Mantenimiento de cuentas contables 
ctbmae005.4gl 
MRS
Enero 2011 
}

DATABASE erpjuridico 

-- Definicion de variables globales 

CONSTANT progname = "ctbmae005"
DEFINE w_mae_pro   RECORD LIKE ctb_mcuentas.*,
       w_mae_cpa   RECORD LIKE ctb_mcuentas.*,
       w_mae_cp1   RECORD LIKE ctb_mcuentas.*,
       w_mae_emp   RECORD LIKE glb_empresas.*,
       w_tip_nom   RECORD LIKE ctb_tiposnom.*,
       w_mae_gru   RECORD LIKE ctb_grupocta.*, 
       formato     LIKE ctb_tiposnom.format, 
       v_cuentas   DYNAMIC ARRAY OF RECORD
        tcodemp     LIKE ctb_mcuentas.codemp, 
        tnumcta     LIKE ctb_mcuentas.numcta,
        tnomcta     LIKE ctb_mcuentas.nomcta, 
        tendrec     CHAR(1) 
       END RECORD, 
       username    VARCHAR(15),
       existe,tn   SMALLINT, 
       v_nivel     ARRAY[10] OF RECORD
        digits     LIKE ctb_tiposnom.dig001 
       END RECORD

-- Subrutina principal

MAIN
 DEFINE savedata SMALLINT 

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Obteniendo usuario del sistema 
 LET username = FGL_GETENV("LOGNAME")

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ

 CALL ctbmae005_cuentas()
END MAIN      

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION ctbmae005_cuentas()
 DEFINE w_mae_cta         RECORD LIKE ctb_mcuentas.*, 
        lgtfmt            SMALLINT,  
        loop,existe,opc   SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 CALL ctbmae005_inival(1)

 -- Seleccionando cuentas
 DECLARE c1 CURSOR FOR
 SELECT 1,
        a.cuenta,
        a.nomcta,
        a.nomcta,  
        a.tipsal,
        a.correl 
  FROM  cuentas a           
  ORDER BY a.correl
  FOREACH c1 INTO w_mae_pro.codemp,
                  w_mae_pro.numcta,
                  w_mae_pro.nomcta,
                  w_mae_pro.nomabr,
                  w_mae_pro.tipsal 

   DISPLAY w_mae_pro.codemp," ",w_mae_pro.numcta," ",w_mae_pro.nomcta 

    -- Buscando datos de la empresa 
    INITIALIZE w_mae_emp.* TO NULL
    CALL librut003_BEmpresa(w_mae_pro.codemp)  
    RETURNING w_mae_emp.*,existe

    -- Verificando si existe definido nomenclatura para la empresa seleccionada 
    CALL librut003_BTiposNomenclaturaCtb(w_mae_emp.tipnom)  
    RETURNING w_tip_nom.*,existe  
    IF NOT existe THEN 
      DISPLAY "Catalogo empresa no existe."  
      EXIT FOREACH 
    END IF

    -- Cargando digitos por nivel del tipo de nomenclatura 
    CALL ctbmae005_Niveles()

    -- Eliminando caracteres y letras del numero de cuenta
    LET w_mae_pro.numcta = librut001_SoloNumeros(w_mae_pro.numcta) 

    -- Formateando nnumero de cuenta 
    LET formato          = librut001_QuitarSeparadores(w_tip_nom.format,w_tip_nom.sepdor) 
    LET lgtfmt           = LENGTH(w_tip_nom.format)
    LET w_mae_pro.numcta = librut001_AjustaCuentaContable(
                                  w_mae_pro.numcta,
                                  (lgtfmt-(w_tip_nom.numniv-w_tip_nom.nivsep)))
    LET w_mae_pro.numcta = librut001_FormatoCuentaContable(
                                  w_mae_pro.numcta,
                                  formato,
                                  (lgtfmt-(w_tip_nom.numniv-w_tip_nom.nivsep)),
                                  "0")

    -- Poniendo separador a la cuenta
    LET w_mae_pro.numcta = librut001_PonerSeparadores(w_mae_pro.numcta,w_tip_nom.sepdor)

    -- Obteniendo datos del grupo de cuenta 
    LET w_mae_pro.codgru = w_mae_pro.numcta[1,w_tip_nom.dig001] 
    INITIALIZE w_mae_gru.* TO NULL
    CALL librut003_BGrupoCuentaContable(w_mae_pro.codgru)
    RETURNING w_mae_gru.*,existe
    IF NOT existe THEN
       DISPLAY "Grupo de cuenta no existe." 
       EXIT FOREACH  
    END IF

    --Asignando datos del grupo de cuenta 
    LET w_mae_pro.tipcat = w_mae_gru.tipcat  

    -- Obteniendo cuentas padre de la cuenta 
    CALL ctbmae005_CuentasPadre(w_mae_pro.numcta)

    -- Verificando si la cuenta padre existe 
    -- Buscando datos de la cuenta padre 
    IF (w_mae_pro.numniv>1) THEN  
       INITIALIZE w_mae_cpa.* TO NULL
       CALL librut003_BCuentaContable(w_mae_pro.codemp,w_mae_pro.cta_pa)
       RETURNING w_mae_cpa.*,existe
       IF NOT existe THEN
          DISPLAY               
          " No existe cuenta de nivel anterior [ "||w_mae_pro.cta_pa CLIPPED||" ]"
          EXIT FOREACH 
       END IF

       -- Asignando tipo de saldo de a cuenta padre 
       LET w_mae_pro.tipsal = w_mae_cpa.tipsal 
    END IF 

    -- Verificando si la cuenta existe  
    INITIALIZE w_mae_cta.* TO NULL
    CALL librut003_BCuentaContable(w_mae_pro.codemp,w_mae_pro.numcta)
    RETURNING w_mae_cta.*,existe

    -- Grabando cuenta
    CALL ctbmae005_grabar(1)
 END FOREACH
 CLOSE c1
 FREE c1 

END FUNCTION

-- Subrutina para grabar/modificar una cuenta

FUNCTION ctbmae005_grabar(operacion)
 DEFINE operacion SMALLINT,
        msg       CHAR(80)

   -- Asignando datos
   LET w_mae_pro.tipnom = w_tip_nom.tipnom 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO ctb_mcuentas   
   VALUES (w_mae_pro.*)

END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION ctbmae005_inival(i)
 DEFINE i SMALLINT

 -- Asignando datos  
 INITIALIZE w_mae_pro.* TO NULL
 LET w_mae_pro.salini = 0
 LET w_mae_pro.salact = 0
 LET w_mae_pro.salant = 0
 LET w_mae_pro.saleje = 0
 LET w_mae_pro.cargos = 0
 LET w_mae_pro.abonos = 0
 LET w_mae_pro.caracu = 0
 LET w_mae_pro.aboacu = 0
 LET w_mae_pro.tipsal = 0 
 LET w_mae_pro.empcta = 0 
 LET w_mae_pro.aplisv = 0 
 LET w_mae_pro.apltcb = 0 
 LET w_mae_pro.fecape = CURRENT 
 LET w_mae_pro.userid = username 
 LET w_mae_pro.fecsis = CURRENT
 LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
END FUNCTION

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION ctbmae005_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Cuentas Contables - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Cuentas Contables - BUSCAR"||msg CLIPPED)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Cuentas Contables - MODIFICAR"||
         msg CLIPPED)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Cuentas Contables - BORRAR"||msg CLIPPED)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Cuentas Contables - NUEVO")
 END CASE
END FUNCTION

-- Subrutina para validar solo numeros en una cadena

FUNCTION librut001_SoloNumeros(wcadena)
 DEFINE wcadena,wresultado CHAR(20), 
        i                  SMALLINT

 LET wresultado = NULL 
 FOR i = 1 TO LENGTH(wcadena)
  IF (wcadena[i,i] MATCHES "[0-9]") THEN
     LET wresultado = wresultado CLIPPED,wcadena[i,i] CLIPPED
  END IF
 END FOR

 RETURN wresultado CLIPPED 
END FUNCTION

-- Subrutina para buscar los niveles del tipo de nomenclatura 

FUNCTION ctbmae005_Niveles()
 DEFINE w_nivel LIKE ctb_tiposnom.dig001 ,i smallint 
 
 -- Seleccionando datos
 LET tn = 0 
 IF w_tip_nom.dig001 >0 THEN LET v_nivel[1].digits  = w_tip_nom.dig001 LET tn = tn+1 END IF 
 IF w_tip_nom.dig002 >0 THEN LET v_nivel[2].digits  = w_tip_nom.dig002 LET tn = tn+1 END IF 
 IF w_tip_nom.dig003 >0 THEN LET v_nivel[3].digits  = w_tip_nom.dig003 LET tn = tn+1 END IF 
 IF w_tip_nom.dig004 >0 THEN LET v_nivel[4].digits  = w_tip_nom.dig004 LET tn = tn+1 END IF 
 IF w_tip_nom.dig005 >0 THEN LET v_nivel[5].digits  = w_tip_nom.dig005 LET tn = tn+1 END IF 
 IF w_tip_nom.dig006 >0 THEN LET v_nivel[6].digits  = w_tip_nom.dig006 LET tn = tn+1 END IF 
 IF w_tip_nom.dig007 >0 THEN LET v_nivel[7].digits  = w_tip_nom.dig007 LET tn = tn+1 END IF 
 IF w_tip_nom.dig008 >0 THEN LET v_nivel[8].digits  = w_tip_nom.dig008 LET tn = tn+1 END IF 
 IF w_tip_nom.dig009 >0 THEN LET v_nivel[9].digits  = w_tip_nom.dig009 LET tn = tn+1 END IF 
 IF w_tip_nom.dig010 >0 THEN LET v_nivel[10].digits = w_tip_nom.dig010 LET tn = tn+1 END IF 
END FUNCTION 

-- Subrutina para obtener las cuentas padre de una cuenta 

FUNCTION ctbmae005_CuentasPadre(w_cuenta)
 DEFINE t_nivel         ARRAY[10] OF RECORD   
         suma           SMALLINT,
         digitos        CHAR(20)
        END RECORD,
        w_cuenta        LIKE ctb_mcuentas.numcta,
        w_ncuenta       LIKE ctb_mcuentas.numcta,
        hil             LIKE ctb_mcuentas.numcta,
        pnivel          LIKE ctb_mcuentas.numniv, 
        i,j,pos,ini,acu SMALLINT,
        dig             SMALLINT

 -- Inicializando vector temporal
 FOR i = 1 TO 10
  INITIALIZE t_nivel[i].* TO NULL
 END FOR 

 -- Eliminando separadores de la cuenta
 LET w_ncuenta = NULL
 LET pnivel    = 0
 FOR i = 1 TO LENGTH(w_cuenta)
  IF (w_cuenta[i,i]=w_tip_nom.sepdor) THEN
     CONTINUE FOR
  END IF
  LET w_ncuenta = w_ncuenta CLIPPED,w_cuenta[i,i]
 END FOR
 LET w_cuenta = w_ncuenta CLIPPED

 -- Recorriendo niveles para encontrar el anterior 
 LET ini = 1
 LET pos = 0
 FOR i = 1 TO tn
  IF v_nivel[i].digits IS NULL THEN
     CONTINUE FOR
  END IF 

  LET pos = (pos+v_nivel[i].digits)
  LET hil = w_cuenta[ini,pos]

  LET acu = 0
  FOR j = 1 TO LENGTH(hil)
   LET dig = hil[j,j]
   LET acu = (acu+dig)
  END FOR 
  LET t_nivel[i].digitos = w_cuenta[ini,pos]

  IF (acu>0) THEN
     LET t_nivel[i].suma = 1
  ELSE
     LET t_nivel[i].suma = 0
  END IF 

  LET ini = (pos+1)
  LET pos = (ini-1)
 END FOR

 -- Obteniendo nivel anterior 
 FOR i =  tn TO 1 STEP -1
  IF (t_nivel[i].suma=1) THEN
     EXIT FOR
  END IF  
 END FOR
 LET pnivel = i

 -- Chequeando niveles 
 IF (pnivel<=1) THEN
    LET j = pnivel
 ELSE
    LET j = (pnivel-1)
 END IF

 -- Creando cuenta padre anterior 
 LET w_mae_pro.cta_pa = NULL
 FOR i = 1 TO j
  LET w_mae_pro.cta_pa = w_mae_pro.cta_pa CLIPPED,t_nivel[i].digitos CLIPPED
 END FOR

 -- Formateando la cuenta padre al formato del tipo de nomenclatura
 LET w_mae_pro.cta_pa = librut001_FormatoCuentaContable(
                        w_mae_pro.cta_pa,
                        formato,
                        (LENGTH(w_tip_nom.format)-(w_tip_nom.numniv-w_tip_nom.nivsep)),
                        "0")
 LET w_mae_pro.cta_pa = librut001_PonerSeparadores(w_mae_pro.cta_pa,w_tip_nom.sepdor)

 -- Formateando la cuenta padre de nivel 1 al formato del tipo de nomenclatura
 LET w_mae_pro.cta_p1 = librut001_FormatoCuentaContable(
                        w_mae_pro.cta_pa[1,1],
                        formato,
                        (LENGTH(w_tip_nom.format)-(w_tip_nom.numniv-w_tip_nom.nivsep)),
                        "0")
 LET w_mae_pro.cta_p1 = librut001_PonerSeparadores(w_mae_pro.cta_p1,w_tip_nom.sepdor)

 -- Asignando tipo de cuenta   
 LET w_mae_pro.numniv = pnivel  
 IF (pnivel=w_tip_nom.numniv) THEN
    LET w_mae_pro.tipcta= "D"
 ELSE
    LET w_mae_pro.tipcta= "M"
 END IF 
END FUNCTION
