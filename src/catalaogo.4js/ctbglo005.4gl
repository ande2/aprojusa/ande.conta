{ 
ctbglo005.4gl
Mynor Ramirez
Mantenimiento de cuentas contables 
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "ctbmae005"
DEFINE w_mae_pro   RECORD LIKE ctb_mcuentas.*,
       w_mae_cpa   RECORD LIKE ctb_mcuentas.*,
       w_mae_cp1   RECORD LIKE ctb_mcuentas.*,
       w_mae_emp   RECORD LIKE glb_empresas.*,
       w_tip_nom   RECORD LIKE ctb_tiposnom.*,
       v_cuentas  DYNAMIC ARRAY OF RECORD
        tcodemp    LIKE ctb_mcuentas.codemp, 
        tnumcta    LIKE ctb_mcuentas.numcta,
        tnomcta    LIKE ctb_mcuentas.nomcta, 
        tendrec    CHAR(1) 
       END RECORD, 
       username    VARCHAR(15)
END GLOBALS
