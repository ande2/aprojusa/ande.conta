{
glbmae043.4gl 
Mantenimiento de cuentas bancarias
}

-- Definicion de variables globales 

GLOBALS "glbglo043.4gl"

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("cuentas")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Obteniendo nombre del cuenta 
 LET username = FGL_GETENV("LOGNAME")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL glbmae043_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION glbmae043_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255),
        existe   SMALLINT, 
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "glbmae043a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header("glbmae043",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox de empresas
  LET gcodemp = librut003_DCbxEmpresaEnUsoXUsuario()
  IF gcodemp IS NULL THEN 
     CALL fgl_winmessage(
     "Atencion","Usuario sin empresa asignada, VERIFICA.","stop") 
     CLOSE WINDOW wing001a
     RETURN 
  END IF 

  -- Menu de opciones
  MENU "Cuentas"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,4,username) THEN
       HIDE OPTION "Buscar"
    END IF
    --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Modificar"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de cuentas."
    CALL glbqbe043_cuentas(1) 
   COMMAND "Nuevo"
    " Ingreso de una nueva cuenta."
    LET savedata = glbmae043_cuentas(1) 
   COMMAND "Modificar"
    " Modificacion de una cuenta existente."
    CALL glbqbe043_cuentas(2) 
   COMMAND "Borrar"
    " Eliminacion de una cuenta existente."
    CALL glbqbe043_cuentas(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION glbmae043_cuentas(operacion)
 DEFINE w_mae_cta         RECORD LIKE bco_mcuentas.*,
        w_mae_ctb         RECORD LIKE ctb_mcuentas.*,
        loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL glbqbe043_EstadoMenu(4,"") 
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL glbmae043_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.numcta,
                w_mae_pro.codbco,
                w_mae_pro.nomcta,
                w_mae_pro.tipcta,
                w_mae_pro.impvou,
                w_mae_pro.numcor,
                w_mae_pro.numctb
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   BEFORE FIELD numcta 
    -- Verificando si no es ingreso
    IF (operacion=2) THEN
       NEXT FIELD codbco 
    END IF 

   AFTER FIELD numcta
    --Verificando cuenta
    IF (LENGTH(w_mae_pro.numcta)=0) THEN
       ERROR "Error: numero de cuenta invalido, VERIFICA."
       LET w_mae_pro.numcta = NULL
       CLEAR numcta
       NEXT FIELD numcta
    END IF

    -- Verificando si la cuenta existe registrada
    INITIALIZE w_mae_cta.* TO NULL
    CALL librut003_BCuentaBanco(w_mae_pro.numcta)
    RETURNING w_mae_cta.*,existe
    IF existe THEN
       ERROR "Error: numero de cuenta ya existe registrada, VERIFICA."
       NEXT FIELD numcta
    END IF 

   AFTER FIELD codbco 
    -- Verificando banco 
    IF w_mae_pro.codbco IS NULL THEN
       ERROR "Error: banco invalido, VERIFICA."
       NEXT FIELD codbco 
    END IF 

   AFTER FIELD nomcta  
    --Verificando nombre del cuenta
    IF (LENGTH(w_mae_pro.nomcta)=0) THEN
       ERROR "Error: nombre de la cuenta invalida, VERIFICA."
       LET w_mae_pro.nomcta = NULL
       NEXT FIELD nomcta  
    END IF

   AFTER FIELD tipcta
    -- Verificando tipo de cuenta
    IF w_mae_pro.tipcta IS NULL THEN
       ERROR "Error: tipo de cuenta invalido, VERIFICA." 
       NEXT FIELD tipcta
    END IF 

   AFTER FIELD impvou
    -- Verificando imprime cheques
    IF w_mae_pro.impvou IS NULL THEN
       ERROR "Error: imprime cheques invalido, VERIFICA." 
       NEXT FIELD impvou
    END IF 

   AFTER FIELD numcor 
    -- Verificando correlativo     
    IF w_mae_pro.numcor IS NULL THEN
       ERROR "Error: correlativo invalido, VERIFICA." 
       NEXT FIELD numcor 
    END IF 
   
   AFTER FIELD numctb
    -- Verificando cuenta contable
    IF w_mae_pro.numctb IS NOT NULL THEN
     -- Verificando si la cuenta existe registrada
     INITIALIZE w_mae_ctb.* TO NULL
     CALL librut003_BCuentaContable(w_mae_pro.codemp,w_mae_pro.numctb)
     RETURNING w_mae_ctb.*,existe
     IF NOT existe THEN
       ERROR "Error: cuenta contable no ha sido registrada, VERIFICA."
       NEXT FIELD numctb
     END IF 
    END IF 
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")
  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL glbmae043_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando 
    CALL glbmae043_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL glbqbe043_EstadoMenu(0,"") 
    CALL glbmae043_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar 

FUNCTION glbmae043_grabar(operacion)
 DEFINE operacion SMALLINT,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando cuenta ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO bco_mcuentas   
   VALUES (w_mae_pro.*)

   --Asignando el mensaje 
   LET msg = "Cuenta (",w_mae_pro.numcta CLIPPED,") registrada."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE bco_mcuentas
   SET    bco_mcuentas.*      = w_mae_pro.*
   WHERE  bco_mcuentas.numcta = w_mae_pro.numcta 

   --Asignando el mensaje 
   LET msg = "Cuenta (",w_mae_pro.numcta CLIPPED,") actualizada."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   -- Borrando cuentas
   DELETE FROM bco_mcuentas 
   WHERE (bco_mcuentas.numcta = w_mae_pro.numcta)

   --Asignando el mensaje 
   LET msg = "Cuenta (",w_mae_pro.numcta CLIPPED,") borrada."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL glbmae043_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION glbmae043_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.codemp = gcodemp 
   LET w_mae_pro.cargos = 0 
   LET w_mae_pro.abonos = 0 
   LET w_mae_pro.salact = 0 
   LET w_mae_pro.numcor = 1 
   LET w_mae_pro.userid = username 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Cargando bancos  
 CALL librut003_CbxBancos() 

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codemp THRU w_mae_pro.fulabo 
 DISPLAY BY NAME w_mae_pro.numcta,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION
