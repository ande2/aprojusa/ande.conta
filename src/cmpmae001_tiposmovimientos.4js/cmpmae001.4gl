{
Mantenimiento de rubros de gasto 
facmae006.4gl 
}

-- Definicion de variables globales 

GLOBALS "cmpglo001.4gl"

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rubros")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo nombre del usuario 
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de principal 
 CALL facmae006_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION facmae006_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255), 
        savedata SMALLINT,
        existe   SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "cmpmae001a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais

  CALL librut001_header("facmae006",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Rubros"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(progname,4,username) THEN 
       HIDE OPTION "Buscar"
    END IF
    --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(progname,2,username) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(progname,3,username) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de rubros."
    CALL facqbe006_rubros(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo rubro."
    LET savedata = facmae006_rubros(1) 
   COMMAND "Modificar"
    " Modificacion de un rubro existente."
    CALL facqbe006_rubros(2) 
   COMMAND "Borrar"
    " Eliminacion de un rubro existente."
    CALL facqbe006_rubros(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION facmae006_rubros(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL facqbe006_EstadoMenu(4,"")
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL facmae006_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nomrub,
                w_mae_pro.estado
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   AFTER FIELD nomrub  
    --Verificando nombre del rubro
    IF (LENGTH(w_mae_pro.nomrub)=0) THEN
       ERROR "Error: nombre del rubro invalida, VERIFICA."
       LET w_mae_pro.nomrub = NULL
       NEXT FIELD nomrub  
    END IF

    -- Verificando que no exista otro rubro con el mismo nombre
    SELECT UNIQUE (a.codrub)
     FROM  fac_rubgasto a
     WHERE (a.codrub != w_mae_pro.codrub) 
       AND (a.nomrub  = w_mae_pro.nomrub) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otro rubro con el mismo nombre, VERIFICA.",
        "information")
        NEXT FIELD nomrub
     END IF

   AFTER FIELD estado
    --Verificando estado
    IF w_mae_pro.estado IS NULL THEN 
       NEXT FIELD estado
    END IF

   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.nomrub IS NULL THEN 
       NEXT FIELD nomrub
    END IF
    IF w_mae_pro.estado IS NULL THEN 
       NEXT FIELD estado
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL facmae006_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando 
    CALL facmae006_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL facqbe006_EstadoMenu(0,"")
    CALL facmae006_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un rubro

FUNCTION facmae006_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando rubro ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.codrub),0)
    INTO  w_mae_pro.codrub
    FROM  fac_rubgasto a
    IF (w_mae_pro.codrub IS NULL) THEN
       LET w_mae_pro.codrub = 1
    ELSE 
       LET w_mae_pro.codrub = (w_mae_pro.codrub+1)
    END IF 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO fac_rubgasto   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.codrub 

   --Asignando el mensaje 
   LET msg = "Rubro (",w_mae_pro.codrub USING "<<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE fac_rubgasto
   SET    fac_rubgasto.*      = w_mae_pro.*
   WHERE  fac_rubgasto.codrub = w_mae_pro.codrub 

   --Asignando el mensaje 
   LET msg = "Rubro (",w_mae_pro.codrub USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando rubros
   DELETE FROM fac_rubgasto 
   WHERE (fac_rubgasto.codrub = w_mae_pro.codrub)

   --Asignando el mensaje 
   LET msg = "Rubro (",w_mae_pro.codrub USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL facmae006_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION facmae006_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.codrub = 0 
   LET w_mae_pro.estado = 1 
   LET w_mae_pro.userid = username 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codrub,w_mae_pro.estado 
 DISPLAY BY NAME w_mae_pro.codrub,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
END FUNCTION
