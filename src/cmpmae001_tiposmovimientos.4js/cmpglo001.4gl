{ 
facglo006.4gl
Mynor Ramirez
Mantenimiento de rubros de gasto 
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "facmae006"
DEFINE w_mae_pro      RECORD LIKE fac_rubgasto.*,
       v_rubros       DYNAMIC ARRAY OF RECORD
        tcodrub       LIKE fac_rubgasto.codrub,
        tnomrub       LIKE fac_rubgasto.nomrub, 
        tendrec       CHAR(1) 
       END RECORD,
       username       VARCHAR(15)
END GLOBALS
