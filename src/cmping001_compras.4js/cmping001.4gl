{
Programo : Mynor Ramirez
Objetivo : Mantenimiento de movimiento de compras 
}

-- Definicion de variables globales

GLOBALS "cmpglb001.4gl"
CONSTANT PagosEfectivo = 0 
DEFINE gtipmov          LIKE cmp_mtransac.tipmov,
       gcodaut          LIKE cmp_mtransac.codaut,
       gcodcos          LIKE cmp_mtransac.codcos,
       wpais            VARCHAR(255), 
       porcenisv        DECIMAL(5,2),
       regreso          SMALLINT,
       existe           SMALLINT,
       msg              STRING 

-- Subrutina principal

MAIN
 -- Atrapando interrupts 
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarMovimientosCompras")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("movimientoscompras")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Definiendo nivel de aislamiento
 SET ISOLATION TO DIRTY READ 

 -- Obteniendo datos del pais 
 CALL librut003_parametros(1,0)
 RETURNING existe,wpais

 -- Obteniendo usuario del sistema
 LET username = FGL_GETENV("LOGNAME")
 LET codigoapl= "CMP" 

 -- Menu de opciones
 CALL cmping001_menu()
END MAIN

-- Subutina para el menu de movimientos de compras 

FUNCTION cmping001_menu()
 -- Abriendo la ventana del mantenimiento
 OPEN WINDOW wing004a AT 5,2  
  WITH FORM "cmping001a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL librut001_header("cmping001",wpais,1)

  -- Escondiendo tabla de consultas 
  CALL f.SetElementHidden("titulolista",1) 
  CALL f.SetElementHidden("tablaconsulta",1) 

  -- Cargando combobox de empresas
  LET gcodemp = librut003_DCbxEmpresaEnUsoXUsuario()
  IF gcodemp IS NULL THEN 
     CALL fgl_winmessage(
     "Atencion","Usuario sin empresa asignada, VERIFICA.","stop") 
     CLOSE WINDOW wing004a
     RETURN 
  END IF 

  -- Cargando tipos de movimiento de compra 
  LET gtipmov = librut003_DCbxTiposMovimientoCompras()
  -- Cargando combobox de socios  
  CALL librut003_CbxProveedores() 
  -- Cargando combobox de autorizantes 
  LET gcodaut = librut003_DCbxAutorizantes() 
  -- Cargando combobox de centros de costo
  LET gcodcos = librut003_DCbxCentrosCosto("codcos")
  -- Cargando combobox de rubros de gasto
  CALL librut003_CbxRubrosGasto() 
  -- Cargando combobox de establecimientos sat
  CALL librut003_CbxEstablecimientosSAT() 

  -- Obteniendo porcentaje de impuesto sobre venta
  CALL librut003_parametros(8,1)  
  RETURNING existe,porcenisv   
  IF NOT existe THEN
     LET porcenisv = 0 
  END IF

  -- Inicializando datos 
  CALL cmping001_inival(1)

  -- Menu de opciones
  MENU "Opciones" 
   BEFORE MENU
    -- Verificando accesos a opciones
    -- Consultar
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "Consultar"
    END IF
    -- Ingresar 
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Ingresar"
    END IF
    -- Anular
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Anular"
    END IF
    -- Eliminar 
    IF NOT seclib001_accesos(progname,4,username) THEN
       HIDE OPTION "delete"
    END IF
    -- Parametros 
    IF NOT seclib001_accesos(progname,6,username) THEN
       HIDE OPTION "parametros"
    END IF

    -- Obteniendo parametros de movimientos de compra 
    INITIALIZE w_mae_par.* TO NULL
    SELECT a.* INTO w_mae_par.* FROM cmp_paramtrs a

   ON ACTION consultar  
    CALL cmpqbx001_Compras(1)
   ON ACTION ingresar  
    CALL cmping001_Compras(1,0) 
   ON ACTION anular  
    CALL cmpqbx001_Compras(2) 
   ON ACTION delete 
    CALL cmpqbx001_Compras(3) 
   ON ACTION parametros 
    CALL cmping001_ParametrosCompras() 
   ON ACTION salir  
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU  
  END MENU
 CLOSE WINDOW wing004a
END FUNCTION

-- Subrutina para el ingreso de los datos de la compra 

FUNCTION cmping001_Compras(operacion,haypagos)
 DEFINE retroceso         SMALLINT,
        operacion,opc     SMALLINT,
        haypagos          SMALLINT,
        loop,existe,i     SMALLINT,
        conteo            INTEGER

 -- Verificando tipo de operacion
 IF (operacion=1) THEN
    -- Desplegando estado del menu
    CALL cmping001_EstadoMenu(4,"")
    -- Inicializando datos 
    CALL cmping001_inival(1) 
    LET retroceso = FALSE
 ELSE
    -- Desplegando estado del menu
    CALL cmping001_EstadoMenu(5,"")
    LET retroceso = TRUE  
 END IF 

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop   
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Verificando si operacion es ingreso
     IF (operacion=1) THEN 
        -- Inicializando datos 
        CALL cmping001_inival(3) 
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_trc.tipmov, 
                w_mae_trc.codsoc, 
                w_mae_trc.numnit,
                w_mae_trc.nomsoc,
                w_mae_trc.codaut,
                w_mae_trc.codcos,
                w_mae_trc.codrub,
                w_mae_trc.fecemi, 
                w_mae_trc.feclbc, 
                w_mae_trc.diacre, 
                w_mae_trc.nserie, 
                w_mae_trc.numdoc,
                w_mae_trc.totdoc, 
                w_mae_trc.totigt, 
                w_mae_trc.totedp, 
                w_mae_trc.totded, 
                w_mae_trc.descrp,
                w_mae_trc.numest,
                w_mae_trc.tipcom,
                w_mae_trc.tipdoc,
                w_mae_trc.frmpag
                WITHOUT DEFAULTS
    ATTRIBUTE(UNBUFFERED,CANCEL=FALSE)

   ON ACTION cancel 
    -- Salida 
    IF (operacion=1) THEN  -- Ingreso 
       IF INFIELD(tipmov) THEN 
         -- Inicializando 
         CALL cmping001_inival(1)
         LET loop = FALSE
         EXIT INPUT
       ELSE
         IF INFIELD(codsoc) THEN 
          -- Inicializando 
          CALL cmping001_inival(2)
          CALL Dialog.SetFieldActive("tipmov",1) 
          LET retroceso = FALSE 
          NEXT FIELD tipmov  
         ELSE
          -- Inicializando 
          CALL cmping001_inival(3)
          CALL Dialog.SetFieldActive("codsoc",1) 
          LET retroceso = FALSE 
          NEXT FIELD codsoc  
         END IF 
      END IF 
    ELSE                 -- Modificacion 
     LET loop = FALSE 
     EXIT INPUT 
    END IF 

   ON ACTION calculator 
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   ON ACTION centros
    -- Agregando mas centros de costo 
    CALL cmping001_MasCentrosCosto() 

   BEFORE INPUT 
    -- Desactivadno otros centros de costo
    IF (totcos=0) THEN 
       CALL Dialog.SetActionActive("centros",0) 
    ELSE
       CALL Dialog.SetActionActive("centros",1) 
    END IF 

    -- Verificando operacion
    IF retroceso THEN
       IF operacion=1 THEN 
          CALL Dialog.SetFieldActive("tipmov",0) 
          CALL Dialog.SetFieldActive("codsoc",0) 

          -- Verificando si socio es grupo 
          IF w_mae_pro.esgrup=0 THEN 
             CALL Dialog.SetFieldActive("numnit",0) 
             CALL Dialog.SetFieldActive("nomsoc",0) 
          ELSE 
             CALL Dialog.SetFieldActive("numnit",1) 
             CALL Dialog.SetFieldActive("nomsoc",1) 
          END IF 
       ELSE 
          CALL Dialog.SetFieldActive("tipmov",0) 
          CALL Dialog.SetFieldActive("codsoc",0) 
          CALL Dialog.SetFieldActive("numnit",0) 
          CALL Dialog.SetFieldActive("nomsoc",0) 
  
          -- Si compra ya tiene pagos deshabilita los campos siguientes:
          IF haypagos THEN 
             CALL Dialog.SetFieldActive("codaut",0) 
             CALL Dialog.SetFieldActive("codrub",0) 
             CALL Dialog.SetFieldActive("diacre",0) 
             CALL Dialog.SetFieldActive("totdoc",0) 
             CALL Dialog.SetFieldActive("totigt",0) 
             CALL Dialog.SetFieldActive("totedp",0) 
             CALL Dialog.SetFieldActive("totded",0) 
          ELSE 
             CALL Dialog.SetFieldActive("codaut",1) 
             CALL Dialog.SetFieldActive("codrub",1) 
             CALL Dialog.SetFieldActive("diacre",1) 
             CALL Dialog.SetFieldActive("totdoc",1) 
             CALL Dialog.SetFieldActive("totigt",1) 
             CALL Dialog.SetFieldActive("totedp",1) 
             CALL Dialog.SetFieldActive("totded",1) 
          END IF 
       END IF 
    END IF 

    -- Verificando si hay pagos en efectivo
    IF PagosEfectivo=0 THEN 
       CALL Dialog.SetFieldActive("frmpag",0)  
    ELSE
       CALL Dialog.SetFieldActive("frmpag",1)  
    END IF 

   BEFORE FIELD tipmov  
    -- Desabilitando accion de accept
    CALL Dialog.SetActionActive("accept",0) 

   ON CHANGE tipmov
    -- Obteniendo datos del tipo de movimiento de compra 
    INITIALIZE w_tip_mov.* TO NULL 
    CALL librut003_BTipoMovimientoCompras(w_mae_trc.tipmov)
    RETURNING w_tip_mov.*,existe

   AFTER FIELD tipmov 
    -- Verificando tipmov 
    IF w_mae_trc.tipmov IS NULL THEN
       ERROR "Error: tipo de movimiento invalido, VERIFICA."
       NEXT FIELD tipmov 
    END IF 

    -- Obteniendo datos del tipo de movimiento de compra 
    INITIALIZE w_tip_mov.* TO NULL 
    CALL librut003_BTipoMovimientoCompras(w_mae_trc.tipmov)
    RETURNING w_tip_mov.*,existe

   ON CHANGE codsoc
    -- Obteniendo datos del socio 
    INITIALIZE w_mae_pro.* TO NULL 
    CALL librut003_BSociosNeg(w_mae_trc.codsoc)
    RETURNING w_mae_pro.*,existe 

   AFTER FIELD codsoc 
    -- Verificando codsoc
    IF w_mae_trc.codsoc IS NULL THEN 
       ERROR "Error: socio invalido, VERIFICA."
       NEXT FIELD codsoc
    END IF  

    -- Obteniendo datos del socio 
    INITIALIZE w_mae_pro.* TO NULL 
    CALL librut003_BSociosNeg(w_mae_trc.codsoc)
    RETURNING w_mae_pro.*,existe 
    LET w_mae_trc.diacre = w_mae_pro.diacre
    DISPLAY BY NAME w_mae_trc.diacre

    -- Verificando si el socio no es grupo
    IF w_mae_pro.esgrup=0 THEN 
       LET w_mae_trc.numnit = w_mae_pro.numnit
       LET w_mae_trc.nomsoc = w_mae_pro.nomsoc 
       DISPLAY BY NAME w_mae_trc.numnit,w_mae_pro.nomsoc 
       CALL Dialog.SetFieldActive("numnit",0) 
       CALL Dialog.SetFieldActive("nomsoc",0) 
    ELSE 
       CALL Dialog.SetFieldActive("numnit",1) 
       CALL Dialog.SetFieldActive("nomsoc",1) 
    END IF 

   AFTER FIELD numnit
    -- Verificando numero de NIT
    IF (w_mae_trc.numnit<=0) THEN
       ERROR "Error: numero de NIT invalido, VERIFICA."
       NEXT FIELD numnit 
    END IF 

    -- Buscando socio por numero de nit
    IF w_mae_trc.nomsoc IS NULL THEN 
     INITIALIZE w_mae_trc.nomsoc TO NULL 
     SELECT a.nomsoc 
      INTO  w_mae_trc.nomsoc 
      FROM  cmp_mtransac a 
      WHERE a.lnkcmp = (SELECT MAX(x.lnkcmp) 
                         FROM  cmp_mtransac x
                         WHERE x.numnit = w_mae_trc.numnit) 
        AND a.numnit = w_mae_trc.numnit  
    END IF 
    DISPLAY BY NAME w_mae_pro.nomsoc 

   AFTER FIELD nomsoc  
    -- Verificando numero de NIT
    IF (w_mae_pro.nomsoc<=0) THEN
       ERROR "Error: nombre del socio invalido, VERIFICA."
       NEXT FIELD nomsoc 
    END IF 

   BEFORE FIELD codaut 
    -- Desactivando campos llave  
    CALL Dialog.SetFieldActive("tipmov",0) 
    CALL Dialog.SetFieldActive("codsoc",0) 
    CALL Dialog.SetFieldActive("numnit",0) 
    CALL Dialog.SetFieldActive("nomsoc",0) 
    LET retroceso = TRUE

   AFTER FIELD codaut 
    -- Verificando codaut
    IF w_mae_trc.codaut IS NULL THEN 
       ERROR "Error: autorizante invalido, VERIFICA."
       NEXT FIELD codaut 
    END IF 

   ON CHANGE codcos 
    -- Asignando centro default
    IF (totcos=0) THEN
       LET totcos = 1 
       LET v_costos[1].codcos = w_mae_trc.codcos 
    ELSE 
       IF (totcos=1) THEN 
          LET v_costos[1].codcos = w_mae_trc.codcos 
          LET v_costos[1].totval = w_mae_trc.totdoc 
       ELSE 
          LET v_costos[1].codcos = w_mae_trc.codcos 
       END IF 
    END IF 

   AFTER FIELD codcos 
    -- Verificando codcos 
    IF w_mae_trc.codcos IS NULL THEN 
       ERROR "Error: centro de costo invalido, VERIFICA."
       NEXT FIELD codcos 
    END IF 

    -- Asignando centro default
    IF (totcos=0) THEN
       LET totcos = 1 
       LET v_costos[1].codcos = w_mae_trc.codcos 
    ELSE 
       IF (totcos=1) THEN 
          LET v_costos[1].codcos = w_mae_trc.codcos 
          LET v_costos[1].totval = w_mae_trc.totdoc 
       ELSE 
          LET v_costos[1].codcos = w_mae_trc.codcos 
       END IF 
    END IF 

   ON CHANGE codrub          
    -- Calculando totales
    CALL cmping001_totales()

   AFTER FIELD codrub 
    -- Verificando codrub
    IF w_mae_trc.codrub IS NULL THEN 
       ERROR "Error: rubro de gasto invalido, VERIFICA."
       NEXT FIELD codrub 
    END IF 

   ON CHANGE fecemi
    -- Recalculando fecha de pago 
    IF (w_mae_trc.diacre>0) THEN
       --Calcular fecha de pago segun dias de credito para pagos del socio 
       LET w_mae_trc.fecven = (w_mae_trc.fecemi+w_mae_trc.diacre)
    ELSE
       LET w_mae_trc.fecven = w_mae_trc.fecemi  
    END IF 
    DISPLAY BY NAME w_mae_trc.fecven

   AFTER FIELD fecemi
    --Verificando fecha de emision del documento
    IF (w_mae_trc.fecemi IS NULL OR
       w_mae_trc.fecemi<(TODAY-w_mae_par.diaing)) THEN
       CALL fgl_winmessage(
       "Atencion",
       "Fecha del documento invalida.\n"||
       "Limite de dias excedido. ("||w_mae_par.diaing||")",
       "stop")
       NEXT FIELD fecemi
    END IF 

    -- Calculando fecha de pago 
    IF (w_mae_trc.diacre>0) THEN
       --Calcular fecha de pago segun dias de credito para pagos del socio 
       LET w_mae_trc.fecven = (w_mae_trc.fecemi+w_mae_trc.diacre)
    ELSE
       LET w_mae_trc.fecven = w_mae_trc.fecemi  
    END IF 
    DISPLAY BY NAME w_mae_trc.fecven

   BEFORE FIELD feclbc 
    --Verificando fecha de libro de compras     
    IF w_mae_trc.feclbc IS NULL OR 
       w_mae_trc.feclbc <w_mae_trc.fecemi THEN 
       LET w_mae_trc.feclbc = w_mae_trc.fecemi 
       DISPLAY BY NAME w_mae_trc.feclbc 
    END IF 

   AFTER FIELD feclbc
    --Verificando fecha de libro de compras     
    IF w_mae_trc.feclbc IS NULL OR
       w_mae_trc.feclbc <w_mae_trc.fecemi THEN 
       LET w_mae_trc.feclbc = w_mae_trc.fecemi 
       DISPLAY BY NAME w_mae_trc.feclbc 
    END IF 

   AFTER FIELD diacre
    -- Verificando diacre 
    IF w_mae_trc.diacre IS NULL OR
       w_mae_trc.diacre <=0 OR
       w_mae_trc.diacre >w_mae_par.diacre THEN
       LET w_mae_trc.diacre = 0
       DISPLAY BY NAME w_mae_trc.diacre 
    END IF 

    -- Calculando fecha de pago 
    IF (w_mae_trc.diacre>0) THEN
       --Calcular fecha de pago segun dias de credito de pagos del socio 
       LET w_mae_trc.fecven = (w_mae_trc.fecemi+w_mae_trc.diacre)
    ELSE
       LET w_mae_trc.fecven = w_mae_trc.fecemi  
    END IF 
    DISPLAY BY NAME w_mae_trc.fecven

   AFTER FIELD nserie
    IF w_mae_trc.nserie IS NULL OR 
       w_mae_trc.nserie <=0 THEN 
       ERROR "Error: serie del documento invalida, VERIFICA."
       NEXT FIELD nserie  
    END IF 

   AFTER FIELD numdoc
    -- Verificando numdoc
    IF w_mae_trc.numdoc IS NULL OR 
       w_mae_trc.numdoc <=0 THEN 
       ERROR "Error: numero de documento invalido, VERIFICA."
       NEXT FIELD numdoc 
    END IF 

    -- Verificando si el documento existe
    SELECT UNIQUE a.lnkcmp
     FROM  cmp_mtransac a
     WHERE a.lnkcmp != w_mae_trc.lnkcmp
      AND  a.tipmov  = w_mae_trc.tipmov 
      AND  a.codsoc  = w_mae_trc.codsoc 
      AND  a.nserie  = w_mae_trc.nserie 
      AND  a.numdoc  = w_mae_trc.numdoc
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        "Atencion",
        "Documento Serie "||w_mae_trc.nserie CLIPPED||" Numero "||
        w_mae_trc.numdoc CLIPPED||" ya existe registrado.",
        "stop")
        NEXT FIELD numdoc
     END IF 

   BEFORE FIELD totdoc
    -- Calculando totales
    CALL cmping001_totales()

   ON CHANGE totdoc 
    -- Calculando totales
    CALL cmping001_totales()

   AFTER FIELD totdoc 
    -- Verificando totdoc
    IF w_mae_trc.totdoc IS NULL OR 
       w_mae_trc.totdoc <=0 OR 
       w_mae_trc.totdoc >w_mae_par.totval THEN 
       ERROR "Error: total documento invalido, VERIFICA."
       NEXT FIELD totdoc 
    END IF 

    -- Calculando totales
    CALL cmping001_totales()

    -- Activadno otros centros de costo
    CALL Dialog.SetActionActive("centros",1) 

    -- Asignando centro default
    IF (totcos=1) THEN
       LET v_costos[1].codcos = w_mae_trc.codcos 
       LET v_costos[1].totval = w_mae_trc.totdoc 
    ELSE 
       LET v_costos[1].codcos = w_mae_trc.codcos 
    END IF 

   ON CHANGE totigt 
    -- Calculando totales
    CALL cmping001_totales()

   AFTER FIELD totigt 
    -- Verificando totigt
    IF w_mae_trc.totigt IS NULL OR 
       w_mae_trc.totigt <0 OR 
       w_mae_trc.totigt >w_mae_par.totval THEN 
       ERROR "Error: total inguat invalido, VERIFICA."
       NEXT FIELD totigt 
    END IF 

    -- Calculando totales
    CALL cmping001_totales()

   ON CHANGE totedp 
    -- Calculando totales
    CALL cmping001_totales()

   AFTER FIELD totedp 
    -- Verificando totedp
    IF w_mae_trc.totedp IS NULL OR 
       w_mae_trc.totedp <0 OR 
       w_mae_trc.totedp >w_mae_par.totval THEN 
       ERROR "Error: total idp invalido, VERIFICA."
       NEXT FIELD totedp 
    END IF 

    -- Calculando totales
    CALL cmping001_totales()

   ON CHANGE totded 
    -- Calculando totales
    CALL cmping001_totales()

   AFTER FIELD totded 
    -- Verificando totded
    IF w_mae_trc.totded IS NULL OR 
       w_mae_trc.totded <0 OR 
       w_mae_trc.totded >w_mae_par.totval THEN 
       ERROR "Error: total otras deducciones invalido, VERIFICA."
       NEXT FIELD totded 
    END IF 

    -- Calculando totales
    CALL cmping001_totales()

   AFTER FIELD descrp
    -- Verificando descrp 
    IF (w_mae_trc.lnkinv=0) THEN 
     IF LENGTH(w_mae_trc.descrp)=0 THEN
       ERROR "Error: descripcion del movimiento de compra invalido, VERIFICA."
       NEXT FIELD descrp 
     END IF 
    END IF 

    -- Habilitando accion de accept
    CALL Dialog.SetActionActive("accept",1)

   BEFORE FIELD tipdoc
    -- Llenando comboox de tipos de documento dependiendo del regimen fiscal
    CALL librut003_CbxTiposDocRegimenFis(w_mae_pro.codgru)

  END INPUT 
  IF NOT loop THEN
     EXIT WHILE 
  END IF 

  -- Verificando totales del movimiento 
  IF (w_mae_trc.totdoc=0) THEN
     CALL fgl_winmessage(
     "Atencion",
     "Total del movimiento debe ser mayor a cero.\n"||
     "VERIFICAR totales.", 
     "stop")
     LET retroceso = TRUE 
     CONTINUE WHILE 
  END IF 

  -- Verificando totales de inguat, edp, otras deducciones 
  IF (w_mae_trc.totigt>0) AND
     (w_mae_trc.totedp>0) AND 
     (w_mae_trc.totded>0) THEN
     CALL fgl_winmessage(
     "Atencion",
     "No incluir INGUAT,IDP,OTR/DED en el mismo movimiento.\n"||
     "VERIFICAR totales.", 
     "stop")
     LET retroceso = TRUE 
     CONTINUE WHILE 
  END IF 

  -- Verificando totales de los centros de costo
  CALL cmping001_TotalDesgloceCentrosCosto()
  IF (valcos=0) OR
     (valcos!=w_mae_trc.totdoc) THEN
     CALL fgl_winmessage(
     "Atencion",
     "Total desgloce centros de costo diferente a valor total del movimiento.\n"||
     "Total centros de costo ( "||valcos||" ) - valor total movimiento ( "||
     w_mae_trc.totdoc||" )",
     "stop")

     LET retroceso = TRUE 
     CONTINUE WHILE 
  END IF 

  -- Ingresando detalle de la poliza
  IF w_tip_mov.polctb=1 THEN
   IF cmping001_DetallePoliza() THEN
      LET retroceso = TRUE
      CONTINUE WHILE
   END IF 

   -- Verificando total del documento y total de la poliza contable
   IF (w_mae_trc.totdoc!=totaldeb) THEN
     CALL fgl_winmessage(
     "Atencion",
     "Valor total del documento diferente al total de la poliza contable."||
     "\nVerificar totales de la poliza contable.",
     "stop")
     LET retroceso = TRUE
     CONTINUE WHILE
   END IF 
  END IF
  
  -- Menu de opciones 
  LET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  -- Verificando opcion 
  CASE (opc)
   WHEN 0 -- Cancelando
    LET retroceso = FALSE 
   WHEN 1 -- Grabando
    LET retroceso = FALSE 

    -- Grabando 
    CALL cmping001_grabar(operacion,haypagos)
   WHEN 2 -- Modificando
    LET retroceso = TRUE 
    CONTINUE WHILE 
  END CASE

  -- Verificando operacion
  IF (operacion=2) THEN
     EXIT WHILE
  END IF
 END WHILE

 -- Inicializando datos 
 IF (operacion=1) THEN 
    CALL cmping001_inival(1) 

    -- Desplegando estado del menu
    CALL cmping001_EstadoMenu(0,"")
 END IF 
END FUNCTION

-- Subrutina para ingresar los parametros de los movimientos de compras 

FUNCTION cmping001_ParametrosCompras() 
 DEFINE grabar SMALLINT 

 -- Abriendo ventana
 OPEN WINDOW wing001d AT 5,2
  WITH FORM "cmping001c" ATTRIBUTE(BORDER)

  -- Obteniendo datos
  INITIALIZE w_mae_par.* TO NULL
  SELECT a.* INTO w_mae_par.* FROM cmp_paramtrs a
  DISPLAY BY NAME w_mae_par.* 

  -- Ingresando datos
  OPTIONS INPUT WRAP 
  INPUT BY NAME w_mae_par.totval,
                w_mae_par.diaing,
                w_mae_par.diamod, 
                w_mae_par.diaanu, 
                w_mae_par.diaeli, 
                w_mae_par.diacre WITHOUT DEFAULTS 
   ATTRIBUTE(UNBUFFERED,FIELD ORDER FORM)

   ON ACTION accept
    -- Aceptar
    LET grabar = TRUE 
    EXIT INPUT

   ON ACTION cancel 
    -- Cancelar
    LET grabar = FALSE 
    EXIT INPUT

   AFTER FIELD totval 
    -- Verificando total
    {IF w_mae_par.totval IS NULL OR 
       w_mae_par.totval<0 OR 
       w_mae_par.totval>9999999 THEN
       LET w_mae_par.totval = 999999.99 
    END IF} 
    DISPLAY BY NAME w_mae_par.totval 

   AFTER FIELD diaing 
    -- Verificando dias
    IF w_mae_par.diaing IS NULL OR 
       w_mae_par.diaing<0 OR 
       w_mae_par.diaing>120 THEN
       LET w_mae_par.diaing = 30 
    END IF 
    DISPLAY BY NAME w_mae_par.diaing 

   AFTER FIELD diamod 
    -- Verificando dias
    IF w_mae_par.diamod IS NULL OR 
       w_mae_par.diamod<0 OR 
       w_mae_par.diamod>120 THEN
       LET w_mae_par.diamod = 30 
    END IF 
    DISPLAY BY NAME w_mae_par.diamod 

   AFTER FIELD diaanu 
    -- Verificando dias
    IF w_mae_par.diaanu IS NULL OR 
       w_mae_par.diaanu<0 OR 
       w_mae_par.diaanu>120 THEN
       LET w_mae_par.diaanu = 30 
    END IF 
    DISPLAY BY NAME w_mae_par.diaanu 

   AFTER FIELD diaeli 
    -- Verificando dias
    IF w_mae_par.diaeli IS NULL OR 
       w_mae_par.diaeli<0 OR 
       w_mae_par.diaeli>120 THEN
       LET w_mae_par.diaeli = 30 
    END IF 
    DISPLAY BY NAME w_mae_par.diaeli 

   AFTER FIELD diacre 
    -- Verificando total
    IF w_mae_par.diacre IS NULL OR 
       w_mae_par.diacre<0 OR 
       w_mae_par.diacre>120 THEN
       LET w_mae_par.diacre = 30 
    END IF 
    DISPLAY BY NAME w_mae_par.diacre 
  END INPUT 

  -- Verificando grabacion 
  IF grabar THEN 
     LET w_mae_par.userid = username 
     LET w_mae_par.fecsis = CURRENT 
     LET w_mae_par.horsis = CURRENT HOUR TO SECOND 

     SELECT a.* FROM cmp_paramtrs a 
     IF (status=NOTFOUND) THEN
      -- Grabando 
      INSERT INTO cmp_paramtrs 
      VALUES (w_mae_par.*) 
     ELSE
      -- Grabando 
      UPDATE cmp_paramtrs 
      SET    cmp_paramtrs.* = w_mae_par.* 
     END IF 
  END IF 
  OPTIONS INPUT NO WRAP 
 CLOSE WINDOW wing001d
END FUNCTION 
 
-- Subrutina para agregar mas centro de costo

FUNCTION cmping001_MasCentrosCosto()
 DEFINE i,arr,scr SMALLINT 

 -- Abriendo ventana
 OPEN WINDOW wing001c AT 5,2
  WITH FORM "cmping001b" ATTRIBUTE(BORDER)

  -- Cargando combobox de centros de costo
  CALL librut003_CbxCentrosCosto("codcos")

  -- Ingresando datos
  INPUT ARRAY v_costos WITHOUT DEFAULTS FROM s_costos.*
   ATTRIBUTE(INSERT ROW=FALSE,ACCEPT=FALSE,CANCEL=FALSE,
	     UNBUFFERED=TRUE,FIELD ORDER FORM)

   ON ACTION accept
    -- Aceptar
    EXIT INPUT

   ON ACTION delete 
    -- Eliminando linea seleccionada
    LET arr = ARR_CURR()
    CALL v_costos.deleteElement(arr)
    NEXT FIELD codcos 

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF 

   AFTER FIELD totval 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando total
    IF v_costos[arr].totval IS NULL OR
       v_costos[arr].totval <=0 THEN
       LET v_costos[arr].totval = NULL
       CLEAR s_costos[scr].totval 
       ERROR "Error: valor invalido, VERIFICA."
       NEXT FIELD totval
    END IF 

   AFTER ROW,INSERT,DELETE 
    LET totcos = ARR_COUNT()

   AFTER INPUT 
    LET totcos = ARR_COUNT()

  END INPUT 
 CLOSE WINDOW wing001c
END FUNCTION 

-- Subrutina para calcular totales del movimiento de compra 

FUNCTION cmping001_totales()
 DEFINE xpagisv LIKE fac_rubgasto.pagisv,
        xafecmp LIKE glb_gruprovs.afecmp 

 -- Calculando totales 
 IF (w_mae_trc.totdoc>0) THEN
    -- Verificando si el rubro de gasto paga impuesto
    LET xpagisv = NULL 
    SELECT a.pagisv
     INTO  xpagisv 
     FROM  fac_rubgasto a
     WHERE a.codrub = w_mae_trc.codrub

    -- Verificando si proveedor esta afecto al impuesto 
    LET xafecmp = NULL
    SELECT y.afecmp 
     INTO  xafecmp
     FROM  glb_sociosng x,glb_gruprovs y 
     WHERE x.codsoc = w_mae_trc.codsoc 
       AND y.codgru = x.codgru 

     -- Si paga impuesto lo calcula
     IF xpagisv=1 AND xafecmp=1 THEN 
       -- Verificando si hay inguat o idp 
       LET w_mae_trc.totgra = ((w_mae_trc.totdoc-
                                w_mae_trc.totigt-
                                w_mae_trc.totedp-
                                w_mae_trc.totded)/
                                (1+(w_mae_trc.porisv/100))) 
       IF w_mae_trc.totgra<0 THEN
          LET w_mae_trc.totgra = 0
       END IF 
       LET w_mae_trc.totisv = ((w_mae_trc.totdoc-
                                w_mae_trc.totigt-
                                w_mae_trc.totedp-
                                w_mae_trc.totded)-
                                w_mae_trc.totgra) 
       IF w_mae_trc.totisv<0 THEN
          LET w_mae_trc.totisv = 0
       END IF 
       LET w_mae_trc.totexe = 0 
    ELSE
       LET w_mae_trc.totgra = 0
       LET w_mae_trc.totisv = 0
       LET w_mae_trc.totexe = (w_mae_trc.totdoc-
                               w_mae_trc.totigt-
                               w_mae_trc.totedp-
                               w_mae_trc.totded) 
       IF (w_mae_trc.totexe<0) THEN
          LET w_mae_trc.totexe = 0
       END IF 
    END IF 
 ELSE
    LET w_mae_trc.totisv = 0
    LET w_mae_trc.totgra = 0
    LET w_mae_trc.totexe = 0
 END IF 

 -- Totalizando
 DISPLAY BY NAME w_mae_trc.totgra,w_mae_trc.totexe,w_mae_trc.totisv,w_mae_trc.totdoc 
END FUNCTION 

-- Subrutina para totalizar desgloce de centros de costo 

FUNCTION cmping001_TotalDesgloceCentrosCosto()
 DEFINE i SMALLINT

 LET valcos = 0
 FOR i = 1 TO totcos
  IF v_costos[i].codcos IS NULL THEN
     CONTINUE FOR
  END IF 
  LET valcos = valcos+v_costos[i].totval 
 END FOR
END FUNCTION 

-- Subrutina para grabar el movimiento de compra  

FUNCTION cmping001_grabar(operacion,haypagos)
 DEFINE w_mae_ctb                 RECORD LIKE ctb_mtransac.*,
        xtotval                   LIKE ctb_mtransac.totdoc,
        xtipnom                   LIKE glb_empresas.tipnom,
        i,correl,operacion,anular SMALLINT,
        haypagos                  SMALLINT, 
        xtipope                   CHAR(1),
        msg                       STRING 

 -- Grabando transaccion
 CASE (operacion)
  WHEN 1 ERROR " Registrando Movimiento de Compra ... " ATTRIBUTE(CYAN)
  WHEN 2 ERROR " Actualizando Movimiento de Compra ... " ATTRIBUTE(CYAN)
 END CASE 
 
 -- Verificando que no hayan pagos efectuados
 IF NOT haypagos THEN 
  -- Verificando si se afectan saldos
  IF w_tip_mov.afesal=1 THEN 
     LET w_mae_trc.totabo = 0 
     LET w_mae_trc.fulabo = NULL 
     LET w_mae_trc.totsal = w_mae_trc.totdoc 
  ELSE 
     LET w_mae_trc.totabo = w_mae_trc.totdoc 
     LET w_mae_trc.fulabo = w_mae_trc.fecemi  
     LET w_mae_trc.totsal = 0 
  END IF   

  -- Asignando datos
  LET w_mae_trc.tipope = w_tip_mov.tipope 

  -- Verificando tipo de operacion 
  CASE (w_mae_trc.tipope) 
   WHEN 1 LET w_mae_trc.totval = w_mae_trc.totdoc          -- Cargo
   WHEN 0 LET w_mae_trc.totval = (w_mae_trc.totdoc*(-1))   -- Abono 
  END CASE 
 END IF 

 -- Iniciando la transaccion
 BEGIN WORK
   -- 1. Encabezado
   CASE (operacion)
    WHEN 1 -- Grabando 
     -- Verificando si el socio no es grupo
     IF w_mae_pro.esgrup=0 THEN 
      -- Asignando datos del socio 
      INITIALIZE w_mae_trc.nomsoc,w_mae_trc.numnit TO NULL  
      SELECT a.nomsoc,a.numnit
       INTO  w_mae_trc.nomsoc,w_mae_trc.numnit
       FROM  glb_sociosng a
       WHERE a.codsoc = w_mae_trc.codsoc 
     END IF 

     -- Grabando movimiento 
     INSERT INTO cmp_mtransac  
     VALUES (w_mae_trc.*)
     LET w_mae_trc.lnkcmp = SQLCA.SQLERRD[2]

     LET msg = " Movimiento de compra "||w_mae_trc.lnkcmp||" registrado."
    WHEN 2 -- Actualizando 

     -- Actualizando movimiento
     UPDATE cmp_mtransac
     SET    cmp_mtransac.*      = w_mae_trc.*
     WHERE  cmp_mtransac.lnkcmp = w_mae_trc.lnkcmp

     LET msg = " Movimiento de compra "||w_mae_trc.lnkcmp||" actualizado."
   END CASE 

   -- Borrando detalle de costos antes de grabar
   DELETE FROM cmp_dtransac 
   WHERE cmp_dtransac.lnkcmp = w_mae_trc.lnkcmp 

   -- Grabando detalle de costos  
   FOR i = 1 TO totcos 
    IF v_costos[i].codcos IS NULL THEN
       CONTINUE FOR
    END IF   

    INSERT INTO cmp_dtransac
    VALUES (w_mae_trc.lnkcmp,i,v_costos[i].codcos,v_costos[i].totval) 
   END FOR 

   -- Grabando maestro de poliza contable
   IF w_tip_mov.polctb=1 THEN 
    -- Borrando datos de la poliza (compras)
    DELETE FROM cmp_dtpoliza 
    WHERE cmp_dtpoliza.lnkcmp = w_mae_trc.lnkcmp  
 
    -- Borrando datos de la poliza (contabilidad) 
    DELETE FROM ctb_mtransac 
    WHERE ctb_mtransac.lnktra = w_mae_trc.lnkctb 

    -- Asignando datos
    LET w_mae_ctb.lnktra = 0
    LET w_mae_ctb.codemp = w_mae_trc.codemp
    LET w_mae_ctb.tiptrn = TIPOTRNPOLIZAEGRESO  

    -- Obteniendo tipo de nomenclatura de la empresa 
    INITIALIZE xtipnom TO NULL
    SELECT a.tipnom INTO xtipnom FROM glb_empresas a
     WHERE a.codemp = w_mae_ctb.codemp 

    LET w_mae_ctb.fecemi = w_mae_trc.fecemi 
    LET w_mae_ctb.numdoc = librut003_NumeroPolizaAutomatica(w_mae_ctb.codemp,
                                                            w_mae_ctb.tiptrn,
                                                            w_mae_ctb.fecemi)
    LET w_mae_ctb.totdoc = w_mae_trc.totdoc 
    LET w_mae_ctb.concep = W_mae_trc.descrp 
    LET w_mae_ctb.codapl = codigoapl 
    LET w_mae_ctb.lnkapl = w_mae_trc.lnkcmp 
    LET w_mae_ctb.coddiv = 1 
    LET w_mae_ctb.estado = 1 
    LET w_mae_ctb.infadi = " SERIE # ("||w_mae_trc.nserie CLIPPED||
                           ") DOCUMENTO # ("||w_mae_trc.numdoc CLIPPED||
                           ") DE FECHA ("||w_mae_trc.fecemi||
                           ") MOVIMIENTO COMPRAS # ("||w_mae_trc.lnkcmp||
                           ") PROVEEDOR ("||w_mae_trc.nomsoc CLIPPED||")" 
    LET w_mae_ctb.userid = username
    LET w_mae_ctb.fecsis = CURRENT 
    LET w_mae_ctb.horsis = CURRENT HOUR TO SECOND 

    INSERT INTO ctb_mtransac
    VALUES (w_mae_ctb.*) 
    LET w_mae_ctb.lnktra = SQLCA.SQLERRD[2] 

    -- Grabando detalle de cuentas de la poliza contable
    LET correl = 0
    FOR i = 1 TO totlin
     IF v_partida[i].cnumcta IS NULL THEN
       CONTINUE FOR
     END IF 
     LET correl = correl+1

     -- Verificando debe y haber
     IF (v_partida[i].ctotdeb>0) THEN
       LET xtipope = "D"
       LET xtotval = v_partida[i].ctotdeb
     ELSE
       LET xtipope = "H"
       LET xtotval = v_partida[i].ctothab
     END IF

     -- Grabando detalle de cuentas
     INSERT INTO cmp_dtpoliza 
     VALUES (w_mae_trc.lnkcmp    , -- link del encabezado
             correl              , -- correlativo
             v_partida[i].cnumcta, -- numero de cuenta
             v_partida[i].ctotdeb, -- total debe
             v_partida[i].ctothab, -- total haber
             v_partida[i].cconcpt, -- concepto de la linea
             xtipope)              -- tipo de operacion 

     -- Grabando detalle de cuentas de poliza contable
     INSERT INTO ctb_dtransac
     VALUES (w_mae_ctb.lnktra    , -- link del encabezado
             correl              , -- correlativo
             v_partida[i].cnumcta, -- numero de cuenta
             v_partida[i].cconcpt, -- concepto de la linea
             v_partida[i].ctotdeb, -- total debe
             v_partida[i].ctothab, -- total haber
             xtipope)

     -- Creando cuentas padre
     CALL librut003_CuentasPadre(
             w_mae_ctb.lnktra,
             correl,
             w_mae_ctb.codemp,
             w_mae_ctb.fecemi,
             v_partida[i].cnumcta,
             xtipnom,
             xtipope,
             xtotval,
             v_partida[i].ctotdeb,
             v_partida[i].ctothab,
             "N",
             "N")
    END FOR 

    -- Actualizando link contable en maestro de compras
    UPDATE cmp_mtransac
    SET    cmp_mtransac.lnkctb = w_mae_ctb.lnktra 
    WHERE  cmp_mtransac.lnkcmp = w_mae_trc.lnkcmp
   END IF 

 -- Finalizando la transaccion
 COMMIT WORK
 CALL fgl_winmessage("Atencion",msg,"information")

 ERROR ""
END FUNCTION  

-- Subrutina para inicializar las variables de trabajo 

FUNCTION cmping001_inival(i)
 DEFINE i       SMALLINT,
        wfecha  VARCHAR(80) 
 
 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1  
   INITIALIZE w_mae_trc.* TO NULL 
   CALL v_costos.clear() 
   CALL cmping001_inivec()
   CLEAR FORM 
   LET w_mae_trc.codemp = gcodemp  
   LET w_mae_trc.tipmov = gtipmov 

   -- Obteniendo datos de la empresa 
   INITIALIZE w_mae_emp.* TO NULL 
   CALL librut003_bempresa(w_mae_trc.codemp)
   RETURNING w_mae_emp.*,existe 
  WHEN 2 
   INITIALIZE w_mae_trc.tipmov THRU w_mae_trc.numnit TO NULL 
   CALL v_costos.clear() 
   CALL cmping001_inivec()
   CLEAR tipmov,numdoc,codsoc,fecemi,feclbc,descrp,codaut,codcos,codrub,
         totdoc,totgra,totexe,totisv,totsal,totabo,totval,frmpag,diacre,
         fecven,estado,lnkinv,userid,fecsis,horsis,motanl,usranl,fecanl,
         horanl,nomsoc,numnit,tipcom,tipdoc,nserie,totigt,totedp,totded,
         numest 
   LET w_mae_trc.tipmov = gtipmov 
  WHEN 3 
   INITIALIZE w_mae_trc.numdoc,w_mae_trc.codsoc THRU w_mae_trc.numnit TO NULL 
   CALL v_costos.clear() 
   CALL cmping001_inivec()
   CLEAR numdoc,codsoc,fecemi,feclbc,descrp,codaut,codcos,codrub,totdoc,
         totgra,totexe,totisv,totsal,totabo,totval,frmpag,diacre,fecven,
         estado,lnkinv,userid,fecsis,horsis,motanl,usranl,fecanl,horanl,
         nomsoc,numnit,tipcom,tipdoc,nserie,totigt,totedp,totded,numest 
 END CASE 

 -- Inicializando datos
 LET w_mae_trc.lnkcmp = 0                    
 LET w_mae_trc.codaut = gcodaut 
 LET w_mae_trc.codcos = gcodcos 
 LET w_mae_trc.fecemi = CURRENT  
 LET w_mae_trc.estado = 1
 LET w_mae_trc.fecven = CURRENT
 LET w_mae_trc.diacre = 0
 LET w_mae_trc.totgra = 0
 LET w_mae_trc.totexe = 0
 LET w_mae_trc.totisv = 0
 LET w_mae_trc.totigt = 0
 LET w_mae_trc.totedp = 0
 LET w_mae_trc.totded = 0
 LET w_mae_trc.totdoc = 0 
 LET w_mae_trc.totsal = 0 
 LET w_mae_trc.totabo = 0
 LET w_mae_trc.totval = 0 
 LET w_mae_trc.porisv = porcenisv 
 LET w_mae_trc.porigt = 0 
 LET w_mae_trc.poredp = 0
 LET w_mae_trc.lnkinv = 0 
 LET w_mae_trc.frmpag = 2
 LET w_mae_trc.numest = 1
 LET w_mae_trc.tipcom = 1
 LET w_mae_trc.userid = username 
 LET w_mae_trc.fecsis = CURRENT 
 LET w_mae_trc.horsis = CURRENT HOUR TO SECOND
 LET w_mae_trc.lnkctb = 0 
 LET totcos           = 0 

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_trc.codemp,
                 w_mae_trc.tipmov, 
                 w_mae_trc.codaut, 
                 w_mae_trc.codcos, 
                 w_mae_trc.fecven,
                 w_mae_trc.diacre,
                 w_mae_trc.fecemi,
                 w_mae_trc.totgra, 
                 w_mae_trc.totexe, 
                 w_mae_trc.totisv, 
                 w_mae_trc.totigt, 
                 w_mae_trc.totedp, 
                 w_mae_trc.totded, 
                 w_mae_trc.totdoc, 
                 w_mae_trc.tipcom, 
                 w_mae_trc.tipdoc, 
                 w_mae_trc.numest, 
                 w_mae_trc.frmpag, 
                 w_mae_trc.userid,
                 w_mae_trc.fecsis,
                 w_mae_trc.horsis,
                 w_mae_trc.estado

 -- Desplegando fecha 
 LET wfecha = librut001_formatofecha(TODAY,1,wpais) CLIPPED
 CALL librut001_dpelement("labelz",wfecha)
END FUNCTION

-- Subrutina para inicializar el vector de trabajo

FUNCTION cmping001_inivec()
 -- Inicializando poliza contable 
 CALL v_partida.clear() 
 LET totlin = 0 
 DISPLAY ARRAY v_partida TO s_partida.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 
END FUNCTION  

-- Subrutina para desplegar los estados del menu 

FUNCTION cmping001_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement(
         "group1","Datos del Movimiento de Compra - MENU")
  WHEN 1 CALL librut001_dpelement(
         "group1","Datos del Movimiento de Compra - CONSULTAR"||msg CLIPPED)
  WHEN 2 CALL librut001_dpelement(
         "group1","Datos del Movimiento de Compra - ANULAR"||msg CLIPPED) 
  WHEN 3 CALL librut001_dpelement(
         "group1","Datos del Movimiento de Compra - ELIMINAR"||msg CLIPPED)
  WHEN 4 CALL librut001_dpelement(
         "group1","Datos del Movimiento de Compra - INGRESAR") 
  WHEN 5 CALL librut001_dpelement(
         "group1","Datos del Movimiento de Compra - MODIFICAR") 
 END CASE
END FUNCTION

-- Subutina para el ingreso del detalle de cuentas de la poliza contable

FUNCTION cmping001_DetallePoliza()
 DEFINE w_mae_cta  RECORD LIKE ctb_mcuentas.*,
        loop,scr   SMALLINT,
        opc,arr,i  SMALLINT, 
        linea      SMALLINT, 
        retroceso  SMALLINT, 
        lastkey    INTEGER  

 -- Inicializando la poliza contable 
 IF (totlin=0) THEN
    -- Cargando cuentas default x rubro  
    {CALL cmping001_PolizaDefault() 
    IF (totlin=0) THEN
       CALL fgl_winmessage(
       "Atencion","Rubro de gasto sin cuentas contables definidas.","exclamation") 
    END IF } 
 END IF 

 -- Iniciando el loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop

  -- Ingresando productos
  INPUT ARRAY v_partida WITHOUT DEFAULTS FROM s_partida.*
   ATTRIBUTE(COUNT=totlin,INSERT ROW=FALSE,ACCEPT=FALSE,CANCEL=FALSE,
             UNBUFFERED=TRUE,FIELD ORDER FORM)

   ON ACTION accept 
    -- Aceptar
    EXIT INPUT

   ON ACTION cancel 
    -- Cancelar
    LET retroceso = TRUE
    LET loop      = FALSE
    EXIT INPUT

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   ON ACTION listacuenta  
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Seleccionado lista de cuentas contables
    CALL librut002_ListaBusqueda("Consulta de Cuentas Contables ",
                            "# Cuenta",
                            "Nombre de la Cuenta",
                            "",
                            "numcta",
                            "nomcta",
                            "", 
                            "ctb_mcuentas",
                            "codemp = "||w_mae_trc.codemp||" AND tipcta='D'",
                            1)
    RETURNING v_partida[arr].cnumcta,v_partida[arr].cnomcta,regreso 
    IF regreso THEN
       NEXT FIELD cnumcta
    ELSE 
       -- Asignando descripcion
       DISPLAY v_partida[arr].cnumcta TO s_partida[scr].cnumcta 
       DISPLAY v_partida[arr].cnomcta TO s_partida[scr].cnomcta 
    END IF  
   
   BEFORE INPUT 
    CALL DIALOG.setActionHidden("append",TRUE)

    -- Totalizando cuentas
    CALL cmping001_TotalCuentasPoliza() 

   BEFORE ROW
    LET totlin = ARR_COUNT()

   BEFORE FIELD cnumcta 
    -- Habilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",TRUE)

   AFTER FIELD cnumcta 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) THEN 
       NEXT FIELD cnumcta 
    END IF
 
   BEFORE FIELD ctotdeb 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    IF v_partida[arr].cnumcta IS NULL THEN 
     -- Seleccionado lista de cuentas contables
     CALL librut002_ListaBusqueda("Consulta de Cuentas Contables ",
                             "# Cuenta",
                             "Nombre de la Cuenta",
                             "",
                             "numcta",
                             "nomcta",
                             "", 
                             "ctb_mcuentas",
                             "codemp = "||w_mae_trc.codemp||" AND tipcta='D'",
                             1)
     RETURNING v_partida[arr].cnumcta,v_partida[arr].cnomcta,regreso 
     IF regreso THEN
        NEXT FIELD cnumcta
     ELSE 
        -- Asignando descripcion
        DISPLAY v_partida[arr].cnumcta TO s_partida[scr].cnumcta 
        DISPLAY v_partida[arr].cnomcta TO s_partida[scr].cnomcta 
     END IF  
    END IF  

    -- Verificando numero de cuenta 
    IF (LENGTH(v_partida[arr].cnumcta)<=0) THEN 
       CALL fgl_winmessage(
       "Atencion",
       "Numero de cuenta invalida, VERIFICA.",
       "stop")
       INITIALIZE v_partida[arr].cnumcta THRU v_partida[arr].cconcpt TO NULL
       CLEAR s_partida[scr].cnumcta,s_partida[scr].cnomcta, 
             s_partida[scr].ctotdeb,s_partida[scr].ctothab, 
             s_partida[scr].cconcpt 
       NEXT FIELD cnumcta 
    END IF 

    -- Verificando si la cuenta existe   
    INITIALIZE w_mae_cta.* TO NULL
    CALL librut003_BCuentaContable(w_mae_trc.codemp,v_partida[arr].cnumcta)
    RETURNING w_mae_cta.*,existe
    IF NOT existe THEN
       CALL fgl_winmessage(
       "Atencion",
       "Numero de cuenta no registrada, VERIFICA.",
       "top")
       INITIALIZE v_partida[arr].cnumcta THRU v_partida[arr].cconcpt TO NULL
       CLEAR s_partida[scr].cnumcta,s_partida[scr].cnomcta, 
             s_partida[scr].ctotdeb,s_partida[scr].ctothab, 
             s_partida[scr].cconcpt
       NEXT FIELD cnumcta 
    END IF 

    -- Asignando descripcion
    LET v_partida[arr].cnomcta = w_mae_cta.nomcta 
    DISPLAY v_partida[arr].cnomcta TO s_partida[scr].cnomcta 

    -- Verificando que cuenta se de detalle 
    IF w_mae_cta.tipcta="M" THEN 
       CALL fgl_winmessage(
       "Atencion",
       "Numero de cuenta no es de detalle, VERIFICA.",
       "stop")
       INITIALIZE v_partida[arr].cnumcta THRU v_partida[arr].cconcpt TO NULL
       CLEAR s_partida[scr].cnumcta,s_partida[scr].cnomcta, 
             s_partida[scr].ctotdeb,s_partida[scr].ctothab, 
             s_partida[scr].cconcpt
       NEXT FIELD cnumcta
    END IF 

    -- Deshabilitando la opcion de aceptar
    CALL DIALOG.setActionActive("accept",FALSE)

    -- Totalizando cuentas
    CALL cmping001_TotalCuentasPoliza() 

   ON CHANGE ctotdeb 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Totalizando cuentas
    CALL cmping001_TotalCuentasPoliza() 

   AFTER FIELD ctotdeb
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD ctotdeb 
    END IF

    -- Verificando total del debe
    IF v_partida[arr].ctotdeb IS NULL OR
       (v_partida[arr].ctotdeb <0) OR
       (v_partida[arr].ctotdeb >w_mae_par.totval) THEN 
       LET v_partida[arr].ctotdeb = 0 
    END IF 
    
    -- Totalizando cuentas
    CALL cmping001_TotalCuentasPoliza() 

   ON CHANGE ctothab 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Totalizando cuentas
    CALL cmping001_TotalCuentasPoliza() 

   AFTER FIELD ctothab 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD ctothab 
    END IF

    -- Verificando total del haber 
    IF v_partida[arr].ctothab IS NULL OR
       (v_partida[arr].ctothab <0) OR
       (v_partida[arr].ctothab >w_mae_par.totval) THEN 
       LET v_partida[arr].ctothab = 0 
    END IF 

    -- Verificando totales de debe y haber = 0
    IF (v_partida[arr].ctotdeb=0) AND (v_partida[arr].ctothab=0) THEN
       LET msg = "Tota del debe y haber no pueden ser ambos ceros, VERIFICA."
       CALL fgl_winmessage("Atencion",msg,"stop")
       NEXT FIELD ctotdeb 
    END IF

    -- Verificando totales de debe y haber >0 
    IF (v_partida[arr].ctotdeb>0) AND (v_partida[arr].ctothab>0) THEN
     LET msg = "Totales debe y haber no pueden ser ambos mayores a cero, VERIFICA."
       CALL fgl_winmessage("Atencion",msg,"stop")
       NEXT FIELD ctotdeb 
    END IF

    -- Totalizando cuentas
    CALL cmping001_TotalCuentasPoliza() 

   AFTER FIELD cconcpt 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Verificando ultima tecla presionada
    LET lastkey = FGL_LASTKEY()
    IF (lastkey = FGL_KEYVAL("down")) OR 
       (lastkey = FGL_KEYVAL("up")) THEN 
       NEXT FIELD cconcpt 
    END IF

   AFTER ROW,INSERT  
    LET totlin = ARR_COUNT()

    -- Totalizando cuentas
    CALL cmping001_TotalCuentasPoliza() 

   ON ACTION delete
    -- Eliminando linea seleccionada
    LET arr = ARR_CURR() 
    CALL v_partida.deleteElement(arr)

    -- Totalizando cuentas
    CALL cmping001_TotalCuentasPoliza() 
    NEXT FIELD cnumcta 

   ON ROW CHANGE 
    LET arr = ARR_CURR()
    LET scr = SCR_LINE()

    -- Totalizando cuentas
    CALL cmping001_TotalCuentasPoliza() 

   AFTER INPUT 
    LET totlin = ARR_COUNT()

    -- Totalizando cuentas
    CALL cmping001_TotalCuentasPoliza() 
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Desplegando datos 
  DISPLAY ARRAY v_partida TO s_partida.*
   BEFORE DISPLAY 
    EXIT DISPLAY 
  END DISPLAY

  -- Totalizando cuentas
  CALL cmping001_TotalCuentasPoliza() 

  -- Verificando lineas incompletas 
  LET linea = cmping001_CuentasPolizaIncompletas()
  IF (linea>0) THEN
     LET msg = " Linea ("||linea||") incompleta. \n VERIFICA."
     CALL fgl_winmessage(
     "Atencion",msg,"stop")
     CONTINUE WHILE
  END IF 

  -- Verificando cuadre de debe y haber 
  IF (totaldeb!=totalhab) OR
     ((totaldeb+totalhab)=0) THEN
     CALL fgl_winmessage(
     "Atencion",
     "Total del debe y haber invalidos."||
     "\nVerificar que ambos totales sean iguales, sin ser ceros.",
     "stop")
     CONTINUE WHILE
  END IF

  LET loop = FALSE 
 END WHILE

 RETURN retroceso
END FUNCTION

-- Subrutina para totalizar el detalle de cuentas de la poliza contable 

FUNCTION cmping001_TotalCuentasPoliza()
 DEFINE i SMALLINT

 -- Totalizando
 LET totaldeb = 0
 LET totalhab = 0
 FOR i = 1 TO totlin  
  IF v_partida[i].ctotdeb IS NOT NULL THEN
     LET totaldeb = (totaldeb+v_partida[i].ctotdeb)
  END IF
  IF (v_partida[i].ctothab IS NOT NULL) THEN
     LET totalhab = (totalhab+v_partida[i].ctothab)
  END IF
 END FOR
 DISPLAY BY NAME totaldeb,totalhab
END FUNCTION

-- Subrutina para verificar si hay cuentas incompletas

FUNCTION cmping001_CuentasPolizaIncompletas()
 DEFINE i,linea SMALLINT 

 LET linea = 0 
 FOR i = 1 TO totlin 
  IF v_partida[i].cnumcta IS NULL THEN
     CONTINUE FOR
  END IF

  -- Verificando lineas
  IF v_partida[i].cnomcta IS NULL OR
     v_partida[i].ctotdeb IS NULL OR
     v_partida[i].ctothab IS NULL THEN 
     LET linea = i 
     EXIT FOR 
  END IF 
 END FOR

 RETURN linea 
END FUNCTION

-- Subrutina para asignar datos a poliza default 

FUNCTION cmping001_PolizaDefault() 
 DEFINE xtipcol LIKE glb_ctasxrub.tipcol 

 -- Obteniendo datos de la poliza default x rubro
 DECLARE c1 CURSOR FOR
 SELECT x.numcta,
        y.nomcta, 
        0,
        0,
        "",
        "", 
        x.tipcol,
        x.numcor
  FROM  fac_ctasxrub x,ctb_mcuentas y
  WHERE x.codrub = w_mae_trc.codrub 
    AND x.codemp = w_mae_trc.codemp 
    AND y.codemp = x.codemp  
    AND y.numcta = x.numcta 
 ORDER BY x.numcor

 LET totlin = 1 
 FOREACH c1 INTO v_partida[totlin].*,xtipcol
  IF (xtipcol="D") THEN
     LET v_partida[totlin].ctotdeb = w_mae_trc.totdoc
  ELSE
     LET v_partida[totlin].ctothab = w_mae_trc.totdoc
  END IF 
  LET totlin = totlin+1 
 END FOREACH
 CLOSE c1
 FREE c1 
 LET totlin = totlin-1 
END FUNCTION 
