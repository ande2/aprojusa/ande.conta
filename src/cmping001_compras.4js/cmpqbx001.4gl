{ 
Programo : Mynor Ramirez 
Objetivo : Programa de criterios de seleccion para:
           Consultar/anular/eliminar movimiento de compras
} 

-- Definicion de variables globales 
GLOBALS "cmpglb001.4gl"
DEFINE v_consulta DYNAMIC ARRAY OF RECORD 
        tlnkcmp   INT, 
        ttipmov   INT, 
        tcodsoc   INT,
        tnommov   CHAR(25), 
        tnomsoc   CHAR(40),
        tfecemi   DATE,
        tnserie   CHAR(15),
        tnumdoc   CHAR(15),
        ttotdoc   DEC(14,2), 
        trellen   CHAR(1) 
       END RECORD 
DEFINE totrec INT 

-- Subrutina para consultar/anular/eliminar compras 

FUNCTION cmpqbx001_Compras(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart,qry STRING, 
        loop,existe         SMALLINT,
        operacion,haypagos  SMALLINT,
        qryres,msg          CHAR(80),
        wherestado          STRING,
        cmdstr              STRING, 
        opc,totreg,res,arr  INTEGER 

 -- Verificando operacion
 CASE (operacion)
  WHEN 1 LET wherestado = NULL
  WHEN 2 LET wherestado = " AND a.estado = 1" 
  WHEN 3 LET wherestado = " AND a.estado = 1" 
 END CASE 

 -- Definiendo nivel de aislamiento 
 SET ISOLATION TO DIRTY READ 

 -- Llenando comboox de tipos de documento del regimen fiscal
 CALL librut003_CbxTiposDocRegimenFisTodos()

 -- Buscando datos 
 LET loop = TRUE 
 WHILE loop 
  -- Inicializando las variables 
  CALL cmping001_EstadoMenu(operacion," ")
  INITIALIZE qrytext,qrypart TO NULL
  LET int_flag = 0 
  CLEAR FORM
                                                     
  -- Construyendo busqueda 
  CONSTRUCT BY NAME qrytext 
                 ON a.tipmov, 
                    a.codsoc,
                    a.numnit, 
                    a.nomsoc,
                    a.codaut,
                    a.codcos, 
                    a.codrub, 
                    a.fecemi,
                    a.feclbc,
                    a.diacre,
                    a.fecven,
                    a.nserie, 
                    a.numdoc, 
                    a.totgra,
                    a.totexe,
                    a.totisv,
                    a.totigt,
                    a.totedp,
                    a.totded, 
                    a.totdoc,
                    a.descrp,
                    a.numest, 
                    a.tipcom,
                    a.tipdoc, 
                    a.frmpag, 
                    a.motanl,
                    a.estado,
                    a.lnkcmp,
                    a.lnkinv, 
                    a.userid,
                    a.fecsis, 
                    a.horsis,
                    a.usranl,
                    a.fecanl,
                    a.horanl,
                    a.totabo,
                    a.totsal,
                    a.fulabo 
   ON ACTION cancel
    -- Salida
    LET loop = FALSE
    EXIT CONSTRUCT
  END CONSTRUCT
  IF NOT loop THEN
     EXIT WHILE
  END IF 

  -- Preparando la busqueda 
  ERROR " Seleccionando datos ... por favor espere ..." ATTRIBUTE(CYAN) 

  -- Creando la busqueda 
  LET qrypart = "SELECT a.lnkcmp,a.tipmov,a.codsoc,t.nommov,",
                       "a.nomsoc,a.fecemi,a.nserie,a.numdoc,a.totdoc,'' ",
                 "FROM  cmp_mtransac a,glb_empresas e,cmp_tipomovs t,",
                       "glb_cencosto c,glb_userauth u,fac_rubgasto r ", 
                 "WHERE a.codemp = ",gcodemp," AND ",
                 qrytext CLIPPED,wherestado CLIPPED,
                  " AND e.codemp = a.codemp ",
                  " AND t.tipmov = a.tipmov ", 
                  " AND c.codcos = a.codcos ",
                  " AND u.codaut = a.codaut ", 
                  " AND r.codrub = a.codrub ",
                 " ORDER BY a.lnkcmp DESC" 

  -- Contando registros
  LET totreg = librut003_NumeroRegistros(qrypart)

  -- Declarando el cursor 
  PREPARE estqbx001 FROM qrypart
  DECLARE c_movimientos SCROLL CURSOR WITH HOLD FOR estqbx001  
     
  LET totrec = 1 
  CALL v_consulta.clear() 
  FOREACH c_movimientos INTO v_consulta[totrec].* 
   LET totrec = totrec+1
  END FOREACH
  CLOSE c_movimientos
  FREE  c_movimientos 
  LET totrec = totrec-1 

  -- Verificando si hay movimientos 
  IF (totrec=0) THEN 
     ERROR ""
     CALL fgl_winmessage(
     "Atencion",
     "No existen movimientos de compras con el criterio seleccionado.",
     "stop") 
  ELSE
     ERROR "" 

     -- Escondiendo tabla de consultas 
     CALL f.SetElementHidden("titulolista",0) 
     CALL f.SetElementHidden("tablaconsulta",0) 

     -- Desplegando datos 
     DIALOG ATTRIBUTES(UNBUFFERED,FIELD ORDER FORM)
 
      -- Desplegando movimientos
      DISPLAY ARRAY v_consulta TO s_consulta.* 
       ATTRIBUTE (COUNT=totrec,KEEP CURRENT ROW) 

       BEFORE DISPLAY 
        CALL Dialog.SetActionHidden("close",1)

        -- Verificando tipo de operacion 
        CASE (operacion)
         WHEN 1 -- Desabilitando acciones              
          CALL Dialog.SetActionActive("anular",0)
          CALL Dialog.SetActionActive("delete",0)

          -- Verificando acceso a opcion de modificacion 
          IF NOT seclib001_accesos(progname,5,username) THEN
             CALL Dialog.SetActionActive("modificar",0)
          END IF
          
         WHEN 2 -- Desabilitando acciones              
          CALL Dialog.SetActionActive("delete",0)
          CALL Dialog.SetActionActive("modificar",0)
         WHEN 3 -- Desabilitando acciones              
          CALL Dialog.SetActionActive("anular",0)
          CALL Dialog.SetActionActive("modificar",0)
        END CASE

        -- Desplegando datos 
        LET msg = " - Registros [ ",totreg||" ]"
        CALL cmping001_EstadoMenu(operacion,msg CLIPPED)

       BEFORE ROW 
        LET arr = DIALOG.getcurrentrow("s_consulta") 
        IF (arr>totrec) THEN
            CALL FGL_SET_ARR_CURR(totrec)
        END IF

        -- Desplegando datos 
        CALL cmpqbx001_datos(v_consulta[arr].tlnkcmp, 
                             v_consulta[arr].ttipmov,
                             v_consulta[arr].tcodsoc)

       AFTER ROW 
        LET arr = DIALOG.getcurrentrow("s_consulta") 

        -- Desplegando datos 
        CALL cmpqbx001_datos(v_consulta[arr].tlnkcmp, 
                             v_consulta[arr].ttipmov,
                             v_consulta[arr].tcodsoc)

       ON ACTION modificar 
        LET arr = DIALOG.getcurrentrow("s_consulta") 

        -- Verificando si documento de compra ya fue anulado
        IF w_mae_trc.estado=0 THEN
           CALL fgl_winmessage(
           "Atencion", 
           "Movimiento de compras ya fue anulado, VERIFICA.",
           "stop") 
           CONTINUE DIALOG  
        END IF 

        -- Verificando fecha del documento a modificar 
        IF (w_mae_trc.fecemi<(TODAY-w_mae_par.diamod)) THEN  
           CALL fgl_winmessage(
           "Atencion",
           "Movimiento de compra no puede modificarse.\n"||
           "Fecha documento excede limite de dias. ("||w_mae_par.diamod||")", 
           "stop")
           CONTINUE DIALOG 
        END IF 
 
        -- Verificando si documento de compra ya tiene abonos
        LET haypagos = FALSE 
        IF w_tip_mov.afesal=1 THEN 
         IF w_mae_trc.totabo>0 THEN
           LET haypagos = TRUE 
           CALL fgl_winmessage(
           "Atencion", 
           "Movimiento de compras ya tiene pagos emitidos."||
           "\nSolo podran modificarse algunos datos.", 
           "stop") 
         END IF 
        END IF 

        -- Modificando movimientos de compra 
        CALL cmping001_Compras(2,haypagos)

        -- Llenando combobox de tipos de documento del regimen fiscal
        CALL librut003_CbxTiposDocRegimenFisTodos()

        -- Desplegando datos 
        CALL cmpqbx001_datos(v_consulta[arr].tlnkcmp, 
                             v_consulta[arr].ttipmov,
                             v_consulta[arr].tcodsoc)

        -- Regresando estado del menu
        CALL cmping001_EstadoMenu(operacion,msg CLIPPED)

       ON ACTION anular 
        -- Verificando si documento fue originado por modulo de inventarios 
        IF w_mae_trc.lnkinv>0 THEN
           CALL fgl_winmessage(
           "Atencion", 
           "Movimiento de compras originado desde inventarios. \n"||
           "Movimiento no puede anularse desde esta opcion, VERIFICA.",
           "stop") 
           CONTINUE DIALOG 
        END IF 

        -- Verificando si documento de compra ya fue anulado
        IF w_mae_trc.estado=0 THEN
           CALL fgl_winmessage(
           "Atencion", 
           "Movimiento de compras ya fue anulado, VERIFICA.",
           "stop") 
           CONTINUE DIALOG 
        END IF 

        -- Verificando fecha del documento a anular    
        IF (w_mae_trc.fecemi<(TODAY-w_mae_par.diaanu)) THEN  
           CALL fgl_winmessage(
           "Atencion",
           "Movimiento de compra no puede anularse.\n"||
           "Fecha de documento excede limite de dias. ("||w_mae_par.diaanu||")", 
           "stop")
           CONTINUE DIALOG 
        END IF 

        -- Verificando si documento de compra ya tiene abonos
        IF w_tip_mov.afesal=1 THEN 
         IF w_mae_trc.totabo>0 THEN
           CALL fgl_winmessage(
           "Atencion", 
           "Movimiento de compras ya tiene pagos emitidos, no puede anularse.",
           "stop") 
           CONTINUE DIALOG 
         END IF 
        END IF 

        -- Anulando compra 
        CALL cmpqbx001_AnularCompra()      
 
        -- Desplegando datos 
        CALL cmpqbx001_datos(v_consulta[arr].tlnkcmp, 
                             v_consulta[arr].ttipmov,
                             v_consulta[arr].tcodsoc)
 
       ON ACTION delete 
        -- Verificando si documento fue originado por modulo de inventarios 
        IF w_mae_trc.lnkinv>0 THEN
           CALL fgl_winmessage(
           "Atencion", 
           "Movimiento de compras originado desde inventarios.\n"||
           "Movimiento no puede eliminarse desde esta opcion, VERIFICA.", 
           "stop") 
           CONTINUE DIALOG 
        END IF 

        -- Verificando si documento de compra ya tiene abonos
        IF w_tip_mov.afesal=1 THEN 
         IF w_mae_trc.totabo>0 THEN
           CALL fgl_winmessage(
           "Atencion", 
           "Movimiento de compras ya tiene pagos emitidos, no puede eliminarse.",
           "stop") 
           CONTINUE DIALOG 
         END IF 
        END IF 

        -- Eliminando compra 
        IF cmpqbx001_EliminarCompra() THEN 
           EXIT DIALOG 
        END IF 

       ON ACTION partida          
        -- Ejecutando consulta de la partida contable 
        IF ui.Interface.getChildInstances("transaccionesctb")==0 THEN
           LET cmdstr = "fglrun transaccionesctb.42r 1 "||w_mae_trc.lnkcmp||" CMP" 
           RUN cmdstr WITHOUT WAITING
        ELSE
           CALL fgl_winmessage(
           "Atencion","Consulta de registro contable ya en ejecucion.","stop")
        END IF 
      END DISPLAY 

      ON ACTION prevrow   
       -- Siguiente fila 

      ON ACTION nextrow   
       -- Fila Anterior  

      ON ACTION consultar  
       -- Volver a consultar 
       EXIT DIALOG  

      ON ACTION cancel
       -- Salida 
       LET loop = FALSE
       EXIT DIALOG 

      ON KEY(F4,CONTROL-E)
       -- Salida 
       LET loop = FALSE
       EXIT DIALOG 
     END DIALOG 
 
     -- Escondiendo tabla de consultas 
     CALL f.SetElementHidden("titulolista",1) 
     CALL f.SetElementHidden("tablaconsulta",1) 
  END IF     
 END WHILE

 -- Desplegando estado del menu 
 CALL cmping001_estadoMenu(0,"")

 -- Inicializando datos 
 CALL cmping001_inival(1)
END FUNCTION 

-- Subrutina para desplegar los datos del movimiento de compra 

FUNCTION cmpqbx001_datos(xlnkcmp,xtipmov,xcodsoc)
 DEFINE xlnkcmp LIKE cmp_mtransac.lnkcmp,
        xtipmov LIKE cmp_mtransac.tipmov,
        xcodsoc LIKE cmp_mtransac.codsoc, 
        existe  SMALLINT

 -- Obteniendo datos del movimiento de compra 
 INITIALIZE w_mae_trc.* TO NULL  
 CALL librut003_BMovimientoCompras(xlnkcmp) 
 RETURNING w_mae_trc.*,existe

 -- Obteniendo datos del tipo de movimiento de compra
 INITIALIZE w_tip_mov.* TO NULL
 CALL librut003_BTipoMovimientoCompras(xtipmov)
 RETURNING w_tip_mov.*,existe

 -- Obteniendo datos del socio
 INITIALIZE w_mae_pro.* TO NULL
 CALL librut003_BSociosNeg(xcodsoc)
 RETURNING w_mae_pro.*,existe

 -- Desplegando datos del movimiento de compra 
 DISPLAY BY NAME w_mae_trc.codemp, 
                 w_mae_trc.tipmov, w_mae_trc.numdoc,
                 w_mae_trc.codaut, w_mae_trc.codrub,
                 w_mae_trc.totgra, w_mae_trc.fecemi, 
                 w_mae_trc.fecven, w_mae_trc.diacre, 
                 w_mae_trc.totisv, w_mae_trc.totdoc, 
                 w_mae_trc.descrp, w_mae_trc.frmpag, 
                 w_mae_trc.motanl, w_mae_trc.estado, 
                 w_mae_trc.userid, w_mae_trc.totexe,
                 w_mae_trc.fecsis, w_mae_trc.horsis, 
                 w_mae_trc.usranl, w_mae_trc.fecanl, 
                 w_mae_trc.horanl, w_mae_trc.lnkinv,
                 w_mae_trc.codsoc, w_mae_trc.codcos,
                 w_mae_trc.lnkcmp, w_mae_trc.totabo,
                 w_mae_trc.totsal, w_mae_trc.nomsoc,
                 w_mae_trc.numnit, w_mae_trc.nserie,
                 w_mae_trc.tipcom, w_mae_trc.tipdoc,
                 w_mae_trc.totigt, w_mae_trc.totedp,
                 w_mae_trc.totded, w_mae_trc.numest, 
                 w_mae_trc.feclbc, w_mae_trc.fulabo, 
                 w_mae_trc.totval 

 -- Obteniendo detalle de costos de la compra
 -- Inicializando datos
 CALL v_costos.clear()
 LET totcos = 1
 DECLARE c_costos CURSOR FOR 
 SELECT a.codcos,
        a.totval,
        "",
        a.correl 
  FROM  cmp_dtransac a
  WHERE a.lnkcmp = w_mae_trc.lnkcmp 
  ORDER BY a.correl
  FOREACH c_costos INTO v_costos[totcos].*
   LET totcos = totcos+1
  END FOREACH
  CLOSE c_costos
  FREE  c_costos
  LET totcos = totcos-1 

 -- Desplegando detalle de la poliza contable
 CALL cmpqbx001_PolizaContable()
END FUNCTION 

-- Subrutina para anular el movimiento de compra 

FUNCTION cmpqbx001_AnularCompra()      
 DEFINE anula SMALLINT

 -- Ingresando motivo de la anulacion
 LET anula = TRUE 
 INPUT BY NAME w_mae_trc.motanl WITHOUT DEFAULTS 
  ATTRIBUTE(UNBUFFERED) 
  AFTER FIELD motanl
   IF LENGTH(w_mae_trc.motanl)<=0 THEN 
      CALL fgl_winmessage(
      "Atencion","Motivo de anulacion invalido. VERIFICA.","stop") 
      NEXT FIELD motanl
   END IF
  ON ACTION accept 
   IF LENGTH(w_mae_trc.motanl)<=0 THEN 
      CALL fgl_winmessage(
      "Atencion","Motivo de anulacion invalido. VERIFICA.","stop") 
      NEXT FIELD motanl
   END IF 

   EXIT INPUT 
  ON ACTION cancel
   LET anula = FALSE 
   EXIT INPUT 
 END INPUT
 IF NOT anula THEN
    RETURN
 END IF 

 -- Confirmando anulacion 
 IF NOT librut001_yesornot("Confirmacion",
                           "Desea Anular el Movimiento de Compra ?",
                           "Si",
                           "No",
                           "question") THEN
    RETURN 
 END IF

 -- Iniciando Transaccion
 BEGIN WORK

  -- Anulando compra 
  UPDATE cmp_mtransac
  SET    cmp_mtransac.estado = 0,
         cmp_mtransac.motanl = w_mae_trc.motanl, 
         cmp_mtransac.fecanl = CURRENT,
         cmp_mtransac.horanl = CURRENT HOUR TO SECOND, 
         cmp_mtransac.usranl = USER 
  WHERE (cmp_mtransac.lnkcmp = w_mae_trc.lnkcmp)

 -- Finalizando Transaccion
 COMMIT WORK

 CALL fgl_winmessage(
 "Atencion",
 "Movimiento de compra # "||w_mae_trc.lnkcmp||" anulado.",
 "information")
END FUNCTION

-- Subrutina para eliminar el movimiento de compra 

FUNCTION cmpqbx001_EliminarCompra()
 DEFINE regreso SMALLINT 

 --Verificando si movimiento es del mismo dia 
 IF (w_mae_trc.fecemi>=(TODAY-w_mae_par.diaeli)) THEN  
    -- Confirmando eliminacion 
    IF NOT librut001_yesornot("Confirmacion",
                              "Desea Eliminar el Movimiento de Compra ?",
                              "Si",
                              "No",
                              "question") THEN
       RETURN FALSE 
    END IF

    -- Iniciando Transaccion
    BEGIN WORK

     -- Compras 
     DELETE FROM cmp_mtransac
     WHERE cmp_mtransac.lnkcmp = w_mae_trc.lnkcmp

     -- Contabilidad 
     DELETE FROM ctb_mtransac 
     WHERE ctb_mtransac.lnktra = w_mae_trc.lnkctb  

    -- Finalizando Transaccion
    COMMIT WORK
   
    CALL fgl_winmessage(
    "Atencion",
    "Movimiento de Compra "||w_mae_trc.lnkcmp||" eliminado.",
    "information")
 ELSE
    CALL fgl_winmessage(
    "Atencion",
    "Movimiento de compra no puede eliminarse.\n"||
    "Fecha de documento excede limite de dias. ("||w_mae_par.diaeli||")", 
    "stop")
    RETURN FALSE 
 END IF 

 RETURN TRUE 
END FUNCTION 

-- Subrutina para desplegar el detalle de la poliza contable 

FUNCTION cmpqbx001_PolizaContable() 
 -- Selecionando datos de la poliza 
 CALL v_partida.clear() 
 DECLARE cpoliza CURSOR FOR 
 SELECT x.numcta, 
        y.nomcta,
        x.totdeb,
        x.tothab,
        x.concep,
        "", 
        x.correl
  FROM  cmp_dtpoliza x,ctb_mcuentas y 
  WHERE x.lnkcmp = w_mae_trc.lnkcmp
    AND y.codemp = w_mae_trc.codemp 
    AND y.numcta = x.numcta
  ORDER BY x.correl 

 LET totlin = 1 
 FOREACH cpoliza INTO v_partida[totlin].*
  LET totlin=totlin+1 
 END FOREACH
 CLOSE cpoliza
 FREE cpoliza
 LET totlin=totlin-1 

 -- Desplegando poliza 
 DISPLAY ARRAY v_partida TO s_partida.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY 

 -- Desplegando totales 
 CALL cmping001_TotalCuentasPoliza()
END FUNCTION 
