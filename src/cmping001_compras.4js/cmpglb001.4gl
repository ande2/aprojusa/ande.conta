{ 
Fecha    : Julio 2014 
Programo : Mynor Ramirez 
Objetivo : Programa de variables globales compras a proveedores 
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname             = "cmping001"
CONSTANT TIPOTRNPOLIZAEGRESO  = 103 
DEFINE w_mae_emp   RECORD LIKE glb_empresas.*,
       w_mae_par   RECORD LIKE cmp_paramtrs.*, 
       w_mae_trc   RECORD LIKE cmp_mtransac.*, 
       w_mae_pro   RECORD LIKE glb_sociosng.*,
       w_tip_mov   RECORD LIKE cmp_tipomovs.*,
       v_costos    DYNAMIC ARRAY OF RECORD
        codcos     LIKE cmp_dtransac.codcos,
        totval     LIKE cmp_dtransac.totval,
        rellen     CHAR(1) 
       END RECORD, 
       username    LIKE glb_permxusr.userid,
       mod_diacre  LIKE cmp_mtransac.diacre,
       b           ui.ComboBox,
       cba         ui.ComboBox,
       totcos      SMALLINT, 
       valcos      DEC(14,2), 
       w           ui.Window,
       f           ui.Form, 
       v_partida   DYNAMIC ARRAY OF RECORD
        cnumcta    LIKE ctb_dtransac.numcta,
        cnomcta    LIKE ctb_mcuentas.nomcta,
        ctotdeb    LIKE ctb_dtransac.totdeb,
        ctothab    LIKE ctb_dtransac.tothab,
        cconcpt    LIKE ctb_dtransac.concep,
        crellen    CHAR(1)
       END RECORD,
       gcodemp     LIKE cmp_mtransac.codemp,
       totaldeb    DEC(14,2),
       totalhab    DEC(14,2),
       totlin      INTEGER, 
       codigoapl   CHAR(3)
END GLOBALS
