{
Mantenimiento de departamentos
glbmae019.4gl 
}

-- Definicion de variables globales 

GLOBALS "glbglo019.4gl"

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("departamentos")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo nombre del usuario 
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de principal 
 CALL glbmae019_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION glbmae019_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255), 
        savedata SMALLINT,
        existe   SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "glbmae019a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais

  CALL librut001_header("glbmae019",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Departamentos"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(progname,4,username) THEN 
       HIDE OPTION "Buscar"
    END IF
    --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(progname,2,username) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(progname,3,username) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de departamentos."
    CALL glbqbe019_departamentos(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo departamento."
    LET savedata = glbmae019_departamentos(1) 
   COMMAND "Modificar"
    " Modificacion de un departamento existente."
    CALL glbqbe019_departamentos(2) 
   COMMAND "Borrar"
    " Eliminacion de un departamento existente."
    CALL glbqbe019_departamentos(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION glbmae019_departamentos(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL glbqbe019_EstadoMenu(4,"")
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL glbmae019_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nomdep,
                w_mae_pro.estado
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   AFTER FIELD nomdep  
    --Verificando nombre del departamento
    IF (LENGTH(w_mae_pro.nomdep)=0) THEN
       ERROR "Error: nombre del departamento invalida, VERIFICA."
       LET w_mae_pro.nomdep = NULL
       NEXT FIELD nomdep  
    END IF

    -- Verificando que no exista otro departamento con el mismo nombre
    SELECT UNIQUE (a.coddep)
     FROM  glb_departos a
     WHERE (a.coddep != w_mae_pro.coddep) 
       AND (a.nomdep  = w_mae_pro.nomdep) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otro departamento con el mismo nombre, VERIFICA.",
        "information")
        NEXT FIELD nomdep
     END IF 

   AFTER FIELD estado
    --Verificando estado
    IF w_mae_pro.estado IS NULL THEN 
       NEXT FIELD estado
    END IF

   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.nomdep IS NULL THEN 
       NEXT FIELD nomdep
    END IF
    IF w_mae_pro.estado IS NULL THEN 
       NEXT FIELD estado
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL glbmae019_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando 
    CALL glbmae019_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL glbqbe019_EstadoMenu(0,"")
    CALL glbmae019_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un departamento

FUNCTION glbmae019_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando departamento ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.coddep),0)
    INTO  w_mae_pro.coddep
    FROM  glb_departos a
    IF (w_mae_pro.coddep IS NULL) THEN
       LET w_mae_pro.coddep = 1
    ELSE 
       LET w_mae_pro.coddep = (w_mae_pro.coddep+1)
    END IF 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO glb_departos   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.coddep 

   --Asignando el mensaje 
   LET msg = "Departamento (",w_mae_pro.coddep USING "<<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE glb_departos
   SET    glb_departos.*      = w_mae_pro.*
   WHERE  glb_departos.coddep = w_mae_pro.coddep 

   --Asignando el mensaje 
   LET msg = "Departamento (",w_mae_pro.coddep USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando departamentos
   DELETE FROM glb_departos 
   WHERE (glb_departos.coddep = w_mae_pro.coddep)

   --Asignando el mensaje 
   LET msg = "Departamento (",w_mae_pro.coddep USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL glbmae019_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION glbmae019_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.coddep = 0 
   LET w_mae_pro.estado = 1 
   LET w_mae_pro.userid = username 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.coddep,w_mae_pro.estado 
 DISPLAY BY NAME w_mae_pro.coddep,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION
