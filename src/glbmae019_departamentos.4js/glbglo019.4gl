{ 
glbglo019.4gl
Mynor Ramirez
Mantenimiento de departamentos 
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "glbmae019"
DEFINE w_mae_pro      RECORD LIKE glb_departos.*,
       v_departamentos DYNAMIC ARRAY OF RECORD
        tcoddep       LIKE glb_departos.coddep,
        tnomdep       LIKE glb_departos.nomdep, 
        tendrec       CHAR(1) 
       END RECORD,
       username       VARCHAR(15)
END GLOBALS
