{ 
Mynor Ramirez 
Reporte de integracion de gastos x centro de costo
}

DATABASE erpjuridico 

--  Definicion de variables globales 

CONSTANT progname = "gasrep001" 
TYPE     datosreporte    RECORD 
          codemp         LIKE bco_mtransac.codemp, 
          nomemp         CHAR(50),
          codrub         LIKE bco_mtransac.codrub,
          nomrub         CHAR(30), 
          totpag         DEC(14,2) 
         END RECORD
DEFINE   w_datos         RECORD
          codemp         LIKE bco_mtransac.codemp,
          fecini         DATE,
          fecfin         DATE
         END RECORD
DEFINE   v_centros       DYNAMIC ARRAY OF RECORD
          codcos         LIKE glb_cencosto.codcos, 
          nomcen         CHAR(14) 
         END RECORD
DEFINE   p               RECORD
          length         SMALLINT,
          topmg          SMALLINT,
          botmg          SMALLINT,
          lefmg          SMALLINT,
          rigmg          SMALLINT
         END RECORD
DEFINE   totcol          ARRAY[200] OF DECIMAL(14,2) 
DEFINE   totallin        DEC(14,2)
DEFINE   totalcol        DEC(14,2)
DEFINE   existe          SMALLINT
DEFINE   totcen          SMALLINT
DEFINE   s,d             CHAR(1)
DEFINE   titulo1         STRING
DEFINE   tituloreporte   STRING
DEFINE   filename        STRING
DEFINE   pipeline        STRING

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarReportes1") 

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("rintegragasxcen")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL gasrep001_IntegracionGastos()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION gasrep001_IntegracionGastos()
 DEFINE imp1              datosreporte, 
        wpais             VARCHAR(255),
        qrytxt,hilera     STRING,
        loop              SMALLINT,
        w                 ui.Window 

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep004a AT 5,2
  WITH FORM "gasrep001a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/IntegracionGastos.spl" 

  -- Llenando combobox de empresas   
  CALL librut003_CbxEmpresas() 

  -- Inicio del loop
  OPTIONS INPUT WRAP 
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
   LET w_datos.codemp = 1 
   LET s = 1 SPACES
   LET d = "~" 
   CLEAR FORM

   -- Construyendo busqueda
   INPUT BY NAME w_datos.codemp,
                 w_datos.fecini, 
                 w_datos.fecfin 
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 
     LET p.length = 57  
     LET p.topmg  = 3
     LET p.botmg  = 2
     LET p.lefmg  = 2
     LET p.rigmg  = 2

     -- Verificando si filtros estan completos
     IF NOT gasrep001_FiltrosCompletos() THEN
        NEXT FIELD codemp 
     END IF
     EXIT INPUT

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf2" 
     LET p.length = 57  
     LET p.topmg  = 0
     LET p.botmg  = 0
     LET p.lefmg  = 0
     LET p.rigmg  = 0

     -- Verificando si filtros estan completos
     IF NOT gasrep001_FiltrosCompletos() THEN
        NEXT FIELD codemp 
     END IF
     EXIT INPUT

    ON ACTION excel 
     -- Asignando dispositivo 
     LET pipeline = "excel" 
     LET p.length = 57  
     LET p.topmg  = 0
     LET p.botmg  = 0
     LET p.lefmg  = 0
     LET p.rigmg  = 0
     LET s        = ASCII(9) 
     LET d        = ASCII(9) 

     -- Verificando si filtros estan completos
     IF NOT gasrep001_FiltrosCompletos() THEN
        NEXT FIELD codemp 
     END IF
     EXIT INPUT
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Cargando centros de costo
   CALL v_centros.clear() 
   LET qrytxt = 
    "SELECT UNIQUE a.codcos,a.nomcen ",
     "FROM  vis_integragasxcen a ",
     "WHERE a.codemp = ",w_datos.codemp,
     "  AND a.fecemi >= '",w_datos.fecini,"' ", 
     "  AND a.fecemi <= '",w_datos.fecfin,"' ", 
     " ORDER BY 1"

    PREPARE p_cencosto FROM qrytxt 
    DECLARE c_cencosto CURSOR FOR p_cencosto 
    LET totcen  = 1
    LET titulo1 = NULL
    FOREACH c_cencosto INTO v_centros[totcen].*
     -- Creando encabezados
     LET hilera = librut001_JustificaDerecha(v_centros[totcen].nomcen,14)
     LET v_centros[totcen].nomcen = hilera 
     LET titulo1 = titulo1.trimright(),v_centros[totcen].nomcen,d
     LET totcen = totcen+1 
    END FOREACH
    CLOSE c_cencosto
    FREE  c_cencosto
    LET totcen = totcen-1

    -- Adicionando totales   
    LET titulo1 = titulo1.trimright(),"  T O T A L  ",s
    LET titulo1 = librut001_replace(titulo1,"~"," ",100)

   -- Construyendo seleccion 
   LET qrytxt = "SELECT UNIQUE a.codemp,a.nomemp,a.codrub,a.nomrub,0 ", 
                  "FROM  vis_integragasxcen a ",
                  "WHERE a.codemp = ",w_datos.codemp,
                  "  AND a.fecemi >= '",w_datos.fecini,"' ", 
                  "  AND a.fecemi <= '",w_datos.fecfin,"' ", 
                  " ORDER BY 4" 

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep004 FROM qrytxt 
   DECLARE c_crep004 SCROLL CURSOR WITH HOLD FOR c_rep004
   LET existe = FALSE
   FOREACH c_crep004 INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       LET existe = TRUE
       START REPORT gasrep001_GeneraReporte TO filename
    END IF 

    -- Llenando el reporte
    OUTPUT TO REPORT gasrep001_GeneraReporte(imp1.*)
   END FOREACH
   CLOSE c_crep004 
   FREE  c_crep004 

   IF existe THEN
    -- Finalizando el reporte
    FINISH REPORT gasrep001_GeneraReporte   

    -- Transfiriendo reporte a excel 
    IF pipeline = "excel" THEN 
      CALL librut005_excel(filename) 
    ELSE 
      -- Enviando reporte al destino seleccionado
      CALL librut001_sendreport
      (filename,pipeline,tituloreporte,
       "--noline-numbers "||
       "--nofooter "||
       "--font-size 7 "||
       "--page-width 1190 --page-height 595  "||
       "--left-margin 35 --right-margin 15 "||
       "--top-margin 30 --bottom-margin 35 "||
       "--title Gastos")
    END IF 

    ERROR "" 
    CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
   ELSE
    ERROR "" 
    CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop")
   END IF 
  END WHILE
  OPTIONS INPUT NO WRAP 
 CLOSE WINDOW wrep004a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION gasrep001_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE
 IF w_datos.codemp IS NULL OR
    w_datos.fecini IS NULL OR
    w_datos.fecfin IS NULL THEN
    CALL fgl_winmessage(
    " Atencion",
    " Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte 

REPORT gasrep001_GeneraReporte(imp1)
 DEFINE imp1      datosreporte,
        linea     STRING, 
        hilera    CHAR(15),
        col,i     SMALLINT,
        lfn,lg    SMALLINT,
        qrytxt    STRING,
        periodo   STRING 

  OUTPUT LEFT MARGIN   p.lefmg
         TOP MARGIN    p.topmg
         BOTTOM MARGIN p.botmg
         PAGE LENGTH   p.length

  FORMAT 
   PAGE HEADER
    -- Definiendo linea 
    LET hilera = "_______________"
    LET linea  = "_______________________________"
    LET lg     = (30+(totcen*15)+15)
    LET lfn    = (lg-20) 

    -- Creando linea 
    FOR i = 1 TO totcen+1 
     LET linea = linea,hilera CLIPPED 
    END FOR

    -- Periodo de fechas
    LET periodo = "PERIODO DEL ",w_datos.fecini," AL ",w_datos.fecfin

    -- Imprimiendo Encabezado
    LET col = librut001_centrado(tituloreporte,lg) 
    PRINT COLUMN   1,"Gastos",
          COLUMN col,UPSHIFT(tituloreporte) CLIPPED,  
	  COLUMN lfn,PAGENO USING "Pagina: <<<<"

    LET col = librut001_centrado(periodo,lg) 
    PRINT COLUMN   1,"gasrep001",
          COLUMN col,periodo CLIPPED, 
          COLUMN lfn,"Fecha : ",TODAY USING "dd/mmm/yyyy" 

    LET col = librut001_centrado(imp1.nomemp,lg) 
    PRINT COLUMN   1,FGL_GETENV("LOGNAME") CLIPPED,
          COLUMN col,imp1.nomemp CLIPPED,
          COLUMN lfn,"Hora  : ",TIME 
    PRINT linea 
   
    -- Verificando si tipo de reporte no es a EXCEL
    IF (pipeline!="excel") THEN 
       PRINT "Centro de Costo               ",s,titulo1.trimright()
       PRINT "Rubro de Gasto                "
    ELSE
       SKIP 2 LINES 
    END IF 

    PRINT linea

   BEFORE GROUP OF imp1.codemp
    -- Verificando si tipo de reporte es a EXCEL
    IF (pipeline="excel") THEN 
       -- Imprimiendo datos de la bodega
       PRINT UPSHIFT(tituloreporte)          CLIPPED,s
       PRINT "PERIODO DEL ",w_datos.fecini," AL ",w_datos.fecfin,s 
       PRINT "EMPRESA:",imp1.nomemp          CLIPPED,s
       PRINT s
       PRINT "Centro de Costo",s,titulo1.trimright()
       PRINT "Rubro de Gasto",s
       PRINT s 
    END IF 

    -- Inicializando totales
    FOR i = 1 TO 100   
     LET totcol[i] = 0
    END FOR 

   ON EVERY ROW
    -- Imprimiendo pesaje 
    PRINT imp1.nomrub,s; 

    -- Imprimiendo detalle x destino 
    LET totallin = 0 
    FOR i = 1 TO totcen
     -- Totalizando por empresa, fecha, centro de costo y rubro de gasto 
     LET qrytxt = "SELECT NVL(SUM(a.totpag),0) ",
                   "FROM  vis_integragasxcen a ",
                   "WHERE a.codemp = ",imp1.codemp,
                    " AND a.codcos = ",v_centros[i].codcos,
                    " AND a.codrub = ",imp1.codrub, 
                    " AND a.fecemi >= '",w_datos.fecini,"' ", 
                    " AND a.fecemi <= '",w_datos.fecfin,"' " 

     PREPARE cmovtos FROM qrytxt 
     DECLARE cintmov CURSOR FOR cmovtos
     FOREACH cintmov INTO imp1.totpag 
     END FOREACH
     
     -- Imprimiendo 
     PRINT imp1.totpag USING "----------&.&&",s;
     
     -- Totalizando
     LET totallin  = totallin+imp1.totpag 
     LET totcol[i] = totcol[i]+imp1.totpag 
    END FOR 

    -- Imprimiendo totales x linea
    PRINT totallin     USING "----------&.&&",s

   AFTER GROUP OF imp1.codemp
    PRINT "TOTAL RUBROS --->",13 SPACES,s; 

    LET totalcol = 0 
    -- Totalizando columnas 
    FOR i = 1 TO totcen
     -- Imprimiendo 
     PRINT totcol[i]   USING "----------&.&&",s;

     -- Totalizando
     LET totalcol = totalcol+totcol[i] 
    END FOR 

    -- Imprimiendo totales x linea 
    PRINT totalcol     USING "----------&.&&",s
END REPORT 
