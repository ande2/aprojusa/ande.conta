drop view vis_integragasxcen;
create view vis_integragasxcen
(tipsol,codemp,nomemp,codcos,nomcen,codrub,nomrub,fecemi,totpag,totval)
as
select 1,
       x.codemp,
       e.nomemp,
       x.codcos,
       c.nomabr,
       x.codrub,
       r.nomrub,
       x.feccob,
       x.totdoc,
       x.totdoc
from bco_mtransac x,glb_cencosto c,fac_rubgasto r,
     glb_empresas e,bco_tipomovs t
where x.lnkbco is not null
  and x.estado = 1
  --and x.lnkcaj = 0 
  and t.tipmov = x.tipmov
  and t.tipope = 0
  and c.codcos = x.codcos
  and r.codrub = x.codrub
  and e.codemp = x.codemp;

grant select on vis_integragasxcen to public;
