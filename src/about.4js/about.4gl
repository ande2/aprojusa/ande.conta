{
Programo: Mynor Ramirez
Objetivo: Acerca de los derechos del sistema 
} 

DATABASE erpjuridico 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos
 CALL ui.Interface.loadStyles("styles")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("about")
    CALL ui.Interface.setType("child")
 END IF

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Abriendo la ventana
 OPEN WINDOW about WITH FORM "about"
  ATTRIBUTE(TEXT="Acerca de Integra")

  -- Desplegando datos 
  MENU ""
   ON ACTION regresar
    EXIT MENU
  END MENU

 -- Cerrando ventana
 CLOSE WINDOW about 
END MAIN 
