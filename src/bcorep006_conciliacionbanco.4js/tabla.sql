create table "sistemas".bco_tmconcil 
  (
    codcil smallint not null ,
    tipmov smallint not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (codcil,tipmov) constraint pkbcotmconcil
  ) extent size 16 next size 16 lock mode row;

alter table "sistemas".bco_tmconcil add constraint (foreign key 
    (tipmov) references "sistemas".bco_tipomovs  constraint "sistemas"
    .fkbcotipomovs3);


