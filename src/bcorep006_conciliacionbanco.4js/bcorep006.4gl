{ 
Programa : bcorep006.4gl                   
Programo : Mynor Ramirez 
Objetivo : Reporte de estado de cuenta bancario 
}

DATABASE erpjuridico 

-- Definicion de variables globales 

CONSTANT progname = "bcorep006" 
CONSTANT Moneda   = "Q." 
TYPE    datosreporte        RECORD 
         codemp             LIKE bco_mtransac.codemp, 
         nomemp             CHAR(50), 
         numcta             LIKE bco_mtransac.numcta,
         nomcta             LIKE bco_mcuentas.nomcta,
         nombco             CHAR(50),
         salant             DEC(14,2) 
        END RECORD 
DEFINE  datos               RECORD 
         codemp             LIKE bco_mtransac.codemp, 
         numcta             LIKE bco_mtransac.numcta,
         fecini             DATE,
         fecfin             DATE
        END RECORD
DEFINE  p                   RECORD 
         length             SMALLINT,
         topmg              SMALLINT, 
         botmg              SMALLINT,
         lefmg              SMALLINT, 
         rigmg              SMALLINT 
        END RECORD 
DEFINE  gcodemp             LIKE bco_mtransac.codemp
DEFINE  gnumcta             LIKE bco_mtransac.numcta 
DEFINE  existe              SMALLINT
DEFINE  tituloreporte       STRING 
DEFINE  filename            STRING
DEFINE  pipeline            STRING
DEFINE  s,d,apostrofe       CHAR(1) 
DEFINE  np                  CHAR(3) 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("ToolBarReportes1")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("restadobancario")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL bcorep006_EstadoBancario() 
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION bcorep006_EstadoBancario()
 DEFINE w_mae_cta RECORD LIKE bco_mcuentas.*,
        myHandler om.SaxDocumentHandler, 
        imp1      datosreporte,
        wpais     VARCHAR(255),
        qrytxt    STRING,
        qrypart   STRING,
        loop      SMALLINT,
        w         ui.Window, 
        f         ui.Form   

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep005a AT 5,2
  WITH FORM "bcorep006a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header(progname,wpais,1)

  -- Obteniendo el titulo de la ventana
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  LET tituloreporte = w.getText()

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/EstadoBancario.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Creando tabla temporal
  CREATE TEMP TABLE tmp_movtos 
  (tipope SMALLINT,
   tipmov SMALLINT,
   totdoc DEC(14,2)) WITH NO LOG 

  -- Cargando combobox de empresas
  LET gcodemp = librut003_DCbxEmpresaEnUsoXUsuario()
  IF gcodemp IS NULL THEN
     CALL fgl_winmessage(
     "Atencion","Usuario sin empresa asignada, VERIFICA.","stop")
     CLOSE WINDOW wrep002a
     RETURN
  END IF

  -- Cargando cuentas
  LET gnumcta = librut003_DCbxCuentasBanco(gcodemp)
  IF gnumcta IS NULL THEN
     CALL fgl_winmessage(
     "Atencion","Empresa sin cuentas de banco asignadas, VERIFICA.","stop")
     CLOSE WINDOW wrep002a
     RETURN
  END IF

  -- Inicio del loop
  OPTIONS INPUT WRAP 
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE pipeline TO NULL
   LET s = 1 SPACES
   LET apostrofe = NULL 
   CLEAR FORM
   DELETE FROM tmp_movtos 
   DISPLAY gcodemp TO codemp 

   -- Ingresando datos 
   DIALOG ATTRIBUTES(UNBUFFERED,FIELD ORDER FORM)

    -- Construyendo busqueda
    INPUT BY NAME datos.numcta,
                  datos.fecini,
                  datos.fecfin
    END INPUT 

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT DIALOG

    ON ACTION excel 
     -- Asignando dispositivo 
     LET pipeline  = "excel" 
     LET apostrofe = "'"
     LET s = ASCII(9) 
     LET p.length = 55 
     LET p.topmg  = 0
     LET p.botmg  = 0
     LET p.lefmg  = 0
     LET p.rigmg  = 0

     -- Verificando ingreso de filtros
     IF NOT bcorep006_FiltrosCompletos() THEN 
        NEXT FIELD numcta 
     END IF 
     EXIT DIALOG

   END DIALOG 
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Obeniendo datos de la cuenta 
   INITIALIZE w_mae_cta.* TO NULL
   CALL librut003_BCuentaBanco(datos.numcta)
   RETURNING w_mae_cta.*,existe
   LET datos.codemp = w_mae_cta.codemp 

   -- Construyendo seleccion 
   LET qrytxt = "SELECT c.codemp,e.nomemp,c.numcta,c.nomcta,b.nombco,0 ",
                 "FROM  bco_mcuentas c,glb_empresas e,glb_mtbancos b ",
                 "WHERE c.codemp = ",datos.codemp,
                 "  AND c.numcta = '",datos.numcta,"'"

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep005 FROM qrytxt 
   DECLARE c_crep005 CURSOR FOR c_rep005
   LET existe = FALSE
   FOREACH c_crep005 INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       LET existe = TRUE
       START REPORT bcorep006_GenerarReporte TO filename 
    END IF 

    -- Llenando el reporte
    OUTPUT TO REPORT bcorep006_GenerarReporte(imp1.*)
   END FOREACH
   CLOSE c_crep005 
   FREE  c_crep005 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT bcorep006_GenerarReporte 

      -- Transfiriendo reporte a excel
      IF pipeline = "excel" THEN
         CALL bcorep006_excel(filename)
      ELSE
       -- Enviando reporte al destino seleccionado
       CALL librut001_sendreport
       (filename,pipeline,tituloreporte, 
        "--noline-numbers "||
        "--nofooter "||
        "--font-size 7 "||
        "--page-width 842 --page-height 595 "||
        "--left-margin 35 --right-margin 15 "||
        "--top-margin 30 --bottom-margin 35 "||
        "--title Bancos")
      END IF

      ERROR "" 
      CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(
      " Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 
  END WHILE

  -- Dropeando tabla temporal
  DROP TABLE tmp_movtos 

  OPTIONS INPUT NO WRAP 

 CLOSE WINDOW wrep005a   
END FUNCTION 

-- Subrutina para verificar si los filtros estan completos para emitir el reporte

FUNCTION bcorep006_FiltrosCompletos()
 DEFINE completos SMALLINT

 -- Verificando filtros
 LET completos = TRUE

 -- Verificando datos 
 IF datos.numcta IS NULL OR 
    datos.fecini IS NULL OR 
    datos.fecfin IS NULL THEN 
    CALL fgl_winmessage(
    " Atencion",
    " Deben de completarse los filtros de seleccion para emitir el reporte.",
    "stop")
    LET completos = FALSE
 END IF

 RETURN completos
END FUNCTION

-- Subrutina para generar el reporte 

REPORT bcorep006_GenerarReporte(imp1)
 DEFINE imp1              datosreporte,
        imp2              RECORD
         totdep           DEC(12,2),
         totcci           DEC(12,2),
         totsal           DEC(12,2),
         acrevi           DEC(14,2),
         depvar           DEC(14,2),
         acrbco           DEC(14,2),
         chques           DEC(14,2), 
         nodacr           DEC(14,2),
         pagvir           DEC(14,2), 
         poling           DEC(14,2), 
         polegr           DEC(14,2), 
         totmov           DEC(14,2), 
         totdeb           DEC(14,2), 
         tothab           DEC(14,2), 
         totdif           DEC(14,2) 
        END RECORD,
        wcheques          RECORD
         feccob           LIKE bco_mtransac.feccob,
         numdoc           LIKE bco_mtransac.numdoc,
         nomchq           LIKE bco_mtransac.nomchq,
         totdoc           LIKE bco_mtransac.totdoc  
        END RECORD,
        w_cargou          DEC(14,2),
        w_cartou,w_abonou DEC(12,2),
        w_abotou          DEC(14,2),
        linea             CHAR(306),
        col,i,nmovs,lg    INTEGER,
        espacios          CHAR(4), 
        fechareporte      STRING

  OUTPUT LEFT MARGIN   p.lefmg
         TOP MARGIN    p.topmg 
         BOTTOM MARGIN p.botmg 
         PAGE LENGTH   p.length

  FORMAT 

  BEFORE GROUP OF imp1.numcta 
   SKIP TO TOP OF PAGE

   -- Llenando linea
   LET linea = NULL
   LET lg = 170 
   FOR i = 1 TO lg
    LET linea = linea CLIPPED,"-"
   END FOR  

   -- Imprimiendo datos
   PRINT imp1.nomemp CLIPPED,s
   PRINT "NOMBRE DE CUENTA:",s,imp1.nomcta,s
   PRINT "BANCO:",s,imp1.nombco CLIPPED,s
   PRINT "NUMERO DE CUENTA:",s,"'",imp1.numcta CLIPPED,s
   PRINT "CONCILIACION CONTABLE DE SALDO AL",datos.fecfin,s 

   -- Inicializando datos 
   LET imp2.totdep = 0
   LET imp2.totcci = 0 
   LET imp2.totsal = 0 
   LET imp2.acrevi = 0
   LET imp2.depvar = 0 
   LET imp2.acrbco = 0 
   LET imp2.chques = 0
   LET imp2.nodacr = 0
   LET imp2.pagvir = 0
   LET imp2.poling = 0
   LET imp2.polegr = 0
   LET imp2.totdeb = 0
   LET imp2.tothab = 0
   LET imp2.totmov = 0
   LET imp2.totdif = 0 

   -- Obteniendo saldo anterior 
   LET imp1.salant = librut003_SaldoAnteriorCuentaBanco(imp1.numcta,datos.fecini)

   -- Caculando Saldo Anterior 
   PRINT s,s,"Saldos del Banco",s,imp1.salant USING "---,---,--&.&&"
   PRINT s 
   PRINT "DEPOSITOS EN TRANSITO",s 
   PRINT "Fecha",s,
         "No. Documento",s,
         "Nombre",s,
         "Monto",s 
   PRINT s,s,"SubTotal",s,imp2.totdep USING "---,---,--&.&&" 
   PRINT s
   PRINT "CHEQUES EN CIRCULACION",s 
   PRINT "Fecha",s,
         "No. Documento",s,
         "Nombre",s,
         "Monto",s 

   -- Seleccionando cheques en circulacion
   INITIALIZE wcheques.* TO NULL 
   DECLARE ccir CURSOR FOR
   SELECT a.*
    FROM  vis_concilbanco a
    WHERE a.codemp  = datos.codemp
      AND a.numcta  = datos.numcta
      AND a.fecemi >= datos.fecini
      AND a.fecemi <= datos.fecfin
      AND a.feccob  > datos.fecfin 
      AND a.pagpro  = 1 
      AND a.estado  = 1 

    FOREACH ccir INTO wcheques.*
     PRINT wcheques.feccob,s,
           wcheques.numdoc,s,
           wcheques.nomchq,s,
           wcheques.totdoc USING "---,---,--&.&&"
     LET imp2.totcci = imp2.totcci+wcheques.totdoc 
    END FOREACH
    CLOSE ccir
    FREE  ccir
   PRINT s,s,"SubTotal",s,imp2.totcci USING "---,---,--&.&&" 
   PRINT s,s,"Saldo Contable",s,imp2.totsal USING "---,---,--&.&&" 

   -- Totalizando movimientos
   PRINT s 
   PRINT "CONCEPTO",s,
         "DEBE",s,
         "HABER",s,
         "SALDO",s 
   PRINT "Saldo al",s

   -- Totalizando acreditamientos virtuales
   SELECT NVL(SUM(a.totdoc),0)
    INTO  imp2.acrevi 
    FROM  vis_concilbanco a
    WHERE a.codemp  = datos.codemp
      AND a.numcta  = datos.numcta
      AND a.feccob >= datos.fecini
      AND a.feccob <= datos.fecfin
      AND EXISTS (SELECT y.tipmov FROM bco_tmconcil y
                   WHERE y.codcil = 1
                   AND y.tipmov = a.tipmov) 
      AND a.estado  = 1 
   LET imp2.totmov = imp2.totmov+imp2.acrevi 
   LET imp2.totdeb = imp2.totdeb+imp2.acrevi 
   PRINT "(+) Acreditamientos Virtuales",s,
         imp2.acrevi USING "---,---,--&.&&",s,s,imp2.totmov USING "---,---,--&.&&"

   -- Totalizando depositos varios
   SELECT NVL(SUM(a.totdoc),0)
    INTO  imp2.depvar 
    FROM  vis_concilbanco a
    WHERE a.codemp  = datos.codemp
      AND a.numcta  = datos.numcta
      AND a.feccob >= datos.fecini
      AND a.feccob <= datos.fecfin
      AND EXISTS (SELECT y.tipmov FROM bco_tmconcil y
                   WHERE y.codcil = 2
                     AND y.tipmov = a.tipmov) 
      AND a.estado  = 1 
   LET imp2.totmov = imp2.totmov+imp2.depvar 
   LET imp2.totdeb = imp2.totdeb+imp2.depvar 
   PRINT "(+) Depositos Varios",s,
         imp2.depvar USING "---,---,--&.&&",s,s,imp2.totmov USING "---,---,--&.&&" 

   -- Totalizando acreditamientos bancarios
   SELECT NVL(SUM(a.totdoc),0)
    INTO  imp2.acrbco 
    FROM  vis_concilbanco a
    WHERE a.codemp  = datos.codemp
      AND a.numcta  = datos.numcta
      AND a.feccob >= datos.fecini
      AND a.feccob <= datos.fecfin
      AND EXISTS (SELECT y.tipmov FROM bco_tmconcil y
                   WHERE y.codcil = 3
                     AND y.tipmov = a.tipmov) 
      AND a.estado  = 1 
   LET imp2.totmov = imp2.totmov+imp2.acrbco 
   LET imp2.totdeb = imp2.totdeb+imp2.acrbco 
   PRINT "(+) Acreditamientos Bancarios",s,
         imp2.acrbco USING "---,---,--&.&&",s,s,imp2.totmov USING "---,---,--&.&&" 

   -- Totalizando poliza de ingresos 
   SELECT NVL(SUM(a.totdoc),0)
    INTO  imp2.poling 
    FROM  vis_concilbanco a
    WHERE a.codemp  = datos.codemp
      AND a.numcta  = datos.numcta
      AND a.feccob >= datos.fecini
      AND a.feccob <= datos.fecfin
      AND EXISTS (SELECT y.tipmov FROM bco_tmconcil y
                   WHERE y.codcil = 4
                     AND y.tipmov = a.tipmov) 
      AND a.estado  = 1 
   LET imp2.totmov = imp2.totmov+imp2.poling 
   LET imp2.totdeb = imp2.totdeb+imp2.poling 
   PRINT "(+) Poliza de Ingresos",s,
       imp2.poling USING "---,---,--&.&&",s,s,imp2.totmov USING "---,---,--&.&&"

   -- Totalizando cheques 
   SELECT NVL(SUM(a.totdoc),0)
    INTO  imp2.chques 
    FROM  vis_concilbanco a
    WHERE a.codemp  = datos.codemp
      AND a.numcta  = datos.numcta
      AND a.feccob >= datos.fecini
      AND a.feccob <= datos.fecfin
      AND EXISTS (SELECT y.tipmov FROM bco_tmconcil y
                   WHERE y.codcil = 5
                     AND y.tipmov = a.tipmov) 
      AND a.estado  = 1 
   LET imp2.totmov = imp2.totmov-imp2.chques 
   LET imp2.tothab = imp2.tothab+imp2.chques 
   PRINT "(-) Cheques",s,s,
         imp2.chques USING "---,---,--&.&&",s,imp2.totmov USING "---,---,--&.&&" 

   -- Totalizando notas debito por acreditamientos 
   SELECT NVL(SUM(a.totdoc),0)
    INTO  imp2.nodacr 
    FROM  vis_concilbanco a
    WHERE a.codemp  = datos.codemp
      AND a.numcta  = datos.numcta
      AND a.feccob >= datos.fecini
      AND a.feccob <= datos.fecfin
      AND EXISTS (SELECT y.tipmov FROM bco_tmconcil y
                   WHERE y.codcil = 6
                     AND y.tipmov = a.tipmov) 
      AND a.estado  = 1 
   LET imp2.totmov = imp2.totmov-imp2.nodacr 
   LET imp2.tothab = imp2.tothab+imp2.nodacr 
   PRINT "(-) N/D por Acreditamientos Bancarios",s,s,
         imp2.nodacr USING "---,---,--&.&&",s,imp2.totmov USING "---,---,--&.&&"

   -- Totalizando pagos virtuales 
   SELECT NVL(SUM(a.totdoc),0)
    INTO  imp2.pagvir 
    FROM  vis_concilbanco a
    WHERE a.codemp  = datos.codemp
      AND a.numcta  = datos.numcta
      AND a.feccob >= datos.fecini
      AND a.feccob <= datos.fecfin
      AND EXISTS (SELECT y.tipmov FROM bco_tmconcil y
                   WHERE y.codcil = 7
                     AND y.tipmov = a.tipmov) 
      AND a.estado  = 1 
   LET imp2.totmov = imp2.totmov-imp2.pagvir 
   LET imp2.tothab = imp2.tothab+imp2.pagvir 
   PRINT "(-) Pagos Virtuales",s,s,
         imp2.pagvir USING "---,---,--&.&&",s,imp2.totmov USING "---,---,--&.&&" 

   -- Totalizando poliza de egresos 
   SELECT NVL(SUM(a.totdoc),0)
    INTO  imp2.polegr 
    FROM  vis_concilbanco a
    WHERE a.codemp  = datos.codemp
      AND a.numcta  = datos.numcta
      AND a.feccob >= datos.fecini
      AND a.feccob <= datos.fecfin
      AND EXISTS (SELECT y.tipmov FROM bco_tmconcil y
                   WHERE y.codcil = 8
                     AND y.tipmov = a.tipmov) 
      AND a.estado  = 1 
   LET imp2.totmov = imp2.totmov-imp2.polegr 
   LET imp2.tothab = imp2.tothab+imp2.polegr 
   PRINT "(-) Poliza de Egresos",s,s,
         imp2.polegr USING "---,---,--&.&&",s,imp2.totmov USING "---,---,--&.&&" 

   -- Calculando totales y diferencias 
   LET imp2.totdif = imp2.totdeb-imp2.tothab 
   PRINT "Suma",s,
         imp2.totdeb USING "---,---,--&.&&",s,imp2.tothab USING "---,---,--&.&&" 

   PRINT s,s,"Diferencia",s,imp2.totdif USING "---,---,--&.&&"   
END REPORT 

-- Subrutina para exportar el reporte a excel usando WinCOM 

FUNCTION bcorep006_excel(filename) 
 DEFINE sb          base.StringBuffer
 DEFINE ch          base.channel
 DEFINE lista       base.StringTokenizer
 DEFINE result      INTEGER
 DEFINE i           INTEGER
 DEFINE xlapp       INTEGER
 DEFINE xlwb        INTEGER
 DEFINE linea       STRING 
 DEFINE lineadep    STRING 
 DEFINE filename    STRING 
 DEFINE columna     STRING 
 DEFINE separador   CHAR(1) 
 DEFINE nuevalinea  CHAR(1) 

 -- Inicializando variables                            
 LET xlapp = -1
 LET xlwb = -1

 -- Creando la instancia de Excel
 CALL ui.Interface.frontCall("WinCOM", "CreateInstance", ['Excel.Application'], [xlapp])
 CALL CheckError(xlapp, xlwb, xlapp)
   
 -- Agregando un libro al documento de excel               
 CALL ui.interface.frontCall("WinCOM", "CallMethod", [xlapp, 'WorkBooks.Add'], [xlwb])
 CALL CheckError(xlapp, xlwb, xlwb)
   
 -- Haciendo visible el libro                 
 CALL excel_set_property(xlapp, xlapp, "Visible","True")
  
 -- Llenando celdas desde el FrontCall 
 -- Construyendo celdas delimitando columnas por TAB (ASCII(9)), 
 -- y filas por CR (ASCII(10)
 -- Copia al clipboard via el Frontcall y luego instruye a excel para pegar el contenido
 -- de la celdas dentro del libro 
 LET sb = base.StringBuffer.create()

 -- Leyendo el archivo que contiene el reporte y copiandolo al clipboard 
 LET ch = base.channel.create()
 call ch.openfile(filename,"r")

 let i = 1 
 let separador  = ascii(9) 
 let nuevalinea = ascii(10)
 while true
  let linea = ch.readLine() 
  if ch.isEof() then exit while end if
  if linea.getindexof(separador,1)>0 then
     CALL sb.append(linea)
     CALL sb.append(nuevalinea)
  end if 
  let i = i+1
 end while
 call ch.close() 

 -- Pegando el contenido de las celdas en el clipboard al libro de excel
 CALL ui.Interface.FrontCall("standard","cbset",sb.toString(),result)
 CALL excel_call_method(xlapp, xlwb, 'ActiveSheet.Paste')
 CALL excel_call_method(xlapp, xlwb, 'Columns("A:Z").EntireColumn.AutoFit')
 CALL excel_set_property(xlapp, xlwb, excel_column("B","Z","NumberFormat"), "0.00")

 -- Liberando informacion del clipboard    
 CALL freeMemory(xlapp, xlwb)
END FUNCTION
