{ 
ctbglo006.4gl
Mynor Ramirez
Mantenimiento de tipos de transaccion 
}

DATABASE erpjuridico 

{ Definicion de variables globale }

GLOBALS
CONSTANT progname = "ctbmae006" 
DEFINE w_mae_pro   RECORD LIKE ctb_tipostrn.*,
       v_tipotrns  DYNAMIC ARRAY OF RECORD
        ttiptrn    LIKE ctb_tipostrn.tiptrn,
        tnomtrn    LIKE ctb_tipostrn.nomtrn, 
        ttipope    CHAR(20)
       END RECORD, 
       v_permxusr  DYNAMIC ARRAY OF RECORD
        tcheckb    SMALLINT,
        tuserid    LIKE ctb_permxtrn.userid,
        tnomusr    VARCHAR(50), 
        tusuaid    LIKE ctb_permxtrn.usuaid,
        tfecsis    LIKE ctb_permxtrn.fecsis,
        thorsis    LIKE ctb_permxtrn.horsis, 
        tendrec    CHAR(1) 
       END RECORD,
       username    VARCHAR(15),
       haytrn      SMALLINT,
       corinitrn   SMALLINT 
END GLOBALS
