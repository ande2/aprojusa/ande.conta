{
Mantenimiento de tipos de transaccion
ctbmae006.4gl 
MRS 
Octubre 2011 
}

-- Definicion de variables globales 

GLOBALS "ctbglo006.4gl"

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("actiondefaults")
 CALL ui.Interface.loadStyles("styles")
 CALL ui.Interface.loadToolbar("toolbar3")

 -- Verificando parametro para agregar programa a un container
 IF NUM_ARGS()>0 THEN
    CALL ui.Interface.setContainer("mainmenu")
    CALL ui.Interface.setName("tipostransaccionctb")
    CALL ui.Interface.setType("child")
 END IF

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "ayuda.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Obteniendo usuario del sistema
 LET username = FGL_GETENV("LOGNAME")

 -- Menu de principal 
 CALL ctbmae006_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION ctbmae006_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255),
        existe   SMALLINT, 
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "ctbmae006a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header(progname,wpais,1) 
  CALL librut001_dpelement("labela","Accesos a Usuarios") 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Obteniendo parametro de correlativo de tipos de transaccion
  CALL librut003_parametros(6,1)
  RETURNING haytrn,corinitrn 

  -- Menu de opciones
  MENU " Tipos de Transaccion" 
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(progname,4,username) THEN
       HIDE OPTION "Buscar"
    END IF
    --Ingresar
    IF NOT seclib001_accesos(progname,1,username) THEN
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(progname,2,username) THEN
       HIDE OPTION "Modificar"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(progname,3,username) THEN
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de tipos de transaccion."
    CALL ctbqbe006_tipotrns(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo tipo de transaccion."
    LET savedata = ctbmae006_tipotrns(1) 
   COMMAND "Modificar"
    " Modificacion de un tipo de transaccion existente."
    CALL ctbqbe006_tipotrns(2) 
   COMMAND "Borrar"
    " Eliminacion de un tipo de transaccion existente."
    CALL ctbqbe006_tipotrns(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION ctbmae006_tipotrns(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    CALL ctbqbe006_EstadoMenu(4,"") 
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Verificando parametro inicial de correlativo de tipos de transaccion
 IF NOT haytrn THEN
    CALL fgl_winmessage(
    " Atencion:",
    " No existe definido el correlativo para tipos de transaccion. \n"||
    " Definir primero correlativo antes de registrar tipos de transaccion.",
    "information")
    RETURN FALSE 
 END IF 

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL ctbmae006_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nomtrn, 
                w_mae_pro.nomabr,
                w_mae_pro.tipope
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   BEFORE INPUT
    -- Verificando integridad
    -- Si tipo de transaccion ya tiene transaccions no pueden modificarse los 
    -- campos de nombre, tipo de operacion
    IF (operacion=2) THEN 
      -- Verificando integridad
      IF ctbqbe006_integridad() THEN
         -- Deshabilitando campos
         CALL Dialog.SetFieldActive("tipope",FALSE) 
      ELSE 
         -- Habilitando campos
         CALL Dialog.SetFieldActive("tipope",TRUE) 
      END IF
    END IF 

   AFTER FIELD nomtrn  
    --Verificando nombre del tipo de transaccion 
    IF (LENGTH(w_mae_pro.nomtrn)=0) THEN
       ERROR "Error: nombre del tipo de transaccion invalido, VERIFICA."
       LET w_mae_pro.nomtrn = NULL
       NEXT FIELD nomtrn  
    END IF

    -- Verificando que no exista otro tipo de transaccion con el mismo nombre
    SELECT UNIQUE (a.tiptrn)
     FROM  ctb_tipostrn a
     WHERE (a.tiptrn != w_mae_pro.tiptrn) 
       AND (a.nomtrn  = w_mae_pro.nomtrn) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion",
        " Existe otro tipo de transaccion con el mismo nombre, VERIFICA.",
        "information")
        NEXT FIELD nomtrn
     END IF 

   AFTER FIELD nomabr  
    --Verificando nombre abreviado
    IF (LENGTH(w_mae_pro.nomabr)=0) THEN
       ERROR "Error: nombre abreviado del tipo de transaccion invalido, VERIFICA."
       LET w_mae_pro.nomabr = NULL
       NEXT FIELD nomabr  
    END IF

   AFTER FIELD tipope  
    --Verificando tipo de operacion
    IF (w_mae_pro.tipope IS NULL) THEN
       ERROR "Error: tipo de operacion invalida, VERIFICA." 
       NEXT FIELD tipope  
    END IF

   AFTER INPUT
    -- Verificando datos
    IF w_mae_pro.nomtrn IS NULL THEN
       NEXT FIELD nomtrn
    END IF
    IF w_mae_pro.nomabr IS NULL THEN
       NEXT FIELD nomabr
    END IF
    IF w_mae_pro.tipope IS NULL THEN
       NEXT FIELD tipope
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL ctbmae006_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando tipo de transaccion
    CALL ctbmae006_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL ctbqbe006_EstadoMenu(0,"") 
    CALL ctbmae006_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un tipo de transaccion

FUNCTION ctbmae006_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando tipo de transaccion ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.tiptrn),0)
    INTO  w_mae_pro.tiptrn
    FROM  ctb_tipostrn a
    IF (w_mae_pro.tiptrn=0) OR
       (w_mae_pro.tiptrn IS NULL) THEN
       LET w_mae_pro.tiptrn = corinitrn 
    ELSE 
       LET w_mae_pro.tiptrn = (w_mae_pro.tiptrn+1)
    END IF 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO ctb_tipostrn   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.tiptrn 

   --Asignando el mensaje 
   LET msg = "Tipo de Transaccion (",w_mae_pro.tiptrn USING "<<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE ctb_tipostrn
   SET    ctb_tipostrn.*      = w_mae_pro.*
   WHERE  ctb_tipostrn.tiptrn = w_mae_pro.tiptrn 

   --Asignando el mensaje 
   LET msg = "Tipo de Transaccion (",w_mae_pro.tiptrn USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando
   DELETE FROM ctb_tipostrn 
   WHERE (ctb_tipostrn.tiptrn = w_mae_pro.tiptrn)

   --Asignando el mensaje 
   LET msg = "Tipo de Transaccion (",w_mae_pro.tiptrn USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL ctbmae006_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION ctbmae006_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL 
   LET w_mae_pro.tiptrn = 0 
   LET w_mae_pro.userid = username 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.tiptrn THRU w_mae_pro.tipope 
 DISPLAY BY NAME w_mae_pro.tiptrn,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION
