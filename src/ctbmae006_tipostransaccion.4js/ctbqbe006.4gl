{
ctbqbe006.4gl 
Mynor Ramirez
Mantenimiento de tipos de transaccion
}

{ Definicion de variables globales }

GLOBALS "ctbglo006.4gl" 
DEFINE totlin,totusr INT,
       w             ui.Window,
       f             ui.Form

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION ctbqbe006_tipotrns(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qry                 STRING,
        msg                 STRING

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   CALL ctbqbe006_EstadoMenu(operacion," ") 
   INITIALIZE qrytext,qrypart TO NULL
   CALL ctbmae006_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.tiptrn,a.nomtrn,a.nomabr,a.tipope,
                                a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel       
     -- Salida
     CALL ctbmae006_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = 
     " SELECT UNIQUE a.tiptrn,a.nomtrn,"||
             "CASE (a.tipope) WHEN 1 THEN 'Movimiento' "||
             " WHEN 2 THEN 'Referencia' WHEN 3 THEN 'Cierre' END,'' "|| 
      " FROM ctb_tipostrn a "||
      " WHERE a.tiptrn >= ",corinitrn,
        " AND "||qrytext CLIPPED||
      " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_tipotrns SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_tipotrns.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_tipotrns INTO v_tipotrns[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_tipotrns
   FREE  c_tipotrns
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    LET msg = " - Registros [ ",totlin||" ]"
    CALL ctbqbe006_EstadoMenu(operacion,msg)

    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_tipotrns TO s_tipotrns.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel  
      -- Salida
      CALL ctbmae006_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar 
      -- Buscar 
      CALL ctbmae006_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      LET res = ctbmae006_tipotrns(2) 
      -- Desplegando datos
      CALL ctbqbe006_datos(v_tipotrns[ARR_CURR()].ttiptrn)
      CALL ctbqbe006_permxusr(v_tipotrns[ARR_CURR()].ttiptrn,1)

     ON KEY (CONTROL-M)
      -- Modificando 
      IF (operacion=2) THEN 
         -- Modificando 
         LET res = ctbmae006_tipotrns(2) 
         -- Desplegando datos
         CALL ctbqbe006_datos(v_tipotrns[ARR_CURR()].ttiptrn)
         CALL ctbqbe006_permxusr(v_tipotrns[ARR_CURR()].ttiptrn,1)
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF ctbqbe006_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Este tipo de transaccion ya tiene registros. "||
         "\nTipo de transaccion no puede borrarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de Borrar este tipo de transaccion ? "
      LET opc = librut001_menuopcs("Confirmacion",msg,"Si","No",NULL,NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL ctbmae006_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION accesos
      IF (totusr=0) THEN 
         ERROR "Atencion: no existen usuarios con accesos autorizados." 
      END IF 

      -- Desplegando accesos x usuario
      CALL ctbqbe006_detpermxusr(v_tipotrns[ARR_CURR()].ttiptrn)

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Tipo Transaccion"
      LET arrcols[2] = "Nombre"
      LET arrcols[3] = "Nombre Abreviado"
      LET arrcols[4] = "Tipo Operacion"
      LET arrcols[5] = "Usuario Registro"
      LET arrcols[6] = "Fecha Registro"
      LET arrcols[7] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry = 
        "SELECT a.tiptrn,a.nomtrn,a.nomabr,", 
               "CASE (a.tipope) WHEN 1 THEN 'Movimiento' "||
               "WHEN 2 THEN 'Referencia' WHEN 3 THEN 'Cierre' END,"|| 
               "a.userid,a.fecsis,a.horsis ", 
        " FROM ctb_tipostrn a ",
        " WHERE ",qrytext CLIPPED,
        " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res = librut002_excelreport("Tipos de Transacciones Contables",qry,7,0,arrcols)

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)

         -- Verificando acceso a opcion de accesos 
         IF NOT seclib001_accesos(progname,6,username) THEN
            CALL DIALOG.setActionActive("accesos",FALSE)
         ELSE
            CALL DIALOG.setActionActive("accesos",TRUE)
         END IF

         -- Verificando acceso a reporte de excel
         IF NOT seclib001_accesos(progname,5,username) THEN
            CALL DIALOG.setActionActive("reporte",FALSE)
         ELSE
            CALL DIALOG.setActionActive("reporte",TRUE)
         END IF

       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
	 CALL DIALOG.setActionActive("accesos",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
	 CALL DIALOG.setActionActive("accesos",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE

      -- Escondiendo campo de seleccion de accesos de usuario
      CALL f.setFieldHidden("tcheckb",1)

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()<=totlin) THEN
         CALL ctbqbe006_datos(v_tipotrns[ARR_CURR()].ttiptrn)
         CALL ctbqbe006_permxusr(v_tipotrns[ARR_CURR()].ttiptrn,1)
      END IF 

    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen tipos de transaccion con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE

  -- Desplegando estado del menu
  CALL ctbqbe006_EstadoMenu(0,"") 
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION ctbqbe006_datos(wtiptrn)
 DEFINE wtiptrn LIKE ctb_tipostrn.tiptrn,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  ctb_tipostrn a "||
              "WHERE a.tiptrn = "||wtiptrn||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_tipotrnst SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_tipotrnst INTO w_mae_pro.*
 END FOREACH
 CLOSE c_tipotrnst
 FREE  c_tipotrnst

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nomtrn THRU w_mae_pro.tipope 
 DISPLAY BY NAME w_mae_pro.tiptrn,w_mae_pro.userid THRU w_mae_pro.horsis 
END FUNCTION 

-- Subrutina para buscar los accesos a usuarios

FUNCTION ctbqbe006_permxusr(wtiptrn,opcion)
 DEFINE wtiptrn   LIKE ctb_tipostrn.tiptrn,
        i,opcion  SMALLINT,
        qrytext   STRING 

 -- Inicializando vector de datos
 CALL v_permxusr.clear()

 -- Seleccionando datos
 CASE (opcion)
  WHEN 1 -- Consulta de usuarios con acceso
   LET qrytext = "SELECT 1,a.userid,b.nomusr,a.usuaid,a.fecsis,a.horsis "||
                  "FROM  ctb_permxtrn a, glb_usuarios b " ||
                  "WHERE a.tiptrn = "||wtiptrn||
                   " AND b.userid = a.userid "||
                   " ORDER BY a.fecsis DESC " 

  WHEN 2 -- Cargando usuarios para seleccionar acceso
   LET qrytext = "SELECT 0,a.userid,a.nomusr,USER,CURRENT,CURRENT HOUR TO SECOND "||
                  "FROM  glb_usuarios a " ||
                  "WHERE NOT exists (SELECT b.userid FROM ctb_permxtrn b "||
                                     "WHERE b.tiptrn = "||wtiptrn||
                                      " AND b.userid = a.userid) "||
                   "ORDER BY 2" 
 END CASE 
  
 -- Seleccionando datos
 PREPARE cp1 FROM qrytext 
 DECLARE cperm CURSOR FOR cp1 
 LET totusr = 1
 FOREACH cperm INTO v_permxusr[totusr].* 
  LET totusr = (totusr+1) 
 END FOREACH
 CLOSE cperm
 FREE  cperm
 LET totusr = (totusr-1) 

 -- Desplegando datos
 DISPLAY ARRAY v_permxusr TO s_permxusr.*
  BEFORE DISPLAY
   EXIT DISPLAY
 END DISPLAY
END FUNCTION 

-- Subrutina para visualizar el detalle de accesos a usuarios

FUNCTION ctbqbe006_detpermxusr(wtiptrn)
 DEFINE wtiptrn    LIKE ctb_tipostrn.tiptrn,
        loop,opc,i SMALLINT

 -- Desplegando usuarios
 LET loop = TRUE
 WHILE loop
  DISPLAY ARRAY v_permxusr TO s_permxusr.*
   ATTRIBUTE(COUNT=totusr,ACCEPT=FALSE,CANCEL=FALSE)
   ON ACTION cancel  
    -- Salida
    LET loop = FALSE
    EXIT DISPLAY 

   ON ACTION agregarusers
    -- Agregando usuarios 
    CALL ctbqbe006_permxusr(wtiptrn,2) 
    IF (totusr=0) THEN
        CALL fgl_winmessage(
        " Atencion",
        " No existen mas usuarios para agregar.\n Todos los usuarios ya tienen acceso.", 
        "stop")

       -- Cargando usuarios con accesos
       CALL ctbqbe006_permxusr(wtiptrn,1) 
       EXIT DISPLAY 
    ELSE  
      -- Mostrando campo de seleccion de accesos de usuario
      CALL f.setFieldHidden("tcheckb",0)

      -- Seleccionando usuarios
      INPUT ARRAY v_permxusr WITHOUT DEFAULTS FROM s_permxusr.*
       ATTRIBUTE(MAXCOUNT=totusr,INSERT ROW=FALSE,
                 APPEND ROW=FALSE,DELETE ROW=FALSE,
                 ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
       ON ACTION cancel
        -- Salida
        EXIT INPUT 

       ON ACTION accept
        -- Verificando si existen usuarios que agregar
        IF NOT ctbqbe006_hayusuarios(1) THEN
           ERROR "Atencion: no existen usuarios marcados que agregar."
           CONTINUE INPUT
        END IF

        -- Grabando usuarios seleccionados
        LET opc = librut001_menugraba("Confirmacion de Accesos",
                                      "Que desea hacer ?",
                                      "Guardar",
                                      "Modificar",
                                      "Cancelar",
                                      NULL)

        CASE (opc)
         WHEN 0 -- Cancelando
          EXIT INPUT 
         WHEN 1 -- Grabando
          -- Iniciando la transaccion
          BEGIN WORK 

          -- Guardando accesos
          FOR i = 1 TO totusr
           IF (v_permxusr[i].tcheckb=0) OR
              (v_permxusr[i].tcheckb IS NULL) THEN 
              CONTINUE FOR
           END IF 

           -- Grabando  
           SET LOCK MODE TO WAIT
           INSERT INTO ctb_permxtrn 
           VALUES (wtiptrn,
                   v_permxusr[i].tuserid, 
                   v_permxusr[i].tusuaid, 
                   v_permxusr[i].tfecsis, 
                   v_permxusr[i].thorsis)
          END FOR

          -- Terminando la transaccion
          COMMIT WORK 

          EXIT INPUT 
         WHEN 2 -- Modificando
          CONTINUE INPUT 
        END CASE

       BEFORE ROW
        -- Verificando control del total de lineas 
        IF (ARR_CURR()>totusr) THEN
           CALL FGL_SET_ARR_CURR(1)
        END IF 
      END INPUT 

      -- Escondiendo campo de seleccion de accesos de usuario
      CALL f.setFieldHidden("tcheckb",1)

      -- Cargando usuarios con accesos
      CALL ctbqbe006_permxusr(wtiptrn,1) 
      EXIT DISPLAY 
    END IF 

   ON ACTION eliminarusers
    IF (totusr=0) THEN
       ERROR "Atencion: no existen usuarios con accesos autorizados que borrar."
       EXIT DISPLAY 
    END IF  

    -- Eliminando usuarios 
    -- Mostrando campo de seleccion de accesos de usuario
    CALL f.setFieldHidden("tcheckb",0)

    -- Seleccionando usuarios
    INPUT ARRAY v_permxusr WITHOUT DEFAULTS FROM s_permxusr.*
     ATTRIBUTE(MAXCOUNT=totusr,INSERT ROW=FALSE,
               APPEND ROW=FALSE,DELETE ROW=FALSE,
               ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
     ON ACTION cancel
      -- Salida
      EXIT INPUT 

     ON ACTION accept   
      -- Verificando si existen usuarios que borrar
      IF NOT ctbqbe006_hayusuarios(0) THEN
         ERROR "Atencion: no existen usuarios desmarcados que borrar."
         CONTINUE INPUT
      END IF

      -- Grabando usuarios seleccionados
      LET opc = librut001_menugraba("Confirmacion de Accesos",
                                    "Que desea hacer ?",
                                    "Borrar",
                                    "Modificar",
                                    "Cancelar",
                                    NULL)

      CASE (opc)
       WHEN 0 -- Cancelando
        EXIT INPUT 
       WHEN 1 -- Grabando
        -- Iniciando la transaccion
        BEGIN WORK 

        -- Guardando accesos
        FOR i = 1 TO totusr
         IF (v_permxusr[i].tcheckb=1) OR 
            (v_permxusr[i].tcheckb IS NULL) THEN 
            CONTINUE FOR
         END IF 

         -- Eliminando  
         SET LOCK MODE TO WAIT
         DELETE FROM ctb_permxtrn 
         WHERE ctb_permxtrn.tiptrn = wtiptrn
           AND ctb_permxtrn.userid = v_permxusr[i].tuserid 
        END FOR

        -- Terminando la transaccion
        COMMIT WORK 

        EXIT INPUT 
       WHEN 2 -- Modificando
        CONTINUE INPUT 
      END CASE

     BEFORE ROW
      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totusr) THEN
         CALL FGL_SET_ARR_CURR(1)
      END IF 
    END INPUT 

    -- Escondiendo campo de seleccion de accesos de usuario
    CALL f.setFieldHidden("tcheckb",1)

    -- Cargando usuarios con accesos
    CALL ctbqbe006_permxusr(wtiptrn,1) 
    EXIT DISPLAY 

   BEFORE ROW 
    -- Verificando control del total de lineas 
    IF (ARR_CURR()>totusr) THEN
       CALL FGL_SET_ARR_CURR(1)
    END IF 
  END DISPLAY 
 END WHILE 
END FUNCTION 

-- Subrutina para verificar si existen usuarios seleccionados que agregar o que borrar

FUNCTION ctbqbe006_hayusuarios(estado)
 DEFINE i,estado,hayusr SMALLINT

 -- Chequeando estado
 LET hayusr = 0
 FOR i = 1 TO totusr
  -- Verificando estado
  IF (v_permxusr[i].tcheckb=estado) THEN
     LET hayusr = 1
     EXIT FOR
  END IF
 END FOR

 RETURN hayusr
END FUNCTION

-- Subrutina para verificar si el tipo de transaccion ya tiene registros

FUNCTION ctbqbe006_integridad()
 DEFINE conteo INTEGER 

 -- Verificando transaccions 
 SELECT COUNT(*)
  INTO  conteo
  FROM  ctb_mtransac a 
  WHERE (a.tiptrn = w_mae_pro.tiptrn) 
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF
END FUNCTION 

-- Subrutina para desplegar el estado del menu de acuerdo a la operacion a realizar

FUNCTION ctbqbe006_EstadoMenu(operacion,msg)
 DEFINE operacion SMALLINT,
        msg       STRING

 -- Desplegando estado del menu
 CASE (operacion)
  WHEN 0 CALL librut001_dpelement("labelx","Lista de Tipos de Transaccion - MENU")
  WHEN 1 CALL librut001_dpelement("labelx","Lista de Tipos de Transaccion - BUSCAR"||msg)
  WHEN 2 CALL librut001_dpelement("labelx","Lista de Tipos de Transaccion - MODIFICAR"||msg)
  WHEN 3 CALL librut001_dpelement("labelx","Lista de Tipos de Transaccion - BORRAR"||msg)
  WHEN 4 CALL librut001_dpelement("labelx","Lista de Tipos de Transaccion - NUEVO")
 END CASE
END FUNCTION
